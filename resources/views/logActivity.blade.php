@extends('layouts.app')

@section('content')

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Activity Log')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>{{__('No')}}</th>
                    <th>{{__('Subject')}}</th>
                    <th>{{__('URL')}}</th>
                    <th>{{__('Method')}}</th>
                    <th>{{__('Ip')}}</th>
                    <th width="300px">{{__('User Agent')}}</th>
                    <th>{{__('User Id')}}</th>
                </tr>
            </thead>
            <tbody>
                @if($logs->count())
                    @foreach($logs as $key => $log)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $log->subject }}</td>
                        <td class="text-success">{{ $log->url }}</td>
                        <td><label class="label label-info">{{ $log->method }}</label></td>
                        <td class="text-warning">{{ $log->ip }}</td>
                        <td class="text-danger">{{ $log->agent }}</td>
                        <td>{{ $log->user_id }}</td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>

    </div>
</div>

@endsection
