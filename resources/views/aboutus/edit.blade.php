@extends('layouts.app')

@section('content')

<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('AboutUs Page Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('aboutus.update', $about->id) }}" method="POST" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="PATCH">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left" for="heading">{{__('Heading')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Enter your heading here')}}" id="heading" name="heading" class="form-control" value="{{ $about->heading }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left" for="content">{{__('Content')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Enter your content here')}}" name="content" id="content" class="form-control" value="{{ $about->content }}">
                    </div>
                </div>
               
             
                
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
