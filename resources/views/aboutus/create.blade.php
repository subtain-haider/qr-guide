@extends('layouts.app')

@section('content')

<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Create new Content')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('aboutus.store') }}" method="POST" enctype="multipart/form-data">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="heading">{{__('Heading')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="Write your heading here" id="heading" name="heading" class="form-control" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="content">{{__('Content')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="Write your content here" id="content" name="content" class="form-control" required="">
                    </div>
                </div>
               
                
              
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
