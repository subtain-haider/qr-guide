@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('homediv.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Content')}}</a>
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Home Sections')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Icon')}}</th>
                    <th>{{__('Heading')}}</th>
                    <th>{{__('Content')}}</th>
                    {{-- <th>{{__('Status')}}</th> --}}
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($homedivs as $key => $homediv)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td><i class="{{ $homediv->icon }}"></i></td>
                        <td>{{ $homediv->heading }}</td>
                        <td>{{ $homediv->content }}</td>
                       
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{route('homediv.edit', encrypt($homediv->id))}}">{{__('Edit')}}</a></li>
                                    <li><a onclick="confirm_modal('{{route('homediv.destroy', $homediv->id)}}');">{{__('Delete')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

@endsection
