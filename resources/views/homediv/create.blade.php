@extends('layouts.app')
<style>
    .click-btn-sel-div{
            display: flex;
            margin-bottom: 5px;
        }
        .click-btn-sel-div p{
            font-size: 12px;
            margin-bottom: 0px;
            font-weight: bold;

        }
        .click-btn-sel-div a{
            font-size: 12px;
            color: #198EA2;
            font-weight: bold;
            padding-left: 5px;
        }



</style>
@section('content')

<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Create new homa page div')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('homediv.store') }}" method="POST" enctype="multipart/form-data">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="icon">{{__('Icon')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="Write icon class here..." id="icon" name="icon" class="form-control" required=""><br>
                            <div class="click-btn-sel-div">
                            <p type="button"  data-toggle="modal" data-target="#exampleModal">
                              Click here for icon instructions
                            </p>
                            <a href="https://fontawesome.com/v5.15/icons?d=gallery&p=2">FontAwesome Link</a>
                            </div>
                    </div>
                  
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="heading">{{__('Heading')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="Write your heading here..." id="heading" name="heading" class="form-control" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="content">{{__('Content')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="Write your content here..." id="content" name="content" class="form-control" required="">
                    </div>
                </div>
               
                
              
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">FontAwesome Instructions</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <p>Click on fontawsome link for getting icons of your own choice. after clicking on icon you were redirect to new page with icon tags. (eg. < i class="fas fa-abacus"> < / i>. you can check it from <a href="https://fontawesome.com/v5.15/icons/abacus?style=solid" target="_blank">this link</a>). <br>Now you just have to put i tag class in icon input box. (eg. fas fa-abacus )</p>
                      </div>
                    </div>
                  </div>
                </div>
    </div>
</div>

@endsection
