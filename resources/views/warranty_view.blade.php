@extends('layouts.app')

@section('content')

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{ $shop->name }} {{__('Warranty')}}</h3>
    </div>
    <div class="panel-body">
        <div class="col-md-4">
            <div class="panel-heading">
                <h3 class="text-lg">{{__('Ventor Info')}}</h3>
            </div>
            <div class="row">
                <label class="col-sm-3 control-label" for="name">{{__('Name')}}</label>
                <div class="col-sm-9">
                    <p>{{ $user->name }}</p>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-3 control-label" for="name">{{__('Email')}}</label>
                <div class="col-sm-9">
                    <p>{{ $user->email }}</p>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-3 control-label" for="name">{{__('Address')}}</label>
                <div class="col-sm-9">
                    <p>{{ $user->address }}</p>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-3 control-label" for="name">{{__('Phone')}}</label>
                <div class="col-sm-9">
                    <p>{{ $user->phone }}</p>
                </div>
            </div>


            <div class="panel-heading">
                <h3 class="text-lg">{{__('Shop Info')}}</h3>
            </div>

            <div class="row">
                <label class="col-sm-3 control-label" for="name">{{__('Shop Name')}}</label>
                <div class="col-sm-9">
                    <p>{{ $shop->name }}</p>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-3 control-label" for="name">{{__('Address')}}</label>
                <div class="col-sm-9">
                    <p>{{ $shop->address }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            @if($warranty->user_id != null)
                <div class="panel-heading">
                    <h3 class="text-lg">{{__('User Info')}}</h3>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label" for="name">{{__('Name')}}</label>
                    <div class="col-sm-9">
                        <p>{{ \App\Models\User::where('id', $warranty->user_id)->first()->name }}</p>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label" for="name">{{__('Email')}}</label>
                    <div class="col-sm-9">
                        <p>{{ \App\Models\User::where('id', $warranty->user_id)->first()->email }}</p>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label" for="name">{{__('Address')}}</label>
                    <div class="col-sm-9">
                        <p>{{ \App\Models\User::where('id', $warranty->user_id)->first()->address }}</p>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 control-label" for="name">{{__('Phone')}}</label>
                    <div class="col-sm-9">
                        <p>{{ \App\Models\User::where('id', $warranty->user_id)->first()->phone }}</p>
                    </div>
                </div>
            @endif

            <div class="panel-heading">
                <h3 class="text-lg">{{__('Warranty Query')}}</h3>
            </div>
            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                <tbody>
                    @foreach (json_decode($warranty->info) as $key => $info)
                        <tr>
                            <th>{{ $info->label }}</th>
                            @if ($info->type == 'text' || $info->type == 'select' || $info->type == 'radio')
                                <td>{{ $info->value }}</td>
                            @elseif ($info->type == 'multi_select')
                                <td>
                                    {{ implode(json_decode($info->value), ', ') }}
                                </td>
                            @elseif ($info->type == 'file')
                                <td>
                                    <a href="{{ asset($info->value) }}" target="_blank" class="btn-info">{{__('Click here')}}</a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
