@extends('layouts.app')

@section('content')

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('orders')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Order Code</th>
					<th>Customer</th>
					<th>Num. of Products</th>
                    <th>Delivery Status</th>
					<th>Seller</th>
                    <th>Dr</th>
					<th>Cr</th>
					<th>Balance</th>
                    <th>Payment Status</th>
                    <th width="10%">{{__('options')}}</th>
                </tr>
            </thead>
            <tbody>
				@php
					$row = 1;
				@endphp
                @foreach ($orders as $key => $order)
					@foreach (\App\Models\OrderDetail::where('order_id', $order->id)->groupBy('seller_id')->get() as $orderDetail)
                    <tr>
                        <td>
                            {{ $row }}
                        </td>
							@php
								$seller_id = $orderDetail->seller_id;
							@endphp
						<td>
                            <a href="#{{ $order->code }}" onclick="show_order_details({{ $order->id }},{{ $seller_id }})">{{ $order->code }}</a>
                        </td>
						<td>
                            @if ($order->user_id != null)
                                {{ $order->user->name }}
                            @else
                                Guest ({{ $order->guest_id }})
                            @endif
                        </td>
                        <td>
                            @php 
								echo $order->orderDetails->where('seller_id', $orderDetail->seller_id)->count(); 
							@endphp
                        </td>
                        <td>
                            @php
                                $status = 'Delivered';
                                foreach ($order->orderDetails as $key => $orderDetail) {
                                    if($orderDetail->delivery_to_admin_status != 'delivered'){
                                        $status = 'Pending';
                                    }
                                }
                            @endphp
                            {{ $status }}
                        </td>
						<td>
                            @php
                                $seller_name = '';
								//$user_id = \App\Models\Seller::where('id', $orderDetail->seller_id)->first()->user_id;
								$seller_name = \App\Models\User::where('id', $orderDetail->seller_id)->first();
								if(isset($seller_name->name)){
									echo $seller_name->name;
								}else{
									echo '--';
								}
							@endphp
                        </td>
						<td>
                            {{ single_price($order->orderDetails->where('seller_id', $orderDetail->seller_id)->sum('price')) }}
                        </td>
						<td>
                            {{ single_price($order->orderDetails->where('seller_id', $orderDetail->seller_id)->sum('admin_to_seller_payment')) }}
                        </td>
						<td>
                            {{ single_price(($order->orderDetails->where('seller_id', $orderDetail->seller_id)->sum('price')-$order->orderDetails->where('seller_id', $orderDetail->seller_id)->sum('admin_to_seller_payment'))) }}
                        </td>
                        <td>
                            <span class="badge badge--2 mr-4">
                                @if ($order->orderDetails->where('seller_id', $orderDetail->seller_id)->first()->payment_status == 'paid')
                                    <i class="bg-green"></i> Paid
                                @else
                                    <i class="bg-red"></i> Unpaid
                                @endif
                            </span>
                        </td>
                        <td>
							<div class="btn-group dropdown">
								<button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a onclick="show_order_details({{ $order->id }},{{ $seller_id }})">{{__('View')}}</a></li>
                                    <li>@php if(($order->orderDetails->where('seller_id', $orderDetail->seller_id)->sum('price')-$order->orderDetails->where('seller_id', $orderDetail->seller_id)->sum('admin_to_seller_payment')) > 0){ @endphp<a onclick="show_payment_details({{ $order->id }},{{ $seller_id }})" class="dropdown-item">{{__('Pay Seller')}}</a>@php }else{ @endphp <a onclick="show_payment_details({{ $order->id }},{{ $seller_id }})" class="dropdown-item">{{__('View Invoice')}}</a> @php } @endphp</li>
									<li><a href="{{ route('customer.invoice.download', $order->id) }}">{{__('Download Invoice')}}</a></li>
                                    <li><a onclick="confirm_modal('{{route('orders.destroy', $order->id)}}');">{{__('Delete')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
					@php
						$row++;
					@endphp
					@endforeach
                @endforeach
            </tbody>
        </table>

    </div>
	<div class="modal fade" id="order_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="order-details-modal-body">

                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
    <script type="text/javascript">
		function show_payment_details(order_id,seller_id)
		{
			$('#order-details-modal-body').html(null);

			if(!$('#modal-size').hasClass('modal-lg')){
				$('#modal-size').addClass('modal-lg');
			}

			$.post('{{ route('sales.pay_seller') }}', { _token : '{{ @csrf_token() }}', order_id : order_id, seller_id : seller_id}, function(data){
				$('#order-details-modal-body').html(data);
				$('#order_details').modal();
				$('.c-preloader').hide();
			});
		}
		function show_order_details(order_id,seller_id)
		{
			$('#order-details-modal-body').html(null);

			if(!$('#modal-size').hasClass('modal-lg')){
				$('#modal-size').addClass('modal-lg');
			}

			$.post('{{ route('orders.details') }}', { _token : '{{ @csrf_token() }}', order_id : order_id, seller_id : seller_id, from : 'admin'}, function(data){
				$('#order-details-modal-body').html(data);
				$('#order_details').modal();
				$('.c-preloader').hide();
			});
		}
		
		$(document).ready(function(){
			$(document).on('change', ".payment_type", function () {
				$('.payment_details_block').addClass('hide');
				$('.payment_details_block input').val('');
				if($(this).val() != ''){
					$('.payment_details_block').removeClass('hide');
				}
			});
		});
    </script>
@endsection
