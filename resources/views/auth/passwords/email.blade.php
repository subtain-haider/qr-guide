@extends('frontend.layouts.new_app_1')

@section('content')

    <section class="recover-pswrd-page-form-sec pt-5">
        <div class="signin-form mt-5">
            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <div class="signin-form-sec-logo mb-4">
                    <a href="javascript:void();"><img src="{{ asset('frontend/images/rounded-logo.png') }}"></a>
                </div>
                <h2>{{__('Recover Password')}}</h2>
                <p>{{__('Enter your registered email below to receive password reset code')}}</p>
                
                <div class="form-group">
                    <input type="email" class="form-control input-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="{{__('Email Address')}}" required="required">
                    <i class="fa fa-envelope-o"></i>
                </div>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg btn-block signin-btn">{{__('Send Code')}}</button>
                </div>
                <div class="recover-pswrd-page-try">
                    <a href="javascript:void();">{{__('Try Another Way')}}</a>
                </div>
                
            </form>
        </div>
    </section>

    <!-- subscribe -->
    <!--<section class="subscribeser">-->
    <!--    <div class="flex-1ser">-->
    <!--        <div class="page-1ser">-->
                
    <!--            <div class="sub-dataser">-->
    <!--                <div class="sub-datasser">-->
    <!--                <h2>Subscribe to our Newsletter</h2>-->
    <!--                    <div>-->
    <!--                        <hr>-->
    <!--                    </div>	-->
    <!--                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod-->
    <!--                tempor incididunt ut labore et dolore magna aliqua. </p>-->
    <!--                </div>   			-->
    <!--            </div>-->

    <!--            <div class="flex-1ser flex-1ser-dirser">-->
    <!--                <div class="sub-width-1ser">-->
    <!--                    <input type="email" name="email" placeholder="Enter Your Email...">-->
    <!--                </div>-->
    <!--                <div class="sub-width-2ser">-->
    <!--                    <button>Subscribe</button>-->
    <!--                </div>-->
    <!--            </div>-->

    <!--        </div>-->
    <!--    </div>-->
    <!--</section>	-->


    <!-- footer -->
    <!--<section class="footerser">-->
    <!--    <div class="flex-1">-->
    <!--        <div class="page-1">-->
    <!--            <div class="flex-2ser wrap padser">-->
    <!--                <div class="wftsser">-->
    <!--                    <h3>Our Office</h3>      -->
    <!--                    <p>3rd Floor,Estate House 200 North<br>City, Atlantica SA 3000</p>    -->
    <!--                </div>-->
    <!--                <div class="wftsser">-->
    <!--                    <h3>Contact Info</h3>-->
    <!--                    <p>Cell: 0123 456 789 0, 0123 456 789 0<br>E-mail: username@yourmail.com</p>-->
    <!--                </div>-->
    <!--                <div class="wftsser">-->
    <!--                    <a href="#">Download App From <i class="fa fa-windows"></i></a>-->
    <!--                    <h1>Follow Us On : <i class="fa fa-instagram"></i><i class="fa fa-facebook"></i><i class="fa fa-twitter"></i></h1>-->
    <!--                </div>-->
    <!--                <div class="wftsser">-->
    <!--                    <a href="#">Download App From <i class="fa fa-apple"></i></a>-->
    <!--                    <h1>Support : 24/7</h1>-->
    <!--                </div>-->

    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->

    <!-- copyrights  -->
    <!--<section class="copyser">-->
    <!--    <div class="flex-1ser">-->
    <!--        <div class="page-1ser flex-1ser">-->
    <!--            <div class="copy-dataser">-->
    <!--                <p>&copy Copyrights musicapp_Design All rights reserved.</p>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>   -->
@endsection

@section('script')
<script type="text/javascript">
    var sermoblmenu = document.getElementById("sermoblmenu");
    function sershowsec(){
        sermoblmenu.style.display = "flex";
        sermoblmenu.style.left = "0%";
    }
    function serhidesec(){
        sermoblmenu.style.left = "-150%";
    }
    
    $(document).ready(function () {
		$(".header-searchser-select-detail").hide();
        $('.header-searchser-select-head').click(function () {
            $('.header-searchser-select-detail').toggle();
        });
        $('.header-searchser-select-pro-data').click(function () {
            $('.header-searchser-select-head h3').html($(this).text()+'<i class="fa fa-angle-down"></i>');
            $('.header-searchser-select-detail').hide();
        });
    });
</script>
@endsection