@extends('frontend.layouts.new_app_1')

@section('content')

    <section class="change-pswrd-page-form-sec pt-5">
        <div class="signin-form mt-5">
            <form method="POST" action="{{ route('password.update') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="signin-form-sec-logo mb-4">
                    <a href="javascript:void();"><img src="{{ asset('frontend/images/rounded-logo.png') }}"></a>
                </div>
                <h2>{{__('Change Password')}}</h2>
                <p>{{__('To change your account password, please fill in the fields below:')}}</p>
                
                <div class="form-group">
                    <input type="email" class="form-control input-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" placeholder="Email" required autofocus>	
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <input id="password" type="password" class="form-control input-lg{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="New Password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <input type="password" class="form-control input-lg" name="password_confirmation" placeholder="Retype Password" required="required">
                </div>
                
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg btn-block signin-btn">Change Password</button>
                </div>
            </form>
        </div>
    </section>

 

    
@endsection

@section('script')
<script type="text/javascript">
    var sermoblmenu = document.getElementById("sermoblmenu");
    function sershowsec(){
        sermoblmenu.style.display = "flex";
        sermoblmenu.style.left = "0%";
    }
    function serhidesec(){
        sermoblmenu.style.left = "-150%";
    }
    
    $(document).ready(function () {
		$(".header-searchser-select-detail").hide();
        $('.header-searchser-select-head').click(function () {
            $('.header-searchser-select-detail').toggle();
        });
        $('.header-searchser-select-pro-data').click(function () {
            $('.header-searchser-select-head h3').html($(this).text()+'<i class="fa fa-angle-down"></i>');
            $('.header-searchser-select-detail').hide();
        });
    });
</script>
@endsection