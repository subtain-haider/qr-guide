@extends('layouts.app')
@section('content')
{{-- @dd(Auth::user()->staff->role->permissions);
@dd(Auth::user()->staff->role->permissions); --}}
@if(Auth::user()->user_type == 'admin' || in_array('1', json_decode(Auth::user()->staff->role->permissions)))
<div class="row">
   <div class="col-md-6">
      <div class="panel">
         <div class="panel-body text-center dash-widget dash-widget-left">
            <div class="dash-widget-vertical">
               <div class="rorate">PRODUCTS</div>
            </div>
            <div class="pad-ver mar-top text-main">
               <i class="demo-pli-data-settings icon-4x"></i>
            </div>
            <br>
            @if(!in_array('14', json_decode(Auth::user()->staff->role->permissions)))
            <p class="text-lg text-main">Total published products: <span class="text-bold">{{ \App\Models\Product::where('published', 1)->get()->count() }}</span></p>
            @if (\App\Models\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
            <p class="text-lg text-main">Total seller's products: <span class="text-bold">{{ \App\Models\Product::where('published', 1)->where('added_by', 'seller')->get()->count() }}</span></p>
            @endif								@else									@php						$total_customers = 0;												$user = auth()->user();												$sellers = \App\Models\Seller::where('referral', $user->ref_code)->orderBy('created_at', 'desc')->get();											foreach($sellers as $seller){													$total_customers += \App\Models\Product::where('published', 1)->where('user_id', $seller->id)->count();													}											@endphp
            <p class="text-lg text-main">Total published products: <span class="text-bold">{{ $total_customers }}</span></p>
            @endif
            @if(!in_array('14', json_decode(Auth::user()->staff->role->permissions)))
            <p class="text-lg text-main">Total admin's products: <span class="text-bold">{{ \App\Models\Product::where('published', 1)->where('added_by', 'admin')->get()->count() }}</span></p>
            <br>
            <a href="{{ route('products.admin') }}" class="btn btn-primary mar-top">Manage Products <i class="fa fa-long-arrow-right"></i></a>								@else								<br>								<br>								<br>								@endif
         </div>
      </div>
   </div>
   @if(!in_array('14', json_decode(Auth::user()->staff->role->permissions)))
   <div class="col-md-6">
      <div class="row">
         <div class="col-sm-6">
            <div class="panel">
               <div class="pad-top text-center dash-widget">
                  <p class="text-normal text-main">Total product category</p>
                  <p class="text-semibold text-3x text-main">{{ \App\Models\Category::all()->count() }}</p>
                  <a href="{{ route('categories.create') }}" class="btn btn-primary mar-top btn-block top-border-radius-no">Create Category</a>
               </div>
            </div>
            <div class="panel">
               <div class="pad-top text-center dash-widget">
                  <p class="text-normal text-main">Total product sub sub category</p>
                  <p class="text-semibold text-3x text-main">{{ \App\Models\SubSubCategory::all()->count() }}</p>
                  <a href="{{ route('subsubcategories.create') }}" class="btn btn-primary mar-top btn-block top-border-radius-no">Create Sub Sub Category</a>
               </div>
            </div>
         </div>
         <div class="col-sm-6">
            <div class="panel">
               <div class="pad-top text-center dash-widget">
                  <p class="text-normal text-main">Total product sub category</p>
                  <p class="text-semibold text-3x text-main">{{ \App\Models\SubCategory::all()->count() }}</p>
                  <a href="{{ route('subcategories.create') }}" class="btn btn-primary mar-top btn-block top-border-radius-no">Create Sub Category</a>
               </div>
            </div>
            <div class="panel">
               <div class="pad-top text-center dash-widget">
                  <p class="text-normal text-main">Total product brand</p>
                  <p class="text-semibold text-3x text-main">{{ \App\Models\Brand::all()->count() }}</p>
                  <a href="{{ route('brands.create') }}" class="btn btn-primary mar-top btn-block top-border-radius-no">Create Brand</a>
               </div>
            </div>
         </div>
      </div>
   </div>
   @endif
</div>
@endif
@if((Auth::user()->user_type == 'admin' || in_array('5', json_decode(Auth::user()->staff->role->permissions))) && \App\Models\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
<div class="row">
   <div class="col-md-4">
      <div class="panel">
         <div class="panel-body text-center dash-widget dash-widget-left">
            <div class="dash-widget-vertical">
               <div class="rorate">SELLERS</div>
            </div>
            <br>
            <p class="text-normal text-main">Total sellers</p>
            @if(in_array('14', json_decode(Auth::user()->staff->role->permissions)))					@php						$user = auth()->user();					@endphp
            <p class="text-semibold text-3x text-main">{{ \App\Models\Seller::where('referral', $user->ref_code)->count() }}</p>
            <br>									@else
            <p class="text-semibold text-3x text-main">{{ \App\Models\Seller::all()->count() }}</p>
            <br>
            <a href="{{ route('sellers.index') }}" class="btn-link">Manage Sellers <i class="fa fa-long-arrow-right"></i></a>								@endif
            <br>
            <br>
         </div>
      </div>
   </div>
   <div class="col-md-4">
      <div class="panel">
         <div class="panel-body text-center dash-widget">
            <br>
            <p class="text-normal text-main">Total approved sellers</p>
            @if(in_array('14', json_decode(Auth::user()->staff->role->permissions)))					@php						$user = auth()->user();					@endphp
            <p class="text-semibold text-3x text-main">{{ \App\Models\Seller::where('verification_status', 1)->where('referral', $user->ref_code)->count() }}</p>
            <br>									@else
            <p class="text-semibold text-3x text-main">{{ \App\Models\Seller::where('verification_status', 1)->get()->count() }}</p>
            <br>
            <a href="{{ route('sellers.index') }}" class="btn-link">Manage Sellers <i class="fa fa-long-arrow-right"></i></a>								@endif
            <br>
            <br>
         </div>
      </div>
   </div>
   <div class="col-md-4">
      <div class="panel">
         <div class="panel-body text-center dash-widget">
            <br>
            <p class="text-normal text-main">Total pending sellers</p>
            @if(in_array('14', json_decode(Auth::user()->staff->role->permissions)))					@php						$user = auth()->user();					@endphp
            <p class="text-semibold text-3x text-main">{{ \App\Models\Seller::where('verification_status', 0)->where('referral', $user->ref_code)->count() }}</p>
            <br>									@else
            <p class="text-semibold text-3x text-main">{{ \App\Models\Seller::where('verification_status', 0)->count() }}</p>
            <br>
            <a href="{{ route('sellers.index') }}" class="btn-link">Manage Sellers <i class="fa fa-long-arrow-right"></i></a>								@endif
            <br>
            <br>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-md-4">
      <div class="panel">
         <div class="panel-body text-center dash-widget dash-widget-left">
            <div class="dash-widget-vertical">
               <div class="rorate">Companies</div>
            </div>
            @if(\App\Models\PricingPlan::count() > 0)
            <br>
            <p class="text-normal text-main">Total {{\App\Models\PricingPlan::first()->name}}</p>
            <p class="text-semibold text-3x text-main">{{ \App\Models\Shop::where('package', \App\Models\PricingPlan::first()->id)->count() }}</p>
            <br>
            <a href="{{ route('sellers.filter', \App\Models\PricingPlan::first()->id) }}" class="btn-link">Manage Compnies <i class="fa fa-long-arrow-right"></i></a>
            <br>
            <br>
            @endif
         </div>
      </div>
   </div>
   @if(\App\Models\PricingPlan::count() > 0)
   @foreach(\App\Models\PricingPlan::all() as $key=>$Plan)
   @if($key > 0)
   <div class="col-md-4">
      <div class="panel">
         <div class="panel-body text-center dash-widget">
            <br>
            <p class="text-normal text-main">Total {{$Plan->name}}</p>
            <p class="text-semibold text-3x text-main">{{ \App\Models\Shop::where('package', $Plan->id)->count() }}</p>
            <br>
            <a href="{{ route('sellers.filter', $Plan->id) }}" class="btn-link">Manage Compnies <i class="fa fa-long-arrow-right"></i></a>
            <br>
            <br>
         </div>
      </div>
   </div>
   @endif
   @endforeach
   @endif
</div>
@endif
@endsection
