@extends('layouts.app')

@section('content')

<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Faq Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('faq.update', $faq->id) }}" method="POST" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="PATCH">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left" for="question">{{__('Question')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Question')}}" id="question" name="question" class="form-control" value="{{ $faq->question }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left" for="answer">{{__('Answer')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Answer')}}" name="answer" id="answer" class="form-control" value="{{ $faq->answer }}">
                    </div>
                </div>
               
             
                
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
