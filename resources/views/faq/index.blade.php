@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('faq.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New FAQ')}}</a>
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('FAQs')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    
                    <th>{{__('Question')}}</th>
                    <th>{{__('Answer')}}</th>
                    {{-- <th>{{__('Status')}}</th> --}}
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($faqs as $key => $faq)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{ $faq->question }}</td>
                        <td>{{ $faq->answer }}</td>
                       
                        {{-- <td>
                            <label class="switch">
                                <input onchange="update_featured(this)" value="{{ $pricing_plan->id }}" type="checkbox" @if($pricing_plan->status == 1) echo "checked"; @endif >
                                <span class="slider round"></span>
                            </label>
                        </td> --}}
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{route('faq.edit', encrypt($faq->id))}}">{{__('Edit')}}</a></li>
                                    <li><a onclick="confirm_modal('{{route('faq.destroy', $faq->id)}}');">{{__('Delete')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

@endsection
