@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('sellers.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Seller')}}</a>
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('sellers')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('Seller') }}</th>
                    <!--<th>{{__('Shop Name')}}</th>-->
                    <th>{{__('City')}}</th>
                    <th>{{__('Contact No.')}}</th>
                    <th>{{ __('Num. of Products') }}</th>
                    
                    <th>{{__('Plan')}}</th>
                    <th>{{__('Start')}}</th>
                    <th>{{__('End')}}</th>
                    {{-- <th>{{__('Registration Date')}}</th> --}}
					<th>{{__('Status')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sellers as $key => $seller)
                    @if(\App\Models\User::where('id', $seller->id)->first() != '')
						<tr>
							<td>{{$key+1}} </td>
							<td>{{ $seller->name }}</td>
							@php
								$seller_name = $seller_email = $seller_country = $seller_city = $created_at = ''; 
								$seller_details = \App\Models\User::where('id', $seller->id)->first();
								if(isset($seller_details->name)){
									$seller_city = $seller_details->city;
								}else{
									$seller_city = \App\Models\User::where('id', $seller->id)->first()->city;
								}
							@endphp
							<!--<td><a href="{{route('company.visit', 'test')}}" target="_blank">@if(\App\Models\Shop::where('user_id', $seller->id)->first() != NULL){{ \App\Models\Shop::where('user_id', $seller->id)->first()->title }} @endif</a></td>-->
							<td>{{ $seller_city }}</td>
							<td>{{ $seller->phone }}</td>
							<td>{{ \App\Models\Product::where('user_id', $seller->id)->count() }}</td>
							
							<td>
								@if(\App\Models\PricingPlan::where('id', $seller->package)->first() != null)
									{{ \App\Models\PricingPlan::where('id', $seller->package)->first()->name }}
								@endif
							</td>
							<td>
								@if(\App\Models\PricingPlan::where('id', $seller->package)->first() != null)
									@if($seller->package_for != '')
										{{date('Y-m-d', strtotime($seller->package_start))}} 
									@endif
								@endif
							</td>
							<td>
								@if(\App\Models\PricingPlan::where('id', $seller->package)->first() != null)
									@if($seller->package_for != '')
										{{date('Y-m-d', strtotime($seller->package_end))}} 
									@endif
								@endif
							</td>
							<!--<td>{{ $seller->created_at }}</td>-->
							<td>
							    
							    
								@if ($seller->status == 1)
									<div class="label label-table label-success">
										<a style="color: #fff;" href="{{route('sellers.reject', $seller->id)}}">{{__('Verified')}}</a>
									</div>
								@elseif ($seller->verification_info != null)
									<a href="{{ route('sellers.show_verification_request', $seller->id) }}">
										<div class="label label-table label-info">
											{{__('Requested')}}
										</div>
									</a>
								@else
									<div class="label label-table label-danger">
										<a style="color: #fff;" href="{{route('sellers.approve', $seller->id)}}">{{__('Not Verified')}}</a>
									</div>
								@endif
							</td>
						</tr>
					@else
						<tr>
							<td>{{ $key+1 }}</td>
							<td>{{$seller->id}}</td>
							<td>{{__('Not Available')}}</td>
							<td>{{__('Not Available')}}</td>
							<td>{{__('Not Available')}}</td>
							<td>
								@if ($seller->verification_status == 1)
									<div class="label label-table label-success">
										{{__('Verified')}}
									</div>
								@elseif ($seller->verification_info != null)
									<a href="{{ route('sellers.show_verification_request', $seller->id) }}">
										<div class="label label-table label-info">
											{{__('Requested')}}
										</div>
									</a>
								@else
									<div class="label label-table label-danger">
										{{__('Not Verified')}}
									</div>
								@endif
							</td>
							<td>{{ \App\Models\Product::where('user_id', $seller->id)->count() }}</td>
							<td>
								@if ($seller->admin_to_pay > 0)
									{{ single_price($seller->admin_to_pay) }}
								@else
									{{ single_price(0) }}
								@endif
							</td>
							<td>
								<div class="btn-group dropdown">
									<button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
										{{__('Actions')}} <i class="dropdown-caret"></i>
									</button>
									<ul class="dropdown-menu dropdown-menu-right">
										@if ($seller->verification_status == 1)
											<li><a href="{{route('sellers.reject', $seller->id)}}">{{__('Block')}}</a></li>
										@else
											<li><a href="{{route('sellers.approve', $seller->id)}}">{{__('Approve')}}</a></li>
										@endif
										<li><a href="{{route('sellers.seller_details', $seller->id)}}">{{__('View Details')}}</a></li>
										<li><a onclick="show_seller_payment_modal('{{$seller->id}}');">{{__('Pay')}}</a></li>
										<li><a href="{{route('sellers.edit', encrypt($seller->id))}}">{{__('Edit')}}</a></li>
										<li><a onclick="confirm_modal('{{route('sellers.destroy', $seller->id)}}');">{{__('Delete')}}</a></li>
									</ul>
								</div>
							</td>
						</tr>
					@endif
                @endforeach
            </tbody>
        </table>

    </div>
</div>


<div class="modal fade" id="payment_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="modal-content">

        </div>
    </div>
</div>


@endsection

@section('script')
    <script type="text/javascript">
        function show_seller_payment_modal(id){
            $.post('{{ route('sellers.payment_modal') }}',{_token:'{{ @csrf_token() }}', id:id}, function(data){
                $('#modal-content').html(data);
                $('#payment_modal').modal('show', {backdrop: 'static'});
                $('.demo-select2-placeholder').select2();
            });
        }
    </script>
@endsection
