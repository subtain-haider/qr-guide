@extends('layouts.app')

@section('content')

@php
	$generalsetting = \App\Models\GeneralSetting::first();
@endphp

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="row">
		<div class="col-md-12">
			<div class="col-md-4">
				<div class="panel-body text-center">
					<span class="store-icon-block">
						<img src="{{ asset('img/black-user-icon.png') }}" alt="" class="img-fluid">
					</span>
					<h3 class="panel-title"><a style="color: red;font-weight: bold;">@php echo fetchTinyUrl(env('APP_URL').'users/registration?ref='.urlencode(Auth::user()->ref_code)); @endphp</a></h3>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="col-md-12 mb-2 mt-5 text-center"><strong>{{ $generalsetting->site_name }}<h4>Customer Registration</h4></strong></div>
				<div class="col-md-12 text-center"><hr class="mt-2"></div>
				<div class="col-md-12 text-center">Invite your friends and family to register and get reward.</div>
			</div>
			
			<div class="col-md-4 text-center">
				<div class="col-md-12 mt-4"><a class="btn btn-success">{{$customer_points_points}}<br>Points</a></div>
				<div class="col-md-12 no-gutters mt-4">
					<div id="share">
						<a title="Share anywhere" class="h2 bg-black share-button" style="display: inline-block; height:32px; width:32px; line-height:normal; margin:2px;"><i class="fa fa-share-alt" style="padding-top:2px; padding-left:3px; padding-right:3px; position: absolute; margin-left: -16px;"></i></a>
						<a href="https://api.whatsapp.com/send?phone=&text=@php echo fetchTinyUrl(env('APP_URL').'users/registration?ref='.urlencode(Auth::user()->ref_code)); @endphp" title="Share this page on whatsapp" class="h2 bg-black" style="display: inline-block; height:32px; width:32px; line-height:normal; margin:2px;"><i class="fa fa-share-alt" style="padding-top:2px; padding-left:3px; padding-right:3px; position: absolute; margin-left: -16px;"></i></a>
						<a href="https://api.whatsapp.com/send?phone=&text=@php echo fetchTinyUrl(env('APP_URL').'users/registration?ref='.urlencode(Auth::user()->ref_code)); @endphp" title="Share this page on whatsapp" class="h2 bg-green" style="display: inline-block; height:32px; width:32px; line-height:normal; margin:2px;"><i class="fa fa-whatsapp" style="padding-top:2px; padding-left:3px; padding-right:3px; position: absolute; margin-left: -16px;"></i></a>
						<a href="http://www.facebook.com/share.php?u=@php echo fetchTinyUrl(env('APP_URL').'users/registration?ref='.urlencode(Auth::user()->ref_code)); @endphp" title="Share this page on facebook" class="pop share-square share-square-facebook" style="display: inline-block;"></a>
						<a href="https://twitter.com/share?url=@php echo fetchTinyUrl(env('APP_URL').'users/registration?ref='.urlencode(Auth::user()->ref_code)); @endphp&text=" title="Share this page on twitter" class="pop share-square share-square-twitter" style="display: inline-block;"></a>
						<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=@php echo fetchTinyUrl(env('APP_URL').'users/registration?ref='.urlencode(Auth::user()->ref_code)); @endphp&title=&amp;summary=&amp;source=in1.com" title="Share this page on linkedin" class="pop share-square share-square-linkedin" style="display: inline-block;"></a>
					</div>
					Share Link Via :
				</div>
			</div>
		</div>
		<div class="col-md-12 text-center"><hr></div>
	@if(in_array('14', json_decode(Auth::user()->staff->role->permissions)))
	<div class="panel-body">
		<h2>How It Works? </h2>
		<br>
		<ul class="timeline">
	
			<!-- timeline item -->
			<li>
				<!-- timeline icon -->
				<i class="fa fa-envelope bg-blue"></i>
				<div class="timeline-item">
					<span class="time">Step 1</span>
		
					<h3 class="timeline-header no-border">Invite a friend to {{ $generalsetting->site_name }} !</h3>
		
					<div class="timeline-body">
						Share your referral id with your friends, family and followers.
					</div>
				</div>
			</li>
			<!-- END timeline item -->
			
			
		
			<!-- timeline item -->
			<li>
				<!-- timeline icon -->
				<i class="fa  fa-dollar bg-yellow"></i>
				<div class="timeline-item">
					<span class="time">Step 2</span>
		
					<h3 class="timeline-header no-border">Earn {{$customer_points_points}} Points for each user you invite..</h3>
		
					<div class="timeline-body">
					   You'll earn {{$customer_points_points}} points for every user you refer
					</div>
				</div>
			</li>
			<!-- END timeline item -->
			
		
			<!-- timeline item -->
			<li>
				<!-- timeline icon -->
				<i class="fa fa-line-chart bg-red"></i>
				<div class="timeline-item">
					<span class="time">Step 3</span>
		
					<h3 class="timeline-header no-border">Keep referring new users</h3>
		
					<div class="timeline-body">
						Keep referring your friends to anybizz and watch your earnings grow up !
					</div>
				</div>
			</li>
			<!-- END timeline item -->
		   
		</ul>
		
	</div>
	@endif
</div>

@endsection

@section('script')
    <script type="text/javascript">
		const shareBtn = document.querySelector('.share-button');
		shareBtn.addEventListener('click', () => {
			if (navigator.share) {
				navigator.share({
					title: '',
					text: '',
					url: @php echo fetchTinyUrl(env('APP_URL').'users/registration?ref='.urlencode(Auth::user()->ref_code)); @endphp
				}).then(() => {
					console.log('Thanks for sharing!');
				})
				.catch(err => {
					console.log("Couldn't share because of", err.message);
				});
			} else {
				console.log('web share not supported');
			}
		});
	</script>
@endsection
