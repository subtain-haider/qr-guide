@extends('layouts.app')

@section('content')

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Referrals')}}</h3>
    </div>
	<div class="panel-body">
        <div class="row mb-5">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box hvr-float-shadow">
					<span class="info-box-icon bg-aqua"><i class="fa fa-line-chart"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Total Earned</span>
						<center>
							<h1><span class="info-box-number"><big>@if(\App\Models\Points::where('user_id', Auth::user()->id)->first() != ""){{\App\Models\Points::where('user_id', Auth::user()->id)->first()->total_points_customer+\App\Models\Points::where('user_id', Auth::user()->id)->first()->total_points_seller}}@endif</big></span></h1>
						</center>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box hvr-float-shadow">
					<span class="info-box-icon bg-red"><i class="fa  fa-check-square-o"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Redeemed Points</span>
						<center>
							<h1><span class="info-box-number"><big>@if(\App\Models\Points::where('user_id', Auth::user()->id)->first() != ""){{\App\Models\Points::where('user_id', Auth::user()->id)->first()->redeemed_points_customer+\App\Models\Points::where('user_id', Auth::user()->id)->first()->redeemed_points_seller}}@endif</big></span></h1>
						</center>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			
			<!-- fix for small devices only -->
			<div class="clearfix visible-sm-block"></div>
			
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box hvr-float-shadow">
					<span class="info-box-icon bg-aqua"><i class="fa fa-line-chart"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Current Points</span>
						<center>
							<h1><span class="info-box-number"><big>@if(\App\Models\Points::where('user_id', Auth::user()->id)->first() != ""){{\App\Models\Points::where('user_id', Auth::user()->id)->first()->current_points_customer+\App\Models\Points::where('user_id', Auth::user()->id)->first()->current_points_seller}}@endif</big></span></h1>
						</center>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box hvr-float-shadow">
					<span class="info-box-icon bg-green"><i class="fa fa-dollar"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Amount</span>
						<center>
							@php
							$businesssetting = \App\Models\BusinessSetting::where('type', 'profit_configuration')->first();
			
							$profit_configuration = 0;
							
							if(isset($businesssetting->value) and !empty($businesssetting->value)){
							
								$businesssetting = json_decode($businesssetting->value);
								
								if($businesssetting->status != NULL){
								
									$profit_configuration = $businesssetting->profit;
									
								}
								
							}
							@endphp
							<h1><span class="info-box-number"><big>@if(\App\Models\Points::where('user_id', Auth::user()->id)->first() != ""){{single_price((\App\Models\Points::where('user_id', Auth::user()->id)->first()->balance_customer+\App\Models\Points::where('user_id', Auth::user()->id)->first()->balance_seller)/$profit_configuration*1)}}@endif</big></span></h1>
						</center>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			
		</div>
		<br><br>
		<table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Type')}}</th>
					<th>{{__('Products')}}</th>
					<th>{{__('Points Earned')}}</th>
					<th>{{__('Status')}}</th>
                    <th>{{__('Date')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $key => $user)
                    <tr>
						<td>{{$key+1}}</td>
						<td>{{$user->name}}</td>
						<td>{{$user->user_type}}</td>
						<td>
							@if($user->user_type == 'seller')
								{{ \App\Models\Product::where('user_id', $user->id)->count() }}
							@endif
						</td>
						<td>
							@if($user->user_type == 'seller' and \App\Models\Seller::where('user_id', $user->id)->first() != NULL)
								{{\App\Models\Seller::where('user_id', $user->id)->first()->points}}
							@elseif($user->user_type == 'customer' and \App\Models\Customer::where('user_id', $user->id)->first() != NULL)
								{{\App\Models\Customer::where('user_id', $user->id)->first()->points}}
							@endif
						</td>
						<td>
							@if($user->user_type == 'seller' and \App\Models\Seller::where('user_id', $user->id)->first() != NULL)
								@if (\App\Models\Seller::where('user_id', $user->id)->first()->verification_status == 1)
								<div class="label label-table label-info">
									{{__('Approved')}}
								</div>
								@else
								<div class="label label-table label-warning">
									{{__('In Process')}}
								</div>
								@endif
							@endif
						</td>
						<td>
							@php
								$created_at = strtotime($user->created_at);
								echo date('d/m/Y',$created_at);
							@endphp
						</td>
					</tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

@endsection
