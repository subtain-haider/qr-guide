@extends('layouts.app')

@section('content')

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Customers')}}</h3>
    </div>
	<div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10%">#</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Email')}}</th>
					<th>{{__('Country')}}</th>
					<th>{{__('City')}}</th>
					<th>{{__('Registration Date')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($customers as $key => $customer)
                    <tr>
						@php
							$customer_name = $customer_email = $customer_country = $customer_city = $created_at = ''; 
							$customer_details = \App\Models\User::where('id', $customer->user_id)->first();
							if(isset($customer_details->name)){
								$customer_name = $customer_details->name;
								$customer_email = $customer_details->email;
								$customer_country = $customer_details->country;
								$customer_city = $customer_details->city;
								$created_at = $customer_details->created_at;
						@endphp
								<td>{{$key+1}}</td>
								<td>
									{{$customer_name}}
								</td>
								<td>{{$customer_email}}</td>
								<td>
								@if(\App\Models\Country::where('code', $customer_country)->count() > 0)
									{{\App\Models\Country::where('code', $customer_country)->first()->name}}
								@endif
								</td>
								<td>
								@if(\App\Models\City::where('id', $customer_city)->count() > 0)
									{{\App\Models\City::where('id', $customer_city)->first()->name}}
								@endif
								</td>
								<td>
								@php
									$created_at = strtotime($created_at);
									echo date('d/m/Y',$created_at);
								@endphp
								</td>
						@php
							}
						@endphp
					</tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

@endsection
