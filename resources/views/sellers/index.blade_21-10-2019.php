@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('sellers.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Seller')}}</a>
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('sellers')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    @if(!in_array('14', json_decode(Auth::user()->staff->role->permissions)))
					<th>#</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Email Address')}}</th>
					<th>{{__('Referred by')}}</th>
                    <th>{{__('Status')}}</th>
                    <th>{{ __('Num. of Products') }}</th>
                    <th>{{ __('Due to seller') }}</th>
                    <th width="10%">{{__('Options')}}</th>
					@else
					<th>#</th>
                    <th>{{__('Shop Name')}}</th>
                    <th>{{__('City')}}</th>
                    <th>{{__('Contact No.')}}</th>
                    <th>{{ __('Num. of Products') }}</th>
                    <th>{{ __('Seller') }}</th>
                    <th>{{__('Registration Date')}}</th>
					<th>{{__('Status')}}</th>
					@endif
                </tr>
            </thead>
            <tbody>
                @foreach($sellers as $key => $seller)
                    @foreach(\App\Models\User::where('id', $seller->user_id)->get() as $userkey => $user )
					<tr>
                        <td>{{$key+1}}</td>
                        @if(!in_array('14', json_decode(Auth::user()->staff->role->permissions)))
							<td>{{ $user->name }}</td>
							<td>{{ $user->email }}</td>
							<td>
								@if($user->referral != '' and $user->referral != NULL)
									@if(\App\Models\User::where('ref_code', $user->referral)->where('ref_code', '!=', '')->first() != '')
										{{\App\Models\User::where('ref_code', $user->referral)->first()->name }}
									@endif
								@endif
							</td>
							<td>
								@if ($seller->verification_status == 1)
									<div class="label label-table label-success">
										{{__('Verified')}}
									</div>
								@elseif ($seller->verification_info != null)
									<a href="{{ route('sellers.show_verification_request', $seller->id) }}">
										<div class="label label-table label-info">
											{{__('Requested')}}
										</div>
									</a>
								@else
									<div class="label label-table label-danger">
										{{__('Not Verified')}}
									</div>
								@endif
							</td>
							<td>{{ \App\Models\Product::where('user_id', $seller->user->id)->count() }}</td>
							<td>
								@if ($seller->admin_to_pay > 0)
									{{ single_price($seller->admin_to_pay) }}
								@else
									{{ single_price(0) }}
								@endif
							</td>
							<td>
								<div class="btn-group dropdown">
									<button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
										{{__('Actions')}} <i class="dropdown-caret"></i>
									</button>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="{{route('sellers.seller_details', $seller->id)}}">{{__('View Details')}}</a></li>
										<li><a onclick="show_seller_payment_modal('{{$seller->id}}');">{{__('Pay')}}</a></li>
										<li><a href="{{route('sellers.edit', encrypt($seller->id))}}">{{__('Edit')}}</a></li>
										<li><a onclick="confirm_modal('{{route('sellers.destroy', $seller->id)}}');">{{__('Delete')}}</a></li>
									</ul>
								</div>
							</td>
						@else
							@php
								$seller_name = $seller_email = $seller_country = $seller_city = $created_at = ''; 
								$seller_details = \App\Models\User::where('id', $seller->user_id)->first();
								if(isset($seller_details->name)){
									$seller_name = $seller_details->name;
									$seller_email = $seller_details->email;
									$seller_country = $seller_details->country;
									$seller_city = $seller_details->city;
									$created_at = $seller_details->created_at;
								}else{
									$seller_name = \App\Models\User::where('id', $seller->user_id)->first()->name;
									$seller_email = \App\Models\User::where('id', $seller->user_id)->first()->email;
									$seller_country = \App\Models\User::where('id', $seller->user_id)->first()->country;
									$seller_city = \App\Models\User::where('id', $seller->user_id)->first()->city;
									$created_at = \App\Models\User::where('id', $seller->user_id)->first()->created_at;
								}
							@endphp
							<td><a href="{{route('sellers.seller_details', $seller->id)}}" target="_blank">{{ \App\Models\Shop::where('user_id', $seller->user_id)->first()->name }}</a></td>
							<td>{{ $seller_city }}</td>
							<td>{{ $seller->user->phone }}</td>
							<td>{{ \App\Models\Product::where('user_id', $seller->user_id)->count() }}</td>
							<td>{{ $user->name }}</td>
							<td>{{ $seller->created_at }}</td>
							<td>
								@if ($seller->verification_status == 1)
									<div class="label label-table label-success">
										{{__('Verified')}}
									</div>
								@elseif ($seller->verification_info != null)
									<a href="{{ route('sellers.show_verification_request', $seller->id) }}">
										<div class="label label-table label-info">
											{{__('Requested')}}
										</div>
									</a>
								@else
									<div class="label label-table label-danger">
										{{__('Not Verified')}}
									</div>
								@endif
							</td>
						@endif
                    </tr>
					@endforeach
                @endforeach
            </tbody>
        </table>

    </div>
</div>


<div class="modal fade" id="payment_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="modal-content">

        </div>
    </div>
</div>


@endsection

@section('script')
    <script type="text/javascript">
        function show_seller_payment_modal(id){
            $.post('{{ route('sellers.payment_modal') }}',{_token:'{{ @csrf_token() }}', id:id}, function(data){
                $('#modal-content').html(data);
                $('#payment_modal').modal('show', {backdrop: 'static'});
                $('.demo-select2-placeholder').select2();
            });
        }
    </script>
@endsection
