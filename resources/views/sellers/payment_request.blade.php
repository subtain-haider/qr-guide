@extends('layouts.app')

@section('content')

@php
	$generalsetting = \App\Models\GeneralSetting::first();
	$businesssetting = \App\Models\BusinessSetting::where('type', 'profit_configuration')->first();
	$profit_configuration = 0;
	if(isset($businesssetting->value) and !empty($businesssetting->value)){
		$businesssetting = json_decode($businesssetting->value);
		if($businesssetting->status != NULL){
			$profit_configuration = $businesssetting->profit;
		}
	}
@endphp
<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="row">
		<div class="col-md-12">
			<div class="col-md-5">
				<div class="panel-body text-center">
					<span class="store-icon-block pull-left" style="width:auto;">
						<img src="{{ asset('img/black-store-icon.png') }}" alt="" class="img-fluid">
					</span>
					<span class="pull-left">
						<h3 class="panel-title">1 Shop = {{$seller_points}} Points</h3>
						<h3>@if(\App\Models\Points::where('user_id', Auth::user()->id)->first() != ""){{\App\Models\Points::where('user_id', Auth::user()->id)->first()->current_points_seller}}@endif Points</h3>
					</span>
				</div>
			</div>
			
			<div class="col-md-4 text-center">
				@php
					if(\App\Models\Points::where('user_id', Auth::user()->id)->first() != ""){
						$total_points_seller = \App\Models\Points::where('user_id', Auth::user()->id)->first()->total_points_seller;
						$redeemed_points_seller = \App\Models\Points::where('user_id', Auth::user()->id)->first()->redeemed_points_seller;
						//echo '.('.$total_points_seller.'-'.$redeemed_points_seller.')/'.$profit_configuration;
						$total_earnings = ($total_points_seller-$redeemed_points_seller)/$profit_configuration;
					}else{
						$total_earnings = 0;
					}
				@endphp
				<div class="panel-body">
					<h3 class="panel-title">{{__('Earnings')}}</h3>
					<h3>@php echo single_price($total_earnings); @endphp</h3>
				</div>
			</div>
			
			<div class="col-md-3 text-center">
				<div class="col-md-12 mt-5"><a href="{{ route('advertiser.seller_payment_request') }}" class="btn btn-warning">{{__('Redeem')}}</a></div>
			</div>
		</div>
		<div class="col-md-12 text-center"><hr></div>
		<div class="col-md-12">
			<div class="col-md-5">
				<div class="panel-body text-center">
					<span class="store-icon-block pull-left" style="width:auto;">
						<img src="{{ asset('img/black-user-icon.png') }}" alt="" class="img-fluid">
					</span>
					<span class="pull-left">
						<h3 class="panel-title">1 User = {{$customer_points}} Points</h3>
						<h3>@if(\App\Models\Points::where('user_id', Auth::user()->id)->first() != ""){{\App\Models\Points::where('user_id', Auth::user()->id)->first()->current_points_customer}}@endif Points</h3>
					</span>
				</div>
			</div>
			
			<div class="col-md-4 text-center">
				@php
					if(\App\Models\Points::where('user_id', Auth::user()->id)->first() != ""){
						$total_points_customer = \App\Models\Points::where('user_id', Auth::user()->id)->first()->total_points_customer;
						$redeemed_points_customer = \App\Models\Points::where('user_id', Auth::user()->id)->first()->redeemed_points_customer;
						$total_earnings = ($total_points_customer-$redeemed_points_customer)/$profit_configuration;
					}else{
						$total_earnings = 0;
					}
				@endphp
				<div class="panel-body">
					<h3 class="panel-title">{{__('Earnings')}}</h3>
					<h3>@php echo single_price($total_earnings); @endphp</h3>
				</div>
			</div>
			
			<div class="col-md-3 text-center">
				<div class="col-md-12 mt-5"><a href="{{ route('advertiser.customer_payment_request') }}" class="btn btn-warning">{{__('Redeem')}}</a></div>
			</div>
		</div>
	</div>
	<div class="col-md-12 text-center"><hr></div>
	<div class="panel-heading mb-5">
        <h3 class="panel-title">{{__('Pending Requests')}}</h3>
    </div>
	
	<div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10%">#</th>
                    <th>{{__('Points Used')}}</th>
                    <th>{{__('Amount')}}</th>
                    <th>{{__('Type')}}</th>
                    <th>{{__('Date')}}</th>
					<th>{{__('Status')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($reward_payments as $key => $payments)
				<tr>
					<td>{{$key+1}}</td>
                    <td>{{$payments->points}}</td>
                    <td>{{single_price($payments->amount)}}</td>
					<td>{{$payments->type}}</td>
                    <td>@php $updated_at = strtotime($payments->updated_at); echo date('d/m/Y', $updated_at); @endphp</td>
                    <td>
						@if ($payments->status == 1)
							<div class="label label-table label-success">
								{{__('Paid')}}
							</div>
						@elseif ($payments->status != null)
							<div class="label label-table label-info">
								{{__('Unpaid')}}
							</div>
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
        </table>

    </div>
</div>

@endsection
