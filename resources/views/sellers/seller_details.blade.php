@extends('layouts.app')

@section('content')

    <div class="panel">
    	<div class="panel-body">
    		<div class="invoice-masthead">
    			<div class="invoice-text pull-left">
    				<h3 class="h1 text-thin mar-no text-primary">{{ __('Seller Details') }}</h3>
    			</div>
    		</div>
			<div class="row">
				<div class="col-md-3">
					<label>{{__('Seller Name:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">{{ $seller->user->name }}</strong>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<label>{{__('Seller Email:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">{{ $seller->user->email }}</strong>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<label>{{__('Gender:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">{{ $seller->user->gender }}</strong>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<label>{{__('CNIC:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">{{ $seller->user->cnic }}</strong>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<label>{{__('Address:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">{{ $seller->user->address }}</strong>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<label>{{__('Phone:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">{{ $seller->user->phone }}</strong>
				</div>
			</div>
			
			<div class="invoice-masthead">
    			<div class="invoice-text pull-left">
    				<h3 class="h1 text-thin mar-no text-primary">{{ __('Seller Shop Details') }}</h3>
    			</div>
    		</div>
			<div class="row">
				<div class="col-md-3">
					<label>{{__('Shop Name:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">@php echo \App\Models\Shop::where('user_id', $seller->user_id)->first()->name; @endphp</strong>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<label>{{__('Seller Address:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">@php echo \App\Models\Shop::where('user_id', $seller->user_id)->first()->address; @endphp</strong>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<label>{{__('Slug:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">@php echo \App\Models\Shop::where('user_id', $seller->user_id)->first()->slug; @endphp</strong>
				</div>
			</div>
			
			<div class="invoice-masthead">
    			<div class="invoice-text pull-left">
    				<h3 class="h1 text-thin mar-no text-primary">{{ __('Bank Details') }}</h3>
    			</div>
    		</div>
			<div class="row">
				<div class="col-md-3">
					<label>{{__('Bank Name:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">{{ $seller->bank_name }}</strong>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<label>{{__('Account No:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">{{ $seller->account_no }}</strong>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<label>{{__('Bank Code:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">{{ $seller->bank_code }}</strong>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<label>{{__('Bank Branch:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">{{ $seller->bank_branch }}</strong>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<label>{{__('Bank Address:')}}</label>
				</div>
				<div class="col-md-9">
					<strong class="text-main">{{ $seller->bank_address }}</strong>
				</div>
			</div>
		</div>
    </div>
@endsection
