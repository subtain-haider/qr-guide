<!--MAIN NAVIGATION-->
<!--===================================================-->
<nav id="mainnav-container">
    <div id="mainnav">

        <!--Menu-->
        <!--================================-->
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content">

                    <!--Profile Widget-->
                    <!--================================-->
                    {{-- <div id="mainnav-profile" class="mainnav-profile">
                        <div class="profile-wrap text-center">
                            <div class="pad-btm">
                                <img class="img-circle img-md" src="{{ asset('img/profile-photos/1.png') }}" alt="Profile Picture">
                            </div>
                            <a href="#profile-nav" class="box-block" data-toggle="collapse" aria-expanded="false">
                                <span class="pull-right dropdown-toggle">
                                    <i class="dropdown-caret"></i>
                                </span>
                                <p class="mnp-name">{{Auth::user()->name}}</p>
                                <span class="mnp-desc">{{Auth::user()->email}}</span>
                            </a>
                        </div>
                        <div id="profile-nav" class="collapse list-group bg-trans">
                            <a href="#" class="list-group-item">
                                <i class="demo-pli-male icon-lg icon-fw"></i> View Profile
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="demo-pli-gear icon-lg icon-fw"></i> Settings
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="demo-pli-information icon-lg icon-fw"></i> Help
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="demo-pli-unlock icon-lg icon-fw"></i> Logout
                            </a>
                        </div>
                    </div> --}}


                    <!--Shortcut buttons-->
                    <!--================================-->
                    <div id="mainnav-shortcut" class="hidden">
                        <ul class="list-unstyled shortcut-wrap">
                            <li class="col-xs-3" data-content="My Profile">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                    <i class="demo-pli-male"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Messages">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                    <i class="demo-pli-speech-bubble-3"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Activity">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                    <i class="demo-pli-thunder"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Lock Screen">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                    <i class="demo-pli-lock-2"></i>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!--================================-->
                    <!--End shortcut buttons-->


                    <ul id="mainnav-menu" class="list-group">

                        <!--Category name-->
                        {{-- <li class="list-header">Navigation</li> --}}

                        <!--Menu list item-->
                        {{-- <li class="{{ areActiveRoutes(['admin.dashboard'])}}"> --}}
                        <li class="">
                            <a class="nav-link" href="{{route('admin.dashboard')}}">
                                <i class="fa fa-home"></i>
                                <span class="menu-title">{{__('Dashboard')}}</span>
                            </a>
                        </li>

                        <!-- Product Menu -->
                        @if(Auth::user()->user_type == 'admin' || in_array('1', json_decode(Auth::user()->staff->role->permissions)))
                            <li>
                                <a href="#">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span class="menu-title">{{__('Products')}}</span>
                                    <i class="arrow"></i>
                                </a>

                                <!--Submenu-->
                                <ul class="collapse">
                                    @if(!in_array('14', json_decode(Auth::user()->staff->role->permissions)))
									{{-- <li class="{{ areActiveRoutes(['brands.index', 'brands.create', 'brands.edit'])}}"> --}}
									<li class="">
                                        <a class="nav-link" href="{{route('brands.index')}}">{{__('Brand')}}</a>
                                    </li>
                                    {{-- <li class="{{ areActiveRoutes(['categories.index', 'categories.create', 'categories.edit'])}}"> --}}
                                    <li class="">
                                        <a class="nav-link" href="{{route('categories.index')}}">{{__('Category')}}</a>
                                    </li>
                                    {{-- <li class="{{ areActiveRoutes(['subcategories.index', 'subcategories.create', 'subcategories.edit'])}}"> --}}
                                    <li class="">
                                        <a class="nav-link" href="{{route('subcategories.index')}}">{{__('Subcategory')}}</a>
                                    </li>
                                    {{-- <li class="{{ areActiveRoutes(['subsubcategories.index', 'subsubcategories.create', 'subsubcategories.edit'])}}"> --}}
                                    <li class="">
                                        <a class="nav-link" href="{{route('subsubcategories.index')}}">{{__('Sub Subcategory')}}</a>
                                    </li>
                                    {{-- <li class="{{ areActiveRoutes(['products.admin', 'products.create', 'products.admin.edit'])}}"> --}}
                                    <li class="">
                                        <a class="nav-link" href="{{route('products.admin')}}">{{__('In House Products')}}</a>
                                    </li>
                                    @endif
									@if(\App\Models\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
                                        {{-- <li class="{{ areActiveRoutes(['products.seller', 'products.seller.edit'])}}"> --}}
                                        <li class="">
                                        <li class="">
                                            <a class="nav-link" href="{{route('products.seller')}}">{{__('Seller Products')}}</a>
                                        </li>
                                    @endif
									{{-- @if(!in_array('14', json_decode(Auth::user()->staff->role->permissions)))
                                    <li class="{{ areActiveRoutes(['reviews.index'])}}">
                                        <a class="nav-link" href="{{route('reviews.index')}}">{{__('Product Reviews')}}</a>
                                    </li>
									@endif --}}
                                </ul>
                            </li>
                        @endif

                        @if(Auth::user()->user_type == 'admin' || in_array('3', json_decode(Auth::user()->staff->role->permissions)))
                            @php
                                $orders = DB::table('orders')
                                            ->orderBy('code', 'desc')
                                            ->join('order_details', 'orders.id', '=', 'order_details.order_id')
                                            ->where('order_details.seller_id', Auth::user()->id)
                                            ->where('orders.viewed', 0)
                                            ->select('orders.id')
                                            ->distinct()
                                            ->count();
                            @endphp
                        {{-- <li class="{{ areActiveRoutes(['orders.index.admin', 'orders.show'])}}"> --}}
                        <li class="">
                            <a class="nav-link" href="{{ route('orders.index.admin') }}">
                                <i class="fa fa-shopping-basket"></i>
                                <span class="menu-title">{{__('Inhouse orders')}} @if($orders > 0)<span class="pull-right badge badge-info">{{ $orders }}</span>@endif</span>
                            </a>
                        </li>
                        <li class="">
                            <a class="nav-link" href="{{ route('inbox') }}">
                                <i class="fa fa-shopping-basket"></i>
                                <span class="menu-title">Support Messages</span>
                            </a>
                        </li>
                        @endif

                        @if(Auth::user()->user_type == 'admin' || in_array('4', json_decode(Auth::user()->staff->role->permissions)))
                        {{-- <li class="{{ areActiveRoutes(['sales.index', 'sales.show'])}}"> --}}
                        {{-- <li class="">--}}
                        <li class="">
                            <a class="nav-link" href="{{ route('sales.index') }}">
                                <i class="fa fa-money"></i>
                                <span class="menu-title">{{__('Total sales')}}</span>
                            </a>
                        </li>
						{{-- <li class="{{ areActiveRoutes(['sales.seller_sales'])}}"> --}}
						{{-- <li class="">--}}
                            <!--<li class="">-->
                            <!--<a class="nav-link" href="{{ route('sales.seller_sales') }}">-->
                            <!--    <i class="fa fa-money"></i>-->
                            <!--    <span class="menu-title">{{__('Sellers sales')}}</span>-->
                            <!--</a>-->
                            <!--</li>-->
                        {{--</li>--}}
                        @endif

                        @if(in_array('14', json_decode(Auth::user()->staff->role->permissions)))
						{{-- <li class="{{ areActiveRoutes(['advertiser_report.index'])}}"> --}}
						<li class="">
                            <li class="">
                            @php
								$sellers = \App\Models\Seller::where('verification_status', 0)->where('verification_info', '!=', null)->count();
                            @endphp
                            <a class="nav-link" href="{{route('sellers.index')}}">
                                <i class="fa fa-user-plus"></i>
                                <span class="menu-title">{{__('Seller List')}}</span>
                            </a>
                        </li>
						@elseif((Auth::user()->user_type == 'admin' || in_array('5', json_decode(Auth::user()->staff->role->permissions))) && \App\Models\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
                        <li>
                            <a href="#">
                                <i class="fa fa-user-plus"></i>
                                <span class="menu-title">{{__('Sellers')}}</span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                {{-- <li class="{{ areActiveRoutes(['sellers.index', 'sellers.create', 'sellers.edit'])}}"> --}}
                                <li class="">
                                <li class="">
                                    @php
                                        $sellers = \App\Models\Seller::where('verification_status', 0)->where('verification_info', '!=', null)->count();
                                    @endphp
                                    <a class="nav-link" href="{{route('sellers.index')}}">{{__('Seller list')}} @if($sellers > 0)<span class="pull-right badge badge-info">{{ $sellers }}</span> @endif</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['business_settings.vendor_commission'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{ route('business_settings.vendor_commission') }}">{{__('Seller Commission')}}</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['warranties', 'show_warranty_request'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{ route('warranties') }}">{{__('Warranties')}}</a>
                                </li>
                            </ul>
                        </li>
                        @endif

                        @if((Auth::user()->user_type == 'admin' || in_array('6', json_decode(Auth::user()->staff->role->permissions))) and !in_array('14', json_decode(Auth::user()->staff->role->permissions)))
                        <li>
                            <a href="#">
                                <i class="fa fa-user-plus"></i>
                                <span class="menu-title">{{__('Customers')}}</span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                {{-- <li class="{{ areActiveRoutes(['customers.index'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{ route('customers.index') }}">{{__('Customer list')}}</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['customers.custom', 'have-orders'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{ route('customers.custom', 'have-orders') }}">{{__('Customer have orders')}}</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['customers.custom', 'no-orders'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{ route('customers.custom', 'no-orders') }}">{{__('Customer have no orders')}}</a>
                                </li>
                            </ul>
                        </li>
                        @endif

                        @if(Auth::user()->user_type == 'admin' || !in_array('14', json_decode(Auth::user()->staff->role->permissions)))
						<li>
                            <a href="#">
                                <i class="fa fa-file"></i>
                                <span class="menu-title">{{__('Reports')}}</span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                {{-- <li class="{{ areActiveRoutes(['stock_report.index'])}}"> --}}
                                {{--<li class="">
                                    <a class="nav-link" href="{{ route('stock_report.index') }}">{{__('Stock Report')}}</a>
                                </li>--}}
                                {{-- <li class="{{ areActiveRoutes(['in_house_sale_report.index'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{ route('in_house_sale_report.index') }}">{{__('In House Sale Report')}}</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['seller_report.index'])}}"> --}}
                                {{-- <li class="{{ areActiveRoutes(['seller_report.index'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{ route('seller_report.index') }}">{{__('Seller Report')}}</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['seller_sale_report.index'])}}"> --}}
                               {{-- <li class="">
                                    <a class="nav-link" href="{{ route('seller_sale_report.index') }}">{{__('Seller Based Selling Report')}}</a>
                                </li> --}}
                            </ul>
                        </li>
						@endif

                        {{-- <li class="{{ areActiveRoutes(['pricing.index', 'pricing.create', 'pricing.edit'])}}"> --}}
                        <li class="">
                            <a class="nav-link" href="{{route('pricing.index')}}">
                                <i class="fa fa-money"></i>
                                <span class="menu-title">{{__('Pricing')}}</span>
                            </a>
                        </li>
                         <li class="">
                            <a class="nav-link" href="{{route('faq.index')}}">
                                <i class="fa fa-question-circle"></i>
                                <span class="menu-title">{{__('FAQs')}}</span>
                            </a>
                        </li>
                        <li class="">
                            <a class="nav-link" href="{{route('aboutus.index')}}">
                                <i class="fa fa-info-circle"></i>
                                <span class="menu-title">{{__('AboutUs')}}</span>
                            </a>
                        </li>
                        <li class="">
                            <a class="nav-link" href="{{route('guiderequest.index')}}">
                                <i class="fa fa-info-circle"></i>
                                <span class="menu-title">{{__('Guide request')}}</span>
                            </a>
                        </li>
                        <li class="">
                            <a class="nav-link" href="{{route('requestus.index')}}">
                                <i class="fa fa-info-circle"></i>
                                <span class="menu-title">{{__('Video request')}}</span>
                            </a>
                        </li>
                        <li class="">
                            <a class="nav-link" href="{{route('homediv.index')}}">
                                <i class="fa fa-home"></i>
                                <span class="menu-title">{{__('Home section')}}</span>
                            </a>
                        </li>

                        @if(Auth::user()->user_type == 'admin' || in_array('8', json_decode(Auth::user()->staff->role->permissions)))
                        <li>
                            <a href="#">
                                <i class="fa fa-briefcase"></i>
                                <span class="menu-title">{{__('Business Settings')}}</span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                {{-- <li class="{{ areActiveRoutes(['activation.index'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{route('activation.index')}}">{{__('Activation')}}</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['payment_method.index'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{ route('payment_method.index') }}">{{__('Payment method')}}</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['smtp_settings.index'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{ route('smtp_settings.index') }}">{{__('SMTP Settings')}}</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['shipping_method.index'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{ route('shipping_method.index') }}">{{__('Shipping method')}}</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['google_analytics.index'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{ route('google_analytics.index') }}">{{__('Google Analytics')}}</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['facebook_chat.index'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{ route('facebook_chat.index') }}">{{__('Facebook Chat')}}</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['social_login.index'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{ route('social_login.index') }}">{{__('Social Media Login')}}</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['currency.index'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{route('currency.index')}}">{{__('Currency')}}</a>
                                </li>
                                {{-- <li class="{{ areActiveRoutes(['languages.index', 'languages.create', 'languages.store', 'languages.show', 'languages.edit'])}}"> --}}
                                <li class="">
                                    <a class="nav-link" href="{{route('languages.index')}}">{{__('Languages')}}</a>
                                </li>
                            </ul>
                        </li>
                        @endif

                        @if(Auth::user()->user_type == 'admin' || in_array('12', json_decode(Auth::user()->staff->role->permissions)))
                        <li>
                            <a href="#">
                                <i class="fa fa-desktop"></i>
                                <span class="menu-title">{{__('E-commerce Setup')}}</span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                <li>
                                    {{-- <li class="{{ areActiveRoutes(['coupon.index','coupon.create','coupon.edit',])}}"> --}}
                                    <li class="">
                                        <a class="nav-link" href="{{route('coupon.index')}}">{{__('Coupon')}}</a>
                                    </li>
                                </li>
                            </ul>
                        </li>
                        @endif

                        @if(Auth::user()->user_type == 'admin' || in_array('13', json_decode(Auth::user()->staff->role->permissions)))
                            @php
                                $support_ticket = DB::table('tickets')
                                            ->where('viewed', 0)
                                            ->select('id')
                                            ->count();
                            @endphp
                        {{-- <li class="{{ areActiveRoutes(['support_ticket.admin_index'])}}"> --}}
                        <li class="">
                            <a class="nav-link" href="{{ route('support_ticket.admin_index') }}">
                                <i class="fa fa-support"></i>
                                <span class="menu-title">{{__('Suppot Ticket')}} @if($support_ticket > 0)<span class="pull-right badge badge-info">{{ $support_ticket }}</span>@endif</span>
                            </a>
                        </li>
                        @endif
                        {{-- <li class="{{ areActiveRoutes(['logActivity'])}}"> --}}
                        <li class="">
                            <a class="nav-link" href="{{route('logActivity')}}">
                                <i class="fa fa-file"></i>
                                <span class="menu-title">{{__('Log Activity')}}</span>
                            </a>
                        </li>

                        {{-- @if(Auth::user()->user_type == 'admin' || in_array('14', json_decode(Auth::user()->staff->role->permissions)))
                            <li class="{{ areActiveRoutes(['advertiser_report.customers'])}}">
                                <a class="nav-link" href="{{route('advertiser_report.customers')}}">
                                    <i class="fa fa-file"></i>
                                    <span class="menu-title">{{__('Customers')}}</span>
                                </a>
                            </li>
						@endif --}}

						@if(in_array('14', json_decode(Auth::user()->staff->role->permissions)))
                            {{-- <li class="{{ areActiveRoutes(['marketing.advertiser_details_view'])}}"> --}}
                            <li class="">
                                <a class="nav-link" href="{{route('marketing.advertiser_details_view')}}">
                                    <i class="demo-pli-male icon-lg icon-fw"></i>
                                    <span class="menu-title">{{__('Profile')}}</span>
                                </a>
                            </li>
						@endif


                    </ul>
                </div>
            </div>
        </div>
        <!--================================-->
        <!--End menu-->

    </div>
</nav>
