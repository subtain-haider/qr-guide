@extends(auth()->user()->is_seller ? 'chatlayouts/app3' : 'chatlayouts/app')


@section('content')

                    <div class="container">
 
          
            @livewire('show2', ['users' => $users, 'messages' => $messages, 'sender' => $sender])
  
    
    </div>
    
@endsection
