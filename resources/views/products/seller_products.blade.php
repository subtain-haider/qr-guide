@extends('layouts.app')

@section('content')

@if($type != 'Seller')
    <div class="row">
        <div class="col-lg-12">
            <a href="{{ route('products.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Product')}}</a>
        </div>
    </div>
@endif

<br>

<div class="col-lg-12">
    <div class="panel">
        <!--Panel heading-->
        <div class="panel-heading">
            <h3 class="panel-title">{{ __($type.' Products') }}</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="20%">{{__('Name')}}</th>
                        <th>{{__('Photo')}}</th>
                        <th>{{__('Current qty')}}</th>
                        <th>{{__('Seller')}}</th>												<th>{{__('Date')}}</th>
                    </tr>
                </thead>
                <tbody>										@php 						$count = 0; 					@endphp					
                    @foreach($sellers as $seller)											@foreach (\App\Models\Product::where('added_by', 'seller')->where('user_id', $seller->user_id)->orderBy('created_at', 'desc')->get() as $key => $product)
							<tr>
								<td>@php echo $count+1; @endphp</td>
								<td><a href="{{ route('product', $product->slug) }}" target="_blank">{{ __($product->name) }}</a></td>
								<td><img class="img-md" src="{{ asset($product->thumbnail_img)}}" alt="Image"></td>
								<td>
									@php
										$qty = 0;
										foreach (json_decode($product->variations) as $key => $variation) {
											$qty += $variation->qty;
										}
										echo $qty;
									@endphp
								</td>
								<td>{{	$seller->user->name }}</td>																<td>{{	$seller->created_at }}</td>
							</tr>
							@php 								$count++; 							@endphp													@endforeach
					@endforeach								</tbody>
            </table>

        </div>
    </div>
</div>

@endsection


@section('script')
    <script type="text/javascript">

        $(document).ready(function(){
            //$('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
        });

        function update_todays_deal(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('products.todays_deal') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Todays Deal updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }

        function update_published(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('products.published') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Published products updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }

        function update_featured(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('products.featured') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Featured products updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }
    </script>
@endsection
