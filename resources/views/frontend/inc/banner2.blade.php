<!DOCTYPE html>
<html>
<head>
	<title>banner2</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<link rel="stylesheet" type="text/css" href="assets/css/style.css"> 	
</head>
<body>
<div class="bn2-flex-1">
	<div class="bn2-page">
		<div class="bn2-main">
			<img src="{{asset('frontend/images/bn2-inner.png')}}">
			 @if(!empty($datas))
			 <a href="#">{{$datas[0]}}</a>
            @elseif(!empty($data))
            <a href="#">{{$data['coupon_text'][0]}}</a>
            @elseif(!empty($coupon))
            <a href="#">{{ $coupon->code }}</a>
            @endif
			
		</div>
	</div>	
</div>
</body>
</html>
<style type="text/css">
	*{
	margin: 0px;
	padding: 0px;
}
.bn2-flex-1 {
	display: flex;
	justify-content: center;
	align-items: center;
	background-color: black;
	height: auto;
	/*overflow-y: scroll;*/
}
.bn2-page {
	width: 100%;
    border: 20px solid white;
    height: 390px;
}
.bn2-main {
	width: 100%;
	position: relative;
	background:url(/public/frontend/images/bn2-main.png);
	background-position: center center;
	background-size: cover;
	background-repeat: no-repeat;
	height: 100%;
}
.bn2-main img {
	position: absolute;
    top: 60px;
    left: 150px;
    width: 47%;
}
.bn2-main a {
	position: absolute;
	top: 288px;
    left: 220px;
	text-decoration: none;
	color: #fff;
	font-weight: 500;
	font-size: 17px;
	font-family: sans-serif;
}
.bn2-main a:hover {
	color:#777;
}	
@media screen and (max-width: 1000px) {
	.bn2-page {
		width: 660px;
		height: 500px;
		border:17px solid white;
	}
	.bn2-main img {
    	top: 69px;
    	left: 173px;
    	width: 320px;
    }	
    .bn2-main a {
	    top: 351px;
	    left: 283px;
	    font-size: 19px;
	}
}
@media screen and (max-width: 685px) {
	.bn2-page {
		width: 480px;
		height: 400px;
		border:13px solid white;
	}
	.bn2-main img {
	    top: 66px;
	    left: 112px;
	    width: 250px;
    }	
    .bn2-main a {
	    top: 286px;
    	left: 195px;
    	font-size: 16px;
	}
}
@media screen and (max-width: 505px) {
	.bn2-page {
		width: 300px;
		height: 280px;
		border:7px solid white;
	}
	.bn2-main img {
        top: 50px;
    	left: 66px;
   		width: 170px;
    }	
    .bn2-main a {
        top: 197px;
    	left: 117px;
    	font-size: 13px;
	}
}
</style>