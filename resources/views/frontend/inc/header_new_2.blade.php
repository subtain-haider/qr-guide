<!-- top bar  -->
<section class="top-bar">
	<div class="flex-1">
		<div class="page-2">
			<div class="flex-2 align-items wrap">
				<div class="tb-width-1">
					@php
                        $generalsetting = \App\Models\GeneralSetting::first();
                    @endphp
                    @if($generalsetting->logo != null)
                        <img src="{{ asset($generalsetting->logo) }}" alt="@yield('meta_title', config('app.name', 'Laravel'))">
                    @else
                        <h2 class="mb-0">@yield('meta_title', config('app.name', 'Laravel'))</h2>
                    @endif
				</div>
				<div class="tb-width-2">
					<nav>
						<ul>
							<li><a href="#">HOW TO USE</a></li>
							<li><a href="#">TOOLS & API</a></li>
							<li><a href="#">FAQ</a></li>
						</ul>
					</nav>
				</div>
				<div class="tb-width-3 flex-1">
					<a href="#">ADD A GUIDE</a>
					<i class="fa fa-bell"></i><i class="fa fa-user-o"></i>
				</div>
			</div>
		</div>	
	</div>	
</section>