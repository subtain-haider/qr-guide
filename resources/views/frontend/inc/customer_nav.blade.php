<header class="dashboard-2-users-res-menu" id="dashboardusertwo">
    <div class="dashboard-2-users-res-menu-page">
        <div class="dashboard-2-users-res-menu-cros">
            <i class="fa fa-times" onclick="hidedashusertwo()"></i>
        </div>
        <div class="dashboard-2-users-res-menu-content">

            <div class="dashboard-2-users-header-srch-res">
                <input type="text" name="" placeholder="Search for...">
                <i class="fa fa-search"></i>
            </div>

            <div class="dashboard-2-users-header-profle-dat">
                <div class="dashboard-2-users-header-profle-icons-2">
                    <i class="fa fa-bell-o"></i>
                    <i class="fa fa-circle"></i>
                </div>

                <div class="dashboard-2-users-header-profle-iconsen-2">
                    <i class="fa fa-envelope-o"></i>
                    <i class="fa fa-circle"></i>
                </div>

                <div class="dashboard-2-users-header-profle-iconss-2">
                    <i class="fa fa-shopping-cart"></i>
                </div>           
            </div>

            <div class="dashboard-2-users-header-detail-profile-logout-cst">
                <a href="{{ route('logout') }}" style="background-color: #D0EAF0;color: #000;font-weight: bold;border: 1px solid #D0EAF0;padding: 6px 42px;">LOGOUT</a>
            </div>
            
            
    
        </div>
    </div>
</header>