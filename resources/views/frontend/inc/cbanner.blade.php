<section class="popup-bg-3">
    <div class="popup-page-3">
        <img src="{{asset('frontend/images/cbanner1.png')}}">
        <div class="popup-inners-5">
           @if(isset($datas))
            <p>{{$datas[0]}}</p>
            @else
            <p>{{$data['coupon_text'][0]}}</p>
            @endif
        </div>
        <div class="popup-inners-6">
            @if(isset($datas))
            <h5>{{$datas[1]}}</h5>
            @else
            <h5>{{$data['coupon_text'][1]}}</h5>
            @endif
        </div>
        <div class="popup-inners-7">
            @if(isset($datas))
            <h5>{{$datas[2]}}</h5>
            @else
            <h5>{{$data['coupon_text'][2]}}</h5>
            @endif
        </div>
        <div class="popup-inners-8">
            @if(isset($datas))
            <p>{{$datas[3]}}</p>
            @else
            <p>{{$data['coupon_text'][3]}}</p>
            @endif
        </div>
        <div class="popup-inners-9">
            @if(isset($datas))
            <p>MYQRCODE{{$datas[4]}}</p>
            @else
            <p>MYQRCODE{{$data['coupon_text'][4]}}</p>
            @endif
            
        </div>
    </div>
</section>
<!-- css -->
<style>
.popup-bg-3 {
display: flex;
justify-content: center;
align-items: center;
}

.popup-page-3 {
width: 650px;
height: 500px;
display: flex;
align-items: center;
position: relative;
overflow: hidden;
}

.popup-page-3 img {
height: 100%;
width: 100%;
}

.popup-inners-5 {
position: absolute;
top: 150px;
left: 27px;
/* transform: rotate(-10.5deg); */
}

.popup-inners-5 p {
letter-spacing: 2px;
color: #122852;
padding: 5px 25px;
font-weight: 700;
font-size: 20px;
transform: skewX(-10deg);
margin-bottom: 0px;
}

.popup-inners-6 {
/* width: 100%; */
text-align: center;
position: absolute;
top: 160px;
overflow: hidden;
left: 45px;
/* transform: rotate(-10.5deg); */
}

.popup-inners-6 h5 {
color: #E30000;
font-weight: 700;
font-size: 87px;
}

.popup-inners-7 {
text-align: center;
position: absolute;
top: 160px;
overflow: hidden;
/* left: 88px; */
left: 375px;
/* transform: rotate(-10.5deg); */
}

.popup-inners-7 h5 {
color: #122852;
font-weight: 700;
font-size: 87px;
padding: 5px 30px;
/* transform: skewX(-20deg); */
margin-bottom: 0px;
}

.popup-inners-8 {
position: absolute;
top: 283px;
left: 324px;
padding: 7px 15px 0px 15px;
border-radius: 20px;
}

.popup-inners-8 p {
color: white;
font-weight: 500;
font-size: 18px;
margin-bottom: 0px;
}

.popup-inners-9 {
position: absolute;
top: 310px;
left: 304px;
padding: 0px 15px;
border-radius: 20px;
}

.popup-inners-9 p {
color: white;
font-weight: 400;
font-size: 16px;
}

/*media query*/

@media screen and (max-width: 650px) {
.popup-page-3 {
width: 550px;
height: 480px;
}
.popup-inners-5 {
left: 20px;
}
.popup-inners-6 {
left: 37px;
}
.popup-inners-6 h5 {
font-size: 76px;
}
.popup-inners-7 {
top: 153px;
left: 311px;
}
.popup-inners-7 h5 {
font-size: 76px;
}
.popup-inners-8 {
top: 270px;
left: 263px;
}
.popup-inners-9 {
top: 303px;
left: 250px;
}
.popup-inners-9 p {
font-size: 14px;
}
}

@media screen and (max-width: 550px) {
.popup-page-3 {
width: 480px;
height: 450px;
}
.popup-inners-5 {
left: 1px;
top: 147px;
}
.popup-inners-6 {
left: 21px;
}
.popup-inners-6 h5 {
font-size: 68px;
}
.popup-inners-7 {
top: 153px;
left: 269px;
}
.popup-inners-7 h5 {
font-size: 68px;
}
.popup-inners-8 {
top: 256px;
left: 230px;
}
.popup-inners-8 p {
font-size: 15px;
}
.popup-inners-9 {
top: 287px;
left: 215px;
}
.popup-inners-9 p {
font-size: 12px;
}
}

@media screen and (max-width: 480px) {
.popup-page-3 {
width: 380px;
height: 360px;
}
.popup-inners-5 {
left: -8px;
top: 120px;
}
.popup-inners-5 p {
font-size: 15px;
letter-spacing: 1px;
}
.popup-inners-6 {
left: 10px;
top: 130px;
}
.popup-inners-6 h5 {
font-size: 55px;
}
.popup-inners-7 {
top: 130px;
left: 200px;
}
.popup-inners-7 h5 {
font-size: 55px;
}
.popup-inners-8 {
top: 203px;
left: 181px;
}
.popup-inners-8 p {
font-size: 11px;
}
.popup-inners-9 {
top: 228px;
left: 172px;
}
.popup-inners-9 p {
font-size: 9px;
}
}

@media screen and (max-width: 380px) {
.popup-page-3 {
width: 320px;
height: 300px;
}
.popup-inners-5 {
left: -8px;
top: 101px;
}
.popup-inners-5 p {
font-size: 12px;
}
.popup-inners-6 {
left: 10px;
top: 111px;
}
.popup-inners-6 h5 {
font-size: 46px;
}
.popup-inners-7 {
top: 108px;
left: 160px;
}
.popup-inners-7 h5 {
font-size: 46px;
}
.popup-inners-8 {
top: 169px;
left: 153px;
}
.popup-inners-8 p {
font-size: 9px;
}
.popup-inners-9 {
top: 191px;
left: 144px;
}
.popup-inners-9 p {
font-size: 7px;
}
}
</style>