<style>
    .dashboard-2-users-header-detail-cart span {
    position: absolute;
    top: 0
px
;
    right: 0
px
;
    background-color: red;
    color: #fff;
    border-radius: 50%;
    width: 12
px
;
    height: 13
px
;
    text-align: center;
    font-size: 10px;
}
</style>
<header class="dashboard-2-users-header">
					<div class="dashboard-2-users-header-page">
						<div class="dashboard-2-users-header-data">

							<div class="dashboard-2-users-header-detail-1">
								<div class="dashboard-2-users-header-srch">
									<input type="text" name="" placeholder="Search for...">
									<i class="fa fa-search"></i>
								</div>
							</div>

							<div class="dashboard-2-users-header-detail-2">
								<!--<div class="dashboard-2-users-header-detail-slct">-->
								<!--	<select name="" id="dashboard2myprofile">-->
								<!--	  <option value="profile" selected><a href="{{ route('dashboard') }}">My Profile</a></option>-->
								<!--	  <option value="Logout"><a href="{{ route('logout') }}">Logout</a></option>-->
								<!--	</select>-->
								<!--</div>-->
								
								
								<!--<div class="dropdown dropdown_center">-->
        <!--                          <a-->
        <!--                            class="-->
        <!--                              btn btn-secondary-->
        <!--                              dropdown-toggle dropdown_color-->
        <!--                              d-button-->
        <!--                            "-->
        <!--                            href="{{ route('dashboard') }}"-->
        <!--                            role="button"-->
        <!--                            id="dropdownMenuLink"-->
        <!--                            data-toggle="dropdown"-->
        <!--                            aria-haspopup="true"-->
        <!--                            aria-expanded="false"-->
        <!--                          >-->
        <!--                            My Profile-->
        <!--                          </a>-->
        
        <!--                          <div-->
        <!--                            class="dropdown-menu d-menu"-->
        <!--                            aria-labelledby="dropdownMenuLink"-->
        <!--                          >-->
        <!--                            <a class="dropdown-item d-button1" href="{{ route('logout') }}"-->
        <!--                              >Logout</a-->
        <!--                            >-->
        <!--                          </div>-->
        <!--                        </div>-->
                                @php
                                    $support_ticket = DB::table('tickets')
                                                ->where('user_id', Auth::user()->id)
                                                ->where('viewed', 0)
                                                ->select('id')
                                                ->count();
                                    $order_ticket = DB::table('orders')
                                                ->where('user_id', Auth::user()->id)
                                                ->where('viewed', 0)
                                                ->select('id')
                                                ->count();
                                @endphp
                                <div class="dashboard-2-users-header-detail-outers">
                                    <div class="dashboard-2-users-header-detail-msg">
                                        <i class="fa fa-envelope-o"></i>
                                         <span class="pull-right badge badge-info">@if($support_ticket > 0) {{ $support_ticket }} @else 0 @endif </span>
                                    </div>
                                    <div class="dashboard-2-users-header-detail-bell">
                                        <i class="fa fa-bell-o"></i>
                                        <span class="pull-right badge badge-info">@if($order_ticket > 0) {{ $order_ticket }} @else 0 @endif </span>
                                    </div>
                                    <div class="dashboard-2-users-header-detail-cart">
                                        @if(\Session::has('cart'))
                                        <a href="#" data-toggle="modal" data-target="#myModal" ><i class="fa fa-shopping-cart"></i></a>
                                        <span class="pull-right badge badge-info">{{count(\Session::get('cart'))}}</span>
                                        @else
                                            <a href="#" data-toggle="modal" data-target="#myModal" ><i class="fa fa-shopping-cart"></i></a>
                                        <span class="pull-right badge badge-info">0</span>
                                        @endif
                                        
                                    </div>
                                    <!-- The Modal -->
                                    <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content bg-white">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Cart Detail</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            	@if(\Session::has('cart'))
<div class="payment-page-summary-detail-table-data">
            <table>
                <tr>
                    <th>{{__('PRODUCTS NAME')}}</th>
                    <th>{{__('Quantity')}}</th>
                    <th>Photo</th>
                    <th>{{__('Amount')}}</th>
                </tr>
                @php
                    $subtotal = 0;
                    $tax = 0;
                    $shipping = 0;
                @endphp
                @foreach (Session::get('cart') as $key => $cartItem)
                    @php
                    $product = \App\Models\Product::find($cartItem['id']);
                    $subtotal += $cartItem['price']*$cartItem['quantity'];
                    $tax += $cartItem['tax']*$cartItem['quantity'];
                    $shipping += $cartItem['shipping']*$cartItem['quantity'];
                    $product_name_with_choice = $product->name;
                    if(isset($cartItem['color'])){
                        $product_name_with_choice .= ' - '.\App\Models\Color::where('code', $cartItem['color'])->first()->name;
                    }
                    foreach (json_decode($product->choice_options) as $choice){
                        $str = $choice->name; // example $str =  choice_0
                        $product_name_with_choice .= ' - '.$cartItem[$str];
                    }
                    @endphp
                    <tr>
                        <td>{{ $product_name_with_choice }}</td>
                        <td>{{ $cartItem['quantity'] }}</td>
                        <td><img src="{{ asset($product['thumbnail_img']) }}" style="height:40px;width:40px"></td>
                        <td>${{ $cartItem['price']*$cartItem['quantity'] }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td>Total</td>
                    <td></td>
                    <td></td>
                    <td>${{$subtotal}}</td>
                </tr>
            </table>
        </div>
        @endif
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
            @if(\Session::has('cart'))
            <a class="btn btn-success" href="{{ route('cart') }}">Proceed Checkout</a>
            <!--<span>{{count(\Session::get('cart'))}}</span>-->
            @endif
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
                                </div>    
								<div class="dashboard-2-users-header-detail-profile-logout-cst">
                <a href="{{ route('logout') }}" style="background-color: #D0EAF0;color: #000;font-weight: bold;border: 1px solid #D0EAF0;padding: 6px 42px;">LOGOUT</a>
            </div>
								<div class="responsive-icon-2">
									<i class="fa fa-bars" onclick="showdashusertwo()"></i>
								</div>
							</div>

						</div>
					</div>
				</header>