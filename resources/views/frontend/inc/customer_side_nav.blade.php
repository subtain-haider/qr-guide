<div class="dashboard-users-sec-detail-1">
                <div class="dashboard-users-sec-detail-1-top-section">
                    <p>{{ Auth::user()->name }}</p>
                    <button><a href="#">Online</a></button>
                </div>
				<div class="dashboard-users-sec-detail-1-side-links">
					<div class="dashboard-users-sec-detail-1-side-button">
						<button data-toggle="tooltip" data-placement="bottom" title="Dashboard"><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i></a></button>
					</div>
					<div class="dashboard-users-sec-detail-1-side-button">
						<button><a href="#"><i class="fa fa-inbox"></i></a></button>
					</div>
					<div class="dashboard-users-sec-detail-1-side-button">
						<button data-toggle="tooltip" data-placement="bottom" title="Profile"><a href="{{ route('profile') }}"><i class="fa fa-user"></i></a></button>
					</div>
					<div class="dashboard-users-sec-detail-1-side-button">
						<button><a href="#"><i class="fa fa-qrcode"></i></a></button>
					</div>
					<div class="dashboard-users-sec-detail-1-side-button">
						<button data-toggle="tooltip" data-placement="bottom" title="Support Tickets"><a href="{{ route('customer.contact_support') }}"><i class="fa fa-comments-o"></i></a></button>
					</div>
					<div class="dashboard-users-sec-detail-1-side-button">
						<button data-toggle="tooltip" data-placement="bottom" title="Purchase History"><a href="{{ route('purchase_history.index') }}"><i class="fa fa-bar-chart"></i></a></button>
					</div>
				</div>
			</div>
			<script>
			    $(function () {
                  $('[data-toggle="tooltip"]').tooltip()
                });
			</script>