<!DOCTYPE html>
<html>
<head>
	<title>banner4</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<link rel="stylesheet" type="text/css" href="assets/css/style.css"> 	
</head>
<body>
<div class="bn4-flex-1">
	<div class="bn4-page">
		<div class="bn4-main">
			<img src="{{asset('frontend/images/bn4-inner-2.png')}}" class="bn4-inner-2">
			<img src="{{asset('frontend/images/bn4-inner-3.png')}}" class="bn4-inner-3">
			<img src="{{asset('frontend/images/bn4-inner-4.png')}}" class="bn4-inner-4">
			<img src="{{asset('frontend/images/bn4-inner-5.png')}}" class="bn4-inner-5">
			<img src="{{asset('frontend/images/bn4-inner-6.png')}}" class="bn4-inner-6">
			<img src="{{asset('frontend/images/bn4-inner-7.png')}}" class="bn4-inner-7">
			@if(!empty($datas))
			 <a href="#">{{$datas[0]}}</a>
            @elseif(!empty($data))
            <a href="#">{{$data['coupon_text'][0]}}</a>
            @elseif(!empty($coupon))
            <a href="#">{{ $coupon->code }}</a>
            @endif
		</div>
	</div>	
</div>
</body>
</html>
<style type="text/css">
	*{
	margin: 0px;
	padding: 0px;
}
.bn4-flex-1 {
	display: flex;
	justify-content: center;
	align-items: center;
	background-color: black;
	height: auto;
}
.bn4-page {
	width: 100%;
    border: 20px solid white;
    height: 403px;
}
.bn4-main {
	width: 100%;
	position: relative;
	background:url(/public/frontend/images/bn4-main.png);
	background-position: center center;
	background-size: cover;
	background-repeat: no-repeat;
	height: 100%;
}
.bn4-inner-2 {
	position: absolute;
    top: 100px;
    left: 180px;
    width: 35px;
    height: 45px;
}
.bn4-inner-3 {
    position: absolute;
    top: 110px;
    left: 210px;
    width: 125px;
}
.bn4-inner-4 {
    position: absolute;
    top: 247px;
    left: 175px;
    width: 120px;
}
.bn4-inner-5 {
    position: absolute;
    top: 175px;
    left: 145px;
    width: 180px;
}
.bn4-inner-6 {
    position: absolute;
   top: 195px;
    left: 150px;
    width: 140px;
}
.bn4-inner-7 {
    position: absolute;
    top: 150px;
    left: 245px;
    width: 150px;
}
.bn4-main a {
	position: absolute;
	top: 252px;
    left: 186px;
	text-decoration: none;
	color: #fff;
	font-weight: 500;
	font-size: 14px;
	font-family: sans-serif;
}
.bn4-main a:hover {
	color:#777;
}	
@media screen and (max-width: 1000px) {
	.bn4-page {
		width: 660px;
		height: 402px;
		border:17px solid white;
	}	
    .bn4-main a {
	    top: 350px;
    	left: 280px;
	    font-size: 19px;
	}
	.bn4-inner-2 {
	    top: 121px;
	    left: 215px;
	    width: 25px;
	    height: 35px;
	}
	.bn4-inner-3 {
	    top: 124px;
	    left: 248px;
	    width: 170px;
	}
	.bn4-inner-4 {
	    top: 343px;
    	left: 261px;
    	width: 145px;
	}
	.bn4-inner-5 {
	    top: 233px;
    	left: 220px;
    	width: 236px;
	}
	.bn4-inner-6 {
	    top: 260px;
    	left: 250px;
    	width: 250px;
	}
	.bn4-inner-7 {
	    top: 169px;
   		left: 227px;
    	width: 175px;
	}
}
@media screen and (max-width: 685px) {
	.bn4-page {
		width: 480px;
		height: 400px;
		border:13px solid white;
	}	
    .bn4-main a {
	    top: 276px;
    	left: 198px;
    	font-size: 16px;
	}
	.bn4-inner-2 {
	    top: 100px;
    	left: 158px;
    	width: 18px;
    	height: 30px;
	}
	.bn4-inner-3 {
        top: 101px;
    	left: 183px;
    	width: 150px;
	}
	.bn4-inner-4 {
        top: 272px;
    	left: 270px;
    	width: 108px;

	}
	.bn4-inner-5 {
	    top: 198px;
   		left: 172px;
    	width: 164px;
	}
	.bn4-inner-6 {
        top: 214px;
    	left: 156px;
    	width: 170px;
	}
	.bn4-inner-7 {
	    top: 141px;
    	left: 167px;
    	width: 145px;
	}
}
@media screen and (max-width: 505px) {
	.bn4-page {
		width: 300px;
		height: 280px;
		border:7px solid white;
	}
    .bn4-main a {
	    top: 193px;
    	left: 272px;
    	font-size: 12px;
	}
	.bn4-inner-2 {
	    top: 73px;
    	left: 88px;
    	width: 16px;
    	height: 25px;
	}
	.bn4-inner-3 {
        top: 75px;
    	left: 108px;
    	width: 110px;
	}
	.bn4-inner-4 {
        top: 190px;
    	left: 111px;
    	width: 79px;
	}
	.bn4-inner-5 {
        top: 133px;
    	left: 102px;
    	width: 110px;
	}
	.bn4-inner-6 {
        top: 146px;
    	left: 82px;
    	width: 135px;
	}
	.bn4-inner-7 {
	    top: 106px;
    	left: 96px;
    	width: 111px;
	}
}
</style>