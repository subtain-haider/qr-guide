<!DOCTYPE html>
<html>
<head>
	<title>banner1</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<link rel="stylesheet" type="text/css" href="assets/css/style.css"> 	
</head>
<body>
<div class="bn1-flex-1">
	<div class="bn1-page">
		<div class="bn1-main">
			<img src="{{asset('frontend/images/bn1-inner.png')}}">
			@if(!empty($datas))
			 <a href="#">{{$datas[0]}}</a>
            @elseif(!empty($data))
            <a href="#">{{$data['coupon_text'][0]}}</a>
            @elseif(!empty($coupon))
            <a href="#">{{ $coupon->code }}</a>
            @endif
		</div>
	</div>	
</div>
</body>
</html>
<style type="text/css">
	*{
	margin: 0px;
	padding: 0px;
}
.bn1-flex-1 {
	display: flex;
	justify-content: center;
	align-items: center;
	background-color: black;
	height: auto;
	/*overflow-y: scroll;*/
}
.bn1-page {
	width: 100%;
    border: 20px solid white;
    height: 390px;
}
.bn1-main {
	width: 100%;
	position: relative;
	background:url(/public/frontend/images/bn1-main.png);
	background-position: center center;
	background-size: cover;
	background-repeat: no-repeat;
	height: 100%;
}
.bn1-main img {
	position: absolute;
    top: 40px;
    left: 200px;
    width: 200px;
}
.bn1-main a {
    position: absolute;
    bottom: 140px;
    left: 225px;
    width: auto;
    background: #d2d100;
    font-size: 18px;
    padding: 0 10px;
    color: #fff;
    font-weight: 700;
}
@media screen and (max-width: 1000px) {
	.bn1-page {
		width: 660px;
		height: 500px;
		border:16px solid white;
	}
	.bn1-main img {
	    top: 34px;
    	left: 299px;
    	width: 320px;
    }	
}
@media screen and (max-width: 700px) {
	.bn1-page {
		width: 480px;
		height: 400px;
		border:12px solid white;
	}
	.bn1-main img {
	    top: 30px;
    	left: 200px;
    	width: 250px;
    }	
}
@media screen and (max-width: 505px) {
	.bn1-page {
		width: 300px;
		height: 280px;
		border:7px solid white;
	}
	.bn1-main img {
        top: 20px;
    	left: 120px;
    	width: 160px;

    }	
}
</style>