<section class="coupon-3-pop-sec">
    <div class="coupon-3-pop-sec-page"> 
        <div class="coupon-3-pop-data">
            <div class="coupon-3-pop-img">
                <img src="{{asset('frontend/images/ccoupon1.png')}}">

            </div>
            <div style="position: relative;/* background: #171e26; *//* padding: 100px; */top: -48%;width: 100%;">
                <div class="coupon-3-pop-head">
                    @if(isset($datas))
                    <h2>{{ $datas[0] }}</h2>
                    @else
                    <h2>{{ $data['coupon_text'][0] }}</h2>
                    @endif
                </div>
            </div>
            <div style="position: relative;/* background: #171e26; *//* padding: 100px; */top: -42%;width: 100%;">
                <div class="coupon-3-pop-para">
                    @if(isset($datas))
                    <p>{{$datas[1]}}</p>
                    @else
                    <p>{{$data['coupon_text'][1]}}</p>
                    @endif
                </div>
            </div>
            <div style="position: relative;/* background: #171e26; *//* padding: 100px; */top: -42%;width: 100%;">
                <div class="coupon-3-pop-sale">
                    @if(isset($datas))
                    <span>{{$datas[2]}}</span>
                    @else
                    <span>{{$data['coupon_text'][2]}}</span>
                    @endif
                </div>
            </div>
            <div>
                <div class="coupon-3-pop-btn">
                    @if(isset($datas))
                    <button style="background: none;">{{$datas[3]}}</button>
                    @else
                    <button style="background: none;">{{$data['coupon_text'][3]}}</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!-- css -->
<style>
/*-------------------------coupon-3-----------------------------*/
.coupon-3-pop-sec{
    display: flex;
    justify-content: center;
    background-color: #000;
    align-items: center;
}
.coupon-3-pop-sec-page{
    width: 700px;
    height: 570px;
    
    border: 10px solid #fff;
}
.coupon-3-pop-data{
    background-color: #1FAEC6;
    height: 100%;
    overflow: hidden;
    position: relative;
    
}
.coupon-3-pop-img{
    width: 1000px;
    height: 450px;
    transform: rotate(-10deg);
    margin-left: -100px;
    
    
}
.coupon-3-pop-img img{
    /*width: 100%;*/
    width: 82%;
    margin-top: 45px;
    height: 100%;
}
.coupon-3-pop-head{
    /*position: absolute;*/
    /*top: 35%;
    left: 30%;*/
    /*top: 38%;
    left: 20%;*/
    transform: rotate(-15deg);
    margin-left: 127px;
    margin-top: -27px;
}
.coupon-3-pop-head h2{
    color: #fff;
    font-size: 82px;
    font-family: 'Play';
    font-weight: 600;
}
.coupon-3-pop-para{
    /*position: absolute;*/
    /*top: 52%;
    left: 30%;*/
    /*top: 52%;
    left: 23%;*/
    transform: rotate(-15deg);
    margin-top: -21px;
    margin-left: 15px;
}
.coupon-3-pop-para p{
    color: #fff;
    font-size: 52px;
    font-family: 'Play';
    font-weight: 600;
}
.coupon-3-pop-sale{
    /*position: absolute;*/
    /*top: 67%;
    left: 30%;*/
    /*top: 66%;
    left: 25%;*/
    /*transform: rotate(-15deg);*/
    transform: rotate(-17deg);
    text-align: center;
    margin-top: -13px;
}
.coupon-3-pop-sale span{
    color: #fff;
    padding: 4px 15px;
    font-size: 18px;
    font-family: 'Play';
    font-weight: 600;
    /*background-color: #FF0000;*/
    border-radius: 20px;
}
.coupon-3-pop-btn{
    position: absolute;
    /*bottom: 70px;
    left: 57%;*/
    bottom: 149px;
    right: 17%;
    transform: rotate(-15deg);
}
.coupon-3-pop-btn button{
    background-color: #fff;
    color: #FF0000;
    /*padding: 6px 20px;*/
    font-size: 18px;
    outline: none;
    border: none;
    font-family: 'Play';
    font-weight: 600;
    border-radius: 30px;

}

@media only screen and (max-width: 750px) {
    .coupon-3-pop-sec-page{
        width: 500px;
        height: 475px;
    }
    .coupon-3-pop-img{
        width: 689px;
        height: 376px;
    }
    .coupon-3-pop-head h2{
        font-size: 62px;
    }
    .coupon-3-pop-para p{
        font-size: 38px;
    }
    .coupon-3-pop-sale{
        top: 65%;
    }
    .coupon-3-pop-btn {
        bottom: 55px;
    }
}

@media only screen and (max-width: 535px) {
    .coupon-3-pop-sec-page{
        width: 274px;
        height: 357px;
    }
    .coupon-3-pop-img{
        width: 429px;
        height: 260px;
    }
    .coupon-3-pop-head h2{
        font-size: 40px;
    }
    .coupon-3-pop-para p{
        font-size: 22px;
    }
    .coupon-3-pop-sale{
        left: 8%;
    }
    .coupon-3-pop-sale span{
        font-size: 14px;
    }

    .coupon-3-pop-btn {
        bottom: 35px;
        left: 28%;
    }
    .coupon-3-pop-btn button{
        font-size: 16px;
    }
}
</style>