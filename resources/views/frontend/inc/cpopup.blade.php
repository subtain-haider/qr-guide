<section class="popup-bg-1">
    <div class="popup-page">
        <img src="{{asset('frontend/images/cpopup1.jpg')}}">
        @if(isset($datas))
        <p>{{$datas[0]}}</p>
        @else
        <p>{{$data['coupon_text'][0]}}</p>
        @endif
        @if(isset($datas))
         <h5>{{$datas[1]}}</h5>
        @else
         <h5>{{$data['coupon_text'][1]}}</h5>
        @endif
        @if(isset($datas))
        <h6>{{$datas[2]}}</h6>
        @else
        <h6>{{$data['coupon_text'][2]}}</h6>
        @endif
        @if(isset($datas))
        <a href="#">{{$datas[3]}}</a>
        @else
        <a href="#">{{$data['coupon_text'][3]}}</a>
        @endif
        

    </div>
</section>
<!-- css -->
<style>
.popup-bg-1 {
    display: flex;
    justify-content: center;
    align-items: center;
}
.popup-page {
    width: 500px;
    height: 400px;
    display: flex;
    align-items: center;
    position: relative;
}
.popup-page img {
    height: 100%;
    width: 100%;
}
.popup-page p {
    position: absolute;
    top: 104px;
    left: 180px;
    font-weight: 600;
    font-size: 20px;
    transform: rotate(-5.5deg);
}
.popup-page h5 {
    color: #FFEB01;
    position: absolute;
    top: 130px;
    left: 180px;
    font-weight: 600;
    font-size: 50px;
    transform: rotate(-5.5deg);
}
.popup-page h6 {
    color: #FFEB01;
    position: absolute;
    top: 205px;
    left: 185px;
    font-weight: 600;
    font-size: 40px;
    transform: rotate(-5.5deg);
}
.popup-page a {
    color: #FFF;
    position: absolute;
    top: 260px;
    left: 245px;
    font-weight: 600;
    font-size: 18px;
    transform: rotate(-5.5deg);
    text-decoration: none;
}
.popup-page a:hover {
    color: #fff;
    text-decoration: none;
}
@media screen and (max-width: 500px) {
    .popup-page {
        width: 400px;
        height: 400px;
    }
    .popup-page p {
        top: 108px;
        left: 146px;
        font-size: 16px;
        transform: rotate(-6.5deg); 
    }
    .popup-page h5 {
        top: 130px;
        left: 130px;
        font-size: 52px;
        transform: rotate(-6.3deg);
    } 
    .popup-page h6 {
        top: 198px;
        left: 147px;
        font-size: 52px;
        transform: rotate(-6.7deg);
    }
    .popup-page a {
        top: 262px;
        left: 190px;
        font-size: 16px;
        transform: rotate(-6.5deg);
    }
}
@media screen and (max-width: 400px) {
    .popup-page {
        width: 350px;
        height: 350px;
    }
    .popup-page p {
        top: 94px;
        left: 127px;
        font-size: 14px;
        transform: rotate(-6.5deg);
    }
    .popup-page h5 {
        top: 116px;
        left: 115px;
        font-size: 44px;
        transform: rotate(-6.5deg);
    } 
    .popup-page h6 {
        top: 173px;
        left: 132px;
        font-size: 44px;
        transform: rotate(-6.7deg);
    } 
    .popup-page a {
        top: 229px;
        left: 166px;
        font-size: 14px;
        transform: rotate(-6.5deg);
    }
}
</style>