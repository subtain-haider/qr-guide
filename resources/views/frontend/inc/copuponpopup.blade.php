<section class="newcoupon-3-pop-sec">
	<div class="newcoupon-3-pop-sec-page">
		<div class="newcoupon-3-pop-data">
			<div class="newcoupon-3-pop-img">
				<img src="images/blk2.jpg">

			</div>
			<div class="newcoupon-3-pop-head">
				<h2>MEGA</h2>
			</div>
			
			<div class="newcoupon-3-pop-para">
				<p>DISCOUNT</p>
			</div>
			<div class="newcoupon-3-pop-sale">
				<span>CODE: {{ $coupon->code }}</span>
			</div>
			<div class="newcoupon-3-pop-btn">
				<button>SHOP NOW</button>
			</div>
		</div>
	</div>
</section>

<style>
.newcoupon-3-pop-sec{
	display: flex;
	justify-content: center;
	background-color: #000;
	height: 100vh;
	align-items: center;
}
.newcoupon-3-pop-sec-page{
	width: 700px;
	height: 570px;
	
	border: 10px solid #fff;
}
.newcoupon-3-pop-data{
	background-color: #1FAEC6;
	height: 100%;
	overflow: hidden;
	position: relative;
	
}
.newcoupon-3-pop-img{
	width: 1000px;
	height: 450px;
	transform: rotate(-10deg);
	margin-left: -100px;
	
	
}
.newcoupon-3-pop-img img{
	width: 100%;
	margin-top: 45px;
	height: 100%;
}
.newcoupon-3-pop-head{
	position: absolute;
	top: 20%;
	left: 30%;
	transform: rotate(-15deg);
}
.newcoupon-3-pop-head h2{
	color: #fff;
	font-size: 82px;
	font-family: 'Play';
	font-weight: 600;
}
.newcoupon-3-pop-para{
	position: absolute;
	top: 35%;
	left: 30%;
	transform: rotate(-15deg);
}
.newcoupon-3-pop-para p{
	color: #fff;
	font-size: 52px;
	font-family: 'Play';
	font-weight: 600;
}
.newcoupon-3-pop-sale{
	position: absolute;
	top: 51%;
	left: 34%;
	transform: rotate(-15deg);
}
.newcoupon-3-pop-sale span{
	color: #fff;
	padding: 4px 15px;
	font-size: 18px;
	font-family: 'Play';
	font-weight: 600;
	background-color: #FF0000;
	border-radius: 20px;
}
.newcoupon-3-pop-btn{
	position: absolute;
	bottom: 170px;
	left: 54%;
	transform: rotate(-15deg);
}
.newcoupon-3-pop-btn button{
	background-color: #fff;
	color: #FF0000;
	padding: 6px 20px;
	font-size: 18px;
	outline: none;
	border: none;
	font-family: 'Play';
	font-weight: 600;
	border-radius: 30px;

}

@media only screen and (max-width: 750px) {
	.newcoupon-3-pop-para{
		left: 34%;
	}
	.newcoupon-3-pop-sec-page{
		width: 500px;
		height: 475px;
	}
	.newcoupon-3-pop-img{
		width: 689px;
    	height: 376px;
	}
	.newcoupon-3-pop-head h2{
		font-size: 62px;
	}
	.newcoupon-3-pop-para p{
		font-size: 38px;
	}
	.newcoupon-3-pop-sale{
		top: 52%;
		left: 30%;
	}
	.newcoupon-3-pop-btn {
		bottom: 117px;
	}
}

@media only screen and (max-width: 535px) {
	.newcoupon-3-pop-head{
		left:20%;
	}
	.newcoupon-3-pop-para {
		left: 26%;
	}
	.newcoupon-3-pop-sec-page{
		width: 274px;
		height: 357px;
	}
	.newcoupon-3-pop-img{
		width: 429px;
    	height: 260px;
	}
	.newcoupon-3-pop-head h2{
		font-size: 40px;
	}
	.newcoupon-3-pop-para p{
		font-size: 22px;
	}
	.newcoupon-3-pop-sale{
		left: 8%;
	}
	.newcoupon-3-pop-sale span{
		font-size: 14px;
	}

	.newcoupon-3-pop-btn {
		bottom: 73px;
		left: 28%;
	}
	.newcoupon-3-pop-btn button{
		font-size: 16px;
	}
}
</style>
