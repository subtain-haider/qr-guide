<div class="header bg-white">
    <!-- Top Bar -->
    <div class="top-navbar d-none d-lg-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col">
                </div>

                <div class="col-5 text-right d-none d-lg-block">
                    <ul class="inline-links">
                        @auth
                        <li>
                            <a href="{{ route('dashboard') }}" class="top-bar-item">{{__('My Profile')}}</a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" class="top-bar-item">{{__('Logout')}}</a>
                        </li>
                        @else
                        <li>
                            <a href="{{ route('user.login') }}" class="top-bar-item">{{__('Login')}}</a>
                        </li>
                        <li>
                            <a href="{{ route('user.registration') }}" class="top-bar-item">{{__('Registration')}}</a>
                        </li>
                        @endauth
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END Top Bar -->
</div>

