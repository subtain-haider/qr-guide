<!-- top bar  -->
<!-- mob-product-3-html before login-->
<style>

	@media screen and (max-width: 600px) {
  .nevhide {
   
    display: none;
  }

  #img1{
  	display: none;
  }
  #img2{
  	display: block !important;
  	
  }
  #img2 .header-logo{
width :  50% !important;
  }
  li{
      list-style:none;
  }
  


}

     @font-face {
        src: url('/public/fonts/Dungeon.ttf');
        font-family: "dungeon";
    }
    
    header{
  font-family:  dungeon;
  font-weight: 0 !important;
letter-spacing: 0.5px;
}
	</style>
	<style type="text/css">
		

			.iconnoti {
			    cursor: pointer;
			    padding: 20px;
			}

			.iconnoti span {
			    background: #f00;
			    padding: 7px;
			    border-radius: 50%;
			    color: #fff;
			    vertical-align: top;
			    margin-left: -25px
			}

			.iconnoti img {
			    display: inline-block;
			    width: 26px;
			    margin-top: 4px
			}

			.iconnoti:hover {
			    opacity: .7
			}

			.logonoti {
			    flex: 1;
			    margin-left: 50px;
			    color: #eee;
			    font-size: 20px;
			    font-family: monospace
			}

			.notificationsnoti {
			    overflow-y: scroll;
			    width: 300px;
			    height: 0px;
			    opacity: 0;
			    position: absolute;
			    top: 40px;
			    right: 0px;
			    border-radius: 5px 0px 5px 5px;
			    background-color: #fff;
			    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
			}

			.notificationsnoti h2 {
			    font-size: 14px;
			    padding: 10px;
			    border-bottom: 1px solid #eee;
			    color: #999
			}

			.notificationsnoti h2 span {
			    color: #f00
			}

			.notifications-itemnoti {
			    display: flex;
			    border-bottom: 1px solid #fbfbfb;
			    padding: 6px 9px;
			    margin-bottom: 0px;
			    background-color: #eee;
			    cursor: pointer
			}

			.notifications-itemnoti:hover {
			    /*background-color: #eee*/
			}

			.notifications-itemnoti img {
			    display: block;
			    width: 50px;
			    height: 50px;
			    margin-right: 9px;
			    border-radius: 50%;
			    margin-top: 2px
			}

			.notifications-itemnoti .textnoti h4 {
			    color: #777;
			    font-size: 16px;
			    margin-top: 3px
			}

			.notifications-item .textnoti p {
			    color: #aaa;
			    font-size: 12px
			}
	</style>
	
<header class="mainmob-product-3-header">
	<div class="mainmob-product-3-header-page">
		<div class="mainmob-product-3-header-data">
			<div class="mainmob-product-3-header-detail" id="img1">
				@auth
    			<a href="{{route('dashboard')}}">
    			@else
    			<a href="{{route('home')}}">
    			@endauth
					@php
						$generalsetting = \App\Models\GeneralSetting::first();
					@endphp
					@if($generalsetting->logo != null)
						<img src="{{ asset($generalsetting->logo) }}" alt="@yield('meta_title', config('app.name', 'Laravel'))" class="header-logo">
					@else
						<h2 class="mb-0">@yield('meta_title', config('app.name', 'Laravel'))</h2>
					@endif
				</a>
			</div>
			<!------ logo big for mobile --------->
			<div class="mainmob-product-3-header-detail" style="display:none" id="img2">
				<a href="{{route('home')}}">
				
						<img src="{{ asset('uploads/logo/biglogo.png') }}" alt="@yield('meta_title', config('app.name', 'Laravel'))" class="header-logo">
				
				</a>
			</div>
<!------ end logo big for mobile --------->

			<div class="mainmob-product-3-header-menu">
				<ul>
					<li><a class="activebtn {{ (Request::segment(1) == 'how-to-use') ? 'myactivebtn' : '' }} " href="#">HOW TO USE</a></li>
					<li><a class="activebtn {{ (Request::segment(1) == 'api-page') ? 'myactivebtn' : '' }}" href="{{ url('/api-page') }}">TOOLS & API</a></li>
					<li><a class="activebtn {{ (Request::segment(1) == 'faq-page') ? 'myactivebtn' : '' }}" href="{{url('/faq-page')}}">FAQ</a></li>
					<li><a class="activebtn {{ (Request::segment(1) == 'pricing') ? 'myactivebtn' : '' }}" href="{{url('/pricing')}}">SELECT PACKAGE</a></li>
				</ul>
			</div>
			<div class="mainmob-product-3-header-icons-data">
				@auth
					<div class="mainmob-product-3-header-btns-data nevhide" >
						<a href="{{ route('seller.guide.upload') }}"><button class="mainmob-product-3-header-btn-1">{{__('ADD A GUIDE')}}</button></a>
					</div>
					<div class="mob-product-3-header-icons-data nevhide" >
						<div class="mob-product-3-header-bellicon">
							
                    		@if(\Session::has('cart'))
                            <a href="#" data-toggle="modal" data-target="#myModal" ><i class="fa fa-shopping-cart"></i></a>
                            <span>{{count(\Session::get('cart'))}}</span>
                            @else
                                <a href="#" data-toggle="modal" data-target="#myModal" ><i class="fa fa-shopping-cart"></i></a>
                            <span>0</span>
                            @endif
                            
						</div>
						<div class="mob-product-3-header-bellicon" id="bellnoti">
						  @php
						   $order_ticket = DB::table('orders')
                                                ->where('user_id', Auth::user()->id)
                                                ->where('viewed', 0)
                                                ->select('id')
                                                ->count();
                            $a = DB::table('users')
                                                ->where('id', Auth::user()->id)
                                                ->where('viewed', 0)
                                                ->select(['id',
                                                DB::raw("'users' as tb"),
                                                DB::raw("'User' as heading"),
                                                DB::raw("'clients-report' as link"),
                                                DB::raw("'New User has been created' as msg"),
                                                ])
                                                ->get();
                            $b = DB::table('orders')
                                                ->where('user_id', Auth::user()->id)
                                                ->where('viewed', 0)
                                                ->select(['id',
                                                DB::raw("'orders' as tb"),
                                                DB::raw("'Orders' as heading"),
                                                DB::raw("'total-sales' as link"),
                                                DB::raw("'New Orders has been created' as msg"),
                                                ])
                                                ->get();
                            $c = DB::table('questions')
                                                ->where('user_id', Auth::user()->id)
                                                ->where('viewed', 0)
                                                ->select(['id',
                                                DB::raw("'questions' as tb"),
                                                DB::raw("'Questions' as heading"),
                                                DB::raw("'seller-questions' as link"),
                                                DB::raw("'New Questions has been created' as msg"),
                                                ])
                                                ->get();
                            $d = DB::table('requestuses')
                                                ->where('userid', Auth::user()->id)
                                                ->where('viewed', 0)
                                                ->select(['id',
                                                DB::raw("'requestuses' as tb"),
                                                DB::raw("'Request' as heading"),
                                                DB::raw("'#' as link"),
                                                DB::raw("'New Request has been created' as msg"),
                                                ])
                                                ->get();
                            $e = DB::table('reviews')
                                                ->where('user_id', Auth::user()->id)
                                                ->where('viewed', 0)
                                                ->select(['id',
                                                DB::raw("'reviews' as tb"),
                                                DB::raw("'Reviews' as heading"),
                                                DB::raw("'sellerreviews' as link"),
                                                DB::raw("'New Reviews has been created' as msg"),
                                                ])
                                                ->get();
                            $f = DB::table('tickets')
                                                ->where('user_id', Auth::user()->id)
                                                ->where('viewed', 0)
                                                ->select(['id',
                                                DB::raw("'tickets' as tb"),
                                                DB::raw("'Tickets' as heading"),
                                                DB::raw("'customers/tickets' as link"),
                                                DB::raw("'New Tickets has been created' as msg"),
                                                ])
                                                ->get();
                            $g = DB::table('guiderequests')
                                                ->where('userid', Auth::user()->id)
                                                ->where('viewed', 0)
                                                ->select(['id',
                                                DB::raw("'guiderequests' as tb"),
                                                DB::raw("'Guid Requests' as heading"),
                                                DB::raw("'#' as link"),
                                                DB::raw("'New Guid Requests has been created' as msg"),
                                                ])
                                                ->get();
                            $notificaitons =  $a->merge($b)->merge($c)->merge($d)->merge($e)->merge($f)->merge($g);
                            @endphp
                            <div class="notificationsnoti" id="boxnoti">
                    	        <h2>Notifications</h2>
                    	        @foreach($notificaitons as $notify)
                    	        <div class="notifications-itemnoti">
                    	            <a href="{{ url('/').'/viewed/'.$notify->tb.'/'.$notify->id }}">
                        	            <div class="textnoti">
                        	                <h4>{{ $notify->heading }}</h4>
                        	                <p>{{ $notify->msg }}</p>
                        	            </div>
                    	            </a>
                    	        </div>
                    	        @endforeach
                    	        <!--<div class="notifications-itemnoti">-->
                    	        <!--    <div class="textnoti">-->
                    	        <!--        <h4>John Silvester</h4>-->
                    	        <!--        <p>+20 vista badge earned</p>-->
                    	        <!--    </div>-->
                    	        <!--</div>-->
                    	    </div>
						  <!--<button type="button" class="btn" data-toggle="modal" data-target="#myModal">-->
                            <i class="fa fa-bell"></i>
                            <span>{{ isset($notificaitons) ? count($notificaitons) : 0 }}</span>
                          <!--</button>-->

                              <!-- The Modal -->
                              <div class="modal" id="myModal">
                                <div class="modal-dialog">
                                  <div class="modal-content bg-white">
                                  
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                      <h4 class="modal-title">Cart Detail</h4>
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        	@if(\Session::has('cart'))
                            <div class="payment-page-summary-detail-table-data">
                                        <table>
                                            <tr>
                                                <th>{{__('PRODUCTS NAME')}}</th>
                                                <th>{{__('Quantity')}}</th>
                                                <th>Photo</th>
                                                <th>{{__('Amount')}}</th>
                                            </tr>
                                            @php
                                                $subtotal = 0;
                                                $tax = 0;
                                                $shipping = 0;
                                            @endphp
                                            @foreach (Session::get('cart') as $key => $cartItem)
                                                @php
                                                $product = \App\Models\Product::find($cartItem['id']);
                                                $subtotal += $cartItem['price']*$cartItem['quantity'];
                                                $tax += $cartItem['tax']*$cartItem['quantity'];
                                                $shipping += $cartItem['shipping']*$cartItem['quantity'];
                                                $product_name_with_choice = $product->name;
                                                if(isset($cartItem['color'])){
                                                    $product_name_with_choice .= ' - '.\App\Models\Color::where('code', $cartItem['color'])->first()->name;
                                                }
                                                foreach (json_decode($product->choice_options) as $choice){
                                                    $str = $choice->name; // example $str =  choice_0
                                                    $product_name_with_choice .= ' - '.$cartItem[$str];
                                                }
                                                @endphp
                                                <tr>
                                                    <td>{{ $product_name_with_choice }}</td>
                                                    <td>{{ $cartItem['quantity'] }}</td>
                                                    <td><img src="{{ asset($product['thumbnail_img']) }}" style="height:40px;width:40px"></td>
                                                    <td>${{ $cartItem['price']*$cartItem['quantity'] }}</td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td>Total</td>
                                                <td></td>
                                                <td></td>
                                                <td>${{$subtotal}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    @endif
                                    </div>
                                    
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        @if(\Session::has('cart'))
                                        <a class="btn btn-success" href="{{ route('cart') }}">Proceed Checkout</a>
                                        <!--<span>{{count(\Session::get('cart'))}}</span>-->
                                        @endif
                                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    </div>
                                    
                                  </div>
                                </div>
                              </div>
  
                

						
						</div>
						<div class="mob-product-3-header-user">
							<a href="{{ route('profile') }}"><i class="fa fa-user-o"></i></a>
						</div>
						<div class="mob-product-3-header-user">
							<a href="{{ route('dashboard') }}"><img src="{{ asset('public/img/myqyd5.png') }}" style="width:30px;" /></a>
						</div>
						<div class="mob-product-3-header-exit">
							<a href="{{ route('logout') }}">
								<i class="fa fa-share-square-o"></i>
								<span>{{__('Exit')}}</span>
							</a>
						</div>
						
					</div>
					
				@else
					<div class="mainmob-product-3-header-btns-data nevhide" >
						<a href="{{route('user.login')}}"><button class="mainmob-product-3-header-btn-1">{{__('ADD A GUIDE')}}</button></a>
					</div>
					<div class="mainmob-product-3-header-btns-data nevhide" >
						<a href="{{route('user.login')}}"><button class="mainmob-product-3-header-btn-2">{{__('LOGIN / SIGN UP')}}</button></a>
					</div>
				@endauth
				<div class="mob-product-3-header-icons-res-bars">
					<i class="fa fa-bars" onclick=" mainresshow()"></i>
				</div>

			</div>
			
		</div>
	</div>
</header>
<!-- responsive header -->
<header class="mob-product-3-header-responsive-sec" id="mainpagres">
	<div class="mob-product-3-header-responsive-sec-page">
		<div class="mob-product-3-header-responsive-sec-logo text-center">
		    @auth
			<a href="{{route('dashboard')}}">
			@else
			<a href="{{route('home')}}">
			@endauth
				@if($generalsetting->logo != null)
					<img src="{{ asset($generalsetting->logo) }}" alt="@yield('meta_title', config('app.name', 'Laravel'))" class="header-logo">
				@else
					<h2 class="mb-0">@yield('meta_title', config('app.name', 'Laravel'))</h2>
				@endif
			</a>
		</div>
		<div class="mob-product-3-header-responsive-data">
			<div class="mainmob-product-3-header-icons-data">
				@auth
					
					<div class="mob-product-3-header-icons-data " >
						<div class="mob-product-3-header-bellicon">
							<a href="http://myqrguide.com/cart"><i class="fa fa-shopping-cart"></i></a>
							
						</div>
						<div class="mob-product-3-header-bellicon">
							<a href="#"><i class="fa fa-bell"></i></a>
							<span>1</span>
						</div>
						<div class="mob-product-3-header-user">
							<a href="{{ route('profile') }}"><i class="fa fa-user-o"></i></a>
						</div>
						<div class="mob-product-3-header-exit">
							<a href="{{ route('logout') }}">
								<i class="fa fa-share-square-o"></i>
								<span>{{__('Exit')}}</span>
							</a>
						</div>
						
					</div>
				@else
					<div class="mainmob-product-3-header-btns-data " style="width:100%" >
						<a href="{{route('user.login')}}"><button class="mainmob-product-3-header-btn-2" style="width:100%">{{__('LOGIN / SIGN UP')}}</button></a>
					</div>
				@endauth

			</div>
			
			<ul>
				@auth
				<li><div class="mainmob-product-3-header-btns-data " style="width:100%" >
					<a href="{{route('seller.guide.upload')}}"><button class="mainmob-product-3-header-btn-1" style="width:100%">ADD A GUIDE</button></a>
					</div></li>
					@else
					<li><div class="mainmob-product-3-header-btns-data " style="width:100%" >
					<a href="{{route('user.login') }}"><button class="mainmob-product-3-header-btn-1" style="width:100%">ADD A GUIDE</button></a>
					</div></li>
					@endauth
				<li><a href="#">HOW TO USE</a></li>
				<li><a href="#">TOOLS & API</a></li>
				<li><a href="{{url('/faq-page')}}">FAQ</a></li>
				<li><a href="{{url('/pricing')}}">SELECT PACKAGE</a></li>
			</ul>
		</div>
		<!-- @auth
			<div class="mob-product-3-header-icons-data">
				<div class="mob-product-3-header-bellicon">
					<a href="#"><i class="fa fa-bell"></i></a>
					<span>1</span>
				</div>
				<div class="mob-product-3-header-user">
					<a href="{{ route('profile') }}"><i class="fa fa-user-o"></i></a>
				</div>
				<div class="mob-product-3-header-exit">
					<a href="{{ route('logout') }}">
						<i class="fa fa-share-square-o"></i>
						<span>Exit</span>
					</a>
				</div>
				<div class="mob-product-3-header-icons-res-bars">
					<i class="fa fa-bars" onclick=" mainresshow()"></i>
				</div>
			</div>
		@else
			<div class="mainmob-product-3-header-btns-data">
				<a href="{{route('user.login')}}"><button class="mainmob-product-3-header-btn-1">ADD A GUIDE</button></a>
				<a href="{{route('user.login')}}"><button class="mainmob-product-3-header-btn-2">LOGIN / SIGN UP</button></a>
			</div>
		@endauth -->
		<div class="mob-product-3-header-responsive-cross">
			<i class="fa fa-times" onclick=" mainreshide()"></i>
		</div>
	</div>
</header>
<div style="width:300px; margin:0 auto;">
<img id="loading" style="width:200px" alt="" src="https://myqrguide.com/public/uploads/logo/Comp-1_1.gif"/>
</div>
<script type="text/javascript">
		
		$(document).ready(function(){

			var down = false;

			$('#bellnoti').click(function(e){

			var color = $(this).text();
			if(down){

			$('#boxnoti').css('height','0px');
			$('#boxnoti').css('opacity','0');
			down = false;
			}else{

			$('#boxnoti').css('height','300px');
			$('#boxnoti').css('opacity','1');
			down = true;

			}

			});

			});
	</script>
	<script type="text/javascript" src="http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#loading").hide();
});
</script>

