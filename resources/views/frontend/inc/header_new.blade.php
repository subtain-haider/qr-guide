<header class="product-page-header">
    <div class="page">
        <div class="product-page-header-data">
            <div class="product-page-header-detail-1">
                <div class="product-page-header-logo">
                    @php
                        $generalsetting = \App\Models\GeneralSetting::first();
                    @endphp
                    @if($generalsetting->logo != null)
                        <img src="{{ asset($generalsetting->logo) }}" alt="@yield('meta_title', config('app.name', 'Laravel'))">
                    @else
                        <h2 class="mb-0">@yield('meta_title', config('app.name', 'Laravel'))</h2>
                    @endif
                    {{-- <a href="#"><img src="images/logo.png"></a> --}}
                </div>
            </div>
            <div class="product-page-header-detail-2">
                <form class="product-page-header-form" action="{{ route('search') }}" method="GET">
                  <input type="text" placeholder="Search.." name="q">
                  <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <div class="product-page-header-detail-3">
                <div class="product-page-header-guide">
                    <a href="#">ADD A GUIDE</a>
                </div>
                <div class="pro-respon-menu" onclick="showsec()">
                    <i class="fa fa-bars"></i>
                    
                </div>
                <div class="product-page-header-icons">
                    
                    <a href="#"><i class="fa fa-user"></i><i class="fa fa-caret-down"></i></a>
                    
                </div>
                
            </div>
        </div>
    </div>
</header>
<!-- mobile menu -->
<section class="pro-mobile-menu-sec" id="promoblmenu">
    <div class="pro-menu-page">
        <div class="pro-mobile-menu-data">
            <div class="pro-mobile-menu-logo">
                <a href="#"><img src="images/logo.png"></a>
            </div>
            <div class="pro-mobile-menu-srch-2">
                <form class="product-page-header-form-mobile" action="{{ route('search') }}" method="GET">
                  <input type="text" placeholder="Search.." name="q">
                  <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <div class="product-page-header-guide-mobile">
                <a href="#">ADD A GUIDE</a>
            </div>
            
        </div>
        <div class="pro-mobile-menu-cross" onclick="hidesec()">
            <i class="fa fa-times"></i>
        </div>

    </div>
</section>