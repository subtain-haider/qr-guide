<footer class="mob-product-3-footer-sec">
    <div class="mob-product-3-footer-sec-page">
        <div class="mob-product-3-footer-data">

            <div class="mob-product-3-footer-detail">
                <div class="mob-product-3-footer-detail-head">
                    <h4>Our Address</h4>
                </div>
                <div class="mob-product-3-footer-detail-para">
                    <p>Hong Kong Queens Road 222 joslin street</p>
                    <p>Tel: 86-1353087909</p>
                    <p>Email: info@myqrguide.com</p>
                </div>
            </div>

            <div class="mob-product-3-footer-detail">
                <div class="mob-product-3-footer-detail-head">
                    <h4>Our Pages</h4>
                </div>
                <div class="mob-product-3-footer-detail-ul">
                    <ul>
                        <li><a href="#">Add a guide</a></li>
                        <li><a href="{{url('/faq-page')}}">FAQS</a></li>
                        <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                        <li><a href="{{url('/about-us')}}">About Us</a></li>
                        <li><a href="{{ url('/terms-and-conditions') }}">Terms and Conditions</a></li>
                    </ul>
                </div>
            </div>

            <div class="mob-product-3-footer-detail">
                <div class="mob-product-3-footer-detail-ul">
                    <ul>
                        <li><a href="#">My dashboard</a></li>
                        <li><a href="#">My Wishlist</a></li>
                        <li><a href="{{ route('cart') }}">My Cart</a></li>
                        <li><a href="#">My Warranty</a></li>
                        <li><a href="#">My reports</a></li>
                        <li><a href="#">My qr guides</a></li>
                    </ul>
                </div>
            </div>

            <div class="mob-product-3-footer-detail">
                <div class="mob-product-3-footer-detail-icon-data">
                    <div class="mob-product-3-footer-detail-icon">
                        <p>Our social pages</p>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>