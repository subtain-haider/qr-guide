@extends('frontend.layouts.new_app_1')

<meta property="og:title" content="{{ $product->meta_title }}">
<meta property="og:description" content="{{ $product->meta_description }}">
<meta property="og:image:secure_url" content="{{ url('/').'/'.$product->meta_img }}">
<meta property="og:image" content="{{ url('/').'/'.$product->meta_img }}">
@section('content')
<style type="text/css">
	/*-- --------------------------------------single-product-page---------------------------------- */
lable:focus{
	border: 2px solid blue;
}
.singlebrd-label-color{
			border:2px solid red;
		}
		.singlebrd-label-color2{
			border:2px solid red;
		}
.single-pro-page-sec{
	display: flex;
	justify-content: center;
	padding: 30px 0px;
	overflow: hidden;
}
.single-pro-page-wdth{
	width: 90%;
}
.single-pro-ban-data{
	display: flex;
	justify-content: space-between;
}
.single-pro-ban-detail-1{
	width: 34%;
}
.single-pro-ban-detail-2{
	width: 64%;
}
.single-pro-ban-detail-1 img{
	width: 100%;
}
input[type="number"] {
  -webkit-appearance: textfield;
    -moz-appearance: textfield;
          appearance: textfield;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none;
}

.number-input button {
  -webkit-appearance: none;
  background-color: transparent;
  border: none;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  margin: 0;
  position: relative;
}
.number-input button:before,
.number-input button:after {
  display: inline-block;
  position: absolute;
  content: '';
  height: 2px;
  transform: translate(-50%, -50%);
  margin-top: -6px;
}
.number-input button.plus:after {
  transform: translate(-50%, -50%) rotate(90deg);
}
.number-input input[type=number] {
  text-align: center;
}


.md-number-input.number-input {
 /* border: 2px solid #ddd;*/
  width: 9rem;
}
.md-number-input.number-input button {
  outline: none;
  width: 2rem;
  height: 2rem;
  padding-top: .8rem;
  background-color: rgba(0,0,0,0.15);
}

.md-number-input.number-input button.plus {
  padding-left: 2px;
}
.md-number-input.number-input button:before,
.md-number-input.number-input button:after {
  width: 1rem;
  background-color: #212121;
}
.md-number-input.number-input input[type=number] {
  max-width: 4rem;
  border:   solid #ddd;
  border-width: 0 2px;
  font-size: 18px;
  height: 2rem;
  outline: none;
}
.carousel {
	position: relative;
}
.carousel-item img {
	object-fit: cover;
}

#carousel-thumbs {
	background: #f0f0f0;
	padding: 0 50px;
}
#carousel-thumbs img:hover {
	opacity: 100%;
}

#carousel-thumbs img {
	opacity: 80%;
	border: 3px solid transparent;
	cursor: pointer;
}
#carousel-thumbs .selected img {
	opacity: 100%;
}

.carousel-control-prev,
.carousel-control-next {
	width: 50px;
}

.carousel-fullscreen-icon {
	position: absolute;
	top: 1rem;
	left: 1rem;
	width: 1.75rem;
	height: 1.75rem;
	z-index: 4;
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgba(255,255,255,.80)'  viewBox='0 0 16 16'%3E%3Cpath d='M1.5 1a.5.5 0 0 0-.5.5v4a.5.5 0 0 1-1 0v-4A1.5 1.5 0 0 1 1.5 0h4a.5.5 0 0 1 0 1h-4zM10 .5a.5.5 0 0 1 .5-.5h4A1.5 1.5 0 0 1 16 1.5v4a.5.5 0 0 1-1 0v-4a.5.5 0 0 0-.5-.5h-4a.5.5 0 0 1-.5-.5zM.5 10a.5.5 0 0 1 .5.5v4a.5.5 0 0 0 .5.5h4a.5.5 0 0 1 0 1h-4A1.5 1.5 0 0 1 0 14.5v-4a.5.5 0 0 1 .5-.5zm15 0a.5.5 0 0 1 .5.5v4a1.5 1.5 0 0 1-1.5 1.5h-4a.5.5 0 0 1 0-1h4a.5.5 0 0 0 .5-.5v-4a.5.5 0 0 1 .5-.5z' /%3E%3C/svg%3E");
}

.carousel-fullscreen-icon:hover {
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgb(255,255,255)' viewBox='0 0 16 16'%3E%3Cpath d='M1.5 1a.5.5 0 0 0-.5.5v4a.5.5 0 0 1-1 0v-4A1.5 1.5 0 0 1 1.5 0h4a.5.5 0 0 1 0 1h-4zM10 .5a.5.5 0 0 1 .5-.5h4A1.5 1.5 0 0 1 16 1.5v4a.5.5 0 0 1-1 0v-4a.5.5 0 0 0-.5-.5h-4a.5.5 0 0 1-.5-.5zM.5 10a.5.5 0 0 1 .5.5v4a.5.5 0 0 0 .5.5h4a.5.5 0 0 1 0 1h-4A1.5 1.5 0 0 1 0 14.5v-4a.5.5 0 0 1 .5-.5zm15 0a.5.5 0 0 1 .5.5v4a1.5 1.5 0 0 1-1.5 1.5h-4a.5.5 0 0 1 0-1h4a.5.5 0 0 0 .5-.5v-4a.5.5 0 0 1 .5-.5z' /%3E%3C/svg%3E");
}

.pause .carousel-pause-icon {
	position: absolute;
	top: 3.75rem;
	left: 1rem;
	width: 1.75rem;
	height: 1.75rem;
	z-index: 4;
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgba(255,255,255,.80)'  viewBox='0 0 16 16'%3E%3Cpath d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.25 5C5.56 5 5 5.56 5 6.25v3.5a1.25 1.25 0 1 0 2.5 0v-3.5C7.5 5.56 6.94 5 6.25 5zm3.5 0c-.69 0-1.25.56-1.25 1.25v3.5a1.25 1.25 0 1 0 2.5 0v-3.5C11 5.56 10.44 5 9.75 5z' /%3E%3C/svg%3E");
}
.pause .carousel-pause-icon:hover {
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgb(255,255,255)'  viewBox='0 0 16 16'%3E%3Cpath d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.25 5C5.56 5 5 5.56 5 6.25v3.5a1.25 1.25 0 1 0 2.5 0v-3.5C7.5 5.56 6.94 5 6.25 5zm3.5 0c-.69 0-1.25.56-1.25 1.25v3.5a1.25 1.25 0 1 0 2.5 0v-3.5C11 5.56 10.44 5 9.75 5z' /%3E%3C/svg%3E");
}

.play .carousel-pause-icon {
	position: absolute;
	top: 3.75rem;
	left: 1rem;
	width: 1.75rem;
	height: 1.75rem;
	z-index: 4;
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgba(255,255,255,.80)'  viewBox='0 0 16 16'%3E%3Cpath d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5z' /%3E%3C/svg%3E");
}

.play .carousel-pause-icon:hover {
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgb(255,255,255)'  viewBox='0 0 16 16'%3E%3Cpath d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5z' /%3E%3C/svg%3E");
}

#carousel-thumbs .carousel-control-prev-icon {
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgba(0,0,0,.60)' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E") !important;
}

#carousel-thumbs .carousel-control-next-icon {
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%60000' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E") !important;
}

.modal-content {
	border-radius: 0;
	background-color: transparent;
	border: none;
}
#lightbox-container-image img {
	width: auto;
	max-height: 520px;
}
.crsl-img-scl img:hover{
	transform: scale(1.1);
	cursor: pointer;
}
.crsl-img-scl img{
	transition: 0.5s;
	}

@media not all and (min-resolution:.001dpcm)
{ @supports (-webkit-appearance:none) and (stroke-color:transparent) {
  .number-input.md-number-input.safari_only button:before, 
  .number-input.md-number-input.safari_only button:after {
    margin-top: -.6rem;
  }
}}

.single-pro-ban-head h2{
	margin-bottom: 2px;
	font-size: 28px;
	font-family: 'Play';
	font-weight: 600;
	
}
.single-pro-ban-icon{
	display: flex;
	flex-direction: row;
	justify-content: flex-end;
	margin-top: 30px;
}
.single-pro-ban-icon i{
	margin-left: 20px;
	color: #777;
	font-size: 20px;
	cursor: pointer;
}
.single-pro-ban-quant{
	display: flex;
	align-items: center;
	margin-top: 30px;
}
.single-pro-ban-des-hed h5{
	margin-bottom: 0px;
	font-size: 20px;
	font-family: 'Play';
}
.single-pro-ban-color{
	display: flex;
	align-items: center;
	margin-top: 20px;
}
.single-pro-ban-des-hed{
	width: 20%;
}
.single-pro-ban-des-hed-2{
	width: 17%;
}
.single-pro-ban-color-detail button{
	width: 40px;
	height: 40px;
	border: none;
}

.single-pro-ban-color-black{
	background-color: #000;
}
.single-pro-ban-color-red{
	background-color: #FF0000;
}
.single-pro-ban-color-sky{
	background-color: #198EA2;
}
.single-pro-ban-size{
	display: flex;
	align-items: center;
	margin-top: 20px;
}
.single-pro-ban-size-detail button{
	width: 40px;
	height: 40px;
	border: none;
}

.single-pro-ban-crt-btns{
	display: flex;
	justify-content: space-between;
	margin-top: 30px;
}
.single-pro-ban-crt-btns button, .single-pro-ban-crt-btns a{
	width: 48%;
	padding: 15px 0px;
	font-size: 18px;
	font-weight: 600;
	border: none;
	outline: none;
	color: #fff;
}
.single-pro-ban-crt-btns button:focus , .single-pro-ban-crt-btns a:focus{
	outline: none;
}
.single-pro-ban-crt-btns .single-pro-ban-buynow{
	background-image: linear-gradient(to right, #F7AD66 , #F7D166);
    text-align: center;
}
.single-pro-ban-crt-btns .single-pro-ban-addto{
	background-image: linear-gradient(to right, #F85C62 , #F37F67);

}

.carousel {
	position: relative;
}
.carousel-item img {
	object-fit: cover;
}

#carousel-thumbs {
	background: #f0f0f0;
	padding: 0 50px;
}
#carousel-thumbs img:hover {
	opacity: 100%;
}

#carousel-thumbs img {
	opacity: 80%;
	border: 3px solid transparent;
	cursor: pointer;
}
#carousel-thumbs .selected img {
	opacity: 100%;
}

.carousel-control-prev,
.carousel-control-next {
	width: 50px;
}

.carousel-fullscreen-icon {
	position: absolute;
	top: 1rem;
	left: 1rem;
	width: 1.75rem;
	height: 1.75rem;
	z-index: 4;
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgba(255,255,255,.80)'  viewBox='0 0 16 16'%3E%3Cpath d='M1.5 1a.5.5 0 0 0-.5.5v4a.5.5 0 0 1-1 0v-4A1.5 1.5 0 0 1 1.5 0h4a.5.5 0 0 1 0 1h-4zM10 .5a.5.5 0 0 1 .5-.5h4A1.5 1.5 0 0 1 16 1.5v4a.5.5 0 0 1-1 0v-4a.5.5 0 0 0-.5-.5h-4a.5.5 0 0 1-.5-.5zM.5 10a.5.5 0 0 1 .5.5v4a.5.5 0 0 0 .5.5h4a.5.5 0 0 1 0 1h-4A1.5 1.5 0 0 1 0 14.5v-4a.5.5 0 0 1 .5-.5zm15 0a.5.5 0 0 1 .5.5v4a1.5 1.5 0 0 1-1.5 1.5h-4a.5.5 0 0 1 0-1h4a.5.5 0 0 0 .5-.5v-4a.5.5 0 0 1 .5-.5z' /%3E%3C/svg%3E");
}

.carousel-fullscreen-icon:hover {
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgb(255,255,255)' viewBox='0 0 16 16'%3E%3Cpath d='M1.5 1a.5.5 0 0 0-.5.5v4a.5.5 0 0 1-1 0v-4A1.5 1.5 0 0 1 1.5 0h4a.5.5 0 0 1 0 1h-4zM10 .5a.5.5 0 0 1 .5-.5h4A1.5 1.5 0 0 1 16 1.5v4a.5.5 0 0 1-1 0v-4a.5.5 0 0 0-.5-.5h-4a.5.5 0 0 1-.5-.5zM.5 10a.5.5 0 0 1 .5.5v4a.5.5 0 0 0 .5.5h4a.5.5 0 0 1 0 1h-4A1.5 1.5 0 0 1 0 14.5v-4a.5.5 0 0 1 .5-.5zm15 0a.5.5 0 0 1 .5.5v4a1.5 1.5 0 0 1-1.5 1.5h-4a.5.5 0 0 1 0-1h4a.5.5 0 0 0 .5-.5v-4a.5.5 0 0 1 .5-.5z' /%3E%3C/svg%3E");
}

.pause .carousel-pause-icon {
	position: absolute;
	top: 3.75rem;
	left: 1rem;
	width: 1.75rem;
	height: 1.75rem;
	z-index: 4;
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgba(255,255,255,.80)'  viewBox='0 0 16 16'%3E%3Cpath d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.25 5C5.56 5 5 5.56 5 6.25v3.5a1.25 1.25 0 1 0 2.5 0v-3.5C7.5 5.56 6.94 5 6.25 5zm3.5 0c-.69 0-1.25.56-1.25 1.25v3.5a1.25 1.25 0 1 0 2.5 0v-3.5C11 5.56 10.44 5 9.75 5z' /%3E%3C/svg%3E");
}
.pause .carousel-pause-icon:hover {
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgb(255,255,255)'  viewBox='0 0 16 16'%3E%3Cpath d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.25 5C5.56 5 5 5.56 5 6.25v3.5a1.25 1.25 0 1 0 2.5 0v-3.5C7.5 5.56 6.94 5 6.25 5zm3.5 0c-.69 0-1.25.56-1.25 1.25v3.5a1.25 1.25 0 1 0 2.5 0v-3.5C11 5.56 10.44 5 9.75 5z' /%3E%3C/svg%3E");
}

.play .carousel-pause-icon {
	position: absolute;
	top: 3.75rem;
	left: 1rem;
	width: 1.75rem;
	height: 1.75rem;
	z-index: 4;
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgba(255,255,255,.80)'  viewBox='0 0 16 16'%3E%3Cpath d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5z' /%3E%3C/svg%3E");
}

.play .carousel-pause-icon:hover {
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgb(255,255,255)'  viewBox='0 0 16 16'%3E%3Cpath d='M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5z' /%3E%3C/svg%3E");
}

#carousel-thumbs .carousel-control-prev-icon {
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='rgba(0,0,0,.60)' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E") !important;
}

#carousel-thumbs .carousel-control-next-icon {
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%60000' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E") !important;
}

.modal-content {
	border-radius: 0;
	background-color: transparent;
	border: none;
}
#lightbox-container-image img {
	width: auto;
	max-height: 520px;
}
.single-pro-img-slide-outer{
	height: 370px!important;
	overflow: hidden;
}
.single-pro-img-slide{
	height: 100%!important;
}
.single-pro-img-slide-outer-2{
	height: 75px;
}
.single-pro-img-slide-outer-2 img{
	height: 100%;
}
/*product-detail*/
.single-pro-des-sec{
	display: flex;
	justify-content: center;
	padding: 30px 0px;
}

.singlenewpropriwrapper .active { 
	color: #1BB3CE;
}
.singlenewpropritabs{
	display: flex;
	
}

.singlenewprotab_item { 
	display: none; 

}
.singlenewpropritabs-btn{
	margin-top: 20px;
	background-color: transparent;
	border: none;
	border-bottom: 2px solid #000;
	cursor: pointer;
	color: #000;
	outline: none;
	font-size: 18px;
	font-weight: 600;
	font-family: 'Play';
	margin-right: 40px;


}
.singlenewpropritabs-btn:focus{
	color: #1BB3CE;
	outline: none;
	border-color: #1BB3CE; 
}
.singlenewpropritabs .propritab{
	cursor: pointer;
}
.singlenewprotab_item:first-child { 
	display: block; 
}
.single-pro-des-all{
	margin-top: 20px;
}
.single-pro-des-all .single-pro-des-para{
	font-size: 16px;
	font-family: 'Play';
}
.single-pro-speci-detail{
	width: 50%;
}
.single-pro-speci-data{
	display: flex;
	justify-content: space-between;
	align-items: center;
	border-bottom: 1px solid #ccc;
	padding: 15px 0px;
}
.single-pro-speci-data h5{
	margin-bottom: 0px;
	font-size: 16px;
	font-weight: bold;
	font-family: 'Play';
}
.single-pro-speci-data p{
	margin-bottom: 0px;
	font-family: 'Play';
	font-size: 16px;
	text-align: left;
}
@media only screen and (max-width: 985px) {
	.single-pro-ban-data{
		flex-direction: column;
	}
	.single-pro-ban-detail-1{
		width: 100%;
	}
	.single-pro-ban-detail-2{
		width: 100%;
	}
}

@media only screen and (max-width: 750px) {
	.single-pro-speci-detail{
		width: 100%;
	}
	.single-pro-ban-quant{
		flex-direction: column;
		align-items: unset;

	}
	.single-pro-ban-color{
		flex-direction: column;
		align-items: unset;
	}
	.single-pro-ban-size{
		flex-direction: column;
		align-items: unset;
	}
	.single-pro-ban-des-hed{
		width: 100%;
		margin-bottom: 15px;
	}
}
@media only screen and (max-width: 497px) {
	.singlenewpropritabs-btn{
		font-size: 14px; 
	}
}
@media only screen and (max-width: 480px) {
	.single-pro-ban-head h2{
		font-size: 24px;
	}
	.single-pro-ban-crt-btns{
		flex-direction: column;
	}
	.single-pro-ban-crt-btns button , .single-pro-ban-crt-btns a{
		width: 100%;
		margin-bottom: 15px;
	}
}
@media only screen and (max-width: 385px) {
	.single-pro-img-slide-outer-2{
		height: 50px;
	}
}
.minipro-page-informa-content-btns-social-icons a img {
    width:50px;
    margin-top: 10px;
}
</style>
<script>	
$(function () {

       // tabs

    $(".singlenewpropriwrapper .singlenewpropritab").click(function() {
	$(".singlenewpropriwrapper .singlenewtabpropri").removeClass("active").eq($(this).index()).addClass("singlenewpropractive");
	$(".singlenewprotab_item").hide().eq($(this).index()).fadeIn()
	}).eq(0).addClass("active");

});



</script>
	<!-- Single Product Page -->
<section class="single-pro-page-sec">
	<div class="single-pro-page-wdth">
		<div class="single-pro-ban-data">
			<div class="single-pro-ban-detail-1">
				<!-- <img src="images/slider2.jpg"> -->
				<div id="wrap" class="container">
					<div class="row">
						<div class="col-12">
                            <div id="carousel" class="carousel fade-in gallery" data-ride="carousel">
				<div class="carousel-inner">
				    @foreach(json_decode($product->photos) as $key => $photos )
						
						<div class="carousel-item @if ($loop->first) active @endif crsl-img-scl" data-.menuwrapper-number="{{$key}}" data-toggle="lightbox" data-gallery="gallery" data-remote="{{ asset($photos) }}">
						<img src="{{ asset($photos) }}" class="d-block w-100 " alt="...">
					</div>
					@endforeach
					
				</div>
				<!--<a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">-->
				<!--	<span class="carousel-control-prev-icon" aria-hidden="true"></span>-->
				<!--	<span class="sr-only">Previous</span>-->
				<!--</a>-->
				<!--<a class="carousel-control-next" href="#carousel" role="button" data-slide="next">-->
				<!--	<span class="carousel-control-next-icon" aria-hidden="true"></span>-->
				<!--	<span class="sr-only">Next</span>-->
				<!--</a>-->
				<a class="carousel-fullscreen" href="#carousel" role="button">
					<span class="carousel-fullscreen-icon" aria-hidden="true"></span>
					<span class="sr-only">Fullscreen</span>
				</a>
				<!--<a class="carousel-pause pause" href="#carousel" role="button">-->
				<!--	<span class="carousel-pause-icon" aria-hidden="true"></span>-->
				<!--	<span class="sr-only">Pause</span>-->
				<!--</a>-->
			</div>

			<!-- Carousel Navigatiom -->
			<div id="carousel-thumbs" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
				    @php $i=0; @endphp
				    
				    <div class="carousel-item active" data-slide-number="0">
						<div class="row mx-0">
						    @foreach(json_decode($product->photos) as $key => $photos )
							<div id="carousel-selector-{{ $i }}" class="thumb col-3 px-1 py-2 @if ($loop->first) selected @endif" data-target="#carousel" data-slide-to="{{ $i }}">
								<img src="{{  asset($photos) }}" class="img-fluid" alt="...">
							</div>
							@php $i++ @endphp
				   @endforeach
						</div>
					</div>
					
				</div>
				<a class="carousel-control-prev" href="#carousel-thumbs" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carousel-thumbs" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="single-pro-ban-detail-2">
				<form id="option-choice-form_{{ $product->id }}" action="{{ route('cart.addToCart', ['id' => $product->id]) }}" method="post" class=" d-lg-block" >
					 

					 <!--<form id="option-choice-form_{{ $product->id }}" class="d-none d-lg-block" method="GET">-->
                                            @csrf

                                            <input type="hidden" name="id" value="{{ $product->id }}">       

				<div class="single-pro-ban-head">
					<h2>{{ $product->name }}</h2>
					
				</div>

				<!-- <div class="single-pro-ban-icon">
					<i class="fa fa-share-alt"></i>
					<i class="fa fa-heart-o"></i>
				</div> -->

				<div class="single-pro-ban-quant">
					<div class="single-pro-ban-des-hed">
						<h5>Quantity</h5>
					</div>
					<div class="container px-0 mx-0">
						<div class="number-input md-number-input">
						  <button type="button" onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus"></button>
						  <input class="quantity" min="1" name="quantity" max="{{ $product->quantity }}" value="1" type="number">
						  <button type="button" onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></button>
						</div>    
					</div>
				</div>
    @if (!empty($product->colors)) 
    @if($product->colors != 'null')
				<div class="single-pro-ban-color">
					<div class="single-pro-ban-des-hed single-pro-ban-des-hed-2">
						<h5>Color</h5>
					</div>

					<div class="single-pro-ban-color-detail">

						
						 
                        
						@foreach(json_decode($product->colors) as $key => $value )

						 <label class="singlelabels-in" for="colorinput{{$key+1}}" style="background-color: {{ $value }}; height: 40px; width: 40px; border: 1px solid #444;" >
   					
   						</label>
   						 <input type="checkbox" id="colorinput{{$key+1}}" name="colors" value="{{ $value }}" style="display: none;">


   						   
						@endforeach
					
						
						
       					

					</div>
				</div>
@endif
@endif
@if (!empty($product->size)) 
	@if($product->size != 'null')
				<div class="single-pro-ban-size">
					<div class="single-pro-ban-des-hed single-pro-ban-des-hed-2">
						<h5>Size</h5>
					</div>
					<div class="single-pro-ban-size-detail">
					
						

					
						@foreach(json_decode($product->size) as $key => $value )

						<label class="singlelabels-in2" for="colorsize{{$key+1}}" style="background-color: #d9d9d957; text-align: center; padding: 10px; height: 40px; width: 40px; " >{{ $value }}

						</label>
   					 <input type="checkbox" id="colorsize{{$key+1}}" name="size" value="{{ $value }}" style="display: none;">
   						 
       					
						@endforeach
						
						
						
					</div>
				</div>
@endif
@endif
				<div class="single-pro-ban-size">
					<div class="single-pro-ban-des-hed single-pro-ban-des-hed-2">
						<h5>Price</h5>
					</div>
					<div class="single-pro-ban-size-detail">
						<h4>${{ $product->unit_price }}</h4>
					</div>
				</div>
                <div class="single-pro-ban-size">
					<div class="single-pro-ban-des-hed single-pro-ban-des-hed-2">
						<h5>Asin Code</h5>
					</div>
					<div class="single-pro-ban-size-detail">
						<p style="margin-bottom: 0;">{{ $product->asin }}</p>
					</div>
				</div>

				<div class="single-pro-ban-crt-btns">
				    
					@if($product->external_website == 'amazon')
				        <a href="http://www.amazon.com/dp/{{ $product->external_link }}/?tag=myqrguide-20" class="single-pro-ban-buynow">Buy Now</a>
				    @elseif($product->external_website == 'ebay')
				        <a href="https://www.ebay.com/itm/{{ $product->external_link }}/?customid=myqrguide&campid=5338839039" class="single-pro-ban-buynow">Buy Now </a>
				    @else
				        <button class="single-pro-ban-buynow" name="buynow" value="buynow">Buy Now</button>
				        <button class="single-pro-ban-addto" >{{__('Add to Cart')}}<i class="fa fa-cart-arrow-down"></i></button>
				    @endif
					
					<!--<div class="row">-->
					<!--    <div class="col-md-6 bg-dark">1</div>-->
					<!--    <div class="col-md-6 bg-dark">2</div>-->
					<!--</div>-->
					        							
					
				</div>
					<div class="minipro-page-informa-content-btns-social-icons">
        
        									<a class="fb-share-button" data-href="{{ url()->current() }}" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}" class="fb-xfbml-parse-ignore"><img src="{{asset('frontend/images/f2.png')}}"></a>

									<a target="_blank" class="twitter-share-button"
									  href="https://twitter.com/intent/tweet?text={{ url()->current() }}"
									  data-size="Default">
									<img src="{{asset('frontend/images/t.png')}}"></a>
        
        									<!-- <a href="#"><img src="{{asset('frontend/images/g2.png')}}"></a>
        
        									<a href="#"><img src="{{asset('frontend/images/f2.png')}}"></a>
        
        									<a href="#"><img src="{{asset('frontend/images/t.png')}}"></a> -->
        
        								</div>


			</form>

			</div>
		</div>
	</div>
</section>
<section class="single-pro-des-sec">
	<div class="single-pro-page-wdth">
		<div class="singlenewpropriwrapper">
	    	<div class="singlenewpropriwrapper-page">
			    <div class="singlenewpropritabs">
			        <span class="singlenewpropritab"><button class="singlenewpropritabs-btn">Product Description</button></span>
			        <span class="singlenewpropritab"><button class="singlenewpropritabs-btn">Product Specification</button></span>
			        <span class="singlenewpropritab"><button class="singlenewpropritabs-btn">Video</button></span>
			    </div>
			    <div class="singlenewtab_content">
			        
			        <div class="singlenewprotab_item">
			        	<div class="single-pro-des-all">
				        	<p class="single-pro-des-para">
				        		{!! $product->description !!}
				        	</p>
			        	</div>
						
			        </div>
			        <div class="singlenewprotab_item">
						<div class="single-pro-des-all">
							<div class="single-pro-speci-detail">
								<!-- <div class="single-pro-speci-data">
									<h5>Status</h5>
									<p>Available</p>
								</div>
								<div class="single-pro-speci-data">
									<h5>Dimensions</h5>
									<p>14.6 * 70.9 * 7.7 mm</p>
								</div>
								<div class="single-pro-speci-data">
									<h5>Weight</h5>
									<p>174 g</p>
								</div>
								<div class="single-pro-speci-data">
									<h5>Type</h5>
									<p>E-Book</p>
								</div>
								<div class="single-pro-speci-data">
									<h5>Size</h5>
									<p>8.5inches, 84.4cm</p>
								</div> -->
								{!! $product->spacification !!}
							</div>
						</div>
			        </div>

			         <div class="singlenewprotab_item">
			        	<div class="single-pro-des-all">
			        	    	<?php preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $product->video_link, $match);
							?>
							@if ($product->video_provider == 'youtube' && $product->video_link != null)

								<iframe class="d-block w-100" width="100%" height="409" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $match[1] }}"></iframe>

							@elseif ($product->video_provider == 'dailymotion' && $product->video_link != null)
								<iframe class="embed-responsive-item d-block w-100" src="https://www.dailymotion.com/embed/video/{{ explode('video/', $product->video_link)[1] }}"></iframe>

							@elseif ($product->video_provider == 'vimeo' && $product->video_link != null)

								<iframe class="d-block w-100" src="https://player.vimeo.com/video/{{ explode('vimeo.com/', $product->video_link)[1] }}" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

							@elseif (!empty($product->video_temp))

								<video  style="w-100">

									<source src="{{$product->video_link}}" type="video/mp4">

									Your browser does not support HTML video.

								</video>

							@endif
			        	</div>
						
			        </div>
			   
			    </div>
			</div>
		</div>
	</div>
</section>
@endsection
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js'></script>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/js/bootstrap.min.js'></script>
<script src='https://kit.fontawesome.com/ad153db3f4.js'></script>
<script type="text/javascript">
function linkclick(val) {
        window.location.href = val;
    }
$(document).ready(function(){
		   $(".singlelabels-in").click(function(){
		   $(".singlebrd-label-color").removeClass("singlebrd-label-color");
		   $(this).addClass("singlebrd-label-color");
		  }); 
  });
$(document).ready(function(){
		   $(".singlelabels-in2").click(function(){
		   $(".singlebrd-label-color2").removeClass("singlebrd-label-color2");
		   $(this).addClass("singlebrd-label-color2");
		  });   
	  });
    function addToCart(id){
        // $('.c-preloader').show();
        $.ajax({
            type:"POST",
            url:'{{ route('cart.addToCart') }}',
            data:$('#'+id).serialize(),
            success: function(data){
                swal('Product successfully add to cart!');
                //    $('.c-preloader').hide();
                //    $('#modal-size').removeClass('modal-lg');
                //    $('#addToCart-modal-body').html(data);
                //    updateNavCart();
            }
       });
    }

$("[id^=carousel-thumbs]").carousel({
	interval: false
});

/** Pause/Play Button **/
$(".carousel-pause").click(function () {
	var id = $(this).attr("href");
	if ($(this).hasClass("pause")) {
		$(this).removeClass("pause").toggleClass("play");
		$(this).children(".sr-only").text("Play");
		$(id).carousel("pause");
	} else {
		$(this).removeClass("play").toggleClass("pause");
		$(this).children(".sr-only").text("Pause");
		$(id).carousel("cycle");
	}
	$(id).carousel;
});

/** Fullscreen Buttun **/
$(".carousel-fullscreen").click(function () {
	var id = $(this).attr("href");
	$(id).find(".active").ekkoLightbox({
		type: "image"
	});
});

if ($("[id^=carousel-thumbs] .carousel-item").length < 2) {
	$("#carousel-thumbs [class^=carousel-control-]").remove();
	$("#carousel-thumbs").css("padding", "0 5px");
}

$("#carousel").on("slide.bs.carousel", function (e) {
	var id = parseInt($(e.relatedTarget).attr("data-slide-number"));
	var thumbNum = parseInt(
		$("[id=carousel-selector-" + id + "]")
			.parent()
			.parent()
			.attr("data-slide-number")
	);
	$("[id^=carousel-selector-]").removeClass("selected");
	$("[id=carousel-selector-" + id + "]").addClass("selected");
	$("#carousel-thumbs").carousel(thumbNum);
});


</script>
</script>