@extends('frontend.layouts.app')

@section('content')

    <section class="payment-page-method-sec">
        <div class="payment-page-wdth">
            <div class="payment-page-method-sec-head">
                <h2>{{__('SELECT PAYMENT METHOD')}}</h2>
            </div>
            <div class="payment-page-method-data">
                <div class="paymentmethodpropritabs">
                    <span class="paymentmethodpropritab"><button class="paymentmethodpropritabs-btn">{{__('MY CART')}}</button></span>
                    <span class="paymentmethodpropritab"><button class="paymentmethodpropritabs-btn">{{__('SHIPING INFO')}}</button></span>
                    <span class="paymentmethodpropritab"><button class="paymentmethodpropritabs-btn">{{__('PAYMENT METHOD')}}</button></span>
                </div>
                <div>
                    <div class="paymentmethodtab_content">
                        <div class="paymentmethodprotab_item">
                            <div class="payment-page-method-tab-1">
                                <div class="payment-page-view-cart-head">
                                    <h2>{{__('VIEW CART')}}</h2>
                                </div>
                                <p class="payment-page-numberitems">{{__('number of items')}} [@if(Session::has('cart')) {{ count(Session::get('cart')) }} @else 0 @endif]</p>
                                <div class="items-table-cart">
                                    @if(Session::has('cart'))
                                        @php
                                            $total = 0;
                                        @endphp
                                        @foreach (Session::get('cart') as $key => $cartItem)
                                            @php
                                            $product = App\Models\Product::find($cartItem['id']);
                                            $slug = $product['slug'];
                                            $total = $total + $cartItem['price']*$cartItem['quantity'];
                                            @endphp
                                            <div class="cart-table-inner">
                                                <div class="cart-table-inner-1">
                                                    <div class="cart-tables-inners-1">
                                                        <div class="cart-tables-inners-1-s">
                                                            <img src="{{ asset($product['thumbnail_img']) }}">
                                                        </div>
                                                    </div>
                                                    <div class="cart-tables-inners-2">
                                                        <h4>{{ $product['name'] }}</h4>
                                                        <p>${{ $cartItem['price'] }}</p>
                                                        @if(isset($cartItem['colors']))
                                                        <p><label class="singlelabels-in2" for="colorsize{{$key+1}}" style="background-color: {{ $cartItem['colors'] }} ; text-align: center; padding: 10px; height: 20px; width: 20px; " >

						</label></p>
						@endif
						@if(isset($cartItem['size']))
						<p><label class="singlelabels-in2" for="colorsize{{$key+1}}" style="background-color: #d9d9d957; text-align: center;  height: 25px; width: 25px; " >{{ $cartItem['size'] }}</label></p>
						@endif
                                                    </div>
                                                </div>
                                                <div class="cart-table-inner-2">
                                                    <div>
                                                        <button class="cart-button-1"><a href="#" onclick="removeFromCartView(event, {{ $key }})">remove from cart</a></button>
                                                    </div>
                                                    <div>
                                                        <button class="cart-button-2"><a href="{{ url('single-product',$slug) }}">view product</a></button>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                        <div class="special-cart-total">
                                            <p>Your Total <span>${{ $total }}</span></p>
                                        </div>
                                    @endif

                                </div>

                                <div class="payment-page-proceed-btn">
                                    <button><a href="{{ route('checkout.shipping_info') }}" class="text-white">CONTINUE TO SHIPPING</a></button>
                                </div>

                                <div class="special-cart-bottom-s">
                                    <a href="{{ route('home') }}">Return to shop</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
    
    function removeFromCartView(e, key){
        e.preventDefault();
        var token = '<?php echo csrf_token() ?>';
        $.ajax({
           type:'POST',
           url:"{{ route('cart.removeFromCart') }}",
           data:{key:key,_token:token},
           success:function(data){
              location.reload();
           }
        });
        removeFromCart(key);
    }

    function updateQuantity(key, element){
        $.post('{{ route('cart.updateQuantity') }}', { _token:'{{ csrf_token() }}', key:key, quantity: element.value}, function(data){
            updateNavCart();
            $('#cart-summary').html(data);
        });
    }

    function showCheckoutModal(){
        $('#GuestCheckout').modal();
    }
    </script>
@endsection
