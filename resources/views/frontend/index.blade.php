@extends('frontend.layouts.new_app_1')

@section('content')
    <!-- banner -->
{{-- icon link chat --}}
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<div style="border-radius: 50%;bottom: 20px;right: 20px;position: fixed;z-index:99;" data-aos="fade-left" data-aos-duration="1000">
    <a href="{{'/user/register'}}" onclick="window.open(this.href,'newwindow','width=800,height=800'); return false;">
        <img src="{{asset('frontend/images/chat.png')}}" style="width: 80px" alt="">
    </a>
</div>
{{-- icon link chat --}}
    <section class="mob-product-3-banner-sec">
        <div class="mob-product-3-banner-sec-page">
{{-- <a href="{{'/user/register'}}" class="btn btn-danger">Chat</a> --}}
            <div class="mob-product-3-banner-data">
                <div class="mob-product-3-banner-head" data-aos="fade-right" data-aos-duration="1000">
                    <h2>{{__('WELCOME TO MY QR GUIDE')}}</h2>
                    <p>{{__('A TOOL THAT IMPROVES BRAND VISIBILITY AND BETTER CONNECTS PRODUCT MANUFACTURERS WITH CUSTOMERS')}}</p>
                </div>
                <div class="mob-product-3-banner-srch" data-aos="fade-left" data-aos-duration="1000">
                    <form action="{{ route('search') }}" id="search-form" method="GET" class="header-searchser">
                        <input type="text" name="gsearch" id="gsearch" placeholder="Search OR Enter SKU/ASIN/SERIAL" class="mob-product-3-banner-srch-bar">
                        <i class="fa fa-search" id="search-btn"></i>
                        <div class="mob-product-3-banner-srch-slct">
                            <select class="selectpicker" data-live-search="true" placeholder="Select">
                                <option value="" data-tokens="">{{__('CHOOSE CATEGORY')}}</option>
                                @foreach (\App\Models\Category::all() as $key => $category)
                                    <option value="{{ $category->id }}" data-tokens="{{ $category->id }}">{{ __($category->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="mob-product-3-banner-overlay"></div>
    </section>

    <!-- why chose us -->
    <section class="mob-product-3-whych-sec">
        <div class="mob-product-3-whych-sec-page">
            <div class="mob-product-3-whych-sec-head">
                <h3>{{__('WHY CHOOSE US')}}</h3>
                <p>{{__('SEE MORE FEATURES')}}</p>
            </div>
            <div class="mob-product-3-whych-data">

                @foreach($homedivs as $key => $homediv)


                <div class="mob-product-3-whych-daetail" data-aos="fade-down-right" data-aos-duration="1{{$key+50}}">
                    <div class="mob-product-3-whych-daetail-icon">
                        <i class="{{ $homediv->icon }}">
                           
                        </i>
                    </div>
                    <div class="mob-product-3-whych-daetail-icon-text">
                        <h4>{{ $homediv->heading }}</h4>
                    </div>
                    <div class="mob-product-3-whych-daetail-icon-para">
                        <p>{{ $homediv->content }}</p>
                    </div>
                </div>


                 @endforeach



            </div>
        </div>
    </section>

@endsection

@section('script')
<script type="text/javascript">
    	AOS.init({
  	once: true,
  });
</script>
@endsection