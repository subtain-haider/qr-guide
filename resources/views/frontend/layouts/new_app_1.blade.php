<!DOCTYPE html>
@if(\App\Models\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
    <html dir="rtl">
@else
    <html>
@endif
<head>

@php
    $seosetting = \App\Models\SeoSetting::first();
@endphp

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<meta name="description" content="@yield('meta_description', $seosetting->description)" />
<meta name="keywords" content="@yield('meta_keywords', $seosetting->keyword)">
<meta name="author" content="{{ $seosetting->author }}">
<meta name="sitemap_link" content="{{ $seosetting->sitemap_link }}">
@yield('meta')
<!-- Favicon -->
<link name="favicon" type="image/x-icon" href="{{ asset(\App\Models\GeneralSetting::first()->favicon) }}" rel="shortcut icon" />

<title>@yield('meta_title', config('app.name', 'Laravel'))</title>

<!-- Fonts -->
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/qr.css?42') }}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />

<link rel="stylesheet" href="//www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    <!-- Fonts -->
    <script src="{{ asset('js/spartan-multi-image-picker-min.js') }}"></script>

    <!-- Font Awesome -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>


    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js' ></script>
    <script  src="./script.js" defer></script>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css'>


<!--Select2 [ OPTIONAL ]-->
<link href="{{ asset('plugins/select2/css/select2.min.css')}}" rel="stylesheet">

<!--Chosen [ OPTIONAL ]-->
{{-- <link href="{{ asset('plugins/chosen/chosen.min.css')}}" rel="stylesheet"> --}}

<!--Bootstrap Tags Input [ OPTIONAL ]-->
<link href="{{ asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet">

<!--Summernote [ OPTIONAL ]-->
<link href="{{ asset('css/jodit.min.css') }}" rel="stylesheet">

<!--Spectrum Stylesheet [ REQUIRED ]-->
<link href="{{ asset('css/spectrum.css')}}" rel="stylesheet">
    <!--<link href="{{ asset('chat/css/app.css') }}" rel="stylesheet">-->
    <!--<link href="{{ asset('chat/css/main.css') }}" rel="stylesheet">-->
<link type="text/css" href="{{ asset('frontend/css/sweetalert2.min.css') }}" rel="stylesheet">

<script src="//cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="//stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.1.1/Chart.min.js"></script>

<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>


<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script src="{{ asset('frontend/js/editor.js') }}" type="text/javascript"></script>

<!--Select2 [ OPTIONAL ]-->
<script src="{{ asset('plugins/select2/js/select2.min.js')}}"></script>

<!--Summernote [ OPTIONAL ]-->
<script src="{{ asset('js/jodit.min.js') }}"></script>

<!--Bootstrap Tags Input [ OPTIONAL ]-->
<script src="{{ asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>

<!--Bootstrap Validator [ OPTIONAL ]-->
<script src="{{ asset('plugins/bootstrap-validator/bootstrapValidator.min.js') }}"></script>

<!--Bootstrap Wizard [ OPTIONAL ]-->
<script src="{{ asset('plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>

<!--Bootstrap Datepicker [ OPTIONAL ]-->
<script src="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<!--Form Component [ SAMPLE ]-->
<script src="{{asset('js/demo/form-wizard.js')}}"></script>

<!--Spectrum JavaScript [ REQUIRED ]-->
<script src="{{ asset('js/spectrum.js')}}"></script>
<script src="{{ asset('js/spartan-multi-image-picker-min.js') }}"></script>
<!--Spartan Image JavaScript [ REQUIRED ]-->

<!-- <script src="{{ asset('frontend/js/these-pages.js') }}"></script> -->


<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('frontend/js/sweetalert2.min.js') }}"></script>

<!-- Begin Inspectlet Asynchronous Code -->
<script type="text/javascript">
(function() {
window.__insp = window.__insp || [];
__insp.push(['wid', 938189323]);
var ldinsp = function(){
if(typeof window.__inspld != "undefined") return; window.__inspld = 1; var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js?wid=938189323&r=' + Math.floor(new Date().getTime()/3600000); var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); };
setTimeout(ldinsp, 0);
})();


    $(document).ready(function(){
         $('.activebtn2').removeClass('activebtn2');
        	$('.activebtn').on('click', function(){
		  
		        $('.activebtn2').removeClass('activebtn2');
		     $(this).addClass('activebtn2');
		   });
    });
    
</script>
<!-- End Inspectlet Asynchronous Code -->

</head>
<body>


<!-- MAIN WRAPPER -->
<div class="body-wrap shop-default shop-cards shop-tech white-bg">

    <!-- Header -->
    @include('frontend.inc.header_new_1')
    
    @yield('content')

    {{-- @include('frontend.partials.modal') --}}

    <!-- Header -->
    @include('frontend.inc.footer')
    
    @include('frontend.partials.modal')
</div><!-- END: body-wrap -->

<script type="text/javascript">
    function showFrontendAlert(type, message){
        swal({
            position: 'top-end',
            type: type,
            title: message,
            showConfirmButton: false,
            timer: 1500
        });
    }
</script>

@foreach (session('flash_notification', collect())->toArray() as $message)
    <script type="text/javascript">
        showFrontendAlert('{{ $message['level'] }}', '{{ $message['message'] }}');
    </script>
@endforeach
<script type="text/javascript">
    $(document).ready(function(){


        if($('.editor').length > 0){
            $('.editor').each(function(el){
                var editor = new Jodit(this, {
                "uploader": {
                    "insertImageAsBase64URI": true
                }
                });
            });
        }

        $( ".dashboard-recover-two-sides-left-links li" ).click(function() {

            $(this).children(".dashboard-recover-two-sides-left-sub-links").toggle();

        });

    });
    
    // three dots in table
    if($('.dashboard-recover-purchase-history-data-table').length > 0){
        document.querySelector('.dashboard-recover-purchase-history-data-table').onclick = ({
            target
        }) => {
            if (!target.classList.contains('dashboard-recover-more')) return
            document.querySelectorAll('.dashboard-recover-dropout.active').forEach(
                (d) => d !== target.parentElement && d.classList.remove('active')
            )
            target.parentElement.classList.toggle('active')
        }
    }

    // menu responsive
    var dashrecovermenu = document.getElementById("dashrecovermenu");
    function showdashrecover(){
        dashrecovermenu.style.left = "0%";
        dashrecovermenu.style.display = "block";
    }
    function hidedashrecover(){
        dashrecovermenu.style.left = "-150%";
    }

    // sublinks responsive

    var dashrecoversublinks = document.getElementById("dashrecoversublinks");
    function showdashrecoversublinks(){
        dashrecoversublinks.style.left = "0%";
        dashrecoversublinks.style.display = "block";
    }
    function hidedashrecoversublinks(){
        dashrecoversublinks.style.left = "-150%";
    }
    
    var mainpagres = document.getElementById("mainpagres");
    function mainresshow(){
        mainpagres.style.left = "0%";
        mainpagres.style.display = "flex";
    }
    function mainreshide(){
        mainpagres.style.left = "-150%";
    }
    
    var form = document.getElementById("search-form");

    if($('#search-btn').length > 0 || event.keyCode === 13){
        document.getElementById("search-btn").addEventListener("click", function () {
            form.submit();
        });
    }

 
form.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("search-btn").click();
    }
});

    
    $(document).on("focus","input[name=gsearch]",function(){
        $('.mob-product-3-banner-srch-slct .bootstrap-select').addClass("bootstrap-select-responsive");
        // $('.mob-product-3-banner-srch-slct .bootstrap-select-responsive').removeClass("bootstrap-select");
    });
    
    $(document).on("focusout","input[name=gsearch]",function(){
        // $('.mob-product-3-banner-srch-slct .bootstrap-select-responsive').addClass("bootstrap-select");
        $('.mob-product-3-banner-srch-slct .bootstrap-select').removeClass("bootstrap-select-responsive");
    });

    function addToCart(id){
        // $('.c-preloader').show();
        $.ajax({
            type:"POST",
            url:'{{ route('cart.addToCart') }}',
            data:$('#'+id).serialize(),
            success: function(data){
                alert('Product successfully add to cart!');
                //    $('.c-preloader').hide();
                //    $('#modal-size').removeClass('modal-lg');
                //    $('#addToCart-modal-body').html(data);
                //    updateNavCart();
            }
       });
    }


 


</script>

@yield('script')
</body>
</html>
