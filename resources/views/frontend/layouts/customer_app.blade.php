<!DOCTYPE html>
<html>
<head>
	<title>Dashboard Two</title>
	 <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, intial-scale=1.0">
	<!--<link rel="stylesheet" type="text/css" href="style.css">-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.1.1/Chart.min.js'></script>
	<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css'>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<style>
    .d-button{
    border-radius: 50px;
    text-align: center;
}
.dropdown_center a{
    background-color: #FC6E62;
    color: white;
    width: 100%;
}

.d-button1, .d-button2{
    width: 80%;
    box-shadow: 1px 1px rgba(0, 0, 0, 0.3);
    border-radius: 20px;
}

.d-button1:hover{
    background-color: #1DB0C9;

}
.d-button2:hover{
    background-color: #1DB0C9;
}

.d-menu{
    background-color: unset;
    padding: 0px 0px;
    border: unset;
     width: 80%;
}

.d-button:hover{
        background-color: #FC6E62;
        color: white;
}

.dropdown_color:focus{
            background-color: #FC6E62 !important;
            border: none;

}

/ <!-- All Css --> /
.dashboard-users-sec{
	display: flex;
	justify-content: center;
}
.dashboard-users-sec-page{
	width: 100%;
}
.dashboard-users-sec-data{
	display: flex;
	justify-content: space-between;
}
.dashboard-users-sec-detail-1{
	width: 6%;
	border-right: 1px solid #000;
}
.dashboard-users-sec-detail-2{
	width: 94%;
}
/*header*/
.dashboard-2-users-header{
	display: flex;
	justify-content: center;
	padding: 0px 0px;
	background-color: #fff;
}
.dashboard-2-users-header-page{
	width: 98%;
}
.dashboard-2-users-header-data{
	display: flex;
	justify-content: space-between;
}
.dashboard-2-users-header-detail-1{
	display: flex;
	align-items: center;
    justify-content: flex-end;
	width: 59%;
}
.dashboard-2-users-header-detail-2{
	width:  39%;
	display: flex;
	justify-content: space-between;
	align-items: center;
}
.dashboard-2-users-header-img{
	width: 30%;
}
.dashboard-2-users-header-img img {
	width: 170px;
}
.dashboard-2-users-header-srch{
	width: 40%;
	position: relative;
}
.dashboard-2-users-header-srch i{
	position: absolute;
	top :8px;
	left: 5px;
	color: #D5DDE4;
}
.dashboard-2-users-header-srch input{
	width: 100%;
	padding: 5px 25px;
	border-radius: 6px;
	border: 1px solid #fff;
    outline: #D5DDE4;
}
.dashboard-2-users-header-srch input::placeholder{
    color: #D5DDE4;
}
.dashboard-2-users-header-detail-slct {
    width: 30%;
}
.dashboard-2-users-header-detail-slct select{
	border-radius: 30px;
	background-color: #FC6E62;
	padding: 5px 20px;
	color: #fff;
	outline: none;
    border: 1px solid #FC6E62;
}
.dashboard-2-users-header-detail-profile-logout{
    width: 30%;
    text-align: center;
}
.dashboard-2-users-header-detail-profile-logout button{
    background-color: #D0EAF0;
    color: #000;
    font-weight: bold;
    border:1px solid #D0EAF0;
    padding: 6px 42px;
}
.dashboard-2-users-header-detail-profile-logout button a{
    color: #000;
    font-weight: bold;
    text-decoration: none;
}
.dashboard-2-users-header-detail-outers{
    display: flex;
    justify-content: space-between;
    width: 40%;
}
.dashboard-2-users-header-detail-bell{
	position: relative;
    width: 33%;
    padding: 20px 0px;
    text-align: center;
}
.dashboard-2-users-header-detail-msg{
	position: relative;
    border-left: 1px solid #D5DDE4;
    border-right: 1px solid #D5DDE4;
    width: 33%;
    padding: 20px 0px;
    text-align: center;
}
.dashboard-2-users-header-detail-cart{
    width: 33%;
    padding: 20px 0px;
    text-align: center; 
}
.dashboard-2-users-header-detail-bell .fa-circle{
	position: absolute;
    top: 26%;
    right: 37%;
	font-size: 8px;
	color: red;
}
.dashboard-2-users-header-detail-bell .fa-bell-o{
	font-size: 20px;
	color: #A9B9C6;
}

.dashboard-2-users-header-detail-msg .fa-circle{
	position: absolute;
	top: 29%;
    right:28%;
	font-size: 8px;
	color: #198EA2;
}
.dashboard-2-users-header-detail-msg .fa-envelope-o{
	font-size: 20px;
	color: #A9B9C6;
}

.dashboard-2-users-header-detail-cart .fa-shopping-cart{
	font-size: 20px;
	color: #A9B9C6;
}
.dashboard-users-sec-detail-1-top-section{
    text-align: center;
    padding-top: 20px;
}
.dashboard-users-sec-detail-1-top-section p {
    font-weight: bold;
    margin-bottom: 2px;
}
.dashboard-users-sec-detail-1-top-section button {
    background-color: transparent;
    border:1px solid #8ACF84;
    color:#8ACF84;
    font-size: 11px;
    font-weight: bold;
    padding:0px 12px;
    transition-duration: 0.3s;
}
.dashboard-users-sec-detail-1-top-section button a{
    color:#8ACF84;
    font-size: 11px;
    font-weight: bold;
    text-decoration: none;
}
.dashboard-users-sec-detail-1-top-section button a:hover{
    color:#fff;
}
.dashboard-users-sec-detail-1-top-section button:hover {
    background-color: #8ACF84;
}
.dashboard-users-sec-detail-1-side-links{
	margin-top: 70px;
}
.dashboard-users-sec-detail-1-side-button button{
	width: 100%;
	outline: none;
	border: transparent;
	background-color: transparent;
    padding: unset;
}
.dashboard-users-sec-detail-1-side-button button:focus{
	outline: none;
}
.dashboard-users-sec-detail-1-side-button{
	margin-top: 10px;
}
.dashboard-users-sec-detail-1-side-button button a{
	display: block;
	color: #ccc;
	font-size: 30px;
}
.dashboard-users-sec-detail-1-side-button button a:hover{
	color: #198EA2;
	border-right: 2px solid #198EA2;
}
/*dashboard-content*/
.dashboard-2-users-top-content-all{
	display: flex;
	justify-content: center;
	background-color: #F6F9FF;
	padding: 30px 0px;
}
.dashboard-2-users-top-content-all-head h3{
	margin-bottom: 0px;
	font-size: 18px;
	color: #404958;
	font-weight: 700;
}
.dashboard-2-users-top-content-all-head p{
	font-size: 14px;
	color: #BBC8D3;
}
.dashboard-2-users-top-content-all-page{
	width: 95%;
}
.dashboard-2-users-top-content-5boxes-data{
	display: flex;
	justify-content: space-between;
	margin-top: 10px;
}
.dashboard-2-users-top-content-5boxes-detail{
	margin-top: 10px;
	background-color: #fff;
	border-radius: 6px;
	width: 19%;
	padding: 25px 20px 0px 20px;
	text-align: center;
}
.dashboard-2-users-top-content-5boxes-detail-icn i{
	background: linear-gradient(to right, #1aa1b8 0%, #1fc4e0 85%);
    color: #fff;
    border-radius: 6px;
    padding: 15px 18px 15px 18px;
    width: 60px;
    text-align: center;
    font-size: 26px;
}
.dashboard-2-users-top-content-5boxes-detail h4{
	color: #1A9FB6;
	font-size: 24px;
    font-weight: bold;
	margin-bottom: 0px;
	margin-top: 3px;
}
.dashboard-2-users-top-content-5boxes-detail p{
	font-size: 14px;
	color: #000;
	margin-bottom: 0px;
    font-weight: bold;
}
/ custom /
.dashboard-2-users-top-content-5boxes-detail-cst-1{
	margin-top: 10px;
	background: linear-gradient(to right, #e9a160 10%, #f7d366 100%);
	border-radius: 6px;
	width: 19%;
	padding: 25px 20px 20px 20px;
	text-align: center;
}
.dashboard-2-users-top-content-5boxes-detail-cst-2{
	margin-top: 10px;
	background: linear-gradient(to right, #f85a62 2%, #f38067 100%);
	border-radius: 6px;
	width: 19%;
	padding: 25px 20px 20px 20px;
	text-align: center;
}
.dashboard-2-users-top-content-5boxes-detail-icn-cst-1 i{
	background: #fff;
    color: #F7C166;
    border-radius: 6px;
    padding: 15px 18px 15px 18px;
    width: 60px;
    text-align: center;
    font-size: 26px;
}
.dashboard-2-users-top-content-5boxes-detail-icn-cst-2 i{
	background: #fff;
    color: #F56D65;
    border-radius: 6px;
    padding: 15px 18px 15px 18px;
    width: 60px;
    text-align: center;
    font-size: 26px;
}
.dashboard-2-users-top-content-5boxes-detail-cst-1 h4{
	color: #fff;
	font-size: 24px;
    font-weight: bold;
	margin-bottom: 0px;
	margin-top: 3px;
}
.dashboard-2-users-top-content-5boxes-detail-cst-1 p{
	font-size: 14px;
	color: #fff;
	margin-bottom: 0px;
    font-weight: bold;
}
.dashboard-2-users-top-content-5boxes-detail-cst-1 summary{
	display: none;
}
.dashboard-2-users-top-content-5boxes-detail-cst-1 canvas.marks{
	height: 50px;
	width: 100%;
}
.dashboard-2-users-top-content-5boxes-detail-cst-2 h4{
	color: #fff;
	font-size: 24px;
    font-weight: bold;
	margin-bottom: 0px;
	margin-top: 3px;
}
.dashboard-2-users-top-content-5boxes-detail-cst-2 p{
	font-size: 14px;
	color: #fff;
	margin-bottom: 0px;
    font-weight: bold;
}
.dashboard-2-users-top-content-5boxes-detail-cst-2 summary{
	display: none;
}
.dashboard-2-users-top-content-5boxes-detail-cst-2 canvas.marks{
	height: 50px;
	width: 100%;
}

/*dashboard-2-users-statistics left and rigt area*/
.dashboard-2-users-btn-area-data{
	display: flex;
	justify-content: space-between;
	margin-top: 20px;
}
.dashboard-2-users-btn-area-detail-1{
	width: 59%;
	padding: 20px 15px;
	background-color: #fff;
	border-radius: 6px;
    display: flex;
    justify-content: center;
}
.dashboard-2-users-btn-inners-1{
    width: 90%;
    display: flex;
    flex-direction: column;
}
.dashboard-2-users-btn-cst-1 {
    color:#fff;
    background: linear-gradient(to right, #1a9fb6 0%, #1fc5e2 100%);
    border: 1px solid #1FC5E2;
    padding: 15px 0px;
    margin-bottom: 15px;
}
.dashboard-2-users-btn-cst-1 a {
    color: #fff;
    font-weight: bold;
    font-size: 18px;
}
.dashboard-2-users-btn-cst-1 a i{
    padding-left: 8px;
    font-size: 16px;
    font-weight: 400;
}
.dashboard-2-users-btn-cst-2 {
    color:#fff;
    background: linear-gradient(to right, #000000 0%, #737373 100%);
    border: 1px solid #737373;
    padding: 15px 0px;
    margin-bottom: 15px;
}
.dashboard-2-users-btn-cst-2 a {
    color: #fff;
    font-weight: bold;
    font-size: 18px;
}
.dashboard-2-users-btn-cst-3 {
    color:#fff;
    background: linear-gradient(to right, #f85a62 0%, #f38067 100%);
    border: 1px solid #F38067;
    padding: 15px 0px;
    margin-bottom: 15px;
}
.dashboard-2-users-btn-cst-3 a {
    color: #fff;
    font-weight: bold;
    font-size: 18px;
}
.dashboard-2-users-btn-cst-3 a i{
    padding-left: 8px;
    font-size: 16px;
    font-weight: 400;
}
.dashboard-2-users-statistics-area-detail-2{
	width: 39%;
	padding: 20px 20px;
	background-color: #fff;
	border-radius: 6px;
}
.dashboard-2-users-statistics-area-all-head-1{
    display: flex;
    justify-content: space-between;
}
.dashboard-2-users-statistics-area-all-head-1 h3{
    font-size: 20px;
    font-weight: bold;
}
.dashboard-2-users-statistics-area-all-head-1 select{
    color: #A9B9C6;
    border: none;
}
.dashboard-2-users-statistics-more-table{
	width: 100%;
}
.dashboard-2-users-statistics-more-table tr td{
	padding: 30.9px 0px;
	font-size: 13px;
    color: #A9B9C6;
    font-weight: 700;
    text-align: center;
    border-bottom: 1px solid #F6F9FF;

}
.dashboard-2-users-statistics-more-table .db-row-1 th{
	font-size: 14px;
	text-align: left;
	color: #A9B9C6;
	padding: 10px 0px;
	font-weight: bold;
	border-bottom: 1px solid #E7ECF0;
}
.dashboar-user-mostsales-viewall{
	display: flex;
	justify-content: flex-end;
	margin-top: 20px;
}
.dashboar-user-mostsales-viewall a{
	font-size: 14px;
	color: #F85D62;
	font-weight: 600;
}
.dashboar-user-mostsales-viewall a:hover{
	text-decoration: none;
}
.dashboard-2-users-statistics-more-sales-data::-webkit-scrollbar {
  width: 6px;
   height: 4px;
}
.dashboard-2-users-statistics-more-sales-data::-webkit-scrollbar-track {
  box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
  border-radius: 10px;
}
 
.dashboard-2-users-statistics-more-sales-data::-webkit-scrollbar-thumb {
  background-color: #1099B1;
  border-radius: 10px;
}
/*responsive menu*/
.dashboard-2-users-res-menu{
	display: none;
	justify-content: center;
	position: fixed;
	top: 0px;
	height: 100vh;
	overflow-y: auto;
	background-color: #fff;
	width: 250px;
	padding: 50px 0px;
	box-shadow: 6px 3px 21px -5px rgba(0,0,0,0.52);
	z-index: 3;
	transition-duration: 0.5s;
}
.dashboard-2-users-res-menu-page{
	width: 90%;
}
.dashboard-2-users-header-logo img {
	width: 180px;
}
.dashboard-2-users-header-profle-data-img-2{
	margin-top: 20px;
}
.dashboard-2-users-header-profle-icons-2{
	position: relative;
	margin-right: 20px;
	cursor: pointer;
	margin-bottom: 10px;
}
.dashboard-2-users-header-profle-dat{
	display: flex;
	margin-left: 15px;
    padding: 15px 0px;
}
.dashboard-2-users-header-profle-icons-2 .fa-bell-o{
	font-size: 24px;
	color: #777;
}
.dashboard-2-users-header-profle-iconss-2 .fa-shopping-cart{
	font-size: 26px;
	color: #777;
    padding-left: 22px;
}
.dashboard-2-users-header-profle-iconsen-2{
	position: relative;
}
.dashboard-2-users-header-profle-iconsen-2  .fa-envelope-o{
	font-size: 24px;
	color: #777;
}
.dashboard-2-users-header-profle-iconsen-2 .fa-circle{
	position: absolute;
	top: 0px;
	right: -3px;
	font-size: 8px;
	color: green;
}
.dashboard-2-users-header-profle-icons-2 .fa-circle{
	position: absolute;
	top: -2px;
    left: 14px;
	font-size: 8px;
	color: red;
}
.dashboard-2-users-header-detail-profile-logout-cst{
    width: 100%;
    text-align: center;
}
.dashboard-2-users-header-detail-profile-logout-cst button{
    width: 100%;
    background-color: #D0EAF0;
    color: #000;
    font-weight: bold;
    border:1px solid #D0EAF0;
    padding: 6px 42px;
}
.dashboard-2-users-header-detail-profile-logout-cst button a{
    color: #000;
    font-weight: bold;
    text-decoration: none;
}
#dashboard2myprofiletext2{
	border: none;
    outline: none;
    margin-top: 10px;
    margin-left: 10px;
}
.dashboard-2-users-res-menu-cros{
	position: absolute;
	top: 20px;
	right: 20px;
}
.dashboard-2-users-res-menu-cros i{
	font-size: 20px;
	cursor: pointer;
	color: #404958;
	transition-duration: 0.5s;
}
.dashboard-2-users-res-menu-cros i:hover{
		color: #198EA2;
}
.responsive-icon-2{
	display: none;
}
/ media query /
@media screen and (max-width: 1200px) {
    .dashboard-2-users-header-detail-1 {
        width: 50%;
    }
    .dashboard-2-users-header-detail-2 {
        width: 50%;
    }
}
@media only screen and (max-width:1040px) {
    .dashboard-users-sec-detail-1 {
        width: 10%;
    }
    .dashboard-users-sec-detail-2 {
        width: 90%;
    }
	.dashboard-2-users-top-content-5boxes-data{
		flex-wrap: wrap;
	}
	.dashboard-2-users-top-content-5boxes-detail{
		width: 49%;
	}
    .dashboard-2-users-top-content-5boxes-detail-cst-1{
		width: 49%;
	}
    .dashboard-2-users-top-content-5boxes-detail-cst-2{
		width: 49%;
	}
	.dashboard-2-users-btn-area-data{
		flex-direction: column;
	}
	.dashboard-2-users-btn-area-detail-1{
		width: 100%;
	}
	.dashboard-2-users-statistics-area-detail-2{
		width: 100%;
		margin-top: 20px;
	}
}
@media only screen and (max-width:650px) {
	.dashboard-2-users-top-content-5boxes-detail{
		width: 100%;
	}
    .dashboard-2-users-top-content-5boxes-detail-cst-1{
		width: 100%;
	}
    .dashboard-2-users-top-content-5boxes-detail-cst-2{
		width: 100%;
	}
}
/*menu*/
@media only screen and (max-width: 975px) {
    .dashboard-2-users-header{
        padding: 15px 0px;
    }
    .dashboard-2-users-header-detail-profile-logout{
        display: none;
    }
    .dashboard-2-users-header-detail-outers {
        display: none;
    }
	.dashboard-2-users-header-detail-2{
		width: 70%;
	}
	.dashboard-2-users-header-detail-slct{
		width: 100%;
	}
	.dashboard-2-users-header-detail-slct select{
		width: 85%;
	}
	.dashboard-2-users-header-srch-res{
			margin-top: 20px;
			position: relative;
		}
		.dashboard-2-users-header-srch-res i{
			position: absolute;
			top :8px;
			left: 5px;
			color: #777;

		}
		.dashboard-2-users-header-srch-res input{
			width: 100%;
			padding: 5px 20px;
			border-radius: 6px;
			outline: none;

			border: 1px solid #ccc;
		}
		.dashboard-2-users-header-srch{
			display: none;
		}
	.responsive-icon-2{
		display: block;
	}
	.dashboard-2-users-header-detail-bell{
		display: none;
	}
	.dashboard-2-users-header-detail-msg{
		display: none;
	}
    .dashboard-2-users-header-detail-cart {
        display: none;
    }
	.dashboard-2-users-header-detail-profile-img{
		display: none;
	}
	.responsive-icon-2 i{
		font-size: 28px;
		cursor: pointer;
		color: #404958;
		transition-duration: 0.5s;
	}
	.responsive-icon-2 i:hover{
		color: #198EA2;
	}
	.dashboard-1-header-profle{
		display: none;
	}
	.dashboard-2-users-res-menu-content-detail{
	margin-bottom: 20px;
	}
}
@media only screen and (max-width: 500px) {
    .dashboard-users-sec-detail-1 {
        width: 14%;
    }
    .dashboard-users-sec-detail-2 {
        width: 86%;
    }
    .dashboard-users-sec-detail-1-top-section p {
        font-weight: unset;
        margin-bottom: 2px;
    }
    .dashboard-users-sec-detail-1-top-section button {
        background-color: transparent;
        border:1px solid #8ACF84;
        color:#8ACF84;
        font-size: 9px;
        font-weight: unset;
        padding:0px 5px;
        transition-duration: 0.3s;
    }
    .dashboard-users-sec-detail-1-top-section button a{
        color:#8ACF84;
        font-size: 9px;
        font-weight: unset;
    }
}
.btn-styled
{
    background-color: #FC6E62;
    color: white;
    border-radius: 5px;
}
	</style>
</head>
<body>


<!-- MAIN WRAPPER -->
<div class="body-wrap shop-default shop-cards shop-tech gry-bg">

    <!-- Header -->
    @include('frontend.inc.customer_nav')

<section class="dashboard-users-sec">
	<div class="dashboard-users-sec-page">
		<div class="dashboard-users-sec-data">
		     @include('frontend.inc.customer_side_nav')
		     			<div class="dashboard-users-sec-detail-2">
		     			    	<!-- header -->
			                        @include('frontend.inc.customer_top_nav')
				                <!-- header-end -->
            @yield('content')
            </div>
		</div>
	</div>
</section>

<script>
    var dashboardusertwo = document.getElementById("dashboardusertwo");
    function showdashusertwo(){
        dashboardusertwo.style.left = "0%";
        dashboardusertwo.style.display = "flex";
    }
    function hidedashusertwo(){
        dashboardusertwo.style.left = "-150%";
    }
</script>
@yield('script')
</body>
</html>
