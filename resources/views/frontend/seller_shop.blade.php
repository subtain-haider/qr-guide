@extends('frontend.layouts.new_app_footer')
<div id="google_translate_element"></div>

<script type="text/javascript">
function googleTranslateElementInit() {
        new google.translate.TranslateElement(
            {pageLanguage: 'en'},
            'google_translate_element'
        );
    }
 
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<style>
/* Style the Image Used to Trigger the Modal */
#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (Image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image (Image Text) - Same Width as the Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation - Zoom in the Modal */
.modal-content, #caption {
  animation-name: zoom;
  animation-duration: 0.6s;
}

@keyframes zoom {
  from {transform:scale(0)}
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
.minipro-page-reviews-detail-2 {
  scrollbar-width: none;
}
.translated-ltr { margin-top:-40px !important; }
.goog-te-banner-frame { display:none !important; }
#google_translate_element { text-align:right; }
    .goog-logo-link {
   display:none !important;
}
.focuschange:focus{background-color: #FC6E62!important;}
.focuschange:hover{background-color: #FC6E62!important;}
.goog-te-gadget {
   color: transparent !important;
}

.goog-te-gadget .goog-te-combo {
   color: #3ab1c4 !important;
    padding: 2px 5px;
    position: absolute;
    right: 0;
    top: 0;
}

/* The Modal (background) */
.modal2 {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 27%;
  top: 0;
  width: 40%;
  margin: 0 auto; 
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: transparent; /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  border: 1px solid #888;
  width: 80%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0} 
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

/* The Close Button */
.closes {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.closes:hover,
.closes:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.modal-header {
  padding: 2px 16px;
  background-color: #5cb85c;
  color: white;
}

.modal-body {padding: 2px 16px;}

.modal-footer {
  padding: 2px 16px;
  background-color: #5cb85c;
  color: white;
}
</style>

@if($shop->minibgcolor)

				<style type="text/css">
					.minibgcolor { background-color: {{$shop->minibgcolor}} !important; }
					
				</style>
	

@endif

<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  {{-- icon link chat --}}

<!--<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>-->
<body style="font-family:{{$shop->font}};" >
{{-- icon link chat --}}

@section('meta')
<meta name="tag" content="{{ $shop->metatag }}">

<meta name="description" content="{{ $shop->metadescription }}">
@endsection
@section('content')

	<div id="fb-root"></div>
	
	<div id="myModalx" class="modal2">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="closes">&times;</span>
      
    </div>
    <div class="modal-body" id="modal-body2">
      <p>No rating available for this product!</p>
      
    </div>
   
  </div>

</div>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v11.0&appId=827935497371969&autoLogAppEvents=1" nonce="qYPZR133"></script>


	@if($shop->c_active != 0)
    {{-- <input type="hidden" id="auth" data-auth="{{Auth::check()}}"></input> --}}
		<div class="modal" id="coupon_active_modal" data-aos="{{$shop->animatecoupan}}">

			<div class="modal-dialog" style="{{ $shop->c_active ==1 ? 'max-width: 700px;' :'' }} {{ $shop->c_active ==2 ? 'max-width: 650px;' :'max-width: 700px;' }}
			{{$shop->c_active ==3 ? 'max-width: 500px;' :'max-width: 700px;' }}">

				<div class="modal-content">

					<button type="button" class="close px-3 bg-danger border-1 pb-2 pt-1 text-white" data-dismiss="modal" style="position: absolute; top: 10px; right: 10px;z-index:1;opacity: 1;">&times;</button>

					@if($shop->c_active == 1)
						@include('frontend.inc.ccoupon')
					@elseif($shop->c_active == 2)
						@include('frontend.inc.cbanner')
					@elseif($shop->c_active == 4)
						@include('frontend.inc.banner1')
					@elseif($shop->c_active == 5)
						@include('frontend.inc.banner2')
					@elseif($shop->c_active == 6)
						@include('frontend.inc.banner3')
					@elseif($shop->c_active == 7)
						@include('frontend.inc.banner4')
					@else
						@include('frontend.inc.cpopup')

					@endif

				</div>

			</div>

		</div>

	@endif
    @if($coupon != null)
    
     <!--&& strtotime(date('d-m-Y')) >= $coupon->start_date && strtotime(date('d-m-Y')) <= $coupon->end_date-->
    {{-- <input type="hidden" id="auth" data-auth="{{Auth::check()}}"></input> --}}

		<div class="modal" id="coupon_modal_active" data-aos="{{$shop->animatecoupan}}">

			<div class="modal-dialog" style="{{ $shop->coupon_id != Null ? 'max-width: 700px;' :'' }}">

				<div class="modal-content">

					<button type="button" class="close px-3 bg-danger border-1 pb-2 pt-1 text-white" data-dismiss="modal" style="position: absolute; top: 10px; right: 10px;z-index:1;opacity: 1;">&times;</button>
                    @if($shop->coupon_act == 1)
						@include('frontend.inc.banner1')

					@elseif($shop->coupon_act == 2)

						@include('frontend.inc.banner2')
					@elseif($shop->coupon_act == 3)
						@include('frontend.inc.banner3')
					@elseif($shop->coupon_act == 4)
						@include('frontend.inc.banner4')
					@else
						@include('frontend.inc.copuponpopup')
					@endif
					

				</div>

			</div>

		</div>

	@endif
    <!-- product picture -->

	@if(App\Models\Product::where('id', $shop->product)->count() > 0)

		@php

			$product = App\Models\Product::where('id', $shop->product)->first();

			$main_photo = '';

			$photos = array();

			if(!empty($product->photos)){

				$photos = json_decode($product->photos);

				$main_photo = $photos[0];

			}

		@endphp

		<section class="miniqr-product-page-sec minibgcolor">
	
			<div class="minipage">

				<div class="miniqr-product-page-head">
                   
					<!--<h2>{{ $product->name }}</h2>-->

				</div>

				<div class="minipro-page-pro-pic-data-slider" data-aos="{{$shop->animateguide}}">

					<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

						<div class="carousel-inner mini-site-slider-img-wdth">

							@for($i=0; $i < count($photos); $i++)

								<div class="carousel-item {{ $i === 0 ? 'active' : '' }}" >

									<a href="#"><img style="object-fit: cover;"  class="d-block w-100" src="{{asset($photos[$i])}}" alt="First slide"></a>

								</div>

							@endfor

						</div>
	
						<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">

							<i class="fa fa-angle-left minisite-slider-moveicn"></i>

							<span class="sr-only">Previous</span>

						</a>

						<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">

							<i class="fa fa-angle-right minisite-slider-moveicn"></i>

							<span class="sr-only">Next</span>

						</a>

					</div>
				</div>

				<div class="minipro-page-pro-pic-data">

					@for($i=0; $i < count($photos); $i++)

					<div class="minipro-page-pro-pic-detail-1">

						<div class="minipro-page-pro-pic-1" data-aos="{{$shop->animateguide}}" data-aos-duration="1000">

							{{-- <a href="#" onclick="@php if(Auth::check(){ echo '<script>alert("helo");</script>'})  @endphp" data-auth="{{Auth::user()->name}}"><img style="object-fit: contain;" src="{{asset($photos[$i])}}"></a> --}}

                            <a href="#" onclick="checkAuth()" id="product" data-auth="{{Auth::check()}}"><img style="object-fit: contain;" src="{{asset($photos[$i])}}"></a>

						</div>

					</div>

					@endfor

				</div>

			</div>

		</section>

	@endif

	@if ($shop->video_provider != null && $shop->video_link != null)

		<!-- link to product store -->

		<section class="minipro-page-store-sec minibgcolor">

			<div class="minipage" data-aos="{{$shop->animatevideo}}">

				<div class="minipro-page-store-head">

					<!--<h2>{{__('INSTRUCTION VIDEO')}}</h2>-->

				</div>

				<div class="minipro-page-store-data-slider">

					<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">

					<div class="carousel-inner">
                        {{ $shop->video_link }}
						<div class="carousel-item active">
							<?php preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $shop->video_link, $match);
							?>
							@if ($shop->video_provider == 'youtube' && $shop->video_link != null)

								<iframe class="d-block w-100" width="100%" height="409" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $match[1] }}"></iframe>

							@elseif ($shop->video_provider == 'dailymotion' && $shop->video_link != null)
								<iframe class="embed-responsive-item d-block w-100" src="https://www.dailymotion.com/embed/video/{{ explode('video/', $shop->video_link)[1] }}"></iframe>

							@elseif ($shop->video_provider == 'vimeo' && $shop->video_link != null)

								<iframe class="d-block w-100" src="https://player.vimeo.com/video/{{ explode('vimeo.com/', $shop->video_link)[1] }}" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

							@elseif (!empty($shop->video_temp))

								<video  style="w-100">

									<source src="{{$shop->video_link}}" type="video/mp4">

									Your browser does not support HTML video.

								</video>

							@endif

						<!-- <iframe class="d-block w-100" height="409" src="https://www.youtube.com/embed/KBo2El1bcwQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

						</div>

						<!-- <div class="carousel-item">

						<iframe class="d-block w-100" height="409" src="https://www.youtube.com/embed/ZwnXW_7fzk0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

						</div>

						<div class="carousel-item">

						<iframe class="d-block w-100" height="409" src="https://www.youtube.com/embed/yXyu7rqlpvs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

						</div> -->

					</div>

					<!-- <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">

						<i class="fa fa-angle-left minisite-slider-moveicn-2"></i>



					</a>

					<a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">

						<i class="fa fa-angle-right minisite-slider-moveicn-2"></i>



					</a> -->

					</div>

				</div>

				<div class="minipro-page-store-data">

					@if ($shop->video_provider == 'youtube' && $shop->video_link != null)

						<iframe width="100%" height="409" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $match[1] }}"></iframe>

					@elseif ($shop->video_provider == 'dailymotion' && $shop->video_link != null)

						<iframe src="https://www.dailymotion.com/embed/video/{{ explode('video/', $shop->video_link)[1] }}"></iframe>

					@elseif ($shop->video_provider == 'vimeo' && $shop->video_link != null)

						<iframe src="https://player.vimeo.com/video/{{ explode('vimeo.com/', $shop->video_link)[1] }}" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

					@elseif (!empty($shop->video_temp))

						<video  style="w-100">

							<source src="{{$shop->video_link}}" type="video/mp4">

							Your browser does not support HTML video.

						</video>

					@endif

				</div>

			</div>

		</section>

	@endif



	<!-- Dowmload and view -->

	@if($shop->pdf != null)

		@php

			$language = false;

			$pdfs = json_decode($shop->pdf);

			foreach($pdfs as $key => $val){

				if($val->lng != null){

					$language = true;

				}

			}

		@endphp

		<section class="minipro-page-downloads-sec minibgcolor">

			<div class="minidown-page" data-aos="{{$shop->animatepdf}}">

				<div class="pro-downloads-head">

					<h2>{{$shop->pdf_title}}</h2>

				</div>

				<div class="minipro-page-downloads-data">

					<div class="minipro-page-downloads-text">

						<div class="minipro-page-downloads-para">

							<p>

								{!! $shop->pdf_text !!}

							</p>

						</div>

					</div>

					<div class="minipro-page-downloads-btns">
 
						@if($language == true)

							<div class="minipro-page-downloads-btns-detail-1" style="display:none;">

								<form class="minipro-page-downloads-btns-detail-select">

									<select name="cars" id="cars">

										<option value="slect" selected>{{__('SELECT LANGUAGE')}}</option>

										@foreach($pdfs as $key => $val)

											@if($val != null)

												<option value="eng">{{__('English')}} </option>

											@endif

										@endforeach

									</select>

								</form>

							</div>

						@endif

						<div class="minipro-page-downloads-btns-detail-2">
							@php

								$getpath = json_decode($shop->pdf);

								foreach($getpath as $key => $val){

									if($val->path != null){

										$path = $val->path;


									}

								}

							@endphp

							<div class="minipro-page-downloads-btns-detail">
								<form class="minipro-page-downloads-btns-detail-select">
									@if(sizeof($getpath) > 1)
									<select onchange="window.location = this.value;">

										<option value="slect" selected>{{__('SELECT ORIGNAL GUIDE FOR VIEW')}}</option>
										@php
										foreach($getpath as $key => $val){

											if($val->path != null){

												$path = $val->path;
												$finalpath = url('/public').'/'.$path;
										@endphp
										<!-- <form method="get" action=""> -->
										   <option value="<?php echo $finalpath ?>">{{__('VIEW ORIGNAL GUIDE')}}</option>
										<!-- </form> -->
										@php
											}

										}
										@endphp
									</select>
									@else
									<button  type="button" value="Open Window" onclick="pdfCheckOrg()" id="pdfCheckOrignal" data-path ="{{$path}}" data-slug="{{$shop->slug}}" data-auth="{{Auth::check()}}" pdf-check="{{$shop->pdf_check}}">{{__('VIEW ORIGNAL GUIDE')}}</button>
									@endif
								</form>
							</div>
							<div class="minipro-page-downloads-btns-detail">
								<form class="minipro-page-downloads-btns-detail-select">
									@if(sizeof($getpath) > 1)
									<select onchange="window.location = this.value;">

										<option value="slect" selected>{{__('VIEW TRANSLATED GUIDE')}}</option>
										@php
										foreach($getpath as $key => $val){

											if($val->path != null){

												$path = $val->path;
												$finalpath = 'http://translate.google.com/translate?sl=en&tl=en&u='.url('/public').'/'.$path
										@endphp
										<!-- <form method="get" action=""> -->
										   <option value="<?php echo $finalpath ?>">{{__('VIEW TRANSLATED GUIDE')}}</option>
										<!-- </form> -->
										@php
											}

										}
										@endphp
									</select>
									@else
									<button  type="button" value="Open Window" onclick="checkPdfTrans()" data-path ="{{$path}}" id="pdfCheckTrans" data-slug="{{$shop->slug}}" data-auth="{{Auth::check()}}" pdf-check="{{$shop->pdf_check}}">{{__('VIEW TRANSLATED GUIDE')}}</button>
									@endif
								</form>
							</div>

							<div class="minipro-page-downloads-btns-detail">
								<form class="minipro-page-downloads-btns-detail-select">
									@if(sizeof($getpath) > 1)
									<select onchange="window.location = this.value;">
										<option value="slect" selected>{{__('DOWNLOAD GUIDE')}}</option>
										@php
										foreach($getpath as $key => $val){

											if($val->path != null){

												$path = $val->path;
												$headers = array(
												  'Content-Type' => 'application/pdf',
												);
										@endphp
												<option value="{{ url('/public').'/'.$path }}">{{__('DOWNLOAD GUIDE')}}</option>

										@php
											}

										}
										@endphp
									</select>
									@else

										<!-- <form method="get" action="{{ url('/public').'/'.$path }}"> -->
											<button type="button" value="Open Window" onclick="checkPdfDownload()" data-path ="{{$path}}" id="pdfCheck" data-slug="{{$shop->slug}}" data-auth="{{Auth::check()}}" pdf-check="{{$shop->pdf_check}}">{{__('DOWNLOAD GUIDE')}} </button>
										<!-- </form> -->
									@endif
								</form>
							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="minipro-page-downloads-overlay"></div>

		</section>

	@endif

	<!-- product information -->

	@if(($shop->product != null) and (!empty($shop->product)) || $shop->questions)

		<section class="minipro-page-informa-sec minibgcolor">

			<div class="minipage">

				<div class="mininewpropriwrapper">

					<div class="mininewpropriwrapper-page">

						<div class="mininewpropritabs">

							@if((App\Models\Product::where('id', $shop->product)->count() > 0) and (!empty(App\Models\Product::where('id',$shop->product)->first()->description)))

								<span class="mininewpropritab"><button class="mininewpropritabs-btn">{{__('PRODUCT INFORMATION')}}</button></span>

							@endif

							@if(!empty($shop->product) and (!empty(App\Models\Product::where('id',$shop->product)->first()->spacification)))

							<span class="mininewpropritab"><button class="mininewpropritabs-btn">{{__('PRODUCT SPECIFICATION')}}</button></span>

							@endif

							<span class="mininewpropritab"><button class="mininewpropritabs-btn">{{__('COMPANY PROFILE')}}</button></span>

						</div>

						<div class="mininewtab_content">

							@if(App\Models\Product::where('id', $shop->product)->count() > 0)

								<div class="mininewprotab_item" data-aos="flip-left">


    								<p>
    									{!! App\Models\Product::where('id', $shop->product)->first()->description !!}
    
    								</p>
    
    								<div class="minipro-page-informa-content-btns">
    
                						<div class="minipro-page-informa-content-btn-1">
                
                							<a href="#" data-toggle="modal" data-target="#improveModal">{{__('HOW CAN WE IMPROVE OUR PRODUCT')}} <i class="fa fa-angle-right"></i></a>
                
                
                						</div>
        
        
        						        <div class="minipro-page-informa-content-btns-data">
        
        							<div class="minipro-page-informa-content-btns-social">
        
        								<div class="minipro-page-informa-content-btns-social-text">
        
        									<p>{{__('SHARE THIS GUIDE')}}</p>
        
        								</div>
        
        								<div class="minipro-page-informa-content-btns-social-icons">
        
        									<a class="fb-share-button" data-href="{{ url()->current() }}" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}" class="fb-xfbml-parse-ignore"><img src="{{asset('frontend/images/f2.png')}}"></a>

        									<a target="_blank" class="twitter-share-button"
        									  href="https://twitter.com/intent/tweet?text={{ url()->current() }}"
        									  data-size="Default">
        									<img src="{{asset('frontend/images/t.png')}}"></a>
                                            <a href="whatsapp://send?text={{ url()->current() }}" data-action="share/whatsapp/share"  
                                            target="_blank"><img src="{{asset('frontend/images/wa.png')}}" style="background: #fff;"></a>   
                                            
                                            
        									<!-- <a href="#"><img src="{{asset('frontend/images/g2.png')}}"></a>
        
        									<a href="#"><img src="{{asset('frontend/images/f2.png')}}"></a>
        
        									<a href="#"><img src="{{asset('frontend/images/t.png')}}"></a> -->
        
        								</div>
        
        							</div>
        
        							<div class="minipro-page-informa-content-btns-buy">
                                        @if($product->external_website == 'amazon')
                    				        <a href="http://www.amazon.com/dp/{{ $product->external_link }}/?tag=myqrguide-20" class="single-pro-ban-buynow">Buy Now<i class="fa fa-shopping-cart"></i></a>
                    				    @elseif($product->external_website == 'ebay')
                    				        <a href="https://www.ebay.com/itm/{{ $product->external_link }}/?customid=myqrguide&campid=5338839039" class="single-pro-ban-buynow">Buy Now <i class="fa fa-shopping-cart"></i></a>
                    				    @else
                    				        <form id="option-choice-form_{{ $product->id }}" action="{{ route('cart.addToCart', ['id' => $product->idy]) }}" method="post" class=" d-lg-block" >
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $product->id }}"> 
                                            <button style="text-align: center; display: block;padding: 12px 0px;color: #000; background-color: #FFA41C;font-weight: 600; transition-duration: 0.5s; width: 100%; border: none;" class="single-pro-ban-buynow minipro-page-informa-content-btns-buy" name="buynow" value="buynow">{{__('BUY NOW')}} <i class="fa fa-shopping-cart"></i></button>
                    				        </form>
                    				    @endif
        								
        
        							</div>
        
        						</div>
    
    					            </div>

								</div>

							@endif
                            @if(!empty($shop->product) and (!empty(App\Models\Product::where('id',$shop->product)->first()->spacification)))
							<div class="mininewprotab_item" data-aos="{{$shop->animateprofile}}">
								{!! App\Models\Product::where('id', $shop->product)->first()->spacification !!}

								<div class="minipro-page-informa-content-btns">

            						<div class="minipro-page-informa-content-btn-1">
            
            							<a href="#" data-toggle="modal" data-target="#improveModal">{{__('HOW CAN WE IMPROVE OUR PRODUCT')}} <i class="fa fa-angle-right"></i></a>
            
            
            						</div>


						            <div class="minipro-page-informa-content-btns-data">

							            <div class="minipro-page-informa-content-btns-social">

								<div class="minipro-page-informa-content-btns-social-text">

									<p>{{__('SHARE THIS GUIDE')}}</p>

								</div>

								<div class="minipro-page-informa-content-btns-social-icons">

									<a class="fb-share-button" data-href="{{ url()->current() }}" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}" class="fb-xfbml-parse-ignore"><img src="{{asset('frontend/images/f2.png')}}"></a>

									<a target="_blank" class="twitter-share-button"
									  href="https://twitter.com/intent/tweet?text={{ url()->current() }}"
									  data-size="Default">
									<img src="{{asset('frontend/images/t.png')}}"></a>

									<!-- <a href="#"><img src="{{asset('frontend/images/g2.png')}}"></a>

									<a href="#"><img src="{{asset('frontend/images/f2.png')}}"></a>

									<a href="#"><img src="{{asset('frontend/images/t.png')}}"></a> -->

								</div>

							</div>

            							<div class="minipro-page-informa-content-btns-buy">
            
            								@if($product->external_website == 'amazon')
                    				        <a href="http://www.amazon.com/dp/{{ $product->external_link }}/?tag=myqrguide-20" class="single-pro-ban-buynow">Buy Now<i class="fa fa-shopping-cart"></i></a>
                    				    @elseif($product->external_website == 'ebay')
                    				        <a href="https://www.ebay.com/itm/{{ $product->external_link }}/?customid=myqrguide&campid=5338839039" class="single-pro-ban-buynow">Buy Now <i class="fa fa-shopping-cart"></i></a>
                    				    @else
                    				        <form id="option-choice-form_{{ $product->id }}" action="{{ route('cart.addToCart', ['id' => $product->idy]) }}" method="post" class=" d-lg-block" >
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $product->id }}"> 
                                            <button style="text-align: center; display: block;padding: 12px 0px;color: #000; background-color: #FFA41C;font-weight: 600; transition-duration: 0.5s; width: 100%; border: none;" class="single-pro-ban-buynow minipro-page-informa-content-btns-buy" name="buynow" value="buynow">{{__('BUY NOW')}} <i class="fa fa-shopping-cart"></i></button>
                    				        </form>
                    				    @endif
            
            							</div>

						            </div>

					            </div>
					        </div>
					        @endif
					        <!-- The Modal -->
                            <div id="myModal" class="modal">
                            
                              <!-- The Close Button -->
                              <span class="close">&times;</span>
                            
                              <!-- Modal Content (The Image) -->
                              <img class="modal-content" id="img01">
                            
                              <!-- Modal Caption (Image Text) -->
                              <div id="caption"></div>
                            </div>
							<div class="mininewprotab_item" data-aos="{{$shop->animateprofile}}">

									<div class="minipro-page-ques-head">

										<h2>{{__('COMPANY PROFILE')}}</h2>

									</div>


									<div class="minipro-page-profile-text">
										@if(!empty($shop->logo))
										<img id="myImg" src="{{ url('/public').'/'.$shop->logo }}" alt="" width="300" height="300" style="object-fit: contain;" />
										@endif
										@if(!empty($shop->address))
										<p><b>Address: </b>{!! $shop->address !!}</p>
										@endif
										@if(!empty($shop->contact_no))
										<p><b>Contact No: </b>{!! $shop->contact_no !!}</p>
										@endif
										@if(!empty($shop->company_email))
										<p><b>Company Email: </b>{!! $shop->company_email !!}</p>
										@endif
										@if(!empty($shop->company_website))
										<p><b>Company Website: </b>{!! $shop->company_website !!}</p>
										@endif
										@if(!empty($shop->description))
										<p><b>Description: </b>{!! $shop->description !!}</p>
										@endif

									</div>
									<div class="minipro-page-profile-content">

										@if ($shop->sliders != null && !empty($shop->sliders))
											<div id="carouselExampleControls2" class="carousel slide" data-ride="carousel">
											  <div class="carousel-inner">
											  	@foreach (json_decode($shop->sliders) as $key => $slide)
											    <div class="carousel-item minipro-page-profile-img {{ $loop->first ? 'active' : '' }}">
											      <img class="d-block w-100" src="{{ asset($slide) }}" alt="First slide">
											    </div>
											    @endforeach
											  </div>
											  <a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-slide="prev">

												<i class="fa fa-angle-left minisite-slider-moveicn"></i>

												<span class="sr-only">Previous</span>

												</a>

												<a class="carousel-control-next" href="#carouselExampleControls2" role="button" data-slide="next">

													<i class="fa fa-angle-right minisite-slider-moveicn"></i>

													<span class="sr-only">Next</span>

												</a>
											</div>
										@endif
									</div>


									@if( isset($shop->linkedin) || isset($shop->whatsapp) || isset($shop->youtube) || isset($shop->twitter) || isset($shop->facebook) || isset($shop->instagram)  || isset($shop->tiktok))
									<div class="minipro-page-profile-oursocial-data" style="margin-top:40px;">

										<div class="minipro-page-profile-oursocial-detail" style="background-color: {{ $shop->social_bgcolor }};"  data-aos="{{$shop->animatesocial}}">

											<div class="minipro-page-profile-oursocial-text" >

												<p>{{__('OUR SOCIAL MEDIA PAGES')}}</p>

											</div>

											<div class="minipro-page-profile-oursocial-icns">

												@if($shop->linkedin != null and !empty($shop->linkedin))
												<a href="{{ $shop->linkedin }}"><img src="{{asset('frontend/images/in.png')}}"></a>
												@endif
												@if($shop->whatsapp != null and !empty($shop->whatsapp))
												<a href="{{ $shop->whatsapp }}"><img src="{{asset('frontend/images/wa.png')}}"></a>
												@endif
												@if($shop->youtube != null and !empty($shop->youtube))
												<a href="{{ $shop->youtube }}"><img src="{{asset('frontend/images/yt.png')}}"></a>
												@endif
												@if($shop->twitter != null and !empty($shop->twitter))
												<a href="{{ $shop->twitter }}"><img src="{{asset('frontend/images/tw.png')}}"></a>
												@endif
												@if($shop->facebook != null and !empty($shop->facebook))
												<a href="{{ $shop->facebook }}"><img src="{{asset('frontend/images/fa.png')}}"></a>
												@endif
												@if($shop->instagram != null and !empty($shop->instagram))
												<a href="{{ $shop->instagram }}"><img src="{{asset('frontend/images/ins.png')}}"></a>
												@endif
												@if($shop->c != null and !empty($shop->tiktok))
												<a href="{{ $shop->tiktok }}"><img src="{{asset('frontend/images/tk.png')}}"></a>
												@endif

											</div>

										</div>

									</div>
									@endif
							</div>
								
						</div>

					</div>


				</div>

			</div>

		</section>

	@endif

	<!-- company profile -->

	<section class="minipro-page-profile-sec minibgcolor">

		<div class="minipage">

			<div class="minipro-page-profile-sec-res">

				<!-- <div class="minipro-profile-head">



				</div> -->
				<!-- Comny profile +share -->


				<!-- <div class="minipro-page-profile-oursocialpro-imgs-data">



					<div class="minipro-page-profile-oursocialpro-imgs-detail">

						<div class="minipro-page-profile-oursocialpro-img-1">

							<img src="{{asset('frontend/images/bans1.jpg')}}"></a>

						</div>

					</div>



					<div class="minipro-page-profile-oursocialpro-imgs-detail">

						<div class="minipro-page-profile-oursocialpro-img-1">

							<a href="#"><img src="{{asset('frontend/images/bans2.jpg')}}"></a>

						</div>

					</div>



					<div class="minipro-page-profile-oursocialpro-imgs-detail">

						<div class="minipro-page-profile-oursocialpro-img-1">

							<a href="#"><img src="{{asset('frontend/images/bans3.jpg')}}"></a>

						</div>

					</div>



				</div> -->

			</div>

			<div class="minipro-page-profile-wranty-data">

				@if(($shop->warranty_text != null and !empty($shop->warranty_text)) || ($shop->warranty != null and !empty($shop->warranty)) || ($shop->warranty_content != null and !empty($shop->warranty_content)))

					<div class="minipro-page-profile-wranty-detail-1"  data-aos="{{$shop->animatewarranty}}">

						<div class="minipro-page-profile-wranty-icon-data">

							<div class="minipro-page-profile-wranty-icon">

								<i class="fa fa-check-square-o"></i>

							</div>

							<div class="minipro-page-profile-wranty-text">

								@php

								$warranty_route = '#';

								$warranty_text = 'Register your warranty here';

								if(($shop->warranty != null and !empty($shop->warranty)) || ($shop->warranty_content != null and !empty($shop->warranty_content))){

									$warranty_route = route('company_warranty.visit', $shop->slug);

								}

								if(!empty($shop->warranty_text)){

									$warranty_text = $shop->warranty_text;

								}

								@endphp
								<h3><a onclick="checkWarranty()" style="cursor: pointer;" id="warrantyCheck" data-auth="{{Auth::check()}}" warranty-route="{{$warranty_route}}" warranty-check="{{$shop->warranty_check}}">{{ $warranty_text }}</a></h3>
								{{-- <h3><a href="{{$warranty_route}}"> {{ $warranty_text }}</a></h3> --}}





							</div>

						</div>

					</div>

				@endif

				@if($shop->club_text != null and !empty($shop->club_text))

					<div class="minipro-page-profile-wranty-detail-2" data-aos="{{$shop->animateclub}}">

						<div class="minipro-page-profile-wranty-icon-data">

							<div class="minipro-page-profile-wranty-icon">

								<i class="fa fa-users"></i>

							</div>

							<div class="minipro-page-profile-wranty-text">

								{{-- <h3><a href="{{ route('club_form',$shop->slug) }}" > {{$shop->club_text}}</a></h3> --}}

								<h3><a onclick="checkClub()" style="cursor: pointer;" id="clubCheck" data-slug="{{$shop->slug}}"  data-auth="{{Auth::check()}}" club-check="{{$shop->club_check}}" > {{$shop->club_text}}</a></h3>

							</div>

						</div>

					</div>

				@endif

			</div>

		</div>

	</section>

	@if($shop->reviews != null and !empty($shop->reviews) and $shop->product != null)

		<!-- coustomer reviews -->

			<section class="minipro-page-reviews-sec minibgcolor">
			<div class="minipage">
				<div class="feednewpropriwrapper">
			    	<div class="feednewpropriwrapper-page">
					    <div class="feednewpropritabs">
					        <span class="feednewpropritab"><button class="feednewpropritabs-btn">CUSTOMER FEEDBACK</button></span>
					        @if($shop->questions != null and !empty($shop->questions))
					        <span class="feednewpropritab"><button class="feednewpropritabs-btn">CUSTOMER Q.A</button></span>   
					        @endif     
					    </div>
					    <div class="feednewtab_content">
					        
					        <div class="feednewprotab_item">
					        	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

								<style type="text/css">

									 .animated {

										    -webkit-transition: height 0.2s;

										    -moz-transition: height 0.2s;

										    transition: height 0.2s;

										}



										.stars

										{

										    margin: 20px 0;

										    font-size: 24px;

										    color: #d17581;

										}

								</style>

								<div class="container" data-aos="{{$shop->animatefaq}}">

									<div class="row" style="margin-top:40px;">

										<div class="col-md-12">

								    	<div class="well well-sm">

								            <div class="text-right">

								                <a class="btn btn-success btn-green" href="#reviews-anchor" id="open-review-box">Leave a Review</a>

								            </div>

								        

								            <div class="row" id="post-review-box" style="display:none;">

								                <div class="col-md-12">

								                    <form accept-charset="UTF-8" action="{{route('company.reviews.submit')}}" method="post">

								                    	{{ csrf_field() }}

								                        <input id="ratings-hidden" name="rating" type="hidden"> 

								                        <input class="form-control" type="hidden" value="{{ $shop->product }}" name="product_id">

								                        <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5"></textarea>

								        

								                        <div class="text-right">

								                            <div class="stars starrr" data-rating="0"></div>

								                            <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">

								                            <span class="glyphicon glyphicon-remove"></span>Cancel</a>

								                            <button class="btn btn-success btn-lg" type="submit">Save</button>

								                        </div>

								                    </form>

								                </div>

								            </div>

								        </div> 

								         

										</div>

									</div>

								</div>   
								
								@if ((App\Models\Review::count('rating')) >= '1')
											
								<div class="minipro-page-reviews-data">

									<div class="minipro-page-reviews-detail-1" data-aos="{{$shop->animatereviews}}">

										<div class="minipro-page-reviews-content">

											<div class="minipro-page-reviews-content-head">

												<h3>{{__('Customer reviews')}}</h3>

											</div>

											<div class="minipro-page-reviews-ls-stars-data">

												<div class="pro-page-reviews-ls-stars">
												    
												 
												
											@php	$rating = round(((App\Models\Review::sum('rating') / (App\Models\Review::count('rating') * 5 )) * 5), 2); @endphp 

										            @foreach(range(1,5) as $i)
										                <span class="fa-stack" style="width:1em">
										                    

										                    @if($rating > 0)
										                        @if($rating < 1)
										                            <i class="fa fa-star-half-o"></i>
										                        @elseif($rating >= 1)
										                            <i class="fa fa-star"></i>
										                        @endif
										                    @else
										                    <i class="fa fa-star-o"></i>
										                    @endif

										                    @php $rating--; @endphp
										                </span>
										            @endforeach
										           

													<!-- <i class="fa fa-star"></i>

													<i class="fa fa-star"></i>

													<i class="fa fa-star"></i>

													<i class="fa fa-star"></i>

													<i class="fa fa-star-half-o"></i> -->

												</div>

												<div class="minipro-page-reviews-ls-stars-text">


                                                    @if(App\Models\Review::where('product_id',$shop->product)->count('rating') != 0)
													<p>{{ round(((App\Models\Review::where('product_id',$shop->product)->sum('rating') / (App\Models\Review::where('product_id',$shop->product)->count('rating') * 5 )) * 5), 2) }} out of 5</p>
                                                    @else
                                                    <p>0 out of 5</p>
                                                    @endif
												</div>

											</div>

											<div class="minipro-page-reviews-global">
												<p>{{ App\Models\Review::where('product_id',$shop->product)->count('rating') }} {{__('global ratings')}}</p>

											</div>

											<!-- 1rating -->

											<div class="minipro-page-ratings-data">

												<div class="minipro-page-ratings-detail-1">

													<p><a type="button" value="5" class="myButton">5 star</a></p>

												</div>

												<div class="minipro-page-ratings-detail-2">
                                                    @if(App\Models\Review::where('product_id',$shop->product)->where('rating',5)->count('rating') != 0)
													<div class="minipro-page-ratings-high-1" style="width: {{ round((App\Models\Review::where('product_id',$shop->product)->where('rating',5)->count('rating') / App\Models\Review::sum('rating') * 100),2) }}%;">

													</div>
                                                    @else
                                                    <div class="minipro-page-ratings-high-1" style="width: 0%;">

													</div>
                                                    @endif
													

													<div class="minipro-page-ratings-low-5">

													</div>

												</div>

												<div class="minipro-page-ratings-detail-3">
                                                    @if(App\Models\Review::where('product_id',$shop->product)->where('rating',5)->count('rating') != 0)
													<p>{{ round((App\Models\Review::where('product_id',$shop->product)->where('rating',5)->count('rating') / App\Models\Review::where('product_id',$shop->product)->sum('rating') * 100),2) }}%</p>
                                                    @else
                                                    <p>0%</p>
                                                    @endif
												</div>

											</div>

											<!-- 2rating -->

											<div class="minipro-page-ratings-data">

												<div class="minipro-page-ratings-detail-1">

													<p><a type="button" value="4" class="myButton">4 star</a></p>

												</div>

												<div class="minipro-page-ratings-detail-2">
                                                    @if(App\Models\Review::where('product_id',$shop->product)->where('rating',4)->count('rating') != 0)
													<div class="minipro-page-ratings-high-2" style="width:{{ round((App\Models\Review::where('product_id',$shop->product)->where('rating',4)->count('rating') / App\Models\Review::where('product_id',$shop->product)->sum('rating') * 100),2) }}%;">
													</div>
                                                    @else
                                                    <div class="minipro-page-ratings-high-2" style="width:0%;"></div>
                                                    @endif
													<div class="minipro-page-ratings-low-2">

													</div>

												</div>

												<div class="minipro-page-ratings-detail-3">
                                                    @if(App\Models\Review::where('product_id',$shop->product)->where('rating',4)->count('rating') != 0)
													<p>{{ round((App\Models\Review::where('product_id',$shop->product)->where('rating',4)->count('rating') / App\Models\Review::where('product_id',$shop->product)->sum('rating') * 100),2) }}%</p>
                                                    @else
                                                    <p>0%</p>
                                                    @endif
												</div>

											</div>

											<!-- 3rating -->

											<div class="minipro-page-ratings-data">

												<div class="minipro-page-ratings-detail-1">

													<p><a type="button" value="3" class="myButton">3 star</a></p>

												</div>

												<div class="minipro-page-ratings-detail-2">
                                                    @if(App\Models\Review::where('product_id',$shop->product)->where('rating',3)->count('rating') != 0)
													<div class="minipro-page-ratings-high-3" style="width: {{ round((App\Models\Review::where('product_id',$shop->product)->where('rating',3)->count('rating') / App\Models\Review::where('product_id',$shop->product)->sum('rating') * 100),2) }}%;"></div>
                                                    @else
                                                    <div class="minipro-page-ratings-high-2" style="width:0%;"></div>
                                                    @endif
													<div class="minipro-page-ratings-low-3">

													</div>

												</div>

												<div class="minipro-page-ratings-detail-3">
                                                    @if(App\Models\Review::where('product_id',$shop->product)->where('rating',3)->count('rating') != 0)
													<p>{{ round((App\Models\Review::where('product_id',$shop->product)->where('rating',3)->count('rating') / App\Models\Review::where('product_id',$shop->product)->sum('rating') * 100),2) }}%</p>
                                                    @else
                                                    <p>0%</p>
                                                    @endif
												</div>

											</div>

											<!-- 4rating -->

											<div class="minipro-page-ratings-data">

												<div class="minipro-page-ratings-detail-1">

													<p><a type="button" value="2" class="myButton">2 star</a></p>

												</div>

												<div class="minipro-page-ratings-detail-2">
                                                    @if(App\Models\Review::where('product_id',$shop->product)->where('rating',2)->count('rating') != 0)
													<div class="minipro-page-ratings-high-4" style="width: {{ round((App\Models\Review::where('product_id',$shop->product)->where('rating',2)->count('rating') / App\Models\Review::where('product_id',$shop->product)->sum('rating') * 100),2) }}%;">

													</div>
													 @else
                                                    <div class="minipro-page-ratings-high-2" style="width:0%;"></div>
                                                    @endif

													<div class="minipro-page-ratings-low-4">

													</div>

												</div>

												<div class="minipro-page-ratings-detail-3">
                                                    @if(App\Models\Review::where('product_id',$shop->product)->where('rating',2)->count('rating') != 0)
													<p>{{ round((App\Models\Review::where('product_id',$shop->product)->where('rating',2)->count('rating') / App\Models\Review::where('product_id',$shop->product)->sum('rating') * 100),2) }}%</p>
                                                    @else
                                                    <p>0%</p>
                                                    @endif
												</div>

											</div>

											<!-- 5rating -->

											<div class="minipro-page-ratings-data">

												<div class="minipro-page-ratings-detail-1">

													<p><a type="button" value="1" class="myButton">1 star</a></p>

												</div>

												<div class="minipro-page-ratings-detail-2">
                                                    @if(App\Models\Review::where('product_id',$shop->product)->where('rating',1)->count('rating') != 0)
													<div class="minipro-page-ratings-high-5" style="width: {{ round((App\Models\Review::where('product_id',$shop->product)->where('rating',1)->count('rating') / App\Models\Review::where('product_id',$shop->product)->sum('rating') * 100),2) }}%;">

													</div>
                                                    @else
                                                    <div class="minipro-page-ratings-high-2" style="width:0%;"></div>
                                                    @endif
													<div class="minipro-page-ratings-low-5">

													</div>

												</div>

												<div class="minipro-page-ratings-detail-3">
                                                    @if(App\Models\Review::where('product_id',$shop->product)->where('rating',1)->count('rating') != 0)
													<p>{{ round((App\Models\Review::where('product_id',$shop->product)->where('rating',1)->count('rating') / App\Models\Review::where('product_id',$shop->product)->sum('rating') * 100),2) }}%</p>
                                                    @else
                                                    <p>0%</p>
                                                    @endif
												</div>

											</div>

											<!-- <div class="minipro-page-ratings-calculate">

												<p><i class="fa fa-angle-down"></i>{{__('How are ratings calculated?')}}</p>

											</div> -->



										</div>

									</div>

								

										<!-- coment-1 -->

										@php 

//echo $genrated_product_GGID;

@endphp



<div class="minipro-page-reviews-detail-2" id="getstardiv">	
										@foreach($reviews as $review)

										<div class="minipro-page-reviews-coments-data" data-star="<?php echo  $review->rating?>">

											<div class="minipro-page-reviews-coments-detail">

												<div class="minipro-page-reviews-coments-img">
													<img src="{{ asset(App\Models\User::find($review->user_id)->avatar_original) }}">

												</div>

												<div class="minipro-page-reviews-coments-img-text">

													<div class="minipro-page-reviews-client-data">

														<div class="minipro-page-reviews-client-name">

															<h4>{{  App\Models\User::find($review->user_id)->name }}</h4>

														</div>

														<div class="minipro-page-reviews-client-stars">

															@for($i=0; $i < $review->rating; $i++)

															<i class="fa fa-star"></i>

															@endfor

															@for($i=$review->rating; $i < 5; $i++)

															<i class="fa fa-star-o"></i>

															@endfor

														</div>

														

													</div>

													<div class="minipro-page-reviews-client-msg">

														<p>{{__($review->comment)}}</p>

													</div>

												</div>

											</div>

										</div>

										

										@endforeach


									</div>

								</div>
								
								@endif
					        </div>
					        @if($shop->questions != null and !empty($shop->questions))
					        <div class="feednewprotab_item" >
									<div class="minipro-page-ques-head">
										<h2>CUSTOMER QUESTION AND ANSWERS</h2>
									</div>
									<!-- <div class="minipro-page-ques-input">
										<form class="minipro-page-ques-form">
											<button type="submit"><i class="fa fa-search"></i></button>
										  	<input type="text" placeholder="Have aquestion? Search for answers" name="search">
										  
										</form>
									</div> -->
									<div class="minipro-page-ques-head">
											<h4>{{__('SUBMIT YOUR QUESTION')}}</h4>
										</div>
										<div class="minipro-page-ques-input">

											<form class="form-inline" method="POST" action="{{route('company.question.submit')}}">

												{{ csrf_field() }}

												<input class="form-control" type="hidden" value="{{ $shop->slug }}" name="slug">

												<input class="form-control col-md-10" style="margin-bottom: 0px;" type="text" placeholder="Have aquestion? " name="question">

												<button type="submit" name="save-question" class="btn btn-success col-md-2 focuschange" id="save-question">Submit</button>

											</form>

										</div>
									<!-- faq 1 -->
									@foreach(json_decode($shop->questions) as $key => $question)
									@if($question->status == 1)
									<div class="minipro-page-ques-data">
										<div class="minipro-page-ques-datail-1">
											<div class="minipro-page-votes-icon">
												<i class="fa fa-caret-up"></i>
											</div>
											<div class="minipro-page-votes-icon-text">
												<p>8</p>
												<p>votes</p>
											</div>
											<div class="minipro-page-votes-icon">
												<i class="fa fa-caret-down"></i>
											</div>
										</div>
										<div class="minipro-page-ques-datail-2">
											<div class="minipro-page-ques-text-data">
												<div class="minipro-page-ques-text-h-1">
													<h3>Question:</h3>
												</div>
												<div class="minipro-page-ques-text-h-2">
													<h3> {{ $question->question }}</h3>
												</div>
											</div>
											<div class="minipro-page-ques-ans-data">
												<div class="minipro-page-ans-text-h-1">
													<h3>Answer:</h3>
												</div>
												@php

													$created_at = strtotime($shop->created_at);

												@endphp

																
												<div class="minipro-page-ans-ans-h-2">
													<p>{{ $question->answer }}</p>
													<div class="minipro-page-ans-analys">
														<p>By analyst on {{ date('Fd, Y',$created_at) }}</p>
													</div>
													<!-- <div class="minipro-page-ans-see-more">
														<p><i class="fa fa-angle-down"></i>See more answers[7]</p>
													</div> -->
												</div>
											</div>
											
										</div>
									</div>
									@endif
									@endforeach
										
									
						    </div>
						    @endif
					    </div>
					</div>
				</div>
			</div>
		</section>
				
	@endif
			<!-- Modal improve -->
							<div class="modal fade" id="improveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">HOW CAN WE IMPROVE OUR PRODUCT</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <form action="{{ route('company.commentsubmit') }}" method="POST">
							      <div class="modal-body">

							        	{{ csrf_field() }}
										<input class="form-control" type="hidden" value="{{ $shop->id }}" name="shop_id">
										<input class="form-control" type="hidden" value="{{ $shop->user_id }}" name="user_id">
										  <div class="form-group">
										    <input type="email" class="form-control" name="email" id="exampleFormControlInput1" placeholder="name@example.com">
										  </div>
										  <div class="form-group">
										    <textarea name="comment" rows="4" cols="50" style="width: 100%;height: 200px;border: 1px solid #e8e8e8;" placeholder="HOW CAN WE IMPROVE OUR PRODUCT..."></textarea>
										  </div>
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							        <button type="submit" class="btn btn-primary">Submit</button>
							      </div>
							      </form>
							    </div>
							  </div>
							</div>
	<!-- related products -->

	@if($shop->related_products != null and $shop->product_type != "guide")

		@php

			$related_products = json_decode($shop->related_products);

		@endphp

		<section class="minipro-page-related-sec minibgcolor">

			<div class="minipage">

				<div class="minipro-page-related-head">

					<h2>{{__('TRY OUR ADDITIONAL ITEMS')}}</h2>

				</div>
	    <div style="border-radius: 50%;bottom: 20px;position: fixed; background-color:blue;" >
    <a href="{{'/user/register2'}}?vid={{$shop->user_id}}" onclick="window.open(this.href,'newwindow','width=800,height=800'); return false;">
        <img src="{{asset('frontend/images/chat.png')}}" style="width: 80px" alt="">
    </a>
</div>
{{-- <a href="{{'/user/register2'}}" class="btn btn-danger">Chat</a> --}}
				<div class="minipro-page-related-hr">

					<hr>

				</div>

				<div class="minipro-page-related-data" data-aos="{{$shop->animateproducts}}">

					@foreach($related_products as $key => $val)

						@if(App\Models\Product::where('id', $val)->count() > 0)

							@php

								$product = App\Models\Product::where('id', $val)->first();

								$main_photo = '';



								if(!empty($product->photos)){

									$photos = json_decode($product->photos);

									$main_photo = $photos[0];

								}

							@endphp

							<div class="minipro-page-related-detail">

								<div class="minipro-page-related-pics" style="cursor: pointer;">

									<a data-refrence="{{ route('singleproduct',$product->slug) }}" onclick="checkProduct(event,this)" id="productCheck" data-slug="{{$shop->slug}}"  data-auth="{{Auth::check()}}" related-check="{{$shop->related_check}}"><img style="object-fit: contain;" src="{{asset($main_photo)}}"></a>

								</div>

								<div class="minipro-page-related-pics-text" style="cursor: pointer;">

									<a data-refrence="{{ route('singleproduct',$product->slug) }}" onclick="checkProduct(event,this)" id="productCheck" data-slug="{{$shop->slug}}"  data-auth="{{Auth::check()}}" related-check="{{$shop->related_check}}"><h4>{{$product->name}}</h4></a>

								</div>

							</div>

						@endif

					@endforeach

				</div>

			</div>

		</section>

	@endif

@endsection

</body>

@section('script')

<script type="text/javascript">
AOS.init({
  	once: true,
  });
@if($shop->c_active != 0)

    $(window).on('load', function() {

        $('#coupon_active_modal').modal('show');

    });

@endif
@if($shop->coupon_id != 0)

    $(window).on('load', function() {

        $('#coupon_modal_active').modal('show');

    });

@endif
    $(function () {

		// tabs

		$(".mininewpropriwrapper .mininewpropritab").click(function() {

		$(".mininewpropriwrapper .mininewtabpropri").removeClass("active").eq($(this).index()).addClass("mininewpropractive");

		$(".mininewprotab_item").hide().eq($(this).index()).fadeIn()

		}).eq(0).addClass("active");

	});



	function myFunction() {

		var miniinfodots = document.getElementById("miniinfodots");

		var miniinfomoreText = document.getElementById("miniinfomore");

		var miniinfobtnText = document.getElementById("miniinfomyBtn");



		if (miniinfodots.style.display === "none") {

			miniinfodots.style.display = "inline";

			miniinfobtnText.innerHTML = "Read more";

			miniinfomoreText.style.display = "none";

		} else {

			miniinfodots.style.display = "none";

			miniinfobtnText.innerHTML = "Read less";

			miniinfomoreText.style.display = "inline";

		}

	}

	// Reviews JS

		(function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o;"getComputedStyle"in window?(t=window.getComputedStyle(u,null),o=u.getBoundingClientRect().width,e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){o-=parseInt(t[i],10)}),s.style.width=o+"px"):s.style.width=Math.max(p.width(),0)+"px"}function a(){var a={};if(t=u,s.className=i.className,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width();p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word",resize:"none"===p.css("resize")||"vertical"===p.css("resize")?"none":"horizontal"}),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);



			var __slice=[].slice;(function(e,t){var n;n=function(){function t(t,n){var r,i,s,o=this;this.options=e.extend({},this.defaults,n);this.$el=t;s=this.defaults;for(r in s){i=s[r];if(this.$el.data(r)!=null){this.options[r]=this.$el.data(r)}}this.createStars();this.syncRating();this.$el.on("mouseover.starrr","span",function(e){return o.syncRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("mouseout.starrr",function(){return o.syncRating()});this.$el.on("click.starrr","span",function(e){return o.setRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("starrr:change",this.options.change)}t.prototype.defaults={rating:void 0,numStars:5,change:function(e,t){}};t.prototype.createStars=function(){var e,t,n;n=[];for(e=1,t=this.options.numStars;1<=t?e<=t:e>=t;1<=t?e++:e--){n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))}return n};t.prototype.setRating=function(e){if(this.options.rating===e){e=void 0}this.options.rating=e;this.syncRating();return this.$el.trigger("starrr:change",e)};t.prototype.syncRating=function(e){var t,n,r,i;e||(e=this.options.rating);if(e){for(t=n=0,i=e-1;0<=i?n<=i:n>=i;t=0<=i?++n:--n){this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass("glyphicon-star")}}if(e&&e<5){for(t=r=e;e<=4?r<=4:r>=4;t=e<=4?++r:--r){this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass("glyphicon-star-empty")}}if(!e){return this.$el.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty")}};return t}();return e.fn.extend({starrr:function(){var t,r;r=arguments[0],t=2<=arguments.length?__slice.call(arguments,1):[];return this.each(function(){var i;i=e(this).data("star-rating");if(!i){e(this).data("star-rating",i=new n(e(this),r))}if(typeof r==="string"){return i[r].apply(i,t)}})}})})(window.jQuery,window);$(function(){return $(".starrr").starrr()})



	$(function(){



		  $('#new-review').autosize({append: "\n"});



		  var reviewBox = $('#post-review-box');

		  var newReview = $('#new-review');

		  var openReviewBtn = $('#open-review-box');

		  var closeReviewBtn = $('#close-review-box');

		  var ratingsField = $('#ratings-hidden');



		  openReviewBtn.click(function(e)

		  {

		    reviewBox.slideDown(400, function()

		      {

		        $('#new-review').trigger('autosize.resize');

		        newReview.focus();

		      });

		    openReviewBtn.fadeOut(100);

		    closeReviewBtn.show();

		  });



		  closeReviewBtn.click(function(e)

		  {

		    e.preventDefault();

		    reviewBox.slideUp(300, function()

		      {

		        newReview.focus();

		        openReviewBtn.fadeIn(200);

		      });

		    closeReviewBtn.hide();



		  });



		  $('.starrr').on('starrr:change', function(e, value){

		    ratingsField.val(value);

		  });

	});
	$('.mininewpropritabs-btn').on('click', function(){
     $('.mininewpropritabs-btn').removeClass('active');
     $('.mininewpropritabs .mininewpropritab').removeClass('active');
     $(this).addClass('active');
   });
$(function () {
		// customer feedback tabs

	    $(".feednewpropriwrapper .feednewpropritab").click(function() {
		$(".feednewpropriwrapper .feednewtabpropri").removeClass("active").eq($(this).index()).addClass("feednewpropractive");
		$(".feednewprotab_item").hide().eq($(this).index()).fadeIn()
		}).eq(0).addClass("active");

	});
</script>
<script>

    function checkProduct(e , el)
    {
        // console.log(el.getAttribute('data-refrence'));
        $href = el.getAttribute('data-refrence');
        $status = $("#productCheck").attr('related-check');
        $slug = $("#productCheck").attr('data-slug');
        console.log($href);
        if($status == "on")
        {
            $auth = $("#productCheck").attr('data-auth');
          if($auth == 1)
            {
                window.location.href = $href;
            }
            else
            {
                var url = "{{url('/users/login')}}";
                window.location.href = url;
            }
        }
        else
        {
            window.location.href = $href;
        }
    }
    
    // function fbCheck()
    // {
    //     $status = $("#fbLink").attr('sm-check');
    //     $slug = $("#fbLink").attr('data-slug');
    //     // console.log($status);
    //     if($status == "on")
    //     {
    //         $auth = $("#fbLink").attr('data-auth');
    //         // cosnole.log($auth);
    //         if($auth == 1)
    //         {
    //             window.location.href = "https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}";
    //         }
    //         else
    //         {
    //             var url = "{{url('/users/login')}}";
    //             window.location.href = url;
    //         }
    
    //     }
    //     else
    //     {
    //           window.location.href = "https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}";
    //     }
    // }
    

    // function twCheck()
    // {
    //     alert("ok");
    //     $status = $("#twLink").attr('sm-check');
    //     $slug = $("#twLink").attr('data-slug');
    //     // console.log($status);
    //     if($status == "on")
    //     {
    //         $auth = $("#twLink").attr('data-auth');
    //         // cosnole.log($auth);
    //         if($auth == 1)
    //         {
    //             window.location.href = "https://twitter.com/intent/tweet?text={{ url()->current() }}";
    //         }
    //         else
    //         {
    //             var url = "{{url('/users/login')}}";
    //             window.location.href = url;
    //         }
    
    //     }
    //     else
    //     {
    //           window.location.href = "https://twitter.com/intent/tweet?text={{ url()->current() }}";
    //     }
    // }
    
    function checkClub()
    {
        $status = $("#clubCheck").attr('club-check');
        $slug = $("#clubCheck").attr('data-slug');
        console.log($slug);
        if($status == "on")
        {
            $auth = $("#clubCheck").attr('data-auth');
            if($auth == 1)
            {
                var url = "{{url('/club-info/')}}";
                window.location.href = url + '/' +$slug;
            }
            else
            {
                var url = "{{url('/users/login')}}";
                window.location.href = url;
            }
        }
        else
        {
            var url = "{{url('/club-info/')}}";
            window.location.href = url + '/' +$slug;
        }
    }
    function checkWarranty()
    {
        $status = $("#warrantyCheck").attr('warranty-check');
        $route = $("#warrantyCheck").attr('warranty-route');
        if($status == "on")
        {
            $auth = $("#warrantyCheck").attr('data-auth');
            console.log($auth);
            if($auth == 1)
            {
                window.location.href = $route;
            }
            else
            {
                var url = "{{url('/users/login')}}";
                window.location.href = url;
            }
        }
        else
        {
            window.location.href = $route;
        }
    }
    
    
$('.myButton').click(function(event){

	 $("#myModalx").css("display","block");

    var getstars = $(this).attr('value');
    $("#modal-body2").empty();
    $('#getstardiv').find( "div[data-star='"+getstars+"']" ).each(function(){



    	$('#modal-body2').prepend($(this).html());


    });
});
$("#save-question").click(function() {
   swal("Your question has been submited", "You will be replied soon.", "success");
});


var modal = document.getElementById("myModalx");

// Get the button that opens the modal
var btn = document.getElementsByClassName("myButton");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("closes")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
$( document ).ready(function() {
    $(".feednewpropritabs-btn").click(function(){
		   $(".feednewpropritabs-btnfocus").removeClass("feednewpropritabs-btnfocus");
		   $(this).addClass("feednewpropritabs-btnfocus");
		  }); 
});
 
function pdfCheckOrg()
    {
        var path = $("#pdfCheck").attr('data-path');
        // alert($path);
        $status = $("#pdfCheckOrignal").attr('pdf-check');
        $slug = $("#pdfCheckOrignal").attr('data-slug');
        console.log($status);
        if($status == "on")
        {
            $auth = $("#pdfCheckOrignal").attr('data-auth');
            // cosnole.log($auth);
            if($auth == 1)
            {
                // alert($path);
                $pathurl = "{{url('/public')}}";
                // window.open(path);
            }
            else
            {
                var url = "{{url('/users/login')}}";
                window.location.href = url;
            }
    
        }
        else
        {
             window.open(path);
        }
    }
    function checkPdfTrans()
    {
        var path = $("#pdfCheck").attr('data-path');
        // alert("ok");
        $status = $("#pdfCheckTrans").attr('pdf-check');
        $slug = $("#pdfCheckTrans").attr('data-slug');
        console.log($status);
        if($status == "on")
        {
            $auth = $("#pdfCheckTrans").attr('data-auth');
            // cosnole.log($auth);
            if($auth == 1)
            {
                // window.open('<?php echo url('/public') . '/' . '' ?>');
            }
            else
            {
                var url = "{{url('/users/login')}}";
                window.location.href = url;
            }
    
        }
        else
        {
             window.open('http://translate.google.com/translate?sl=auto&tl=en&u='+path);
        }
    }
    function checkPdfDownload()
    {
        $path = $("#pdfCheck").attr('data-path');
        $status = $("#pdfCheck").attr('pdf-check');
        $slug = $("#pdfCheck").attr('data-slug');
        console.log($status);
        if($status == "on")
        {
            $auth = $("#pdfCheck").attr('data-auth');
            // cosnole.log($auth);
            if($auth == 1)
            {
                window.open('<?php echo url('/public') . '/' . '' ?>')
            }
            else
            {
                var url = "{{url('/users/login')}}";
                window.location.href = url;
            }
    
        }
        else
        {
            window.open($path);
        }
    }
// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}
</script>
@endsection
