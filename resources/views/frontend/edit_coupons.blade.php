@extends('frontend.layouts.new_app_1')
 
<style type="text/css">
<!-- All css -->

body{ 
    background-color: #F7F7F7;
}
* {
    margin: 0;
    padding: 0;
    box-sizing: 0;
}
.support-ticket{
    padding: 50px 0px;
}    
.spt-flex-1{
    display: flex;
    justify-content: center;
}
.spt-flex-2{
    display: flex;
    justify-content: space-between;
}
.spt-page {
    width: 80%;
}
.spt-pg-inner  {
    width: 100%;
}
.spt-inners-pg-1 {
    width: 100%;
}
.spt-inners-pg-1 h3 {
    text-align: center;
    font-weight: bolder;
    font-size: 30px;
    margin-bottom: 20px;
}
.spt-inners-pg-2 table {
    width: 100%;
}
.spt-inners-pg-2 thead th {
    background-color: #000000;
    color:#fff;
    padding: 5px 0px 5px 10px;
    border-right: 2px solid #F7F7F7;
    font-size: 16px;
    font-weight: 400;
}
.spt-custom-th {
    background-color: #1B9FB6!important;
}
.spt-inners-pg-2  table {
    width: 100%;
    box-shadow: #e3e3e3 0px 0px 12px 0px;
    overflow-x: auto;
}
.spt-tr-cst {
    border-bottom: 1px solid #000000;
    margin-bottom: 5px;
    background-color: #fff;
}
.spt-tr-cst th {
    border-right: 2px solid #F7F7F7;
    padding: 20px 0px 20px 10px;
    font-size: 16px;
    font-weight: bold;
}
.spt-tr-cst  button{
    background-color: #FC6E62;
    color: #fff;
    border:unset;
    padding: 1px 30px 1px 30px;
    font-size: 16px;
    font-weight: 400;
    outline: unset;
}
.spt-tr-cst  button a{
    color: #fff;
    font-size: 16px;
    font-weight: 400;
}
.spt-inners-pg-3 {
    display: flex;
    justify-content: center;
    width: 100%;
    padding-top: 10px;
}
.spt-inners-pg-3 button{
    width: 100%;
    text-align: center;
    border: unset;
    background-color: #D1EAEF;
    padding: 25px 0px;
}
.spt-inners-pg-3 button:hover{
    width: 100%;
    text-align: center;
    border: unset;
    background-color: #D1EAEF;
    padding: 25px 0px;
}
.spt-inners-pg-3 button:focus{
    width: 100%;
    text-align: center;
    border: unset;
    background-color: #D1EAEF;
    padding: 25px 0px;
}
.spt-inners-pg-3 button a{
    color: #000;
    font-size: 20px;
    font-weight: bold;
}
/* modal */
.mdt-cst {
    background-color: #000;
    width: 100%;
    color: #fff;
    padding:0px 0px 5px 10px;
    font-weight: 400;
    font-size: 19px!important;
}
.mdt-cst-btn {
    background-color: #FC6E61!important;
    color: #fff!important;
    padding: 1px 10px 4px 10px!important;
    margin: unset!important;
    line-height: unset!important;
    font-size: 19px!important;
}
.mdt-cst-btn span{
    background-color: #FC6E61!important;
    color: #fff!important;
    line-height: 1.5!important;
}
.spt-modal-inner {
    display: flex;
    justify-content: center;
    width: 100%;
}
.spt-modal-inner input {
    width: 100%;
    padding: 6px 0px 6px 8px;
    border:1px solid #1B9FB6;
    outline-color: #1B9FB6;
}
.cst-btn-modal-1 {
    background-color: #000!important;
    color: #fff!important;
    padding: 2px 35px!important;
    border-radius: 2px!important;
}
.cst-btn-modal-2 {
    background-color: #1B9FB6!important;
    color: #fff!important;
    padding: 2px 35px!important;
    border-radius: 2px!important;
}
.border-none {
    border-bottom: none!important;
    border-top: none!important;
}

/* text box */
		.editor
        {
			border:solid 1px #ccc;
			padding: 20px;
			min-height:200px;
        }

        .sample-toolbar
        {
			border:solid 1px #ddd;
			background:#f4f4f4;
			padding: 5px 5px 5px 5px;
			border-radius:3px;
            margin-top: 15px;
        }

        .sample-toolbar > span
        {
			cursor:pointer;
		}

        .sample-toolbar > span:hover
        {
			text-decoration:underline;
		}
@media screen and (max-width: 768px) {
    .spt-inners-pg-2 table{
        width: 600px;
    }
    .spt-inners-pg-2 {
        overflow: auto;
    }
    
}
</style>

@section('content')
<section class="support-ticket">    
    <div class="spt-flex-1">
        <div class="spt-page">
            <div class="spt-flex-2">
                <div class="spt-pg-inner">
                    <div class="spt-inners-pg-1">
                        <h3>Coupon Information Update</h3>
                    </div>    
                      
                    <form class="form-horizontal" action="{{ route('seller_coupon.edit', $coupon->id) }}" method="POST" enctype="multipart/form-data">
                        
                    	@csrf
                        <div class="panel-body">
                            <input type="hidden" name="id" value="{{ $coupon->id }}" id="id">
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="name">{{__('Coupon Type')}}</label>
                                <div class="col-lg-9">
                                    <select name="coupon_type" id="coupon_type" class="form-control demo-select2-placeholder" onchange="coupon_form()" required>
                                        @if ($coupon->type == "product_base"))
                                            <option value="product_base" selected>{{__('For Products')}}</option>
                                        @elseif ($coupon->type == "cart_base")
                                            <option value="cart_base">{{__('For Total Orders')}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
        
                            <div id="coupon_form">
        
                            </div>
        
                        <div class="panel-footer text-right">
                            <button class="btn btn-success" type="submit">{{__('Save')}}</button>
                        </div>
                    </form>

                </div>
            </div> 
        </div> 
    </div>   
</section>

@endsection
@section('script')

<script type="text/javascript">

    function coupon_form(){
        var coupon_type = $('#coupon_type').val();
        var id = $('#id').val();
		$.post('{{ route('coupon.seller_get_coupon_form_edit') }}',{_token:'{{ csrf_token() }}', coupon_type:coupon_type, id:id}, function(data){
            $('#coupon_form').html(data);

            $('#demo-dp-range .input-daterange').datepicker({
                startDate: '-0d',
                todayBtn: "linked",
                autoclose: true,
                todayHighlight: true
        	});
		});
    }

    $(document).ready(function(){
        coupon_form();
    });


</script>

@endsection
