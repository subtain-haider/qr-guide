<div class="sidebar sidebar--style-3 no-border stickyfill p-0">
    <div class="widget mb-0">
        <div class="widget-profile-box text-center p-3">
            <div class="image" style="background-image:url('{{ asset(Auth::user()->avatar_original) }}')"></div>
            {{-- @if(Auth::user()->seller->verification_status == 1)
                <div class="name mb-0">{{ Auth::user()->name }} <span class="ml-2"><i class="fa fa-check-circle" style="color:green"></i></span></div>
            @else
                <div class="name mb-0">{{ Auth::user()->name }} <span class="ml-2"><i class="fa fa-times-circle" style="color:red"></i></span></div>
            @endif --}}
        </div>
        <div class="sidebar-widget-title py-3">
            <span>{{__('Menu')}}</span>
        </div>
        @php
            $PackageInfo = \App\Models\PricingPlan::where('id', Auth::user()->shop->package)->first();
            $PP = Auth::user()->shop->package_plane;
            $Permissions = json_decode($PackageInfo->permissions);
        @endphp
        <div class="widget-profile-menu py-3">
            <ul class="categories categories--style-3">
                <li>
                    <a href="{{ route('dashboard') }}" class="{{ areActiveRoutesHome(['dashboard'])}}">
                        <i class="la la-dashboard"></i>
                        <span class="category-name">
                            {{__('Dashboard')}}
                        </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('seller.products') }}" class="{{ areActiveRoutesHome(['seller.products', 'seller.products.upload', 'seller.products.edit'])}}">
                        <i class="la la-diamond"></i>
                        <span class="category-name">
                            {{__('Products')}}
                        </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('orders.index') }}" class="{{ areActiveRoutesHome(['orders.index'])}}">
                        <i class="la la-file-text"></i>
                        <span class="category-name">
                            {{__('Orders')}}
                        </span>
                    </a>
                </li>
                @if(in_array(2,$Permissions))
                <li>
                    <a href="{{ route('company.index') }}" class="{{ areActiveRoutesHome(['company.index'])}}">
                        <i class="la la-cog"></i>
                        <span class="category-name">
                            {{__('Company Setting')}}
                        </span>
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{ route('company.warranty') }}" class="{{ areActiveRoutesHome(['company.warranty'])}}">
                        <i class="la la-cog"></i>
                        <span class="category-name">
                            {{__('Company Warranty')}}
                        </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('packageinfo') }}" class="{{ areActiveRoutesHome(['packageinfo'])}}">
                        <i class="la la-cog"></i>
                        <span class="category-name">
                            {{__('Package info')}}
                        </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('profile') }}" class="{{ areActiveRoutesHome(['profile'])}}">
                        <i class="la la-user"></i>
                        <span class="category-name">
                            {{__('Manage Profile')}}
                        </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('question.index') }}" class="{{ areActiveRoutesHome(['question.index'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Questions')}}
                        </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('support_ticket.index') }}" class="{{ areActiveRoutesHome(['support_ticket.index'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Support Ticket')}}
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
