@extends('frontend.layouts.customer_app')

@section('content')



    <section class="gry-bg py-4 profile">

        <div class="dashboard-2-users-top-content-all">
					<div class="dashboard-2-users-top-content-all-page">
						<div class="dashboard-2-users-top-content-all-head">

                    <div class="main-content">

                        <!-- Page title -->

                        <div class="page-title">

                            <div class="row align-items-center">

                                <div class="col-md-6 col-12">

                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0">

                                        {{__('Purchase History')}}

                                    </h2>

                                </div>

                                <!--<div class="col-md-6 col-12">-->

                                <!--    <div class="float-md-right">-->

                                <!--        <ul class="breadcrumb">-->

                                <!--            <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>-->

                                <!--            <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>-->

                                <!--            <li class="active"><a href="{{ route('purchase_history.index') }}">{{__('Purchase History')}}</a></li>-->

                                <!--        </ul>-->

                                <!--    </div>-->

                                <!--</div>-->

                            </div>

                        </div>

                            <!-- Order history table -->
                            <div class="card no-border mt-4">
                                <div>
                                    <table class="table table-sm table-hover table-responsive-md">
                                        <thead>
                                            <tr>
                                                <th>{{__('Code')}}</th>
                                                <th>{{__('Date')}}</th>
                                                <th>{{__('Amount')}}</th>
                                                <th>{{__('Delivery Status')}}</th>
                                                <th>{{__('Payment Status')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($orders) > 0)
                                                @foreach ($orders as $key => $order)
                                                <tr>
                                                    <td>
                                                        <a href="#{{ $order->code }}" onclick="show_purchase_history_details({{ $order->id }})">{{ $order->code }}</a>
                                                    </td>
                                                    <td>{{ date('d-m-Y', $order->date) }}</td>
                                                    <td>
                                                        {{ single_price($order->grand_total) }}
                                                    </td>
                                                    <td>
                                                        @php
                                                            $status = '';
                                                            if($order->orderDetails->first()){
                                                                $status = $order->orderDetails->first()->delivery_status;
                                                            }
                                                            //$status = $order->orderDetails->first()->delivery_status;    
                                                            // print_r($order->orderDetails->first());
                                                            // $status = $order->delivery_status;

                                                        @endphp
                                                        {{ ucfirst(str_replace('_', ' ', $status)) }}
                                                    </td>
                                                    <td>
                                                        <span class="badge badge--2 mr-4">
                                                            @if ($order->payment_status == 'paid')
                                                                <i class="bg-green"></i> {{__('Paid')}}
                                                            @else
                                                                <i class="bg-red"></i> {{__('Unpaid')}}
                                                            @endif
                                                        </span>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6">
                                                        Sorry No Data found........!
                                                    </td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        <div class="pagination-wrapper py-4">

                            <ul class="pagination justify-content-end">

                                {{ $orders->links() }}

                            </ul>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>



    <div class="modal fade" id="order_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">

            <div class="modal-content position-relative">

                <div class="c-preloader">

                    <i class="fa fa-spin fa-spinner"></i>

                </div>

                <div id="order-details-modal-body">



                </div>

            </div>

        </div>

    </div>



@endsection

