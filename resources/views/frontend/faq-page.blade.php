@extends('frontend.layouts.new_app_1')
<!-- faq-page -->
@section('content')
<section class="faq-page-sec">
	<div class="faq-page-sec-wdth">
		<div class="faq-page-sec-head">
			<h2>FAQS</h2>
			<p>QUESTIONS WE GET ASKED A LOT</p>
		</div>
		<div class="faq-page-sec-data">
			<div id="accordion faq-page-acordian-all">
 
				@foreach($faqs as $key => $faq)

			  <div class="card faq-page-detail-card-area">
			    <div class="card-header faq-page-detail-heding-data" id="headingOne">
			      <h5 class="mb-0 faq-page-detail-hedng">
			        <button class="btn btn-link faq-page-detail-butn faq-page-detail-butn2" data-toggle="collapse" data-target="#collapseOne{{$key}}" aria-expanded="true" aria-controls="collapseOne">
			         <p class="faq-page-detail-butn-spn"> {{$key+1}}- {{ $faq->question }}</p><i class="fa fa-chevron-down"></i>
			        </button>
			      </h5>
			    </div>

			    <div id="collapseOne{{$key}}" class="collapse faq-page-dtail-cols" aria-labelledby="headingOne" data-parent="#accordion">
			      <div class="card-body show faq-page-dtail-cd-bd">
			     {{ $faq->answer }}
			      </div>
			    </div>
			  </div>

			  @endforeach

			 			  </div>

			</div>
		</div>
	
</section>
@endsection
@section('script')
<script type="text/javascript">
 $('.faq-page-detail-butn2').removeClass('faq-page-detail-butn2');
        	$('.faq-page-detail-butn').on('click', function(){
		  
		        $('.faq-page-detail-butn2').removeClass('faq-page-detail-butn2');
		     $(this).addClass('faq-page-detail-butn2');
		   });

</script>
@endsection    