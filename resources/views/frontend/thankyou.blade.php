@extends('frontend.layouts.new_app_1')

@section('content')

    <section class="th-purchase">
        <div class="purchase-flex-1">
            <div class="purchase-page purchase-flex-1">
                <h1>{{__('thank you for your purchase')}}</h1>
                <p>{{__('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.')}}</p>
                <div><button class="custom-btn"><a href="{{route('home')}}">{{__('go to home')}}</a></button></div>
                <div><button><a href="#">{{__('back to dashboard')}}</a></button></div>
            </div>
        </div>
        <div class="bg-background"></div>
    </section>    

@endsection

@section('script')
<script type="text/javascript">
    (function($) {
        var currentChart;
        
        function setChartDimensions() {
            var width = $("#chart").width(),
                height = $("#footer").offset().top - $("canvas").offset().top - 20;
            if (currentChart) {
                currentChart.chart.aspectRatio = 100 / 100;
            } else {
                $("canvas").attr("width", width);
                $("canvas").attr("height", height);
            }
        }
        
        function drawChart() {
            var data = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(220,220,220,0.2)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [65, 59, 80, 81, 56, 55, 40]
                    },
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [28, 48, 40, 19, 86, 27, 90]
                    }
                ]
            };
            setChartDimensions();
            var ctx = $("canvas")[0].getContext("2d");
            currentChart = new Chart(ctx).Line(data, {animation: false, responsive: false});
        }
        
        window.onload = function() {
            drawChart();
            $(window).resize(function() {
                if (currentChart) {
                    setChartDimensions();
                    currentChart.stop();
                    currentChart.resize(currentChart.render, true);
                }
            });
        };
    })(jQuery);

</script>
@endsection