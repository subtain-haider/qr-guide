@extends('frontend.layouts.new_app_1')
<!-- faq-page -->
<style>
    .api-ban-sec{
	display: flex;
	justify-content: center;
	padding: 35px 0px;
	background-image: url('https://myqrguide.com/frontend/images/api/bner.png');
	background-size: 100% 100%;
	background-position: center center;
	background-repeat: no-repeat;
}
.api-ban-wdth{
	width: 100%;
}
.api-bandetail-1-text h1{
	font-family: 'Play';
	font-size: 52px;
	font-weight: 700;
}
.api-bandetail-1-text{
	padding: 0px 60px;
}
.api-bandetail-1-text p{
	font-family: 'Play';
	font-size: 16px;
}
.api-ban-data{
	display: flex;
	justify-content: space-between;
}
.api-bandetail-1{
	width: 48%;
}
.api-bandetail-2{
	width: 48%;
}
.api-bandetail-2-img{
	height: 530px;
}
.api-bandetail-2-img img{
	width: 100%;
	height: 100%;
}
.api-abt-sec{
	display: flex;
	justify-content: center;
	align-items: center;
	padding: 50px 0px;
	background-color: #F6F9FF;
	height: 600px;
	overflow: hidden;
	
}
.api-abt-wdth{
	width: 100%;
}
.api-abt-data{
	display: flex;
	justify-content: space-between;
	align-items: center;

}
.api-abt-detail-1{
	width: 45%;
	padding: 50px 0px;
	background-image: url('https://myqrguide.com/frontend/images/api/abt.png');
	background-size: 100% 100%;
	background-position: center center;
	background-repeat: no-repeat; 
}
.api-abt-detail-2 .api-abt-img1{
	position: absolute;
	top: 0px;
	left: 30%;
}
.api-abt-detail-2 .api-abt-img2{
	position: absolute;
	top: 60px;
	left: 0px;
}
.api-abt-detail-2 .api-abt-img3{
	position: absolute;
	top: 70%;
	left: 0px;
}
.api-abt-detail-2 .api-abt-img4{
	position: absolute;
	top: 70%;
	left: 30%;
}
.api-abt-detail-2 .api-abt-img5{
	position: absolute;
	top: 40%;
	left: 15%;
}
.api-abt-detail-2 .api-abt-img6{
	position: absolute;
	top: 36%;
	left: 40%;
}
.api-abt-detail-text{
	padding: 0px 30px 0px 50px;
}
.api-abt-detail-text h2{
	font-family: 'Play';
	font-size: 32px;
	color: #fff;
}
.api-abt-detail-text p{
	font-family: 'Play';
	font-size: 16px;
	color: #fff;
	margin-top: 20px;
}
.api-abt-detail-2{
	width: 45%;
	height: 500px;
	position: relative;
}
.api-abt-detail-btn{
	margin-top: 20px;
}
.api-abt-detail-btn button{
	background-color: #000;
	color: #fff;
	padding: 5px 20px 5px 60px;
	outline: none;
	border: none;
	font-family: 'Play';
}
.api-abt-detail-btn i{
	padding-left: 20px;
}
.api-abt-detail-2 img{
	width: 100px;
}
.api-struc-sec{
	display: flex;
	justify-content: center;
	padding: 50px 0px;
	margin-top: 30px;
	background-color: #F6F9FF;
}
.api-struc-wdth{
	width: 94%;
}
.api-struc-data img{
	width: 100%;
}
.api-icns-sec{
	display: flex;
	justify-content: center;
	padding: 30px 0px;
	margin-top: 30px;
	background-color: #F6F9FF;
}
.api-icns-wdth{
	width: 75%;
}
.api-icns-data{
	display: flex;
	justify-content: space-between;
	flex-wrap: wrap;

}
.api-icns-detail{
	background-color: #fff;
	border-radius: 8px;
	width: 30%;
	padding: 50px;
	text-align: center;
	margin-top: 20px;
}
.api-icns-img img{
	width:60px;
	height: 60px;
}
.api-icns-text{
	margin-top: 30px;
}
.api-icns-text p{
	font-family: 'Play';
	font-size: 18px;
	font-weight: 600;
}
.api-icns-detail-clr{
	background-image: linear-gradient(to right, #1BB8D2 , #1990A5);
}
.api-icns-text-clr{
	color: #fff;
}
@media only screen and (max-width: 850px) {
	.api-abt-data{
		flex-direction: column;
	}
	.api-abt-detail-2{
		height: unset;
		display: flex;
		justify-content: space-between;
		flex-wrap: wrap;
	}
	.api-abt-detail-2 img{
		width: 45%;
	}
	.api-abt-sec{
		height: unset;
		overflow: unset;
	}
	.api-abt-detail-2 .api-abt-img1{
		position: unset;
	}
	.api-abt-detail-2 .api-abt-img2{
		position: unset;
	}
	.api-abt-detail-2 .api-abt-img3{
		position: unset;
	}
	.api-abt-detail-2 .api-abt-img4{
		position: unset;
	}
	.api-abt-detail-2 .api-abt-img5{
		position: unset;
	}
	.api-abt-detail-2 .api-abt-img6{
		position: unset;
	}
	.api-abt-detail-1{
		width: 100%;
	}
	.api-bandetail-1{
		width: 100%;
	}
	.api-ban-data{
		flex-direction: column;
	}
	.api-bandetail-2-img img{
		width: 300px;
	}
	.api-bandetail-2-img{
		height: unset;
		display: flex;
		justify-content: flex-end;
	}
	.api-bandetail-2{
		width: 100%;
	}
	.api-icns-detail{
		width: 45%;
	}
}
@media only screen and (max-width: 575px) {
	.api-bandetail-1-text h1{
		font-size: 32px;
	}
	.api-icns-detail{
		width: 100%;
	}
}
@media only screen and (max-width: 350px) {
	.api-bandetail-2-img img{
		width: 250px;
	}
	.api-bandetail-1-text h1{
		font-size: 28px;
	}
}
</style>
@section('content')
<section class="api-ban-sec">
	<div class="api-ban-wdth">
		<div class="api-ban-data">
			<div class="api-bandetail-1">
				<div class="api-bandetail-1-text">
					<h1>TOOLS & API CONNECTIVITY</h1>
					<!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis augue lacus, vehicula suscipit nulla ut, ullamcorper venenatis mauris.</p>-->
				</div>
			</div>
			<div class="api-bandetail-2">
				<div class="api-bandetail-2-img">
					<img src="{{ url('/frontend/images/api/ban.png') }}">
				</div>
			</div>
		</div>
	</div>
</section>
<section class="api-abt-sec">
	<div class="api-abt-wdth">
		<div class="api-abt-data">
			<div class="api-abt-detail-1">
				<div class="api-abt-detail-text">
					<h2>API CONNECTIVITY</h2>
					<p>Api connectivity are for shopify and woocomrace aslo we are connected to Amazon / eBay and any platform can work with our platform.</p>
				</div>
				<div class="api-abt-detail-btn">
					<button><a style="color:#fff;" href="{{ url('/users/login') }}">CONNECT NOW <i class="fa fa-long-arrow-right"></i></a></button>
				</div>  
			</div>
			<div class="api-abt-detail-2">
				<img class="api-abt-img1" src="{{ url('/frontend/images/api/ab2.png') }}">
				<img class="api-abt-img2" src="{{ url('/frontend/images/api/ab1.png') }}">
				<img class="api-abt-img3" src="{{ url('/frontend/images/api/ab5.png') }}">
				<img class="api-abt-img4" src="{{ url('/frontend/images/api/ab6.png') }}">
				<img class="api-abt-img5" src="{{ url('/frontend/images/api/ab3.png') }}">
				<img class="api-abt-img6" src="{{ url('/frontend/images/api/ab4.png') }}">
			</div>
		</div>
	</div>
</section>
<section class="api-struc-sec">
	<div class="api-struc-wdth">
		<div class="api-struc-data">
			<img src="{{ url('/frontend/images/api/struc.png') }}"
		</div>
	</div>
</section>
<section class="api-icns-sec">
	<div class="api-icns-wdth">
		<div class="api-icns-data">
			<div class="api-icns-detail">
				<div class="api-icns-img">
					<img src="{{ url('/frontend/images/api/icn2.png') }}"/>
				</div>
				<div class="api-icns-text">
					<p>AI SMART REPORTS</p>
				</div>
			</div>

			<div class="api-icns-detail">
				<div class="api-icns-img">
					<img src="{{ url('/frontend/images/api/icn3.png') }}"/>
				</div>
				<div class="api-icns-text">
					<p>TRACKING</p>
				</div>
			</div>

			<div class="api-icns-detail">
				<div class="api-icns-img">
					<img src="{{ url('/frontend/images/api/icn1.png') }}"/>
				</div>
				<div class="api-icns-text">
					<p>WARRANTY TOOL</p>
				</div>
			</div>

			<div class="api-icns-detail">
				<div class="api-icns-img">
					<img src="{{ url('/frontend/images/api/icn5.png') }}"/>
				</div>
				<div class="api-icns-text">
					<p>MULTI LANGUAGE TRANSLATOR</p>
				</div>
			</div>

			<div class="api-icns-detail">
				<div class="api-icns-img">
					<img src="{{ url('/frontend/images/api/icn6.png') }}"/>
				</div>
				<div class="api-icns-text">
					<p>EMAIL MARKETING</p>
				</div>
			</div>

			<div class="api-icns-detail">
				<div class="api-icns-img">
					<img src="{{ url('/frontend/images/api/icn4.png') }}"/>
				</div>
				<div class="api-icns-text">
					<p>REVIEW SYSTEM</p>
				</div>
			</div>

			<div class="api-icns-detail">
				<div class="api-icns-img">
					<img src="{{ url('/frontend/images/api/icn7.png') }}"/>
				</div>
				<div class="api-icns-text">
					<p>CHAT MODULE</p>
				</div>
			</div>

			<div class="api-icns-detail">
				<div class="api-icns-img">
					<img src="{{ url('/frontend/images/api/icn8.png') }}"/>
				</div>
				<div class="api-icns-text">
					<p>DRAG AND DROP SITE CREATION</p>
				</div>
			</div>

			<div class="api-icns-detail api-icns-detail-clr">
				<div class="api-icns-img">
					<img src="{{ url('/frontend/images/api/icn9.png') }}"/>
				</div>
				<div class="api-icns-text api-icns-text-clr">
					<p>AUTOMATIC GENERATED QR CODE SYSTEM</p>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('script')
@endsection    