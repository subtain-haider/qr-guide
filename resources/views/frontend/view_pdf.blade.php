@extends('frontend.layouts.new_app')

@section('content')
    <!-- <section>
        <img src="https://via.placeholder.com/2000x300.jpg" alt="" class="img-fluid">
    </section> -->

    @php
        header("Content-type: application/pdf"); // add here more headers for diff. extensions
        header("Content-Disposition: inline; filename=\"/public/". $product->pdf."\""); 
        $total = 0;
        $rating = 0;
        foreach ($product->user->products as $key => $seller_product) {
            $total += $seller_product->reviews->count();
            $rating += $seller_product->reviews->sum('rating');
        }
    @endphp

        <!-- product picture -->
        <section class="qr-product-page-sec">
            <div class="page">
                <div class="qr-product-page-head">
                    <h2>PRODUCT PICTURES</h2>
                </div>
                @php
                    $main_photo_3 = $main_photo_2 = $main_photo = '';
                    if(!empty($product->photos)){
                        $photos = json_decode($product->photos);
                        $main_photo = $photos[0];
                        if(isset($photos[1])){
                            $main_photo_2 = $photos[1];
                        }
                        if(isset($photos[2])){
                            $main_photo_3 = $photos[2];
                        }
                    }
                @endphp
                <div class="pro-page-pro-pic-data">
                    <div class="pro-page-pro-pic-detail-1">
                        <div class="pro-page-pro-pic-1">
                            <a href="#"><img src="@if($product->thumbnail_img != ''){{ asset($product->thumbnail_img) }} @elseif($product->featured_img != ''){{ asset($product->featured_img) }} @elseif($product->flash_deal_img != ''){{ asset($product->flash_deal_img) }} @elseif($main_photo != ''){{ asset($main_photo) }} @else {{ asset('img/no-image-available.png') }} @endif"></a>
                        </div>
                    </div>
                    <div class="pro-page-pro-pic-detail-2">
                        <div class="pro-page-pro-pic-rt">
                            <a href="#"><img src="@if($main_photo_2 != ''){{ asset($main_photo_2) }} @else {{ asset('img/no-image-available.png') }} @endif"></a>
                        </div>
                        <div class="pro-page-pro-pic-rt pro-pic-rt-mrg ">
                            <a href="#"><img src="@if($main_photo_3 != ''){{ asset($main_photo_3) }} @else {{ asset('img/no-image-available.png') }} @endif"></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- link to product store -->
        <section class="pro-page-store-sec">
            <div class="page">
                <div class="pro-page-store-head">
                    <h2>View PDF</h2>
                </div>
                <div class="pro-page-store-data">
                    <div id="pdf_viewer"></div>
                    {{-- <iframe src="/public/{{ $product->pdf }}#toolbar=0" height="200" width="300"></iframe> --}}
                    {{-- <embed src="/public/{{ $product->pdf }}" type="application/pdf" width="100%" height="600px" /> --}}
                </div>
            </div>
        </section>

@endsection

@section('script')
    <script src="{{ asset('frontend/js/PDFObject-master/pdfobject.min.js') }}"></script>
    <script>PDFObject.embed("/public/{{ $product->pdf }}", "#pdf_viewer");</script>
@endsection