@extends('frontend.layouts.new_app_1')
<!-- faq-page -->
<style>
	.about-p-page-sec{
	display: flex;
	justify-content: center;
	padding: 50px 0px;
	background-color: #F7F7F7;
}
.about-p-page-wdth{
	width: 85%;
}
.about-p-page-head{
	text-align: center;
}
.about-p-page-head h2{
	font-family: 'Play';
	font-size: 38px;
	color: #000;
	font-weight: 600;
	margin-bottom: 2px;
}
.about-p-page-content{
	margin-top: 50px;
}
.about-p-page-content h3{ 
	font-family: 'Play';
	font-size: 24px;
	color: #000;
	font-weight: 600;
	margin-bottom: 0px;
}
.about-p-page-content hr{
	border: 2px solid #08A3BE;
	margin-top: 2px;
	width: 80px;
}
</style>
@section('content')

<section class="about-p-page-sec">
	<div class="about-p-page-wdth">
		<div class="about-p-page-head">
			<h2>About QR Guide</h2>
		</div>

			@foreach($abouts as $key => $about)
		<div class="about-p-page-content">
			<h3>{{ $about->heading }}</h3>
			<hr>
			<p> {{ $about->content }}</p>
		</div>

		  @endforeach
		
	</div>
</section>

@endsection
