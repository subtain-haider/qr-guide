@extends('frontend.layouts.app')

@section('content')

    <section class="gry-bg py-4 profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-12">
                    <!-- Page title -->
                    <div class="page-title">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                    {{__('Careers')}}
                                </h2>
                            </div>
                            <div class="col-md-6">
                                <div class="float-md-right">
                                    <ul class="breadcrumb">
                                        <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                                        <li class="active"><a href="{{ route('careers') }}">{{__('careers')}}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- dashboard content -->
                    <div class="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2 text-center">
                                        {{__('Latest Jobs')}}
                                    </div>
                                    <div class="form-box-content p-3">
                                        <table class="table mb-0 table-bordered" style="font-size:14px;">
                                            <thead>
												<tr>
													<th>#</th>
													<th>{{__('Title')}}</th>
													<th>{{__('Job Type')}}</th>
													<th>{{ __('Salary') }}</th>
													<th>{{ __('Posted On') }}</th>
													<th>{{__('Actions')}}</th>
												</tr>
											</thead>
											<tbody>
												@foreach($careers as $key => $career)
													<tr>
														<td>{{$key+1}}</td>
														<td>{{ $career->title }}</td>
														<td>{{ $career->job_type }}</td>
														<td>{{ single_price($career->salary) }}</td>
														<td>
															@php
																$date = strtotime($career->created_at);
																echo date('d F Y',$date);
															@endphp
														</td>
														<td>
															<a href="{{ route('career_show', encrypt($career->id)) }}">
																<span class="p-2 bg-base-1 rounded">{{__('View')}}</span>
															</a>
														</td>
													</tr>
												@endforeach
											</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
