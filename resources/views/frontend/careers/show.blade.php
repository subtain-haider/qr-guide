@extends('frontend.layouts.app')

@section('content')

    <section class="gry-bg py-4 profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-12">
                    <!-- Page title -->
                    <div class="page-title">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                    {{__('Careers')}}
                                </h2>
                            </div>
                            <div class="col-md-6">
                                <div class="float-md-right">
                                    <ul class="breadcrumb">
                                        <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                                        <li class="active"><a href="{{ route('careers') }}">{{__('careers')}}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- dashboard content -->
                    <div class="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2 text-center">
                                        <h3>{{__($careers->title)}}</h3>
                                    </div>
                                    <div class="form-box-content p-3">
                                        <table class="table mb-0 table-borderless" style="font-size:14px;" border="0">
                                            <tr>
												<td colspan="2"><h3>{{__('Details')}}</h3></td>
											</tr>
											<tr>
												<td>{{__('Employment Type')}}</td>
												<td>{{ $careers->job_type }}</td>
											</tr>
											<tr>
												<td>{{__('Minimum Education Level:')}}</td>
												<td>{{ $careers->education_level }}</td>
											</tr>
											<tr>
												<td>{{__('Minimum Work Experience')}}</td>
												<td>{{ $careers->experience }} {{__('Years')}}</td>
											</tr>
											<tr>
												<td>{{__('Field of Experience')}}</td>
												<td>{{ $careers->field_experience }}</td>
											</tr>
											<tr>
												<td>{{__('Date of Joining')}}</td>
												<td>{{ $careers->joining }} {{__('Weeks')}}</td>
											</tr>
											<tr>
												<td>{{__('Location')}}</td>
												<td>{{ $careers->location }}</td>
											</tr>
											<tr>
												<td colspan="2">
													<h3>{{__('Description')}}</h3>
												</td>
											</tr>
											<tr>
												<td colspan="2">@php echo $careers->details; @endphp</td>
											</tr>
											@if(!Auth::user() || (Auth::user() and Auth::user()->user_type != 'staff'))
											<tr>
												<td colspan="2" class="text-center">
													<button onclick="show_apply_form({{ $careers->id }})" class="btn btn-info">{{__('Apply')}}</button>
												</td>
											</tr>
											@endif
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	@if(!Auth::user() || (Auth::user() and Auth::user()->user_type != 'staff'))
	<div class="modal fade" id="apply_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="order-details-modal-body">
					<div class="col-md-12 mb-4 mt-2">
						<form class="form-horizontal" action="{{ route('register_application') }}" method="POST" enctype="multipart/form-data">
							@csrf
							<input type="hidden" id="job_id" name="job_id" value="{{$careers->id}}" required>
							<div class="panel-heading mb-4">
								<h4 class="panel-title">Apply For {{__($careers->title)}}</h4>
							</div>
							<div class="panel-body">
								<div class="form-group">
									<label class="col-sm-12 control-label" for="name">{{__('Profile')}}</label>
									<div class="col-sm-12">
										<input type="file" name="photo" id="file-3" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                                        <label for="file-3" class="mw-100 mb-3">
                                            <span></span>
                                            <strong>
                                                <i class="fa fa-upload"></i>
                                                {{__('Choose image')}}
                                            </strong>
                                        </label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-12 control-label" for="name">{{__('Name')}}</label>
									<div class="col-sm-12">
										<input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-12 control-label" for="gender">{{__('Gender')}}</label>
									<div class="col-sm-12">
										<select name="gender" required class="form-control demo-select2-placeholder">
											<option value="">{{__('Please Select')}}</option>
											<option value="{{__('male')}}">{{__('Male')}}</option>
											<option value="{{__('female')}}">{{__('Female')}}</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-12 control-label" for="cnic">{{__('CNIC')}}</label>
									<div class="col-sm-12">
										<input type="text" placeholder="{{__('CNIC')}}" id="cnic" name="cnic" class="form-control" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-12 control-label" for="phone">{{__('Phone No.')}}</label>
									<div class="col-sm-12">
										<input type="text" placeholder="{{__('Phone No.')}}" id="phone" name="phone" class="form-control" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-12 control-label" for="country">{{__('Country')}}</label>
									<div class="col-sm-12">
										<select name="country" required class="form-control demo-select2-placeholder get_citites_by_country">
											<option value="">{{__('Please Select')}}</option>
											@foreach($countries as $CountryDetail)
												<option value="{{$CountryDetail->code}}">{{$CountryDetail->asciiname}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-12 control-label" for="city">{{__('City')}}</label>
									<div class="col-sm-12">
										<select name="city" id="city" required class="form-control demo-select2-placeholder"></select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-12 control-label" for="email">{{__('Email')}}</label>
									<div class="col-sm-12">
										<input type="text" placeholder="{{__('Email')}}" id="email" name="email" class="form-control" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-12 control-label" for="password">{{__('Password')}}</label>
									<div class="col-sm-12">
										<input type="password" placeholder="{{__('Password')}}" id="password" name="password" class="form-control" required>
									</div>
								</div>
							</div>
							<div class="panel-footer text-right">
								<button class="btn btn-purple" type="submit">{{__('Save')}}</button>
							</div>
						</form>
					</div>
                </div>
            </div>
        </div>
    </div>
	@endif

@endsection
@if(!Auth::user() || (Auth::user() and Auth::user()->user_type != 'staff'))
@section('script')
    <script type="text/javascript">
		$(document).ready(function(){
			$(document).on('change', ".get_citites_by_country", function () {
				$.post('{{ route('cities_by_country') }}',{_token:'{{ csrf_token() }}', country_code:$(this).val()}, function(data){
					var returnedData = JSON.parse(data);
					$('#city').html(null);
					$('#city').append(returnedData.options);
				});
			});
		});
    </script>
@endsection
@endif