@extends('frontend.layouts.new_app_1')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style type="text/css">
      .warranty-all-page-sec{
    display: flex;
    justify-content: center;
    padding: 50px 0px;
}
.warranty-all-wdth{
    width: 85%;
}
.warranty-detail-head{
    margin-bottom: 30px;
    text-align: center;
}
.warranty-detail-head h2{
    font-size: 36px;
    font-weight: bold;
    font-family: 'Play';
}
.warranty-all-data{
    display: flex;
    justify-content: space-between;
}
.warranty-all-detail{
    width: 48%;
    border: 1px solid #ccc;


}
.warranty-all-text-data{
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 15px 10px;
    border-bottom: 1px solid #ccc;

}
.warranty-all-text-data h5{
    margin-bottom: 0px;
    font-family: 'Play';
    font-size: 16px;
    font-weight: bold;
}
.warranty-all-text-data p{
    margin-bottom: 0px;
    font-family: 'Play';
}
.warranty-all-detail-head{
    text-align: center;
    background-color: #1099B1;
    padding: 12px 0px;
    color: #fff;
}
.warranty-all-detail-head h3{
    font-family: 'Play';
    margin-bottom: 0px;
}
.warranty-all-detail-shop{
    width: 100%;
    border: 1px solid #ccc;

}
.warranty-all-data-shop{
    margin-top: 50px;
}
.warranty-all-text-data .warranty-all-text-btn-1{
    border: none;
    background-color: #1099B1;
    font-family: 'Play';
    color: #fff;
    font-size: 14px;
    font-weight: bold;
    padding: 6px 15px;
}
.warranty-approve-btn{
    margin-top: 30px;
    display: flex;
    justify-content: center;
}
.warranty-approve-btn button{
    width: 70%;
    border: none;
    background-color: #1099B1;
    font-family: 'Play';
    color: #fff;
    font-size: 16px;
    font-weight: bold;
    padding: 12px 15px;

}

@media only screen and (max-width: 788px) {
    .warranty-all-data{
        flex-direction: column;
    }
    .warranty-all-detail{
        width: 100%;
        margin-top: 50px;
    }
    .warranty-approve-btn button{
        width: 100%;
    }
}
  </style>
@php
    $PackageInfo = \App\Models\User::where('package', Auth::user()->package)->first();
    $PP = Auth::user()->package_for;
    $Permissions = json_decode($PackageInfo->package_for);
@endphp
@section('content')
<section class="dashboard-recover-sub-links-sec" id="dashrecoversublinks">
        <div class="dashboard-recover-two-sides-left-all">
            <div class="dashboard-recover-two-sides-left-all-ul">
                           <ul class="dashboard-recover-two-sides-left-links">
                <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                <!-- <li><a href="{{ route('company.index') }}">{{__('Create Mini Site')}}</a></li> -->
                <li><a href="#">{{__('Mini Sites')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('company.index') }}">{{__('Mini Sites')}}</a></li>
                        <li><a href="{{ route('company.create') }}">{{__('Create Mini Site')}}</a></li>
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                
               
                        
                <li><a href="#">{{__('Products')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('seller.products') }}">{{__('All Products and Guides')}}</a></li>
                        <li><a href="{{ route('seller.products.upload')}}">{{__('Add Product to Sell')}}</a></li>
                        @if(in_array(3,$Permissions))
                        <li><a href="{{ route('seller.guide.upload')}}">{{__('Add Products Guides')}}</a></li>
                        @endif
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                <li><a href="#">{{__('Shopify Store')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{route('shopify.index')}}">{{__('All Shopify Store')}}</a></li>
                        <li><a href="{{route('shopify.create')}}">{{__('Add Shopify Store')}}</a></li>
                        
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                <li><a href="{{route('woocommerce.index')}}">{{__('WooCommerce')}}</a></li>
                <!-- <li><a href="#">{{__('Guides')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('seller.guides') }}">{{__('Guides')}}</a></li>
                        <li><a href="{{ route('seller.guide.upload')}}">{{__('Add Guide')}}</a></li>
                      <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> 
                    </ul>
                </li> -->
                <!-- <li><a href="#">{{__('Inhouse Orders')}}</a></li> -->
                <li><a href="{{ route('total.sales') }}">{{__('Total Sales')}}</a></li>
                <li><a href="#">{{__('Saller Sales via Affiliate Links')}}</a></li>
                <li><a href="{{ route('sellercoupon') }}">{{__('Saller Coupons')}}</a></li>
                <!-- <li><a href="#">{{__('Vendors')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li> -->
                <li><a href="#">{{__('Customer')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('company_suggessions',Auth::user()->id) }}">Suggessions</a></li>
                        <li><a href="{{ route('seller.reviews',Auth::user()->id) }}">Reviews</a></li>
                         <li><a href="{{ route('clientsreport') }}">Clients Report</a></li>
                         @php
                            $domain = (explode(".",request()->getHost()));
                            $shops = App\Models\Shop::where('slug',$domain[0])->first();
                        @endphp
                        @if(in_array(15,$Permissions))
                        <li><a href="{{ route('seller_questions',Auth::user()->id) }}">Seller Questions</a></li>
                        @endif
                        @if(in_array(19,$Permissions))
                        <li><a href="{{ route('clientsreport') }}">Clients Report</a></li>
                        @endif
                    </ul>
                </li>
                <li><a href="#">{{__('Reports')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        @if(in_array(11,$Permissions))
                        <li><a href="{{ route('club_info') }}">Club Info</a></li>
                        @endif
                        @if(in_array(6,$Permissions))
                        <li><a href="{{ route('company_warranties', Auth::user()->id) }}">Warranty Request </a></li>
                        @endif
                        <li><a href="{{ route('sellingreport') }}">Selling Report</a></li>
                    </ul>
                </li>
                <!-- <li><a href="#">{{__('Pricing')}} </a></li>
                <li><a href="#">{{__('Bussines Setting')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li>
                <li><a href="#">{{__('E-commerce Setup')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li> -->
                @if(in_array(13,$Permissions))
                <li>
                    <a href="{{ route('support_ticket.index') }}" class="{{ areActiveRoutesHome(['support_ticket.index'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Support Ticket')}}
                        </span>
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{ route('marketing') }}" class="{{ areActiveRoutesHome(['marketing'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Email Marketing')}}
                        </span>
                    </a>
                </li>
                 <li class="">
                            <a class="nav-link" href="{{ route('inboxseller') }}">
                              
                                <span class="menu-title">Support Messages</span>
                            </a>
                </li>
                
                <li><a href="#">{{__('Log Activity')}}</a></li>
                
            </ul>


            </div>
        </div>
        <div class="dashboard-recover-res-sublinks-cross">
            <i class="fa fa-times" onclick="hidedashrecoversublinks()"></i>
        </div>
    </section>
    <!-- end res sub mnu -->
    <section class="dashboard-recover-burdger">
        <div class="dashboard-recover-burdger-page">
            <div class="dashboard-recover-burdger-bars">
                <i class="fa fa-bars" onclick="showdashrecoversublinks()"></i>
            </div>
        </div>
    </section>
<!-- Basic Data Tables -->
<!--===================================================-->

<section class="warranty-all-page-sec">
    @include('frontend.inc.seller_side_nav_new')
    <div class="warranty-all-wdth">
        <div class="warranty-detail-head">
            <h2>{{ $shop->name }} {{__('Warranty')}}</h2>
        </div>
        <div class="warranty-all-data">

            <div class="warranty-all-detail">
                <div class="warranty-all-detail-head">
                    <h3>Vendor Info</h3>
                </div>
                <div class="warranty-all-text-data">
                    <h5>Name</h5>
                    <p>{{ $user->name }}</p>
                </div>
                <div class="warranty-all-text-data">
                    <h5>Email</h5>
                    <p>{{ $user->email }}</p>
                </div>
                <div class="warranty-all-text-data">
                    <h5>Address</h5>
                    <p>{{ $user->address }}</p>
                </div>
                <div class="warranty-all-text-data">
                    <h5>Phone</h5>
                    <p>{{ $user->phone }}</p>
                </div>
            </div>
            @if($warranty->user_id != null)
            <div class="warranty-all-detail">
                <div class="warranty-all-detail-head">
                    <h3>User Info</h3>
                </div>
                <div class="warranty-all-text-data">
                    <h5>Name</h5>
                    <p>{{ \App\Models\User::where('id', $warranty->user_id)->first()->name }}</p>
                </div>
                <div class="warranty-all-text-data">
                    <h5>Email</h5>
                    <p>{{ \App\Models\User::where('id', $warranty->user_id)->first()->email }}</p>
                </div>
                <div class="warranty-all-text-data">
                    <h5>Address</h5>
                    <p>{{ \App\Models\User::where('id', $warranty->user_id)->first()->address }}</p>
                </div>
                <div class="warranty-all-text-data">
                    <h5>Phone</h5>
                    <p>{{ \App\Models\User::where('id', $warranty->user_id)->first()->phone }}</p>
                </div>
            </div>
             @endif
        </div>

        <div class="warranty-all-data-shop">
            <div class="warranty-all-detail-shop">
                <div class="warranty-all-detail-head">
                    <h3>Shop Info</h3>
                </div>
                <div class="warranty-all-text-data">
                    <h5>Shop Name</h5>
                    <p>{{ $shop->name }}</p>
                </div>
                <div class="warranty-all-text-data">
                    <h5>Shop Address</h5>
                    <p>{{ $shop->address }}</p>
                </div>
                <div class="warranty-all-text-data">
                    <h5>Address</h5>
                    <p>113, city, country</p>
                </div>
            </div>
        </div>

        <div class="warranty-all-data-shop">
            <div class="warranty-all-detail-shop">
                <div class="warranty-all-detail-head">
                    <h3>Warranty Query</h3>
                </div>
                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                <tbody>
                    @foreach (json_decode($warranty->info) as $key => $info)
                        <tr>
                            <th>{{ $info->label }}</th>
                            @if ($info->type == 'text' || $info->type == 'select' || $info->type == 'radio')
                                <td>{{ $info->value }}</td>
                            @elseif ($info->type == 'multi_select')
                                <td>
                                    {{ implode(json_decode($info->value), ', ') }}
                                </td>
                            @elseif ($info->type == 'select')
                                <td>
                                    {{ implode(json_decode($info->value), ', ') }}
                                </td>
                            @elseif ($info->type == 'file')
                                <td>
                                    <a href="{{ asset($info->value) }}" target="_blank" class="btn-info">{{__('Click Here Download')}}</a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
            <form action="{{ route('company_warranty_submits',$warranty->id) }}" method="POST">
            <div class="warranty-approve-btn">
                
                {{ csrf_field() }}
                <input type="hidden" name="warranty_id" value="{{ $warranty->id }}">
                <input type="hidden" name="shop_id" value="{{ $warranty->shop_id }}">
                <button type="submit" {{ ($warranty->id == 1) ? 'disabled' : '' }}>{{ ($warranty->id == 1) ? 'Warranty has been Claimed' : 'CLICK HERE TO APROVED' }}</button>
            
            </div>
            </form>
        </div>

    </div>
</section>

@endsection
