@extends('frontend.layouts.new_app_1')

@section('content')

    <section class="payment-page-method-sec">
        <div class="payment-page-wdth">
            <div class="payment-page-method-sec-head">
                <h2>{{__('SELECT PAYMENT METHOD')}}</h2>
            </div>
            <div class="payment-page-method-data">
                <div class="paymentmethodpropritabs">
                    <span class="paymentmethodpropritab"><button class="paymentmethodpropritabs-btn">{{__('MY CART')}}</button></span>
                    <span class="paymentmethodpropritab"><button class="paymentmethodpropritabs-btn">{{__('SHIPING INFO')}}</button></span>
                    <span class="paymentmethodpropritab"><button class="paymentmethodpropritabs-btn">{{__('PAYMENT METHOD')}}</button></span>         
                </div>
                <div>
                    <div class="paymentmethodtab_content">
                        <div class="paymentmethodprotab_item">
        <form action="{{ route('payment.checkout') }}" class="form-default" data-toggle="validator" role="form" method="POST">
                            <!--<form action="{{ route('payment') }}" class="form-default" data-toggle="validator" role="form" method="POST">-->
                                @csrf
                                <div class="payment-page-method-tab-3-data">
                                    <!-- @if (\App\Models\BusinessSetting::where('type', 'paypal_payment')->first()->value == 1)
                                        <div class="payment-page-method-tab-3-detail">
                                            <label class="w-100" style="margin-top:5px;">
                                                <input type="radio" id="" name="payment_option" value="credit-card" checked>
                                                <button>
                                                    <div class="payment-page-method-tab-3-img">
                                                        <img src="{{asset('frontend/images/credit-2.png')}}">
                                                        <div class="payment-page-method-tab-3-img-position">
                                                            <img src="{{asset('frontend/images/credit-1.png')}}">
                                                        </div>
                                                    </div>
                                                    <h5>CREDIT CARD</h5>
                                                </button>
                                            </label>
                                        </div>
                                    @endif -->

                                    @if (\App\Models\BusinessSetting::where('type', 'paypal_payment')->first()->value == 1)
                                        <div class="payment-page-method-tab-3-detail">
                                            <input type="radio" id="" name="payment_option" value="paypal" class="d-none" checked>
                                            <button type="button">
                                                <div class="payment-page-method-tab-3-img">
                                                    <img src="{{asset('frontend/images/credit-4.png')}}">
                                                    <div class="payment-page-method-tab-3-img-position">
                                                        <img src="{{asset('frontend/images/credit-3.png')}}">
                                                    </div>
                                                </div>
                                                <h5>PAYPAL</h5>
                                            </button>
                                        </div>
                                    @endif

                                    @if(\App\Models\BusinessSetting::where('type', 'stripe_payment')->first()->value == 1)
                                        <div class="payment-page-method-tab-3-detail">
                                            <input type="radio" id="" name="payment_option" value="stripe" class="d-none" checked>
                                            <button type="button">
                                                <div class="payment-page-method-tab-3-img">
                                                    <img src="{{asset('frontend/images/credit-6.png')}}">
                                                    <div class="payment-page-method-tab-3-img-position">
                                                        <img src="{{asset('frontend/images/credit-5.png')}}">
                                                    </div>
                                                </div>
                                                <h5>STRIPE</h5>
                                            </button>
                                        </div>
                                    @endif

                                    <!--<div class="payment-page-method-tab-3-detail">-->
                                    <!--    <button type="button">-->
                                    <!--        <div class="payment-page-method-tab-3-img payment-page-method-tab-3-img-stripe">-->
                                    <!--            <img src="{{asset('frontend/images/credit-4.png')}}">-->
                                    <!--            <div class="payment-page-method-tab-3-img-position">-->
                                    <!--                <img src="{{asset('frontend/images/credit-3.png')}}">-->
                                    <!--            </div>-->
                                    <!--        </div>-->
                                    <!--        <h5>APPLE PAY</h5>-->
                                    <!--    </button>-->
                                    <!--</div>-->
                                </div>
                                <!-- basic info -->
                                <!-- <div class="payment-page-basic-sec-head">
                                    <h2>{{__('BASIC INFO')}}</h2>
                                </div>
                                <div class="payment-page-basic-data">
                                    <form class="payment-page-basic-form">
                                        <div class="payment-page-basic-form-feilds payment-page-inputs">
                                            <input type="text" name="" placeholder="*NAME" required>
                                        </div>
                                        <div class="payment-page-basic-form-feilds payment-page-inputs">
                                            <input type="email" name="" placeholder="*EMAIL" required>
                                        </div>
                                        <div class="payment-page-basic-form-feilds-data-1 payment-page-basic-form-feilds payment-page-inputs">
                                            <div class="payment-page-basic-form-feilds-data-imgs">
                                                <img src="images/cards.png">
                                            </div>
                                            <div class="payment-page-basic-form-feilds-wdth payment-page-inputs">
                                                <input type="text" name="" placeholder="*ENTER NUMBER" required>
                                            </div>
                                            
                                        </div>
                                        <div class="payment-page-basic-form-feilds-data-2 payment-page-basic-form-feilds">
                                            <div class="payment-page-basic-form-feilds-detail-1 payment-page-inputs">
                                                <select name="month">
                                                    <option value="month">MONTH</option>
                                                    <option value="01">January</option>
                                                    <option value="02">February</option>
                                                    <option value="03">March</option>
                                                    <option value="04">April</option>
                                                    <option value="05">May</option>
                                                    <option value="06">June</option>
                                                    <option value="07">July</option>
                                                    <option value="08">August</option>
                                                    <option value="09">September</option>
                                                    <option value="10">October</option>
                                                    <option value="11">November</option>
                                                    <option value="12">December</option>
                                                </select> 
                                            </div>
                                            <div class="payment-page-basic-form-feilds-detail-1 payment-page-inputs payment-page-inputs-margin">
                                                <input type="email" name="" placeholder="*YEAR" required>
                                            </div>
                                            <div class="payment-page-basic-form-feilds-detail-1 payment-page-inputs payment-page-inputs-margin">
                                                <input type="email" name="" placeholder="*CVV" required>
                                            </div>
                                        </div>
                                        <div class="payment-page-basic-form-feilds payment-page-inputs">
                                            <input type="text" name="" placeholder="*COUNTRY" required>
                                        </div>
                                        <div class="payment-page-basic-form-feilds payment-page-inputs">
                                            <input type="text" name="" placeholder="*ADDRESS" required>
                                        </div>
                                        <div class="payment-page-basic-form-feilds payment-page-inputs-checkbox">
                                            <input type="checkbox" id="paymentcheck" name="">
                                            <label for="paymentcheck">by confirming this box, i agree to the terms and conditions, privicy policy</label>
                                        </div>
                                    </form>
                                </div> -->
                                @include('frontend.partials.cart_summary')
                                <div class="payment-page-proceed-btn">
                                    <button>{{__('PROCEED ORDER')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
  function removeFromCartView(e, key){
        e.preventDefault();
        var token = '<?php echo csrf_token() ?>';
        $.ajax({
           type:'POST',
           url:"{{ route('cart.removeFromCart') }}",
           data:{key:key,_token:token},
           success:function(data){
              location.reload();
           }
        });
        removeFromCart(key);
    }

    function updateQuantity(key, element){
        $.post('{{ route('cart.updateQuantity') }}', { _token:'{{ csrf_token() }}', key:key, quantity: element.value}, function(data){
            updateNavCart();
            $('#cart-summary').html(data);
        });
    }

    function showCheckoutModal(){
        $('#GuestCheckout').modal();
    }

    $(function () {
        $('.payment-page-method-tab-3-detail button').on('click', function(){
            $(this).prev('input').prop('checked', true);
        });
    });
    </script>
@endsection