<div style="margin-left:auto;margin-right:auto;">
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style media="all">
	@import url('https://fonts.googleapis.com/css?family=Open+Sans:400,700');
	*{
		margin: 0;
		padding: 0;
		line-height: 1.5;
		font-family: 'Open Sans', sans-serif;
		color: #333542;
	}
	div{
		font-size: 1rem;
	}
	.gry-color *,
	.gry-color{
		color:#878f9c;
	}
	table{
		width: 100%;
	}
	table th{
		font-weight: normal;
	}
	table.padding th{
		padding: .5rem .7rem;
	}
	table.padding td{
		padding: .7rem;
	}
	table.sm-padding td{
		padding: .2rem .7rem;
	}
	.border-bottom td,
	.border-bottom th{
		border-bottom:1px solid #eceff4;
	}
	.text-left{
		text-align:left;
	}
	.text-right{
		text-align:right;
	}
	.small{
		font-size: .85rem;
	}
	.strong{
		font-weight: bold;
	}
</style>

	@php
		$generalsetting = \App\Models\GeneralSetting::first();
	@endphp
	<div style="padding: 1.5rem; text-align:center; margin-top:1.5rem; margin: 1.5rem auto;">
		<div class="panel-heading">
				@foreach($shopid as $ke => $sho)
				@if($sho != NULL)<img style="width: 135px; height: 135px;" src="{{ $sho }}">@else <img style="width: 135px; height: 135px;" src="{{ asset('frontend/images/rounded-logo.png') }}"> @endif
				@endforeach
                <h3 class="text-lg">{{__('Warranty Query')}} </h3>
            </div>
            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                <tbody>

                    @foreach($shops as $key => $sho)
                    	@if ($loop->first) @continue @endif
                    	@if($key == 'warranty')
                    	<tr>
                    	<th>DATE OF PURCHASE</th>
                    	<th>DATE OF PRODUCT ENDING</th>
                    	<?php
                    		$ary = json_decode($sho);
                    		foreach($ary as $ar) {
                    			?> 
                    			@if( $ar->label == 'upload invoice') @continue @endif
                            	<th>{{ $ar->label }}</th>
                                
                        	
                    	<?php
                    			}
                    	?>
                    	</tr>
                    	@endif
                        
                    @endforeach
                    <tr>
	                    @foreach($shops as $key => $sho)
	                    	@if ($loop->first) @continue @endif
	                    	@if($key == 'warranty') @continue @endif
	                    	@if($key == 'shopid') @continue @endif
	                    		<td>{{ $sho }} </td>
	                    @endforeach
                	</tr>
                </tbody>
            </table>
        
    </div>
</div>
