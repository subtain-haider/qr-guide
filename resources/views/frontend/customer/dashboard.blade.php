@extends('frontend.layouts.customer_app')

@section('content')
@php
use App\Models\Club;
$auth_email = \Auth::user()->email;
$club = App\Models\Club::where('email',$auth_email)->get();
$num_club = count($club);
$user = App\Models\User::where('email',$auth_email)->first();
$user_id = $user->id;
$ticket = App\Models\Ticket::where('user_id',$user_id)->get();
$num_ticket = count($ticket);
$warranties = \DB::table('warranties')->where('user_id',$user_id)->get();
$num_warranties = count($warranties);


@endphp
<style>
    .dashboard-2-users-top-content-5boxes-detail-cst-1{
   margin-top: 10px;
    background: linear-gradient(to right, #F7AD66 2%, #F7D166 100%);
    border-radius: 6px;
    width: 19%;
    padding: 25px 20px 20px 20px;
    text-align: center;
}
a:hover {
    color:#fff;
    text-decoration:none;
}
 .butoncolor {
    background-color: #FC6E62;
    padding: 4px 10px;
    color: #fff;
}
</style>
				<!-- dashboard-content -->
				<div class="dashboard-2-users-top-content-all">
					<div class="dashboard-2-users-top-content-all-page">
						<div class="dashboard-2-users-top-content-all-head">
							<h3>Dashboard</h3>
							<p>The Updates about the Support Tickets</p>
						</div>
						<!-- 5boxes -->
						<div class="dashboard-2-users-top-content-5boxes-data">

							<div class="dashboard-2-users-top-content-5boxes-detail">
								<div class="dashboard-2-users-top-content-5boxes-detail-icn">
									<i class="fa fa-check-square"></i>
								</div>
								<h4>MY WARRANTIES</h4>
								<p>{{$num_warranties}}</p>
							</div>

							<div class="dashboard-2-users-top-content-5boxes-detail">
								<div class="dashboard-2-users-top-content-5boxes-detail-icn">
									<i class="fa fa-list-ul"></i>
								</div>
								<h4>CLUB</h4>
								<p>Club I'm registered <span>{{$num_club}}</span></p>
							</div>

							<div class="dashboard-2-users-top-content-5boxes-detail">
								<div class="dashboard-2-users-top-content-5boxes-detail-icn">
									<a href="{{ route('customer.contact_support') }}"><i class="fa fa-ticket"></i></a>
								</div>
								<h4>MY TICKETS</h4>
								<p>{{$num_ticket}}</p>
								<a class="butoncolor" href="{{ route('customer.contact_support') }}">Submit Ticket<i class="fa fa-ticket"></i></a>
							</div>

                            <div class="dashboard-2-users-top-content-5boxes-detail-cst-1">
								<div class="dashboard-2-users-top-content-5boxes-detail-icn-cst-1">
									<i class="fa fa-clock-o"></i>
								</div>
								<h4>MY CHAT LOGS</h4>
								<p>my chat logs</p>
							</div>
                            
                            <div class="dashboard-2-users-top-content-5boxes-detail-cst-2">
                                <a href="{{ url('/pricing') }}">
    								<div class="dashboard-2-users-top-content-5boxes-detail-icn-cst-2">
    									<i class="fa fa-refresh"></i>
    								</div>
    								<h4>UPGRADE</h4>
    								<p>from free user to one of the packages</p>
    							</a>
							</div>

						</div>
						<!-- 5boxes end -->

						<!-- dashboard-2-users-statistics left and rigt area-->
						<div class="dashboard-2-users-btn-area-data">

							<div class="dashboard-2-users-btn-area-detail-1">
                                <div class="dashboard-2-users-btn-inners-1">
                                    <button class="dashboard-2-users-btn-cst-1"><a href="#">ADD A GUIDE<i class="fa fa-plus"></i></a></button>
                                    <button class="dashboard-2-users-btn-cst-3"><a href="{{ route('profile') }}">MANAGE PROFILE <i class="fa fa-list-ul"></i></a></button>
                                    <!--<button class="dashboard-2-users-btn-cst-2"><a href="{{ route('profile') }}">MANAGE PROFILE<i class="fa fa-list-ul"></i></a></button>-->
                                </div>
							</div>

							<!-- stat right side -->
							<div class="dashboard-2-users-statistics-area-detail-2">
								<div class="dashboard-2-users-statistics-area-all-head-1">
									<h3>My Order history</h3>
									<select name="" id="dashboarduserstat">
									  <option value="1year" selected>Today</option>
									  <option value="2year">Yesterday</option>
									  <option value="3year">1 week</option>
									</select>
								</div>
								<div class="dashboard-2-users-statistics-more-sales-data">
									<table class="dashboard-2-users-statistics-more-table">
									  <tr class="db-row-1">
									    <th>Customer Name</th>
									    <th>Tickets</th>
									    <th>Location</th>
									    <th>Last reply</th>
									  </tr>
									    @php
                                        $orders = \App\Models\Order::where('user_id', Auth::user()->id)->get();
                                        $total = 0;
                                        @endphp
                                        @foreach ($orders as $key => $order)
                                        <tr char="db-row-2">
									    <td>{{json_decode($order->shipping_address)->name}}</td>
									    <td></td>
									    <td>a</td>
									    <td>a</td>
									    </tr>
									    @endforeach
									  
									</table>
								</div>
								<div class="dashboar-user-mostsales-viewall">
									<a href="#">View All</a>
								</div>
							</div>

						</div>

						<!-- dashboar-2 statistics left and right end -->
					</div>
				</div>
				<!-- dashboard-content end -->
@endsection
