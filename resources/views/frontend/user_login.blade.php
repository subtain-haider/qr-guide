@extends('frontend.layouts.new_app_1')

@section('content')

    <section class="signin-form-sec pt-5">
        <div class="signin-form mt-5">
            <form class="form-default" role="form" action="{{ route('user.login.submit') }}" method="POST">
                @csrf
                <div class="signin-form-sec-logo mb-4">
                    <a href="javascript:void();"><img src="{{ asset('frontend/images/rounded-logo.png') }}"></a>
                </div>
                <h2 class="mb-5">{{__('Login Account')}}</h2>
                
                <div class="form-group">
                    <input type="email" class="form-control input-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" id="email" placeholder="{{__('Username')}}" required="required">
                    <i class="fa fa-envelope-o"></i>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control input-lg {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{__('Password')}}" required="required" id="password">
                    <i class="fa fa-lock"></i>
                </div>
                <div class="login-page-forgot row">
                    <div class="col-12 col-sm-6">
                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label>{{__('Save Password')}}</label>
                    </div>
                    <div class="col-12 col-sm-6">
                        <a href="{{ route('password.request') }}">{{__('Forgot Your password?')}}</a>
                    </div>
                </div>  
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg btn-block signin-btn">{{__('Login to Account')}}</button>
                </div>
                
                <div class="or-seperator"><span>OR</span></div>
                <div class="social-btn text-center">
                    @if(\App\Models\BusinessSetting::where('type', 'google_login')->first()->value == 1)
                        <a href="{{ route('social.login', ['provider' => 'google']) }}" class="btn btn-lg p-0" title="Google"><img src="{{ asset('frontend/images/google-icon.png') }}" class="w-100"></a>
                    @endif

                    @if (\App\Models\BusinessSetting::where('type', 'facebook_login')->first()->value == 1)
                        <a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="btn btn-primary btn-lg" title="Facebook"><i class="fa fa-facebook"></i></a>
                    @endif

                    @if (\App\Models\BusinessSetting::where('type', 'twitter_login')->first()->value == 1)
                        <a href="{{ route('social.login', ['provider' => 'twitter']) }}" class="btn btn-info btn-lg" title="Twitter"><i class="fa fa-twitter"></i></a>
                    @endif
                    <a href="{{ route('social.login', ['provider' => 'linkedin']) }}" class="btn btn-info btn-lg" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                </div>
                <div class="sigin-in-havenot">
                    {{__("Don't have an account?")}} <a href="{{ route('user.registration') }}">{{__('Sign up')}}</a>
                </div>
            </form>
        </div>
    </section>

    <!-- subscribe -->
   <!--  <section class="subscribeser">
        <div class="flex-1ser">
            <div class="page-1ser">
                
                <div class="sub-dataser">
                    <div class="sub-datasser">
                    <h2>Subscribe to our Newsletter</h2>
                        <div>
                            <hr>
                        </div>	
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>   			
                </div>

                <div class="flex-1ser flex-1ser-dirser">
                    <div class="sub-width-1ser">
                        <input type="email" name="email" placeholder="Enter Your Email...">
                    </div>
                    <div class="sub-width-2ser">
                        <button>Subscribe</button>
                    </div>
                </div>

            </div>
        </div>
    </section>	 -->


    <!-- footer -->
   <!--  <section class="footerser">
        <div class="flex-1">
            <div class="page-1">
                <div class="flex-2ser wrap padser">
                    <div class="wftsser">
                        <h3>Our Office</h3>      
                        <p>3rd Floor,Estate House 200 North<br>City, Atlantica SA 3000</p>    
                    </div>
                    <div class="wftsser">
                        <h3>Contact Info</h3>
                        <p>Cell: 0123 456 789 0, 0123 456 789 0<br>E-mail: username@yourmail.com</p>
                    </div>
                    <div class="wftsser">
                        <a href="#">Download App From <i class="fa fa-windows"></i></a>
                        <h1>Follow Us On : <i class="fa fa-instagram"></i><i class="fa fa-facebook"></i><i class="fa fa-twitter"></i></h1>
                    </div>
                    <div class="wftsser">
                        <a href="#">Download App From <i class="fa fa-apple"></i></a>
                        <h1>Support : 24/7</h1>
                    </div>

                </div>
            </div>
        </div>
    </section> -->

    <!-- copyrights  -->
    <!-- <section class="copyser">
        <div class="flex-1ser">
            <div class="page-1ser flex-1ser">
                <div class="copy-dataser">
                    <p>&copy Copyrights musicapp_Design All rights reserved.</p>
                </div>
            </div>
        </div>
    </section>   --> 
@endsection

@section('script')
<script type="text/javascript">
    var sermoblmenu = document.getElementById("sermoblmenu");
    function sershowsec(){
        sermoblmenu.style.display = "flex";
        sermoblmenu.style.left = "0%";
    }
    function serhidesec(){
        sermoblmenu.style.left = "-150%";
    }
    
    $(document).ready(function () {
		$(".header-searchser-select-detail").hide();
        $('.header-searchser-select-head').click(function () {
            $('.header-searchser-select-detail').toggle();
        });
        $('.header-searchser-select-pro-data').click(function () {
            $('.header-searchser-select-head h3').html($(this).text()+'<i class="fa fa-angle-down"></i>');
            $('.header-searchser-select-detail').hide();
        });
    });
</script>
@endsection