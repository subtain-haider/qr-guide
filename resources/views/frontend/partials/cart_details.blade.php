<div class="container">
        <div class="section-title-1 clearfix">
                    <h3 class="heading-5 strong-700 mb-0">
                        <span class="mr-4">{{__('My Cart')}}</span><br><small>3 Items</small>
                    </h3>
                </div>
            @if(Session::has('cart'))
                <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-xl-8 p-sm-0">
                    <!-- <form class="form-default bg-white p-4" data-toggle="validator" role="form"> -->
                    <div class="form-default bg-white p-lg-4 bg-sm-transparent p-0">
                        <div class="">
                            <div class="">
                                <table class="table-cart border-bottom d-none d-lg-block">
                                    <thead>
                                        <tr>
                                            <th class="product-image"></th>
                                            <th class="product-name d-none d-lg-block">{{__('Product')}}</th>                                            <th class="product-price d-none d-lg-table-cell">{{__('Price')}}</th>                                            <th class="product-quanity d-md-table-cell">{{__('Quantity')}}</th>                                            <th class="product-total">{{__('Total')}}</th>                                            <th class="product-remove"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $total = 0;
                                        @endphp
                                        @foreach (Session::get('cart') as $key => $cartItem)
                                            @php
                                            $product = \App\Models\Product::find($cartItem['id']);
                                            $total = $total + $cartItem['price']*$cartItem['quantity'];
                                            $product_name_with_choice = $product->name;
                                            if(isset($cartItem['color'])){
                                                $product_name_with_choice .= ' - '.\App\Models\Color::where('code', $cartItem['color'])->first()->name;
                                            }
                                            foreach (json_decode($product->choice_options) as $choice){
                                                $str = $choice->name; // example $str =  choice_0
                                                $product_name_with_choice .= ' - '.$cartItem[$str];
                                            }
                                            @endphp
                                            <tr class="cart-item">
                                                <td class="product-image align-center">                                                    
                                                    <a href="#" class="mr-3"><img src="{{ asset($product->thumbnail_img) }}"></a>                                                                                                       
                                                    <span class="pr-4 d-block d-lg-none mt-1 d-lg-table-cell">{{ $product_name_with_choice }}</span>
                                                </td>                                                
                                                <td class="product-name d-none ">                                                    
                                                    <span class="pr-4 d-block">{{ $product_name_with_choice }}</span>
                                                </td>                                                
                                                <td class="product-price d-none d-lg-table-cell">                                                    
                                                    <span class="pr-3 d-block">{{ single_price($cartItem['price']) }}</span>
                                                </td>                                                
                                                <td class="product-quantity d-md-table-cell">                                                    
                                                    <div class="input-group input-group--style-2 pr-4" style="width: 130px;">                                                        
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-number" type="button" data-type="minus" data-field="quantity[{{ $key }}]"><i class="la la-minus"></i></button></span>                                                        
                                                                <input type="text" name="quantity[{{ $key }}]" class="form-control input-number" placeholder="1" value="{{ $cartItem['quantity'] }}" min="1" max="10" onchange="updateQuantity({{ $key }}, this)">                                                        
                                                                <span class="input-group-btn"><button class="btn btn-number" type="button" data-type="plus" data-field="quantity[{{ $key }}]">                                                                
                                                                <i class="la la-plus"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </td>                                                
                                                <td class="product-total">
                                                    <span>{{ single_price($cartItem['price']*$cartItem['quantity']) }}</span>
                                                </td>                                                
                                                <td class="product-remove"><a class="text-danger" href="#" onclick="removeFromCartView(event, {{ $key }})" class="text-right pl-4">                                                        
                                                    <i class="la la-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

<ul class="list-group list-group-flush mb-4 d-lg-none">
@php
                                        $total = 0;
                                        @endphp
                                        @foreach (Session::get('cart') as $key => $cartItem)
                                            @php
                                            $product = \App\Models\Product::find($cartItem['id']);
                                            $total = $total + $cartItem['price']*$cartItem['quantity'];
                                            $product_name_with_choice = $product->name;
                                            if(isset($cartItem['color'])){
                                                $product_name_with_choice .= ' - '.\App\Models\Color::where('code', $cartItem['color'])->first()->name;
                                            }
                                            foreach (json_decode($product->choice_options) as $choice){
                                                $str = $choice->name; // example $str =  choice_0
                                                $product_name_with_choice .= ' - '.$cartItem[$str];
                                            }
                                            @endphp
<li class="list-group-item">
    <div class="row">
        <div class="col-auto align-self-center">
            <button class="btn btn-sm btn-link p-0 float-right"><i class="fa fa-minus-circle text-danger text-lg"></i></button>
        </div>
        <div class="col-2 pl-0 align-self-center">
            <figure class="product-image h-auto"><img src="{{ asset($product->thumbnail_img) }}" class="vm"></figure>
        </div>
        <div class="col px-0">
            <a href="#" class="text-dark mb-1 d-block">{{ $product_name_with_choice }} </a>
            <h5 class="text-success font-weight-normal mb-0">{{ single_price($cartItem['price']*$cartItem['quantity']) }}
                    {{-- <small class="text-muted">/4</small> --}}
            </h5>
            <p class="text-secondary small text-mute mb-0">Unit Price <span class=" text-success ml-2">{{ single_price($cartItem['price']) }}</span></p>
        </div>
        <div class="col-auto align-self-center">
<div class="input-group input-group-sm">                                                        
<div class="input-group-prepend">
<button class="btn btn-number" type="button" data-type="minus" data-field="quantity[{{ $key }}]"><i class="la la-minus"></i></button>
</div>                                                        
<input type="text" name="quantity[{{ $key }}]" class="form-control input-number w-35" placeholder="1" value="{{ $cartItem['quantity'] }}" min="1" max="10" onchange="updateQuantity({{ $key }}, this)">                                                        
<div class="input-group-prepend">
<button class="btn btn-number" type="button" data-type="plus" data-field="quantity[{{ $key }}]"><i class="la la-plus"></i></button>
                                                </div></div>

        </div>
    </div>
</li>
@endforeach
</ul>

                            </div>
                        </div>

                        <div class="row align-items-center pt-4 d-none d-lg-block">
                            <div class="col-6">
                                <a href="{{ route('home') }}" class="link link--style-3">
                                    <i class="la la-mail-reply"></i>
                                    {{__('Return to shop')}}
                                </a>
                            </div>
                            <div class="col-6 text-right">
                                @if(Auth::check())
                                    <a href="{{ route('checkout.shipping_info') }}" class="btn btn-styled btn-base-1">{{__('Continue to Shipping')}}</a>
                                @else
                                    <button class="btn btn-styled btn-base-1" onclick="showCheckoutModal()">{{__('Checkout')}}</button>
                                @endif
                            </div>
                        </div>
                    </div>




                    <!-- </form> -->
                </div>

                <div class="col-xl-4 ml-lg-auto"> 
                 @include('frontend.partials.cart_summary')                </div>
            </div>
            @else
                <div class="dc-header">
                    <h3 class="heading heading-6 strong-700">{{__('Your Cart is empty')}}</h3>
                </div>
            @endif
        </div>



<script type="text/javascript">

    cartQuantityInitialize();

</script>

