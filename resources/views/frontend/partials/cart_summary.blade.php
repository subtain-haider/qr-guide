<!-- order summary -->
<div class="payment-page-summary-head">
    <h2>{{__('ORDER SUMMARY')}}</h2>
    <p>@if(Session::has('cart')) {{ count(Session::get('cart')) }} @else 0 @endif {{__('Items')}}</p>
</div>
<div class="payment-page-summary-data">
    <div class="payment-page-summary-detail-1">
        <div class="payment-page-summary-detail-1-head">
            <h4>{{__('Last Orders')}}</h4>
        </div>
        <div class="payment-page-summary-detail-table-data">
            <table>
                <tr>
                    <th>{{__('PRODUCTS NAME')}}</th>
                    <th>{{__('Quantity')}}</th>
                    <th>{{__('Amount')}}</th>
                </tr>
                @php
                    $subtotal = 0;
                    $tax = 0;
                    $shipping = 0;
                @endphp
                @foreach (Session::get('cart') as $key => $cartItem)
                    @php
                    $product = \App\Models\Product::find($cartItem['id']);
                    $subtotal += $cartItem['price']*$cartItem['quantity'];
                    $tax += $cartItem['tax']*$cartItem['quantity'];
                    $shipping += $cartItem['shipping']*$cartItem['quantity'];
                    $product_name_with_choice = $product->name;
                    if(isset($cartItem['color'])){
                        $product_name_with_choice .= ' - '.\App\Models\Color::where('code', $cartItem['color'])->first()->name;
                    }
                    foreach (json_decode($product->choice_options) as $choice){
                        $str = $choice->name; // example $str =  choice_0
                        $product_name_with_choice .= ' - '.$cartItem[$str];
                    }
                    @endphp
                    <tr>
                        <td>{{ $product_name_with_choice }}</td>
                        <td>{{ $cartItem['quantity'] }}</td>
                        <td>{{ single_price($cartItem['price']*$cartItem['quantity']) }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="payment-page-summary-detail-2">
        <div class="payment-page-summary-detail-2-head">
            <h4>{{__('TOTAL')}}</h4>
        </div>
        <div class="payment-page-summary-detail-total-table-data">
            <table>
                <tr>
                    <td>{{__('SUBTOTAL')}}</td>
                    <td class="payment-page-summary-right">{{ single_price($subtotal) }}</td>
                </tr>
                <tr>
                    <td>{{__('TAX')}}</td>
                    <td class="payment-page-summary-right">{{ single_price($tax) }}</td>
                </tr>
                <tr>
                    <td>{{__('TOTAL SHIPPING')}}</td>
                    <td class="payment-page-summary-right">{{ single_price(Session::get('shipping_method')['cost']) }}</td>
                </tr>
                <tr>
                    <td>{{__('TOTAL')}}</td>
                    @php
                        //$total = $subtotal+$tax+$shipping;
                        $total = $subtotal+$tax+Session::get('shipping_method')['cost'];
                        if(Session::has('coupon_discount')){
                            $total -= Session::get('coupon_discount');
                        }
                        
                    @endphp
                        
                    <td class="payment-page-summary-right">{{ single_price($total) }}</td>
                </tr>
                <tr>
                    <td>{{__('Coupon Code')}}</td>
                    
                    <td class="payment-page-summary-right">
                            <input type="text" name="couponcode" {{ (Session::has('coupon_discount'))? "disabled" : "" }}  id="couponcode" placeholder='code'>
                            <input type="hidden" id="tokn" name="_token" value="{{ csrf_token() }}">
                            @if(Session::has('coupon_discount'))
                            <a class="btn btn-xs btn-success btn_remove" disabled href="#">Applied</a>
                            @else
                            <a class="btn btn-xs btn-success btn_remove" href="#">Apply</a>
                            @endif
                        </form>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function () {
    $('.btn_remove').on('click', function() {
        var tokn = $('#tokn').val(); 
      var coupon_code = $('#couponcode').val();  // get the `id` from data property
    
      // send a ajax request
      $.ajax({
         type: 'POST',
         url: '{{ route("checkout.apply_coupon") }}',
         data: { coupon_code: coupon_code, _token: tokn },
         success: function (data) { 
             console.log(data);
            location.reload();
                    }
      });
    
    });
});
</script>