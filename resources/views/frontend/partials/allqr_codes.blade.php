
<div style="margin-left:auto;margin-right:auto;">
<style media="all">
	@import url('https://fonts.googleapis.com/css?family=Open+Sans:400,700');
	*{
		margin: 0;
		padding: 0;
		line-height: 1.5;
		font-family: 'Open Sans', sans-serif;
		color: #333542;
	}
	div{
		font-size: 1rem;
	}
	.gry-color *,
	.gry-color{
		color:#878f9c;
	}
	table{
		width: 100%;
	}
	table th{
		font-weight: normal;
	}
	table.padding th{
		padding: .5rem .7rem;
	}
	table.padding td{
		padding: .7rem;
	}
	table.sm-padding td{
		padding: .2rem .7rem;
	}
	.border-bottom td,
	.border-bottom th{
		border-bottom:1px solid #eceff4;
	}
	.text-left{
		text-align:left;
	}
	.text-right{
		text-align:right;
	}
	.small{
		font-size: .85rem;
	}
	.strong{
		font-weight: bold;
	}
</style>

	@php
		$generalsetting = \App\Models\GeneralSetting::first();
	@endphp
	 @foreach($shop as $qr)
	<div style="padding: 1.5rem; text-align:center; margin-top:1.5rem">
        <h2 style="margin-bottom:1.5rem">
            @if($qr->title == null)
                Mini Site Title: Untitled
            @else
                Mini Site Title: {{ $qr->title }};
            @endif
        </h2>
        <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(200)->generate('http://'.$qr->slug.'.myqrguide.com')) }} ">
        <!-- <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(200)->generate(route('company.visit', $qr->slug))) }} "> -->
    </div>
@endforeach
</div>
