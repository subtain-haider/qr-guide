@extends('frontend.layouts.new_app')

@section('content')
    <!-- <section>
        <img src="https://via.placeholder.com/2000x300.jpg" alt="" class="img-fluid">
    </section> -->

    @php
        $total = 0;
        $rating = 0;
        foreach ($product->user->products as $key => $seller_product) {
            $total += $seller_product->reviews->count();
            $rating += $seller_product->reviews->sum('rating');
        }
    @endphp

        <!-- product picture -->
        <section class="qr-product-page-sec">
            <div class="page">
                <div class="qr-product-page-head">
                    <h2>PRODUCT PICTURES</h2>
                </div>
                @php
                    $main_photo_3 = $main_photo_2 = $main_photo = '';
                    if(!empty($product->photos)){
                        $photos = json_decode($product->photos);
                        $main_photo = $photos[0];
                        if(isset($photos[1])){
                            $main_photo_2 = $photos[1];
                        }
                        if(isset($photos[2])){
                            $main_photo_3 = $photos[2];
                        }
                    }
                @endphp
                <div class="pro-page-pro-pic-data">
                    <div class="pro-page-pro-pic-detail-1">
                        <div class="pro-page-pro-pic-1">
                            <a href="#"><img src="@if($product->thumbnail_img != ''){{ asset($product->thumbnail_img) }} @elseif($product->featured_img != ''){{ asset($product->featured_img) }} @elseif($product->flash_deal_img != ''){{ asset($product->flash_deal_img) }} @elseif($main_photo != ''){{ asset($main_photo) }} @else {{ asset('img/no-image-available.png') }} @endif"></a>
                        </div>
                    </div>
                    <div class="pro-page-pro-pic-detail-2">
                        <div class="pro-page-pro-pic-rt">
                            <a href="#"><img src="@if($main_photo_2 != ''){{ asset($main_photo_2) }} @else {{ asset('img/no-image-available.png') }} @endif"></a>
                        </div>
                        <div class="pro-page-pro-pic-rt pro-pic-rt-mrg ">
                            <a href="#"><img src="@if($main_photo_3 != ''){{ asset($main_photo_3) }} @else {{ asset('img/no-image-available.png') }} @endif"></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- link to product store -->
        <section class="pro-page-store-sec">
            <div class="page">
                <div class="pro-page-store-head">
                    <h2>LINK TO PRODUCT STORE</h2>
                </div>
                <div class="pro-page-store-data">
                    @if ($product->video_provider == 'youtube' && $product->video_link != null)
                        <iframe width="100%" height="409" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="embed-responsive-item" src="https://www.youtube.com/embed/{{ explode('=', $product->video_link)[1] }}"></iframe>
                    @elseif ($product->video_provider == 'dailymotion' && $product->video_link != null)
                        <iframe class="embed-responsive-item" src="https://www.dailymotion.com/embed/video/{{ explode('video/', $product->video_link)[1] }}"></iframe>
                    @elseif ($product->video_provider == 'vimeo' && $product->video_link != null)
                        <iframe src="https://player.vimeo.com/video/{{ explode('vimeo.com/', $product->video_link)[1] }}" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    @endif
                </div>
            </div>
        </section>

        <!-- related products -->
        <section class="pro-page-related-sec">
            <div class="page">
                <div class="pro-page-related-head">
                    <h2>RELATED PRODUCTS</h2>
                </div>
                <div class="pro-page-related-hr">
                    <hr>
                </div>
                <div class="pro-page-related-data">
                    @foreach (filter_products(\App\Models\Product::where('subcategory_id', $product->subcategory_id)->where('id', '!=', $product->id))->limit(3)->get() as $key => $related_product)
                        @php
                            $main_photo = '';
                            if(!empty($related_product->photos)){
                                $photos = json_decode($related_product->photos);
                                $main_photo = $photos[0];
                            }
                        @endphp
                        <div class="pro-page-related-detail">
                            <div class="pro-page-related-pics">
                                <a href="#"><img src="@if($related_product->thumbnail_img != ''){{ asset($related_product->thumbnail_img) }} @elseif($related_product->featured_img != ''){{ asset($related_product->featured_img) }} @elseif($related_product->flash_deal_img != ''){{ asset($related_product->flash_deal_img) }} @elseif($main_photo != ''){{ asset($main_photo) }} @else {{ asset('img/no-image-available.png') }} @endif"></a>
                            </div>
                            <div class="pro-page-related-pics-text">
                                <a href="#"><h4>{{ __($related_product->name) }}</h4></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    
        <!-- Dowmload and view -->
        <section class="pro-page-downloads-sec">
            <div class="down-page">
                <div class="pro-downloads-head">
                    <h2>DOWNLOAD & VIEW<br> MANUALS BELOW</h2>
                </div>
                <div class="pro-page-downloads-data">
                    <div class="pro-page-downloads-text">
                        <div class="pro-page-downloads-para">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin blandit libero quis lacus luctus ultricies. Phasellus sed risus id ex dictum laoreet. Pellentesque blandit scelerisque urna, in tempor nibh ultricies sed. Phasellus tempus risus eros, at rutrum orci porttitor et. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi fringilla, leo ac feugiat pulvinar, nisl sem tristique libero, vel bibendum orci diam eu sapien. Nam efficitur, nisi nec accumsan feugiat, ex dui tristique nisl, vel semper metus nunc eget massa. Aliquam risus tortor, cursus vel ligula ac, suscipit vulputate felis. Donec ut odio non libero pretium pulvinar sit amet vel sapien. Sed et est urna. Etiam quis elit nisi. Suspendisse commodo consequat aliquam. Pellentesque quis vulputate nisl, id varius est. Nullam euismod nunc ut turpis aliquam, vitae fringilla ante laoreet. Vivamus vitae metus ac eros dictum mollis.
                            </p>
                        </div>
                    </div>
					@if($product->pdf != null and $product->pdf != '')
						@php
						$pdfs = json_decode($product->pdf);
						@endphp
						<div class="pro-page-downloads-btns">
							<div class="pro-page-downloads-btns-detail">
								<button onclick="window.open('http:/\/\docs.google.com/gview?url={{ URL::to('/') }}/public/{{ $pdfs[0]->path }}&embedded=true', '_blank')">VIEW PDF</button>
							</div>
							<div class="pro-page-downloads-btns-detail">
								<button onclick="window.open('/{{ $pdfs[0]->path }}', '_blank')">Download PDF</button>
							</div>
						</div>
					@endif
                </div>
            </div>
            <div class="pro-page-downloads-overlay"></div>
        </section>
        <!-- product information -->
        <section class="pro-page-informa-sec">
            <div class="page">
                <div class="pro-infoarma-head">
                    <h2>PRODUCT INFORMATION</h2>
                </div>
                <?php echo $product->description; ?>
            </div>
        </section>
    
        <!-- company profile -->
	@php
		$Permissions = array();
		if(\App\Models\PricingPlan::where('id', $product->user->shop->package)->count() > 0){
			$PackageInfo = \App\Models\PricingPlan::where('id', $product->user->shop->package)->first();
			$Permissions = json_decode($PackageInfo->permissions);
		}
	@endphp
	@if(in_array(2,$Permissions))
	<section class="pro-page-profile-sec">
		<div class="page">
			<div class="pro-profile-head">
				<h2>COMPANY PROFILE</h2>
			</div>
			<div class="pro-page-profile-content">
                @if ($product->user->shop->sliders != null)
                    @foreach (json_decode($product->user->shop->sliders) as $key => $slide)
                        <div class="pro-page-profile-img">
                            <img src="{{ asset($slide) }}">
                        </div>
                    @endforeach
                @endif
				<div class="pro-page-profile-text">
					@php
                        echo $product->user->shop->description;
                    @endphp
				</div>
			</div>
			<div class="pro-page-profile-data">
				<div class="pro-page-profile-detail-1">
                    @foreach (filter_products(\App\Models\Product::where('user_id', $product->user->shop->user_id)->orderBy('id', 'DESC'))->limit(1)->get() as $key => $productImg)
                        @php
                            if(!empty($productImg->photos)){
                                $photos = json_decode($productImg->photos);
                                $main_photo = $photos[0];
                            }
                        @endphp
                        <div class="pro-page-profile-detail-pics">
                            <a href="#"><img src="@if($productImg->thumbnail_img != ''){{ asset($productImg->thumbnail_img) }} @elseif($productImg->featured_img != ''){{ asset($productImg->featured_img) }} @elseif($productImg->flash_deal_img != ''){{ asset($productImg->flash_deal_img) }} @elseif($main_photo != ''){{ asset($main_photo) }} @else {{ asset('img/no-image-available.png') }} @endif"></a>
                        </div>
                    @endforeach
                </div>
                <div class="pro-page-profile-detail-2">
                    @foreach (filter_products(\App\Models\Product::where('user_id', $product->user->shop->user_id)->orderBy('id', 'DESC'))->skip(1)->limit(4)->get() as $key => $productImg)
                        @php
                            if(!empty($productImg->photos)){
                                $photos = json_decode($productImg->photos);
                                $main_photo = $photos[0];
                            }
                        @endphp
                        <div class="pro-page-profile-imgs-detail pro-page-profile-imgs-mr">
                            <a href="#"><img src="@if($productImg->thumbnail_img != ''){{ asset($productImg->thumbnail_img) }} @elseif($productImg->featured_img != ''){{ asset($productImg->featured_img) }} @elseif($productImg->flash_deal_img != ''){{ asset($productImg->flash_deal_img) }} @elseif($main_photo != ''){{ asset($main_photo) }} @else {{ asset('img/no-image-available.png') }} @endif"></a>
                        </div>
                    @endforeach
                </div>
                <div class="pro-page-profile-detail-3">
                    @foreach (filter_products(\App\Models\Product::where('user_id', $product->user->shop->user_id)->orderBy('id', 'DESC'))->skip(1)->limit(4)->get() as $key => $productImg)
                        @php
                            if(!empty($productImg->photos)){
                                $photos = json_decode($productImg->photos);
                                $main_photo = $photos[0];
                            }
                        @endphp    
                        <div class="pro-page-profile-detail-pics">
                            <a href="#"><img src="@if($productImg->thumbnail_img != ''){{ asset($productImg->thumbnail_img) }} @elseif($productImg->featured_img != ''){{ asset($productImg->featured_img) }} @elseif($productImg->flash_deal_img != ''){{ asset($productImg->flash_deal_img) }} @elseif($main_photo != ''){{ asset($main_photo) }} @else {{ asset('img/no-image-available.png') }} @endif"></a>
                        </div>
                    @endforeach
                </div>
			</div>
			<div class="pro-page-profile-wranty-data">
				<div class="pro-page-profile-wranty-detail-1">
					<div class="pro-page-profile-wranty-icon-data">
						<div class="pro-page-profile-wranty-icon">
							<i class="fa fa-check-square-o"></i>
						</div>
						<div class="pro-page-profile-wranty-text">
							<h3><a href="#"> Register your warranty here</h3></a>
						</div>
					</div>
				</div>
				<div class="pro-page-profile-wranty-detail-2">
					<div class="pro-page-profile-wranty-icon-data">
						<div class="pro-page-profile-wranty-icon">
							<i class="fa fa-users"></i>
						</div>
						<div class="pro-page-profile-wranty-text">
							<h3><a href="#"> Join Our Club Member</h3></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	@endif
	<!-- coustomer reviews -->
	<section class="pro-page-reviews-sec">
		<div class="page">
			<div class="pro-reviews-head">
				<h2>CUSTOMER REVIEWS</h2>
			</div>
			<div class="pro-page-reviews-data">
				<div class="pro-page-reviews-detail-1">
					<div class="pro-page-reviews-content">
						<div class="pro-page-reviews-content-head">
							<h3>Customer reviews</h3>
						</div>
						<div class="pro-page-reviews-ls-stars-data">
							<div class="pro-page-reviews-ls-stars">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-o"></i>
							</div>
							<div class="pro-page-reviews-ls-stars-text">
								<p>4.8 out of 5</p>
							</div>
						</div>
						<div class="pro-page-reviews-global">
							<p>8,895 global ratings</p>
						</div>
						<!-- 1rating -->
						<div class="pro-page-ratings-data">
							<div class="pro-page-ratings-detail-1">
								<p>5 star</p>
							</div>
							<div class="pro-page-ratings-detail-2">
								<div class="pro-page-ratings-high-1">
								</div>
								<div class="pro-page-ratings-low-1">
								</div>
							</div>
							<div class="pro-page-ratings-detail-3">
								<p>86%</p>
							</div>
						</div>
						<!-- 2rating -->
						<div class="pro-page-ratings-data">
							<div class="pro-page-ratings-detail-1">
								<p>4 star</p>
							</div>
							<div class="pro-page-ratings-detail-2">
								<div class="pro-page-ratings-high-2">
								</div>
								<div class="pro-page-ratings-low-2">
								</div>
							</div>
							<div class="pro-page-ratings-detail-3">
								<p>9%</p>
							</div>
						</div>
						<!-- 3rating -->
						<div class="pro-page-ratings-data">
							<div class="pro-page-ratings-detail-1">
								<p>3 star</p>
							</div>
							<div class="pro-page-ratings-detail-2">
								<div class="pro-page-ratings-high-3">
								</div>
								<div class="pro-page-ratings-low-3">
								</div>
							</div>
							<div class="pro-page-ratings-detail-3">
								<p>3%</p>
							</div>
						</div>
						<!-- 4rating -->
						<div class="pro-page-ratings-data">
							<div class="pro-page-ratings-detail-1">
								<p>2 star</p>
							</div>
							<div class="pro-page-ratings-detail-2">
								<div class="pro-page-ratings-high-4">
								</div>
								<div class="pro-page-ratings-low-4">
								</div>
							</div>
							<div class="pro-page-ratings-detail-3">
								<p>1%</p>
							</div>
						</div>
						<!-- 5rating -->
						<div class="pro-page-ratings-data">
							<div class="pro-page-ratings-detail-1">
								<p>1 star</p>
							</div>
							<div class="pro-page-ratings-detail-2">
								<div class="pro-page-ratings-high-5">
								</div>
								<div class="pro-page-ratings-low-5">
								</div>
							</div>
							<div class="pro-page-ratings-detail-3">
								<p>1%</p>
							</div>
						</div>
						<div class="pro-page-ratings-calculate">
							<p><i class="fa fa-angle-down"></i>How are ratings calculated?</p>
						</div>

					</div>
				</div>
				<div class="pro-page-reviews-detail-2">
					<!-- coment-1 -->
					<div class="pro-page-reviews-coments-data">
						<div class="pro-page-reviews-coments-detail">
							<div class="pro-page-reviews-coments-img">
								<img src="{{ asset('frontend/images/tes1.jpg') }}">
							</div>
							<div class="pro-page-reviews-coments-img-text">
								<div class="pro-page-reviews-client-data">
									<div class="pro-page-reviews-client-name">
										<h4>John Deo</h4>
									</div>
									<div class="pro-page-reviews-client-stars">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-o"></i>
									</div>
									
								</div>
								<div class="pro-page-reviews-client-msg">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin blandit libero quis lacus luctus ultricies.</p>
								</div>
							</div>
						</div>

					</div>

					<!-- coment-1 -->
					<div class="pro-page-reviews-coments-data">
						<div class="pro-page-reviews-coments-detail">
							<div class="pro-page-reviews-coments-img">
								<img src="{{ asset('frontend/images/tes1.jpg') }}">
							</div>
							<div class="pro-page-reviews-coments-img-text">
								<div class="pro-page-reviews-client-data">
									<div class="pro-page-reviews-client-name">
										<h4>John Deo</h4>
									</div>
									<div class="pro-page-reviews-client-stars">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-o"></i>
									</div>
									
								</div>
								<div class="pro-page-reviews-client-msg">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin blandit libero quis lacus luctus ultricies.</p>
								</div>
							</div>
						</div>
						
					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- customers questions and answers -->
	<section class="pro-page-ques-sec">
		<div class="page">
			<div class="pro-page-ques-head">
				<h2>CUSTOMER QUESTION AND ANSWERS</h2>
			</div>
			<div class="pro-page-ques-input">
				<form class="pro-page-ques-form">
					<button type="submit"><i class="fa fa-search"></i></button>
				  	<input type="text" placeholder="Have aquestion? Search for answers" name="search">
				</form>
			</div>
			<!-- faq 1 -->
			@auth
				@if(\App\Models\BusinessSetting::where('type', 'question_answer')->first()->value == 1)
					<div class="pro-page-ques-input">
						<form class="pro-page-ques-form" action="{{ route('question.store_question') }}" method="post">
							@csrf
							<input type="hidden" name="product_id" value="{{$product->id}}">
							<input type="text" placeholder="Ask Question" name="question" style="width:90%;border-right: 0px;border-radius: 6px;border-top-right-radius: 0px;border-bottom-right-radius: 0px;">
							<button type="submit" style="width:10%;border-left: none;cursor: pointer;border-top-left-radius: 0px;border-bottom-left-radius: 0px;border-top-right-radius: 6px;border-bottom-right-radius: 6px;">{{__('Submit')}}</button>
						</form>
					</div>
				@endif
			@endauth
			@if(\App\Models\Question::where('product_id', $product->id)->count() > 0)
				@foreach(\App\Models\Question::where('product_id', $product->id)->where('parent_id', null)->get() as $key => $question)
					<div class="pro-page-ques-data">
						<div class="pro-page-ques-datail-1">
							<div class="pro-page-votes-icon">
								<i class="fa fa-caret-up"></i>
							</div>
							<div class="pro-page-votes-icon-text">
								<p>8</p>
								<p>votes</p>
							</div>
							<div class="pro-page-votes-icon">
								<i class="fa fa-caret-down"></i>
							</div>
						</div>
						<div class="pro-page-ques-datail-2">
							<div class="pro-page-ques-text-data">
								<div class="pro-page-ques-text-h-1">
									<h3>Question:</h3>
								</div>
								<div class="pro-page-ques-text-h-2">
									<h3>{{ $question->comment }}</h3>
								</div>
							</div>
							@if(\App\Models\Question::where('product_id', $product->id)->where('parent_id', $question->id)->count() > 0)
								<div class="pro-page-ques-ans-data">
									<div class="pro-page-ans-text-h-1">
										<h3>Answer:</h3>
									</div>
									<div class="pro-page-ans-ans-h-2">
										<p>{{ \App\Models\Question::where('product_id', $product->id)->where('parent_id', $question->id)->first()->comment }}</p>
										<div class="pro-page-ans-analys">
											@php
												$created_at = strtotime($question->created_at);
											@endphp
											<p>By {{\App\Models\User::where('id', $question->user_id)->first()->name}} on {{ date('Fd, Y',$created_at) }}</p>
										</div>
										@if(\App\Models\Question::where('product_id', $product->id)->where('parent_id', $question->id)->orderBy('id', 'DESC')->count() > 1)
											<div class="pro-page-ans-see-more" id="pro-page-ans-see-more">
												<p><i class="fa fa-angle-down"></i>See more answers[{{\App\Models\Question::where('product_id', $product->id)->where('parent_id', $question->id)->orderBy('id', 'DESC')->count()-1}}]</p>
											</div>
										@endif
									</div>
								</div>
								<div id="ques-ans-data-hide" style="display: none;">
									@foreach(\App\Models\Question::where('product_id', $product->id)->where('parent_id', $question->id)->orderBy('id', 'DESC')->get() as $key=>$answer)
										@if($key > 0)
											<div class="pro-page-ques-ans-data ques-ans-data-hide">
												<div class="pro-page-ans-text-h-1">
													<h3>Answer:</h3>
												</div>
												<div class="pro-page-ans-ans-h-2">
													<p>{{ $answer->comment }}</p>
													<div class="pro-page-ans-analys">
														@php
															$created_at = strtotime($answer->created_at);
														@endphp
														<p>By {{\App\Models\User::where('id', $question->user_id)->first()->name}} on {{ date('Fd, Y',$created_at) }}</p>
													</div>
												</div>
											</div>
										@endif
									@endforeach
								</div>
							@endif
						</div>
					</div>
				@endforeach
			@endif
		</div>
	</section>
	<!-- More guides -->
	@if(in_array(6,$Permissions))
	<section class="pro-page-guides-sec">
		<div class="page">
			<div class="pro-page-guides-head">
				<h2>MORE GUIDES</h2>
			</div>
			<div class="pro-page-guides-data">
                @php
                    $MoreShops = \App\Models\Product::where('id', '!=', $product->user->id)->orderBy('id', 'desc')->limit(3)->get();
                @endphp
                @foreach ($MoreShops as $product)
                    @php
                        $main_photo_3 = $main_photo_2 = $main_photo = '';
                        if(!empty($product->photos)){
                            $photos = json_decode($product->photos);
                            $main_photo = $photos[0];
                            if(isset($photos[1])){
                                $main_photo_2 = $photos[1];
                            }
                            if(isset($photos[2])){
                                $main_photo_3 = $photos[2];
                            }
                        }
                    @endphp
                    <div class="pro-page-guides-detail">
                        <div class="pro-page-guides-pics">
                            <a href="{{ route('product', $product->slug) }}"><img src="@if($product->thumbnail_img != ''){{ asset($product->thumbnail_img) }} @elseif($product->featured_img != ''){{ asset($product->featured_img) }} @elseif($product->flash_deal_img != ''){{ asset($product->flash_deal_img) }} @elseif($main_photo != ''){{ asset($main_photo) }} @else {{ asset('img/no-image-available.png') }} @endif"></a>
                        </div>
                        <div class="pro-page-guides-contents">

                            <div class="pro-page-guides-title-data">
								@isset($product->user->shop->logo)
                                <div class="pro-page-guides-title-img">
                                    <a href="{{ route('product', $product->slug) }}"><img src="{{ asset($product->user->shop->logo) }}"></a>
                                </div>
								@endisset
                                <div class="pro-page-guides-title-text">
                                    <a href="{{ route('product', $product->slug) }}">{{ $product->name }}</a>
                                </div>
                            </div>
                            <div class="pro-page-guides-title-para" style="white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">
                                <p>@php echo $product->description @endphp</p>
                            </div>
                            <div class="pro-page-guides-title-rating-data">
                                <div class="pro-page-guides-title-rating-detail">
                                    <p><span><i class="fa fa-star"></i>5.0</span> (350)</p>
                                </div>
                                @php
                                $pdfUrl = route('pdf.view', $product->slug);
                                @endphp
                                <div class="pro-page-guides-title-btns">
                                    <button class="pro-page-guides-title-btn-1" onclick="window.open('{{ $pdfUrl }}', '_blank')">VIEW PDF</button>
                                    <button class="pro-page-guides-title-btn-2" onclick="window.open('/{{ $product->pdf }}', '_blank')">DOWNLOAD PDF</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

			</div>
		</div>
	</section>
	@endif
@endsection
