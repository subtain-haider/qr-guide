@extends('frontend.layouts.new_app_1')

@section('content')

    <section class="payment-page-method-sec">
        <div class="payment-page-wdth">
            <div class="payment-page-method-sec-head">
                <h2>{{__('SELECT PAYMENT METHOD')}}</h2>
            </div>
            <div class="payment-page-method-data">
                <div class="paymentmethodpropritabs">
                    <span class="paymentmethodpropritab"><button class="paymentmethodpropritabs-btn">{{__('MY CART')}}</button></span>
                    <span class="paymentmethodpropritab"><button class="paymentmethodpropritabs-btn">{{__('SHIPING INFO')}}</button></span>
                    <span class="paymentmethodpropritab"><button class="paymentmethodpropritabs-btn">{{__('PAYMENT METHOD')}}</button></span>
                </div>
                <div>
                    <div class="paymentmethodtab_content">
                        <div class="paymentmethodprotab_item">
                            <!-- shipping info -->
                            <div class="payment-page-basic-sec-head">
                                <h2>{{__('SHIPPING INFO')}}</h2>
                            </div>
                            {{-- <form class="payment-page-shipping-form" data-toggle="validator" action="{{ route('payment') }}" role="form" method="POST"> --}}

                            <form class="payment-page-shipping-form" data-toggle="validator" action="{{ route('checkout.store_shipping_infostore') }}" role="form" method="POST">
                                @csrf
                                <div class="payment-page-shipping-data">
                                    <!-- <form class="payment-page-shipping-form" data-toggle="validator" action="{{ route('checkout.store_shipping_infostore') }}" role="form" method="POST">
                                        @csrf -->
                                        @if(Auth::check())
                                            @php
                                                $user = Auth::user();
                                            @endphp
                                            <div class="payment-page-shipping-form-feilds payment-page-shipping-inputs">
                                                <input type="text" name="name" value="{{ $user->name }}" placeholder="*NAME" required>
                                            </div>
                                            <div class="payment-page-shipping-form-feilds payment-page-shipping-inputs">
                                                <input type="email" name="email" value="{{ $user->email }}" placeholder="*EMAIL" required>
                                            </div>

                                            <div class="payment-page-shipping-form-feilds payment-page-shipping-inputs">
                                                <input type="text" name="address" value="{{ $user->address }}" placeholder="*ADDRESS" required>
                                            </div>

                                            <div class="payment-page-shipping-form-feilds-data-2 payment-page-shipping-form-feilds">
                                                <div class="payment-page-shipping-form-feilds-detail-1 payment-page-shipping-inputs payment-page-shipping-inputs-margin">
                                                    <select class="form-control" id="countries" name="country">
                                                        @foreach (\App\Models\Country::all() as $key => $country)
                                                            <option value="{{ $country->name }}" @if ($country->code == $user->country) selected @endif>{{ $country->name }}</option>
                                                        @endforeach
                                                    </select>

                                                </div>

                                                <div class="payment-page-shipping-form-feilds-detail-1 payment-page-shipping-inputs ">
                                                    <input type="text" name="city" value="{{ $user->city }}" placeholder="*CITY" required>
                                                </div>
                                            </div>
                                            <div class="payment-page-shipping-form-feilds-data-2 payment-page-shipping-form-feilds">
                                                <div class="payment-page-shipping-form-feilds-detail-1 payment-page-shipping-inputs payment-page-shipping-inputs-margin">
                                                    <input type="text" name="postal_code" value="{{ $user->postal_code }}" placeholder="*POSTAL CODE" required>
                                                </div>

                                                <div class="payment-page-shipping-form-feilds-detail-1 payment-page-shipping-inputs">
                                                    <input type="text" name="phone" value="{{ $user->phone }}" placeholder="*PHONE" required>
                                                </div>
                                            </div>
                                        @else
                                            <div class="payment-page-shipping-form-feilds payment-page-shipping-inputs">
                                                <input type="text" name="name" value="" placeholder="*NAME" required>
                                            </div>
                                            <div class="payment-page-shipping-form-feilds payment-page-shipping-inputs">
                                                <input type="email" name="email" value="" placeholder="*EMAIL" required>
                                            </div>

                                            <div class="payment-page-shipping-form-feilds payment-page-shipping-inputs">
                                                <input type="text" name="address" value="" placeholder="*ADDRESS" required>
                                            </div>

                                            <div class="payment-page-shipping-form-feilds-data-2 payment-page-shipping-form-feilds">
                                                <div class="payment-page-shipping-form-feilds-detail-1 payment-page-shipping-inputs payment-page-shipping-inputs-margin">
                                                    <select class="form-control" id="countries" name="country">
                                                        @foreach (\App\Models\Country::all() as $key => $country)
                                                            <option value="{{ $country->name }}">{{ $country->name }}</option>
                                                        @endforeach
                                                    </select>

                                                </div>

                                                <div class="payment-page-shipping-form-feilds-detail-1 payment-page-shipping-inputs ">
                                                    <input type="text" name="city" value="" placeholder="*CITY" required>
                                                </div>
                                            </div>
                                            <div class="payment-page-shipping-form-feilds-data-2 payment-page-shipping-form-feilds">
                                                <div class="payment-page-shipping-form-feilds-detail-1 payment-page-shipping-inputs payment-page-shipping-inputs-margin">
                                                    <input type="text" name="postal_code" value="" placeholder="*POSTAL CODE" required>

                                                </div>

                                                <div class="payment-page-shipping-form-feilds-detail-1 payment-page-shipping-inputs">
                                                    <input type="text" name="phone" value="" placeholder="*PHONE" required>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="payment-page-shipping-form-feilds payment-page-shipping-inputs-checkbox">
                                            <input type="checkbox" id="paymentcheck" name="">
                                            <label for="paymentcheck">{{__('by confirming this box, i agree to the terms and conditions, privicy policy')}}</label>
                                        </div>
                                    <!-- </form> -->
                                </div>
                                <!-- Shiping Charges -->
                                <div class="payment-page-basic-sec-head">
                                    <h2>{{__('SHIPPING CHARGES')}}</h2>
                                </div>
                                <div class="payment-page-shipping-charges-data">
                                    @foreach (\App\Models\BusinessSetting::where('type', 'free_shipping')->get() as $free_shipping)
                                        @php
                                            $FreeShipping = json_decode($free_shipping->value);
                                        @endphp
                                        @if($FreeShipping->status == 'on')
                                            <div class="payment-page-shipping-charges-detail">
                                                <label class="w-100" style="margin-top:5px;">
                                                    <input type="radio" class="d-none" name="checkout_type" required value="free_shipping"@if(Session::get('shipping_method')['type'] == 'free_shipping') checked @endif>
                                                    <button type="button">{{__('FREE SHIPPING')}}</button>
                                                </label>
                                                <P>2-4 {{__('WEEKS')}}</P>
                                            </div>
                                        @endif
                                    @endforeach

                                    @foreach (\App\Models\BusinessSetting::where('type', 'standard_shipping')->get() as $standard_shipping)
                                        @php
                                            $StandardShipping = json_decode($standard_shipping->value);
                                        @endphp
                                        @if($StandardShipping->status == 'on')
                                            <div class="payment-page-shipping-charges-detail">
                                                <label class="w-100" style="margin-top:5px;">
                                                    <input type="radio" class="d-none" name="checkout_type" required value="standard_shipping"@if(Session::get('shipping_method')['type'] == 'standard_shipping') checked @endif>
                                                    <button type="button">{{__('STANDARD SHIPPING')}}</button>
                                                </label>
                                                <P>7-10 {{__('DAYS')}}</P>
                                            </div>
                                        @endif
                                    @endforeach

                                    @foreach (\App\Models\BusinessSetting::where('type', 'express_shipping')->get() as $express_shipping)
                                        @php
                                            $ExpressShipping = json_decode($express_shipping->value);
                                        @endphp
                                        @if($ExpressShipping->status == 'on')
                                            <div class="payment-page-shipping-charges-detail">
                                                <label class="w-100" style="margin-top:5px;">
                                                    <input type="radio" class="d-none" name="checkout_type" required value="express_shipping"@if(Session::get('shipping_method')['type'] == 'express_shipping') checked @endif>
                                                    <button type="button">{{__('EXPRESS SHIPPING')}}</button>
                                                </label>
                                                <P>3-4 {{__('DAYS')}}</P>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                @include('frontend.partials.cart_summary')
                                <div class="payment-page-proceed-btn">
                                    <button type="submit">{{__('CONTINUE TO PAYMENT')}}</button>
                                </div>
                                <div class="special-cart-bottom-s">
                                    <a href="{{ route('home') }}">Return to shop</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
    function removeFromCartView(e, key){
        e.preventDefault();
        removeFromCart(key);
    }

    function updateQuantity(key, element){
        $.post('{{ route('cart.updateQuantity') }}', { _token:'{{ csrf_token() }}', key:key, quantity: element.value}, function(data){
            updateNavCart();
            $('#cart-summary').html(data);
        });
    }

    function showCheckoutModal(){
        $('#GuestCheckout').modal();
    }

    $(function () {
        $('.payment-page-shipping-charges-detail button').on('click', function(){
            $(this).prev('input').prop('checked', true);
        });
    });
    </script>
@endsection
