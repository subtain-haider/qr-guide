@extends('frontend.layouts.new_app_1')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
@section('content')

 <section class="signin-form-sec pt-5">
	<div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
		<div class="page-title col-lg-12 mb-4">
                   <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                       {{$shop->name}} {{__('Warranty')}}
                   </h2>
                </div>
                @if($shop->warranty_content != null)
                <div class="col-lg-6">
                    {!! $shop->warranty_content !!}
                </div>
                @endif
        <div class="signin-form @if($shop->warranty_content != null) col-lg-6 @else col-lg-6 @endif">
            <form action="{{ route('shop.verify.store', $shop->slug) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="signin-form-sec-logo mb-4">
                    <a href="javascript:void();">@if($shop->logo != NULL)<img style="width: 135px; height: 135px;" src="{{ asset($shop->logo) }}">@else <img style="width: 135px; height: 135px;" src="{{ asset('frontend/images/rounded-logo.png') }}"> @endif</a>
                </div>
                <h2 class="mb-5">{{__('Warranty info')}}</h2>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control mb-3" placeholder="*DATE OF PURCHASE" name="date_of_purchase" required="required">
                                            <input type="hidden" name="warranty" value="{{ $shop->warranty }}" />
                                            <input type="hidden" name="shopid" value="{{ $shop->id }}" />
                                        </div>
                                    </div>
                                    <!--<div class="row">-->
                                    <!--    <div class="col-md-12">-->
                                    <!--        <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control mb-3" placeholder="*DATE OF PRODUCT ENDING" name="date_of_product_end" required>-->
                                            
                                    <!--    </div>-->
                                    <!--</div>-->
                @foreach (json_decode($shop->warranty) as $key => $element)
                                    @if ($element->type == 'text')
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="{{ $element->type }}" required="required" class="form-control mb-3" placeholder="{{ $element->label }}" name="element_{{ $key }}" required>
                                            </div>
                                        </div>
                                    @elseif($element->type == 'file')
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="{{ $element->type }}" name="element_{{ $key }}"  id="file-2" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" required/>
                                                <label for="file-2" class="mw-100 mb-3">
                                                    <span></span>
                                                    <strong>
                                                        <i class="fa fa-upload"></i>
                                                        {{ $element->label }}
                                                    </strong>
                                                </label>
                                            </div>
                                        </div>
                                    @elseif ($element->type == 'select' and $element->options != 'null')
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <select class="form-control "  required="required" name="element_{{ $key }}" required>
                                                        @foreach (json_decode($element->options) as $value)
                                                            <option value="{{ $value }}">{{ $value }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    @elseif ($element->type == 'multi_select' and $element->options != 'null')
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <select class="form-control demo-select2 multiselect" name="element_{{ $key }}[]" id="related_products" multiple required>
                                                        @foreach (json_decode($element->options) as $value)
                                                            <option value="{{ $value }}">{{ $value }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    @elseif ($element->type == 'radio')
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    @foreach (json_decode($element->options) as $value)
                                                        <div class="radio radio-inline">
                                                            <input type="radio" name="element_{{ $key }}" value="{{ $value }}" id="{{ $value }}" required>
                                                            <label for="{{ $value }}">{{ $value }}</label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                <div class="form-group">
                    @if (\Session::has('success'))
                    <div class="alert alert-success" role="alert">
                      {!! \Session::get('success') !!}
                    </div>
                    @endif 
                    @if (\Session::has('fail'))
                    <div class="alert alert-danger" role="alert">
                      {!! \Session::get('fail') !!}
                    </div>
                    @endif 
                    <button id="warranty_invoice_download" class="btn btn-success btn-lg btn-block">{{__('Download')}}</button>

                    <button type="submit" class="btn btn-danger btn-lg btn-block">{{__('Apply')}}</button>
                </div>
            </form>
        </div>
    </section>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
         $('.demo-select2').select2();
  $("#warranty_invoice_download").click(function(event){
     event.preventDefault();
    var alldata = $('form').serialize();
    console.log(alldata);
    $.ajax({
           type:'POST',
           url:"{{ route('company.warranty.download') }}",
           data:alldata,
           success:function(data){
               console.log(data);
               alert(data);
            window.open(data);
           }
        });
  });
});
</script>
@endsection