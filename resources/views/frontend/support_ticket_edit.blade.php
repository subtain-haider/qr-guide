@extends('frontend.layouts.new_app_1')

<style type="text/css">
.support-ticket-new {
    padding: 20px 0px;
    background-color: #F1F1F1;
}  
.spt-modal-inner-new {
    display: flex;
    justify-content: flex-start;
    flex-direction: column;
    width: 100%;
    background-color: #fff;
    padding: 10px 0px 10px 10px;
}

.spt-modal-inner-news {
    display: flex;
    justify-content: flex-start;
    flex-direction: column;
    width: 100%;
    background-color: #fff;
    padding: 15px 15px 15px 15px;
    margin-top: 18px;
}
.spt-modal-inner-new-p {
    margin-top: 10px;
    margin-bottom: 0px;
}
.spt-modal-inner-news-inner{
    margin-right: 1%;
}
.spt-modal-inner-news-inner p {
    margin-bottom: 4px;
}
.spt-modal-inner-news-inner input {
    padding: 3px 3px 3px 5px; 
    background-color: #F1F1F1;
    border:1px solid #F1F1F1;
    border-radius: 4px;
}
.spt-modal-inner-cst-new {
    display: flex;
    justify-content: flex-end;
    width: 100%;
    padding-top: 15px;

}
.spt-modal-inner-cst-new button{
    background-color: #2FA360;
    color: #fff;
    padding: 5px 10px;
    border: 1px solid #2FA360;
    border-radius: 4px;
}
.spt-modal-inner-news-inner-1{
    display: flex;
}



	.sugession-page-sec{
	display: flex;
	justify-content: center;
	padding: 50px 0px;
}
.sugession-page-wdth{
	width: 85%;
}
#suges-customers {
  font-family: 'Play';
  border-collapse: collapse;
  width: 100%;
}

#suges-customers td, #suges-customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#suges-customers tr:nth-child(even){background-color: #f2f2f2;}

#suges-customers tr:hover {background-color: #ddd;}

#suges-customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #1099B1;
  color: white;
}
#suges-customers td button{
	border: none;
	outline: none;
	padding: 4px 30px;
	border-radius: 0px;
	
	font-size: 14px;
	font-family: 'Play';
}
#suges-customers td .suges-customers-btn-1{
	background-color: #000;
	color: #fff;
}
#suges-customers td .suges-customers-btn-2{
	background-color: red;
	color: #fff;
}
</style>

@section('content')
	<section class="support-ticket-new sugession-page-sec">    
    <div class="spt-flex-1">
        <div class="spt-page">
            <div class="spt-flex-2">
                <button class="btn btn-{{ ($ticket->is_closed == '0') ? 'success' : 'primary'; }}" {{ ($ticket->is_closed == '0') ? '' : 'disabled'; }}>{{ ($ticket->is_closed == '0') ? 'Ticket is Open' : 'Ticket is Closed'; }}</button>
                <div class="spt-pg-inner-new">
                    @foreach ($ticket_replies as $ticketreply)
                    <p class="spt-modal-inner-new-p">Posted by {{ $ticketreply->user->name }} on {{ date('d-m-Y', strtotime($ticketreply->created_at)) }}</p>
                    <div class="spt-modal-inner-new">
                        <h6>{{ $ticketreply->user->name }}</h6>
                        <p>{{ $ticketreply->reply }}</p>
                    </div>
             @endforeach
                    
                    <div class="spt-modal-inner-news">
                        <h3>Reply</h3>
                        <div class="spt-modal-inner-news-inner-1">
                            <div class="spt-modal-inner-news-inner">
                                <p>Subject</p>
                                <input value="{{ $ticket->subject }}">
                            </div>
                            <div class="spt-modal-inner-news-inner">
                                <p>Deatils</p>
                                <input value="{{ $ticket->details }}">
                            </div>
                        </div>
                        <form class="form-horizontal" action="{{route('support_ticket.seller_store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="ticket_id" value="{{$ticket->id}}">
                            <input type="hidden" name="user_id" value="{{$ticket->user_id}}">
                            <textarea class="editor" name="reply" required {{ ($ticket->is_closed == '0') ? 'success' : 'readonly'; }}></textarea>
                            <div class="spt-modal-inner-cst-new">
                                <button  type="submit" {{ ($ticket->is_closed == '0') ? '' : 'disabled'; }}>{{__('Send Reply')}}</button>
                            </div>
                        </form>
                </div>
            </div> 
        </div> 
    </div>   
</section>

<div class="modal fade" id="ticket_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="modal-header">
                    <h5 class="modal-title strong-600 heading-5">{{__('Send Ticket')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="" action="{{ route('support_ticket.store') }}" method="post">
                    @csrf
                    <div class="modal-body gry-bg px-3 pt-3">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" class="form-control mb-3" name="subject" placeholder="Subject" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <textarea class="editor" name="details"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('cancel')}}</button>
                        <button type="submit" class="btn btn-success">{{__('Confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function show_ticket_modal(){
            $('#ticket_modal').modal('show');
        }
    </script>
@endsection