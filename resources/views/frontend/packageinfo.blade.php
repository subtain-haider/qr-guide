@extends('frontend.layouts.app')

@section('content')

    <section class="gry-bg py-4 profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @include('frontend.inc.seller_side_nav')
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                        {{__('Package Info')}}
                                    </h2>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="float-md-right">
                                        <ul class="breadcrumb">
                                            <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                                            <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                                            <li class="active"><a href="{{ route('packageinfo') }}">{{__('Package info')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9 m-auto">
                            <div class="card no-border mt-4">
                                <div>
                                    <table class="table table-sm table-hover table-responsive-md text-center">
                                        @php
                                            $PackageInfo = \App\Models\PricingPlan::where('id', Auth::user()->shop->package)->first();
                                            $PP = Auth::user()->shop->package_plane;

                                            $Permissions = array(
                                                1 => 'Simple Add Products',
                                                2 => 'Vendor Can add Company info',
                                                3 => 'Option to add external link such as main web site of the company',
                                                4 => 'Create a QR code to direct link to the company store',
                                                5 => 'Video Link',
                                                6 => 'Related Products',
                                                7 => 'Option to register people directly to the artist email address for future communication',
                                                8 => 'Full mini site capability to let client create his own guide as a mini site with 6 pages'
                                            )

                                        @endphp
                                        <thead>
                                            <tr>
                                                <th><b class="pull-left">{{__('Current Package:')}} {{ $PackageInfo->name }} @if(Auth::user()->shop->package_plane != null){{$PackageInfo->$PP}}$/@if(Auth::user()->shop->package_plane == 'per_month')m @elseif(Auth::user()->shop->package_plane == 'per_year')Y @endif @endif</b><a href="#" class="btn btn-base-1 ml-3 pull-right">{{__('Upgrade Package')}}</a></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach (json_decode($PackageInfo->permissions) as $key => $val)
                                                <tr>
                                                    <td>{{$Permissions[$val]}}</td>
                                                </tr>    
                                            @endforeach
                                            {{-- <tr>
                                                <td>basic uploading pdf with preview of the pdf on the site and see the pdf </td>
                                            </tr>
                                            <tr>
                                                <td>Vendor Can add Company info</td>
                                            </tr>
                                            <tr>
                                                <td>Option to add external link such as main web site of the company</td>
                                            </tr>
                                            <tr>
                                                <td>Create a QR code to direct link to the company store</td>
                                            </tr>
                                            <tr>
                                                <td>pdf + video uploading of the guide </td>
                                            </tr>
                                            <tr>
                                                <td>Upsales products he can upload</td>
                                            </tr>
                                            <tr>
                                                <td>Option to register people directly to the artist email address for future communication</td>
                                            </tr>
                                            <tr>
                                                <td>Full mini site capability to let client create his own guide as a mini site with 6 pages</td>
                                            </tr> --}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@php	$policies_class = ' d-none d-lg-block';@endphp