@extends('layouts.app')

@section('content')

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Activity Log')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>{{__('No')}}</th>
                    <th>{{__('User')}}</th>
                    <th>{{__('Shop')}}</th>
                    <th>{{__('Action')}}</th>
                </tr>
            </thead>
            <tbody>
                @if($warranties->count())
                    @foreach($warranties as $key => $warranty)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>
                            @if($warranty->user_id != null)
                                {{ \App\Models\User::where('id', $warranty->user_id)->first()->name }}
                            @else
                                {{__('Guest')}}
                            @endif
                        </td>
                        <td>{{ \App\Models\Shop::where('id', $warranty->shop_id)->first()->name }}</td>
                        <td>
                            <a href="{{ route('show_warranty_request', $warranty->id) }}">
                                <div class="label label-table label-info">
                                    {{__('View')}}
                                </div>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>

    </div>
</div>

@endsection
