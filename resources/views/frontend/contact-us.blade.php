@extends('frontend.layouts.new_app_1')	
<!-- contact-us page -->

@section('content')
<section class="contact-us-page-form-sec">
		<div class="signin-form">
		    <form action="" method="">
		    	<div class="signin-form-sec-logo">
		    		<a href="#"><img src="http://myqrguide.com/public/frontend/images/rounded-logo.png"></a>
		    	</div>
				<h2>Contact Us</h2>
				
		        <div class="form-group">
		        	<input type="text" class="form-control input-lg" name="fullname" placeholder="Full Name" required="required">
		        	<i class="fa fa-user"></i>
		        </div>
				<div class="form-group">
		            <input type="password" class="form-control input-lg" name="email" placeholder="Email Address" required="required">
		           	<i class="fa fa-envelope-o"></i>
		        </div>
		        <div class="form-group">
		            <textarea rows="5" class="form-control input-lg" name="email" placeholder="Your Message" required="required"></textarea>
		        </div>
		        
		        <div class="form-group">
		            <button type="submit" class="btn btn-success btn-lg btn-block signin-btn">Send Message</button>
		        </div>
		        
		    </form>
		</div>
	</section>
	
@endsection
	