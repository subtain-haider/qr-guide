@extends('frontend.layouts.new_app_1')
@section('content')

 <section class="signin-form-sec pt-5">
	<div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
		<div class="page-title col-lg-12 mb-4">
                   <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                       {{$shop->club_text}} 
                   </h2>
                </div>
        <div class="signin-form col-lg-6">
            <form action="{{ route('company.clubsubmit') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="signin-form-sec-logo mb-4">
                    <a href="javascript:void();">@if($shop->logo != NULL)<img style="width: 135px; height: 135px;" src="{{ asset($shop->logo) }}">@else <img style="width: 135px; height: 135px;" src="{{ asset('frontend/images/rounded-logo.png') }}"> @endif</a>
                </div>
                <h2 class="mb-5">{{__('Club Info')}}</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <input class="form-control" type="hidden" value="{{ $shop->slug }}" name="slug">
                            <input type="email" class="form-control" name="email" id="exampleFormControlInput1"  required placeholder="name@example.com">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="contact_no" id="exampleFormControlInput2" required placeholder="(000)0000000">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="date" name="dob" class="form-control" placeholder="dd-mm-yyyy" value="" required min="1980-01-01" max="2030-12-31">
                        </div>
                    </div>
                    <div class="row" style="margin:5px 0;">
                        <div class="col-md-12">
                            <input type="checkbox" class="form-check-input" name="check" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Reciving Information From our Company</label>
                        </div>
                    </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg btn-block">{{__('Submit')}}</button>
                </div>
            </form>
        </div>
    </section>
@endsection