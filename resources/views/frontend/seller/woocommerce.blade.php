@extends('frontend.layouts.new_app_1')

<style type="text/css">
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
	.sugession-page-sec{
	display: flex;
	justify-content: center;
	padding: 50px 0px;
}
.sugession-page-wdth{
	width: 85%;
}
#suges-customers {
  font-family: 'Play';
  border-collapse: collapse;
  width: 100%;
}

#suges-customers td, #suges-customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#suges-customers tr:nth-child(even){background-color: #f2f2f2;}

#suges-customers tr:hover {background-color: #ddd;}

#suges-customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #1099B1;
  color: white;
}
#suges-customers td button{
	border: none;
	outline: none;
	padding: 4px 30px;
	border-radius: 0px;
	
	font-size: 14px;
	font-family: 'Play';
}
#suges-customers td .suges-customers-btn-1{
	background-color: #000;
	color: #fff;
}
#suges-customers td .suges-customers-btn-2{
	background-color: red;
	color: #fff;
}
</style>

@section('content') 
	
	<section class="sugession-page-sec">
	<div class="sugession-page-wdth">
		<div class="sugession-page-data">
		    <div class="row">
		        <div class="col-md-12" style="display: flex;justify-content: center;">
		            <img src="{{asset('img/add.png')}}" onClick="popModal()" alt="Avatar" style="border-radius: 50%;height:100px;width:100px;cursor:pointer">
		    </div>
		    
		    </div>
			<table class="table table-striped table-bordered demo-dt-basic" id="suges-customers" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Store URL</th>
                    <th>Consumer key</th>
                    <th>Consumer Secret</th>
            </thead>
            <tbody>
                @foreach($woocomerces as $woocomerce)
                <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$woocomerce->url}}</td>
                    <td>{{$woocomerce->consumer_key}}</td>
                    <td>{{$woocomerce->consumer_secret}}</td>
                </tr>
                @endforeach
            </tbody>
           
        </table>
		</div>
	</div>
	
	<div class="modal fade" id="woocommerceModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add Woocommerce store</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        
      
      <!-- Modal body -->
      <form action="{{route('woocommerce.store')}}" method="post">
      @csrf
        <div class="modal-body">
          <div class="form-group">
              <div class="row">
                  <div class="col-md-4">
                    <label for="">Store type :</label>          
                  </div>
                  <div class="col-md-8">
                    <label for="">WooCommerce</label>          
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-4">
                    <label for="">Store URL :</label>          
                  </div>
                  <div class="col-md-8">
                    <input type="text" class="form-control" name="url" placeholder="https://example.com">          
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-4">
                    <label for="">Consumer Key :</label>          
                  </div>
                  <div class="col-md-8">
                    <input type="text" class="form-control" name="consumer_key" placeholder="ck_xxxxxxxxxxx....">          
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-4">
                    <label for="">Consumer Secret :</label>          
                  </div>
                  <div class="col-md-8">
                    <input type="text" class="form-control" name="consumer_secret" placeholder="cs_xxxxxxxxxxx....">          
                  </div>
              </div>

          </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-info text-white">Authorize</button>
        </div>
        
      </div>
      </form>
    </div>
  </div>
    
</section>

@endsection



@section('script')
<script>
    function popModal()
    {
        $('#woocommerceModal').modal('show');
    }
</script>

@endsection