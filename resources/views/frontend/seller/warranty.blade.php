@extends('frontend.layouts.app')

@section('content')

    <section class="gry-bg py-4 profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @if(Auth::user()->user_type == 'seller')
                        @include('frontend.inc.seller_side_nav')
                    @elseif(Auth::user()->user_type == 'customer')
                        @include('frontend.inc.customer_side_nav')
                    @endif
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-md-6 col-12 mb-2">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                        {{__('Manage Warranty')}}
                                        <a href="{{ route('company_warranty.visit', Auth::user()->shop->slug) }}" class="btn btn-link btn-sm" target="_blank">({{__('Visit Company Warranty')}})<i class="la la-external-link"></i>)</a>
                                    </h2>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="float-md-right">
                                        <ul class="breadcrumb">
                                            <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                                            <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                                            <li class="active"><a href="{{ route('company.warranty') }}">{{__('Manage Warranty')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="{{ route('company.warranty.update') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-lg-8 form-horizontal" id="form">
                                    @if(Auth::user()->shop->warranty != null)
                                        @foreach (json_decode(Auth::user()->shop->warranty) as $key => $element)
                                            @if ($element->type == 'text' || $element->type == 'file')
                                                <div class="form-group d-inline-block w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">
                                                    <input type="hidden" name="type[]" value="{{ $element->type }}">
                                                    <div class="col-lg-3 pull-left">
                                                        <label class="control-label">{{ ucfirst($element->type) }}</label>
                                                    </div>
                                                    <div class="col-lg-7 pull-left">
                                                        <input class="form-control" type="text" name="label[]" value="{{ $element->label }}" placeholder="Label">
                                                    </div>
                                                    <div class="col-lg-2 pull-left"><i class="la la-times" onclick="delete_choice_clearfix(this)"></i></div>
                                                </div>
                                            @elseif ($element->type == 'select' || $element->type == 'multi_select' || $element->type == 'radio')
                                                <div class="form-group d-inline-block w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">
                                                    <input type="hidden" name="type[]" value="{{ $element->type }}">
                                                    <input type="hidden" name="option[]" class="option" value="{{ $key }}">
                                                    <div class="col-lg-3 pull-left">
                                                        <label class="control-label">{{ ucfirst(str_replace('_', ' ', $element->type)) }}</label>
                                                    </div>
                                                    <div class="col-lg-7 pull-left">
                                                        <input class="form-control" type="text" name="label[]" value="{{ $element->label }}" placeholder="Select Label" style="margin-bottom:10px">
                                                        <div class="customer_choice_options_types_wrap_child">
                                                            @foreach (json_decode($element->options) as $value)
                                                                <div class="form-group d-inline-block w-100">
                                                                    <div class="col-sm-6 col-sm-offset-4 pull-left">
                                                                        <input class="form-control" type="text" name="options_{{ $key }}[]" value="{{ $value }}" required="">
                                                                    </div>
                                                                    <div class="col-sm-2 pull-left"> <i class="la la-times" onclick="delete_choice_clearfix(this)"></i></div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                        <button class="btn btn-success pull-right" type="button" onclick="add_customer_choice_options(this)"><i class="glyphicon glyphicon-plus"></i> Add option</button>
                                                    </div>
                                                    <div class="col-lg-2 pull-left"><i class="la la-times" onclick="delete_choice_clearfix(this)"></i></div>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                                <div class="col-lg-4">
        
                                    <ul class="list-group">
                                        <li class="list-group-item btn" style="text-align: left;" onclick="appenddToForm('text')">{{__('Text Input')}}</li>
                                        <li class="list-group-item btn" style="text-align: left;" onclick="appenddToForm('select')">{{__('Select')}}</li>
                                        <li class="list-group-item btn" style="text-align: left;" onclick="appenddToForm('multi-select')">{{__('Multiple Select')}}</li>
                                        <li class="list-group-item btn" style="text-align: left;" onclick="appenddToForm('radio')">{{__('Radio')}}</li>
                                        <li class="list-group-item btn" style="text-align: left;" onclick="appenddToForm('file')">{{__('File')}}</li>
                                    </ul>
        
                                </div>
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2">
                                        {{__('Content')}}
                                    </div>
                                    <div class="form-box-content p-3">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>{{__('Content')}}</label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="mb-3">
                                                    <textarea class="editor" name="content">{!! Auth::user()->shop->warranty_content !!}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer text-right">
                                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

<script type="text/javascript">

    var i = 0;

    function add_customer_choice_options(em){
        var j = $(em).closest('.form-group').find('.option').val();
        var str = '<div class="form-group d-inline-block w-100">'
                        +'<div class="col-sm-6 col-sm-offset-4 pull-left">'
                            +'<input class="form-control" type="text" name="options_'+j+'[]" value="" required>'
                        +'</div>'
                        +'<div class="col-sm-2 pull-left"> <i class="la la-times" onclick="delete_choice_clearfix(this)"></i>'
                        +'</div>'
                    +'</div>'
        $(em).parent().find('.customer_choice_options_types_wrap_child').append(str);
    }
    function delete_choice_clearfix(em){
        $(em).parent().parent().remove();
    }
    function appenddToForm(type){
        //$('#form').removeClass('seller_form_border');
        if(type == 'text'){
            var str = '<div class="form-group d-inline-block w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
                            +'<input type="hidden" name="type[]" value="text">'
                            +'<div class="col-lg-3 pull-left">'
                                +'<label class="control-label">Text</label>'
                            +'</div>'
                            +'<div class="col-lg-7 pull-left">'
                                +'<input class="form-control" type="text" name="label[]" placeholder="Label">'
                            +'</div>'
                            +'<div class="col-lg-2 pull-left">'
                                +'<i class="la la-times" onclick="delete_choice_clearfix(this)"></i>'
                            +'</div>'
                        +'</div>';
            $('#form').append(str);
        }
        else if (type == 'select') {
            i++;
            var str = '<div class="form-group d-inline-block w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
                            +'<input type="hidden" name="type[]" value="select"><input type="hidden" name="option[]" class="option" value="'+i+'">'
                            +'<div class="col-lg-3 pull-left">'
                                +'<label class="control-label">Select</label>'
                            +'</div>'
                            +'<div class="col-lg-7 pull-left">'
                                +'<input class="form-control" type="text" name="label[]" placeholder="Select Label" style="margin-bottom:10px">'
                                +'<div class="customer_choice_options_types_wrap_child">'

                                +'</div>'
                                +'<button class="btn btn-success pull-right" type="button" onclick="add_customer_choice_options(this)"><i class="glyphicon glyphicon-plus"></i> Add option</button>'
                            +'</div>'
                            +'<div class="col-lg-2 pull-left">'
                                +'<i class="la la-times" onclick="delete_choice_clearfix(this)"></i>'
                            +'</div>'
                        +'</div>';
            $('#form').append(str);
        }
        else if (type == 'multi-select') {
            i++;
            var str = '<div class="form-group d-inline-block w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
                            +'<input type="hidden" name="type[]" value="multi_select"><input type="hidden" name="option[]" class="option" value="'+i+'">'
                            +'<div class="col-lg-3 pull-left">'
                                +'<label class="control-label">Multiple select</label>'
                            +'</div>'
                            +'<div class="col-lg-7 pull-left">'
                                +'<input class="form-control" type="text" name="label[]" placeholder="Multiple Select Label" style="margin-bottom:10px">'
                                +'<div class="customer_choice_options_types_wrap_child">'

                                +'</div>'
                                +'<button class="btn btn-success pull-right" type="button" onclick="add_customer_choice_options(this)"><i class="glyphicon glyphicon-plus"></i> Add option</button>'
                            +'</div>'
                            +'<div class="col-lg-2 pull-left">'
                                +'<i class="la la-times" onclick="delete_choice_clearfix(this)"></i>'
                            +'</div>'
                        +'</div>';
            $('#form').append(str);
        }
        else if (type == 'radio') {
            i++;
            var str = '<div class="form-group d-inline-block w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
                            +'<input type="hidden" name="type[]" value="radio"><input type="hidden" name="option[]" class="option" value="'+i+'">'
                            +'<div class="col-lg-3 pull-left">'
                                +'<label class="control-label">Radio</label>'
                            +'</div>'
                            +'<div class="col-lg-7 pull-left">'
                                +'<input class="form-control" type="text" name="label[]" placeholder="Radio Label" style="margin-bottom:10px">'
                                +'<div class="customer_choice_options_types_wrap_child">'

                                +'</div>'
                                +'<button class="btn btn-success pull-right" type="button" onclick="add_customer_choice_options(this)"><i class="glyphicon glyphicon-plus"></i> Add option</button>'
                            +'</div>'
                            +'<div class="col-lg-2 pull-left">'
                                +'<i class="la la-times" onclick="delete_choice_clearfix(this)"></i>'
                            +'</div>'
                        +'</div>';
            $('#form').append(str);
        }
        else if (type == 'file') {
            var str = '<div class="form-group d-inline-block w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
                            +'<input type="hidden" name="type[]" value="file">'
                            +'<div class="col-lg-3 pull-left">'
                                +'<label class="control-label">File</label>'
                            +'</div>'
                            +'<div class="col-lg-7 pull-left">'
                                +'<input class="form-control" type="text" name="label[]" placeholder="Label">'
                            +'</div>'
                            +'<div class="col-lg-2 pull-left">'
                                +'<i class="la la-times" onclick="delete_choice_clearfix(this)"></i>'
                            +'</div>'
                        +'</div>';
            $('#form').append(str);
        }
    }
</script>
@endsection

@php
	$policies_class = ' d-none d-lg-block';
@endphp