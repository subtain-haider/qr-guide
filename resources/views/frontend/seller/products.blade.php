@php
            $PackageInfo = \App\Models\User::where('package', Auth::user()->package)->first();
            $PP = Auth::user()->package_for;
            $Permissions = json_decode($PackageInfo->package_for);
        @endphp
        
        @extends('frontend.layouts.new_app_1')

@section('content')
 @php
            $PackageInfo = \App\Models\User::where('package', Auth::user()->package)->first();
            $PP = Auth::user()->package_for;
            $Permissions = json_decode($PackageInfo->package_for);
        @endphp
        <style>
            .w-5{
                width:10px!important;
            }
        </style>
    <section class="dashboard-recover-sub-links-sec" id="dashrecoversublinks">
        <div class="dashboard-recover-two-sides-left-all">
            <div class="dashboard-recover-two-sides-left-all-ul">
                <ul class="dashboard-recover-two-sides-left-links">
                <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                <!-- <li><a href="{{ route('company.index') }}">{{__('Create Mini Site')}}</a></li> -->
                <li><a href="#">{{__('Mini Sites')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('company.index') }}">{{__('Mini Sites')}}</a></li>
                        <li><a href="{{ route('company.create') }}">{{__('Create Mini Site')}}</a></li>
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                
               
                        
                <li><a href="#">{{__('Products')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('seller.products') }}">{{__('All Products and Guides')}}</a></li>
                        <li><a href="{{ route('seller.products.upload')}}">{{__('Add Product to Sell')}}</a></li>
                        @if(in_array(3,$Permissions))
                        <li><a href="{{ route('seller.guide.upload')}}">{{__('Add Products Guides')}}</a></li>
                        @endif
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                <li><a href="#">{{__('Shopify Store')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{route('shopify.index')}}">{{__('All Shopify Store')}}</a></li>
                        <li><a href="{{route('shopify.create')}}">{{__('Add Shopify Store')}}</a></li>
                        
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                <li><a href="{{route('woocommerce.index')}}">{{__('WooCommerce')}}</a></li>
                <!-- <li><a href="#">{{__('Guides')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('seller.guides') }}">{{__('Guides')}}</a></li>
                        <li><a href="{{ route('seller.guide.upload')}}">{{__('Add Guide')}}</a></li>
                      <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> 
                    </ul>
                </li> -->
                <!-- <li><a href="#">{{__('Inhouse Orders')}}</a></li> -->
                <li><a href="{{ route('total.sales') }}">{{__('Total Sales')}}</a></li>
                <li><a href="#">{{__('Saller Sales via Affiliate Links')}}</a></li>
                <li><a href="{{ route('sellercoupon') }}">{{__('Saller Coupons')}}</a></li>
                <!-- <li><a href="#">{{__('Vendors')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li> -->
                <li><a href="#">{{__('Customer')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('company_suggessions',Auth::user()->id) }}">Suggessions</a></li>
                        <li><a href="{{ route('seller.reviews',Auth::user()->id) }}">Reviews</a></li>
                         <li><a href="{{ route('clientsreport') }}">Clients Report</a></li>
                         @php
                            $domain = (explode(".",request()->getHost()));
                            $shop = App\Models\Shop::where('slug',$domain[0])->first();
                        @endphp
                        @if(in_array(15,$Permissions))
                        <li><a href="{{ route('seller_questions',Auth::user()->id) }}">Seller Questions</a></li>
                        @endif
                        @if(in_array(19,$Permissions))
                        <li><a href="{{ route('clientsreport') }}">Clients Report</a></li>
                        @endif
                    </ul>
                </li>
                <li><a href="#">{{__('Reports')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        @if(in_array(11,$Permissions))
                        <li><a href="{{ route('club_info') }}">Club Info</a></li>
                        @endif
                        @if(in_array(6,$Permissions))
                        <li><a href="{{ route('company_warranties', Auth::user()->id) }}">Warranty Request </a></li>
                        @endif
                        <li><a href="{{ route('sellingreport') }}">Selling Report</a></li>
                    </ul>
                </li>
                <!-- <li><a href="#">{{__('Pricing')}} </a></li>
                <li><a href="#">{{__('Bussines Setting')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li>
                <li><a href="#">{{__('E-commerce Setup')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li> -->
                @if(in_array(13,$Permissions))
                <li>
                    <a href="{{ route('support_ticket.index') }}" class="{{ areActiveRoutesHome(['support_ticket.index'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Support Ticket')}}
                        </span>
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{ route('marketing') }}" class="{{ areActiveRoutesHome(['marketing'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Email Marketing')}}
                        </span>
                    </a>
                </li>
                 <li class="">
                            <a class="nav-link" href="{{ route('inboxseller') }}">
                              
                                <span class="menu-title">Support Messages</span>
                            </a>
                </li>
                
                <li><a href="#">{{__('Log Activity')}}</a></li>
                
            </ul>

            </div>
        </div>
        <div class="dashboard-recover-res-sublinks-cross">
            <i class="fa fa-times" onclick="hidedashrecoversublinks()"></i>
        </div>
    </section>
    <!-- end res sub mnu -->
    <section class="dashboard-recover-burdger">
        <div class="dashboard-recover-burdger-page">
            <div class="dashboard-recover-burdger-bars">
                <i class="fa fa-bars" onclick="showdashrecoversublinks()"></i>
            </div>
        </div>
    </section>
    <section class="product-listing-page-sec">
        <div class="product-listing-page-wdth">
            <div class="product-listing-page-data">

                @include('frontend.inc.seller_side_nav_new')

                <div class="product-listing-page-right">
                    <div class="product-listing-page-right-all">
                        <div class="product-listing-page-right-all-head">
                            <h2>{{__('PRODUCTS')}}</h2>
                        </div>
                        <div class="product-listing-page-right-table-area">
                            <div class="product-listing-page-right-table-1-manage">
                                <table class="product-listing-page-right-table-1">
                                    @php 
                                    $auth_id = Auth::user()->id;
                                    @endphp
                                <tr>
                                    <th class="product-listing-page-right-table-1-btn">[{{count($products)}} {{__('PRODUCTS')}}] <a href="{{ route('seller.products.upload')}}"><button>{{__('Add New Product')}}</button></a>@if(in_array(3,$Permissions)) <a href="{{ route('seller.guide.upload') }}"><button>{{__('Add New Guide')}}</button></a>@endif</th>
                                    
                                    <th>{{__('APPROVE PRODUCTS')}}[{{App\Models\Product::where('user_id', Auth::user()->id)->where('status', 1)->count()}}]</th>
                                    <th>{{__('PENDING PRODUCTS')}}[{{App\Models\Product::where('user_id', Auth::user()->id)->where('status', 0)->count()}}]</th>
                                    <!--<th>{{__('DRAFT')}}[{{App\Models\Product::where('user_id', Auth::user()->id)->where('status', 4)->count()}}]</th>-->
                                </tr>
                                @php
                                $shopify = \App\Models\Shopify::where('user_id',$auth_id)->first();
                                @endphp
                                @if($shopify)
                                <tr>
                                    <th class="product-listing-page-right-table-1-btn pt-2"><a href="{{ url('productsync',$auth_id) }}"><button>{{__('Import Shopify Product')}}</button></a></th>
                                </tr>
                                @endif
                                @php
                                $woocommerce = \App\Models\Woocommerce::where('user_id',$auth_id)->first();
                                @endphp
                                @if($woocommerce)
                                <tr>
                                    <th class="product-listing-page-right-table-1-btn pt-2"><a href="{{ url('woocommerce-productsync',$auth_id) }}"><button>{{__('Import WooCommerce Product')}}</button></a></th>
                                </tr>
                                @endif
                                </table>
                                <div class="mob-product-3-banner-srch">
                                    <form action="{{ route('search.products') }}" id="search-form" method="GET" class="header-searchser">
                                        <input type="text" name="gsearch" id="gsearch" placeholder="Search Product" class="mob-product-3-banner-srch-bar">
                                        <i class="fa fa-search" id="search-btn"></i>
                                    </form>
                                </div>
                            </div>
                            <div class="product-listing-page-right-table-2-manage">
                                <table class="product-listing-page-right-table-2">
                                <tr>
                                    <th class="product-listing-page-right-table-2-pl">{{__('Product Name')}}<hr></th>
                                    <th>{{__('Category')}}<hr></th>
                                    <th>{{__('Published')}}<hr></th>
                                    <th class="product-listing-page-right-table-2-pr">{{__('Option')}}<hr style="width: 60px;"></th>
                                </tr>
                                @foreach ($products as $key => $product)
                                    @php
                                        $main_photo = '';
                                        if(!empty($product->photos)){
                                            $photos = json_decode($product->photos);
                                            if(isset($photos[0])){
                                                $main_photo = $photos[0];
                                            }
                                        }
                                    @endphp
                                    <tr>
                                        <td class="product-listing-page-right-table-2-img">
                                            <div>
                                                <a href="{{route('seller.products.edit', encrypt($product->id))}}">
                                                    <img src="@if($product->thumbnail_img != ''){{ asset($product->thumbnail_img) }} @elseif($product->featured_img != ''){{ asset($product->featured_img) }} @elseif($product->flash_deal_img != ''){{ asset($product->flash_deal_img) }} @elseif($main_photo != ''){{ asset($main_photo) }} @else {{ asset('img/no-image-available.png') }} @endif">
                                                </a>
                                            </div>
                                            <a href="{{ route('singleproduct',$product->slug) }}" target="_blank"><h5>{{ __($product->name) }}</h5></a>
                                        </td>
                                        <td class="product-listing-page-right-table-2-cate pr-4">{{ $product->subsubcategory->name }}</td>
                                        @if($product->published == 1)
                                            <td class="product-listing-page-right-table-2-text-1">{{__('Approved')}}</td>
                                        @elseif($product->published == 0)
                                            <td class="product-listing-page-right-table-2-text-2">{{__('Pending')}}</td>
                                        @elseif($product->published == 2)
                                            <td class="product-listing-page-right-table-2-text-3">{{__('Failed')}}</td>
                                        @endif
                                        <td>
                                            <div class="product-listing-tb-dropout">
                                                <button class="product-listing-tb-more">
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                </button>
                                                <ul class="product-listing-tb-ul">
                                                    <li>
                                                        <a href="{{route('seller.products.edit', encrypt($product->id))}}" class="dropdown-item">{{__('Edit')}}</a>
                                                    </li>
                                                    <li>
                                                        <button onclick="confirm_modal('{{route('products.destroy', $product->id)}}')" class="dropdown-item">{{__('Delete')}}</button>
                                                    </li>
                                                    <!-- <li>
                                                        <a href="{{route('products.duplicate', $product->id)}}" class="dropdown-item">{{__('Duplicate')}}</a>
                                                    </li> -->
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </table>
                            </div>
                            {{ $products->links() }}
                            

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('script')
<script type="text/javascript">
	document.querySelector('.product-listing-page-right-table-2').onclick = ({
        target
    }) => {
        if (!target.classList.contains('product-listing-tb-more')) return
        document.querySelectorAll('.product-listing-tb-dropout.active').forEach(
            (d) => d !== target.parentElement && d.classList.remove('active')
        )
        target.parentElement.classList.toggle('active')
    }
</script>
@endsection
