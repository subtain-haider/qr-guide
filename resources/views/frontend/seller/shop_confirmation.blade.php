@extends('frontend.layouts.new_app_1')



@section('content')
	@if(isset($data['coupon_active']) and $data['coupon_active'] != 0)
		<div class="modal" id="coupon_active_modal">
			<div class="modal-dialog" style="{{$data['coupon_active'] ==1 ? 'max-width: 700px;' :''}}{{$data['coupon_active'] ==2 ? 'max-width: 650px;' :''}}{{$data['coupon_active'] ==3 ? 'max-width: 500px;' :''}}">
				<div class="modal-content">
					<button type="button" class="close px-3 bg-danger border-1 pb-2 pt-1 text-white" data-dismiss="modal" style="position: absolute; top: 10px; right: 10px;z-index:1;opacity: 1;">&times;</button>
					@if($data['coupon_active'] == 1)
						@include('frontend.inc.ccoupon')
					@elseif($data['coupon_active'] == 2)
						@include('frontend.inc.cbanner')
					@elseif($data['coupon_active'] == 4)
						@include('frontend.inc.banner1')
						@elseif($data['coupon_active'] == 5)
						@include('frontend.inc.banner2')
						@elseif($data['coupon_active'] == 6)
						@include('frontend.inc.banner3')
						@elseif($data['coupon_active'] == 7)
						@include('frontend.inc.banner4')
					@else
						@include('frontend.inc.cpopup')
					@endif
				</div>
			</div>
		</div>
	@endif
    <!-- product picture -->
	@if(isset($data['product']) and App\Models\Product::where('id', $data['product'])->count() > 0)

		@php

			$product = App\Models\Product::where('id', $data['product'])->first();
            
			$main_photo = '';
			$photos = array();
			if(!$product->count()){

				$photos = json_decode($product->photos);

				$main_photo = $photos[0];

			}

		@endphp
		<?php
		if(isset($data['minibgcolor'])){
		?>
				<style type="text/css">
					.minibgcolor { background-color: <?php echo $data['minibgcolor']; ?> }
				</style>
		<?php } else { ?>
			<style type="text/css">
					.minibgcolor { background-color: #ffffff; }
				</style>
		<?php } ?>

		<section class="miniqr-product-page-sec minibgcolor">

			<div class="minipage">

				<div class="miniqr-product-page-head">

					<h2>{{ $product->name }}</h2>

				</div>

				<div class="minipro-page-pro-pic-data-slider">

					<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

						<div class="carousel-inner mini-site-slider-img-wdth">

							@for($i=0; $i < count($photos); $i++)

								<div class="carousel-item  {{ $i === 0 ? 'active' : '' }}">

									<a href="#"><img class="d-block w-100" src="{{asset($photos[$i])}}" alt="First slide"></a>

								</div>

							@endfor

						</div>

						<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">

							<i class="fa fa-angle-left minisite-slider-moveicn"></i>

							<span class="sr-only">Previous</span>

						</a>

						<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">

							<i class="fa fa-angle-right minisite-slider-moveicn"></i>

							<span class="sr-only">Next</span>

						</a>

					</div>

				</div>

				<div class="minipro-page-pro-pic-data">

					@for($i=0; $i < count($photos); $i++)

					<div class="minipro-page-pro-pic-detail-1">

						<div class="minipro-page-pro-pic-1">

							<a href="#"><img style="object-fit: contain;" src="{{asset($photos[$i])}}"></a>

						</div>

					</div>

					@endfor

				</div>

			</div>

		</section>

	@endif

	@if (isset($data['video_temp']) || (isset($data['video_link']) and !empty($data['video_link'])))

		<!-- link to product store -->

		<section class="minipro-page-store-sec">

			<div class="minipage">

				<div class="minipro-page-store-head">

					<h2>{{__('INSTRUCTION VIDEO')}}</h2>

				</div>

				<div class="minipro-page-store-data-slider">

					<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">

					<div class="carousel-inner">

						<div class="carousel-item active">

							@if ($data['video_provider'] == 'youtube' and !empty($data['video_link']))
								<?php preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $data['video_link'], $match);
							?>

								<iframe class="d-block w-100" width="100%" height="409" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $match[1] }}"></iframe>

							@elseif ($data['video_provider'] == 'dailymotion' and !empty($data['video_link']))

								<iframe class="embed-responsive-item d-block w-100" src="https://www.dailymotion.com/embed/video/{{ explode('video/', $data['video_link'])[1] }}"></iframe>

							@elseif ($data['video_provider'] == 'vimeo' and !empty($data['video_link']))

								<iframe class="d-block w-100" src="https://player.vimeo.com/video/{{ explode('vimeo.com/', $data['video_link'])[1] }}" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

							@elseif (!empty($data['video_temp']))

								<video  style="w-100">
									<source src="{{$data['video_temp']}}" type="video/mp4">
									Your browser does not support HTML video.
								</video>

							@endif

						<!-- <iframe class="d-block w-100" height="409" src="https://www.youtube.com/embed/KBo2El1bcwQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

						</div>

						<!-- <div class="carousel-item">

						<iframe class="d-block w-100" height="409" src="https://www.youtube.com/embed/ZwnXW_7fzk0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

						</div>

						<div class="carousel-item">

						<iframe class="d-block w-100" height="409" src="https://www.youtube.com/embed/yXyu7rqlpvs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

						</div> -->

					</div>

					<a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">

						<i class="fa fa-angle-left minisite-slider-moveicn-2"></i>



					</a>

					<a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">

						<i class="fa fa-angle-right minisite-slider-moveicn-2"></i>



					</a>

					</div>

				</div>

				<div class="minipro-page-store-data">

					@if ($data['video_provider'] == 'youtube' and !empty($data['video_link']))

						<iframe width="100%" height="409" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $match[1] }}"></iframe>

					@elseif ($data['video_provider'] == 'dailymotion' and !empty($data['video_link']))

						<iframe src="https://www.dailymotion.com/embed/video/{{ explode('video/', $data['video_link'])[1] }}"></iframe>

					@elseif ($data['video_provider'] == 'vimeo' and !empty($data['video_link']))

						<iframe src="https://player.vimeo.com/video/{{ explode('vimeo.com/', $data['video_link'])[1] }}" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

					@elseif (!empty($data['video_temp']))

							<video  style="w-100">
								<source src="{{$data['video_temp']}}" type="video/mp4">
								Your browser does not support HTML video.
							</video>

					@endif

				</div>

			</div>

		</section>

	@endif

	<!-- Dowmload and view -->

	@if(isset($data['pdf_temp']) and !empty($data['pdf_temp']))

		@php

			$language = false;

			if(count($data['pdf_temp']) > 1){

				$language = true;

			}

		@endphp

		<section class="minipro-page-downloads-sec">

			<div class="minidown-page">

				<div class="pro-downloads-head">

					<h2>{{ $data['pdf_title'] }}</h2>

				</div>

				<div class="minipro-page-downloads-data">

					<div class="minipro-page-downloads-text">

						<div class="minipro-page-downloads-para">

							<p>{{ $data['pdf_text'] }}</p>

						</div>

					</div>

					<div class="minipro-page-downloads-btns">

						@if($language == true)

							<div class="minipro-page-downloads-btns-detail-1">

								<form class="minipro-page-downloads-btns-detail-select" style="display: none;">

									<select name="cars" id="cars">

										<option value="slect" selected>{{__('SELECT LANGUAGE')}}</option>

										@foreach($data['lng'] as $key => $val)

											@if($val != null)

												<option value="eng">{{__('English')}}</option>

											@endif

										@endforeach

									</select>

								</form>

							</div>

						@endif

						<div class="minipro-page-downloads-btns-detail-2">
							@php
								$mainary = json_encode($data['pdf_temp'],true);
								$getpath = json_decode($mainary);

								foreach($getpath as $key => $val){



										$path = $val;




								}

							@endphp

							<div class="minipro-page-downloads-btns-detail">
								<form class="minipro-page-downloads-btns-detail-select">
								@if(sizeof($getpath) > 1)
									<select onchange="window.location = this.value;">

										<option value="slect" selected>{{__('SELECT ORIGNAL GUIDE FOR VIEW')}}</option>
										@php
										foreach($getpath as $key => $val){

											if($val != null){

												$path = $val;
												$finalpath = $path
										@endphp
										<!-- <form method="get" action=""> -->
										   <option value="<?php echo $finalpath ?>">{{__('VIEW ORIGNAL GUIDE')}}</option>
										<!-- </form> -->
										@php
											}

										}
										@endphp
									</select>
									@else
									<button type="button" value="Open Window" onclick="window.open('<?php echo $data['pdf_temp'][0] ?>')">{{__('VIEW ORIGNAL GUIDE')}}</button>
									@endif

								</form>
							</div>
							<div class="minipro-page-downloads-btns-detail">
								<form class="minipro-page-downloads-btns-detail-select">
								@if(sizeof($getpath) > 1)
									<select onchange="window.location = this.value;">

										<option value="slect" selected>{{__('VIEW TRANSLATED GUIDE')}}</option>
										@php
										foreach($getpath as $key => $val){

											if($val != null){

												$path = $val;
												$finalpath = 'http://translate.google.com/translate?sl=auto&tl=en&u='.$path
										@endphp
										<!-- <form method="get" action=""> -->
										   <option value="<?php echo $finalpath ?>">{{__('VIEW TRANSLATED GUIDE')}}</option>
										<!-- </form> -->
										@php
											}

										}
										@endphp
									</select>
									@else
									<button type="button" value="Open Window" onclick="window.open('<?php echo 'http://translate.google.com/translate?sl=en&tl=ur&u='.$data['pdf_temp'][0] ?>')">{{__('VIEW TRANSLATED GUIDE')}}</button>
									@endif

								</form>
							</div>
							<div class="minipro-page-downloads-btns-detail">
								<form class="minipro-page-downloads-btns-detail-select">
								@if(sizeof($getpath) > 1)
									<select onchange="window.location = this.value;">
										<option value="slect" selected>{{__('DOWNLAOD GUIDE')}}</option>
										@php
										foreach($getpath as $key => $val){

											if($val != null){

												$path = $val;
												$headers = array(
												  'Content-Type' => 'application/pdf',
												);
										@endphp
												<option value="{{ $path }}">{{__('DOWNLAOD GUIDE')}}</option>

										@php
											}

										}
										@endphp
									</select>
									@else

										<!-- <form method="get" action="{{ url('/public').'/'.$path }}"> -->
											<button type="button" value="Open Window" onclick="window.open('<?php echo $data['pdf_temp'][0] ?>')">{{__('DOWNLAOD GUIDE')}} </button>
										<!-- </form> -->
									@endif
								</form>
							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="minipro-page-downloads-overlay"></div>

		</section>

	@endif

	<!-- product information -->

	@if(isset($data['product']) || isset($data['question']))

		<section class="minipro-page-informa-sec minibgcolor">

			<div class="minipage">

				<div class="mininewpropriwrapper">

					<div class="mininewpropriwrapper-page">

						<div class="mininewpropritabs">

							@if(isset($data['product']) and (!empty(App\Models\Product::where('id', $data['product'])->first()->description)) and App\Models\Product::where('id', $data['product'])->count() > 0)
								<span class="mininewpropritab"><button class="mininewpropritabs-btn">{{__('PRODUCT INFORMATION')}}</button></span>
							@endif
							@if(isset($data['product']) and App\Models\Product::where('id', $data['product'])->first()->spacification)
							<span class="mininewpropritab"><button class="mininewpropritabs-btn">{{__('PRODUCT SPECIFICATION')}}</button></span>
							@endif
							<span class="mininewpropritab"><button class="mininewpropritabs-btn">{{__('COMPANY PROFILE')}}</button></span>

						</div>

						<div class="mininewtab_content">

							@if(isset($data['product']) and App\Models\Product::where('id', $data['product'])->count() > 0)
								<div class="mininewprotab_item">



									{!! App\Models\Product::where('id', $data['product'])->first()->description !!}



									<div class="minipro-page-informa-content-btns">

										<div class="minipro-page-informa-content-btn-1">

											<a href="#"  data-toggle="modal" data-target="#improveModal" >{{__('HOW CAN WE IMPROVE OUR PRODUCT')}} <i class="fa fa-angle-right"></i></a>

										</div>

										<div class="minipro-page-informa-content-btns-data">

											<div class="minipro-page-informa-content-btns-social">

												<div class="minipro-page-informa-content-btns-social-text">

													<p>{{__('SHARE THIS GUIDE')}}</p>

												</div>

												<div class="minipro-page-informa-content-btns-social-icons">

													<a href="#"><img src="{{asset('frontend/images/g2.png')}}"></a>

													<a href="#"><img src="{{asset('frontend/images/f2.png')}}"></a>

													<a href="#"><img src="{{asset('frontend/images/t.png')}}"></a>

												</div>

											</div>

											<div class="minipro-page-informa-content-btns-buy">

												<a href="#">{{__('BUY NOW')}} <i class="fa fa-shopping-cart"></i></a>

											</div>

										</div>

									</div>

								</div>

								<div class="mininewprotab_item">

									<div class="minipro-page-guides-head">

										<h2>{{__('PRODUCT SPECIFICATION')}}</h2>

									</div>

									<div class="minipro-page-guides-data">
										{!! App\Models\Product::where('id', $data['product'])->first()->spacification !!}

										<!-- <div class="minipro-page-guides-detail">

											<div class="minipro-page-guides-pics">

												<a href="#"><img src="{{asset('frontend/images/ban1.jpg')}}"></a>

											</div>

											<div class="minipro-page-guides-contents">



												<div class="minipro-page-guides-title-data">

													<div class="minipro-page-guides-title-img">

														<a href="#"><img src="{{asset('frontend/images/tes1.jpg')}}"></a>

													</div>

													<div class="minipro-page-guides-title-text">

														<a href="#">{{__('minisite title')}}</a>

													</div>

												</div>

												<div class="minipro-page-guides-title-para">

													<p>{{__('Lorem ipsum dolor sit amet, consectetur adipiscing elit.. ')}}</p>

												</div>

												<div class="minipro-page-guides-title-rating-data">

													<div class="minipro-page-guides-title-rating-detail">

														<p><span><i class="fa fa-star"></i>5.0</span> (350)</p>

													</div>

													<!-- <div class="minipro-page-guides-title-btns">

														<button class="minipro-page-guides-title-btn-1">VIEW PDF</button>

														<button class="minipro-page-guides-title-btn-2">DOWNLOAD PDF</button>

													</div> --/>

												</div>

											</div>

										</div>



										<div class="minipro-page-guides-detail">

											<div class="minipro-page-guides-pics">

												<a href="#"><img src="{{asset('frontend/images/ban2.jpg')}}"></a>

											</div>

											<div class="minipro-page-guides-contents">



												<div class="minipro-page-guides-title-data">

													<div class="minipro-page-guides-title-img">

														<a href="#"><img src="{{asset('frontend/images/tes2.jpg')}}"></a>

													</div>

													<div class="minipro-page-guides-title-text">

														<a href="#">{{__('minisite title')}}</a>

													</div>

												</div>

												<div class="minipro-page-guides-title-para">

													<p>{{__('Lorem ipsum dolor sit amet, consectetur adipiscing elit.. ')}}</p>

												</div>

												<div class="minipro-page-guides-title-rating-data">

													<div class="minipro-page-guides-title-rating-detail">

														<p><span><i class="fa fa-star"></i>5.0</span> (350)</p>

													</div>

													<!-- <div class="minipro-page-guides-title-btns">

														<button class="minipro-page-guides-title-btn-1">VIEW PDF</button>

														<button class="minipro-page-guides-title-btn-2">DOWNLOAD PDF</button>

													</div> --/>

												</div>

											</div>

										</div>



										<div class="minipro-page-guides-detail">

											<div class="minipro-page-guides-pics">

												<a href="#"><img src="{{asset('frontend/images/ban1.jpg')}}"></a>

											</div>

											<div class="minipro-page-guides-contents">



												<div class="minipro-page-guides-title-data">

													<div class="minipro-page-guides-title-img">

														<a href="#"><img src="{{asset('frontend/images/tes1.jpg')}}"></a>

													</div>

													<div class="minipro-page-guides-title-text">

														<a href="#">{{__('minisite title')}}</a>

													</div>

												</div>

												<div class="minipro-page-guides-title-para">

													<p>{{__('Lorem ipsum dolor sit amet, consectetur adipiscing elit.. ')}}</p>

												</div>

												<div class="minipro-page-guides-title-rating-data">

													<div class="minipro-page-guides-title-rating-detail">

														<p><span><i class="fa fa-star"></i>5.0</span> (350)</p>

													</div>

													<!-- <div class="minipro-page-guides-title-btns">

														<button class="minipro-page-guides-title-btn-1">VIEW PDF</button>

														<button class="minipro-page-guides-title-btn-2">DOWNLOAD PDF</button>

													</div> --/>

												</div>

											</div>

										</div> -->



									</div>
									<div class="minipro-page-informa-content-btns">

											<div class="minipro-page-informa-content-btn-1">

												<a href="#"  data-toggle="modal" data-target="#improveModal" >{{__('HOW CAN WE IMPROVE OUR PRODUCT')}} <i class="fa fa-angle-right"></i></a>

											</div>

											<div class="minipro-page-informa-content-btns-data">

												<div class="minipro-page-informa-content-btns-social">

													<div class="minipro-page-informa-content-btns-social-text">

														<p>{{__('SHARE THIS GUIDE')}}</p>

													</div>

													<div class="minipro-page-informa-content-btns-social-icons">

														<a href="#"><img src="{{asset('frontend/images/g2.png')}}"></a>

														<a href="#"><img src="{{asset('frontend/images/f2.png')}}"></a>

														<a href="#"><img src="{{asset('frontend/images/t.png')}}"></a>

													</div>

												</div>

												<div class="minipro-page-informa-content-btns-buy">

													<a href="#">{{__('BUY NOW')}} <i class="fa fa-shopping-cart"></i></a>

												</div>

											</div>

										</div>
								</div>

							@endif
							<div class="mininewprotab_item">

								<div class="minipro-page-ques-head">

									<h2>{{__('COMPANY PROFILE')}}</h2>

								</div>

								<div class="minipro-page-profile-text">
									@if(isset($data['logo_temp']))
									<img src="{{ $data['logo_temp'] }}" alt="" width="300" height="300" style="object-fit: contain;" />
									@endif
									@if(isset($data['address']))
									<p><b>Address: </b>{!! $data['address'] !!}</p>
									@endif
									@if(isset($data['whatsapp_number']))
									<p><b>Contact No: </b>{!! $data['whatsapp_number'] !!}</p>
									@endif
									@if(isset($data['company_email']))
									<p><b>Company Email: </b>{!! $data['company_email'] !!}</p>
									@endif
									@if(isset($data['company_website']))
									<p><b>Company Website: </b>{!! $data['company_website'] !!}</p>
									@endif
									@if(isset($data['description']))
									<p><b>Description: </b>{!! $data['description'] !!}</p>
									@endif

								</div>
								<div class="minipro-page-profile-content">

								@if (isset($data['company_picture_temp']) and !empty($data['company_picture_temp']))
									@foreach (json_decode($data['company_picture_temp']) as $key => $slide)
									<div class="minipro-page-profile-img">

										<!-- <img src="{{ $data['company_picture_temp'] }}"> -->
										<img src="{{ $slide }}">

									</div>
									@endforeach
								@endif
								@if (isset($data['slide']) and !empty($data['slide']))
								<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
									  <div class="carousel-inner">
									  	@foreach (($data['slide']) as $key => $slid)
									    <div class="carousel-item minipro-page-profile-img {{ $loop->first ? 'active' : '' }}">
									      <img class="d-block w-100" src="{{ asset($slid) }}" style="object-fit: cover;" alt="First slide">
									    </div>
									    @endforeach
									  </div>
									 <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">

										<i class="fa fa-angle-left minisite-slider-moveicn"></i>

										<span class="sr-only">Previous</span>

									</a>

									<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">

										<i class="fa fa-angle-right minisite-slider-moveicn"></i>

										<span class="sr-only">Next</span>

									</a>
									</div>
								@endif
							</div>

				@if( isset($data['linkedin_link']) || isset($data['whatsapp']) || isset($data['youtube_link']) || isset($data['twitter_link']) || isset($data['facebook_link']) || isset($data['instagram_link'])  || isset($data['tiktox_link']))
				<div class="minipro-page-profile-oursocial-data" style="margin-top: 40px;">

					<div class="minipro-page-profile-oursocial-detail" style="background-color: {{ $data['social_bgcolor'] }};">

						<div class="minipro-page-profile-oursocial-text">

							<p>{{__('OUR SOCIAL MEDIA PAGES')}} </p>

						</div>

						<div class="minipro-page-profile-oursocial-icns">
							@if(isset($data['linkedin_link']) and !empty($data['linkedin_link']))
							<a href="{{ $data['linkedin_link'] }}" target="_blank"><img src="{{asset('frontend/images/in.png')}}"></a>
							@endif
							@if(isset($data['whatsapp']) and !empty($data['whatsapp']))
							<a href="{{ $data['whatsapp'] }}" target="_blank"><img src="{{asset('frontend/images/wa.png')}}"></a>
							@endif
							@if(isset($data['youtube_link']) and !empty($data['youtube_link']))
							<a href="{{ $data['youtube_link'] }}" target="_blank"><img src="{{asset('frontend/images/yt.png')}}"></a>
							@endif
							@if(isset($data['twitter_link']) and !empty($data['twitter_link']))
							<a href="{{ $data['twitter_link'] }}" target="_blank"><img src="{{asset('frontend/images/tw.png')}}"></a>
							@endif
							@if(isset($data['facebook_link']) and !empty($data['facebook_link']))
							<a href="{{ $data['facebook_link'] }}" target="_blank"><img src="{{asset('frontend/images/fa.png')}}"></a>
							@endif
							@if(isset($data['instagram_link']) and !empty($data['instagram_link']))
							<a href="{{ $data['instagram_link'] }}" target="_blank"><img src="{{asset('frontend/images/ins.png')}}"></a>
							@endif
							@if(isset($data['tiktox_link']) and !empty($data['tiktox_link']))
							<a href="{{ $data['tiktox_link'] }}" target="_blank"><img src="{{asset('frontend/images/tk.png')}}"></a>
							@endif
						</div>

					</div>

				</div>
				@endif
							</div>

						</div>

					</div>

				</div>

			</div>

		</section>

	@endif

	<!-- company profile -->

	<section class="minipro-page-profile-sec minibgcolor" >

		<div class="minipage">

			<div class="minipro-page-profile-sec-res">

				<!-- <div class="minipro-profile-head">


				</div> -->



				<!-- <div class="minipro-page-profile-oursocialpro-imgs-data">



					<div class="minipro-page-profile-oursocialpro-imgs-detail">

						<div class="minipro-page-profile-oursocialpro-img-1">

							<img src="{{asset('frontend/images/bans1.jpg')}}"></a>

						</div>

					</div>



					<div class="minipro-page-profile-oursocialpro-imgs-detail">

						<div class="minipro-page-profile-oursocialpro-img-1">

							<a href="#"><img src="{{asset('frontend/images/bans2.jpg')}}"></a>

						</div>

					</div>



					<div class="minipro-page-profile-oursocialpro-imgs-detail">

						<div class="minipro-page-profile-oursocialpro-img-1">

							<a href="#"><img src="{{asset('frontend/images/bans3.jpg')}}"></a>

						</div>

					</div>



				</div> -->

			</div>

			<div class="minipro-page-profile-wranty-data">
				@if((isset($data['warranty_text']) and !empty($data['warranty_text'])) || (isset($data['warranty']) and !empty($data['warranty'])) || (isset($data['warranty_content']) and !empty($data['warranty_content'])))
					<div class="minipro-page-profile-wranty-detail-1">

						<div class="minipro-page-profile-wranty-icon-data">

							<div class="minipro-page-profile-wranty-icon">

								<i class="fa fa-check-square-o"></i>

							</div>


						
							<div class="minipro-page-profile-wranty-text">

								<h3><a href=""> {{ $data['warranty_text'] }}</h3></a>

							</div>

						</div>

					</div>
				@endif
				@if(isset($data['club_text']) and !empty($data['club_text']))

					<div class="minipro-page-profile-wranty-detail-2">

						<div class="minipro-page-profile-wranty-icon-data">

							<div class="minipro-page-profile-wranty-icon">

								<i class="fa fa-users"></i>

							</div>
						
							<div class="minipro-page-profile-wranty-text">

								<h3><a href="" > {{ $data['club_text'] }}</a></h3>

							</div>

						</div>

					</div>

				@endif

			</div>

		</div>

	</section>

	<!-- coustomer reviews -->
	@if(isset($data['reviews']) and $data['reviews'] == 'true')

	<section class="minipro-page-reviews-sec minibgcolor">
		<div class="minipage">
			<div class="feednewpropriwrapper">
		    	<div class="feednewpropriwrapper-page">
				    <div class="feednewpropritabs">
				        <span class="feednewpropritab"><button class="feednewpropritabs-btn">CUSTOMER FEEDBACK</button></span>
				        <span class="feednewpropritab"><button class="feednewpropritabs-btn">CUSTOMER Q.A</button></span>
				    </div>
				    <div class="feednewtab_content">

				        <div class="feednewprotab_item">

							<div class="minipro-page-reviews-data">

								<div class="minipro-page-reviews-detail-1">

									<div class="minipro-page-reviews-content">

										<div class="minipro-page-reviews-content-head">

											<h3>{{__('Customer reviews')}}</h3>

										</div>

										<div class="minipro-page-reviews-ls-stars-data">

											<div class="pro-page-reviews-ls-stars">

												<i class="fa fa-star"></i>

												<i class="fa fa-star"></i>

												<i class="fa fa-star"></i>

												<i class="fa fa-star"></i>

												<i class="fa fa-star-half-o"></i>

											</div>

											<div class="minipro-page-reviews-ls-stars-text">

												<p>4.8 out of 5</p>

											</div>

										</div>

										<div class="minipro-page-reviews-global">

											<p>8,895 {{__('global ratings')}}</p>

										</div>

										<!-- 1rating -->

										<div class="minipro-page-ratings-data">

											<div class="pro-page-ratings-detail-1">

												<p>5 star</p>

											</div>

											<div class="minipro-page-ratings-detail-2">

												<div class="minipro-page-ratings-high-1">

												</div>

												<div class="minipro-page-ratings-low-1">

												</div>

											</div>

											<div class="minipro-page-ratings-detail-3">

												<p>86%</p>

											</div>

										</div>

										<!-- 2rating -->

										<div class="minipro-page-ratings-data">

											<div class="minipro-page-ratings-detail-1">

												<p>4 star</p>

											</div>

											<div class="minipro-page-ratings-detail-2">

												<div class="minipro-page-ratings-high-2">

												</div>

												<div class="minipro-page-ratings-low-2">

												</div>

											</div>

											<div class="minipro-page-ratings-detail-3">

												<p>9%</p>

											</div>

										</div>

										<!-- 3rating -->

										<div class="minipro-page-ratings-data">

											<div class="minipro-page-ratings-detail-1">

												<p>3 star</p>

											</div>

											<div class="minipro-page-ratings-detail-2">

												<div class="minipro-page-ratings-high-3">

												</div>

												<div class="minipro-page-ratings-low-3">

												</div>

											</div>

											<div class="minipro-page-ratings-detail-3">

												<p>3%</p>

											</div>

										</div>

										<!-- 4rating -->

										<div class="minipro-page-ratings-data">

											<div class="minipro-page-ratings-detail-1">

												<p>2 star</p>

											</div>

											<div class="minipro-page-ratings-detail-2">

												<div class="minipro-page-ratings-high-4">

												</div>

												<div class="minipro-page-ratings-low-4">

												</div>

											</div>

											<div class="minipro-page-ratings-detail-3">

												<p>1%</p>

											</div>

										</div>

										<!-- 5rating -->

										<div class="minipro-page-ratings-data">

											<div class="minipro-page-ratings-detail-1">

												<p>1 star</p>

											</div>

											<div class="minipro-page-ratings-detail-2">

												<div class="minipro-page-ratings-high-5">

												</div>

												<div class="minipro-page-ratings-low-5">

												</div>

											</div>

											<div class="minipro-page-ratings-detail-3">

												<p>1%</p>

											</div>

										</div>

										<!-- <div class="minipro-page-ratings-calculate">

											<p><i class="fa fa-angle-down"></i>{{__('How are ratings calculated?')}}</p>

										</div> -->



									</div>

								</div>

								<div class="minipro-page-reviews-detail-2">

									<!-- coment-1 -->

									<div class="minipro-page-reviews-coments-data">

										<div class="minipro-page-reviews-coments-detail">

											<div class="minipro-page-reviews-coments-img">

												<img src="images/tes1.jpg">

											</div>

											<div class="minipro-page-reviews-coments-img-text">

												<div class="minipro-page-reviews-client-data">

													<div class="minipro-page-reviews-client-name">

														<h4>John Deo</h4>

													</div>

													<div class="minipro-page-reviews-client-stars">

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star-half-o"></i>

													</div>



												</div>

												<div class="minipro-page-reviews-client-msg">

													<p>{{__('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin blandit libero quis lacus luctus ultricies.')}}</p>

												</div>

											</div>

										</div>



									</div>



									<!-- coment-1 -->

									<div class="minipro-page-reviews-coments-data">

										<div class="minipro-page-reviews-coments-detail">

											<div class="minipro-page-reviews-coments-img">

												<img src="{{asset('frontend/images/tes1.jpg')}}">

											</div>

											<div class="minipro-page-reviews-coments-img-text">

												<div class="minipro-page-reviews-client-data">

													<div class="minipro-page-reviews-client-name">

														<h4>John Deo</h4>

													</div>

													<div class="minipro-page-reviews-client-stars">

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star"></i>

														<i class="fa fa-star-half-o"></i>

													</div>



												</div>

												<div class="minipro-page-reviews-client-msg">

													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin blandit libero quis lacus luctus ultricies.</p>

												</div>

											</div>

										</div>



									</div>



								</div>

							</div>
				        </div>
				        <div class="feednewprotab_item">
							@if(isset($data['question']) and !empty($data['question']))

								<div class="mininewprotab_item">

									<div class="minipro-page-ques-head">

										<h2>{{__('CUSTOMER QUESTION AND ANSWERS')}}</h2>

									</div>

									<!-- <div class="minipro-page-ques-input">

										<form class="minipro-page-ques-form">

											<button type="submit"><i class="fa fa-search"></i></button>

											<input type="text" placeholder="Have aquestion? Search for answers" name="search">

										</form>

									</div> -->
									<div class="minipro-page-ques-head">

										<h4>{{__('SUBMIT YOUR QUESTION')}}</h4>

									</div>

									<div class="minipro-page-ques-input">

										<form class="form-inline" method="POST" action="{{route('company.question.submit')}}">

											{{ csrf_field() }}
										

										
										
											<input class="form-control" type="hidden" value="{{$data['title']}}" name="slug">
											
											<input class="form-control col-md-10" style="margin-bottom: 0px;" type="text" placeholder="Have aquestion? " name="question">

											<button type="submit" name="save-question" class="btn btn-success col-md-2" id="save-question">Submit</button>

										</form>

									</div>
									<!-- faq 1 -->
									@foreach($data['question'] as $key => $question)
									<div class="minipro-page-ques-data">
										<div class="minipro-page-ques-datail-1">
											<div class="minipro-page-votes-icon">
												<i class="fa fa-caret-up"></i>
											</div>
											<div class="minipro-page-votes-icon-text">
												<p>8</p>
												<p>votes</p>
											</div>
											<div class="minipro-page-votes-icon">
												<i class="fa fa-caret-down"></i>
											</div>
										</div>
										<div class="minipro-page-ques-datail-2">
											<div class="minipro-page-ques-text-data">
												<div class="minipro-page-ques-text-h-1">
													<h3>Question:</h3>
												</div>
												<div class="minipro-page-ques-text-h-2">
													<h3> {{ $question }}</h3>
												</div>
											</div>
											<div class="minipro-page-ques-ans-data">
												<div class="minipro-page-ans-text-h-1">
													<h3>Answer:</h3>
												</div>


												<div class="minipro-page-ans-ans-h-2">
													<p>{{ $data['answer'][$key] }}</p>
													<div class="minipro-page-ans-analys">
														<p>By analyst on September04, 2021</p>
													</div>
													<!-- <div class="minipro-page-ans-see-more">
														<p><i class="fa fa-angle-down"></i>See more answers[7]</p>
													</div> -->
												</div>
											</div>

										</div>
									</div>

									@endforeach

								</div>
							@endif
				        </div>

				    </div>
				</div>
			</div>
		</div>
	</section>

	@endif

	@if(isset($data['related_products']) and !empty($data['related_products']))

		<section class="minipro-page-related-sec minibgcolor">

			<div class="minipage">

				<div class="minipro-page-related-head">

					<h2>{{__('TRY OUR ADDITIONAL ITEMS')}}</h2>

				</div>

				<div class="minipro-page-related-hr">

					<hr>

				</div>

				<div class="minipro-page-related-data">

					@foreach(explode(",",$data['related_products'][0]) as $key => $val)

						@if(App\Models\Product::where('id', $val)->count() > 0)

							@php

								$product = App\Models\Product::where('id', $val)->first();

								$main_photo = '';

								if(!empty($product->photos)){

									$photos = json_decode($product->photos);

									$main_photo = $photos[0];

								}

							@endphp

							<div class="minipro-page-related-detail">

								<div class="minipro-page-related-pics">

									<a href="{{ route('singleproduct',$product->slug) }}"><img style="object-fit: contain;" src="{{asset($main_photo)}}"></a>

								</div>

								<div class="minipro-page-related-pics-text">

									<a href="{{ route('singleproduct',$product->slug) }}"><h4>{{$product->name}}</h4></a>

								</div>

							</div>

						@endif

					@endforeach

				</div>

			</div>

		</section>

	@endif
<!-- Modal improve -->
<div class="modal fade" id="improveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">HOW CAN WE IMPROVE OUR PRODUCT</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('company.commentsubmit') }}" method="POST">
      <div class="modal-body">

        	{{ csrf_field() }}
			<input class="form-control" type="hidden" value="1" name="shop_id">
			  <div class="form-group">
			    <input type="email" class="form-control" name="email" id="exampleFormControlInput1" placeholder="name@example.com">
			  </div>
			  <div class="form-group">
			    <textarea name="comment" rows="4" cols="50" style="width: 100%;height: 200px;border: 1px solid #e8e8e8;" placeholder="HOW CAN WE IMPROVE OUR PRODUCT..."></textarea>
			  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection



@section('script')

<script type="text/javascript">

	@if(isset($data['coupon_active']) != 0)
		$(window).on('load', function() {
			$('#coupon_active_modal').modal('show');
		});
	@endif

    $(function () {

		// tabs

		$(".mininewpropriwrapper .mininewpropritab").click(function() {

		$(".mininewpropriwrapper .mininewtabpropri").removeClass("active").eq($(this).index()).addClass("mininewpropractive");

		$(".mininewprotab_item").hide().eq($(this).index()).fadeIn()

		}).eq(0).addClass("active");

	});



	function myFunction() {

		var miniinfodots = document.getElementById("miniinfodots");

		var miniinfomoreText = document.getElementById("miniinfomore");

		var miniinfobtnText = document.getElementById("miniinfomyBtn");



		if (miniinfodots.style.display === "none") {

			miniinfodots.style.display = "inline";

			miniinfobtnText.innerHTML = "Read more";

			miniinfomoreText.style.display = "none";

		} else {

			miniinfodots.style.display = "none";

			miniinfobtnText.innerHTML = "Read less";

			miniinfomoreText.style.display = "inline";

		}

	}
	$('.mininewpropritabs-btn').on('click', function(){
     $('.mininewpropritabs-btn').removeClass('active');
     $('.mininewpropritabs .mininewpropritab').removeClass('active');
     $(this).addClass('active');
   });
	$(function () {
		// customer feedback tabs

	    $(".feednewpropriwrapper .feednewpropritab").click(function() {
		$(".feednewpropriwrapper .feednewtabpropri").removeClass("active").eq($(this).index()).addClass("feednewpropractive");
		$(".feednewprotab_item").hide().eq($(this).index()).fadeIn()
		}).eq(0).addClass("active");

	});
</script>


@endsection
