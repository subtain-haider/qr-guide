@extends('frontend.layouts.new_app_1')

@section('content')


<style type="text/css">
	.img-upload-preview img {width: 100%;}


	/*-- --------------------------------------single-product-page---------------------------------- */
.single-pro-page-sec{
	display: flex;
	justify-content: center;
	padding: 30px 0px;
	overflow: hidden;
}
.single-pro-page-wdth{
	width: 90%;
}
.single-pro-ban-data{
	display: flex;
	justify-content: space-between;
}
.single-pro-ban-detail-1{
	width: 34%;
}
.single-pro-ban-detail-2{
	width: 64%;
}
.single-pro-ban-detail-1 img{
	width: 100%;
}
input[type="number"] {
  -webkit-appearance: textfield;
    -moz-appearance: textfield;
          appearance: textfield;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none;
}

.number-input button {
  -webkit-appearance: none;
  background-color: transparent;
  border: none;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  margin: 0;
  position: relative;
}
.number-input button:before,
.number-input button:after {
  display: inline-block;
  position: absolute;
  content: '';
  height: 2px;
  transform: translate(-50%, -50%);
  margin-top: -6px;
}
.number-input button.plus:after {
  transform: translate(-50%, -50%) rotate(90deg);
}
.number-input input[type=number] {
  text-align: center;
}


.md-number-input.number-input {
 /* border: 2px solid #ddd;*/
  width: 9rem;
}
.md-number-input.number-input button {
  outline: none;
  width: 2rem;
  height: 2rem;
  padding-top: .8rem;
  background-color: rgba(0,0,0,0.15);
}

.md-number-input.number-input button.plus {
  padding-left: 2px;
}
.md-number-input.number-input button:before,
.md-number-input.number-input button:after {
  width: 1rem;
  background-color: #212121;
}
.md-number-input.number-input input[type=number] {
  max-width: 4rem;
  border:   solid #ddd;
  border-width: 0 2px;
  font-size: 18px;
  height: 2rem;
  outline: none;
}
@media not all and (min-resolution:.001dpcm)
{ @supports (-webkit-appearance:none) and (stroke-color:transparent) {
  .number-input.md-number-input.safari_only button:before, 
  .number-input.md-number-input.safari_only button:after {
    margin-top: -.6rem;
  }
}}

.single-pro-ban-head h2{
	margin-bottom: 2px;
	font-size: 28px;
	font-family: 'Play';
	font-weight: 600;
	
}
.single-pro-ban-icon{
	display: flex;
	flex-direction: row;
	justify-content: flex-end;
	margin-top: 30px;
}
.single-pro-ban-icon i{
	margin-left: 20px;
	color: #777;
	font-size: 20px;
	cursor: pointer;
}
.single-pro-ban-quant{
	display: flex;
	align-items: center;
	margin-top: 30px;
}
.single-pro-ban-des-hed h5{
	margin-bottom: 0px;
	font-size: 20px;
	font-family: 'Play';
}
.single-pro-ban-color{
	display: flex;
	align-items: center;
	margin-top: 20px;
}
.single-pro-ban-des-hed{
	width: 20%;
}
.single-pro-ban-des-hed-2{
	width: 17%;
}
.single-pro-ban-color-detail button{
	width: 40px;
	height: 40px;
	border: none;
}

.single-pro-ban-color-black{
	background-color: #000;
}
.single-pro-ban-color-red{
	background-color: #FF0000;
}
.single-pro-ban-color-sky{
	background-color: #198EA2;
}
.single-pro-ban-size{
	display: flex;
	align-items: center;
	margin-top: 20px;
}
.single-pro-ban-size-detail input{
	
	border: none;
}
.single-pro-ban-size-detail label{
	margin-right:10px;
	margin-left:20px;
}
.sngl-pr-clr{
	display:flex;
	align-items:center;
	flex-wrap:wrap;
}
.sngl-pr-clr input{
	margin-bottom: 0px;
	margin-left:20px;
}
.clr-sec-pro-frm{
	display:flex;
	align-itmes:center;
}
.clr-sec-pro-frm-wdth{
	width:20%;
}
.clr-sec-pro-frm-wdth h5{
	margin-bottom: 0px;
    font-size: 20px;
    font-family: 'Play';
}
.clr-sec-pro-frm-wdth-2{
	width:80%;
}
.form-group{
	/*margin-bottom:0px!important;*/
}
</style>
    <section class="addpro-page-form-sec">

		<div class="addpropage">

			<div class="addpro-page-form-sec-head">
                @if($product->product_type == 'guide')
				<h2>{{__('Product Guide Information')}}</h2>
				@else
				<h2>{{__('Product Information')}}</h2>
				@endif

			</div>

			<div class="addnewpropriwrapper">

		    	<div class="addnewpropriwrapper-data">

		    		<div class="addnewpropriwrapper-detail-1">

					    <div class="addnewpropritabs">

					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('General')}}</button></span>

					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Images')}}</button></span>

					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Videos')}}</button></span>
					        @if($product->product_type == 'guide')
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Meta Tags')}}</button></span>
					        @endif
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Price')}}</button></span>

					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('External link')}}</button></span>

					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Description')}}</button></span>
                            
							<span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Spacification')}}</button></span>
							@if($product->product_type != 'guide')
							<span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Variations')}}</button></span>
							@endif
							@if($product->product_type == 'guide')
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('PDF Specification')}}</button></span> 
					        @endif       

					    </div>

					</div>

					<div class="addnewpropriwrapper-detail-2">

					    <div class="addnewtab_content">

							<form class="" action="{{route('products.update', $product->id)}}" method="POST" enctype="multipart/form-data" id="choice_form">

								<!-- General-->

								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">

										<div class="addnewprotab_item-sec-page">

											<div class="addpro-page-form-general-data">

												@csrf

												<!-- <input type="hidden" name="added_by" value="seller"> -->

												<div class="form-group addpro-page-form-general-data-all">

													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Product Name')}}</label>

													<div class="addpro-page-form-img-general-2">

													    <input type="text" class="form-control" name="name" required placeholder="{{__('Product Name')}}" value="{{ __($product->name) }}">

													</div>

												</div>
                                                <div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('SKU')}}</label>
													<div class="addpro-page-form-img-general-2">
													<input type="text" class="form-control" name="sku" required placeholder="{{__('SKU')}}" value="{{ __($product->sku) }}">
													</div>
												</div>
                                                <div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('ASIN')}}</label>
													<div class="addpro-page-form-img-general-2">
													<input type="text" class="form-control" name="asin" required placeholder="{{__('ASIN')}}" value="{{ __($product->asin) }}">
													</div>
												</div>
												<div class="form-group addpro-page-form-general-data-all">

													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Category')}}</label>

													<div class="addpro-page-form-img-general-2">

													<select class="form-control demo-select2 demo-select2-placeholder" name="category_id" id="category_id" required>

														<option value="">{{__('Please Select')}}</option>

														@foreach($categories as $category)

															<option value="{{$category->id}}"@if($product->category_id == $category->id) selected @endif>{{__($category->name)}}</option>

														@endforeach

													</select>

													</div>

												</div>

												<div class="form-group addpro-page-form-general-data-all">

													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Subcategory')}}</label>

													<div class="addpro-page-form-img-general-2">

														<select class="form-control demo-select2 demo-select2-placeholder" name="subcategory_id" id="subcategory_id" data-live-search="true" required>

                                                            @foreach(App\Models\SubCategory::where('category_id', $product->category_id)->get() as $subcategory)

                                                                <option value="{{$subcategory->id}}"@if($product->subcategory_id == $subcategory->id) selected @endif>{{__($subcategory->name)}}</option>

                                                            @endforeach

                                                        </select>

													</div>

												</div>

												<div class="form-group addpro-page-form-general-data-all">

													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Sub Subcategory')}}</label>

													<div class="addpro-page-form-img-general-2">

														<select class="form-control demo-select2 demo-select2-placeholder" name="subsubcategory_id" id="subsubcategory_id" data-live-search="true" required>

                                                            @foreach(App\Models\SubSubCategory::where('sub_category_id', $product->subcategory_id)->get() as $subsubcategory)

                                                                <option value="{{$subsubcategory->id}}"@if($product->subsubcategory_id == $subsubcategory->id) selected @endif>{{__($subsubcategory->name)}}</option>

                                                            @endforeach

                                                        </select>

													</div>

												</div>

												<div class="form-group addpro-page-form-general-data-all">

													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Unit')}}</label>

													<div class="addpro-page-form-img-general-2">
                                                        <select class="form-control" name="unit" required>
													        <option disable>Select Unit (e.g. KG, Pc etc)</option>
													        <option value='kg' {{ ($product->unit == 'kg') ? 'selected' : '' }}>KG</option>
													        <option value='pc' {{ ($product->unit == 'pc') ? 'selected' : '' }}>PC</option>
													    </select>
													    
													</div>

												</div>

												<div class="form-group addpro-page-form-general-data-all">

													<label for="" class="form-control-label addpro-page-form-general-detail-1">Tags</label>

													<div class="addpro-page-form-img-general-2">

													    <input type="text" class="form-control" name="tags[]" value="tag" placeholder="Type & hit enter" data-role="tagsinput" required placeholder="Type to add a tag" value="{{ $product->tags }}">

													</div>

												</div>

											</div>

										</div>

									</div>

								</div>

								<!-- Images -->

								<div class="addnewprotab_item">

									

									<div class="addnewprotab_item-sec">

										<div class="addnewprotab_item-sec-page">

											<div class="addpro-page-form-img-data-1-head">

												<p>Main Images</p>

											</div>

											<div class="addpro-page-form-img-data-1">

												

												<div class="addpro-page-form-img-data-1-detail-all">



													<div id="photos">

                                                        @if(is_array(json_decode($product->photos)))

                                                            @foreach (json_decode($product->photos) as $key => $photo)

                                                                <div class="col-md-4 col-sm-4 col-xs-6">

                                                                    <div class="img-upload-preview">

                                                                        <img style="width: 100%" src="{{ asset($photo) }}" alt="" class="img-responsive">

                                                                        <input type="hidden" name="previous_photos[]" value="{{ $photo }}">

                                                                        <button type="button" class="btn btn-danger close-btn remove-files"><i class="fa fa-times"></i></button>

                                                                    </div>

                                                                </div>

                                                            @endforeach

                                                        @endif

                                                    </div>



												</div>

											</div>

											<!-- thumbnail -->

											<div class="addpro-page-form-img-data-2">

												<div class="addpro-page-form-img-data-1-head">

													<p>Thumnnail Image(290X300)</p>

												</div>

												<div class="addpro-page-form-img-data-2-detail-all">



													<div id="thumbnail_img">

                                                        @if ($product->thumbnail_img != null)

                                                            <div class="col-md-4 col-sm-4 col-xs-6">

                                                                <div class="img-upload-preview">

                                                                    <img src="{{ asset($product->thumbnail_img) }}" alt="" class="img-responsive">

                                                                    <input type="hidden" name="previous_thumbnail_img" value="{{ $product->thumbnail_img }}">

                                                                    <button type="button" class="btn btn-danger close-btn remove-files"><i class="fa fa-times"></i></button>

                                                                </div>

                                                            </div>

                                                        @endif

                                                    </div>



												</div>

											</div>

										</div>

									</div>

									

								</div>

								<!-- Videos -->

								<div class="addnewprotab_item">



									<div class="addnewprotab_item-sec">

										<div class="addnewprotab_item-sec-page">

											<div class="form-group addpro-page-form-img-data">

												<label for="" class="form-control-label addpro-page-form-img-detail-1">{{__('Video Provider')}}</label>

												<div class="addpro-page-form-img-detail-2">

												<select class="form-control demo-select2-placeholder" id="select-country" data-live-search="true" name="video_provider">

                                                    <option value="youtube" <?php if($product->video_provider == 'youtube') echo "selected";?> >{{__('Youtube')}}</option>

                                                    <option value="dailymotion" <?php if($product->video_provider == 'dailymotion') echo "selected";?> >{{__('Dailymotion')}}</option>

                                                    <option value="vimeo" <?php if($product->video_provider == 'vimeo') echo "selected";?> >{{__('Vimeo')}}</option>

												</select>



												</div>

											</div>

											<div class="form-group addpro-page-form-img-data">

												<label for="" class="form-control-label addpro-page-form-img-detail-1">{{__('Video URL')}}</label>

												<div class="addpro-page-form-img-detail-2">

												    <input type="text" class="form-control" name="video_link" value="{{ $product->video_link }}" placeholder="Video Link">

												</div>

											</div>

										</div>

									</div>

								

								</div>

								<!-- Meta Tags -->
								@if($product->product_type == 'guide')
								<div class="addnewprotab_item">



									<div class="addnewprotab_item-sec">

										<div class="addnewprotab_item-sec-page">

											<div class="form-group addpro-page-form-meta-data-all">

												<label for="" class="form-control-label addpro-page-form-meta-detail-1">{{__('Meta Title')}}</label>

												<div class="addpro-page-form-meta-detail-2">

												<input type="text" name="meta_title" required placeholder="Meta Title" value="{{ $product->meta_title }}">

												</div>

											</div>

											<div class="form-group addpro-page-form-meta-data-all">

												<label for="" class="form-control-label addpro-page-form-meta-detail-1">{{__('Description')}}</label>

												<div class="addpro-page-form-meta-detail-2">

												    <textarea name="meta_description" rows="8" class="form-control mb-3">{{ $product->meta_description }}</textarea>

												</div>

											</div>

											<div class="addpro-page-form-meta-data-2">

												<div class="addpro-page-form-meta-data-1-head">

													<p>Meta Image</p>

												</div>

												<div class="addpro-page-form-meta-data-2-detail-all">



													<div id="meta_photo">

                                                        @if ($product->meta_img != null)

                                                            <div class="col-md-4 col-sm-4 col-xs-6">

                                                                <div class="img-upload-preview">

                                                                    <img src="{{ asset($product->meta_img) }}" alt="" class="img-responsive">

                                                                    <input type="hidden" name="previous_meta_img" value="{{ $product->meta_img }}">

                                                                    <button type="button" class="btn btn-danger close-btn remove-files"><i class="fa fa-times"></i></button>

                                                                </div>

                                                            </div>

                                                        @endif

                                                    </div>



												</div>

											</div>

										</div>

									</div>

								

								</div>
								@endif
								<!--Price-->

								<div class="addnewprotab_item">
									@php
									$currency_symbols = array(
											'AED' => '&#1583;.&#1573;', // ?
											'AFN' => '&#65;&#102;',
											'ALL' => '&#76;&#101;&#107;',
											'ANG' => '&#402;',
											'AOA' => '&#75;&#122;', // ?
											'ARS' => '&#36;',
											'AUD' => '&#36;',
											'AWG' => '&#402;',
											'AZN' => '&#1084;&#1072;&#1085;',
											'BAM' => '&#75;&#77;',
											'BBD' => '&#36;',
											'BDT' => '&#2547;', // ?
											'BGN' => '&#1083;&#1074;',
											'BHD' => '.&#1583;.&#1576;', // ?
											'BIF' => '&#70;&#66;&#117;', // ?
											'BMD' => '&#36;',
											'BND' => '&#36;',
											'BOB' => '&#36;&#98;',
											'BRL' => '&#82;&#36;',
											'BSD' => '&#36;',
											'BTN' => '&#78;&#117;&#46;', // ?
											'BWP' => '&#80;',
											'BYR' => '&#112;&#46;',
											'BZD' => '&#66;&#90;&#36;',
											'CAD' => '&#36;',
											'CDF' => '&#70;&#67;',
											'CHF' => '&#67;&#72;&#70;',
											'CLP' => '&#36;',
											'CNY' => '&#165;',
											'COP' => '&#36;',
											'CRC' => '&#8353;',
											'CUP' => '&#8396;',
											'CVE' => '&#36;', // ?
											'CZK' => '&#75;&#269;',
											'DJF' => '&#70;&#100;&#106;', // ?
											'DKK' => '&#107;&#114;',
											'DOP' => '&#82;&#68;&#36;',
											'DZD' => '&#1583;&#1580;', // ?
											'EGP' => '&#163;',
											'ETB' => '&#66;&#114;',
											'EUR' => '&#8364;',
											'FJD' => '&#36;',
											'FKP' => '&#163;',
											'GBP' => '&#163;',
											'GEL' => '&#4314;', // ?
											'GHS' => '&#162;',
											'GIP' => '&#163;',
											'GMD' => '&#68;', // ?
											'GNF' => '&#70;&#71;', // ?
											'GTQ' => '&#81;',
											'GYD' => '&#36;',
											'HKD' => '&#36;',
											'HNL' => '&#76;',
											'HRK' => '&#107;&#110;',
											'HTG' => '&#71;', // ?
											'HUF' => '&#70;&#116;',
											'IDR' => '&#82;&#112;',
											'ILS' => '&#8362;',
											'INR' => '&#8377;',
											'IQD' => '&#1593;.&#1583;', // ?
											'IRR' => '&#65020;',
											'ISK' => '&#107;&#114;',
											'JEP' => '&#163;',
											'JMD' => '&#74;&#36;',
											'JOD' => '&#74;&#68;', // ?
											'JPY' => '&#165;',
											'KES' => '&#75;&#83;&#104;', // ?
											'KGS' => '&#1083;&#1074;',
											'KHR' => '&#6107;',
											'KMF' => '&#67;&#70;', // ?
											'KPW' => '&#8361;',
											'KRW' => '&#8361;',
											'KWD' => '&#1583;.&#1603;', // ?
											'KYD' => '&#36;',
											'KZT' => '&#1083;&#1074;',
											'LAK' => '&#8365;',
											'LBP' => '&#163;',
											'LKR' => '&#8360;',
											'LRD' => '&#36;',
											'LSL' => '&#76;', // ?
											'LTL' => '&#76;&#116;',
											'LVL' => '&#76;&#115;',
											'LYD' => '&#1604;.&#1583;', // ?
											'MAD' => '&#1583;.&#1605;.', //?
											'MDL' => '&#76;',
											'MGA' => '&#65;&#114;', // ?
											'MKD' => '&#1076;&#1077;&#1085;',
											'MMK' => '&#75;',
											'MNT' => '&#8366;',
											'MOP' => '&#77;&#79;&#80;&#36;', // ?
											'MRO' => '&#85;&#77;', // ?
											'MUR' => '&#8360;', // ?
											'MVR' => '.&#1923;', // ?
											'MWK' => '&#77;&#75;',
											'MXN' => '&#36;',
											'MYR' => '&#82;&#77;',
											'MZN' => '&#77;&#84;',
											'NAD' => '&#36;',
											'NGN' => '&#8358;',
											'NIO' => '&#67;&#36;',
											'NOK' => '&#107;&#114;',
											'NPR' => '&#8360;',
											'NZD' => '&#36;',
											'OMR' => '&#65020;',
											'PAB' => '&#66;&#47;&#46;',
											'PEN' => '&#83;&#47;&#46;',
											'PGK' => '&#75;', // ?
											'PHP' => '&#8369;',
											'PKR' => '&#8360;',
											'PLN' => '&#122;&#322;',
											'PYG' => '&#71;&#115;',
											'QAR' => '&#65020;',
											'RON' => '&#108;&#101;&#105;',
											'RSD' => '&#1044;&#1080;&#1085;&#46;',
											'RUB' => '&#1088;&#1091;&#1073;',
											'RWF' => '&#1585;.&#1587;',
											'SAR' => '&#65020;',
											'SBD' => '&#36;',
											'SCR' => '&#8360;',
											'SDG' => '&#163;', // ?
											'SEK' => '&#107;&#114;',
											'SGD' => '&#36;',
											'SHP' => '&#163;',
											'SLL' => '&#76;&#101;', // ?
											'SOS' => '&#83;',
											'SRD' => '&#36;',
											'STD' => '&#68;&#98;', 
											'SVC' => '&#36;',
											'SYP' => '&#163;',
											'SZL' => '&#76;', 
											'THB' => '&#3647;',
											'TJS' => '&#84;&#74;&#83;',
											'TMT' => '&#109;',
											'TND' => '&#1583;.&#1578;',
											'TOP' => '&#84;&#36;',
											'TRY' => '&#8356;',
											'TTD' => '&#36;',
											'TWD' => '&#78;&#84;&#36;',
											'UAH' => '&#8372;',
											'UGX' => '&#85;&#83;&#104;',
											'USD' => '&#36;',
											'UYU' => '&#36;&#85;',
											'UZS' => '&#1083;&#1074;',
											'VEF' => '&#66;&#115;',
											'VND' => '&#8363;',
											'VUV' => '&#86;&#84;',
											'WST' => '&#87;&#83;&#36;',
											'XAF' => '&#70;&#67;&#70;&#65;',
											'XCD' => '&#36;',
											'XPF' => '&#70;',
											'YER' => '&#65020;',
											'ZAR' => '&#82;',
											'ZMK' => '&#90;&#75;', 
											'ZWL' => '&#90;&#36;',
										);
									@endphp


									<div class="addnewprotab_item-sec">

										<div class="addnewprotab_item-sec-page">

											<div class="form-group addpro-page-form-price-data-all">

												<label for="" class="form-control-label addpro-page-form-price-detail-1">Unit price</label>

												<div class="addpro-page-form-price-detail-2">

												    <input type="number" min="0" value="{{$product->unit_price}}" step="0.01" placeholder="{{__('Unit price')}}" name="unit_price" class="form-control" required>

												</div>

												<div class="addpro-page-form-price-detail-3">
													<select class="form-control demo-select2-placeholder" name="currency" id="select-price" data-live-search="true">
														@foreach($currency_symbols as $key => $val)
														<option style="font-family:Arial;" {{ $key === $product->currency ? "selected" : "Goodbye" }} value="{{$key}}">{{$key}} <?php echo $val; ?></option>
														@endforeach
													</select>
												</div>

											</div>

											<div class="form-group addpro-page-form-price-data-all">

												<label for="" class="form-control-label addpro-page-form-price-detail-1">Purchase price</label>

												<div class="addpro-page-form-price-detail-2">

												    <input type="number" min="0" value="{{$product->purchase_price}}" step="0.01" placeholder="{{__('Purchase price')}}" name="purchase_price" class="form-control" required>

												</div>

												<div class="addpro-page-form-price-detail-3">

												<select class="form-control demo-select2-placeholder" name="currency1" id="select-price" data-live-search="true">
														@foreach($currency_symbols as $key => $val)
														<option style="font-family:Arial;" {{ $key === $product->currency ? "selected" : "Goodbye" }} value="{{$key}}">{{$key}} <?php echo $val; ?></option>
														@endforeach
													</select>

												</div>

											</div>

											<div class="form-group addpro-page-form-price-data-all">

												<label for="" class="form-control-label addpro-page-form-price-detail-1">Tax</label>

												<div class="addpro-page-form-price-detail-2">

												    <input type="number" min="0" value="{{$product->tax}}" step="0.01" placeholder="{{__('Tax')}}" name="tax" class="form-control" required>

												</div>

												<div class="addpro-page-form-price-detail-3">

													<select class="form-control demo-select2-placeholder" id="select-price" data-live-search="true" name="tax_type">

                                                        <option value="amount" <?php if($product->tax_type == 'amount') echo "selected";?> >$</option>

	                                	                <option value="percent" <?php if($product->tax_type == 'percent') echo "selected";?> >%</option>

													</select>

												</div>

											</div>

											<div class="form-group addpro-page-form-price-data-all">

												<label for="" class="form-control-label addpro-page-form-price-detail-1">Discount</label>

												<div class="addpro-page-form-price-detail-2">

												    <input type="number" min="0" value="{{ $product->discount }}" step="0.01" placeholder="{{__('Discount')}}" name="discount" class="form-control" required>

												</div>

												<div class="addpro-page-form-price-detail-3">

													<select class="form-control demo-select2-placeholder" id="select-price" data-live-search="true">

                                                        <option value="amount" <?php if($product->discount_type == 'amount') echo "selected";?> >$</option>

                                                        <option value="percent" <?php if($product->discount_type == 'percent') echo "selected";?> >%</option>

													</select>

												</div>

											</div>

										</div>

									</div>

								

								</div>

								<!-- External link -->

								<div class="addnewprotab_item">

                                    <div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
										    <div class="form-group addpro-page-form-img-data">
												<label for="" class="form-control-label addpro-page-form-img-detail-1">{{__('Select Website')}}</label>
												<div class="addpro-page-form-img-detail-2">
												    <select name="external_website" class="form-control mb-3">
												        <option  value="" >Select Website</option>
												        <option {{ $product->external_website === "amazon" ? "selected" : "Goodbye" }} value="amazon" >Amazon</option>
												        <option {{ $product->external_website === "ebay" ? "selected" : "Goodbye" }} value="ebay" >Ebay</option>
												    </select>
												</div>
											</div>
											<div class="form-group addpro-page-form-img-data">
												<label for="" class="form-control-label addpro-page-form-img-detail-1">{{__('Product Asin')}}</label>
												<div class="addpro-page-form-img-detail-2">
												<input type="text" class="form-control mb-3" name="external_link" placeholder="{{__('Product ASIN')}}" value="{{$product->external_link}}">
												</div>
											</div>
										</div>
									</div>

								</div>

								<!-- Description -->

								<div class="addnewprotab_item">



									<div class="addnewprotab_item-sec">

										<div class="addnewprotab_item-sec-page">

											<div class="container-fluid">

												<div class="row">

													<div class="container">

														<div class="row">

															<div class="col-lg-12 nopadding">

																<textarea id="txtEditor" class="editor" name="description">{{$product->description}}</textarea>

															</div>

														</div>

													</div>

												</div>

											</div>

										</div>

									</div>

								

								</div>

								<!-- spacification -->

								<div class="addnewprotab_item">
									
									<div class="addnewprotab_item-sec">

										<div class="addnewprotab_item-sec-page">

											<div class="container-fluid">

												<div class="row">

													<div class="container">

														<div class="row">

															<div class="col-lg-12 nopadding">

																<textarea id="txtEditor" class="editor" name="spacification">{{$product->spacification}}</textarea>

															</div>

														</div>

													</div>

												</div>

											</div>

										</div>

									</div>

								

								</div>
@if($product->product_type != 'guide')

                            

								<!-- Variations -->

								<div class="addnewprotab_item">
									
									<div class="addnewprotab_item-sec">

										<div class="addnewprotab_item-sec-page">

											<div class="container-fluid">

												<div class="row">

													<div class="container">

														<div class="row">

															<div class="col-lg-12 nopadding clr-sec-pro-frm-wdth-2">
					<div class="single-pro-ban-size">
						<div class="single-pro-ban-des-hed single-pro-ban-des-hed-2">
						<h5>Colors</h5>
					  </div>
						
						<div class="single-pro-ban-size-detail">
							<div class="input_fields_wrap sngl-pr-clr">
								<div class="form-group">
								    <button class="add_field_button btn btn-success">Add Color</button>
								   @if (!empty($product->colors) && ($product->colors != 'null')) 
								   
								    @foreach(json_decode($product->colors) as $value )
								    <span style="position:relative;">
								    <input type="color" name="colors[]" value="{{ $value }}" style="height: 33px; margin-left:20px; margin-bottom:0px;">
								    <a href="#" class="remove_field" style="position:absolute; right:-2px; top:-18px;"><i class="fa fa-times"></i></a>
								    </span>
								    @endforeach
								    
								    @endif
																    
						</div>			
						</div>
					</div>
														
			     </div>
                <div class="single-pro-ban-size">
					<div class="single-pro-ban-des-hed single-pro-ban-des-hed-2">
						<h5>Size</h5>
					</div>
					<div class="single-pro-ban-size-detail">
						<label for="spro-small">Small</label><input type="checkbox" name="size[]" value="S" id="spro-small" @if (!empty($product->size)  && ($product->size != 'null')) @foreach(json_decode($product->size) as $value ) {{ $value == 'S' ? 'checked' : '' }} @endforeach @endif>
						<label for="spro-medium">Medium</label><input type="checkbox" name="size[]" value="M" id="spro-medium" @if (!empty($product->size)  && ($product->size != 'null'))  @foreach(json_decode($product->size) as $value ) {{ $value == 'M' ? 'checked' : '' }} @endforeach @endif>
						<label for="spro-large">Large</label><input type="checkbox" name="size[]" value="large" id="spro-large" @if (!empty($product->size)  && ($product->size != 'null'))  @foreach(json_decode($product->size) as $value ) {{ $value == 'L' ? 'checked' : '' }} @endforeach @endif>
						<label for="spro-extralarge">Extra Large</label><input type="checkbox" name="size[]" value="XL" id="spro-extralarge" @if (!empty($product->size)  && ($product->size != 'null'))  @foreach(json_decode($product->size) as $value ) {{ $value == 'XL' ? 'checked' : '' }} @endforeach @endif>
					
					</div>
				</div>

				<div class="single-pro-ban-quant">
					<div class="single-pro-ban-des-hed">
						<h5>Quantity</h5>
					</div>
					<div class="container px-0 mx-0">
						<div class="number-input md-number-input">
						  <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus"></button>
						  <input class="quantity" min="1" name="quantity" value="{{ $product->quantity }}"  type="number">
						  <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></button>
						</div>    
					</div>
				</div>

											</div>

														</div>

													</div>

												</div>

											</div>

										</div>

									</div>

								

								</div>
                            @endif

								<!--PDF Specification  -->
								@if($product->product_type == 'guide')
								<div class="addnewprotab_item">



									<div class="addnewprotab_item-sec">

										<div class="addnewprotab_item-sec-page">

											<div class="form-group addpro-page-form-pdf-data-all">

												<div class="form-group addpro-page-form-pdf-data-1" id="pdfconatiner">

													<div class="form-group addpro-page-form-pdf-data-2" id="pdfshowsec">

														<label for="" class="form-control-label addpro-page-form-pdf-detail-1">PDF Specification</label>

														<div class="addpro-page-form-pdf-detail-2">

														<select name="lng[]" class="form-control demo-select2-placeholder" id="select-pdf" data-live-search="true">

                                                            @foreach (\App\Models\Language::all() as $key => $language)

                                                                <option value="{{$language->id}}">{{ $language->name}}</option>

                                                            @endforeach

														</select>

														</div>

														<!-- <div class="form-group addpro-page-form-general-data-all">

															<div class="addpro-page-form-img-general-2">

																<input type="file" class="form-control" name="pdf" required>

															</div>

														</div> -->

														<div class="addpro-page-form-pdf-detail-3">

															<input type="file" class="form-control p-1" name="pdf" id="pdf">

														</div>

													</div>

												</div>

												<div class="addpro-page-form-pdf-detail-4">

													<p class="addpro-page-form-pdf-addmore" id="pdfaddmore">Add More</p>

												</div>

											</div>

										</div>

									</div>

								

								</div>
								@endif
								<!-- end PDF Specification -->



								<div class="panel-footer text-right mt-3">

									<button type="submit" name="button" class="btn btn-info upload-product">Save</button>

								</div>

							</form>

					    </div>

					</div>

				</div>

			</div>

			

			

		</div>

	</section>    



@endsection



@section('script')

<script type="text/javascript">

    var category_name = "";

    var subcategory_name = "";

    var subsubcategory_name = "";



    var category_id = null;

    var subcategory_id = null;

    var subsubcategory_id = null;

    $(function () {
$(document).ready(function() {
	var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
	var add_button      = $(".add_field_button"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append('<div class="form-group"><span style="position:relative;"><input type="color" name="colors[]" style="height:33px; margin-bottom:0px; margin-left:20px;"/><a href="#" class="remove_field" style="position:absolute; right:-2px; top:-18px;"><i class="fa fa-times"><i/></a></span></div>'); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('span').remove(); x--;
	})
});
        // tabs

        $(".addnewpropriwrapper .addnewpropritab").click(function() {

			$(".addnewpropriwrapper .addnewpropritab button").removeClass('active');

			$(this).children().addClass('active');

            $(".addnewpropriwrapper .addnewtabpropri").removeClass("active").eq($(this).index()).addClass("addnewpropractive");

            $(".addnewprotab_item").hide().eq($(this).index()).fadeIn()

	    }).eq(0).children().addClass("active");


		$(".upload-product").click(function() {
			$('#choice_form input, #choice_form select, #choice_form textarea').each(function() {
				var attr = $(this).attr('required');
				if (typeof attr !== 'undefined' && attr !== false && $(this).val() == '') {
					$('.addnewpropritab button').removeClass('active');
					$('.addnewprotab_item').hide();
					$(this).closest('.addnewprotab_item').show();
					var Buttonindex = $(this).closest('.addnewprotab_item').index();
					$('.addnewpropritab').eq(Buttonindex).children().addClass('active');
					return false;
				}
			});
		});


        $(document).ready(function(){

            $('#hideimgform1').click(function(){

                $('.hidedivform1').hide();

            });

            $('#hideimgform2').click(function(){

                $('.hidedivform2').hide();

            });

            $('#hideimgform3').click(function(){

                $('.hidedivform3').hide();

            });

            $('#hideimgform4').click(function(){

                $('.hidedivform4').hide();

            });

            $('#hideimgform5').click(function(){

                $('.hidedivform5').hide();

            });

            $('#hideimgformthumb').click(function(){

                $('.hidedivformthumb').hide();

            });

            $('#hideimgformmeta').click(function(){

                $('.hidedivformmeta').hide();

            });





            $('#category_id').on('change', function() {

                get_subcategories_by_category();

            });



            $('#subcategory_id').on('change', function() {

                get_subsubcategories_by_subcategory();

            });



            $('#subsubcategory_id').on('change', function() {

                get_brands_by_subsubcategory();

            });



            $("#photos").spartanMultiImagePicker({

                fieldName:        'photos[]',

                maxCount:         10,

                rowHeight:        '200px',

                groupClassName:   'col-md-4 col-sm-4 col-xs-6',

                maxFileSize:      '',

                dropFileLabel : "Drop Here",

                onExtensionErr : function(index, file){

                    console.log(index, file,  'extension err');

                    alert('Please only input png or jpg type file')

                },

                onSizeErr : function(index, file){

                    console.log(index, file,  'file size too big');

                    alert('File size too big');

                }

            });



            $("#thumbnail_img").spartanMultiImagePicker({

                fieldName:        'thumbnail_img',

                maxCount:         1,

                rowHeight:        '200px',

                groupClassName:   'col-md-4 col-sm-4 col-xs-6',

                maxFileSize:      '',

                dropFileLabel : "Drop Here",

                onExtensionErr : function(index, file){

                    console.log(index, file,  'extension err');

                    alert('Please only input png or jpg type file')

                },

                onSizeErr : function(index, file){

                    console.log(index, file,  'file size too big');

                    alert('File size too big');

                }

            });



            $("#meta_photo").spartanMultiImagePicker({

                fieldName:        'meta_img',

                maxCount:         1,

                rowHeight:        '200px',

                groupClassName:   'col-md-4 col-sm-4 col-xs-6',

                maxFileSize:      '',

                dropFileLabel : "Drop Here",

                onExtensionErr : function(index, file){

                    console.log(index, file,  'extension err');

                    alert('Please only input png or jpg type file')

                },

                onSizeErr : function(index, file){

                    console.log(index, file,  'file size too big');

                    alert('File size too big');

                }

            });

            

            $('.remove-files').on('click', function(){

                $(this).parents(".col-md-4").remove();

            });

        });

        $(function () {
        	

            $('.demo-select2').select2();

        });

    });



    // $(document).ready(function() {

    //     $("#txtEditor").Editor();

    // });



    document.getElementById("pdfaddmore").onclick = function() {

        var container = document.getElementById("pdfconatiner");

        var section = document.getElementById("pdfshowsec");

        container.appendChild(section.cloneNode(true));

    }



    function get_subcategories_by_category(){

            var category_id = $('#category_id').val();

            $.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){

                $('#subcategory_id').html(null);

                for (var i = 0; i < data.length; i++) {

                    $('#subcategory_id').append($('<option>', {

                        value: data[i].id,

                        text: data[i].name

                    }));

                    $('.demo-select2').select2();

                }

                get_subsubcategories_by_subcategory();

            });

        }



	function get_subsubcategories_by_subcategory(){

		var subcategory_id = $('#subcategory_id').val();

		$.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){

		    $('#subsubcategory_id').html(null);

		    for (var i = 0; i < data.length; i++) {

		        $('#subsubcategory_id').append($('<option>', {

		            value: data[i].id,

		            text: data[i].name

		        }));

		        $('.demo-select2').select2();

		    }

		    get_brands_by_subsubcategory();

		});

	}



	function get_brands_by_subsubcategory(){

		var subsubcategory_id = $('#subsubcategory_id').val();

		$.post('{{ route('subsubcategories.get_brands_by_subsubcategory') }}',{_token:'{{ csrf_token() }}', subsubcategory_id:subsubcategory_id}, function(data){

		    $('#brand_id').html(null);

		    for (var i = 0; i < data.length; i++) {

		        $('#brand_id').append($('<option>', {

		            value: data[i].id,

		            text: data[i].name

		        }));

		        $('.demo-select2').select2();

		    }

		});

	}

	

	function product_form_submit() {

        var $fileUpload = $("input[name='photos[]']");

        if (parseInt($fileUpload.get(0).files.length) > 8){

            alert("You are only allowed to upload a maximum of 8 files");

            return false;

        }

        return true;

    }



    $("input[data-role=tagsinput]").tagsinput();

</script>

@endsection