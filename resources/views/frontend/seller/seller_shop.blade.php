@extends('frontend.layouts.new_app_1')

<?php
if (isset($data['minibgcolor']))
{
?>
				<style type="text/css">
					.minibgcolor { background-color: <?php echo $data['minibgcolor']; ?> }
				</style>
		<?php
}
else
{ ?>
			<style type="text/css">
					.minibgcolor { background-color: #ffffff !important; }
				</style>
		<?php
} ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
@section('content')
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<div id="fb-root"></div>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v11.0&appId=827935497371969&autoLogAppEvents=1" nonce="qYPZR133"></script>
	@if($shop->c_active != 0)

		<div class="modal" id="coupon_active_modal" data-aos="{{$shop->animatecoupan}}">

			<div class="modal-dialog" style="{{ $shop->c_active ==1 ? 'max-width: 700px;' :'' }} {{ $shop->c_active ==2 ? 'max-width: 650px;' :'' }}
			{{$shop->c_active ==3 ? 'max-width: 500px;' :'' }}">

				<div class="modal-content">

					<button type="button" class="close px-3 bg-danger border-1 pb-2 pt-1 text-white" data-dismiss="modal" style="position: absolute; top: 10px; right: 10px;z-index:1;opacity: 1;">&times;</button>

					@if($shop->c_active == 1)



						@include('frontend.inc.ccoupon')

					@elseif($shop->c_active == 2)

						@include('frontend.inc.cbanner')
					@elseif($shop->c_active == 4)
						@include('frontend.inc.banner1')
						@elseif($shop->c_active == 5)
						@include('frontend.inc.banner2')
					@else

						@include('frontend.inc.cpopup')

					@endif

				</div>

			</div>

		</div>

	@endif

    <!-- product picture -->

	@if(App\Models\Product::where('id', $shop->product)->count() > 0)

		@php

			$product = App\Models\Product::where('id', $shop->product)->first();

			$main_photo = '';

			$photos = array();

			if(!empty($product->photos)){

				$photos = json_decode($product->photos);

				$main_photo = $photos[0];

			}

		@endphp

		<section class="miniqr-product-page-sec minibgcolor">

			<div class="minipage">

				<div class="miniqr-product-page-head">

					<h2>{{ $product->name }}</h2>

				</div>

				<div class="minipro-page-pro-pic-data-slider" data-aos="{{$shop->animateguide}}">

					<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

						<div class="carousel-inner mini-site-slider-img-wdth">

							@for($i=0; $i < count($photos); $i++)

								<div class="carousel-item {{ $i === 0 ? 'active' : '' }}" >

									<a href="#"><img style="object-fit: cover;"  class="d-block w-100" src="{{asset($photos[$i])}}" alt="First slide"></a>

								</div>

							@endfor

						</div>

						<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">

							<i class="fa fa-angle-left minisite-slider-moveicn"></i>

							<span class="sr-only">Previous</span>

						</a>

						<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">

							<i class="fa fa-angle-right minisite-slider-moveicn"></i>

							<span class="sr-only">Next</span>

						</a>

					</div>
				</div>

				<div class="minipro-page-pro-pic-data">

					@for($i=0; $i < count($photos); $i++)

					<div class="minipro-page-pro-pic-detail-1">

						<div class="minipro-page-pro-pic-1">

							<a href="#"><img style="object-fit: contain;" src="{{asset($photos[$i])}}"></a>

						</div>

					</div>

					@endfor

				</div>

			</div>

		</section>

	@endif

	@if ($shop->video_provider != null && $shop->video_link != null)

		<!-- link to product store -->

		<section class="minipro-page-store-sec minibgcolor">

			<div class="minipage" data-aos="{{$shop->animatevideo}}">

				<div class="minipro-page-store-head">

					<h2>{{__('INSTRUCTION VIDEO')}}</h2>

				</div>

				<div class="minipro-page-store-data-slider">

					<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">

					<div class="carousel-inner">

						<div class="carousel-item active">
							<?php preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $shop->video_link, $match);
							?>
							@if ($shop->video_provider == 'youtube' && $shop->video_link != null)

								<iframe class="d-block w-100" width="100%" height="409" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $match[1] }}"></iframe>

							@elseif ($shop->video_provider == 'dailymotion' && $shop->video_link != null)
								<iframe class="embed-responsive-item d-block w-100" src="https://www.dailymotion.com/embed/video/{{ explode('video/', $shop->video_link)[1] }}"></iframe>

							@elseif ($shop->video_provider == 'vimeo' && $shop->video_link != null)

								<iframe class="d-block w-100" src="https://player.vimeo.com/video/{{ explode('vimeo.com/', $shop->video_link)[1] }}" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

							@elseif (!empty($shop->video_temp))

								<video  style="w-100">

									<source src="{{$shop->video_link}}" type="video/mp4">

									Your browser does not support HTML video.

								</video>

							@endif

						<!-- <iframe class="d-block w-100" height="409" src="https://www.youtube.com/embed/KBo2El1bcwQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

						</div>

						<!-- <div class="carousel-item">

						<iframe class="d-block w-100" height="409" src="https://www.youtube.com/embed/ZwnXW_7fzk0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

						</div>

						<div class="carousel-item">

						<iframe class="d-block w-100" height="409" src="https://www.youtube.com/embed/yXyu7rqlpvs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

						</div> -->

					</div>

					<!-- <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">

						<i class="fa fa-angle-left minisite-slider-moveicn-2"></i>



					</a>

					<a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">

						<i class="fa fa-angle-right minisite-slider-moveicn-2"></i>



					</a> -->

					</div>

				</div>

				<div class="minipro-page-store-data">

					@if ($shop->video_provider == 'youtube' && $shop->video_link != null)

						<iframe width="100%" height="409" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $match[1] }}"></iframe>

					@elseif ($shop->video_provider == 'dailymotion' && $shop->video_link != null)

						<iframe src="https://www.dailymotion.com/embed/video/{{ explode('video/', $shop->video_link)[1] }}"></iframe>

					@elseif ($shop->video_provider == 'vimeo' && $shop->video_link != null)

						<iframe src="https://player.vimeo.com/video/{{ explode('vimeo.com/', $shop->video_link)[1] }}" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

					@elseif (!empty($shop->video_temp))

						<video  style="w-100">

							<source src="{{$shop->video_link}}" type="video/mp4">

							Your browser does not support HTML video.

						</video>

					@endif

				</div>

			</div>

		</section>

	@endif



	<!-- Dowmload and view -->

	@if($shop->pdf != null)

		@php

			$language = false;

			$pdfs = json_decode($shop->pdf);

			foreach($pdfs as $key => $val){

				if($val->lng != null){

					$language = true;

				}

			}

		@endphp

		<section class="minipro-page-downloads-sec minibgcolor">

			<div class="minidown-page" data-aos="{{$shop->animatepdf}}">

				<div class="pro-downloads-head">

					<h2>{{$shop->pdf_title}}</h2>

				</div>

				<div class="minipro-page-downloads-data">

					<div class="minipro-page-downloads-text">

						<div class="minipro-page-downloads-para">

							<p>

								{!! $shop->pdf_text !!}

							</p>

						</div>

					</div>

					<div class="minipro-page-downloads-btns">

						@if($language == true)

							<div class="minipro-page-downloads-btns-detail-1" style="display:none;">

								<form class="minipro-page-downloads-btns-detail-select">

									<select name="cars" id="cars">

										<option value="slect" selected>{{__('SELECT LANGUAGE')}}</option>

										@foreach($pdfs as $key => $val)

											@if($val != null)

												<option value="eng">{{__('English')}} </option>

											@endif

										@endforeach

									</select>

								</form>

							</div>

						@endif

						<div class="minipro-page-downloads-btns-detail-2">
							@php

								$getpath = json_decode($shop->pdf);

								foreach($getpath as $key => $val){

									if($val->path != null){

										$path = $val->path;


									}

								}

							@endphp

							<div class="minipro-page-downloads-btns-detail">
								<form class="minipro-page-downloads-btns-detail-select">
									@if(sizeof($getpath) > 1)
									<select onchange="window.location = this.value;">

										<option value="slect" selected>{{__('SELECT ORIGNAL GUIDE FOR VIEW')}}</option>
										@php
										foreach($getpath as $key => $val){

											if($val->path != null){

												$path = $val->path;
												$finalpath = url('/public').'/'.$path;
										@endphp
										<!-- <form method="get" action=""> -->
										   <option value="<?php echo $finalpath ?>">{{__('VIEW ORIGNAL GUIDE')}}</option>
										<!-- </form> -->
										@php
											}

										}
										@endphp
									</select>
									@else
									<button  type="button" value="Open Window" onclick="window.open('<?php echo url('/public') . '/' . $path ?>')" >{{__('VIEW ORIGNAL GUIDE')}}</button>
									@endif
								</form>
							</div>
							<div class="minipro-page-downloads-btns-detail">
								<form class="minipro-page-downloads-btns-detail-select">
									@if(sizeof($getpath) > 1)
									<select onchange="window.location = this.value;">

										<option value="slect" selected>{{__('VIEW TRANSLATED GUIDE')}}</option>
										@php
										foreach($getpath as $key => $val){

											if($val->path != null){

												$path = $val->path;
												$finalpath = 'http://translate.google.com/translate?sl=en&tl=en&u='.url('/public').'/'.$path
										@endphp
										<!-- <form method="get" action=""> -->
										   <option value="<?php echo $finalpath ?>">{{__('VIEW TRANSLATED GUIDE')}}</option>
										<!-- </form> -->
										@php
											}

										}
										@endphp
									</select>
									@else
									<button  type="button" value="Open Window" onclick="window.open('<?php echo 'http://translate.google.com/translate?sl=auto&tl=en&u=' . url('/public') . '/' . $path ?>')" >{{__('VIEW TRANSLATED GUIDE')}}</button>
									@endif
								</form>
							</div>

							<div class="minipro-page-downloads-btns-detail">
								<form class="minipro-page-downloads-btns-detail-select">
									@if(sizeof($getpath) > 1)
									<select onchange="window.location = this.value;">
										<option value="slect" selected>{{__('DOWNLAOD GUIDE')}}</option>
										@php
										foreach($getpath as $key => $val){

											if($val->path != null){

												$path = $val->path;
												$headers = array(
												  'Content-Type' => 'application/pdf',
												);
										@endphp
												<option value="{{ url('/public').'/'.$path }}">{{__('DOWNLAOD GUIDE')}}</option>

										@php
											}

										}
										@endphp
									</select>
									@else

										<!-- <form method="get" action="{{ url('/public').'/'.$path }}"> -->
											<button type="button" value="Open Window" onclick="window.open('<?php echo url('/public') . '/' . $path ?>')">{{__('DOWNLAOD GUIDE')}} </button>
										<!-- </form> -->
									@endif
								</form>
							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="minipro-page-downloads-overlay"></div>

		</section>

	@endif

	<!-- product information -->

	@if(($shop->product != null) and (!empty($shop->product)) || $shop->questions)

		<section class="minipro-page-informa-sec minibgcolor">

			<div class="minipage">

				<div class="mininewpropriwrapper">

					<div class="mininewpropriwrapper-page">

						<div class="mininewpropritabs">

							@if((App\Models\Product::where('id', $shop->product)->count() > 0) and (!empty(App\Models\Product::where('id',$shop->product)->first()->description)))

								<span class="mininewpropritab"><button class="mininewpropritabs-btn">{{__('PRODUCT INFORMATION')}}</button></span>

							@endif

							@if(!empty($shop->product) and (!empty(App\Models\Product::where('id',$shop->product)->first()->spacification)))

							<span class="mininewpropritab"><button class="mininewpropritabs-btn">{{__('PRODUCT SPECIFICATION')}}</button></span>

							@endif

							<span class="mininewpropritab"><button class="mininewpropritabs-btn">{{__('COMPANY PROFILE')}}</button></span>

						</div>

						<div class="mininewtab_content">

							@if(App\Models\Product::where('id', $shop->product)->count() > 0)

								<div class="mininewprotab_item" data-aos="flip-left">


								<p>
									{!! App\Models\Product::where('id', $shop->product)->first()->description !!}

								</p>

								<div class="minipro-page-informa-content-btns">

						<div class="minipro-page-informa-content-btn-1">

							<a href="#" data-toggle="modal" data-target="#improveModal">{{__('HOW CAN WE IMPROVE OUR PRODUCT')}} <i class="fa fa-angle-right"></i></a>


						</div>


						<div class="minipro-page-informa-content-btns-data">

							<div class="minipro-page-informa-content-btns-social">

								<div class="minipro-page-informa-content-btns-social-text">

									<p>{{__('SHARE THIS GUIDE')}}</p>

								</div>

								<div class="minipro-page-informa-content-btns-social-icons">

									<a class="fb-share-button" data-href="{{ url()->current() }}" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}" class="fb-xfbml-parse-ignore"><img src="{{asset('frontend/images/f2.png')}}"></a>

									<a target="_blank" class="twitter-share-button"
									  href="https://twitter.com/intent/tweet?text={{ url()->current() }}"
									  data-size="Default">
									<img src="{{asset('frontend/images/t.png')}}"></a>

									<!-- <a href="#"><img src="{{asset('frontend/images/g2.png')}}"></a>

									<a href="#"><img src="{{asset('frontend/images/f2.png')}}"></a>

									<a href="#"><img src="{{asset('frontend/images/t.png')}}"></a> -->

								</div>

							</div>

							<div class="minipro-page-informa-content-btns-buy" >

								<a href="#">{{__('BUY NOW')}} <i class="fa fa-shopping-cart"></i></a>

							</div>

						</div>

					</div>

								</div>

							@endif

							<div class="mininewprotab_item">



								{!! App\Models\Product::where('id', $shop->product)->first()->spacification !!}

								<div class="minipro-page-informa-content-btns">

						<div class="minipro-page-informa-content-btn-1">

							<a href="#" data-toggle="modal" data-target="#improveModal">{{__('HOW CAN WE IMPROVE OUR PRODUCT')}} <i class="fa fa-angle-right"></i></a>


						</div>


						<div class="minipro-page-informa-content-btns-data">

							<div class="minipro-page-informa-content-btns-social">

								<div class="minipro-page-informa-content-btns-social-text">

									<p>{{__('SHARE THIS GUIDE')}}</p>

								</div>

								<div class="minipro-page-informa-content-btns-social-icons">

									<a class="fb-share-button" data-href="{{ url()->current() }}" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}" class="fb-xfbml-parse-ignore"><img src="{{asset('frontend/images/f2.png')}}"></a>

									<a target="_blank" class="twitter-share-button"
									  href="https://twitter.com/intent/tweet?text={{ url()->current() }}"
									  data-size="Default">
									<img src="{{asset('frontend/images/t.png')}}"></a>

									<!-- <a href="#"><img src="{{asset('frontend/images/g2.png')}}"></a>

									<a href="#"><img src="{{asset('frontend/images/f2.png')}}"></a>

									<a href="#"><img src="{{asset('frontend/images/t.png')}}"></a> -->

								</div>

							</div>

							<div class="minipro-page-informa-content-btns-buy">

								<a href="#">{{__('BUY NOW')}} <i class="fa fa-shopping-cart"></i></a>

							</div>

						</div>

					</div>
							</div>




								<div class="mininewprotab_item" data-aos="{{$shop->animateprofile}}">

									<div class="minipro-page-ques-head">

										<h2>{{__('COMPANY PROFILE')}}</h2>

									</div>


									<div class="minipro-page-profile-text">
										@if(!empty($shop->logo))
										<img src="{{ url('/public').'/'.$shop->logo }}" alt="" width="300" height="300" style="object-fit: contain;" />
										@endif
										@if(!empty($shop->address))
										<p><b>Address: </b>{!! $shop->address !!}</p>
										@endif
										@if(!empty($shop->contact_no))
										<p><b>Contact No: </b>{!! $shop->contact_no !!}</p>
										@endif
										@if(!empty($shop->company_email))
										<p><b>Company Email: </b>{!! $shop->company_email !!}</p>
										@endif
										@if(!empty($shop->company_website))
										<p><b>Company Website: </b>{!! $shop->company_website !!}</p>
										@endif
										@if(!empty($shop->description))
										<p><b>Description: </b>{!! $shop->description !!}</p>
										@endif

									</div>
									<div class="minipro-page-profile-content">

										@if ($shop->sliders != null && !empty($shop->sliders))
											<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
											  <div class="carousel-inner">
											  	@foreach (json_decode($shop->sliders) as $key => $slide)
											    <div class="carousel-item minipro-page-profile-img {{ $loop->first ? 'active' : '' }}">
											      <img class="d-block w-100" src="{{ asset($slide) }}" alt="First slide">
											    </div>
											    @endforeach
											  </div>
											  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">

												<i class="fa fa-angle-left minisite-slider-moveicn"></i>

												<span class="sr-only">Previous</span>

												</a>

												<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">

													<i class="fa fa-angle-right minisite-slider-moveicn"></i>

													<span class="sr-only">Next</span>

												</a>
											</div>
										@endif
									</div>


									@if( isset($shop->linkedin) || isset($shop->whatsapp) || isset($shop->youtube) || isset($shop->twitter) || isset($shop->facebook) || isset($shop->instagram)  || isset($shop->tiktok))
									<div class="minipro-page-profile-oursocial-data" style="margin-top:40px;">

										<div class="minipro-page-profile-oursocial-detail" style="background-color: {{ $shop->social_bgcolor }};" data-aos="{{$shop->animatesocial}}">

											<div class="minipro-page-profile-oursocial-text" >

												<p>{{__('OUR SOCIAL MEDIA PAGES')}}</p>

											</div>

											<div class="minipro-page-profile-oursocial-icns">

												@if($shop->linkedin != null and !empty($shop->linkedin))
												<a href="{{ $shop->linkedin }}"><img src="{{asset('frontend/images/in.png')}}"></a>
												@endif
												@if($shop->whatsapp != null and !empty($shop->whatsapp))
												<a href="{{ $shop->whatsapp }}"><img src="{{asset('frontend/images/wa.png')}}"></a>
												@endif
												@if($shop->youtube != null and !empty($shop->youtube))
												<a href="{{ $shop->youtube }}"><img src="{{asset('frontend/images/yt.png')}}"></a>
												@endif
												@if($shop->twitter != null and !empty($shop->twitter))
												<a href="{{ $shop->twitter }}"><img src="{{asset('frontend/images/tw.png')}}"></a>
												@endif
												@if($shop->facebook != null and !empty($shop->facebook))
												<a href="{{ $shop->facebook }}"><img src="{{asset('frontend/images/fa.png')}}"></a>
												@endif
												@if($shop->instagram != null and !empty($shop->instagram))
												<a href="{{ $shop->instagram }}"><img src="{{asset('frontend/images/ins.png')}}"></a>
												@endif
												@if($shop->c != null and !empty($shop->tiktok))
												<a href="{{ $shop->tiktok }}"><img src="{{asset('frontend/images/tk.png')}}"></a>
												@endif

											</div>

										</div>

									</div>
									@endif
								</div>
						</div>

					</div>


				</div>

			</div>

		</section>

	@endif

	<!-- company profile -->

	<section class="minipro-page-profile-sec minibgcolor">

		<div class="minipage">

			<div class="minipro-page-profile-sec-res">

				<!-- <div class="minipro-profile-head">



				</div> -->
				<!-- Comny profile +share -->


				<!-- <div class="minipro-page-profile-oursocialpro-imgs-data">



					<div class="minipro-page-profile-oursocialpro-imgs-detail">

						<div class="minipro-page-profile-oursocialpro-img-1">

							<img src="{{asset('frontend/images/bans1.jpg')}}"></a>

						</div>

					</div>



					<div class="minipro-page-profile-oursocialpro-imgs-detail">

						<div class="minipro-page-profile-oursocialpro-img-1">

							<a href="#"><img src="{{asset('frontend/images/bans2.jpg')}}"></a>

						</div>

					</div>



					<div class="minipro-page-profile-oursocialpro-imgs-detail">

						<div class="minipro-page-profile-oursocialpro-img-1">

							<a href="#"><img src="{{asset('frontend/images/bans3.jpg')}}"></a>

						</div>

					</div>



				</div> -->

			</div>

			<div class="minipro-page-profile-wranty-data">

				@if(($shop->warranty_text != null and !empty($shop->warranty_text)) || ($shop->warranty != null and !empty($shop->warranty)) || ($shop->warranty_content != null and !empty($shop->warranty_content)))

					<div class="minipro-page-profile-wranty-detail-1" data-aos="zoom-out">

						<div class="minipro-page-profile-wranty-icon-data">

							<div class="minipro-page-profile-wranty-icon">

								<i class="fa fa-check-square-o"></i>

							</div>

							<div class="minipro-page-profile-wranty-text">

								@php

								$warranty_route = '#';

								$warranty_text = 'Register your warranty here';

								if(($shop->warranty != null and !empty($shop->warranty)) || ($shop->warranty_content != null and !empty($shop->warranty_content))){

									$warranty_route = route('company_warranty.visit', $shop->slug);

								}

								if(!empty($shop->warranty_text)){

									$warranty_text = $shop->warranty_text;

								}

								@endphp

								<h3><a href="{{$warranty_route}}"> {{ $warranty_text }}</a></h3>





							</div>

						</div>

					</div>

				@endif

				@if($shop->club_text != null and !empty($shop->club_text))

					<div class="minipro-page-profile-wranty-detail-2" data-aos="{{$shop->animateclub}}">

						<div class="minipro-page-profile-wranty-icon-data">

							<div class="minipro-page-profile-wranty-icon">

								<i class="fa fa-users"></i>

							</div>

							<div class="minipro-page-profile-wranty-text">

								<h3><a href="{{ route('club_form',$shop->slug) }}" > {{$shop->club_text}}</a></h3>

							</div>

						</div>

					</div>

				@endif

			</div>

		</div>

	</section>

	@if($shop->reviews != null and !empty($shop->reviews) and $shop->product != null)

		<!-- coustomer reviews -->

		<section class="minipro-page-reviews-sec minibgcolor">
			<div class="minipage">
				<div class="feednewpropriwrapper">
			    	<div class="feednewpropriwrapper-page">
					    <div class="feednewpropritabs">
					        <span class="feednewpropritab"><button class="feednewpropritabs-btn">CUSTOMER FEEDBACK</button></span>
					        @if($shop->questions != null and !empty($shop->questions))
					        <span class="feednewpropritab"><button class="feednewpropritabs-btn">CUSTOMER Q.A</button></span>
					        @endif
					    </div>
					    <div class="feednewtab_content">

					        <div class="feednewprotab_item">
					        	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

								<style type="text/css">

									 .animated {

										    -webkit-transition: height 0.2s;

										    -moz-transition: height 0.2s;

										    transition: height 0.2s;

										}



										.stars

										{

										    margin: 20px 0;

										    font-size: 24px;

										    color: #d17581;

										}

								</style>

								<div class="container" data-aos="{{$shop->animatefaq}}">

									<div class="row" style="margin-top:40px;">

										<div class="col-md-12">

								    	<div class="well well-sm">

								            <div class="text-right">

								                <a class="btn btn-success btn-green" href="#reviews-anchor" id="open-review-box">Leave a Review</a>

								            </div>



								            <div class="row" id="post-review-box" style="display:none;">

								                <div class="col-md-12">

								                    <form accept-charset="UTF-8" action="{{route('company.reviews.submit')}}" method="post">

								                    	{{ csrf_field() }}

								                        <input id="ratings-hidden" name="rating" type="hidden">

								                        <input class="form-control" type="hidden" value="{{ $shop->product }}" name="product_id">

								                        <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5"></textarea>



								                        <div class="text-right">

								                            <div class="stars starrr" data-rating="0"></div>

								                            <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">

								                            <span class="glyphicon glyphicon-remove"></span>Cancel</a>

								                            <button class="btn btn-success btn-lg" type="submit">Save</button>

								                        </div>

								                    </form>

								                </div>

								            </div>

								        </div>



										</div>

									</div>

								</div>
								<div class="minipro-page-reviews-data" data-aos="{{$shop->animatereviews}}">

									<div class="minipro-page-reviews-detail-1">

										<div class="minipro-page-reviews-content">

											<div class="minipro-page-reviews-content-head">

												<h3>{{__('Customer reviews')}}</h3>

											</div>

											<div class="minipro-page-reviews-ls-stars-data">

												<div class="pro-page-reviews-ls-stars">
													@php $rating = round(((App\Models\Review::sum('rating') / (App\Models\Review::count('rating') * 5 )) * 5), 2); @endphp

										            @foreach(range(1,5) as $i)
										                <span class="fa-stack" style="width:1em">


										                    @if($rating > 0)
										                        @if($rating < 1)
										                            <i class="fa fa-star-half-o"></i>
										                        @elseif($rating >= 1)
										                            <i class="fa fa-star"></i>
										                        @endif
										                    @else
										                    <i class="fa fa-star-o"></i>
										                    @endif

										                    @php $rating--; @endphp
										                </span>
										            @endforeach


													<!-- <i class="fa fa-star"></i>

													<i class="fa fa-star"></i>

													<i class="fa fa-star"></i>

													<i class="fa fa-star"></i>

													<i class="fa fa-star-half-o"></i> -->

												</div>

												<div class="minipro-page-reviews-ls-stars-text">



													<p>{{ round(((App\Models\Review::sum('rating') / (App\Models\Review::count('rating') * 5 )) * 5), 2) }} out of 5</p>

												</div>

											</div>

											<div class="minipro-page-reviews-global">

												<p>{{ App\Models\Review::sum('rating') }} {{__('global ratings')}}</p>

											</div>

											<!-- 1rating -->

											<div class="minipro-page-ratings-data">

												<div class="minipro-page-ratings-detail-1">

													<p>5 star</p>

												</div>

												<div class="minipro-page-ratings-detail-2">

													<div class="minipro-page-ratings-high-1" style="width: {{ round((Models\Review::where('rating',5)->count('rating') / App\Models\Review::sum('rating') * 100),2) }}%;">

													</div>

													<div class="minipro-page-ratings-low-5">

													</div>

												</div>

												<div class="minipro-page-ratings-detail-3">

													<p>{{ round((Models\Review::where('rating',5)->count('rating') / App\Models\Review::sum('rating') * 100),2) }}%</p>

												</div>

											</div>

											<!-- 2rating -->

											<div class="minipro-page-ratings-data">

												<div class="minipro-page-ratings-detail-1">

													<p>4 star</p>

												</div>

												<div class="minipro-page-ratings-detail-2">

													<div class="minipro-page-ratings-high-2" style="width:{{ round((App\Models\Review::where('rating',4)->count('rating') / App\Models\Review::sum('rating') * 100),2) }}%;">

													</div>

													<div class="minipro-page-ratings-low-2">

													</div>

												</div>

												<div class="minipro-page-ratings-detail-3">

													<p>{{ round((App\Models\Review::where('rating',4)->count('rating') / App\Models\Review::sum('rating') * 100),2) }}%</p>

												</div>

											</div>

											<!-- 3rating -->

											<div class="minipro-page-ratings-data">

												<div class="minipro-page-ratings-detail-1">

													<p>3 star</p>

												</div>

												<div class="minipro-page-ratings-detail-2">

													<div class="minipro-page-ratings-high-3" style="width: {{ round((App\Models\Review::where('rating',3)->count('rating') / App\Models\Review::sum('rating') * 100),2) }}%;">

													</div>

													<div class="minipro-page-ratings-low-3">

													</div>

												</div>

												<div class="minipro-page-ratings-detail-3">

													<p>{{ round((App\Models\Review::where('rating',3)->count('rating') / App\Models\Review::sum('rating') * 100),2) }}%</p>

												</div>

											</div>

											<!-- 4rating -->

											<div class="minipro-page-ratings-data">

												<div class="minipro-page-ratings-detail-1">

													<p>2 star</p>

												</div>

												<div class="minipro-page-ratings-detail-2">

													<div class="minipro-page-ratings-high-4" style="width: {{ round((App\Models\Review::where('rating',2)->count('rating') / App\Models\Review::sum('rating') * 100),2) }}%;">

													</div>

													<div class="minipro-page-ratings-low-4">

													</div>

												</div>

												<div class="minipro-page-ratings-detail-3">

													<p>{{ round((App\Models\Review::where('rating',2)->count('rating') / App\Models\Review::sum('rating') * 100),2) }}%</p>

												</div>

											</div>

											<!-- 5rating -->

											<div class="minipro-page-ratings-data">

												<div class="minipro-page-ratings-detail-1">

													<p>1 star</p>

												</div>

												<div class="minipro-page-ratings-detail-2">

													<div class="minipro-page-ratings-high-5" style="width: {{ round((App\Models\Review::where('rating',1)->count('rating') / App\Models\Review::sum('rating') * 100),2) }}%;">

													</div>

													<div class="minipro-page-ratings-low-5">

													</div>

												</div>

												<div class="minipro-page-ratings-detail-3">

													<p>{{ round((Models\Review::where('rating',1)->count('rating') / App\Models\Review::sum('rating') * 100),2) }}%</p>

												</div>

											</div>

											<!-- <div class="minipro-page-ratings-calculate">

												<p><i class="fa fa-angle-down"></i>{{__('How are ratings calculated?')}}</p>

											</div> -->



										</div>

									</div>

									<div class="minipro-page-reviews-detail-2">

										<!-- coment-1 -->

										@foreach($reviews as $review)

										<div class="minipro-page-reviews-coments-data">

											<div class="minipro-page-reviews-coments-detail">

												<div class="minipro-page-reviews-coments-img">
													<img src="{{ asset(App\Models\User::find($review->user_id)->avatar_original) }}">

												</div>

												<div class="minipro-page-reviews-coments-img-text">

													<div class="minipro-page-reviews-client-data">

														<div class="minipro-page-reviews-client-name">

															<h4>{{  App\Models\User::find($review->user_id)->name }}</h4>

														</div>

														<div class="minipro-page-reviews-client-stars">

															@for($i=0; $i < $review->rating; $i++)

															<i class="fa fa-star"></i>

															@endfor

															@for($i=$review->rating; $i < 5; $i++)

															<i class="fa fa-star-o"></i>

															@endfor

														</div>



													</div>

													<div class="minipro-page-reviews-client-msg">

														<p>{{__($review->comment)}}</p>

													</div>

												</div>

											</div>

										</div>

										@endforeach


									</div>

								</div>
					        </div>
					        @if($shop->questions != null and !empty($shop->questions))
					        <div class="feednewprotab_item">
									<div class="minipro-page-ques-head">
										<h2>CUSTOMER QUESTION AND ANSWERS</h2>
									</div>
									<!-- <div class="minipro-page-ques-input">
										<form class="minipro-page-ques-form">
											<button type="submit"><i class="fa fa-search"></i></button>
										  	<input type="text" placeholder="Have aquestion? Search for answers" name="search">

										</form>
									</div> -->
									<div class="minipro-page-ques-head">
											<h4>{{__('SUBMIT YOUR QUESTION')}}</h4>
										</div>
										<div class="minipro-page-ques-input">

											<form class="form-inline" method="POST" action="{{route('company.question.submit')}}">

												{{ csrf_field() }}

												<input class="form-control" type="hidden" value="{{ $shop->slug }}" name="slug">

												<input class="form-control col-md-10" style="margin-bottom: 0px;" type="text" placeholder="Have aquestion? " name="question">

												<button type="submit" name="save-question" class="btn btn-success col-md-2" id="save-question">Submit</button>

											</form>

										</div>
									<!-- faq 1 -->
									@foreach(json_decode($shop->questions) as $key => $question)
									@if($question->status == 1)
									<div class="minipro-page-ques-data">
										<div class="minipro-page-ques-datail-1">
											<div class="minipro-page-votes-icon">
												<i class="fa fa-caret-up"></i>
											</div>
											<div class="minipro-page-votes-icon-text">
												<p>8</p>
												<p>votes</p>
											</div>
											<div class="minipro-page-votes-icon">
												<i class="fa fa-caret-down"></i>
											</div>
										</div>
										<div class="minipro-page-ques-datail-2">
											<div class="minipro-page-ques-text-data">
												<div class="minipro-page-ques-text-h-1">
													<h3>Question:</h3>
												</div>
												<div class="minipro-page-ques-text-h-2">
													<h3> {{ $question->question }}</h3>
												</div>
											</div>
											<div class="minipro-page-ques-ans-data">
												<div class="minipro-page-ans-text-h-1">
													<h3>Answer:</h3>
												</div>
												@php

													$created_at = strtotime($shop->created_at);

												@endphp


												<div class="minipro-page-ans-ans-h-2">
													<p>{{ $question->answer }}</p>
													<div class="minipro-page-ans-analys">
														<p>By analyst on {{ date('Fd, Y',$created_at) }}</p>
													</div>
													<!-- <div class="minipro-page-ans-see-more">
														<p><i class="fa fa-angle-down"></i>See more answers[7]</p>
													</div> -->
												</div>
											</div>

										</div>
									</div>
									@endif
									@endforeach


						    </div>
						    @endif
					    </div>
					</div>
				</div>
			</div>
		</section>
							<!-- Modal improve -->
							<div class="modal fade" id="improveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">HOW CAN WE IMPROVE OUR PRODUCT</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <form action="{{ route('company.commentsubmit') }}" method="POST">
							      <div class="modal-body">

							        	{{ csrf_field() }}
										<input class="form-control" type="hidden" value="{{ $shop->id }}" name="shop_id">
										<input class="form-control" type="hidden" value="{{ $shop->user_id }}" name="user_id">
										  <div class="form-group">
										    <input type="email" class="form-control" name="email" id="exampleFormControlInput1" placeholder="name@example.com">
										  </div>
										  <div class="form-group">
										    <textarea name="comment" rows="4" cols="50" style="width: 100%;height: 200px;border: 1px solid #e8e8e8;" placeholder="HOW CAN WE IMPROVE OUR PRODUCT..."></textarea>
										  </div>
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							        <button type="submit" class="btn btn-primary">Submit</button>
							      </div>
							      </form>
							    </div>
							  </div>
							</div>
	@endif

	<!-- related products -->

	@if($shop->related_products != null and $shop->product_type != "guide")

		@php

			$related_products = json_decode($shop->related_products);

		@endphp

		<section class="minipro-page-related-sec minibgcolor">

			<div class="minipage">

				<div class="minipro-page-related-head">

					<h2>{{__('TRY OUR ADDITIONAL ITEMS')}}</h2>

				</div>

				<div class="minipro-page-related-hr">

					<hr>

				</div>

				<div class="minipro-page-related-data" data-aos="{{$shop->animateproducts}}">

					@foreach($related_products as $key => $val)

						@if(Models\Product::where('id', $val)->count() > 0)

							@php

								$product = App\Models\Product::where('id', $val)->first();

								$main_photo = '';



								if(!empty($product->photos)){

									$photos = json_decode($product->photos);

									$main_photo = $photos[0];

								}

							@endphp

							<div class="minipro-page-related-detail">

								<div class="minipro-page-related-pics">

									<a href="{{ route('singleproduct',$product->slug) }}"><img style="object-fit: contain;" src="{{asset($main_photo)}}"></a>

								</div>

								<div class="minipro-page-related-pics-text">

									<a href="{{ route('singleproduct',$product->slug) }}"><h4>{{$product->name}}</h4></a>

								</div>

							</div>

						@endif

					@endforeach

				</div>

			</div>

		</section>

	@endif

@endsection



@section('script')

<script type="text/javascript">
AOS.init({
  	once: true,
  });
@if($shop->c_active != 0)

    $(window).on('load', function() {

        $('#coupon_active_modal').modal('show');

    });

@endif

    $(function () {

		// tabs

		$(".mininewpropriwrapper .mininewpropritab").click(function() {

		$(".mininewpropriwrapper .mininewtabpropri").removeClass("active").eq($(this).index()).addClass("mininewpropractive");

		$(".mininewprotab_item").hide().eq($(this).index()).fadeIn()

		}).eq(0).addClass("active");

	});



	function myFunction() {

		var miniinfodots = document.getElementById("miniinfodots");

		var miniinfomoreText = document.getElementById("miniinfomore");

		var miniinfobtnText = document.getElementById("miniinfomyBtn");



		if (miniinfodots.style.display === "none") {

			miniinfodots.style.display = "inline";

			miniinfobtnText.innerHTML = "Read more";

			miniinfomoreText.style.display = "none";

		} else {

			miniinfodots.style.display = "none";

			miniinfobtnText.innerHTML = "Read less";

			miniinfomoreText.style.display = "inline";

		}

	}

	// Reviews JS

		(function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o;"getComputedStyle"in window?(t=window.getComputedStyle(u,null),o=u.getBoundingClientRect().width,e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){o-=parseInt(t[i],10)}),s.style.width=o+"px"):s.style.width=Math.max(p.width(),0)+"px"}function a(){var a={};if(t=u,s.className=i.className,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width();p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word",resize:"none"===p.css("resize")||"vertical"===p.css("resize")?"none":"horizontal"}),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);



			var __slice=[].slice;(function(e,t){var n;n=function(){function t(t,n){var r,i,s,o=this;this.options=e.extend({},this.defaults,n);this.$el=t;s=this.defaults;for(r in s){i=s[r];if(this.$el.data(r)!=null){this.options[r]=this.$el.data(r)}}this.createStars();this.syncRating();this.$el.on("mouseover.starrr","span",function(e){return o.syncRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("mouseout.starrr",function(){return o.syncRating()});this.$el.on("click.starrr","span",function(e){return o.setRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("starrr:change",this.options.change)}t.prototype.defaults={rating:void 0,numStars:5,change:function(e,t){}};t.prototype.createStars=function(){var e,t,n;n=[];for(e=1,t=this.options.numStars;1<=t?e<=t:e>=t;1<=t?e++:e--){n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))}return n};t.prototype.setRating=function(e){if(this.options.rating===e){e=void 0}this.options.rating=e;this.syncRating();return this.$el.trigger("starrr:change",e)};t.prototype.syncRating=function(e){var t,n,r,i;e||(e=this.options.rating);if(e){for(t=n=0,i=e-1;0<=i?n<=i:n>=i;t=0<=i?++n:--n){this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass("glyphicon-star")}}if(e&&e<5){for(t=r=e;e<=4?r<=4:r>=4;t=e<=4?++r:--r){this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass("glyphicon-star-empty")}}if(!e){return this.$el.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty")}};return t}();return e.fn.extend({starrr:function(){var t,r;r=arguments[0],t=2<=arguments.length?__slice.call(arguments,1):[];return this.each(function(){var i;i=e(this).data("star-rating");if(!i){e(this).data("star-rating",i=new n(e(this),r))}if(typeof r==="string"){return i[r].apply(i,t)}})}})})(window.jQuery,window);$(function(){return $(".starrr").starrr()})



	$(function(){



		  $('#new-review').autosize({append: "\n"});



		  var reviewBox = $('#post-review-box');

		  var newReview = $('#new-review');

		  var openReviewBtn = $('#open-review-box');

		  var closeReviewBtn = $('#close-review-box');

		  var ratingsField = $('#ratings-hidden');



		  openReviewBtn.click(function(e)

		  {

		    reviewBox.slideDown(400, function()

		      {

		        $('#new-review').trigger('autosize.resize');

		        newReview.focus();

		      });

		    openReviewBtn.fadeOut(100);

		    closeReviewBtn.show();

		  });



		  closeReviewBtn.click(function(e)

		  {

		    e.preventDefault();

		    reviewBox.slideUp(300, function()

		      {

		        newReview.focus();

		        openReviewBtn.fadeIn(200);

		      });

		    closeReviewBtn.hide();



		  });



		  $('.starrr').on('starrr:change', function(e, value){

		    ratingsField.val(value);

		  });

	});
	$('.mininewpropritabs-btn').on('click', function(){
     $('.mininewpropritabs-btn').removeClass('active');
     $('.mininewpropritabs .mininewpropritab').removeClass('active');
     $(this).addClass('active');
   });
$(function () {
		// customer feedback tabs

	    $(".feednewpropriwrapper .feednewpropritab").click(function() {
		$(".feednewpropriwrapper .feednewtabpropri").removeClass("active").eq($(this).index()).addClass("feednewpropractive");
		$(".feednewprotab_item").hide().eq($(this).index()).fadeIn()
		}).eq(0).addClass("active");

	});
</script>

@endsection
