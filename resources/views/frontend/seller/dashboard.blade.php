 @php
            $PackageInfo = \App\Models\User::where('package', Auth::user()->package)->first();
            $PP = Auth::user()->package_for;
            $Permissions = json_decode($PackageInfo->package_for);
        @endphp
        @extends('frontend.layouts.new_app_1')

@section('content')

    <!-- left and right sides -->
    <section class="dashboard-recover-sub-links-sec" id="dashrecoversublinks">
        <div class="dashboard-recover-two-sides-left-all">
            <div class="dashboard-recover-two-sides-left-all-ul">
                           <ul class="dashboard-recover-two-sides-left-links">
                <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                <!-- <li><a href="{{ route('company.index') }}">{{__('Create Mini Site')}}</a></li> -->
                <li><a href="#">{{__('Mini Sites')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('company.index') }}">{{__('Mini Sites')}}</a></li>
                        <li><a href="{{ route('company.create') }}">{{__('Create Mini Site')}}</a></li>
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                
               
                        
                <li><a href="#">{{__('Products')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('seller.products') }}">{{__('All Products and Guides')}}</a></li>
                        <li><a href="{{ route('seller.products.upload')}}">{{__('Add Product to Sell')}}</a></li>
                        @if(in_array(3,$Permissions))
                        <li><a href="{{ route('seller.guide.upload')}}">{{__('Add Products Guides')}}</a></li>
                        @endif
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                <li><a href="#">{{__('Shopify Store')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{route('shopify.index')}}">{{__('All Shopify Store')}}</a></li>
                        <li><a href="{{route('shopify.create')}}">{{__('Add Shopify Store')}}</a></li>
                        
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                <li><a href="{{route('woocommerce.index')}}">{{__('WooCommerce')}}</a></li>
                <!-- <li><a href="#">{{__('Guides')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('seller.guides') }}">{{__('Guides')}}</a></li>
                        <li><a href="{{ route('seller.guide.upload')}}">{{__('Add Guide')}}</a></li>
                      <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> 
                    </ul>
                </li> -->
                <!-- <li><a href="#">{{__('Inhouse Orders')}}</a></li> -->
                <li><a href="{{ route('total.sales') }}">{{__('Total Sales')}}</a></li>
                <li><a href="#">{{__('Saller Sales via Affiliate Links')}}</a></li>
                <li><a href="{{ route('sellercoupon') }}">{{__('Saller Coupons')}}</a></li>
                <!-- <li><a href="#">{{__('Vendors')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li> -->
                <li><a href="#">{{__('Customer')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('company_suggessions',Auth::user()->id) }}">Suggessions</a></li>
                        <li><a href="{{ route('seller.reviews',Auth::user()->id) }}">Reviews</a></li>
                         <li><a href="{{ route('clientsreport') }}">Clients Report</a></li>
                         @php
                            $domain = (explode(".",request()->getHost()));
                            $shop = App\Models\Shop::where('slug',$domain[0])->first();
                        @endphp
                        @if(in_array(15,$Permissions))
                        <li><a href="{{ route('seller_questions',Auth::user()->id) }}">Seller Questions</a></li>
                        @endif
                        @if(in_array(19,$Permissions))
                        <li><a href="{{ route('clientsreport') }}">Clients Report</a></li>
                        @endif
                    </ul>
                </li>
                <li><a href="#">{{__('Reports')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        @if(in_array(11,$Permissions))
                        <li><a href="{{ route('club_info') }}">Club Info</a></li>
                        @endif
                        @if(in_array(6,$Permissions))
                        <li><a href="{{ route('company_warranties', Auth::user()->id) }}">Warranty Request </a></li>
                        @endif
                        <li><a href="{{ route('sellingreport') }}">Selling Report</a></li>
                    </ul>
                </li>
                <!-- <li><a href="#">{{__('Pricing')}} </a></li>
                <li><a href="#">{{__('Bussines Setting')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li>
                <li><a href="#">{{__('E-commerce Setup')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li> -->
                @if(in_array(13,$Permissions))
                <li>
                    <a href="{{ route('support_ticket.index') }}" class="{{ areActiveRoutesHome(['support_ticket.index'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Support Ticket')}}
                        </span>
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{ route('marketing') }}" class="{{ areActiveRoutesHome(['marketing'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Email Marketing')}}
                        </span>
                    </a>
                </li>
                 <li class="">
                            <a class="nav-link" href="{{ route('inboxseller') }}">
                              
                                <span class="menu-title">Support Messages</span>
                            </a>
                </li>
                
                <li><a href="#">{{__('Log Activity')}}</a></li>
                
            </ul>


            </div>
        </div>
        <div class="dashboard-recover-res-sublinks-cross">
            <i class="fa fa-times" onclick="hidedashrecoversublinks()"></i>
        </div>
    </section>
    <!-- end res sub mnu -->
    <section class="dashboard-recover-burdger">
        <div class="dashboard-recover-burdger-page">
            <div class="dashboard-recover-burdger-bars">
                <i class="fa fa-bars" onclick="showdashrecoversublinks()"></i>
            </div>
        </div>
    </section>
    <section class="dashboard-recover-two-sides-sec">
        <div class="dashboard-recover-two-sides-sec-page">
            <div class="dashboard-recover-two-sides-data">
                @include('frontend.inc.seller_side_nav_new')
                <!-- right side -->
                <div class="dashboard-recover-two-sides-right">
                    <div class="dashboard-recover-two-sides-right-all">
                        <div class="dashboard-recover-two-sides-right-head">
                            <h2>Dashboard</h2>
                            <p>The Update about the Support Tickets</p>
                        </div>
                        <div class="dashboard-recover-two-sides-inner-data">
                            <div class="dashboard-recover-two-sides-inner-left">

                                <div class="dashboard-recover-two-sides-inner-manage-data">
                                    <div class="dashboard-recover-two-sides-inner-manage-detail">
                                        <div class="dashboard-recover-two-sides-inner-manage-img">
                                            <img src="{{ asset('frontend/images/cart.png') }}">
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-head">
                                            <h4>PRODUCTS</h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-para">
                                            <p>Number of products on sale</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-link">
                                            <a href="{{ route('seller.products') }}">Manage Products <i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-manage-detail">
                                        <div class="dashboard-recover-two-sides-inner-manage-img">
                                            <img src="{{ asset('frontend/images/heart.png') }}">
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-head">
                                            <h4>GUIDES</h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-para">
                                            <p>Number of qr guides on line</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-link">
                                            <a href="{{ route('seller.guides') }}">Manage Guides <i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-manage-detail">
                                        <div class="dashboard-recover-two-sides-inner-manage-img">
                                            <img src="{{ asset('frontend/images/doc.png') }}">
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-head">
                                            <h4>WARRANTY</h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-para">
                                            <p>Number of active warranty</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-link">
                                            <a href="{{ route('company_warranties', Auth::user()->id) }}">See Warranty Reports <i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="dashboard-recover-two-sides-inner-moreinfo-data">
                                    <div class="dashboard-recover-two-sides-inner-moreinfo-detail">
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-text">
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-head">
                                                <h4><?php $getvisit= App\Models\Shop::where('user_id', Auth::user()->id)->select('slug')->get();
                                                $finalget2 = 0;
                                                if(!empty($getvisit)){
                                                 foreach($getvisit as $finalget){
                                                   
                                                   $finalget2 = DB::table('shetabit_visits')->where('url', 'http://'.$finalget->slug.'.myqrguide.com')->whereDate('created_at', DB::raw('CURDATE()'))->count();
                                                    
                                                 }
                                                 echo $finalget2;
                                                 } else {
                                                     echo $finalget2;
                                                 }
                                                 
                                                 ?></h4>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <p>Total today visitors</p>
                                            </div>
                                            <!--<div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">-->
                                            <!--    <a href="#">More info <i class="fa fa-angle-right"></i></a>-->
                                            <!--</div>-->
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-img">
                                            <img src="{{ asset('frontend/images/document.png') }}">
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-moreinfo-detail">
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-text">
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-head">
                                                <h4>{{ App\Models\Order::where('user_id', Auth::user()->id)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count() }}</h4>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <p>Total ordered been added today</p>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <a href="#">More info <i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-img">
                                            <img src="{{ asset('frontend/images/process.png') }}">
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-moreinfo-detail">
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-text">
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-head">
                                                <h4>{{App\Models\Shop::where('user_id', Auth::user()->id)->count()}}</h4>
                                            </div>

                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <p>QR code</p>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <a href="{{ route('company.index')}}">{{ request()->getHost() }} <i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div>

                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-img-1">
                                            <a href="{{ route('company.allqrcode.download', Auth::user()->id) }}" target="_blank"><img src="{{ asset('frontend/images/qr.png') }}"></a>
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-moreinfo-detail">
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-text">
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-head">
                                                <h4><?php $getvisit= App\Models\Shop::where('user_id', Auth::user()->id)->select('slug')->get();
                                                $finalget2 = 0;
                                                if(!empty($getvisit)){
                                                 foreach($getvisit as $finalget){
                                                   
                                                   $finalget2 = DB::table('shetabit_visits')->where('url', 'http://'.$finalget->slug.'.myqrguide.com')->count();
                                                    
                                                 }
                                                 echo $finalget2;
                                                } else {
                                                    echo $finalget2;
                                                }
                                                 ?></h4>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <p>Total weekly visitors</p>
                                            </div>
                                            <!--<div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">-->
                                            <!--    <a href="#">More info <i class="fa fa-angle-right"></i></a>-->
                                            <!--</div>-->
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-img">
                                            <img src="{{ asset('frontend/images/document.png') }}">
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-moreinfo-detail">
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-text">
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-head">
                                                <h4>07</h4>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <p>Total products on wishlist</p>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <a href="#">More info <i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-img">
                                            <img src="{{ asset('frontend/images/process.png') }}">
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-moreinfo-detail">
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-text">
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-head">
                                              
                                                <h4>{{ App\Models\User::where('slug',Auth::user()->id)->where('user_type','support')->count();}}</h4>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <p>Total registered clients</p>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <a href="{{ route('clientsreport') }}">More info <i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-img">
                                            <img src="{{ asset('frontend/images/users.png') }}">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="dashboard-recover-two-sides-inner-right">
                                <div class="dashboard-recover-two-sides-inner-right-data">
                                    <div class="dashboard-recover-two-sides-inner-right-detail">
                                        <div class="dashboard-recover-two-sides-inner-right-detail-par">
                                            <p>Total Tickets Open</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-hed">
                                            <h4>{{ $wordCount = \DB::table('tickets')->where('user_id', '=', Auth::user()->id)
            ->count(); }}</h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-btn">
                                            <a href="{{ url('/support_ticket') }}">See Open Tickets</a>
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-right-detail">
                                        <div class="dashboard-recover-two-sides-inner-right-detail-par">
                                            <p>Total active mini sites</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-hed">
                                            <h4>{{ $wordCount = \DB::table('shops')->where('user_id', '=', Auth::user()->id)
            ->count(); }}</h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-btn">
                                            <a href="{{ route('company.create') }}">Create Mini Site</a>
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-right-detail inner-right-total-margin">
                                        <div class="dashboard-recover-two-sides-inner-right-detail-par">
                                            <p>Total registered users</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-hed">
                                            <h4>{{ $wordCount = \DB::table('users')->where('user_type','=','customer')->where('slug', '=', Auth::user()->id)->count(); }}</h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-btn">
                                            <a style="padding: 4px 24px;" href="{{ url('/clients-report') }}">See User Info</a>
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-right-detail inner-right-total-margin">
                                        <div class="dashboard-recover-two-sides-inner-right-detail-par">
                                            <p>Total daily visitors</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-hed">
                                            <h4><?php $getvisit= App\Models\Shop::where('user_id', Auth::user()->id)->select('slug')->get();
                                            $finalget2 = 0;
                                            if(!empty($getvisit)){
                                                 foreach($getvisit as $finalget){
                                                   
                                                   $finalget2 = DB::table('shetabit_visits')->where('url', 'http://'.$finalget->slug.'.myqrguide.com')->whereDate('created_at', DB::raw('CURDATE()'))->count();
                                                    
                                                 }
                                                 echo $finalget2;
                                                 } else {
                                                     echo $finalget2;
                                                 }
                                                 ?>
                                            </h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-btn">
                                            <a style="padding: 4px 9px;" href="{{ url('/clients-report') }}">View Visitors Data</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="dashboard-recover-two-sides-inner-right-supports">
                                    <div class="dashboard-recover-two-sides-inner-right-suport-btn" style="margin-top: 30px;">
                                        <a style="padding: 15px 82px;" href="{{ url('/support_ticket') }}">Contact Support</a>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-right-suport-btn" style="margin-top: 30px;">
                                        <a style="padding: 15px 70px;" href="{{ url('/inboxseller') }}">Live Chat With Team</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- purchase history -->
    <section class="dashboard-recover-purchase-history-sec">
        <div class="dashboard-recover-purchase-history-page">
            <div class="dashboard-recover-purchase-history-sec-head">
                <h2>{{__('Purchase History')}}</h2>
            </div>
            <div class="dashboard-recover-purchase-history-head-bottom">
                <div class="dashboard-recover-purchase-history-head-bottom-left">
                    <p>Latest Purchase (Showing 01 to {{count($orders)}} of {{count($orders)}} Purchase)</p>
                </div>
                <div class="dashboard-recover-purchase-history-head-bottom-right">
                    <label>Sort By:</label>
                    <input type="text" name="" placeholder="Date Created">
                </div>
            </div>
            <div class="dashboard-recover-purchase-history-data">
                <table class="dashboard-recover-purchase-history-data-table">
                <tr>
                    <th>{{__('ID')}}</th>
                    <th>{{__('Date')}}</th>
                    <th>{{__('Amount')}}</th>
                    <th>{{__('Delivery Status')}}</th>
                    <th>{{__('Payment Status')}}</th>
                    <th>{{__('Option')}}</th>
                </tr>
                @if (count($orders) > 0)
                    @foreach ($orders as $key => $order)
                        <tr>
                            <td><a href="#{{ $order->code }}">#{{ $order->code }}</a></td>
                            <td>{{ date('d-m-Y', $order->date) }}</td>
                            <td class="dashboard-recover-purchase-history-data-td-color">{{ single_price($order->grand_total) }}</td>
                            @php
                                $status = '';
                                if($order->orderDetails->first()){
                                    $status = $order->orderDetails->first()->delivery_status;
                                }
                                //$status = $order->orderDetails->first()->delivery_status;
                                // print_r($order->orderDetails->first());
                                // $status = $order->delivery_status;

                            @endphp
                            <td class="dashboard-recover-purchase-history-data-td-btn-1">
                                <form class="" action="{{ route('orders.seller_update_delivery_status') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="order_id" value="{{ $order->id }}" id="orderid" />
                                    <input type="hidden" name="status" value="{{ ($status == 'deliverd') ? 'pending' : 'deliverd'; }}"  />
                                    <button class="btn btn-{{ ($status == 'deliverd') ? 'success' : 'primary'; }}" type="submit">{{ ucfirst(str_replace('_', ' ', $status)) }} </button>
                                </form>
                                
                            </td>
                            <!-- <td class="dashboard-recover-purchase-history-data-td-btn-2"><button>Complete</button></td> -->
                            <td>
                                <form class="" action="{{ route('orders.seller_update_payment_status') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="order_id" value="{{ $order->id }}" id="orderid" />
                                    <input type="hidden" name="status" value="{{ ($order->payment_status == 'paid') ? 'unpaid' : 'paid'; }}"  />
                                    <button class="btn btn-{{ ($order->payment_status == 'paid') ? 'success' : 'primary'; }}" type="submit">{{ $order->payment_status }} </button>
                                </form>
                            </td>
                            <td>
                                <div class="dashboard-recover-dropout">
                                    <button class="dashboard-recover-more">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </button>
                                    <ul class="dashboard-recover-more-ul">
                                        <li>
                                            <button>{{__('Order Details')}}</button>
                                        </li>
                                        <li>
                                            <a href="{{ route('customer.invoice.download', $order->id) }}" class="dropdown-item">{{__('Download Invoice')}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </table>
            </div>
        </div>
    </section>

@endsection
