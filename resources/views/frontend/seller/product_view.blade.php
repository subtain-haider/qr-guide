@extends('frontend.layouts.new_app_1')

@section('content')

    <section class="dashboard-recover-sub-links-sec" id="dashrecoversublinks">
        <div class="dashboard-recover-two-sides-left-all">
            <div class="dashboard-recover-two-sides-left-all-ul">
                <ul class="dashboard-recover-two-sides-left-links">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Product <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Inhouse Orders</a></li>
                    <li><a href="#">Total Sales</a></li>
                    <li><a href="#">Saller Sales via Affiliate Links</a></li>
                    <li><a href="#">Vendors <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Customer <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Reports <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Pricing </a></li>
                    <li><a href="#">Bussines Setting <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">E-commerce Setup <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Suppot Ticket</a></li>
                    <li><a href="#">Log Activity</a></li>
                </ul>
                
            </div>
        </div>
        <div class="dashboard-recover-res-sublinks-cross">
            <i class="fa fa-times" onclick="hidedashrecoversublinks()"></i>
        </div>
    </section>
    <!-- end res sub mnu -->
    <section class="dashboard-recover-burdger">
        <div class="dashboard-recover-burdger-page">
            <div class="dashboard-recover-burdger-bars">
                <i class="fa fa-bars" onclick="showdashrecoversublinks()"></i>
            </div>
        </div>
    </section>
    <section class="product-listing-page-sec">
        <div class="product-listing-page-wdth">
            <div class="product-listing-page-data">

                @include('frontend.inc.seller_side_nav_new')

                <div class="product-listing-page-right">
                    <div class="product-listing-page-right-all">
                        <div class="product-listing-page-right-all-head">
                            <h2>{{__('PRODUCT VIEW')}}</h2>
                        </div>
                        <div class="product-listing-page-right-table-area">
                            <div class="product-listing-page-right-table-2-manage product_view">
                                <table class="product-listing-page-right-table-2">
                                    <tr>
                                        <td class="product-listing-page-right-table-2-img" colspan="2">
                                            <h5 class="text-center">{{ __('General') }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __('Name') }} :</h5>
                                        </td>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __($product->name) }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __('Category') }} :</h5>
                                        </td>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __($product->category->name) }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __('Sub Category') }} :</h5>
                                        </td>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __($product->subcategory->name) }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __('Sub Sub Category') }} :</h5>
                                        </td>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __($product->subsubcategory->name) }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __('Brand') }} :</h5>
                                        </td>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>@if($product->brand != null){{ __($product->brand->name) }}@endif</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __('Unit') }} :</h5>
                                        </td>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __($product->unit) }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __('Tags') }} :</h5>
                                        </td>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __($product->tags) }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="product-listing-page-right-table-2-img">
                                            <h5>{{ __('Images') }} :</h5>
                                        </td>
                                        <td class="product-listing-page-right-table-2-img">
                                            @php
                                                $photos = array();
                                                if(!empty($product->photos)){
                                                    $photos = json_decode($product->photos);
                                                }
                                            @endphp
                                            @foreach($photos as $photo)
                                                <a href="" target="_blank"><img src="{{ asset($photo) }}" width="150"></a>
                                            @endforeach
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section> 
    
@endsection

@section('script')
<script type="text/javascript">
	document.querySelector('.product-listing-page-right-table-2').onclick = ({
        target
    }) => {
        if (!target.classList.contains('product-listing-tb-more')) return
        document.querySelectorAll('.product-listing-tb-dropout.active').forEach(
            (d) => d !== target.parentElement && d.classList.remove('active')
        )
        target.parentElement.classList.toggle('active')
    }
</script>
@endsection