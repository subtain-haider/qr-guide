 @php
            $PackageInfo = \App\Models\User::where('package', Auth::user()->package)->first();
            $PP = Auth::user()->package_for;
            $Permissions = json_decode($PackageInfo->package_for);
        @endphp
        @extends('frontend.layouts.new_app_1')

@section('content')
<style>
    /* The grid: Four equal columns that floats next to each other */
.column {
  float: left;
  width: 25%;
  padding: 10px;
}

/* Style the images inside the grid */
.column img {
  opacity: 0.8;
  cursor: pointer;
}

.column img:hover {
  opacity: 1;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* The expanding image container (positioning is needed to position the close button and the text) */
.container {
  position: relative;
  display: none;
}

/* Expanding image text */
#imgtext {
  position: absolute;
  bottom: 15px;
  left: 15px;
  color: white;
  font-size: 20px;
}

/* Closable button inside the image */
.closebtn {
  position: absolute;
  top: 10px;
  right: 15px;
  color: white;
  font-size: 35px;
  cursor: pointer;
} 
</style>
    <section class="dashboard-recover-sub-links-sec" id="dashrecoversublinks">
        <div class="dashboard-recover-two-sides-left-all">
            <div class="dashboard-recover-two-sides-left-all-ul">
                 <ul class="dashboard-recover-two-sides-left-links">
                <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                <!-- <li><a href="{{ route('company.index') }}">{{__('Create Mini Site')}}</a></li> -->
                <li><a href="#">{{__('Mini Sites')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('company.index') }}">{{__('Mini Sites')}}</a></li>
                        <li><a href="{{ route('company.create') }}">{{__('Create Mini Site')}}</a></li>
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                
               
                        
                <li><a href="#">{{__('Products')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('seller.products') }}">{{__('All Products and Guides')}}</a></li>
                        <li><a href="{{ route('seller.products.upload')}}">{{__('Add Product to Sell')}}</a></li>
                        @if(in_array(3,$Permissions))
                        <li><a href="{{ route('seller.guide.upload')}}">{{__('Add Products Guides')}}</a></li>
                        @endif
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                <li><a href="#">{{__('Shopify Store')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{route('shopify.index')}}">{{__('All Shopify Store')}}</a></li>
                        <li><a href="{{route('shopify.create')}}">{{__('Add Shopify Store')}}</a></li>
                        
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                <li><a href="{{route('woocommerce.index')}}">{{__('WooCommerce')}}</a></li>
                <!-- <li><a href="#">{{__('Guides')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('seller.guides') }}">{{__('Guides')}}</a></li>
                        <li><a href="{{ route('seller.guide.upload')}}">{{__('Add Guide')}}</a></li>
                      <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> 
                    </ul>
                </li> -->
                <!-- <li><a href="#">{{__('Inhouse Orders')}}</a></li> -->
                <li><a href="{{ route('total.sales') }}">{{__('Total Sales')}}</a></li>
                <li><a href="#">{{__('Saller Sales via Affiliate Links')}}</a></li>
                <li><a href="{{ route('sellercoupon') }}">{{__('Saller Coupons')}}</a></li>
                <!-- <li><a href="#">{{__('Vendors')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li> -->
                <li><a href="#">{{__('Customer')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('company_suggessions',Auth::user()->id) }}">Suggessions</a></li>
                        <li><a href="{{ route('seller.reviews',Auth::user()->id) }}">Reviews</a></li>
                         <li><a href="{{ route('clientsreport') }}">Clients Report</a></li>
                         @php
                            $domain = (explode(".",request()->getHost()));
                            $shop = App\Models\Shop::where('slug',$domain[0])->first();
                        @endphp
                        @if(in_array(15,$Permissions))
                        <li><a href="{{ route('seller_questions',Auth::user()->id) }}">Seller Questions</a></li>
                        @endif
                        @if(in_array(19,$Permissions))
                        <li><a href="{{ route('clientsreport') }}">Clients Report</a></li>
                        @endif
                    </ul>
                </li>
                <li><a href="#">{{__('Reports')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        @if(in_array(11,$Permissions))
                        <li><a href="{{ route('club_info') }}">Club Info</a></li>
                        @endif
                        @if(in_array(6,$Permissions))
                        <li><a href="{{ route('company_warranties', Auth::user()->id) }}">Warranty Request </a></li>
                        @endif
                        <li><a href="{{ route('sellingreport') }}">Selling Report</a></li>
                    </ul>
                </li>
                <!-- <li><a href="#">{{__('Pricing')}} </a></li>
                <li><a href="#">{{__('Bussines Setting')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li>
                <li><a href="#">{{__('E-commerce Setup')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li> -->
                @if(in_array(13,$Permissions))
                <li>
                    <a href="{{ route('support_ticket.index') }}" class="{{ areActiveRoutesHome(['support_ticket.index'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Support Ticket')}}
                        </span>
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{ route('marketing') }}" class="{{ areActiveRoutesHome(['marketing'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Email Marketing')}}
                        </span>
                    </a>
                </li>
                 <li class="">
                            <a class="nav-link" href="{{ route('inboxseller') }}">
                              
                                <span class="menu-title">Support Messages</span>
                            </a>
                </li>
                
                <li><a href="#">{{__('Log Activity')}}</a></li>
                
            </ul>
                
            </div>
        </div>
        <div class="dashboard-recover-res-sublinks-cross">
            <i class="fa fa-times" onclick="hidedashrecoversublinks()"></i>
        </div>
    </section>
    <!-- end res sub mnu -->
    <section class="dashboard-recover-burdger">
        <div class="dashboard-recover-burdger-page">
            <div class="dashboard-recover-burdger-bars">
                <i class="fa fa-bars" onclick="showdashrecoversublinks()"></i>
            </div>
        </div>
    </section>
    <section class="product-listing-page-sec">
        <div class="product-listing-page-wdth">
            <div class="product-listing-page-data">

                @include('frontend.inc.seller_side_nav_new')

                <div class="product-listing-page-right">
                    <div class="product-listing-page-right-all">
                        <div class="product-listing-page-right-table-area">
                            <div class="product-listing-page-right-table-2-manage">
                                <form class="" action="{{ route('seller.profile.update') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-box bg-white mt-4">
                                        <div class="product-listing-page-right-all-head">
                                            <h2>{{__('Manage Profile')}}</h2>
                                        </div>
                                        <div class="form-box-content p-3">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{__('Photo')}}</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <!--<div class="w-100 d-flex align-items-center justify-content-center flex-column" id="photos">-->
                                                    <!--</div>-->
                                                    <img src="{{ asset(Auth::user()->avatar_original) }}" width="200" height="200" style="margin-bottom:10px"/>
                                                    <label class="btn btn-primary">
                                                        <i class="fa fa-image"></i> Upload Image<input type="file" style="display: none;" name="photo">
                                                    </label>
                                                    <!--<input type="file" id="myfile" name="photo"><br><br>-->
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{__('Your Name')}}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control mb-3" placeholder="{{__('Your Name')}}" name="name" value="{{ Auth::user()->name }}">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{__('Your Email')}}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="email" class="form-control mb-3" placeholder="{{__('Your Email')}}" name="email" value="{{ Auth::user()->email }}" disabled>
                                                </div>
                                            </div>
                                        
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{__('Your Password')}}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="password" class="form-control mb-3" placeholder="{{__('New Password')}}" name="new_password">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{__('Confirm Password')}}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="password" class="form-control mb-3" placeholder="{{__('Confirm Password')}}" name="confirm_password">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Paypal Client Id</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="password" class="form-control mb-3" id="show" placeholder="Paypal Client Id" name="paypal_c_id" value="{{ Auth::user()->paypal_c_id }}">
                                                    <i class="fa fa-eye" onclick="show()" style="position: absolute; right: 25px; top: 10px;"></i>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Paypal Secret Id</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="password" class="form-control mb-3" id="show1" placeholder="Paypal Secret Id" name="paypal_s_id" value="{{ Auth::user()->paypal_s_id }}">
                                                    <i class="fa fa-eye" onclick="show1()" style="position: absolute; right: 25px; top: 10px;"></i>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Stipe Client Id</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="password" class="form-control mb-3" id="show2" placeholder="Stipe Client Id" name="stripe_c_id" value="{{ Auth::user()->stripe_c_id }}">
                                                    <i class="fa fa-eye" onclick="show2()" style="position: absolute; right: 25px; top: 10px;"></i>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Strpe Secret Id</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="password" class="form-control mb-3" id="show3" placeholder="Stipe Secret Id" name="stripe_s_id" value="{{ Auth::user()->stripe_s_id }}">
                                                    <i class="fa fa-eye" onclick="show3()" style="position: absolute; right: 25px; top: 10px;"></i>
                                                </div>
                                            </div>
                                           <!-- The grid: four columns -->
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Select Order Template</label>
                                                </div>
                                                <div class="col-md-10">
                                                    
                                              <div class="column">
                                                <img src="{{asset('img/temp-1.png')}}" alt="img" data-name="temp-1" onclick="window.open(this.src, '_blank');" style="width:100px">
                                              </div>
                                              <div class="column">
                                                  <img src="{{asset('img/temp-2.png')}}" alt="img" data-name="temp-2" onclick="window.open(this.src, '_blank');" style="width:100px">
                                              </div>
                                                </div>
                                            </div>
                                            <input type="hidden" class="form-control mb-3" name="template" value="" id="template">
                                            <!-- The expanding image container -->
                                            <div class="container">
                                              <!-- Close the image -->
                                              <span onclick="this.parentElement.style.display='none'" class="closebtn">&times;</span>
                                            
                                              <!-- Expanded image -->
                                              <img id="expandedImg" style="width:200px">
                                            
                                              <!-- Image text -->
                                              <div id="imgtext"></div>
                                            </div>
                                            <div class="text-right mt-4">
                                                <button type="submit" class="btn btn-info">{{__('Update Profile')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section> 
    
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() { 
        $("#photos").spartanMultiImagePicker({
            fieldName:        'photo',
            maxCount:         1,
            rowHeight:        '200px',
            groupClassName:   'w-100',
            maxFileSize:      '',
            dropFileLabel : "Drop Here",
            onExtensionErr : function(index, file){
                console.log(index, file,  'extension err');
                alert('Please only input png or jpg type file')
            },
            onSizeErr : function(index, file){
                console.log(index, file,  'file size too big');
                alert('File size too big');
            }
        });
    });
</script>
<script>
    function myFunction(imgs) {
        $imgName = imgs.getAttribute("data-name");
        console.log($imgName);
        document.getElementById('template').value = $imgName;
  // Get the expanded image
  var expandImg = document.getElementById("expandedImg");
  // Get the image text
  var imgText = document.getElementById("imgtext");
  // Use the same src in the expanded image as the image being clicked on from the grid
  expandImg.src = imgs.src;
  // Use the value of the alt attribute of the clickable image as text inside the expanded image
  imgText.innerHTML = imgs.alt;
  // Show the container element (hidden with CSS)
  expandImg.parentElement.style.display = "block";
}
</script>
<script>
function show() {
  var x = document.getElementById("show");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function show1() {
  var x = document.getElementById("show1");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function show2() {
  var x = document.getElementById("show2");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function show3() {
  var x = document.getElementById("show3");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>
@endsection