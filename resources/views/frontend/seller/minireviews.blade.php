@extends('frontend.layouts.new_app_1')

<style type="text/css">
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
	.sugession-page-sec{
	display: flex;
	justify-content: center;
	padding: 50px 0px;
}
.sugession-page-wdth{
	width: 85%;
}
#suges-customers {
  font-family: 'Play';
  border-collapse: collapse;
  width: 100%;
}

#suges-customers td, #suges-customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#suges-customers tr:nth-child(even){background-color: #f2f2f2;}

#suges-customers tr:hover {background-color: #ddd;}

#suges-customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #1099B1;
  color: white;
}
#suges-customers td button{
	border: none;
	outline: none;
	padding: 4px 30px;
	border-radius: 0px;
	
	font-size: 14px;
	font-family: 'Play';
}
#suges-customers td .suges-customers-btn-1{
	background-color: #000;
	color: #fff;
}
#suges-customers td .suges-customers-btn-2{
	background-color: red;
	color: #fff;
}
</style>

@section('content')
	@php
            $PackageInfo = \App\Models\User::where('package', Auth::user()->package)->first();
            $PP = Auth::user()->package_for;
            $Permissions = json_decode($PackageInfo->package_for);
        @endphp
@section('content')
<section class="dashboard-recover-sub-links-sec" id="dashrecoversublinks">
        <div class="dashboard-recover-two-sides-left-all">
            <div class="dashboard-recover-two-sides-left-all-ul">
                           <ul class="dashboard-recover-two-sides-left-links">
                <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                <!-- <li><a href="{{ route('company.index') }}">{{__('Create Mini Site')}}</a></li> -->
                <li><a href="#">{{__('Mini Sites')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('company.index') }}">{{__('Mini Sites')}}</a></li>
                        <li><a href="{{ route('company.create') }}">{{__('Create Mini Site')}}</a></li>
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                
               
                        
                <li><a href="#">{{__('Products')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('seller.products') }}">{{__('All Products and Guides')}}</a></li>
                        <li><a href="{{ route('seller.products.upload')}}">{{__('Add Product to Sell')}}</a></li>
                        @if(in_array(3,$Permissions))
                        <li><a href="{{ route('seller.guide.upload')}}">{{__('Add Products Guides')}}</a></li>
                        @endif
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                <li><a href="#">{{__('Shopify Store')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{route('shopify.index')}}">{{__('All Shopify Store')}}</a></li>
                        <li><a href="{{route('shopify.create')}}">{{__('Add Shopify Store')}}</a></li>
                        
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                <li><a href="{{route('woocommerce.index')}}">{{__('WooCommerce')}}</a></li>
                <!-- <li><a href="#">{{__('Guides')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('seller.guides') }}">{{__('Guides')}}</a></li>
                        <li><a href="{{ route('seller.guide.upload')}}">{{__('Add Guide')}}</a></li>
                      <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> 
                    </ul>
                </li> -->
                <!-- <li><a href="#">{{__('Inhouse Orders')}}</a></li> -->
                <li><a href="{{ route('total.sales') }}">{{__('Total Sales')}}</a></li>
                <li><a href="#">{{__('Saller Sales via Affiliate Links')}}</a></li>
                <li><a href="{{ route('sellercoupon') }}">{{__('Saller Coupons')}}</a></li>
                <!-- <li><a href="#">{{__('Vendors')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li> -->
                <li><a href="#">{{__('Customer')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('company_suggessions',Auth::user()->id) }}">Suggessions</a></li>
                        <li><a href="{{ route('seller.reviews',Auth::user()->id) }}">Reviews</a></li>
                         <li><a href="{{ route('clientsreport') }}">Clients Report</a></li>
                         @php
                            $domain = (explode(".",request()->getHost()));
                            $shop = App\Models\Shop::where('slug',$domain[0])->first();
                        @endphp
                        @if(in_array(15,$Permissions))
                        <li><a href="{{ route('seller_questions',Auth::user()->id) }}">Seller Questions</a></li>
                        @endif
                        @if(in_array(19,$Permissions))
                        <li><a href="{{ route('clientsreport') }}">Clients Report</a></li>
                        @endif
                    </ul>
                </li>
                <li><a href="#">{{__('Reports')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        @if(in_array(11,$Permissions))
                        <li><a href="{{ route('club_info') }}">Club Info</a></li>
                        @endif
                        @if(in_array(6,$Permissions))
                        <li><a href="{{ route('company_warranties', Auth::user()->id) }}">Warranty Request </a></li>
                        @endif
                        <li><a href="{{ route('sellingreport') }}">Selling Report</a></li>
                    </ul>
                </li>
                <!-- <li><a href="#">{{__('Pricing')}} </a></li>
                <li><a href="#">{{__('Bussines Setting')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li>
                <li><a href="#">{{__('E-commerce Setup')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li> -->
                @if(in_array(13,$Permissions))
                <li>
                    <a href="{{ route('support_ticket.index') }}" class="{{ areActiveRoutesHome(['support_ticket.index'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Support Ticket')}}
                        </span>
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{ route('marketing') }}" class="{{ areActiveRoutesHome(['marketing'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Email Marketing')}}
                        </span>
                    </a>
                </li>
                 <li class="">
                            <a class="nav-link" href="{{ route('inboxseller') }}">
                              
                                <span class="menu-title">Support Messages</span>
                            </a>
                </li>
                
                <li><a href="#">{{__('Log Activity')}}</a></li>
                
            </ul>


            </div>
        </div>
        <div class="dashboard-recover-res-sublinks-cross">
            <i class="fa fa-times" onclick="hidedashrecoversublinks()"></i>
        </div>
    </section>
    <!-- end res sub mnu -->
    <section class="dashboard-recover-burdger">
        <div class="dashboard-recover-burdger-page">
            <div class="dashboard-recover-burdger-bars">
                <i class="fa fa-bars" onclick="showdashrecoversublinks()"></i>
            </div>
        </div>
    </section>
	<section class="sugession-page-sec">
	    @include('frontend.inc.seller_side_nav_new')
	<div class="sugession-page-wdth">
	    <div class="spt-inners-pg-1" style="text-align: center;">
            <h3>CUSTOMERS REVIEWS</h3>
        </div>   
		<div class="sugession-page-data">
			<table class="table table-striped table-bordered demo-dt-basic" id="suges-customers" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <!-- <th>{{__('Product')}}</th> -->
                    <th>{{__('Product Owner')}}</th>
                    <th>{{__('Customer')}}</th>
                    <th>{{__('Rating')}}</th>
                    <th>{{__('Comment')}}</th>
                    <th>{{__('Published')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($reviews as $key => $review)
                    @if ($review != null && $review != null)
                        <tr>
                            <td>{{$key+1}}</td>
                             <!--<td><a href="#" target="_blank">{{ __($review->name) }}</a></td> -->
                            <td>{{ $review->added_by }}</td>
                            <td>{{ $review->name }} ({{ $review->email }})</td>
                            <td>
                             @foreach(range(1,5) as $i)
										                <span class="fa-stack" style="width:1em">
										                    @if($review->rating  > 0)
										                        @if($review->rating < 1)
										                            <i class="fa fa-star-half-o"></i>
										                        @elseif($review->rating  >= 1)
										                            <i class="fa fa-star"></i>
										                        @endif
										                    @else
										                    <i class="fa fa-star-o"></i>
										                    @endif

										                    @php $review->rating--; @endphp
										                </span>
										            @endforeach
                          </div>
                        </td>
                            <td>{{ $review->comment }}</td>
                            <td><label class="switch">
                                <input onchange="update_published(this)" value="{{ $review->rid }}" type="checkbox" <?php if($review->rstatus == 1) echo "checked";?> >
                                <span class="slider round"></span></label></td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
		</div>
	</div>
</section>

@endsection



@section('script')
    <script type="text/javascript">
        function update_published(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('reviews.sellerupdatePublished') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Published reviews updated successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }
    </script>
@endsection