@extends('frontend.layouts.new_app_1')

@section('content')

    <section class="addpro-page-form-sec">
		<div class="addpropage">
			<h2 class="text-center">{{__('Add Guide')}}</h2>
			<div class="addpro-page-form-sec-head">
				<h2>{{__('Product Information')}}</h2>
			</div>
			<div class="addnewpropriwrapper">
		    	<div class="addnewpropriwrapper-data">
		    		<div class="addnewpropriwrapper-detail-1">
					    <div class="addnewpropritabs">
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('General')}}</button></span>
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Images')}}</button></span>
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Videos')}}</button></span>
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Meta Tags')}}</button></span>
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Price')}}</button></span>
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('External link')}}</button></span>
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Description')}}</button></span>
					         <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Spacification')}}</button></span> 
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('PDF Specification')}}</button></span>        
					    </div>
					</div>
					<div class="addnewpropriwrapper-detail-2">
					    <div class="addnewtab_content">
							<form class="" action="{{route('products.store')}}" onsubmit="return product_form_submit()" method="POST" enctype="multipart/form-data" id="choice_form">
								<!-- General-->
								<div class="addnewprotab_item">
									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="addpro-page-form-general-data">
												@csrf
												<input type="hidden" name="product_type" value="guide">
												<input type="hidden" name="added_by" value="seller">
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Guide Name')}}</label>
													<div class="addpro-page-form-img-general-2">
													<input type="text" class="form-control" name="name" required placeholder="{{__('Product Name')}}">
													</div>
												</div>
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('SKU')}}</label>
													<div class="addpro-page-form-img-general-2">
													<input type="text" class="form-control" name="sku" required placeholder="{{__('SKU')}}">
													</div>
												</div>
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('ASIN')}}</label>
													<div class="addpro-page-form-img-general-2">
													<input type="text" class="form-control" name="asin" required placeholder="{{__('ASIN')}}">
													</div>
												</div>
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Category')}}</label>
													<div class="addpro-page-form-img-general-2">
													<select class="form-control demo-select2-placeholder" name="category_id" id="category_id" required>
														<option value="">{{__('Please Select')}}</option>
														@foreach($categories as $category)
															<option value="{{$category->id}}">{{__($category->name)}}</option>
														@endforeach
													</select>
													</div>
												</div>
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Subcategory')}}</label>
													<div class="addpro-page-form-img-general-2">
														<select class="form-control demo-select2-placeholder" name="subcategory_id" id="subcategory_id" data-live-search="true"></select>
													</div>
												</div>
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Sub Subcategory')}}</label>
													<div class="addpro-page-form-img-general-2">
														<select class="form-control demo-select2-placeholder" name="subsubcategory_id" id="subsubcategory_id" data-live-search="true"></select>
													</div>
												</div>
												<!--<div class="form-group addpro-page-form-general-data-all">-->
												<!--	<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Brand (Optional)')}}</label>-->
												<!--	<div class="addpro-page-form-img-general-2">-->
												<!--		<select class="form-control demo-select2-placeholder" data-placeholder="Select a brand" id="brands" name="brand_id" data-live-search="true">-->
												<!--		<option value="-1">Please select</option>-->
												<!--	</select>-->
												<!--	</div>-->
												<!--</div>-->
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Unit')}}</label>
													<div class="addpro-page-form-img-general-2">
													    <select class="form-control" name="unit" required>
													        <option disable>Select Unit (e.g. KG, Pc etc)</option>
													        <option value='kg'>KG</option>
													        <option value='pc'>PC</option>
													    </select>
													</div>
												</div>
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">Tags</label>
													<div class="addpro-page-form-img-general-2">
													<input type="text" class="form-control" name="tags[]" placeholder="Type & hit enter" data-role="tagsinput"  placeholder="Type to add a tag">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Images -->
								<div class="addnewprotab_item">
									
									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="addpro-page-form-img-data-1-head">
												<p>Main Images(1080X1080)</p>
											</div>
											<div class="addpro-page-form-img-data-1">
												
												<div class="addpro-page-form-img-data-1-detail-all">

													<div id="photos"></div>

												</div>
											</div>
											<!-- thumbnail -->
											<div class="addpro-page-form-img-data-2">
												<div class="addpro-page-form-img-data-1-head">
													<p>Thumnnail Image(290X300)</p>
												</div>
												<div class="addpro-page-form-img-data-2-detail-all">

													<div id="thumbnail_img"></div>

												</div>
											</div>
										</div>
									</div>
									
								</div>
								<!-- Videos -->
								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="form-group addpro-page-form-img-data">
												<label for="" class="form-control-label addpro-page-form-img-detail-1">{{__('Video Provider')}}</label>
												<div class="addpro-page-form-img-detail-2">
												<select class="form-control demo-select2-placeholder" id="select-country" data-live-search="true" name="video_provider">
													<option value="youtube">{{__('Youtube')}}</option>
													<option value="dailymotion">{{__('Dailymotion')}}</option>
													<option value="vimeo">{{__('Vimeo')}}</option>
												</select>

												</div>
											</div>
											<div class="form-group addpro-page-form-img-data">
												<label for="" class="form-control-label addpro-page-form-img-detail-1">{{__('Video URL')}}</label>
												<div class="addpro-page-form-img-detail-2">
												<input type="text" class="form-control mb-3" name="video_link" placeholder="{{__('Video link')}}">
												<a href="javascript:;" onclick="formguide()">You don’t have a designed guide ? Press here to request one</a>
												</div>
											</div>
										</div>
									</div>
								
											
										
								</div>
								<!-- Meta Tags -->
								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="form-group addpro-page-form-meta-data-all">
												<label for="" class="form-control-label addpro-page-form-meta-detail-1">{{__('Meta Title')}}</label>
												<div class="addpro-page-form-meta-detail-2">
												<input type="text" name="meta_title" required placeholder="Meta Title">
												</div>
											</div>
											<div class="form-group addpro-page-form-meta-data-all">
												<label for="" class="form-control-label addpro-page-form-meta-detail-1">{{__('Description')}}</label>
												<div class="addpro-page-form-meta-detail-2">
												<textarea name="meta_description" rows="8" class="form-control mb-3"></textarea>
												</div>
											</div>
											<div class="addpro-page-form-meta-data-2">
												<div class="addpro-page-form-meta-data-1-head">
													<p>Meta Image</p>
												</div>
												<div class="addpro-page-form-meta-data-2-detail-all">

													<div id="meta_photo"></div>

												</div>
											</div>
										</div>
									</div>
								
								</div>
								<!--Price-->
								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="form-group addpro-page-form-price-data-all">
												<label for="" class="form-control-label addpro-page-form-price-detail-1">Unit price</label>
												<div class="addpro-page-form-price-detail-2">
												<input type="number" min="0" value="0" step="0.01" placeholder="{{__('Unit price')}}" name="unit_price" class="form-control" required>
												</div>
												<div class="addpro-page-form-price-detail-3"></div>
											</div>
											<div class="form-group addpro-page-form-price-data-all">
												<label for="" class="form-control-label addpro-page-form-price-detail-1">Purchase price</label>
												<div class="addpro-page-form-price-detail-2">
												<input type="number" min="0" value="0" step="0.01" placeholder="{{__('Purchase price')}}" name="purchase_price" class="form-control" required>
												</div>
												<div class="addpro-page-form-price-detail-3">
												
												</div>
											</div>
											<div class="form-group addpro-page-form-price-data-all">
												<label for="" class="form-control-label addpro-page-form-price-detail-1">Tax</label>
												<div class="addpro-page-form-price-detail-2">
												<input type="number" min="0" value="0" step="0.01" placeholder="{{__('Tax')}}" name="tax" class="form-control" required>
												</div>
												<div class="addpro-page-form-price-detail-3">
													<select class="form-control demo-select2-placeholder" id="select-price" data-live-search="true" name="tax_type">
														<option value="amount">$</option>
														<option value="percent">%</option>
													</select>
												</div>
											</div>
											<div class="form-group addpro-page-form-price-data-all">
												<label for="" class="form-control-label addpro-page-form-price-detail-1">Discount</label>
												<div class="addpro-page-form-price-detail-2">
												<input type="number" min="0" value="0" step="0.01" placeholder="{{__('Discount')}}" name="discount" class="form-control" required>
												</div>
												<div class="addpro-page-form-price-detail-3">
													<select class="form-control demo-select2-placeholder" id="select-price" data-live-search="true">
														<option value="amount">$</option>
														<option value="percent">%</option>
													</select>
												</div>
											</div>
										</div>
									</div>
								
								</div>
								<!-- External link -->
								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="form-group addpro-page-form-img-data">
												<label for="" class="form-control-label addpro-page-form-img-detail-1">{{__('Product URL')}}</label>
												<div class="addpro-page-form-img-detail-2">
												<input type="text" class="form-control mb-3" name="product_link" placeholder="{{__('Product link')}}">
												</div>
											</div>
										</div>
									</div>
								
								</div>
								<!-- Description -->
								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="container-fluid">
												<div class="row">
													<div class="container">
														<div class="row">
															<div class="col-lg-12 nopadding">
																<textarea id="txtEditor" class="editor" name="description"></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								
								</div>
								<!-- Spacifications -->
								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="container-fluid">
												<div class="row">
													<div class="container">
														<div class="row">
															<div class="col-lg-12 nopadding">
																<textarea id="txtEditor" class="editor" name="spacification"></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							
								<!--PDF Specification  -->
								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="form-group addpro-page-form-pdf-data-all">
												<div class="form-group addpro-page-form-pdf-data-1" id="pdfconatiner">
													<div class="form-group addpro-page-form-pdf-data-2" id="pdfshowsec">
														<label for="" class="form-control-label addpro-page-form-pdf-detail-1">PDF Specification</label>
														<div class="addpro-page-form-pdf-detail-2">
														<select class="form-control demo-select2-placeholder" id="select-pdf" data-live-search="true">
															<option data-tokens="china">English</option>
															<option data-tokens="malayasia">Bangla</option>
															<option data-tokens="malayasia">Arabic</option>
														</select>
														</div>
														<!-- <div class="form-group addpro-page-form-general-data-all">
															<div class="addpro-page-form-img-general-2">
																<input type="file" class="form-control" name="pdf" required>
															</div>
														</div> -->
														<div class="addpro-page-form-pdf-detail-3">
															<input type="file" class="form-control p-1" name="pdf" required>
														</div>
													</div>
												</div>
												<div class="addpro-page-form-pdf-detail-4">
													<p class="addpro-page-form-pdf-addmore" id="pdfaddmore">Add More</p>
												</div>
											</div>
										</div>
									</div>
								
								</div>
								<!-- end PDF Specification -->

								<div class="panel-footer text-right mt-3">
									<button type="submit" name="button" class="btn btn-info upload-product">Save</button>
								</div>
							</form>
					    </div>
					</div>
				</div>
			</div>
			
			<form id="the-form" method="post">
			
				<input type="hidden" id="notify" name="notify" value="Want new video for guide">

				<input type="hidden" id="userid" name="userid" value="{{auth()->user()->id}}">
				<input type="hidden" id="username" name="username" value="{{auth()->user()->name}}">
				<input type="hidden" id="useremail" name="useremail" value="{{auth()->user()->email}}">
				<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

			</form>
		</div>
	</section>    

@endsection

@section('script')
<script type="text/javascript">

	$(document).ready(function() { 

        $('#submit-the-form').click(function() {
            $('#the-form').submit();
        });
    
    });



function formguide(){
    
event.preventDefault();
        var customerData = {};
        customerData._token = $('#token').val();
        customerData.notify = $('#notify').val();
        customerData.userid = $('#userid').val();
        customerData.username = $('#username').val();
        customerData.useremail = $('#useremail').val();
        console.log(customerData);
        $.ajax({
            url: '{{ route("products.guiderequest") }}',
            type: 'POST',
            data: customerData,
            dataType: 'JSON',
            success: function (data) {
            
               swal("Your guide request has been submitted","Team will contact you ASAP","success");
            }

        })
    
}


    var category_name = "";
    var subcategory_name = "";
    var subsubcategory_name = "";

    var category_id = null;
    var subcategory_id = null;
    var subsubcategory_id = null;
    $(function () {
        // tabs
        $(".addnewpropriwrapper .addnewpropritab").click(function() {
			$(".addnewpropriwrapper .addnewpropritab button").removeClass('active');
			$(this).children().addClass('active');
            $(".addnewpropriwrapper .addnewtabpropri").removeClass("active").eq($(this).index()).addClass("addnewpropractive");
            $(".addnewprotab_item").hide().eq($(this).index()).fadeIn()
	    }).eq(0).children().addClass("active");


        $(document).ready(function(){
            $('#hideimgform1').click(function(){
                $('.hidedivform1').hide();
            });
            $('#hideimgform2').click(function(){
                $('.hidedivform2').hide();
            });
            $('#hideimgform3').click(function(){
                $('.hidedivform3').hide();
            });
            $('#hideimgform4').click(function(){
                $('.hidedivform4').hide();
            });
            $('#hideimgform5').click(function(){
                $('.hidedivform5').hide();
            });
            $('#hideimgformthumb').click(function(){
                $('.hidedivformthumb').hide();
            });
            $('#hideimgformmeta').click(function(){
                $('.hidedivformmeta').hide();
            });


            $('#category_id').on('change', function() {
                get_subcategories_by_category();
            });

            $('#subcategory_id').on('change', function() {
                get_subsubcategories_by_subcategory();
            });

            $('#subsubcategory_id').on('change', function() {
                get_brands_by_subsubcategory();
            });
            var jq = $.noConflict();
            $("#photos").spartanMultiImagePicker({
                fieldName:        'photos[]',
                maxCount:         10,
                rowHeight:        '200px',
                groupClassName:   'col-md-4 col-sm-4 col-xs-6',
                maxFileSize:      '',
                dropFileLabel : "Drop Here",
                onExtensionErr : function(index, file){
                    console.log(index, file,  'extension err');
                    alert('Please only input png or jpg type file')
                },
                onSizeErr : function(index, file){
                    console.log(index, file,  'file size too big');
                    alert('File size too big');
                }
            });

            $("#thumbnail_img").spartanMultiImagePicker({
                fieldName:        'thumbnail_img',
                maxCount:         1,
                rowHeight:        '200px',
                groupClassName:   'col-md-4 col-sm-4 col-xs-6',
                maxFileSize:      '',
                dropFileLabel : "Drop Here",
                onExtensionErr : function(index, file){
                    console.log(index, file,  'extension err');
                    alert('Please only input png or jpg type file')
                },
                onSizeErr : function(index, file){
                    console.log(index, file,  'file size too big');
                    alert('File size too big');
                }
            });

            $("#meta_photo").spartanMultiImagePicker({
                fieldName:        'meta_img',
                maxCount:         1,
                rowHeight:        '200px',
                groupClassName:   'col-md-4 col-sm-4 col-xs-6',
                maxFileSize:      '',
                dropFileLabel : "Drop Here",
                onExtensionErr : function(index, file){
                    console.log(index, file,  'extension err');
                    alert('Please only input png or jpg type file')
                },
                onSizeErr : function(index, file){
                    console.log(index, file,  'file size too big');
                    alert('File size too big');
                }
            });
            $(function () {
            $('.demo-select2').select2();
        });
        });
        

		$(".upload-product").click(function() {
			$('#choice_form input, #choice_form select, #choice_form textarea').each(function() {
				var attr = $(this).attr('required');
				if (typeof attr !== 'undefined' && attr !== false && $(this).val() == '') {
					$('.addnewpropritab button').removeClass('active');
					$('.addnewprotab_item').hide();
					$(this).closest('.addnewprotab_item').show();
					var Buttonindex = $(this).closest('.addnewprotab_item').index();
					$('.addnewpropritab').eq(Buttonindex).children().addClass('active');
					return false;
				}
			});
		});
    });

    // $(document).ready(function() {
    //     $("#txtEditor").Editor();
    // });

    document.getElementById("pdfaddmore").onclick = function() {
        var container = document.getElementById("pdfconatiner");
        var section = document.getElementById("pdfshowsec");
        container.appendChild(section.cloneNode(true));
    }

    function get_subcategories_by_category(){
            var category_id = $('#category_id').val();
            $.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
                $('#subcategory_id').html(null);
                for (var i = 0; i < data.length; i++) {
                    $('#subcategory_id').append($('<option>', {
                        value: data[i].id,
                        text: data[i].name
                    }));
                    $('.demo-select2').select2();
                }
                get_subsubcategories_by_subcategory();
            });
        }

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
		    $('#subsubcategory_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		    get_brands_by_subsubcategory();
		});
	}
	function get_brands_by_subsubcategory(){
		var subsubcategory_id = $('#subsubcategory_id').val();
		$.post('{{ route('subsubcategories.get_brands_by_subsubcategory') }}',{_token:'{{ csrf_token() }}', subsubcategory_id:subsubcategory_id}, function(data){
		    $('#brand_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#brand_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		});
	}

	function product_form_submit() {
		var $fileUpload = $("input[name='photos[]']");
        if (parseInt($fileUpload.get(0).files.length) > 8){
            alert("You are only allowed to upload a maximum of 8 files");
            return false;
        }
        return true;
    }

    $("input[data-role=tagsinput]").tagsinput();
</script>
@endsection