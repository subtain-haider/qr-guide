@extends('frontend.layouts.new_app_1')

@section('content')
<style>
    .switch {
      position: relative;
      display: inline-block;
      width: 60px;
      height: 34px;
    }

    .switch input {
      opacity: 0;
      width: 0;
      height: 0;
    }

    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }

    .slider:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }

    input:checked + .slider {
      background-color: #2196F3;
    }

    input:focus + .slider {
      box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
      border-radius: 34px;
    }

    .slider.round:before {
      border-radius: 50%;
    }
    </style>
    @php
    $RelatedProductsOptions = $ProductsOptions = $LanguageOptions = $ProductsOptions1 ='';
    $tabs = explode(',', $shop->tabs);
    @endphp
    @foreach (\App\Models\Language::all() as $key => $language)
        @php
            $LanguageOptions .= '<option value="'.$language->id.'">'.$language->name.'</option>';
        @endphp
    @endforeach

    @foreach(App\Models\Product::where('user_id', Auth::user()->id)->where('product_type','guide')->where('status','1')->get() AS $product)
        @php
            $ProductsOptions .= '<option value="'.$product->id.'" data-edit="'.route('seller.products.edit', encrypt($product->id)).'">'.str_replace("\n", "", addslashes($product->name)).'</option>';
        @endphp
    @endforeach
    @foreach(App\Models\Product::where('user_id', Auth::user()->id)->where('product_type','product')->where('status','1')->get() AS $product)
        @php
            $ProductsOptions1 .= '<option value="'.$product->id.'" data-edit="'.route('seller.products.edit', encrypt($product->id)).'">'.str_replace("\n", "", addslashes($product->name)).'</option>';
        @endphp
    @endforeach
    @foreach(App\Models\Category::all() AS $category)
        @php
            $RelatedProductsOptions .= "<option value='".$category->id."'>".$category->name."</option>";
        @endphp
    @endforeach
    @php
        $PackageInfo = \App\Models\User::where('package', Auth::user()->package)->first();
        $PP = Auth::user()->package_for;
        $Permissions = json_decode($PackageInfo->package_for);
        $coupons = \App\Models\Coupon::where('user_id', Auth::user()->id)->get();
    @endphp

    <section class="mini-dashboard-sec">
		<div class="mini-dashboard-sec-page">
			<div class="mini-dashboard-sec-data">

				<div class="mini-dashboard-sec-left-side">
					<div class="mini-dashboard-left-side-data">
							<div class="minidashpropritabs">
                            <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="main-product" data-tab="main-product"><button style="border-top: 2px solid #1A9FB6;" class="minidashpropritabs-btn"><i class="fa fa-product-hunt mr-2"></i>{{__('Add Guide')}}</button></span>
                            <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="related" data-tab="related"><button style="border-top: 2px solid #1A9FB6;" class="minidashpropritabs-btn"><i class="fa fa-product-hunt mr-2"></i>{{__('Related Products')}}</button></span>
                            <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="info" data-tab="info"><button style="border-top: 2px solid #1A9FB6;" class="minidashpropritabs-btn"><i class="fa fa-building-o mr-2"></i>{{__('Add company Profile')}}</button></span>
					        <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="video" data-tab="video"><button class="minidashpropritabs-btn"><i class="fa fa-youtube-play mr-2"></i>{{__('Add a video')}}</button></span>
					        <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="company" data-tab="company"><button class="minidashpropritabs-btn"><i class="fa fa-picture-o mr-2"></i>{{__('Add Slides')}}</button></span>
					        <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="pdf" data-tab="pdf"><button class="minidashpropritabs-btn"><i class="fa fa-file-pdf-o mr-2"></i>{{__('Add a Pdf, Guide & Language')}}</button></span>
					        <!-- <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="title" data-tab="title"><button class="minidashpropritabs-btn"><i class="fa fa-comments mr-2"></i>{{__('Change minisite titles text')}}</button></span>
                            <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="external_links" data-tab="external_links"><button class="minidashpropritabs-btn"><i class="fa fa-link mr-2"></i>{{__('Add Amazon, Ebay, Alibaba product links')}}</button></span> -->
					        <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="club" data-tab="club"><button class="minidashpropritabs-btn"><i class="fa fa-users mr-2"></i>{{__('Edit a text of own client club')}}</button></span>

                            <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="warranty" data-tab="warranty"><button class="minidashpropritabs-btn"><i class="fa fa-users mr-2"></i>{{__('Edit Warranty text and Form')}}</button></span>

					        <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="sm-support" data-tab="sm-support"><button class="minidashpropritabs-btn"><i class="fa fa-facebook mr-2"></i>{{__('Social media support')}}</button></span>
					        <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="qa" data-tab="qa"><button class="minidashpropritabs-btn"><i class="fa fa-comments-o mr-2"></i>{{__('Frequently Asked Questions')}}</button></span>
					        <!-- <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="p-info" data-tab="p-info"><button class="minidashpropritabs-btn"><i class="fa fa-newspaper-o mr-2"></i>{{__('Customer Reviews')}}</button></span> -->
                            <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="reviews" data-tab="reviews"><button class="minidashpropritabs-btn"><i class="fa fa-newspaper-o mr-2"></i>{{__('Customer Reviews')}}</button></span>
                            <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="coupon" data-tab="coupon"><button class="minidashpropritabs-btn"><i class="fa fa-gift mr-2"></i>{{__('Banners')}}</button></span>
					        <span class="minidashpropritab" draggable="true" ondragstart="drag(event)" id="coupons" data-tab="coupons"><button class="minidashpropritabs-btn"><i class="fa fa-gift mr-2"></i>{{__('Coupons')}}</button></span>

					    </div>
					</div>
				</div>
                
                    <div class="mini-dashboard-sec-right-side">
                        <div class="mini-dashboard-right-side-data">
                            <div class="mini-dashboard-right-side-page">
                                <div class="minidashtab_content">
                                    <!--tab 1-->
                                           <div class="mini-dashboard-profile-data-head">
                                                    <h2>{{__('DECORATE YOUR MINISITE WITH FEW SIMPLE CLICKS')}}</h2>
                                                <p>{{__('DRAG A SECTION FROM THE LEFT SIDE AND APPLY HERE')}}</p>
                                                <img style="width: 10%;" src="https://1.bp.blogspot.com/-TIWCWcTxQBU/XbCHJXsCjEI/AAAAAAAABrk/oaLfk3Du3LYS_meUvfV2gBMIC4f1pIY6QCPcBGAYYCw/s640/tenor.gif">
                                                <p>{{__('WebSite Title')}}</p>
                                                </div>
                                            <div class="col-md-12 p-3">
                                             <form class="" action="{{route('slug.change', $shop->id)}}" id="save_slug" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="row">
                                                    <div class="col-md-10"><div class="text-left"><b>Minisite URL</b></div></div>
                                                    <div class="col-md-10">
                                                        <div class="d-flex align-items-center justify-content-center flex-column">
                                                           <input type="text" name="slug" maxlength="20" placeholder="Minisite URL" value="{{$shop->slug}}" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">                                         
                                                        <div class="d-flex align-items-center justify-content-center flex-column"> 
                                                            <button type="submit" name="urlupdate" value="urlupdate" class="btn btn-success" id="save-button">Update URL</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                               </div> 
                                    <form class="" action="{{route('company.update', $shop->id)}}" id="save_form" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="tab_sorting" id="tabs_sorting" required>
                                    <div class="minidashprotab_item">
                                        <div class="mini-dashboard-profile-data">
                                            <div class="mini-dashboard-profile-data-head">
                                             
                                                <div class="col-md-12 p-3">
                                                    <div class="row">
                                                        <!-- <div class="col-md-2">
                                                            <label>Company Title</label>
                                                        </div> -->
                                                         <div class="col-md-10"><div class="text-left"><b>Minisite Title</b></div></div>
                                                        <div class="col-md-12">
                                                            <div class="d-flex align-items-center justify-content-center flex-column">

                                                               <input type="text" name="title" placeholder="WebSite Title" value="{{$shop->title}}" required="required" class="form-control">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-10"><div class="text-left"><b>Select background color</b></div></div>
                                                        <div class="col-md-12">
                                                            <div class="d-flex align-items-center justify-content-center flex-column">
                                                               <input type="color" style="height:40px;" name="minibgcolor" placeholder="#000000" value="{{$shop->minibgcolor}}" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="conteudo border rounded p-5 task-container ui-sortable ui-state-highlight ui-droppable" id="conteudo" ondrop="drop(event)" ondragover="allowDrop(event)" style="min-height: 100px;">
                                        <span class="p-5 text-center h2 drag-and-drop-text ui-sortable-handle d-none">Drag and Drop here</span>
                                        @php
                                        $CloseBtn = '<a href="javascript:void(0)" data-spartanindexremove="0" style="background: rgb(237, 60, 32);border-radius: 3px;width: 30px;height: 30px;line-height: 30px;text-align: center;text-decoration: none;color: rgb(255, 255, 255);margin-top: -15px;margin-right: -15px;" class="spartan_remove_row pull-right"><i class="fa fa-times"></i></a>';
                                        $CloseBtn .= '<a href="javascript:void(0)" data-spartanindexremove="0" style="background: rgb(237, 60, 32);border-radius: 3px;width: 30px;height: 30px;line-height: 30px;text-align: center;text-decoration: none;color: rgb(255, 255, 255);margin-top: -15px;margin-right: 5px;" class="pull-right todo-task "><i class="fa fa-arrows"></i></a>';
                                        @endphp
                                        @foreach($tabs as $tab)
                                            @php
                                                $added_by = ' ui-droppable '.$tab.'_block rounded border mb-5';
                                            @endphp
                                            @if($tab == 'info')
                                               <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content p-3">
                                                        <div class="row">
                                                            <h5 style="padding: 0px 12px;">Add company Profile</h5>
                                                        </div>
                                                        @if($shop->logo != null)
                                                            <div class="row">
                                                                <label class="col-lg-2 control-label">{{__('Current Logo')}}</label>
                                                                <div class="col-lg-10">
                                                                    <img src="{{asset($shop->logo)}}" width="400">
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>{{__('Logo')}}</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column">
                                                                    <input type="file" name="logo" class="form-control py-1 mb-3" data-file-holder="logo-holder" data-file-holder-name="logo_temp" accept="image/*">
                                                                    <div id="logo-holder" class="d-none">
                                                                        @if($shop->logo != null)
                                                                            <input value="{{asset($shop->logo)}}" name="logo_temp" class="tmp-files" />
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>{{__('Address')}}</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <input type="text" class="form-control mb-3" placeholder="Address" name="address" value="{{$shop->address}}">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>{{__('Contact No.')}}</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="input-group mb-3">
                                                                    <input type="text" class="form-control" placeholder="(000) 0000000" name="whatsapp_number" value="{{$shop->contact_no}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>{{__('Company Email.')}}</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="input-group mb-3">
                                                                    <input type="text" class="form-control" placeholder="Email" name="company_email" value="{{$shop->company_email }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>{{__('Company Website.')}}</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="input-group mb-3">
                                                                    <input type="text" class="form-control" placeholder="Website link" name="company_website" value="{{$shop->company_website }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>{{__('Description')}}</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column highlight info2">
                                                                    <textarea class="editor" name="description" value="{{$shop->description }}">{!! $shop->description !!}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($tab == 'video')
                                                 <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content p-3">
                                                        <div class="row">
                                                            <h5 style="padding: 0px 12px;">Add a video</h5>
                                                        </div>
                                                        <div class="row">
                                                            <label class="col-lg-2 control-label">{{__('Video Provider')}}</label>
                                                            <div class="col-lg-10 mb-3">
                                                                <select class="form-control demo-select2" name="video_provider" id="video_provider">
                                                                    <option value="youtube" @if('youtube' == $shop->video_provider) selected @endif>{{__('Youtube')}}</option>
                                                                    <option value="dailymotion" @if('dailymotion' == $shop->video_provider) selected @endif>{{__('Dailymotion')}}</option>
                                                                    <option value="vimeo" @if('vimeo' == $shop->video_provider) selected @endif>{{__('Vimeo')}}</option>
                                                                    <option value="upload_manual" @if('upload_manual' == $shop->video_provider) selected @endif>{{__('Upload Video')}}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row link_video_block">
                                                            <label class="col-lg-2 control-label">{{__('Video Link')}}</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" class="form-control" name="video_link" placeholder="https://www.youtube.com/watch?v=xxxxxxx" value="{{$shop->video_link}}">
                                                            </div>
                                                        </div>
                                                        @if($shop->video_provider == 'upload_manual')
                                                            <div class="row link_video_block">
                                                                <label class="col-lg-2 control-label">{{__('Current Video')}}</label>
                                                                <div class="col-lg-10">
                                                                    <video  width="400" height="400" autoplay loop muted>
                                                                        <source src="{{ asset($shop->video_link) }}" type="video/mp4">
                                                                        Your browser does not support HTML video.
                                                                    </video>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <div class="row d-none manual_video_block">
                                                            <label class="col-lg-2 control-label">{{__('Video')}}</label>
                                                            <div class="col-lg-10">
                                                                <input type="file" name="video" class="form-control py-1 mb-3" data-file-holder="video-holder" data-file-holder-name="video_temp" accept="video/*">
                                                                <div id="video-holder" class="d-none">
                                                                    @if($shop->video_provider == 'upload_manual')
                                                                        <input value="{{ asset($shop->video_link) }}" name="video_temp" class="tmp-files" />
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <div class="row">
                                                        <label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">                                                           </i> ) {{__('Select Animation')}}</label> 
                                                        <div class="col-lg-4"> 
                                                        <select class="form-control demo-select2 " name="animatevideo" id="animatevideo" ><option value="">None</option><option value="fade-up" {{ ($shop->animatevideo=="fade-up")? "selected" : "" }}>fade-up</option><option value="fade-down" {{ ($shop->animatevideo=="fade-down")? "selected" : "" }}>fade-down</option><option value="fade-right" {{ ($shop->animatevideo=="fade-right")? "selected" : "" }}>fade-right</option><option value="fade-left" {{ ($shop->animatevideo=="fade-left")? "selected" : "" }}>fade-left</option></select> 
                                                        </div>
                                                        </div>
                                    <div class="row">
                                        <div class="col-md-4">Show to logged in user.</div>
                                        <div class="col-md-8">
                                            <label class="switch">
                                                <input type="checkbox" name="video_check" {{ ($shop->video_check=="on")? "checked='checked'" : "" }}>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                            </div>
                                                    
                                                </div>
                                            @elseif($tab == 'company')
                                                <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content p-3">
                                                        <div class="row">
                                                            <h5 style="padding: 0px 12px;">Add Slides</h5>
                                                        </div>
                                                        @if($shop->sliders != null)
                                                            <div class="row">
                                                                <label class="col-lg-2 control-label">{{__('Current Slides')}}</label>
                                                                <div class="col-lg-10">
                                                                    @foreach (json_decode($shop->sliders) as $key => $slide)
                                                                    <img src="{{asset($slide)}}" width="400">
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                            
                                                        @endif
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>{{__('Slides')}}</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column">
                                                                    @if($shop->sliders != null)
                                                                    <?php
                                                                    foreach (json_decode($shop->sliders) as $key => $slide)

                                                                        echo '<input type="hidden" name="banner[]" class="tmp-files form-control py-1 mb-3" value="'. $slide. '">';
                                                                    ?>
                                                                    @endif
                                                                    <input type="file" multiple name="banner[]" id="file-2" class="form-control py-1 mb-3" data-file-holder="company-image-holder" data-file-holder-name="company_picture_temp" accept="image/*">
                                                                    <div id="company-image-holder" class="d-none">
                                                                        @if($shop->video_provider == 'upload_manual')
                                                                            <input value="{{ asset($shop->video_link) }}" name="company_picture_temp" class="tmp-files" />
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <div class="row">
                                                        <label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">                                                           </i> ) {{__('Select Animation')}}</label> 
                                                        <div class="col-lg-4"> 
                                                        <select class="form-control demo-select2 " name="animateslides" id="animateslides" ><option value="">None</option><option value="fade-up" {{ ($shop->animateslides=="fade-up")? "selected" : "" }}>fade-up</option><option value="fade-down" {{ ($shop->animateslides=="fade-down")? "selected" : "" }}>fade-down</option><option value="fade-right" {{ ($shop->animateslides=="fade-right")? "selected" : "" }}>fade-right</option><option value="fade-left" {{ ($shop->animateslides=="fade-left")? "selected" : "" }}>fade-left</option></select> 
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($tab == 'pdf' and $shop->pdf != null)
                                                @php
                                                $pdfs = json_decode($shop->pdf);
                                                @endphp
                                                <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content pdfs-box p-3">
                                                        <div class="row">
                                                            <h5 style="padding: 0px 12px;">Add a Pdf, Guide & Language</h5>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>{{__('PDF Tab Title')}}</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column">
                                                                    <input type="text" class="form-control mb-3" placeholder="{{__('PDF Tab Title')}}" name="pdf_title" value="{{$shop->pdf_title}}"> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>{{__('Add More info For Your Guide')}}</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column pdf2">
                                                                    <textarea name="pdf_text" class="form-control mb-3">{{$shop->pdf_title}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @foreach($pdfs as $pdf)
                                                            <div class="row">
                                                                <label class="col-lg-2 control-label">{{__('PDF Specification')}}</label>
                                                                <div class="col-lg-2">
                                                                    @if($pdf->lng != null)
                                                                        {{ \App\Models\Language::where('id', $pdf->lng)->first()->name }}
                                                                    @endif
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <a href="{{asset($pdf->path)}}">View PDF</a>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        <div class="row">
                                                            <label class="col-lg-2 control-label">{{__('PDF Specification')}}</label>
                                                            <div class="col-lg-2">
                                                                <select class="form-control demo-select2" name="lng[]" id="video_provider">{!! $LanguageOptions !!}</select>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <input type="file" class="form-control py-1" placeholder="{{__('PDF')}}" name="pdf[]" data-file-holder="pdf-holder" data-file-holder-name="pdf_temp[]" accept="application/pdf">
                                                                @if($shop->pdf != null)
                                                                    @foreach($pdfs as $pdfKey => $pdf)
                                                                        <div @if($pdfKey== 0)id="pdf-holder"@else id="pdf-holder{{$pdfKey}}" @endif class="d-none">
                                                                            <input value="{{asset($pdf->path)}}" name="pdf_temp[]" class="tmp-files" />
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button type="button" class="btn btn-success btn-base-1" id="add-more-pdf">Add More Files</button>
                                                            </div>
                                                        </div>
                                                         <div class="row">
                                                        <label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                                          </i> ) {{__('Select Animation')}}</label> 
                                                        <div class="col-lg-4"> 
                                                        <select class="form-control demo-select2 " name="animatepdf" id="animatepdf" ><option value="">None</option><option value="fade-up" {{ ($shop->animatepdf=="fade-up")? "selected" : "" }}>fade-up</option><option value="fade-down" {{ ($shop->animatepdf=="fade-down")? "selected" : "" }}>fade-down</option><option value="fade-right" {{ ($shop->animatepdf=="fade-right")? "selected" : "" }}>fade-right</option><option value="fade-left" {{ ($shop->animatepdf=="fade-left")? "selected" : "" }}>fade-left</option></select> 
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($tab == 'main-product')
                                                <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content p-3">
                                                        <div class="row">
                                                            <h5 style="padding: 0px 12px;">Select Guide</h5>
                                                        </div>
                                                        <div class="row">
                                                            <label class="col-lg-2 control-label">{{__('Select Guide')}}</label>
                                                            <div class="col-lg-4">
                                                                <select class="form-control demo-select2 mb-3" id="main-product" name="product" id="product" required>
                                                                    <option value="">Please Select</option>
                                                                    @foreach(App\Models\Product::where('user_id', Auth::user()->id)->where('product_type','guide')->where('status','1')->get() AS $product)
                                                                        <option value="{{$product->id}}" data-edit="{{route('seller.products.edit', encrypt($product->id))}}" @if($product->id == $shop->product) selected @endif>{{$product->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <a href="" class="btn btn-styled btn-base-1 product-edit-btn" target="_blank">Edit</a>
                                                                <a href="{{ route('seller.guide.upload') }}" class="btn btn-styled btn-base-1 product-edit-btn" target="_blank">Add Guide</a>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                        <label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">                                                           </i> ) {{__('Select Animation')}}</label> 
                                                        <div class="col-lg-4"> 
                                                        <select class="form-control demo-select2 " name="animateguide" id="animateguide" ><option value="">None</option><option value="fade-up" {{ ($shop->animateguide=="fade-up")? "selected" : "" }}>fade-up</option><option value="fade-down" {{ ($shop->animateguide=="fade-down")? "selected" : "" }}>fade-down</option><option value="fade-right" {{ ($shop->animateguide=="fade-right")? "selected" : "" }}>fade-right</option><option value="fade-left" {{ ($shop->animateguide=="fade-left")? "selected" : "" }}>fade-left</option></select> 
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                             @elseif($tab == 'related')
                                                <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content p-3">
                                                        <div class="row">
                                                            <h5 style="padding: 0px 12px;">Related Products</h5>
                                                        </div>
                                                        <div class="row mb-3">
                                                            <label class="col-lg-3 control-label">{{__('Select Max 3 Products')}}</label>
                                                            <div class="col-lg-6">
                                                                @php
                                                                $related_products = json_decode($shop->related_products);
                                                                @endphp
                                                                
                                                                <select class="form-control demo-select2 multiselect" name="related_products[]" id="related_products" multiple>
                                                                        @foreach(App\Models\Product::where('user_id', Auth::user()->id)->where('product_type','product')->where('status',1)->get() AS $product)
                                                                            @if(!empty($related_products))
                                                                            <option value="{{$product->id}}" @if(in_array($product->id, $related_products)) selected @endif>{{$product->name}}</option>
                                                                            @else
                                                                                <option value="{{$product->id}}" >{{$product->name}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                </select>
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                        <label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">                                                           </i> ) {{__('Select Animation')}}</label> 
                                                        <div class="col-lg-4"> 
                                                        <select class="form-control demo-select2 " name="animateproducts" id="animateproducts" ><option value="">None</option><option value="fade-up" {{ ($shop->animateproducts=="fade-up")? "selected" : "" }}>fade-up</option><option value="fade-down" {{ ($shop->animateproducts=="fade-down")? "selected" : "" }}>fade-down</option><option value="fade-right" {{ ($shop->animateproducts=="fade-right")? "selected" : "" }}>fade-right</option><option value="fade-left" {{ ($shop->animateproducts=="fade-left")? "selected" : "" }}>fade-left</option></select> 
                                                        </div>
                                                        </div>
                                                        <div class="row">
                                                         <div class="col-md-4">Show to logged in user.</div>
                                                                <div class="col-md-8">
                                                                    <label class="switch">
                                                                        <input type="checkbox" name="related_check" {{ ($shop->related_check=="on")? "checked='checked'" : "" }}>
                                                                        <span class="slider round"></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        <div>
                                                    </div>
                                                </div>
                                                </div>
                                            @elseif($tab == 'title')
                                                <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content p-3">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>{{__('Company Title')}}</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column">
                                                                    <input type="text" name="title" class="form-control" value="{{$shop->title}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($tab == 'external_links')
                                                <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content p-3">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Amazon, Ebay, Alibaba link</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column">
                                                                    <input type="text" name="link" class="form-control" value="{{$shop->external_link}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            @elseif($tab == 'club')
                                                <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content p-3">
                                                        <div class="row">
                                                            <h5 style="padding: 0px 12px;">Edit a text of own client club</h5>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Club Text</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column">
                                                                    <input type="text" class="form-control" name="club_text" value="{{$shop->club_text}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <div class="row">
                                                        <label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">                                                           </i> ) {{__('Select Animation')}}</label> 
                                                        <div class="col-lg-4"> 
                                                        <select class="form-control demo-select2 " name="animateclub" id="animateclub" ><option value="">None</option><option value="fade-up" {{ ($shop->animateclub=="fade-up")? "selected" : "" }}>fade-up</option><option value="fade-down" {{ ($shop->animateclub=="fade-down")? "selected" : "" }}>fade-down</option><option value="fade-right" {{ ($shop->animateclub=="fade-right")? "selected" : "" }}>fade-right</option><option value="fade-left" {{ ($shop->animateclub=="fade-left")? "selected" : "" }}>fade-left</option></select> 
                                                        </div>
                                                        </div>
                                    <div class="row">
                                        <div class="col-md-4">Show to logged in user.</div>
                                        <div class="col-md-8">
                                            <label class="switch">
                                                <input type="checkbox" name="club_check" {{ ($shop->club_check=="on")? "checked='checked'" : "" }}>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                                    </div>
                                                
                                           @elseif($tab == 'coupon')
                                                <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content p-3">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h2>Banners</h2>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <input type="radio" onclick="myfunction()" id="couponA" name="coupon_active" value="1" {{$shop->c_active == 1 ? 'checked' : ''}}>
                                                                <label for="couponA" class="mr-4"><img src="{{asset("frontend/images/ccoupon.jpg")}}" width="40" height="40"></label>
                                                                <input type="radio" onclick="myfunction()" id="bannerA" name="coupon_active" value="2" {{$shop->c_active == 2 ? 'checked' : ''}}>
                                                                <label for="bannerA" class="mr-4"><img src="{{asset("frontend/images/cbanner.jpg")}}" width="40" height="40"></label>
                                                                <input type="radio" onclick="myfunction()" id="popupA" name="coupon_active" value="3" {{$shop->c_active == 3 ? 'checked' : ''}}> 
                                                                <label for="popupA" class="mr-4"><img src="{{asset("frontend/images/cpopup.jpg")}}" width="40" height="40"></label> 
                                                                <input type="radio" onclick="myfunction()" id="bannerNA" name="coupon_active" value="4" {{$shop->c_active == 4 ? 'checked' : ''}}> 
                                                                <label for="bannerNA" class="mr-4"><img src="{{asset("frontend/images/bn1-main.png")}}" width="40" height="40"></label> 
                                                                <input type="radio" onclick="myfunction()" id="bannerNB" name="coupon_active" value="5" {{$shop->c_active == 5 ? 'checked' : ''}}> 
                                                                <label for="bannerNB" class="mr-4"><img src="{{asset("frontend/images/bn2-main.png")}}" width="40" height="40"></label> 
                                                                <input type="radio" onclick="myfunction()" id="bannerNC" name="coupon_active" value="6" {{$shop->c_active == 6 ? 'checked' : ''}}> 
                                                                <label for="bannerNC" class="mr-4"><img src="{{asset("frontend/images/bn3-main.png")}}" width="40" height="40"></label> 
                                                                <input type="radio" onclick="myfunction()" id="bannerND" name="coupon_active" value="7" {{$shop->c_active == 7 ? 'checked' : ''}}> 
                                                                <label for="bannerND" class="mr-4"><img src="{{asset("frontend/images/bn4-main.png")}}" width="40" height="40"></label> 
                                                                <input type="radio" onclick="myfunction()" id="couponD" name="coupon_active" value="0" {{$shop->c_active == 0 ? 'checked' : ''}}>
                                                                <label for="couponD">None</label>
                                                                <div id="filedscoupon">
                                                                @if($shop->c_active)
                                                                 @if($shop->coupon_text != 'null')
                                                                    @foreach(json_decode($shop->coupon_text) as $coupentxt)
                                                                    <div class="form-group d-flex w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">
                                                                        <div class="col-lg-3 pull-left">
                                                                            <label class="control-label">Field</label>
                                                                        </div>
                                                                        <div class="col-lg-7 pull-left">
                                                                            <input class="form-control" required type="text" maxlength="8" name="coupon_text[]" value="{{$coupentxt}}" placeholder="Label">
                                                                        </div>
                                                                    </div>
                                                                    @endforeach
                                                                @endif
                                                                @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                        <label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">                                                           </i> ) {{__('Select Animation')}}</label> 
                                                        <div class="col-lg-4"> 
                                                          <select class="form-control demo-select2 " name="animatecoupan" id="animatecoupan" ><option value="">None</option><option value="fade-up" {{ ($shop->animatecoupan=="fade-up")? "selected" : "" }}>fade-up</option><option value="fade-down" {{ ($shop->animatecoupan=="fade-down")? "selected" : "" }}>fade-down</option><option value="fade-right" {{ ($shop->animatecoupan=="fade-right")? "selected" : "" }}>fade-right</option><option value="fade-left" {{ ($shop->animatecoupan=="fade-left")? "selected" : "" }}>fade-left</option></select> 
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>


                                             @elseif($tab == 'warranty')
                                                <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content p-3">
                                                        <div class="row">
                                                            <h5 style="padding: 0px 12px;">Edit Warranty text and Form</h5>
                                                        </div>
                                                        <input type="hidden" class="form-control" name="warranty" value="true">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Warranty Text</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column">
                                                                    <input type="text" class="form-control" required="required" name="warranty_text" value="{{$shop->warranty_text}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-8 form-horizontal" id="form">
                                                                @foreach (json_decode($shop->warranty) as $key => $element)
                                                                    @if ($element->type == 'text' || $element->type == 'file')
                                                                        <div class="form-group d-inline-block w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">
                                                                            <input type="hidden" name="type[]" value="{{ $element->type }}">
                                                                            <div class="col-lg-3 pull-left">
                                                                                <label class="control-label">{{ ucfirst($element->type) }}</label>
                                                                            </div>
                                                                            <div class="col-lg-7 pull-left">
                                                                                <input class="form-control" type="text" name="label[]" value="{{ $element->label }}" placeholder="Label">
                                                                            </div>
                                                                            <div class="col-lg-2 pull-left"><i class="fa fa-times" onclick="delete_choice_clearfix(this)"></i></div>
                                                                        </div>
                                                                    @elseif ($element->type == 'select' || $element->type == 'multi_select' || $element->type == 'radio')
                                                                        <div class="form-group d-inline-block w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">
                                                                            <input type="hidden" name="type[]" value="{{ $element->type }}">
                                                                            <input type="hidden" name="option[]" class="option" value="{{ $key }}">
                                                                            <div class="col-lg-3 pull-left">
                                                                                <label class="control-label">{{ ucfirst(str_replace('_', ' ', $element->type)) }}</label>
                                                                            </div>
                                                                            <div class="col-lg-7 pull-left">
                                                                                <input class="form-control" type="text" name="label[]" value="{{ $element->label }}" placeholder="Select Label" style="margin-bottom:10px">
                                                                                <div class="customer_choice_options_types_wrap_child">
                                                                                    @if($element->options != 'null')
                                                                                    @foreach (json_decode($element->options) as $value)
                                                                                        <div class="form-group d-inline-block w-100">
                                                                                            <div class="col-sm-6 col-sm-offset-4 pull-left">
                                                                                                <input class="form-control" type="text" name="options_{{ $key }}[]" value="{{ $value }}" required="">
                                                                                            </div>
                                                                                            <div class="col-sm-2 pull-left"> <i class="fa fa-times" onclick="delete_choice_clearfix(this)"></i></div>
                                                                                        </div>
                                                                                    @endforeach
                                                                                    @endif
                                                                                </div>
                                                                                <button class="btn btn-success pull-right" type="button" onclick="add_customer_choice_options(this)"><i class="glyphicon glyphicon-plus"></i> Add option</button>
                                                                            </div>
                                                                            <div class="col-lg-2 pull-left"><i class="fa fa-times" onclick="delete_choice_clearfix(this)"></i></div>
                                                                        </div>
                                                                        
                                                                    @endif
                                                                @endforeach
                                                                @if(!empty($shop->w_edate))
                                                                    <div class="form-group d-flex w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">
                                                                        <div class="col-lg-6 pull-left">
                                                                            <label class="control-label">Amount of X (Months)</label>
                                                                        </div>
                                                                        <div class="col-lg-5 pull-left">'
                                                                            <input type="number" id="edate" name="edate" value="{{ $shop->w_edate }}" placeholder="Number of Months">
                                                                        </div>
                                                                        <div class="col-lg-1 pull-left">
                                                                            <i class="fa fa-times" onclick="delete_choice_clearfix(this)"></i>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <div class="col-lg-4">
                                    
                                                                <ul class="list-group" id="warranty-fields">
                                                                    <li class="list-group-item btn" style="text-align: left;" data-field="text">{{__('Text Input')}}</li>
                                                                    <li class="list-group-item btn" style="text-align: left;" data-field="select">{{__('Select')}}</li>
                                                                    <li class="list-group-item btn" style="text-align: left;" data-field="multi-select">{{__('Multiple Select')}}</li>
                                                                    <li class="list-group-item btn" style="text-align: left; display:none" data-field="radio">{{__('Tags')}}</li>
                                                                    <li class="list-group-item btn" style="text-align: left;" data-field="file">{{__('File')}}</li>
                                                                    <li class="list-group-item btn" style="text-align: left;" data-field="date">{{__('Date')}}</li>
                                                                </ul>
                                    
                                                            </div>
                                                        </div>
                                                        <div class="form-box bg-white mt-4 col-12">
                                                            <div class="form-box-content p-3">
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <label>{{__('Content')}}</label>
                                                                    </div>
                                                                    <div class="col-md-10">
                                                                        <div class="mb-3 war2">
                                                                            <textarea class="editor" name="content">{!! $shop->warranty_content !!}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                        <label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">                                                           </i> ) {{__('Select Animation')}}</label> 
                                                        <div class="col-lg-4"> 
                                                        <select class="form-control demo-select2 " name="animatewarranty" id="animatewarranty" ><option value="">None</option><option value="fade-up" {{ ($shop->animatewarranty=="fade-up")? "selected" : "" }}>fade-up</option><option value="fade-down" {{ ($shop->animatewarranty=="fade-down")? "selected" : "" }}>fade-down</option><option value="fade-right" {{ ($shop->animatewarranty=="fade-right")? "selected" : "" }}>fade-right</option><option value="fade-left" {{ ($shop->animatewarranty=="fade-left")? "selected" : "" }}>fade-left</option></select> 
                                                        </div>
                                                        </div>
                                                        '<div class="row">
                                                            <div class="col-md-4">Show to logged in user.</div>
                                                            <div class="col-md-8">
                                                                <label class="switch">
                                                                    <input type="checkbox" name="warranty_check" {{ ($shop->warranty_check=="on")? "checked='checked'" : "" }}>
                                                                    <span class="slider round"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                               
                                             @elseif($tab == 'sm-support')
                                                <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content p-3">
                                                        <div class="row">
                                                            <h5 style="padding: 0px 12px;">Social media support</h5>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Background Color</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column"> 
                                                                    <input type="color" class="form-control" name="social_bgcolor" value="{{$shop->social_bgcolor}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Facebook</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column"> 
                                                                    <input type="text" class="form-control" name="facebook_link" value="{{$shop->facebook}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Twitter</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column"> 
                                                                    <input type="text" class="form-control" name="twitter_link" value="{{$shop->twitter}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Instagram</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column"> 
                                                                    <input type="text" class="form-control" name="instagram_link" value="{{$shop->instagram}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Linkedin</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column"> 
                                                                    <input type="text" class="form-control" name="linkedin_link" value="{{$shop->linkedin}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Youtube</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column"> 
                                                                    <input type="text" class="form-control" name="youtube_link" value="{{$shop->youtube}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Tiktok</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column"> 
                                                                    <input type="text" class="form-control" name="tiktox_link" value="{{$shop->tiktok}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>Whatsapp</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column"> 
                                                                    <input type="text" class="form-control" name="whatsapp" value="{{$shop->whatsapp}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($tab == 'qa' and $shop->questions != null)
                                                @php
                                                $questions = json_decode($shop->questions);
                                                @endphp
                                                <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content p-3">
                                                        <div class="row">
                                                            <h5 style="padding: 0px 12px;">Frequently Asked Questions</h5>
                                                        </div>
                                                        <div class="question_block">
                                                            @foreach($questions as $question)
                                                            <div class="question_answer">
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <label>Question</label>
                                                                    </div>
                                                                    <div class="col-md-10">
                                                                        <div class="d-flex align-items-center justify-content-center flex-column highlight"> 
                                                                            <input type="text" name="question[]" class="form-control py-1 mb-3" value="{{$question->question}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <label>Answer</label>
                                                                    </div>
                                                                    <div class="col-md-10">
                                                                        <div class="d-flex align-items-center justify-content-center flex-column highlight"> 
                                                                            <input type="text" name="answer[]" class="form-control py-1 mb-3" value="{{$question->answer}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 text-right">
                                                                <button type="button" class="btn btn-danger" id="remove_more_question" style="margin-right:10px;">{{ __('Remove') }}</button>
                                                                <button type="button" class="btn btn-success" id="add_more_question">{{ __('Add More') }}</button>
                                                            </div>
                                                        </div>
                                                         <div class="row">
                                                        <label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">                                                           </i> ) {{__('Select Animation')}}</label> 
                                                        <div class="col-lg-4"> 
                                                        <select class="form-control demo-select2 " name="animatefaq" id="animatefaq" ><option value="">None</option><option value="fade-up" {{ ($shop->animatefaq=="fade-up")? "selected" : "" }}>fade-up</option><option value="fade-down" {{ ($shop->animatefaq=="fade-down")? "selected" : "" }}>fade-down</option><option value="fade-right" {{ ($shop->animatefaq=="fade-right")? "selected" : "" }}>fade-right</option><option value="fade-left" {{ ($shop->animatefaq=="fade-left")? "selected" : "" }}>fade-left</option></select> 
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 @elseif($tab == 'coupons')
                                                    <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                '<h2>Coupons</h2>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <input type="radio"  id="couponNA" name="coupon_act" value="1" {{$shop->coupon_act == 1 ? 'checked' : ''}}>
                                                                <label for="couponNA" class="mr-4"><img src="{{asset("frontend/images/bn1-main.png")}}" width="40" height="40"></label>
                                                                <input type="radio"  id="couponNB" name="coupon_act" value="2" {{$shop->coupon_act == 2 ? 'checked' : ''}}>
                                                                <label for="couponNB" class="mr-4"><img src="{{asset("frontend/images/bn2-main.png")}}" width="40" height="40"></label>
                                                                <input type="radio"  id="couponNC" name="coupon_act" value="3" {{$shop->coupon_act == 3 ? 'checked' : ''}}>
                                                                <label for="couponNC" class="mr-4"><img src="{{asset("frontend/images/bn3-main.png")}}" width="40" height="40"></label>
                                                                <input type="radio"  id="couponND" name="coupon_act" value="4" {{$shop->coupon_act == 4 ? 'checked' : ''}}>
                                                                <label for="couponND" class="mr-4"><img src="{{asset("frontend/images/bn4-main.png")}}" width="40" height="40"></label>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 control-label" for="coupon_code">Select Coupon code</label>
                                                                <div class="col-lg-8">
                                                                    '<select class="form-control" name="selectedcoupon" id="selectedcoupon">
                                                                    @foreach($coupons as $coupon)
                                                                      <option value="{{ $coupon->id }}" {{ ($shop->coupon_id == $coupon->id)? "selected" : "" }} >{{ $coupon->code }} / {{ $coupon->type }} / {{ $coupon->discount }}</option>
                                                                    @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                           @elseif($tab == 'reviews')
                                                <div class="minidashprotab_item{{$added_by}}" data-tab="{{$tab}}">{!! $CloseBtn !!}
                                                    <div class="form-box-content p-3">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>{{__('Customer Reviews')}}</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <div class="d-flex align-items-center justify-content-center flex-column">
                                                                    <span class="p-5 text-center d-block h2">Customer Reviews</span>
                                                                    <input type="hidden" class="form-control tmp-files" name="reviews" value="true">
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <div class="row">
                                                        <label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">                                                           </i> ) {{__('Select Animation')}}</label> 
                                                        <div class="col-lg-4"> 
                                                        <select class="form-control demo-select2 " name="animatereviews" id="animatereviews" ><option value="">None</option><option value="fade-up" {{ ($shop->animatereviews=="fade-up")? "selected" : "" }}>fade-up</option><option value="fade-down" {{ ($shop->animatereviews=="fade-down")? "selected" : "" }}>fade-down</option><option value="fade-right" {{ ($shop->animatereviews=="fade-right")? "selected" : "" }}>fade-right</option><option value="fade-left" {{ ($shop->animatereviews=="fade-left")? "selected" : "" }}>fade-left</option></select> 
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <div class="row" style="margin-top:10px;"> 
                                        <div class="col-md-4">Active / Deactive Buy Now Button</div> 
                                        <div class="col-md-8"> 
                                    <label class="switch">
                                            <input type="checkbox" name="buynonw_check" {{ ($shop->related_check=="on")? "checked='checked'" : "" }}>
                                            <span class="slider round"></span>
                                        </label>
                                        </div>
                                    </div>
                                     <div class="row"> 
                                        <div class="col-md-4">Show Footer.</div> 
                                        <div class="col-md-8"> 
                                        
                                        <label for="nofoot">Hide footer</label>
                                                <input type="radio" id="nofoot" name="footer_check" value="no" {{ ($shop->footer_check == "no")? "checked" : "" }} > <br>
                                            <label for="nofoot">Hide footer</label>
                                                <input type="radio" id="nofoot" name="footer_check" value="no" {{ ($shop->footer_check == "no")? "checked" : "" }} > <br>
                                                  <label for="yesfoot">Show Footer</label>
                                                <input type="radio" name="footer_check" id="yesfoot" value="yes" {{ ($shop->footer_check == "yes")? "checked" : "" }} >
                                            
                                               
                                            
                                        </div> 
                                    </div> 
                                    
                                     <div class="row"> 
                                        <div class="col-md-4">Select Font</div> 
                                        <div class="col-md-5"> 
                                            <select class="form-control" name="font" id="font" >
                                              
                                              <option value="">None</option>
                                              <option value="Times New Roman" {{ ($shop->font=="Times New Roman")? "selected" : "" }}>Times New Roman</option>
                                              <option value="Arial" {{ ($shop->font=="Arial")? "selected" : "" }}>Arial</option>
                                              <option value="Miriam" {{ ($shop->font=="Miriam")? "selected" : "" }}>Miriam</option>
                                              <option value="Verdana" {{ ($shop->font=="Verdana")? "selected" : "" }} >Verdana</option>
                                              <option value="Comic Sans MS" {{ ($shop->font=="Comic Sans MS")? "selected" : "" }} >Comic Sans MS</option>
                                              <option value="WildWest" {{ ($shop->font=="WildWest")? "selected" : "" }} >WildWest</option>
                                              <option value="Bedrock" {{ ($shop->font=="Bedrock")? "selected" : "" }} >Bedrock</option>
                                              
                                            </select>
                                            
                                               
                                            
                                        </div> 
                                    </div>
                                    
                                     <div class="row"> 
                                        <div class="col-md-4">Meta tag</div> 
                                        <div class="col-md-5"> 
                                            <input class="form-control" type="text" name="metatag" value="{{$shop->metatag}}" placeholder="Enter Meta tag here...">
                                           
                                        </div> 
                                    </div> 
                                    
                                    <div class="row"> 
                                        <div class="col-md-4">Meta description</div> 
                                        <div class="col-md-5"> 
                                          
                                             <textarea class="form-control" name="metadescription" placeholder="Enter Meta description here..."  rows="3">{!!$shop->metadescription!!}</textarea>
                                           
                                        </div> 
                                    </div> 
                                    <div class="panel-footer text-right" style="margin-top:10px;">
                                            <button style="background: #fc8177;border-color: #fc8177;" type="submit" name="submitbutton" value="submitbutton" class="btn btn-success" id="save-button">{{ __('Submit') }}</button>
                                            <a href="{{route('company.index')}}" class="btn btn-danger" id="cancel-button">{{ __('Cancel') }}</a>
                                            <button type="button" name="button" class="btn btn-info" data-action="{{route('company.confirmation')}}" id="confirmation-button">{{ __('Preview') }}</button>
                                            <button type="submit" name="button" class="btn btn-success" id="save-button">{{ __('Save') }}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                <form class="" action="{{route('company.confirmation')}}" id="confirmation_form" target="_blank" method="POST" enctype="multipart/form-data">
                    @csrf
                </form>
			</div>
		</div>
	</section>

@endsection

@section('script')
<script>
    $(document).ready(function(){
  $('.demo-select2').select2();
});
        var $container = $(".task-container");
        var $task = $('.todo-task');

        // /*$task.draggable({
        //     addClasses: false,
        //     connectToSortable: ".task-container",
        // });*/

        $container.droppable({
            accept: ".todo-task"
        });


        $(".ui-droppable").sortable({
            placeholder: "ui-state-highlight",
            opacity: .5,
            helper: 'original',
            beforeStop: function (event, ui) {
                newItem = ui.item;
                var tabs = [];
                $('.minidashprotab_item').each(function() {
                    tabs.push($(this).attr('data-tab'));
                });
                tabs.join(',');
                $('#tabs_sorting').val(tabs);
            },
            receive: function (event, ui) {

            }
        });

        ///Drag'n Drop functions
        function allowDrop(ev) 
        {
            ev.preventDefault();
        }

        function drag(ev) 
        {
            ev.dataTransfer.setData("text", ev.target.id);
            ev.dataTransfer.effectAllowed = "copy";
        }


        function drop(ev) 
        {
           
                     
            ev.preventDefault();

            var tab = ev.dataTransfer.getData("text");
            var Newtab = document.createElement("div");
            var parent = document.createElement("conteudo");
            var original = document.getElementById(tab);
            var html = '';

            var added_by = ' ui-droppable '+tab+'_block rounded border mb-5';
            var CloseBtn = '<a href="javascript:void(0)" data-spartanindexremove="0" style="background: rgb(237, 60, 32);border-radius: 3px;width: 30px;height: 30px;line-height: 30px;text-align: center;text-decoration: none;color: rgb(255, 255, 255);margin-top: -15px;margin-right: -15px;" class="spartan_remove_row pull-right"><i class="fa fa-times"></i></a>';

            CloseBtn += '<a href="javascript:void(0)" data-spartanindexremove="0" style="background: rgb(237, 60, 32);border-radius: 3px;width: 30px;height: 30px;line-height: 30px;text-align: center;text-decoration: none;color: rgb(255, 255, 255);margin-top: -15px;margin-right: 5px;" class="pull-right todo-task "><i class="fa fa-arrows"></i></a>';
            if(tab == 'info'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                                '<div class="row">' +
                                    '<h5 style="padding: 0px 12px;">Add company Profile</h5>'+
                                '</div>'+
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Logo</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' +
                                            '<input type="file" name="logo" class="form-control py-1 mb-3" data-file-holder="logo-holder" data-file-holder-name="logo_temp" accept="image/*">' +
                                            '<div id="logo-holder" class="d-none"></div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Address</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<input type="text" class="form-control mb-3" placeholder="Address" name="address" value="">' + 
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Contact No.</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="input-group mb-3">' +
                                            '<input type="text" class="form-control" placeholder="(000) 0000000" name="whatsapp_number" value="">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Company Website.</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="input-group mb-3">' +
                                            '<input type="text" class="form-control" placeholder="Website" name="company_website" value="">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Description</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column highlight info1">' + 
                                            '<textarea class="editor" name="description"></textarea>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i> ) {{__('Select Animation')}}</label>' +
                                    '<div class="col-lg-4">' +
                                        '<select class="form-control demo-select2 " name="animateprofile" id="animateprofile"><option value="">None</option><option value="fade-up">fade-up</option><option value="fade-down">fade-down</option><option value="fade-right">fade-right</option><option value="fade-left">fade-left</option></select>' +
                                    '</div>' +
                            '</div>' +
                        '</div>';                
            }else if(tab == 'video'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                                '<div class="row">' +
                                    '<h5 style="padding: 0px 12px;">Add a video</h5>'+
                                '</div>'+
                                '<div class="row">' +
                                    '<label class="col-lg-2 control-label">{{__('Video Provider')}}</label>' +
                                    '<div class="col-lg-10 mb-3">' +
                                        '<select class="form-control demo-select2" name="video_provider" id="video_provider">' +
                                            '<option value="youtube">{{__('Youtube')}}</option>' +
                                            '<option value="dailymotion">{{__('Dailymotion')}}</option>' +
                                            '<option value="vimeo">{{__('Vimeo')}}</option>' +
                                            '<option value="upload_manual">{{__('Upload Video')}}</option>' +
                                        '</select>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row link_video_block">' +
                                    '<label class="col-lg-2 control-label">{{__('Video Link')}}</label>' +
                                    '<div class="col-lg-10">' +
                                        '<input type="text" class="form-control" name="video_link" placeholder="https://www.youtube.com/watch?v=xxxxxxx">' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row d-none manual_video_block">' +
                                    '<label class="col-lg-2 control-label">{{__('Video')}}</label>' +
                                    '<div class="col-lg-10">' +
                                        '<input type="file" name="video" class="form-control py-1 mb-3" data-file-holder="video-holder" data-file-holder-name="video_temp" accept="video/*">' +
                                        '<div id="video-holder" class="d-none"></div>' +
                                    '</div>' +
                                '</div>' +
                                '<label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i> ) {{__('Select Animation')}}</label>' +
                                    '<div class="col-lg-4">' +
                                        '<select class="form-control demo-select2 " name="animatevideo" id="animatevideo"><option value="">None</option><option value="fade-up">fade-up</option><option value="fade-down">fade-down</option><option value="fade-right">fade-right</option><option value="fade-left">fade-left</option></select>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="col-md-4">Show to logged in user.</div>' +
                                        '<div class="col-md-8">' +
                                            '<label class="switch">' +
                                                '<input type="checkbox" name="video_check">' +
                                                '<span class="slider round"></span>' +
                                            '</label>' +
                                        '</div>' +
                                    '</div>' +
                            '</div>'  +
                            '</div>' +
                        '</div>';
            }else if(tab == 'company'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Company Pictures</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' + 
                                            '<input type="file" multiple name="banner" id="file-2" class="form-control py-1 mb-3" data-file-holder="company-image-holder" data-file-holder-name="company_picture_temp" accept="image/*">' +
                                            '<div id="company-image-holder" class="d-none"></div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i> ) {{__('Select Animation')}}</label>' +
                                    '<div class="col-lg-4">' +
                                        '<select class="form-control demo-select2 " name="animateprofile" id="animateprofile"><option value="">None</option><option value="fade-up">fade-up</option><option value="fade-down">fade-down</option><option value="fade-right">fade-right</option><option value="fade-left">fade-left</option></select>' +
                                    '</div>' +
                        '</div>';
            }else if(tab == 'pdf'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content pdfs-box p-3">' +
                                '<div class="row">' +
                                    '<h5 style="padding: 0px 12px;">Add a Pdf, Guide & Language</h5>'+
                                '</div>'+
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>{{__('PDF Tab Title')}}</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' +
                                            '<input type="text" class="form-control mb-3" placeholder="{{__('PDF Tab Title')}}" name="pdf_title" value="">' + 
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>{{__('Add More info For Your Guide')}}</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' +
                                            '<textarea name="pdf_text" class="form-control mb-3"></textarea>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<label class="col-lg-2 control-label">{{__('PDF Specification')}}</label>' +
                                    '<div class="col-lg-2">' +
                                        '<select class="form-control demo-select2" name="lng[]" id="video_provider">{!! $LanguageOptions !!}</select>' +
                                    '</div>' +
                                    '<div class="col-lg-6">' +
                                        '<input type="file" class="form-control py-1" placeholder="{{__('PDF')}}" name="pdf[]" data-file-holder="pdf-holder" data-file-holder-name="pdf_temp[]" accept="application/pdf">' +
                                        '<div id="pdf-holder" class="d-none"></div>' +
                                    '</div>' +
                                    '<div class="col-md-2">' +
                                        '<button type="button" class="btn btn-success btn-base-1" id="add-more-pdf">Add More Files</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i> ) {{__('Select Animation')}}</label>' +
                                    '<div class="col-lg-4">' +
                                        '<select class="form-control demo-select2 " name="animatepdf" id="animatepdf"><option value="">None</option><option value="fade-up">fade-up</option><option value="fade-down">fade-down</option><option value="fade-right">fade-right</option><option value="fade-left">fade-left</option></select>' +
                                    '</div>' +
                        '</div>';
            }else if(tab == 'main-product'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                                '<div class="row">' +
                                    '<h5 style="padding: 0px 12px;">Select Guide</h5>'+
                                '</div>'+
                                '<div class="row">' +
                                    '<label class="col-lg-2 control-label">{{__('Select Guide')}}</label>' +
                                    '<div class="col-lg-4">' +
                                        '<select class="form-control demo-select2 mb-3" id="main-product" name="product" id="product" required><option value="">Please Select</option>{!! $ProductsOptions !!}</select>' +
                                    '</div>' +
                                    '<div class="col-md-6">' +
                                        '<a href="" class="btn btn-styled btn-base-1 product-edit-btn" target="_blank">Edit</a>' +
                                        '<a href="{{ route('seller.products.upload') }}" class="btn btn-styled btn-base-1 product-edit-btn" target="_blank">Add Guide</a>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
            }else if(tab == 'related'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                                '<div class="row">' +
                                    '<h5 style="padding: 0px 12px;">Related Products</h5>'+
                                '</div>'+
                                '<div class="row mb-3">' +
                                    '<label class="col-lg-3 control-label">{{__('Select Max 3 Products')}}</label>' +
                                    '<div class="col-lg-6">' +
                                        '<select class="form-control demo-select2 multiselect" name="related_products[]" id="related_products" multiple>{!! $ProductsOptions1 !!}</select>' +
                                    '</div>' +
                                    '<label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i> ) {{__('Select Animation')}}</label>' +
                                    '<div class="col-lg-4">' +
                                        '<select class="form-control demo-select2 " name="animateproducts" id="animateproducts"><option value="">None</option><option value="fade-up">fade-up</option><option value="fade-down">fade-down</option><option value="fade-right">fade-right</option><option value="fade-left">fade-left</option></select>' +
                                    '</div>' +
                                '</div>' +
                                 '<div class="row">' +
                                 '<div class="col-md-4">Show to logged in user.</div>' +
                                        '<div class="col-md-8">' +
                                            '<label class="switch">' +
                                                '<input type="checkbox" name="related_check">' +
                                                '<span class="slider round"></span>' +
                                            '</label>' +
                                        '</div>' +
                                    '</div>' +
                                '<div>' +
                            '</div>' +
                        '</div>';
                // html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                //             '<div class="form-box-content p-3">' +
                //                 '<div class="row">' +
                //                     '<label class="col-lg-2 control-label">{{__('Select Max 3 Products')}}</label>' +
                //                     '<div class="col-lg-4">' +
                //                         '<select class="form-control demo-select2 mb-3 multiselect" name="related_products[]" id="video_provider" multiple>{!! $RelatedProductsOptions !!}</select>' +
                //                     '</div>' +
                //                 '</div>' +
                //             '</div>' +
                //         '</div>';
            }else if(tab == 'title'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Company Title</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' + 
                                            '<input type="text" name="title"" class="form-control">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
            }else if(tab == 'external_links'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Amazon, Ebay, Alibaba link</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' + 
                                            '<input type="text" name="link" class="form-control">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
            }else if(tab == 'club'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                                '<div class="row">' +
                                    '<h5 style="padding: 0px 12px;">Edit a text of own client club</h5>'+
                                '</div>'+
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Club Text</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' + 
                                            '<input type="text" class="form-control" name="club_text" value="">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                
                                    '<label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i> ) {{__('Select Animation')}}</label>' +
                                    '<div class="col-lg-4">' +
                                        '<select class="form-control demo-select2 " name="animateclub" id="animateclub"><option value="">None</option><option value="fade-up">fade-up</option><option value="fade-down">fade-down</option><option value="fade-right">fade-right</option><option value="fade-left">fade-left</option></select>' +
                                    '</div>' +
                                     '<div class="row">' +
                                        '<div class="col-md-4">Show to logged in user.</div>' +
                                        '<div class="col-md-8">' +
                                            '<label class="switch">' +
                                                '<input type="checkbox" name="club_check">' +
                                                '<span class="slider round"></span>' +
                                            '</label>' +
                                        '</div>' +
                                    '</div>' +
                                    '</div>' +
                            '</div>' +
                        '</div>';
	}else if(tab == 'coupon'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                                '<div class="row">' +
                                    '<div class="col-md-12">' +
                                        '<h2>Banners</h2>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<input type="radio" onclick="myfunction()" id="couponA" name="coupon_active" value="1">'+ 
                                        '<label for="couponA" class="mr-4"><img src="{{asset("frontend/images/ccoupon.jpg")}}" width="40" height="40"></label>'+ 
                                        '<input type="radio" onclick="myfunction()" id="bannerA" name="coupon_active" value="2">'+ 
                                        '<label for="bannerA" class="mr-4"><img src="{{asset("frontend/images/cbanner.jpg")}}" width="40" height="40"></label>'+
                                        '<input type="radio" onclick="myfunction()" id="popupA" name="coupon_active" value="3">'+ 
                                        '<label for="popupA" class="mr-4"><img src="{{asset("frontend/images/cpopup.jpg")}}" width="40" height="40"></label>'+ 
                                        '<input type="radio" onclick="myfunction()" id="bannerNA" name="coupon_active" value="4">'+ 
                                        '<label for="bannerNA" class="mr-4"><img src="{{asset("frontend/images/bn1-main.png")}}" width="40" height="40"></label>'+ 
                                        '<input type="radio" onclick="myfunction()" id="bannerNB" name="coupon_active" value="5">'+ 
                                        '<label for="bannerNB" class="mr-4"><img src="{{asset("frontend/images/bn2-main.png")}}" width="40" height="40"></label>'+ 
                                        '<input type="radio" onclick="myfunction()" id="bannerNC" name="coupon_active" value="6">'+ 
                                        '<label for="bannerNC" class="mr-4"><img src="{{asset("frontend/images/bn3-main.png")}}" width="40" height="40"></label>'+ 
                                        '<input type="radio" onclick="myfunction()" id="bannerND" name="coupon_active" value="7">'+ 
                                        '<label for="bannerND" class="mr-4"><img src="{{asset("frontend/images/bn4-main.png")}}" width="40" height="40"></label>'+
                                        '<input type="radio" onclick="myfunction()" id="couponD" name="coupon_active" value="0">'+ 
                                        '<label for="couponD">None</label>'+
                                        '<div id="filedscoupon"></div>'+
                                    '</div>' +
                                '</div>' +
                                '<label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i> ) {{__('Select Animation')}}</label>' +
                                    '<div class="col-lg-4">' +
                                        '<select class="form-control demo-select2 " name="animatecoupan" id="animatecoupan"><option value="">None</option><option value="fade-up">fade-up</option><option value="fade-down">fade-down</option><option value="fade-right">fade-right</option><option value="fade-left">fade-left</option></select>' +
                                    '</div>' +
                            '</div>' +
                        '</div>';
	        }else if(tab == 'coupons'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                                '<div class="row">' +
                                    '<div class="col-md-12">' +
                                        '<h2>Coupons</h2>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<input type="radio"  id="couponNA" name="coupon_act" value="1">'+
                                        '<label for="couponNA" class="mr-4"><img src="{{asset("frontend/images/bn1-main.png")}}" width="40" height="40"></label>'+
                                        '<input type="radio"  id="couponNB" name="coupon_act" value="2">'+
                                        '<label for="couponNB" class="mr-4"><img src="{{asset("frontend/images/bn2-main.png")}}" width="40" height="40"></label>'+
                                        '<input type="radio"  id="couponNC" name="coupon_act" value="3">'+
                                        '<label for="couponNC" class="mr-4"><img src="{{asset("frontend/images/bn3-main.png")}}" width="40" height="40"></label>'+
                                        '<input type="radio"  id="couponND" name="coupon_act" value="4">'+
                                        '<label for="couponND" class="mr-4"><img src="{{asset("frontend/images/bn4-main.png")}}" width="40" height="40"></label>'+
                                    '</div>' +
                                    '<div class="form-group row">'+
                                        '<label class="col-lg-4 control-label" for="coupon_code">Select Coupon code</label>'+
                                        '<div class="col-lg-8">'+
                                            '<select class="form-control" name="selectedcoupon" id="selectedcoupon">'+
                                            @foreach($coupons as $coupon)
                                              '<option value="{{ $coupon->id }}" {{ ($shop->coupon_id == $coupon->id)? "selected" : "" }} >{{ $coupon->code }} / {{ $coupon->type }} / {{ $coupon->discount }}</option>'+
                                            @endforeach
                                            '</select>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>' +
                                '<label class="col-lg-2 control-label">{{__("Select Animation")}}</label>' +
                                    '<div class="col-lg-4">' +
                                        '<select class="form-control demo-select2 " name="animatecoupan" id="animatecoupan"><option value="">None</option><option value="fade-up">fade-up</option><option value="fade-down">fade-down</option><option value="fade-right">fade-right</option><option value="fade-left">fade-left</option></select>' +
                                    '</div>' +
                            '</div>' +
                        '</div>';
            }else if(tab == 'warranty'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                                '<div class="row">' +
                                    '<h5 style="padding: 0px 12px;">Edit Warranty text and Form</h5>'+
                                '</div>'+
                                '<input type="hidden" class="form-control" name="warranty" value="true">' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Warranty Text</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' +
                                            '<input type="text" class="form-control" name="warranty_text" value="">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-lg-8 form-horizontal" id="form"></div>' +
                                    '<div class="col-lg-4">' +
                                        '<ul class="list-group" id="warranty-fields">' +
                                            '<li class="list-group-item btn" style="text-align: left;" data-field="text">{{__('Text Input')}}</li>' +
                                            '<li class="list-group-item btn" style="text-align: left;" data-field="select">{{__('Select')}}</li>' +
                                            '<li class="list-group-item btn" style="text-align: left;" data-field="multi-select">{{__('Multiple Select')}}</li>' +
                                            '<li class="list-group-item btn" style="text-align: left;" data-field="radio">{{__('Tags')}}</li>' +
                                            '<li class="list-group-item btn" style="text-align: left;" data-field="file">{{__('File')}}</li>' +
                                            '<li class="list-group-item btn" style="text-align: left;" data-field="date">{{__('Date')}}</li>' +
                                        '</ul>' +
                                    '</div>' +
                                    '<div class="form-box bg-white mt-4 col-12">' +
                                        '<div class="form-box-content p-3">' +
                                            '<div class="row">' +
                                                '<div class="col-md-2">' +
                                                    '<label>{{__('Content')}}</label>' +
                                                '</div>' +
                                                '<div class="col-md-10">' +
                                                    '<div class="mb-3 war1">' +
                                                        '<textarea class="editor" name="content"></textarea>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i> ) {{__('Select Animation')}}</label>' +
                                    '<div class="col-lg-4">' +
                                        '<select class="form-control demo-select2 " name="animatewarranty" id="animatewarranty"><option value="">None</option><option value="fade-up">fade-up</option><option value="fade-down">fade-down</option><option value="fade-right">fade-right</option><option value="fade-left">fade-left</option></select>' +
                                    '</div>' +
                                     '<div class="row">' +
                                        '<div class="col-md-4">Show to logged in user.</div>' +
                                        '<div class="col-md-8">' +
                                            '<label class="switch">' +
                                                '<input type="checkbox" name="warranty_check">' +
                                                '<span class="slider round"></span>' +
                                            '</label>' +
                                        '</div>' +
                                    '</div>' +
                                    '</div>' +
                            '</div>' +
                        '</div>';
            }else if(tab == 'sm-support'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                                '<div class="row">' +
                                    '<h5 style="padding: 0px 12px;">Social media support</h5>'+
                                '</div>'+
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Background Color</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' + 
                                            '<input type="color" class="form-control" name="social_bgcolor" value="#fff" placeholder="example: #000000">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Facebook</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' + 
                                            '<input type="text" class="form-control" name="facebook_link" value="">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Twitter</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' + 
                                            '<input type="text" class="form-control" name="twitter_link" value="">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Instagram</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' + 
                                            '<input type="text" class="form-control" name="instagram_link" value="">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Linkedin</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' + 
                                            '<input type="text" class="form-control" name="linkedin_link" value="">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Youtube</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' + 
                                            '<input type="text" class="form-control" name="youtube_link" value="">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Tiktok</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' + 
                                            '<input type="text" class="form-control" name="tiktox_link" value="">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-md-2">' +
                                        '<label>Whatsapp</label>' +
                                    '</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' + 
                                            '<input type="text" class="form-control" name="whatsapp" value="">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                             '<label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i> ) {{__('Select Animation')}}</label>' +
                                    '<div class="col-lg-4">' +
                                        '<select class="form-control demo-select2 " name="animatesocial" id="animatesocial"><option value="">None</option><option value="fade-up">fade-up</option><option value="fade-down">fade-down</option><option value="fade-right">fade-right</option><option value="fade-left">fade-left</option></select>' +
                                    '</div>' +
                        '</div>';
                // html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                //             '<div class="mini-dashboard-social-data">' + 
                //                 '<a href="#"><i class="fa fa-facebook"></i></a>' +
                //                 '<a href="#"><i class="fa fa-twitter"></i></a>' +
                //                 '<a href="#"><i class="fa fa-whatsapp"></i></a>' + 
                //                 '<a href="#"><i class="fa fa-linkedin"></i></a>' + 
                //                 '<a href="#"><i class="fa fa-link"></i></a>' + 
                //             '</div>' + 
                //         '</div>';
            }else if(tab == 'qa'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                               '<div class="row">' +
                                    '<h5 style="padding: 0px 12px;">Frequently Asked Questions</h5>'+
                                '</div>'+
                                '<div class="question_block">' +
                                    '<div class="question_answer">' +
                                        '<div class="row">' +
                                            '<div class="col-md-2">' +
                                                '<label>Question</label>' +
                                            '</div>' +
                                            '<div class="col-md-10">' +
                                                '<div class="d-flex align-items-center justify-content-center flex-column highlight">' + 
                                                    '<input type="text" name="question[]" class="form-control py-1 mb-3">' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="row">' +
                                            '<div class="col-md-2">' +
                                                '<label>Answer</label>' +
                                            '</div>' +
                                            '<div class="col-md-10">' +
                                                '<div class="d-flex align-items-center justify-content-center flex-column highlight">' + 
                                                    '<input type="text" name="answer[]" class="form-control py-1 mb-3">' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-md-12 text-right">' +
                                        '<button type="button" class="btn btn-danger" id="remove_more_question" style="margin-right:10px;">{{ __('Remove') }}</button>' +
                                        '<button type="button" class="btn btn-success" id="add_more_question">{{ __('Add More') }}</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                             '<label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i> ) {{__('Select Animation')}}</label>' +
                                    '<div class="col-lg-4">' +
                                        '<select class="form-control demo-select2 " name="animatefaq" id="animatefaq"><option value="">None</option><option value="fade-up">fade-up</option><option value="fade-down">fade-down</option><option value="fade-right">fade-right</option><option value="fade-left">fade-left</option></select>' +
                                    '</div>' +
                        '</div>';
            }else if(tab == 'reviews'){
                html = '<div class="minidashprotab_item'+added_by+'" data-tab="'+tab+'">' + CloseBtn +
                            '<div class="form-box-content p-3">' +
                                '<div class="row">' +
                                    '<div class="col-md-12">' +
                                        '<div class="d-flex align-items-center justify-content-center flex-column">' + 
                                            '<span class="p-5 text-center d-block h2">Customer Reviews</span>' +
                                            '<input type="hidden" class="form-control tmp-files" name="reviews" value="true">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                             '<label class="col-lg-12 control-label">( <i  class="fa fa-info" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."></i> ) {{__('Select Animation')}}</label>' +
                                    '<div class="col-lg-4">' +
                                        '<select class="form-control demo-select2 " name="animatereviews" id="animatereviews"><option value="">None</option><option value="fade-up">fade-up</option><option value="fade-down">fade-down</option><option value="fade-right">fade-right</option><option value="fade-left">fade-left</option></select>' +
                                    '</div>' +
                        '</div>';
            }
            Newtab.classList.add("new_added");
            Newtab.innerHTML = html;
            ev.target.appendChild(Newtab);
            var parentrow = $('.new_added').closest('.row');
            var parrenttab = parentrow.closest('.minidashprotab_item').attr('data-tab');
            var CurPossition = $( "."+parrenttab+"_block .row" ).index(parentrow);
            var TotalRows = $( "."+parrenttab+"_block .row" ).length-1;
            if(CurPossition != -1){
                TotalRows = TotalRows/CurPossition;
                if(CurPossition >= TotalRows){
                    $('.new_added').closest('.minidashprotab_item').after($('.new_added').html());
                }else{
                    $('.new_added').closest('.minidashprotab_item').before($('.new_added').html());
                }
                $('.new_added').remove();
            }else if($('.new_added').parent().hasClass('drag-and-drop-text')){
                $('.conteudo').append($('.new_added').html());
                $('.new_added').remove();
            }else if($('.new_added').parent().hasClass('form-box-content')){
                $('.new_added').closest('.minidashprotab_item').after($('.new_added').html());
                $('.new_added').remove();
            }else if($('.new_added').parent().parent().hasClass('minidashprotab_item')){
                $('.new_added').closest('.minidashprotab_item').after($('.new_added').html());
                $('.new_added').remove();
            }else if($('.new_added').parent().parent().parent().hasClass('minidashprotab_item')){
                $('.new_added').closest('.minidashprotab_item').after($('.new_added').html());
                $('.new_added').remove();
            }
            $('.new_added').removeClass('new_added');
              
            if(tab == 'info'){
               
                $('.editor').each(function(el){
                    //   $('.editor').remove();
                    
                   var editor = new Jodit(this, {
                    "uploader": {
                        "insertImageAsBase64URI": true
                    }
                    });
                    
                // var $div = $('div.className:contains(jodit_container jodit_default_theme jodit_toolbar_size-middle jodit_wysiwyg_mode)');

                if ($('.war2 .jodit_container').length > 1) {
                   $('.war2 .jodit_container').not('.war2 .jodit_container:last').remove();
                }
                if ($('.info2 .jodit_container').length > 1) {
                   $('.info2 .jodit_container').not('.info2 .jodit_container:last').remove();
                }
                 if ($('.war1 .jodit_container').length > 1) {
                   $('.war1 .jodit_container').not('.war1 .jodit_container:last').remove();
                }
                if ($('.info1 .jodit_container').length > 1) {
                   $('.info1 .jodit_container').not('.info1 .jodit_container:last').remove();
                }
                
                
                });
                
            }
                
                
                
                
                 
                 
                 if(tab == 'warranty'){
               
                $('.editor').each(function(el){
                    //   $('.editor').remove();
                    
                   var editor = new Jodit(this, {
                    "uploader": {
                        "insertImageAsBase64URI": true
                    }
                    });
                    

                if ($('.war2 .jodit_container').length > 1) {
                   $('.war2 .jodit_container').not('.war2 .jodit_container:last').remove();
                }
                 if ($('.info2 .jodit_container').length > 1) {
                   $('.info2 .jodit_container').not('.info2 .jodit_container:last').remove();
                }
                 if ($('.war1 .jodit_container').length > 1) {
                   $('.war1 .jodit_container').not('.war1 .jodit_container:last').remove();
                }
                if ($('.info1 .jodit_container').length > 1) {
                   $('.info1 .jodit_container').not('.info1 .jodit_container:last').remove();
                }
                
                });
                
                 }
               

             
            
          
                 
                
            
            
            if(tab == 'main-product'){
                $('#main-product').trigger('change');
            }
            var tabs = [];
            $('.minidashprotab_item').each(function() {
                tabs.push($(this).attr('data-tab'));
            });
            tabs.join(',');
            $('#tabs_sorting').val(tabs);
            if($('.minidashprotab_item').length > 1){
                $('#save-button, #confirmation-button').prop('disabled', false);
                // $('.conteudo').removeClass('rounded');
                // $('.conteudo').removeClass('border');
                $('.drag-and-drop-text').addClass('d-none');
                $('.drag-and-drop-text').removeClass('d-block');
            }else{
                // $('.conteudo').addClass('rounded');
                // $('.conteudo').addClass('border');
                $('.drag-and-drop-text').addClass('d-block');
                $('.drag-and-drop-text').removeClass('d-none');
            }
            $('.demo-select2').select2();
        }

        var i = 0;

        function add_customer_choice_options(em){
            var j = $(em).closest('.form-group').find('.option').val();
            var str = '<div class="form-group d-flex w-100">'
                            +'<div class="col-sm-6 col-sm-offset-4 pull-left">'
                                +'<input class="form-control" type="text" name="options_'+j+'[]" value="" required>'
                            +'</div>'
                            +'<div class="col-sm-2 pull-left"> <i class="fa fa-times" onclick="delete_choice_clearfix(this)"></i>'
                            +'</div>'
                        +'</div>'
            $(em).parent().find('.customer_choice_options_types_wrap_child').append(str);
        }
        function delete_choice_clearfix(em){
            $(em).parent().parent().remove();
        }
        function appenddToForm(type){
            //$('#form').removeClass('seller_form_border');
            if(type == 'text'){
                var str = '<div class="form-group d-flex w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
                                +'<input type="hidden" name="type[]" value="text">'
                                +'<div class="col-lg-3 pull-left">'
                                    +'<label class="control-label">Text</label>'
                                +'</div>'
                                +'<div class="col-lg-7 pull-left">'
                                    +'<input class="form-control" type="text" name="label[]" placeholder="Label">'
                                +'</div>'
                                +'<div class="col-lg-2 pull-left">'
                                    +'<i class="fa fa-times" onclick="delete_choice_clearfix(this)"></i>'
                                +'</div>'
                            +'</div>';
                $('#form').append(str);
            }
            else if (type == 'select') {
                i++;
                var str = '<div class="form-group d-flex w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
                                +'<input type="hidden" name="type[]" value="select"><input type="hidden" name="option[]" class="option" value="'+i+'">'
                                +'<div class="col-lg-3 pull-left">'
                                    +'<label class="control-label">Select</label>'
                                +'</div>'
                                +'<div class="col-lg-7 pull-left">'
                                    +'<input class="form-control" type="text" name="label[]" placeholder="Select Label" style="margin-bottom:10px">'
                                    +'<div class="customer_choice_options_types_wrap_child">'

                                    +'</div>'
                                    +'<button class="btn btn-success pull-right" type="button" onclick="add_customer_choice_options(this)"><i class="glyphicon glyphicon-plus"></i> Add option</button>'
                                +'</div>'
                                +'<div class="col-lg-2 pull-left">'
                                    +'<i class="fa fa-times" onclick="delete_choice_clearfix(this)"></i>'
                                +'</div>'
                            +'</div>';
                $('#form').append(str);
            }
            else if (type == 'multi-select') {
                i++;
                var str = '<div class="form-group d-flex w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
                                +'<input type="hidden" name="type[]" value="multi_select"><input type="hidden" name="option[]" class="option" value="'+i+'">'
                                +'<div class="col-lg-3 pull-left">'
                                    +'<label class="control-label">Multiple select</label>'
                                +'</div>'
                                +'<div class="col-lg-7 pull-left">'
                                    +'<input class="form-control" type="text" name="label[]" placeholder="Multiple Select Label" style="margin-bottom:10px">'
                                    +'<div class="customer_choice_options_types_wrap_child">'

                                    +'</div>'
                                    +'<button class="btn btn-success pull-right" type="button" onclick="add_customer_choice_options(this)"><i class="glyphicon glyphicon-plus"></i> Add option</button>'
                                +'</div>'
                                +'<div class="col-lg-2 pull-left">'
                                    +'<i class="fa fa-times" onclick="delete_choice_clearfix(this)"></i>'
                                +'</div>'
                            +'</div>';
                $('#form').append(str);
            }
            else if (type == 'radio') {
                i++;
                var str = '<div class="form-group d-flex w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
                                +'<input type="hidden" name="type[]" value="radio"><input type="hidden" name="option[]" class="option" value="'+i+'">'
                                +'<div class="col-lg-3 pull-left">'
                                    +'<label class="control-label">Radio</label>'
                                +'</div>'
                                +'<div class="col-lg-7 pull-left">'
                                    +'<input class="form-control" type="text" name="label[]" placeholder="Radio Label" style="margin-bottom:10px">'
                                    +'<div class="customer_choice_options_types_wrap_child">'

                                    +'</div>'
                                    +'<button class="btn btn-success pull-right" type="button" onclick="add_customer_choice_options(this)"><i class="glyphicon glyphicon-plus"></i> Add option</button>'
                                +'</div>'
                                +'<div class="col-lg-2 pull-left">'
                                    +'<i class="fa fa-times" onclick="delete_choice_clearfix(this)"></i>'
                                +'</div>'
                            +'</div>';
                $('#form').append(str);
            }
            else if (type == 'file') {
                var str = '<div class="form-group d-flex w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
                                +'<input type="hidden" name="type[]" value="file">'
                                +'<div class="col-lg-3 pull-left">'
                                    +'<label class="control-label">File</label>'
                                +'</div>'
                                +'<div class="col-lg-7 pull-left">'
                                    +'<input class="form-control" type="text" name="label[]" placeholder="Label">'
                                +'</div>'
                                +'<div class="col-lg-2 pull-left">'
                                    +'<i class="fa fa-times" onclick="delete_choice_clearfix(this)"></i>'
                                +'</div>'
                            +'</div>';
                $('#form').append(str);
            }
            else if (type == 'date') {
                var str = '<div class="form-group d-flex w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
                                // +'<input type="hidden" name="type[]" value="date">'
                                +'<div class="col-lg-6 pull-left">'
                                    +'<label class="control-label">Amount of X (Months)</label>'
                                +'</div>'
                                +'<div class="col-lg-5 pull-left">'

                                    +'<input type="Number" id="edate" name="edate" placeholder="Number of Months">'
                                +'</div>'
                                +'<div class="col-lg-1 pull-left">'
                                    +'<i class="fa fa-times" onclick="delete_choice_clearfix(this)"></i>'
                                +'</div>'
                            +'</div>';
                $('#form').append(str);
            }
        }

        var last_valid_selection = null;

        $('.multiselect').change(function(event) {

            if ($(this).val().length > 3) {

                $(this).val(last_valid_selection);
            } else {
                last_valid_selection = $(this).val();
            }
        });

        $('body').on('click', '#warranty-fields .list-group-item', function(){
            appenddToForm($(this).attr('data-field'));
        })

        $('body').on('click', '#confirmation-button', function(){
            $('#confirmation_form .preview_from_fields').remove();
            $('#save_form input[type=text]').each(function() {
                $('#confirmation_form').append('<input type="hidden" class="preview_from_fields" name="'+$(this).attr('name')+'" value="'+$(this).val()+'">');
            });
            $('#save_form input[type=radio]:checked').each(function() {
                $('#confirmation_form').append('<input type="hidden" class="preview_from_fields" name="'+$(this).attr('name')+'" value="'+$(this).val()+'">');
            });
            $('#save_form .tmp-files').each(function() {
                $('#confirmation_form').append('<input type="hidden" class="preview_from_fields" name="'+$(this).attr('name')+'" value="'+$(this).val()+'">');
            });
            $('#save_form textarea').each(function() {
                $('#confirmation_form').append('<textarea type="hidden" class="preview_from_fields" class="d-none" name="'+$(this).attr('name')+'">'+$(this).val()+'</textarea>');
            });
            $('#save_form select').each(function() {
                $('#confirmation_form').append('<input type="hidden" class="preview_from_fields" name="'+$(this).attr('name')+'" value="'+$(this).val()+'">');
            });
            // var FormAction = $(this).attr('data-action');
            // $('#save_form').attr('action', FormAction);
            // $('#save_form').attr('target', '_blank');
            // $('input[type=file]').prop('disabled', true);
            $('#confirmation_form').submit();
            // $('input[type=file]').prop('disabled', false);
        });

        $('body').on('change', 'input[type=file]', function(){
            var imgPath = $(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var file_holder_name = $(this).attr('data-file-holder-name');

            if (typeof (FileReader) != "undefined") {

                var image_holder = $('#'+$(this).attr('data-file-holder'));
                image_holder.empty();

                var reader = new FileReader();
                reader.onload = function (e) {
                    $("<input />", {
                        "value": e.target.result,
                        "name": file_holder_name,
                        "class": "tmp-files"
                    }).appendTo(image_holder);
                }
                image_holder.show();
                reader.readAsDataURL($(this)[0].files[0]);
            } else {
                alert("This browser does not support FileReader.");
            }
        });

        $('body').on('change', '#main-product', function(){
            var url = $('option:selected', this).attr('data-edit');
            $('.product-edit-btn').attr('href', url);
        });

        $('body').on('click', '.spartan_remove_row', function(){
            $(this).parent().remove();
            var tabs = [];
            $('.minidashprotab_item').each(function() {
                tabs.push($(this).attr('data-tab'));
            });
            tabs.join(',');
            $('#tabs_sorting').val(tabs);
            if($('.minidashprotab_item').length > 1){
                $('#save-button, #confirmation-button').prop('disabled', false);
            }else{
                // $('.conteudo').addClass('rounded');
                // $('.conteudo').addClass('border');
                $('.drag-and-drop-text').removeClass('d-none');
                $('.drag-and-drop-text').addClass('d-block');
            }
        });
        
        $('body').on('change', '#video_provider', function(){
            $('.link_video_block, .manual_video_block').addClass('d-none');
            $('.manual_video_block input, .link_video_block input').prop('disabled', true);
            if($(this).val() != 'upload_manual'){
                $('.link_video_block').removeClass('d-none');
                $('.link_video_block input').prop('disabled', false);
            }else if($(this).val() == 'upload_manual'){
                $('.manual_video_block').removeClass('d-none');
                $('.manual_video_block input').prop('disabled', false);
            }
        });
        
        $('body').on('click','#add-more-pdf',function(){
            var TotalPdfs = $('.pdfs-box input[type=file]').length;
            
            var TabHtml = $(this).parent().parent().html();
            TabHtml = TabHtml.replace("pdf-holder", "pdf-holder"+TotalPdfs);
            TabHtml = TabHtml.replace('"pdf-holder"', '"pdf-holder'+TotalPdfs+'"');

            $('.pdfs-box').append('<div class="row d-none added-pdf-row">'+TabHtml+'</div>');
            $('.added-pdf-row').children().next().next().next().html('');
            $('.added-pdf-row .custom-input-file').attr('id', 'pdf-file-'+$('.pdfs-box .row').length);
            $('.added-pdf-row .pdf-field-label').attr('for', 'pdf-file-'+$('.pdfs-box .row').length);
            $('.added-pdf-row').removeClass('d-none');
        });

        $('body').on('change','#category_id',function(){
            get_subcategories_by_category();
        });

        $('body').on('change','#subcategory_id',function(){
            get_subsubcategories_by_subcategory();
        });

        $('body').on('change','#subsubcategory_id',function(){
            get_products_by_subsubcategory();
        });
        
        $('body').on('click','.Editor-editor',function(e){
            $(this).focus();
            e.preventDefault();
            return false;
        });

        $('body').on('click','#add_more_question',function(e){
            $('.question_block').append('<div class="question_answer new_added_question_answer">'+$('.question_answer').first().html()+'</div>');
            $('.new_added_question_answer input').val(null);
        });
        $('body').on('click','#remove_more_question',function(e){
            $('.question_block .question_answer:last').fadeOut('slow',function(){
                $(this).remove();
            });
            
        });
        var left_side_height = $('.mini-dashboard-left-side-data').height();
        var one_tab_height = $('.mini-dashboard-left-side-data .minidashpropritabs .minidashpropritab').first().height();
        one_tab_height = one_tab_height*2;
        $('.conteudo').css('min-height', (left_side_height-one_tab_height)+'px');

        function get_subcategories_by_category(){
            var category_id = $('#category_id').val();
            $.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
                $('#subcategory_id').html(null);
                for (var i = 0; i < data.length; i++) {
                    $('#subcategory_id').append($('<option>', {
                        value: data[i].id,
                        text: data[i].name
                    }));
                    $('.demo-select2').select2();
                }
                get_subsubcategories_by_subcategory();
            });
        }

        function get_subsubcategories_by_subcategory(){
            var subcategory_id = $('#subcategory_id').val();
            $.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
                $('#subsubcategory_id').html(null);
                for (var i = 0; i < data.length; i++) {
                    $('#subsubcategory_id').append($('<option>', {
                        value: data[i].id,
                        text: data[i].name
                    }));
                    $('.demo-select2').select2();
                }
                get_products_by_subsubcategory();
            });
        }

        function get_products_by_subsubcategory(){
            var subsubcategory_id = $('#subsubcategory_id').val();
            $.post('{{ route('subsubcategories.get_products_by_subsubcategory') }}',{_token:'{{ csrf_token() }}', subsubcategory_id:subsubcategory_id}, function(data){
                $('#related_products').html(null);
                for (var i = 0; i < data.length; i++) {
                    $('#related_products').append($('<option>', {
                        value: data[i].id,
                        text: data[i].name
                    }));
                    $('.demo-select2').select2();
                }
            });
        }
        function myfunction() {
            var radios = document.getElementsByName('coupon_active');
            if (radios[0].checked) {
                radvalue = radios[0].value
                radvalue = 4
            } else if (radios[1].checked) {
                radvalue = radios[1].value
                radvalue = 5
            }
            else if (radios[2].checked) {
                radvalue = radios[2].value
                radvalue = 4
            }
            else if (radios[3].checked) {
                radvalue = radios[3].value
                radvalue = 1
            }
            else if (radios[4].checked) {
                radvalue = radios[4].value
                radvalue = 1
            }
            else if (radios[5].checked) {
                radvalue = radios[5].value
                radvalue = 1
            }
            else if (radios[6].checked) {
                radvalue = radios[6].value
                radvalue = 1
            }

            else if (radios[7].checked) {
                radvalue = radios[7].value
                radvalue = 0
            }

            // alert(radvalue);
            $('#filedscoupon').empty();
            var str = '';
                for (let i = 0; i < radvalue; i++) {
                  // text += cars[i] + "<br>";
                  str += '<div class="form-group d-flex w-100" style="background:rgba(0,0,0,0.1);padding:10px 0;">'
                                +'<div class="col-lg-3 pull-left">'
                                    +'<label class="control-label">Field</label>'
                                +'</div>'
                                +'<div class="col-lg-7 pull-left">'
                                    +'<input class="form-control" maxlength="8" required type="text" name="coupon_text[]" placeholder="Label">'
                                +'</div>'
                        +'</div>';
                }
                $('#filedscoupon').append(str);

            // document.getElementById('txt').value = radvalue;
        }
        $(function () {
  $('[data-toggle="popover"]').popover()
});
    </script>
@endsection