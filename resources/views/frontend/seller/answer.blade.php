@extends('frontend.layouts.app')

@section('content')

    <section class="gry-bg py-4 profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @include('frontend.inc.seller_side_nav')
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                        {{__('Questions')}}
                                    </h2>
                                </div>
                                <div class="col-md-6">
                                    <div class="float-md-right">
                                        <ul class="breadcrumb">
                                            <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                                            <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                                            <li class="active"><a href="{{ route('question.index') }}">{{__('Questions')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Order history table -->
                        <div class="card no-border mt-4">
                            <div>
                                <table class="table table-sm table-hover table-responsive-md">
                                    <tbody>
                                        <tr>
                                            <td>{{ __('Question') }}</td>
                                            <td>{{ $question->comment }}</td>
                                        </tr>
                                        @if(\App\Models\BusinessSetting::where('type', 'question_answer')->first()->value == 1)
                                            <tr>
                                                <td>{{__('Answer:')}}</td>
                                                <td>
                                                    <form class="" action="{{ route('question.store') }}" method="post">
                                                        @csrf
                                                        <input type="hidden" value="{{$question->id}}" name="question">
                                                        <div class="modal-body gry-bg px-3 pt-3">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <textarea class="editor" name="details"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-base-1">{{__('Save')}}</button>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endif
                                        @foreach (\App\Models\Question::where('parent_id', $question->id)->orderBy('id', 'DESC')->get() as $key => $question)
                                            @if($question->product_id != null && $question->user_id != null)
                                                <tr>
                                                    <td>
                                                        {{__('Answer')}} {{ $key+1 }}
                                                    </td>
                                                    <td>{{ $question->comment }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        {{-- <div class="pagination-wrapper py-4">
                            <ul class="pagination justify-content-end">
                                {{ $questions->links() }}
                            </ul>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@php	$policies_class = ' d-none d-lg-block';@endphp
