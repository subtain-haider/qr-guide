@extends('frontend.layouts.new_app_1')

@section('content')
<style>
    /* The grid: Four equal columns that floats next to each other */
.column {
  float: left;
  width: 25%;
  padding: 10px;
}

/* Style the images inside the grid */
.column img {
  opacity: 0.8;
  cursor: pointer;
}

.column img:hover {
  opacity: 1;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* The expanding image container (positioning is needed to position the close button and the text) */
.container {
  position: relative;
  display: none;
}

/* Expanding image text */
#imgtext {
  position: absolute;
  bottom: 15px;
  left: 15px;
  color: white;
  font-size: 20px;
}

/* Closable button inside the image */
.closebtn {
  position: absolute;
  top: 10px;
  right: 15px;
  color: white;
  font-size: 35px;
  cursor: pointer;
}
@php
    $PackageInfo = \App\Models\User::where('package', Auth::user()->package)->first();
    $PP = Auth::user()->package_for;
    $Permissions = json_decode($PackageInfo->package_for);
    $coupons = \App\Models\Coupon::where('user_id', Auth::user()->id)->get();
    $maillist = \App\Models\User::where('slug', Auth::user()->id)->get();
@endphp
</style>
    <section class="dashboard-recover-sub-links-sec" id="dashrecoversublinks">
        <div class="dashboard-recover-two-sides-left-all">
            <div class="dashboard-recover-two-sides-left-all-ul">
                <ul class="dashboard-recover-two-sides-left-links">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Product <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Inhouse Orders</a></li>
                    <li><a href="#">Total Sales</a></li>
                    <li><a href="#">Saller Sales via Affiliate Links</a></li>
                    <li><a href="#">Vendors <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Customer <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Reports <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Pricing </a></li>
                    <li><a href="#">Bussines Setting <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">E-commerce Setup <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Suppot Ticket</a></li>
                    <li><a href="#">Log Activity</a></li>
                </ul>
                
            </div>
        </div>
        <div class="dashboard-recover-res-sublinks-cross">
            <i class="fa fa-times" onclick="hidedashrecoversublinks()"></i>
        </div>
    </section>
    <!-- end res sub mnu -->
    <section class="dashboard-recover-burdger">
        <div class="dashboard-recover-burdger-page">
            <div class="dashboard-recover-burdger-bars">
                <i class="fa fa-bars" onclick="showdashrecoversublinks()"></i>
            </div>
        </div>
    </section>
    <section class="product-listing-page-sec">
        <div class="product-listing-page-wdth">
            <div class="product-listing-page-data">

                @include('frontend.inc.seller_side_nav_new')

                <div class="product-listing-page-right">
                    <div class="product-listing-page-right-all">
                        <div class="product-listing-page-right-table-area">
                            <div class="product-listing-page-right-table-2-manage">
                                <form class="" action="{{ route('seller.send.email') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-box bg-white mt-4">
                                        <div class="product-listing-page-right-all-head">
                                            <h2>{{__('Email Marketing')}}</h2>
                                        </div>
                                            <div class=" p-3">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label>{{__('Select Template')}}</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <select class="form-control"  name="template" id="template">
                                                            <option value="mail">Default</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=" p-3">
                                                <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{__('Select Email List')}}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <select class="form-control demo-select2 multiselect" name="selectelist[]" id="selectelist" multiple>
                                                            @foreach($maillist as $list)
                                                                @if(!empty($maillist))
                                                                <option value="{{ $list->email }}" >{{ $list->name  }}</option>
                                                                @else
                                                                    <option value="{{ $list->email }}" >{{ $list->name }}</option>
                                                                @endif
                                                            @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            </div>
                                            <div class=" p-3">
                                                <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{__('Enter External Email')}}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="email" class="form-control " placeholder="{{__('Enter External Email')}}" name="cemail" >
                                                    
                                                </div>
                                            </div>
                                            </div>
                                            <div class=" p-3">
                                                <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{__('Select Coupon')}}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <select class="form-control "  name="selectedcoupon" id="selectedcoupon">
                                                        @foreach($coupons as $coupon)
                                                        <option value="{{ $coupon->code }}">{{ $coupon->code }} / {{ $coupon->type }} / {{ $coupon->discount }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            </div>
                                            <div class=" p-3">
                                                <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{__('Email From')}}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="email" class="form-control " placeholder="{{__('Your Email')}}" name="email" value="{{ Auth::user()->email }}" disabled>
                                                    <input type="hidden" class="form-control " placeholder="{{__('Your Email')}}" name="fromemail" value="{{ Auth::user()->email }}">
                                                </div>
                                            </div>
                                            </div>
                                            <div class=" p-3">
                                                <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{__('Subject')}}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control " placeholder="{{__('Your Subject')}}" name="subject" >
                                                </div>
                                            </div>
                                            </div>
                                            <div class=" p-3">
                                                <div class="row">
                                                <div class="col-md-2">
                                                    <label>{{__('Message')}}</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <textarea id="bodymessage" class="editor" name="bodymessage" rows="4" cols="50">
                                                      They offer free tutorials in all web development technologies.
                                                     </textarea>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="text-right mt-4">
                                                <button type="submit" class="btn btn-info">{{__('Send Email')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section> 
    
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() {
	    $('.demo-select2').select2({
          placeholder: 'Select Emails',
          allowClear: true
        });
        $("#photos").spartanMultiImagePicker({
            fieldName:        'photo',
            maxCount:         1,
            rowHeight:        '200px',
            groupClassName:   'w-100',
            maxFileSize:      '',
            dropFileLabel : "Drop Here",
            onExtensionErr : function(index, file){
                console.log(index, file,  'extension err');
                alert('Please only input png or jpg type file')
            },
            onSizeErr : function(index, file){
                console.log(index, file,  'file size too big');
                alert('File size too big');
            }
        });
    });
</script>
<script>
    function myFunction(imgs) {
        $imgName = imgs.getAttribute("data-name");
        console.log($imgName);
        document.getElementById('template').value = $imgName;
  // Get the expanded image
  var expandImg = document.getElementById("expandedImg");
  // Get the image text
  var imgText = document.getElementById("imgtext");
  // Use the same src in the expanded image as the image being clicked on from the grid
  expandImg.src = imgs.src;
  // Use the value of the alt attribute of the clickable image as text inside the expanded image
  imgText.innerHTML = imgs.alt;
  // Show the container element (hidden with CSS)
  expandImg.parentElement.style.display = "block";
}
</script>
@endsection