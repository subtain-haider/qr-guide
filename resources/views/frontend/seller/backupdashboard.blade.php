@extends('frontend.layouts.new_app_1')

@section('content')

    <!-- left and right sides -->
    <section class="dashboard-recover-sub-links-sec" id="dashrecoversublinks">
        <div class="dashboard-recover-two-sides-left-all">
            <div class="dashboard-recover-two-sides-left-all-ul">
                <ul class="dashboard-recover-two-sides-left-links">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Product <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Inhouse Orders</a></li>
                    <li><a href="#">Total Sales</a></li>
                    <li><a href="#">Saller Sales via Affiliate Links</a></li>
                    <li><a href="#">Vendors <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Customer <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Reports <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Pricing </a></li>
                    <li><a href="#">Bussines Setting <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">E-commerce Setup <i class="fa fa-angle-right"></i></a>
                        <ul class="dashboard-recover-two-sides-left-sub-links">
                            <li><a href="#">Sub links1</a></li>
                            <li><a href="#">Sub links2</a></li>
                            <li><a href="#">Sub links3</a></li>
                            <li><a href="#">Sub links4</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Suppot Ticket</a></li>
                    <li><a href="#">Log Activity</a></li>
                </ul>

            </div>
        </div>
        <div class="dashboard-recover-res-sublinks-cross">
            <i class="fa fa-times" onclick="hidedashrecoversublinks()"></i>
        </div>
    </section>
    <!-- end res sub mnu -->
    <section class="dashboard-recover-burdger">
        <div class="dashboard-recover-burdger-page">
            <div class="dashboard-recover-burdger-bars">
                <i class="fa fa-bars" onclick="showdashrecoversublinks()"></i>
            </div>
        </div>
    </section>
    <section class="dashboard-recover-two-sides-sec">
        <div class="dashboard-recover-two-sides-sec-page">
            <div class="dashboard-recover-two-sides-data">
                @include('frontend.inc.seller_side_nav_new')
                <!-- right side -->
                <div class="dashboard-recover-two-sides-right">
                    <div class="dashboard-recover-two-sides-right-all">
                        <div class="dashboard-recover-two-sides-right-head">
                            <h2>Dashboard</h2>
                            <p>The Update about the Support Tickets</p>
                        </div>
                        <div class="dashboard-recover-two-sides-inner-data">
                            <div class="dashboard-recover-two-sides-inner-left">

                                <div class="dashboard-recover-two-sides-inner-manage-data">
                                    <div class="dashboard-recover-two-sides-inner-manage-detail">
                                        <div class="dashboard-recover-two-sides-inner-manage-img">
                                            <img src="{{ asset('frontend/images/cart.png') }}">
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-head">
                                            <h4>PRODUCTS</h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-para">
                                            <p>Number of products on sale</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-link">
                                            <a href="#">Manage products <i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-manage-detail">
                                        <div class="dashboard-recover-two-sides-inner-manage-img">
                                            <img src="{{ asset('frontend/images/heart.png') }}">
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-head">
                                            <h4>GUIDES</h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-para">
                                            <p>Number of qr guides on line</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-link">
                                            <a href="#">Manage products <i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-manage-detail">
                                        <div class="dashboard-recover-two-sides-inner-manage-img">
                                            <img src="{{ asset('frontend/images/doc.png') }}">
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-head">
                                            <h4>WARRANTY</h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-para">
                                            <p>Number of active warranty</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-manage-link">
                                            <a href="#">Manage products <i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="dashboard-recover-two-sides-inner-moreinfo-data">
                                    <div class="dashboard-recover-two-sides-inner-moreinfo-detail">
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-text">
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-head">
                                                <h4>07</h4>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <p>Total sellers</p>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <a href="#">More info <i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-img">
                                            <img src="{{ asset('frontend/images/document.png') }}">
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-moreinfo-detail">
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-text">
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-head">
                                                <h4>07</h4>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <p>Total ordered been added today</p>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <a href="#">More info <i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-img">
                                            <img src="{{ asset('frontend/images/process.png') }}">
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-moreinfo-detail">
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-text">
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-head">
                                                <h4>{{App\Models\Shop::where('user_id', Auth::user()->id)->count()}}</h4>
                                            </div>
                                            <?php
                                                $qrs = App\Models\Shop::where('user_id', Auth::user()->id)->select('slug')->get();
                                                $qrcode = array();
                                            ?>
                                            @foreach($qrs as $qr)
                                               <?php $qrcode[] =  QrCode::size(100)->generate(route('company.visit', $qr)) ?>
                                            @endforeach
                                            <a href="{{ route('company.qrcode.download', App\Models\Shop::where('user_id', Auth::user()->id)->select('slug')->first()) }}" target="_blank">
                                                {{QrCode::size(100)->generate(route('company.visit', App\Models\Shop::where('user_id', Auth::user()->id)->select('slug')->first()))}}
                                            </a>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <p>QR code {{ App\Models\Shop::where('user_id', Auth::user()->id)->select('slug')->first() }}</p>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <a href="{{ route('company.index')}}">{{ request()->getHost() }} <i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div>

                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-img-1">
                                            <a href="{{ asset('frontend/images/qr.png') }}" download="{{ basename(asset('frontend/images/qr.png')) }}"><img src="{{ asset('frontend/images/qr.png') }}"></a>
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-moreinfo-detail">
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-text">
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-head">
                                                <h4>07</h4>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <p>Total sellers</p>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <a href="#">More info <i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-img">
                                            <img src="{{ asset('frontend/images/document.png') }}">
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-moreinfo-detail">
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-text">
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-head">
                                                <h4>07</h4>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <p>Total products on wishlist</p>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <a href="#">More info <i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-img">
                                            <img src="{{ asset('frontend/images/process.png') }}">
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-moreinfo-detail">
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-text">
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-head">
                                                <h4>07</h4>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <p>Total sellers</p>
                                            </div>
                                            <div class="dashboard-recover-two-sides-inner-moreinfo-detail-para">
                                                <a href="#">More info <i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-moreinfo-detail-img">
                                            <img src="{{ asset('frontend/images/van.png') }}">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="dashboard-recover-two-sides-inner-right">
                                <div class="dashboard-recover-two-sides-inner-right-data">
                                    <div class="dashboard-recover-two-sides-inner-right-detail">
                                        <div class="dashboard-recover-two-sides-inner-right-detail-par">
                                            <p>Total product category</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-hed">
                                            <h4>07</h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-btn">
                                            <button>Create Category</button>
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-right-detail">
                                        <div class="dashboard-recover-two-sides-inner-right-detail-par">
                                            <p>Total active mini sites</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-hed">
                                            <h4>01</h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-btn">
                                            <button>Create Category</button>
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-right-detail inner-right-total-margin">
                                        <div class="dashboard-recover-two-sides-inner-right-detail-par">
                                            <p>Total registered users</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-hed">
                                            <h4>02</h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-btn">
                                            <button>Create Category</button>
                                        </div>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-right-detail inner-right-total-margin">
                                        <div class="dashboard-recover-two-sides-inner-right-detail-par">
                                            <p>Total daily visitors</p>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-hed">
                                            <h4>50</h4>
                                        </div>
                                        <div class="dashboard-recover-two-sides-inner-right-detail-btn">
                                            <button>Create Category</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="dashboard-recover-two-sides-inner-right-supports">
                                    <div class="dashboard-recover-two-sides-inner-right-suport-btn">
                                        <button>Contact Support</button>
                                    </div>
                                    <div class="dashboard-recover-two-sides-inner-right-suport-btn">
                                        <button>Live Chat With Team</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- purchase history -->
    <section class="dashboard-recover-purchase-history-sec">
        <div class="dashboard-recover-purchase-history-page">
            <div class="dashboard-recover-purchase-history-sec-head">
                <h2>{{__('Purchase History')}}</h2>
            </div>
            <div class="dashboard-recover-purchase-history-head-bottom">
                <div class="dashboard-recover-purchase-history-head-bottom-left">
                    <p>Latest Purchase (Showing 01 to {{count($orders)}} of {{count($orders)}} Purchase)</p>
                </div>
                <div class="dashboard-recover-purchase-history-head-bottom-right">
                    <label>Sort By:</label>
                    <input type="text" name="" placeholder="Date Created">
                </div>
            </div>
            <div class="dashboard-recover-purchase-history-data">
                <table class="dashboard-recover-purchase-history-data-table">
                <tr>
                    <th>{{__('ID')}}</th>
                    <th>{{__('Date')}}</th>
                    <th>{{__('Amount')}}</th>
                    <th>{{__('Delivery Status')}}</th>
                    <th>{{__('Payment Status')}}</th>
                    <th>{{__('Option')}}</th>
                </tr>
                @if (count($orders) > 0)
                    @foreach ($orders as $key => $order)
                        <tr>
                            <td><a href="#{{ $order->code }}">#{{ $order->code }}</a></td>
                            <td>{{ date('d-m-Y', $order->date) }}</td>
                            <td class="dashboard-recover-purchase-history-data-td-color">{{ single_price($order->grand_total) }}</td>
                            @php
                                $status = '';
                                if($order->orderDetails->first()){
                                    $status = $order->orderDetails->first()->delivery_status;
                                }
                                //$status = $order->orderDetails->first()->delivery_status;
                                // print_r($order->orderDetails->first());
                                // $status = $order->delivery_status;

                            @endphp
                            <td class="dashboard-recover-purchase-history-data-td-btn-1"><button>{{ ucfirst(str_replace('_', ' ', $status)) }}</button></td>
                            <!-- <td class="dashboard-recover-purchase-history-data-td-btn-2"><button>Complete</button></td> -->
                            <td>
                                @if ($order->payment_status == 'paid')
                                    <i class="bg-green"></i> {{__('Paid')}}
                                @else
                                    <i class="bg-red"></i> {{__('Unpaid')}}
                                @endif
                            </td>
                            <td>
                                <div class="dashboard-recover-dropout">
                                    <button class="dashboard-recover-more">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </button>
                                    <ul class="dashboard-recover-more-ul">
                                        <li>
                                            <button>{{__('Order Details')}}</button>
                                        </li>
                                        <li>
                                            <a href="{{ route('customer.invoice.download', $order->id) }}" class="dropdown-item">{{__('Download Invoice')}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </table>
            </div>
        </div>
    </section>

@endsection
