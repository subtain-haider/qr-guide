@extends('frontend.layouts.new_app_1')

@section('content')

    <section class="addpro-page-form-sec">
		<div class="addpropage">
			<h2 class="text-center">{{__('Add Shopify Store')}}</h2>
				<form class="" action="{{route('shopify.store')}}" method="POST" enctype="multipart/form-data">
				    @csrf
		<div class="input-group mb-3">
  <input type="text" class="form-control" placeholder="Shopify store name" aria-describedby="basic-addon2" name="shop">
  <div class="input-group-append">
      <button class="input-group-text btn">myshopify.com</button>
    <button class="input-group-text btn" style="background:#28a745; color:#fff;border: 1px solid #28a745;" id="basic-addon2">Confirm</button>
  </div>
</div>
</div>
</form>
		</div>
	</section>    

@endsection

@section('script')
   
<script type="text/javascript">
$(document).ready(function() { 

    $('#submit-the-form').click(function() {
        $('#the-form').submit();
    });

});



 $(function () {
 $('#the-form').on('submit', function(event) {
       
   
event.preventDefault();
        var customerData = {};
        customerData._token = $('#token').val();
        customerData.notify = $('#notify').val();
        customerData.userid = $('#userid').val();
        customerData.username = $('#username').val();
        customerData.useremail = $('#useremail').val();
        console.log(customerData);
        $.ajax({
            url: '{{ route("products.requestus") }}',
            type: 'POST',
            data: customerData,
            dataType: 'JSON',
            success: function (data) {
            
               swal("Your request has been submitted","Team will contact you ASAP","success");
            }

        })
    
});
});
 // $(function () {
	// $('#the-form').on('submit', function(event) {
  
	// 		event.preventDefault();
	// 		var notify = $('#notify').val();
 //        var email = $('#email').val();
 //        var userid = $('#userid').val();
 //        var username = $('#username').val();
 //        var useremail = $('#useremail').val();
 //        var token = $('#token').val();
      
	// 		$.ajax({
	// 		    type: "POST",
	// 		    url: "{{ route('products.requestus') }}",
	// 		    data: { email:email},		    
	// 		    dataType: "json",
	// 		    success: function(data){
	// 		        			    alert(data);
	// 		        			    //event.preventDefault();
	// 		        			    }
	// 		});
			 
	// 	});
	// });
      
    var category_name = "";
    var subcategory_name = "";
    var subsubcategory_name = "";

    var category_id = null;
    var subcategory_id = null;
    var subsubcategory_id = null;
    $(function () {
        // tabs
        	$(document).ready(function() {
	var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
	var add_button      = $(".add_field_button"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append('<span style="position:relative;"><input type="color" name="colors[]" style="height:33px; margin-bottom:0px; margin-left:20px;"/><a href="#" class="remove_field" style="position:absolute; right:-2px; top:-18px;"><i class="fa fa-times"><i/></a></span>'); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('span').remove(); x--;
	})
});
        $(".addnewpropriwrapper .addnewpropritab").click(function() {
			$(".addnewpropriwrapper .addnewpropritab button").removeClass('active');
			$(this).children().addClass('active');
            $(".addnewpropriwrapper .addnewtabpropri").removeClass("active").eq($(this).index()).addClass("addnewpropractive");
            $(".addnewprotab_item").hide().eq($(this).index()).fadeIn()
	    }).eq(0).children().addClass("active");


        $(document).ready(function(){
            $('#hideimgform1').click(function(){
                $('.hidedivform1').hide();
            });
            $('#hideimgform2').click(function(){
                $('.hidedivform2').hide();
            });
            $('#hideimgform3').click(function(){
                $('.hidedivform3').hide();
            });
            $('#hideimgform4').click(function(){
                $('.hidedivform4').hide();
            });
            $('#hideimgform5').click(function(){
                $('.hidedivform5').hide();
            });
            $('#hideimgformthumb').click(function(){
                $('.hidedivformthumb').hide();
            });
            $('#hideimgformmeta').click(function(){
                $('.hidedivformmeta').hide();
            });


            $('#category_id').on('change', function() {
                get_subcategories_by_category();
            });

            $('#subcategory_id').on('change', function() {
                get_subsubcategories_by_subcategory();
            });

            $('#subsubcategory_id').on('change', function() {
                get_brands_by_subsubcategory();
            });

            $("#photos").spartanMultiImagePicker({
                fieldName:        'photos[]',
                maxCount:         10,
                rowHeight:        '200px',
                groupClassName:   'col-md-4 col-sm-4 col-xs-6',
                maxFileSize:      '',
                dropFileLabel : "Drop Here",
                onExtensionErr : function(index, file){
                    console.log(index, file,  'extension err');
                    alert('Please only input png or jpg type file')
                },
                onSizeErr : function(index, file){
                    console.log(index, file,  'file size too big');
                    alert('File size too big');
                }
            });

            $("#thumbnail_img").spartanMultiImagePicker({
                fieldName:        'thumbnail_img',
                maxCount:         1,
                rowHeight:        '200px',
                groupClassName:   'col-md-4 col-sm-4 col-xs-6',
                maxFileSize:      '',
                dropFileLabel : "Drop Here",
                onExtensionErr : function(index, file){
                    console.log(index, file,  'extension err');
                    alert('Please only input png or jpg type file')
                },
                onSizeErr : function(index, file){
                    console.log(index, file,  'file size too big');
                    alert('File size too big');
                }
            });

            $("#meta_photo").spartanMultiImagePicker({
                fieldName:        'meta_img',
                maxCount:         1,
                rowHeight:        '200px',
                groupClassName:   'col-md-4 col-sm-4 col-xs-6',
                maxFileSize:      '',
                dropFileLabel : "Drop Here",
                onExtensionErr : function(index, file){
                    console.log(index, file,  'extension err');
                    alert('Please only input png or jpg type file')
                },
                onSizeErr : function(index, file){
                    console.log(index, file,  'file size too big');
                    alert('File size too big');
                }
            });

        });
        $(function () {
            $('.demo-select2').select2();
        });

		$(".upload-product").click(function() {
			$('#choice_form input, #choice_form select, #choice_form textarea').each(function() {
				var attr = $(this).attr('required');
				if (typeof attr !== 'undefined' && attr !== false && $(this).val() == '') {
					$('.addnewpropritab button').removeClass('active');
					$('.addnewprotab_item').hide();
					$(this).closest('.addnewprotab_item').show();
					var Buttonindex = $(this).closest('.addnewprotab_item').index();
					$('.addnewpropritab').eq(Buttonindex).children().addClass('active');
					return false;
				}
			});
		});
    });

    // $(document).ready(function() {
    //     $("#txtEditor").Editor();
    // });

    document.getElementById("pdfaddmore").onclick = function() {
        var container = document.getElementById("pdfconatiner");
        var section = document.getElementById("pdfshowsec");
        container.appendChild(section.cloneNode(true));
    }

    function get_subcategories_by_category(){
            var category_id = $('#category_id').val();
            $.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
                $('#subcategory_id').html(null);
                for (var i = 0; i < data.length; i++) {
                    $('#subcategory_id').append($('<option>', {
                        value: data[i].id,
                        text: data[i].name
                    }));
                    $('.demo-select2').select2();
                }
                get_subsubcategories_by_subcategory();
            });
        }

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
		    $('#subsubcategory_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		    get_brands_by_subsubcategory();
		});
	}
	function get_brands_by_subsubcategory(){
		var subsubcategory_id = $('#subsubcategory_id').val();
		$.post('{{ route('subsubcategories.get_brands_by_subsubcategory') }}',{_token:'{{ csrf_token() }}', subsubcategory_id:subsubcategory_id}, function(data){
		    $('#brand_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#brand_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		});
	}

	function product_form_submit() {
		var $fileUpload = $("input[name='photos[]']");
        if (parseInt($fileUpload.get(0).files.length) > 8){
            alert("You are only allowed to upload a maximum of 8 files");
            return false;
        }
        return true;
    }

    $("input[data-role=tagsinput]").tagsinput();


  
</script>
@endsection
