@extends('frontend.layouts.new_app_1')

<style type="text/css">
	.sugession-page-sec{
	display: flex;
	justify-content: center;
	padding: 50px 0px;
}
.sugession-page-wdth{
	width: 85%;
}
#suges-customers { 
  font-family: 'Play';
  border-collapse: collapse;
  width: 100%;
}

#suges-customers td, #suges-customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#suges-customers tr:nth-child(even){background-color: #f2f2f2;}

#suges-customers tr:hover {background-color: #ddd;}

#suges-customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #1099B1;
  color: white;
}
#suges-customers td button{
	border: none;
	outline: none;
	padding: 4px 30px;
	border-radius: 0px;
	
	font-size: 14px;
	font-family: 'Play';
}
#suges-customers td .suges-customers-btn-1{
	background-color: #000;
	color: #fff;
}
#suges-customers td .suges-customers-btn-2{
	background-color: red;
	color: #fff;
}
</style>

@section('content')
	
	 <!-- purchase history -->
    <section class="dashboard-recover-purchase-history-sec">
        <div class="dashboard-recover-purchase-history-page">
            <div class="dashboard-recover-purchase-history-sec-head">
                <h2>{{__('Shop Store History')}}</h2>
            </div>
            <div class="dashboard-recover-purchase-history-head-bottom">
                <div class="dashboard-recover-purchase-history-head-bottom-right">
                    <label>Sort By:</label>
                    <input type="text" name="" placeholder="Date Created">
                </div>
            </div>
            <div class="dashboard-recover-purchase-history-data">
                <table class="dashboard-recover-purchase-history-data-table">
                <tr>
                    <th>{{__('ID')}}</th>
                    <th>{{__('Store Name')}}</th>
                    <th>{{__('Created at')}}</th>
                    <!--<th>{{__('Amount')}}</th>-->
                    <!--<th>{{__('Delivery Status')}}</th>-->
                    <!--<th>{{__('Payment Status')}}</th>-->
                    <!--<th>{{__('Option')}}</th>-->
                    @foreach($shopifies as $key => $shopify)
                </tr>
                    <!--<th>{{ $loop->index + 1}}</th>-->
                    <td>{{ $loop->index + 1}}</td>
                    <td>{{$shopify->shop}}</td>
                    <td>{{$shopify->created_at}}</td>
                    <!--<th>{{__('Amount')}}</th>-->
                    <!--<th>{{__('Delivery Status')}}</th>-->
                    <!--<th>{{__('Payment Status')}}</th>-->
                    <!--<th>{{__('Option')}}</th>-->
                </tr>
                @endforeach
                </table>
            </div>
        </div>
    </section>

@endsection



@section('script')



@endsection