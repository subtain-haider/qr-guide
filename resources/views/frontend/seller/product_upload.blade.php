@extends('frontend.layouts.new_app_1')

@section('content')
<style type="text/css">
	.img-upload-preview img {width: 100%;}


	/*-- --------------------------------------single-product-page---------------------------------- */
.single-pro-page-sec{
	display: flex;
	justify-content: center;
	padding: 30px 0px;
	overflow: hidden;
}
.single-pro-page-wdth{
	width: 90%;
}
.single-pro-ban-data{
	display: flex;
	justify-content: space-between;
}
.single-pro-ban-detail-1{
	width: 34%;
}
.single-pro-ban-detail-2{
	width: 64%;
}
.single-pro-ban-detail-1 img{
	width: 100%;
}
input[type="number"] {
  -webkit-appearance: textfield;
    -moz-appearance: textfield;
          appearance: textfield;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none;
}

.number-input button {
  -webkit-appearance: none;
  background-color: transparent;
  border: none;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  margin: 0;
  position: relative;
}
.number-input button:before,
.number-input button:after {
  display: inline-block;
  position: absolute;
  content: '';
  height: 2px;
  transform: translate(-50%, -50%);
  margin-top: -6px;
}
.number-input button.plus:after {
  transform: translate(-50%, -50%) rotate(90deg);
}
.number-input input[type=number] {
  text-align: center;
}


.md-number-input.number-input {
 /* border: 2px solid #ddd;*/
  width: 9rem;
}
.md-number-input.number-input button {
  outline: none;
  width: 2rem;
  height: 2rem;
  padding-top: .8rem;
  background-color: rgba(0,0,0,0.15);
}

.md-number-input.number-input button.plus {
  padding-left: 2px;
}
.md-number-input.number-input button:before,
.md-number-input.number-input button:after {
  width: 1rem;
  background-color: #212121;
}
.md-number-input.number-input input[type=number] {
  max-width: 4rem;
  border:   solid #ddd;
  border-width: 0 2px;
  font-size: 18px;
  height: 2rem;
  outline: none;
}
@media not all and (min-resolution:.001dpcm)
{ @supports (-webkit-appearance:none) and (stroke-color:transparent) {
  .number-input.md-number-input.safari_only button:before, 
  .number-input.md-number-input.safari_only button:after {
    margin-top: -.6rem;
  }
}}

.single-pro-ban-head h2{
	margin-bottom: 2px;
	font-size: 28px;
	font-family: 'Play';
	font-weight: 600;
	
}
.single-pro-ban-icon{
	display: flex;
	flex-direction: row;
	justify-content: flex-end;
	margin-top: 30px;
}
.single-pro-ban-icon i{
	margin-left: 20px;
	color: #777;
	font-size: 20px;
	cursor: pointer;
}
.single-pro-ban-quant{
	display: flex;
	align-items: center;
	margin-top: 30px;
}
.single-pro-ban-des-hed h5{
	margin-bottom: 0px;
	font-size: 20px;
	font-family: 'Play';
}
.single-pro-ban-color{
	display: flex;
	align-items: center;
	margin-top: 20px;
}
.single-pro-ban-des-hed{
	width: 20%;
}
.single-pro-ban-des-hed-2{
	width: 17%;
}
.single-pro-ban-color-detail button{
	width: 40px;
	height: 40px;
	border: none;
}

.single-pro-ban-color-black{
	background-color: #000;
}
.single-pro-ban-color-red{
	background-color: #FF0000;
}
.single-pro-ban-color-sky{
	background-color: #198EA2;
}
.single-pro-ban-size{
	display: flex;
	align-items: center;
	margin-top: 20px;
}
.single-pro-ban-size-detail input{
	
	border: none;
}
.single-pro-ban-size-detail label{
	margin-right:10px;
	margin-left:20px;
}
.sngl-pr-clr{
	display:flex;
	align-items:center;
	flex-wrap:wrap;
}
.sngl-pr-clr input{
	margin-bottom: 0px;
	margin-left:20px;
}
.clr-sec-pro-frm{
	display:flex;
	align-itmes:center;
}
.clr-sec-pro-frm-wdth{
	width:20%;
}
.clr-sec-pro-frm-wdth h5{
	margin-bottom: 0px;
    font-size: 20px;
    font-family: 'Play';
}
.clr-sec-pro-frm-wdth-2{
	width:80%;
}
.bootstrap-tagsinput {
    height:35px !important;
}
.form-group{
	/*margin-bottom:0px!important;*/
}
</style>
    <section class="addpro-page-form-sec">
		<div class="addpropage">
			<h2 class="text-center">{{__('Add Product')}}</h2>
			<div class="addpro-page-form-sec-head">
				<h2>{{__('Product Information')}}</h2>
			</div>
			<div class="addnewpropriwrapper">
		    	<div class="addnewpropriwrapper-data">
		    		<div class="addnewpropriwrapper-detail-1">
					    <div class="addnewpropritabs">
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('General')}}</button></span>
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Images')}}</button></span>
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Videos')}}</button></span>
					        <!-- <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Meta Tags')}}</button></span> -->
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Price')}}</button></span>
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('External link')}}</button></span>
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Description')}}</button></span>
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Spacification')}}</button></span>
					        <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('Variations')}}</button></span>
					        <!-- <span class="addnewpropritab"><button class="addnewpropritabs-btn">{{__('PDF Specification')}}</button></span>  -->       
					    </div>
					</div>
					<div class="addnewpropriwrapper-detail-2">
					    <div class="addnewtab_content">
							<form class="" action="{{route('products.store')}}" onsubmit="return product_form_submit()" method="POST" enctype="multipart/form-data" id="choice_form">
								<!-- General-->
								<div class="addnewprotab_item">
									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="addpro-page-form-general-data">
												@csrf
												<input type="hidden" name="added_by" value="seller">
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Product Name')}}</label>
													<div class="addpro-page-form-img-general-2">
													<input type="text" class="form-control" name="name" required placeholder="{{__('Product Name')}}">
													</div>
												</div>
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('SKU')}}</label>
													<div class="addpro-page-form-img-general-2">
													<input type="text" class="form-control" name="sku" required placeholder="{{__('SKU')}}">
													</div>
												</div>
												
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('ASIN')}}</label>
													<div class="addpro-page-form-img-general-2">
													<input type="text" class="form-control" name="asin" required placeholder="{{__('ASIN')}}">
													</div>
												</div>

												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Category')}}</label>
													<div class="addpro-page-form-img-general-2">
													<select class="form-control  demo-select2-placeholder" name="category_id" id="category_id" required>
														<option value="">{{__('Please Select')}}</option>
														@foreach($categories as $category)
															<option value="{{$category->id}}">{{__($category->name)}}</option>
														@endforeach
													</select>
													<a href="#"  data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus" aria-hidden="true"> Add</i></a>
													</div>
												</div>
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Subcategory')}}</label>
													<div class="addpro-page-form-img-general-2">
														<select class="form-control demo-select2-placeholder" required name="subcategory_id" id="subcategory_id" data-live-search="true"></select>
														<a href="#"  data-toggle="modal" data-target="#exampleModal1"><i class="fa fa-plus" aria-hidden="true"> Add</i></a>
													</div>
													
												</div>
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Sub Subcategory')}}</label>
													<div class="addpro-page-form-img-general-2">
														<select class="form-control demo-select2-placeholder" required name="subsubcategory_id" id="subsubcategory_id" data-live-search="true"></select>
														<a href="#"  data-toggle="modal" data-target="#exampleModal2"><i class="fa fa-plus" aria-hidden="true"> Add</i></a>
													</div>
												</div>
												<!--<div class="form-group addpro-page-form-general-data-all">-->
												<!--	<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Brand (Optional)')}}</label>-->
												<!--	<div class="addpro-page-form-img-general-2">-->
												<!--		<select class="form-control demo-select2-placeholder" data-placeholder="Select a brand" id="brands" name="brand_id" data-live-search="true">-->
												<!--		<option value="-1">Please select</option>-->
												<!--	</select>-->
												<!--	</div>-->
												<!--</div>-->
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">{{__('Unit')}}</label>
													<div class="addpro-page-form-img-general-2">
													    <select class="form-control" name="unit" required>
													        <option disable>Select Unit (e.g. KG, Pc etc)</option>
													        <option value='kg'>KG</option>
													        <option value='pc'>PC</option>
													    </select>
													</div>
												</div>
												<div class="form-group addpro-page-form-general-data-all">
													<label for="" class="form-control-label addpro-page-form-general-detail-1">Tags</label>
													<div class="addpro-page-form-img-general-2">
													<input type="text" class="form-control" name="tags[]" value="tag" required placeholder="Type & hit enter" data-role="tagsinput" >
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Images -->
								<div class="addnewprotab_item">
									
									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="addpro-page-form-img-data-1-head">
												<p>Main Images (1080X1080)</p>
											</div>
											<div class="addpro-page-form-img-data-1">
												
												<div class="addpro-page-form-img-data-1-detail-all">

													<div id="photos"></div>
												</div>
											</div>
											<!-- thumbnail -->
											<div class="addpro-page-form-img-data-2">
												<div class="addpro-page-form-img-data-1-head">
													<p>Thumnnail Image(290X300)</p>
												</div>
												<div class="addpro-page-form-img-data-2-detail-all">

													<div id="thumbnail_img"></div>

												</div>
											</div>
										</div>
									</div>
									
								</div>
								<!-- Videos -->
								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="form-group addpro-page-form-img-data">
												<label for="" class="form-control-label addpro-page-form-img-detail-1">{{__('Video Provider')}}</label>
												<div class="addpro-page-form-img-detail-2">
												<select class="form-control demo-select2-placeholder" id="select-country" data-live-search="true" name="video_provider">
													<option value="youtube">{{__('Youtube')}}</option>
													<option value="dailymotion">{{__('Dailymotion')}}</option>
													<option value="vimeo">{{__('Vimeo')}}</option>
												</select>

												</div>
											</div><br>
											<div class="form-group addpro-page-form-img-data">
												<label for="" class="form-control-label addpro-page-form-img-detail-1">{{__('Video URL')}}</label>
												<div class="addpro-page-form-img-detail-2">
												<input type="text" class="form-control mb-3" name="video_link" placeholder="{{__('Video link')}}">
												<a href="javascript:;" onclick="formrequest()">You don’t have a video for you product? Press here to request one</a>
												</div>
											</div>
										</div>
									
									</div>
								
								</div>

								<!-- Meta Tags 
								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="form-group addpro-page-form-meta-data-all">
												<label for="" class="form-control-label addpro-page-form-meta-detail-1">{{__('Meta Title')}}</label>
												<div class="addpro-page-form-meta-detail-2">
												<input type="text" name="meta_title" required placeholder="Meta Title">
												</div>
											</div>
											<div class="form-group addpro-page-form-meta-data-all">
												<label for="" class="form-control-label addpro-page-form-meta-detail-1">{{__('Description')}}</label>
												<div class="addpro-page-form-meta-detail-2">
												<textarea name="meta_description" rows="8" class="form-control mb-3"></textarea>
												</div>
											</div>
											<div class="addpro-page-form-meta-data-2">
												<div class="addpro-page-form-meta-data-1-head">
													<p>Meta Image</p>
												</div>
												<div class="addpro-page-form-meta-data-2-detail-all">

													<div id="meta_photo"></div>

												</div>
											</div>
										</div>
									</div>
								
								</div>-->
								<!--Price-->
								<div class="addnewprotab_item">
									@php
									$currency_symbols = array(
											'AED' => '&#1583;.&#1573;', // ?
											'AFN' => '&#65;&#102;',
											'ALL' => '&#76;&#101;&#107;',
											'ANG' => '&#402;',
											'AOA' => '&#75;&#122;', // ?
											'ARS' => '&#36;',
											'AUD' => '&#36;',
											'AWG' => '&#402;',
											'AZN' => '&#1084;&#1072;&#1085;',
											'BAM' => '&#75;&#77;',
											'BBD' => '&#36;',
											'BDT' => '&#2547;', // ?
											'BGN' => '&#1083;&#1074;',
											'BHD' => '.&#1583;.&#1576;', // ?
											'BIF' => '&#70;&#66;&#117;', // ?
											'BMD' => '&#36;',
											'BND' => '&#36;',
											'BOB' => '&#36;&#98;',
											'BRL' => '&#82;&#36;',
											'BSD' => '&#36;',
											'BTN' => '&#78;&#117;&#46;', // ?
											'BWP' => '&#80;',
											'BYR' => '&#112;&#46;',
											'BZD' => '&#66;&#90;&#36;',
											'CAD' => '&#36;',
											'CDF' => '&#70;&#67;',
											'CHF' => '&#67;&#72;&#70;',
											'CLP' => '&#36;',
											'CNY' => '&#165;',
											'COP' => '&#36;',
											'CRC' => '&#8353;',
											'CUP' => '&#8396;',
											'CVE' => '&#36;', // ?
											'CZK' => '&#75;&#269;',
											'DJF' => '&#70;&#100;&#106;', // ?
											'DKK' => '&#107;&#114;',
											'DOP' => '&#82;&#68;&#36;',
											'DZD' => '&#1583;&#1580;', // ?
											'EGP' => '&#163;',
											'ETB' => '&#66;&#114;',
											'EUR' => '&#8364;',
											'FJD' => '&#36;',
											'FKP' => '&#163;',
											'GBP' => '&#163;',
											'GEL' => '&#4314;', // ?
											'GHS' => '&#162;',
											'GIP' => '&#163;',
											'GMD' => '&#68;', // ?
											'GNF' => '&#70;&#71;', // ?
											'GTQ' => '&#81;',
											'GYD' => '&#36;',
											'HKD' => '&#36;',
											'HNL' => '&#76;',
											'HRK' => '&#107;&#110;',
											'HTG' => '&#71;', // ?
											'HUF' => '&#70;&#116;',
											'IDR' => '&#82;&#112;',
											'ILS' => '&#8362;',
											'INR' => '&#8377;',
											'IQD' => '&#1593;.&#1583;', // ?
											'IRR' => '&#65020;',
											'ISK' => '&#107;&#114;',
											'JEP' => '&#163;',
											'JMD' => '&#74;&#36;',
											'JOD' => '&#74;&#68;', // ?
											'JPY' => '&#165;',
											'KES' => '&#75;&#83;&#104;', // ?
											'KGS' => '&#1083;&#1074;',
											'KHR' => '&#6107;',
											'KMF' => '&#67;&#70;', // ?
											'KPW' => '&#8361;',
											'KRW' => '&#8361;',
											'KWD' => '&#1583;.&#1603;', // ?
											'KYD' => '&#36;',
											'KZT' => '&#1083;&#1074;',
											'LAK' => '&#8365;',
											'LBP' => '&#163;',
											'LKR' => '&#8360;',
											'LRD' => '&#36;',
											'LSL' => '&#76;', // ?
											'LTL' => '&#76;&#116;',
											'LVL' => '&#76;&#115;',
											'LYD' => '&#1604;.&#1583;', // ?
											'MAD' => '&#1583;.&#1605;.', //?
											'MDL' => '&#76;',
											'MGA' => '&#65;&#114;', // ?
											'MKD' => '&#1076;&#1077;&#1085;',
											'MMK' => '&#75;',
											'MNT' => '&#8366;',
											'MOP' => '&#77;&#79;&#80;&#36;', // ?
											'MRO' => '&#85;&#77;', // ?
											'MUR' => '&#8360;', // ?
											'MVR' => '.&#1923;', // ?
											'MWK' => '&#77;&#75;',
											'MXN' => '&#36;',
											'MYR' => '&#82;&#77;',
											'MZN' => '&#77;&#84;',
											'NAD' => '&#36;',
											'NGN' => '&#8358;',
											'NIO' => '&#67;&#36;',
											'NOK' => '&#107;&#114;',
											'NPR' => '&#8360;',
											'NZD' => '&#36;',
											'OMR' => '&#65020;',
											'PAB' => '&#66;&#47;&#46;',
											'PEN' => '&#83;&#47;&#46;',
											'PGK' => '&#75;', // ?
											'PHP' => '&#8369;',
											'PKR' => '&#8360;',
											'PLN' => '&#122;&#322;',
											'PYG' => '&#71;&#115;',
											'QAR' => '&#65020;',
											'RON' => '&#108;&#101;&#105;',
											'RSD' => '&#1044;&#1080;&#1085;&#46;',
											'RUB' => '&#1088;&#1091;&#1073;',
											'RWF' => '&#1585;.&#1587;',
											'SAR' => '&#65020;',
											'SBD' => '&#36;',
											'SCR' => '&#8360;',
											'SDG' => '&#163;', // ?
											'SEK' => '&#107;&#114;',
											'SGD' => '&#36;',
											'SHP' => '&#163;',
											'SLL' => '&#76;&#101;', // ?
											'SOS' => '&#83;',
											'SRD' => '&#36;',
											'STD' => '&#68;&#98;', 
											'SVC' => '&#36;',
											'SYP' => '&#163;',
											'SZL' => '&#76;', 
											'THB' => '&#3647;',
											'TJS' => '&#84;&#74;&#83;',
											'TMT' => '&#109;',
											'TND' => '&#1583;.&#1578;',
											'TOP' => '&#84;&#36;',
											'TRY' => '&#8356;',
											'TTD' => '&#36;',
											'TWD' => '&#78;&#84;&#36;',
											'UAH' => '&#8372;',
											'UGX' => '&#85;&#83;&#104;',
											'USD' => '&#36;',
											'UYU' => '&#36;&#85;',
											'UZS' => '&#1083;&#1074;',
											'VEF' => '&#66;&#115;',
											'VND' => '&#8363;',
											'VUV' => '&#86;&#84;',
											'WST' => '&#87;&#83;&#36;',
											'XAF' => '&#70;&#67;&#70;&#65;',
											'XCD' => '&#36;',
											'XPF' => '&#70;',
											'YER' => '&#65020;',
											'ZAR' => '&#82;',
											'ZMK' => '&#90;&#75;', 
											'ZWL' => '&#90;&#36;',
										);
									@endphp
									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="form-group addpro-page-form-price-data-all">
												<label for="" class="form-control-label addpro-page-form-price-detail-1">Unit price</label>
												<div class="addpro-page-form-price-detail-2">
												<input type="number" min="0" value="0" step="0.01" placeholder="{{__('Unit price')}}" name="unit_price" class="form-control" required>
												</div>
												<div class="addpro-page-form-price-detail-3">
													<select class="form-control demo-select2-placeholder" name="currency" id="select-price" data-live-search="true">
														@foreach($currency_symbols as $key => $val)
														<option style="font-family:Arial;" {{ $key === "USD" ? "selected" : "Goodbye" }} value="{{$key}}">{{$key}} <?php echo $val; ?></option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="form-group addpro-page-form-price-data-all">
												<label for="" class="form-control-label addpro-page-form-price-detail-1">Purchase price</label>
												<div class="addpro-page-form-price-detail-2">
												<input type="number" min="0" value="0" step="0.01" placeholder="{{__('Purchase price')}}" name="purchase_price" class="form-control" required>
												</div>
												<div class="addpro-page-form-price-detail-3">
													<select class="form-control demo-select2-placeholder" name="currency1" id="select-price" data-live-search="true">
														@foreach($currency_symbols as $key => $val)
														<option style="font-family:Arial;" {{ $key === "USD" ? "selected" : "Goodbye" }} value="{{$key}}">{{$key}} <?php echo $val; ?></option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="form-group addpro-page-form-price-data-all">
												<label for="" class="form-control-label addpro-page-form-price-detail-1">Tax</label>
												<div class="addpro-page-form-price-detail-2">
												<input type="number" min="0" value="0" step="0.01" placeholder="{{__('Tax')}}" name="tax" class="form-control" required>
												</div>
												<div class="addpro-page-form-price-detail-3">
													<select class="form-control demo-select2-placeholder" id="select-price" data-live-search="true" name="tax_type">
														<option value="amount">$</option>
														<option value="percent">%</option>
													</select>
												</div>
											</div>
											<div class="form-group addpro-page-form-price-data-all">
												<label for="" class="form-control-label addpro-page-form-price-detail-1">Discount</label>
												<div class="addpro-page-form-price-detail-2">
												<input type="number" min="0" value="0" step="0.01" placeholder="{{__('Discount')}}" name="discount" class="form-control" required>
												</div>
												<div class="addpro-page-form-price-detail-3">
													<select class="form-control demo-select2-placeholder" id="select-price" data-live-search="true">
														<option value="amount">$</option>
														<option value="percent">%</option>
													</select>
												</div>
											</div>

											
										</div>
									</div>
								
								</div>
								<!-- External link -->
								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
										    <div class="form-group addpro-page-form-img-data">
												<label for="" class="form-control-label addpro-page-form-img-detail-1">{{__('Select Website')}}</label>
												<div class="addpro-page-form-img-detail-2">
												    <select name="external_website" class="form-control mb-3">
												        <option selected value="" >Select Website</option>
												        <option value="amazon" >Amazon</option>
												        <option value="ebay" >Ebay</option>
												    </select>
												</div>
											</div>
											<div class="form-group addpro-page-form-img-data">
												<label for="" class="form-control-label addpro-page-form-img-detail-1">{{__('Product Asin')}}</label>
												<div class="addpro-page-form-img-detail-2">
												<input type="text" class="form-control mb-3" name="external_link" placeholder="{{__('Product ASIN')}}">
												</div>
											</div>
										</div>
									</div>
								
								</div>
								<!-- Description -->
								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="container-fluid">
												<div class="row">
													<div class="container">
														<div class="row">
															<div class="col-lg-12 nopadding">
																<textarea id="txtEditor" class="editor" name="description"></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								
								</div>
								<!-- Spacifications -->
								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="container-fluid">
												<div class="row">
													<div class="container">
														<div class="row">
															<div class="col-lg-12 nopadding">
																<textarea id="txtEditor" class="editor" name="spacification"></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<!-- Variations -->

								<div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="container-fluid">
												<div class="row">
													<div class="container">
														<div class="row">
													
					<div class="col-lg-12 nopadding clr-sec-pro-frm-wdth-2">
					<div class="single-pro-ban-size">
						<div class="single-pro-ban-des-hed single-pro-ban-des-hed-2">
						<h5>Colors</h5>
					  </div>
						
						<div class="single-pro-ban-size-detail">
												<div class="input_fields_wrap sngl-pr-clr">
													<div class="form-group">
																    <button class="add_field_button btn btn-success">Add Color</button>
																    <!--<input type="color" name="colors[]" style="height: 33px; margin-left:20px; margin-bottom:0px;" required="">-->
						</div>			
						</div>
					</div>
														
			     </div>
<div class="single-pro-ban-size">
					<div class="single-pro-ban-des-hed single-pro-ban-des-hed-2">
						<h5>Size</h5>
					</div>
					<div class="single-pro-ban-size-detail">
						<label for="spro-small">Small</label><input type="checkbox" name="size[]" value="S" id="spro-small">
						<label for="spro-medium">Medium</label><input type="checkbox" name="size[]" value="M" id="spro-medium">
						<label for="spro-large">Large</label><input type="checkbox" name="size[]" value="L" id="spro-large">
						<label for="spro-extralarge">Extra Large</label><input type="checkbox" name="size[]" value="XL" id="spro-extralarge">
					</div>
				</div>

				<div class="single-pro-ban-quant">
					<div class="single-pro-ban-des-hed">
						<h5>Quantity</h5>
					</div>
					<div class="container px-0 mx-0">
						<div class="number-input md-number-input">
						  <button type="button" onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus"></button>
						  <input class="quantity" min="0" name="quantity" value="1" type="number">
						  <button type="button" onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></button>
						</div>    
					</div>
				</div>

											</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<!--PDF Specification  -->
								<!-- <div class="addnewprotab_item">

									<div class="addnewprotab_item-sec">
										<div class="addnewprotab_item-sec-page">
											<div class="form-group addpro-page-form-pdf-data-all">
												<div class="form-group addpro-page-form-pdf-data-1" id="pdfconatiner">
													<div class="form-group addpro-page-form-pdf-data-2" id="pdfshowsec">
														<label for="" class="form-control-label addpro-page-form-pdf-detail-1">PDF Specification</label>
														<div class="addpro-page-form-pdf-detail-2">
														<select class="form-control demo-select2-placeholder" id="select-pdf" data-live-search="true">
															<option data-tokens="china">English</option>
															<option data-tokens="malayasia">Bangla</option>
															<option data-tokens="malayasia">Arabic</option>
														</select>
														</div> -->
														<!-- allready hide -->
														<!-- <div class="form-group addpro-page-form-general-data-all">
															<div class="addpro-page-form-img-general-2">
																<input type="file" class="form-control" name="pdf" required>
															</div>
														</div> -->
														<!-- <div class="addpro-page-form-pdf-detail-3">
															<input type="file" class="form-control p-1" name="pdf" required>
														</div>
													</div>
												</div>
												<div class="addpro-page-form-pdf-detail-4">
													<p class="addpro-page-form-pdf-addmore" id="pdfaddmore">Add More</p>
												</div>
											</div>
										</div>
									</div>
								
								</div> -->
								<!-- end PDF Specification -->

								<div class="panel-footer text-right mt-3">
									<button type="submit" name="button" class="btn btn-info upload-product">Save</button>
								</div>
							</form>
					    </div>
					</div>
				</div>
			</div>
			<form id="the-form" method="post">
			
				<input type="hidden" id="notify" name="notify" value="Want new video">

				<input type="hidden" id="userid" name="userid" value="{{auth()->user()->id}}">
				<input type="hidden" id="username" name="username" value="{{auth()->user()->name}}">
				<input type="hidden" id="useremail" name="useremail" value="{{auth()->user()->email}}">
				<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

			</form>
			
		</div>
		<!-- Modal Cat -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Category Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form class="form-horizontal" action="{{ route('storecategories') }}" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
                
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">{{__('Name')}}</label>
                    <div class="col-sm-10">
                        <input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="banner">{{__('Banner')}} <small>(200x300)</small></label>
                    <div class="col-sm-10">
                        <input type="file" id="banner" name="banner" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="icon">{{__('Icon')}} <small>(32x32)</small></label>
                    <div class="col-sm-10">
                        <input type="file" id="icon" name="icon" class="form-control">
                    </div>
                </div>
            </div>
            
        
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="submit">{{__('Save')}}</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        <!-- Modal subCat -->
        <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Subcategory Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form class="form-horizontal" action="{{ route('storesubcategories') }}" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name">{{__('Name')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name">{{__('Category')}}</label>
                    <div class="col-sm-9">
                        <select name="category_id" required class="form-control demo-select2-placeholder">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{__($category->name)}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="submit">{{__('Save')}}</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        <!-- Modal sub subCat -->
        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sub Subcategory Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form class="form-horizontal" action="{{ route('storesubsubcategories') }}" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name">{{__('Name')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name">{{__('Category')}}</label>
                    <div class="col-sm-9">
                        <select name="category_id" id="category_id1" class="form-control demo-select2-placeholder" required>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{__($category->name)}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name">{{__('Subcategory')}}</label>
                    <div class="col-sm-9">
                        <select name="sub_category_id" id="sub_category_id" class="form-control demo-select2-placeholder" required>

                        </select>
                    </div>
                </div>
                
            </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="submit">{{__('Save')}}</button>
              </div>
              </form>
            </div>
          </div>
        </div>
	</section>    

@endsection

@section('script')
   
<script type="text/javascript">
$(document).ready(function() { 
get_subcategories_by_category1();
    $('#submit-the-form').click(function() {
        $('#the-form').submit();
    });

});
$('#category_id1').on('change', function() {
        get_subcategories_by_category1();
    });
function get_subcategories_by_category1(){
        var category_id = $('#category_id1').val();
        $.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
            $('#sub_category_id').html(null);
            for (var i = 0; i < data.length; i++) {
                $('#sub_category_id').append($('<option>', {
                    value: data[i].id,
                    text: data[i].name
                }));
                $('.demo-select2').select2();
            }
        });
    }
function formrequest(){
    
   
event.preventDefault();
        var customerData = {};
        customerData._token = $('#token').val();
        customerData.notify = $('#notify').val();
        customerData.userid = $('#userid').val();
        customerData.username = $('#username').val();
        customerData.useremail = $('#useremail').val();
        console.log(customerData);
        $.ajax({
            url: '{{ route("products.requestus") }}',
            type: 'POST',
            data: customerData,
            dataType: 'JSON',
            success: function (data) {
                
               swal("Your request has been submitted","Team will contact you ASAP","success");
            }

        })
}

 $(function () {
 $('#the-form').on('submit', function(event) {
       
   
event.preventDefault();
        var customerData = {};
        customerData._token = $('#token').val();
        customerData.notify = $('#notify').val();
        customerData.userid = $('#userid').val();
        customerData.username = $('#username').val();
        customerData.useremail = $('#useremail').val();
        console.log(customerData);
        $.ajax({
            url: '{{ route("products.requestus") }}',
            type: 'POST',
            data: customerData,
            dataType: 'JSON',
            success: function (data) {
            
               swal("Your request has been submitted","Team will contact you ASAP","success");
            }

        })
    
});
});
 // $(function () {
	// $('#the-form').on('submit', function(event) {
  
	// 		event.preventDefault();
	// 		var notify = $('#notify').val();
 //        var email = $('#email').val();
 //        var userid = $('#userid').val();
 //        var username = $('#username').val();
 //        var useremail = $('#useremail').val();
 //        var token = $('#token').val();
      
	// 		$.ajax({
	// 		    type: "POST",
	// 		    url: "{{ route('products.requestus') }}",
	// 		    data: { email:email},		    
	// 		    dataType: "json",
	// 		    success: function(data){
	// 		        			    alert(data);
	// 		        			    //event.preventDefault();
	// 		        			    }
	// 		});
			 
	// 	});
	// });
      
    var category_name = "";
    var subcategory_name = "";
    var subsubcategory_name = "";

    var category_id = null;
    var subcategory_id = null;
    var subsubcategory_id = null;
    $(function () {
        // tabs
        	$(document).ready(function() {
	var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
	var add_button      = $(".add_field_button"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append('<span style="position:relative;"><input type="color" name="colors[]" style="height:33px; margin-bottom:0px; margin-left:20px;"/><a href="#" class="remove_field" style="position:absolute; right:-2px; top:-18px;"><i class="fa fa-times"><i/></a></span>'); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('span').remove(); x--;
	})
});
        $(".addnewpropriwrapper .addnewpropritab").click(function() {
			$(".addnewpropriwrapper .addnewpropritab button").removeClass('active');
			$(this).children().addClass('active');
            $(".addnewpropriwrapper .addnewtabpropri").removeClass("active").eq($(this).index()).addClass("addnewpropractive");
            $(".addnewprotab_item").hide().eq($(this).index()).fadeIn()
	    }).eq(0).children().addClass("active");


        $(document).ready(function(){
            $('#hideimgform1').click(function(){
                $('.hidedivform1').hide();
            });
            $('#hideimgform2').click(function(){
                $('.hidedivform2').hide();
            });
            $('#hideimgform3').click(function(){
                $('.hidedivform3').hide();
            });
            $('#hideimgform4').click(function(){
                $('.hidedivform4').hide();
            });
            $('#hideimgform5').click(function(){
                $('.hidedivform5').hide();
            });
            $('#hideimgformthumb').click(function(){
                $('.hidedivformthumb').hide();
            });
            $('#hideimgformmeta').click(function(){
                $('.hidedivformmeta').hide();
            });


            $('#category_id').on('change', function() {
                get_subcategories_by_category();
            });

            $('#subcategory_id').on('change', function() {
                get_subsubcategories_by_subcategory();
            });

            $('#subsubcategory_id').on('change', function() {
                get_brands_by_subsubcategory();
            });
            var jq = $.noConflict();
            $("#photos").spartanMultiImagePicker({
                fieldName:        'photos[]',
                maxCount:         10,
                rowHeight:        '200px',
                groupClassName:   'col-md-4 col-sm-4 col-xs-6',
                maxFileSize:      '',
                dropFileLabel : "Drop Here",
                onExtensionErr : function(index, file){
                    console.log(index, file,  'extension err');
                    alert('Please only input png or jpg type file')
                },
                onSizeErr : function(index, file){
                    console.log(index, file,  'file size too big');
                    alert('File size too big');
                }
            });

            $("#thumbnail_img").spartanMultiImagePicker({
                fieldName:        'thumbnail_img',
                maxCount:         1,
                rowHeight:        '200px',
                groupClassName:   'col-md-4 col-sm-4 col-xs-6',
                maxFileSize:      '',
                dropFileLabel : "Drop Here",
                onExtensionErr : function(index, file){
                    console.log(index, file,  'extension err');
                    alert('Please only input png or jpg type file')
                },
                onSizeErr : function(index, file){
                    console.log(index, file,  'file size too big');
                    alert('File size too big');
                }
            });

            $("#meta_photo").spartanMultiImagePicker({
                fieldName:        'meta_img',
                maxCount:         1,
                rowHeight:        '200px',
                groupClassName:   'col-md-4 col-sm-4 col-xs-6',
                maxFileSize:      '',
                dropFileLabel : "Drop Here",
                onExtensionErr : function(index, file){
                    console.log(index, file,  'extension err');
                    alert('Please only input png or jpg type file')
                },
                onSizeErr : function(index, file){
                    console.log(index, file,  'file size too big');
                    alert('File size too big');
                }
            });

        });
        $(function () {
            $('.demo-select2').select2();
        });

		$(".upload-product").click(function() {
			$('#choice_form input, #choice_form select, #choice_form textarea').each(function() {
				var attr = $(this).attr('required');
				if (typeof attr !== 'undefined' && attr !== false && $(this).val() == '') {
					$('.addnewpropritab button').removeClass('active');
					$('.addnewprotab_item').hide();
					$(this).closest('.addnewprotab_item').show();
					var Buttonindex = $(this).closest('.addnewprotab_item').index();
					$('.addnewpropritab').eq(Buttonindex).children().addClass('active');
					return false;
				}
			});
		});
    });

    // $(document).ready(function() {
    //     $("#txtEditor").Editor();
    // });

    document.getElementById("pdfaddmore").onclick = function() {
        var container = document.getElementById("pdfconatiner");
        var section = document.getElementById("pdfshowsec");
        container.appendChild(section.cloneNode(true));
    }

    function get_subcategories_by_category(){
            var category_id = $('#category_id').val();
            $.post('{{ route('subcategories.get_subcategories_by_category') }}',{_token:'{{ csrf_token() }}', category_id:category_id}, function(data){
                $('#subcategory_id').html(null);
                for (var i = 0; i < data.length; i++) {
                    $('#subcategory_id').append($('<option>', {
                        value: data[i].id,
                        text: data[i].name
                    }));
                    $('.demo-select2').select2();
                }
                get_subsubcategories_by_subcategory();
            });
        }

	function get_subsubcategories_by_subcategory(){
		var subcategory_id = $('#subcategory_id').val();
		$.post('{{ route('subsubcategories.get_subsubcategories_by_subcategory') }}',{_token:'{{ csrf_token() }}', subcategory_id:subcategory_id}, function(data){
		    $('#subsubcategory_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#subsubcategory_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		    get_brands_by_subsubcategory();
		});
	}
	function get_brands_by_subsubcategory(){
		var subsubcategory_id = $('#subsubcategory_id').val();
		$.post('{{ route('subsubcategories.get_brands_by_subsubcategory') }}',{_token:'{{ csrf_token() }}', subsubcategory_id:subsubcategory_id}, function(data){
		    $('#brand_id').html(null);
		    for (var i = 0; i < data.length; i++) {
		        $('#brand_id').append($('<option>', {
		            value: data[i].id,
		            text: data[i].name
		        }));
		        $('.demo-select2').select2();
		    }
		});
	}

	function product_form_submit() {
		var $fileUpload = $("input[name='photos[]']");
        if (parseInt($fileUpload.get(0).files.length) > 8){
            alert("You are only allowed to upload a maximum of 8 files");
            return false;
        }
        return true;
    }

    $("input[data-role=tagsinput]").tagsinput();


  
</script>
@endsection
