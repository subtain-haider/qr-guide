@extends('frontend.layouts.app')

@section('content')

    <section class="gry-bg py-4 profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @include('frontend.inc.seller_side_nav')
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                        {{__('Questions')}}
                                    </h2>
                                </div>
                                <div class="col-md-6">
                                    <div class="float-md-right">
                                        <ul class="breadcrumb">
                                            <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                                            <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                                            <li class="active"><a href="{{ route('question.index') }}">{{__('Questions')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if (count($questions) > 0)
                            <!-- Order history table -->
                            <div class="card no-border mt-4">
                                <div>
                                    <table class="table table-sm table-hover table-responsive-md">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{__('Product')}}</th>
                                                <th>{{__('Customer')}}</th>
                                                <th>{{__('Question')}}</th>
                                                <th>{{__('Answers')}}</th>
                                                <th>{{__('Create Date')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($questions as $key => $question)
                                                @if($question->product_id != null && $question->user_id != null)
                                                    <tr>
                                                        <td>
                                                            {{ $key+1 }}
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('product', \App\Models\Product::where('id', $question->product_id)->first()->slug) }}" target="_blank">{{ __(\App\Models\Product::where('id', $question->product_id)->first()->name) }}</a>
                                                        </td>
                                                        <td>{{ \App\Models\User::where('id', $question->user_id)->first()->name }} ({{ \App\Models\User::where('id', $question->user_id)->first()->email }})</td>
                                                        <td><a href="{{ route('answer', [$question->id]) }}">{{ $question->comment }}</a></td>
                                                        <td>{{\App\Models\Question::where('parent_id', $question->id)->count()}}</td>
                                                        <td>{{ date('Y-m-d', strtotime($question->created_at)) }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif

                        {{-- <div class="pagination-wrapper py-4">
                            <ul class="pagination justify-content-end">
                                {{ $questions->links() }}
                            </ul>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@php	$policies_class = ' d-none d-lg-block';@endphp
