@extends('frontend.layouts.new_app_1')
@section('content')

 <section class="signin-form-sec pt-5">
	<div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
		<div class="page-title col-lg-12 mb-4">
                   <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                       {{$shop->name}} {{__('Warranty')}}
                   </h2>
                </div>
                @if($shop->warranty_content != null)
                <div class="col-lg-6">
                    {!! $shop->warranty_content !!}
                </div>
                @endif
        <div class="signin-form @if($shop->warranty_content != null) col-lg-6 @else col-lg-6 @endif">
            <form action="{{ route('company_warranty_submits', $shop->slug) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="signin-form-sec-logo mb-4">
                    <a href="javascript:void();">@if($shop->logo != NULL)<img style="width: 135px; height: 135px;" src="{{ asset('uploads/photos/7Cbnap0wlm62l9JacxhSSXHyWQ3XkWcX7cC2KE0w.png') }}">@else <img style="width: 135px; height: 135px;" src="{{ asset('frontend/images/rounded-logo.png') }}"> @endif</a>
                </div>
                <h2 class="mb-5">{{__('CLAIM WARRANTY')}}</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control mb-3" placeholder="*DATE OF PURCHASE" name="date_of_purchase" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control mb-3" placeholder="*DATE OF PRODUCT ENDING" name="date_of_product_end" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control mb-3" placeholder="*CLIENT NAME" name="client_name" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control mb-3" placeholder="*SQ OF PRODUCT" name="sq_of_product" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control mb-3" placeholder="*PRODUCT SERIAL NUMBER" name="product_serial_number" required>
                        </div>
                    </div>
                <div class="form-group">

                    <button type="submit" class="btn btn-success btn-lg btn-block">{{__('Download')}}</button>
                    <button type="button" class="btn btn-danger btn-lg btn-block">{{__('Apply')}}</button>
                    @if (\Session::has('msg'))
                    <div class="alert alert-danger" role="alert">
                      {!! \Session::get('msg') !!}
                    </div>
                    @endif
                </div>
            </form>
        </div>
    </section>
@endsection