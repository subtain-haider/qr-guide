@extends('frontend.layouts.new_app_1')

@section('content')

    <section class="product-guide">
        <div class="product-guide-flex-1">
            <div class="product-guide-page">
                <h1>{{__('Product guides')}}</h1>
                <div class="product-guide-flex-1 pg-bg">
                    <div class="product-guide-page">
                        <div class="pg-inner">
                            <div class="pg-inner-s">
                                <p>{{__('search')}}</p>
                                <input type="name" name="search">
                            </div>
                            <div class="pg-inner-s">
                                <p>category</p>
                                <select class="form-control demo-select2" data-live-search="true">
                                    <option data-tokens="china">Select Category</option>
                                    <option data-tokens="malayasia">Dailymotion</option>
                                    <option data-tokens="singapore">Vimeo</option>
                                </select>
                            </div>
                            <div class="pg-inner-s">
                                <p>brand</p>
                                <select class="form-control demo-select2" data-live-search="true">
                                    <option data-tokens="china">Select Category</option>
                                    <option data-tokens="malayasia">Dailymotion</option>
                                    <option data-tokens="singapore">Vimeo</option>
                                </select>
                            </div>
                            <div class="pg-inner-s">
                                <p>sort by</p>
                                <select class="form-control demo-select2" data-live-search="true">
                                    <option data-tokens="china">Select Category</option>
                                    <option data-tokens="malayasia">Dailymotion</option>
                                    <option data-tokens="singapore">Vimeo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="product-guide-flex-1 ">
                    <div class="pg-inners">
                        @foreach ($products as $key => $product)
                            @php
                                $main_photo = '';
                                if(!empty($product->photos)){
                                    $photos = json_decode($product->photos);
                                    $main_photo = $photos[0];
                                }
                            @endphp
                            <div class="pg-inners-s pg-1">
                                <div class="pg-inner-width-s">
                                    {{-- <img src="{{asset('storage/products/photos/'.$product->photo)}}"> --}}
                                    <img src="@if($product->thumbnail_img != ''){{ asset($product->thumbnail_img) }} @elseif($product->featured_img != ''){{ asset($product->featured_img) }} @elseif($product->flash_deal_img != ''){{ asset($product->flash_deal_img) }} @elseif($main_photo != ''){{ asset($main_photo) }} @else {{ asset('img/no-image-available.png') }} @endif">
                                </div>
                                <div class="pg-box pgs-1">
                                    <h3><a href="{{ route('singleproduct', $product->slug) }}">{{ __($product->name) }}</a></h3>
                                    <div>
                                        <p>{{ $product->asin }}</p>
                                        <div>
                                            <!-- <i class="fa fa-star pg-icon"></i>
                                            <i class="fa fa-star pg-icon"></i>
                                            <i class="fa fa-star pg-icon"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i> -->
                                            {{-- {{ renderStarRating($product->rating) }} --}}
                                        </div>
                                    </div>
                                    <span>
                                        <form id="option-choice-form_{{ $product->id }}" class="d-none d-lg-block">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $product->id }}">
                                            <input type="text" name="quantity" class="form-control input-number text-center d-none" placeholder="1" value="1" min="1" max="10">
                                        </form>
                                     
                                   
                                        <a href="<?php $product->slug?>qrguide.com">Buy Now</a>
                                    </span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
<script type="text/javascript">
    $(function () {
        $('.demo-select2').select2();
    });
    function linkclick(val) {
        alert(val);
    }
    
</script>
@endsection
