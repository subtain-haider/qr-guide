@extends('frontend.layouts.app')

@section('content')

    <section class="gry-bg py-4 profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-9 mx-auto">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                        {{__('Company Informations')}}
                                    </h2>
                                </div>
                                <div class="col-md-6">
                                    <div class="float-md-right">
                                        <ul class="breadcrumb">
                                            <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                                            <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                                            <li class="active"><a href="#">{{__('Create Company')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form class="" action="{{ route('payment.checkout') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="plan" value="{{$id}}">
                            @if (!Auth::check())
                                <div class="form-box bg-white mt-4">
                                    <div class="form-box-title px-3 py-2">
                                        {{__('User Info')}}
                                    </div>
                                    <div class="form-box-content p-3">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <div class="input-group input-group--style-1">
                                                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{\App\Models\PricingPlan::where('id', $id)->first()->name}}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <div class="input-group input-group--style-1">
                                                        <input type="text" class="form-control" value="${{\App\Models\PricingPlan::where('id', $id)->first()->per_month}}/m" readonly name="plan_for">
                                                        {{-- <select name="plan_for" class="form-control select2">
                                                            <option>Please Select</option>
                                                            @if(!empty(\App\Models\PricingPlan::where('id', $id)->first()->per_month))
                                                                <option name="month" value="month" selected disabled>${{\App\Models\PricingPlan::where('id', $id)->first()->per_month}}/m</option>
                                                            @endif
                                                            @if(!empty(\App\Models\PricingPlan::where('id', $id)->first()->per_year))
                                                                <option name="year" value="year" >${{\App\Models\PricingPlan::where('id', $id)->first()->per_year}}/y</option>
                                                            @endif
                                                        </select> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <!-- <label>{{ __('Name') }}</label> -->
                                                    <div class="input-group input-group--style-1">
                                                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" placeholder="{{ __('Name') }}" name="name">
                                                        <span class="input-group-addon">
                                                            <i class="text-md la la-user"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <!-- <label>{{ __('Email') }}</label> -->
                                                    <div class="input-group input-group--style-1">
                                                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{ __('Email') }}" name="email">
                                                        <span class="input-group-addon">
                                                            <i class="text-md la la-envelope"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <!-- <label>{{ __('Password') }}</label> -->
                                                    <div class="input-group input-group--style-1">
                                                        <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Password') }}" name="password">
                                                        <span class="input-group-addon">
                                                            <i class="text-md la la-lock"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <!-- <label>{{ __('Confirm Password') }}</label> -->
                                                    <div class="input-group input-group--style-1">
                                                        <input type="password" class="form-control" placeholder="{{ __('Confirm Password') }}" name="password_confirmation">
                                                        <span class="input-group-addon">
                                                            <i class="text-md la la-lock"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="form-box bg-white mt-4">
                                <div class="form-box-title px-3 py-2">
                                    {{__('Basic Info')}}
                                </div>
                                <div class="form-box-content p-3">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Company Name')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Company Name')}}" name="name" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Logo')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="file" name="logo" id="file-2" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                                            <label for="file-2" class="mw-100 mb-3">
                                                <span></span>
                                                <strong>
                                                    <i class="fa fa-upload"></i>
                                                    {{__('Choose image')}}
                                                </strong>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>{{__('Address')}} <span class="required-star">*</span></label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control mb-3" placeholder="{{__('Address')}}" name="address" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-md-2">
                                                <label>{{__('Contact No')}} <span class="required-star">*</span></label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <select name="calling_code" required>
                                                                @foreach(\App\Models\Country::all() as $Country)
                                                                    @if($Country->phone != '')
                                                                        <option value="{{$Country->code}}"@if($Country->code == 'PK') selected @endif>+{{str_replace('+','',$Country->phone)}}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" id="whatsapp_number" placeholder="{{__('(000) 0000000')}}" name="whatsapp_number" required>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="form-box bg-white mt-4">
                                <div class="card">
                                    <div class="card-title px-4">
                                        <h3 class="heading heading-5 strong-500">
                                            {{__('Select a payment option')}}
                                        </h3>
                                    </div>
                                    <div class="card-body text-center">
                                        <ul class="inline-links">
                                            @if (\App\Models\BusinessSetting::where('type', 'paypal_payment')->first()->value == 1)
                                                <li>
                                                    <label class="payment_option" onclick="payment_option('paypal_payment')">
                                                        <input type="radio" id="" name="payment_option" value="paypal" checked>
                                                        <span>
                                                            <img src="{{ asset('frontend/images/icons/cards/paypal.png')}}" class="img-fluid">
                                                        </span>
                                                    </label>
                                                </li>
                                            @endif
                                            @if(\App\Models\BusinessSetting::where('type', 'stripe_payment')->first()->value == 1)
                                                <li>
                                                    <label class="payment_option" onclick="payment_option('stripe_payment')">
                                                        <input type="radio" id="" name="payment_option" value="stripe" checked>
                                                        <span>
                                                            <img src="{{ asset('frontend/images/icons/cards/stripe.png')}}" class="img-fluid">
                                                        </span>
                                                    </label>
                                                </li>
                                            @endif
                                            @if(\App\Models\BusinessSetting::where('type', 'sslcommerz_payment')->first()->value == 1)
                                                <li>
                                                    <label class="payment_option" onclick="payment_option('sslcommerz_payment')">
                                                        <input type="radio" id="" name="payment_option" value="sslcommerz" checked>
                                                        <span>
                                                            <img src="{{ asset('frontend/images/icons/cards/sslcommerz.png')}}" class="img-fluid">
                                                        </span>
                                                    </label>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right mt-4">
                                <button type="submit" class="btn btn-styled btn-base-1">{{__('Save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script>
        document.getElementById('whatsapp_number').addEventListener('input', function (e) {
            var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
            e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? x[3] : '');
        });
    </script>
@endsection
