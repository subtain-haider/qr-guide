@extends('frontend.layouts.new_app_1')
<style type="text/css">
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
	.sugession-page-sec{
	display: flex;
	justify-content: center;
	padding: 50px 0px;
}
.sugession-page-wdth{
	width: 85%;
}
#suges-customers {
  font-family: 'Play';
  border-collapse: collapse;
  width: 100%;
}

#suges-customers td, #suges-customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#suges-customers tr:nth-child(even){background-color: #f2f2f2;}

#suges-customers tr:hover {background-color: #ddd;}

#suges-customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #1099B1;
  color: white;
}
#suges-customers td button{
	border: none;
	outline: none;
	padding: 4px 30px;
	border-radius: 0px;
	
	font-size: 14px;
	font-family: 'Play';
}
#suges-customers td .suges-customers-btn-1{
	background-color: #000;
	color: #fff;
}
#suges-customers td .suges-customers-btn-2{
	background-color: red;
	color: #fff;
}
</style>
@section('content')

@php

    $status = $order->delivery_status;

    $payment_status = $order->payment_status;

@endphp
<section class="sugession-page-sec">
	<div class="sugession-page-wdth">
		<div class="sugession-page-data">
            <div class="panel">
    	<div class="panel-body">
    		<div class="invoice-masthead">
    			{{-- <div class="offset-lg-2 col-lg-4 col-sm-6">

					<div class="form-inline">

						<label class="my-2" >{{__('Payment Status')}}</label>
						<select class="form-control selectpicker form-control-sm"  data-minimum-results-for-search="Infinity" id="update_payment_status">

							<option value="unpaid" @if ($payment_status == 'unpaid') selected @endif>{{__('Unpaid')}}</option>

							<option value="paid" @if ($payment_status == 'paid') selected @endif>{{__('Paid')}}</option>

						</select>

					</div>

				</div>

				<div class="col-lg-4 col-sm-6">

					<div class="form-inline">

						<label class="my-2" >{{__('Delivery Status')}}</label>
						<select class="form-control selectpicker form-control-sm"  data-minimum-results-for-search="Infinity" id="update_delivery_status">

							<option value="pending" @if ($status == 'pending') selected @endif>{{__('Pending')}}</option>

							<option value="on_review" @if ($status == 'on_review') selected @endif>{{__('On review')}}</option>

							<option value="on_delivery" @if ($status == 'on_delivery') selected @endif>{{__('On delivery')}}</option>

							<option value="delivered" @if ($status == 'delivered') selected @endif>{{__('Delivered')}}</option>

						</select>

					</div>

				</div> --}}
				<div class="invoice-text">
    				<h3 class="h1 text-thin mar-no text-primary">{{ __('Order Details') }}</h3>
    			</div>
    		</div>
    		<div class="invoice-bill row">
    			<div class="col-sm-6 text-xs-center">
    				<address>
        				<strong class="text-main">{{ json_decode($order->shipping_address)->name }}</strong><br>
                         {{ json_decode($order->shipping_address)->email }}<br>
        				 {{ json_decode($order->shipping_address)->address }}, {{ json_decode($order->shipping_address)->city }}, {{ json_decode($order->shipping_address)->country }}
                    </address>
    			</div>
    			<div class="col-sm-6 text-xs-center">
    				<table class="invoice-details">
    				<tbody>
    				<tr>
    					<td class="text-main text-bold">
    						{{__('Order #')}}
    					</td>
    					<td class="text-right text-info text-bold">
    						{{ $order->code }}
    					</td>
    				</tr>
    				<tr>
    					<td class="text-main text-bold">
    						{{__('Order Status')}}
    					</td>
                        @php
                            $status = $order->delivery_status;
							if(empty($status)){
								$status = 'pending';
							}
                        @endphp
    					<td class="text-right">
                            @if($status == 'delivered')
                                <span class="badge badge-success">{{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                            @else
                                <span class="badge badge-info">{{ ucfirst(str_replace('_', ' ', $status)) }}</span>
                            @endif
    					</td>
    				</tr>
    				<tr>
    					<td class="text-main text-bold">
    						{{__('Order Date')}}
    					</td>
    					<td class="text-right">
    						{{ date('d-m-Y h:i A', $order->date) }} (UTC)
    					</td>
    				</tr>
                    <tr>
    					<td class="text-main text-bold">
    						{{__('Total amount')}}
    					</td>
    					<td class="text-right">
    						{{ single_price($order->orderDetails->sum('price') + $order->orderDetails->sum('tax')) }}
    					</td>
    				</tr>
                    <tr>
    					<td class="text-main text-bold">
    						{{__('Payment method')}}
    					</td>
    					<td class="text-right">
    						{{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}
    					</td>
    				</tr>
    				</tbody>
    				</table>
    			</div>
    		</div>
    		<hr class="new-section-sm bord-no">
    		<div class="row">
    			<div class="col-lg-12 table-responsive">
    				<table class="table table-bordered invoice-summary">
        				<thead>
            				<tr class="bg-trans-dark">
                                <th class="min-col">#</th>
            					<th class="text-uppercase">
            						{{__('Description')}}
            					</th>
            					<th class="min-col text-center text-uppercase">
            						{{__('Qty')}}
            					</th>
            					<th class="min-col text-center text-uppercase">
            						{{__('Price')}}
            					</th>
            					<th class="min-col text-right text-uppercase">
            						{{__('Total')}}
            					</th>
            				</tr>
        				</thead>
        				<tbody>
                            @foreach ($order->orderDetails as $key => $orderDetail)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                					<td>
                						<strong><a href="{{ route('singleproduct', $orderDetail->product->slug) }}" target="_blank">{{ $orderDetail->product->name }}</a></strong>
                						<small>{{ $orderDetail->variation }}</small>
                					</td>
                					<td class="text-center">
                						{{ $orderDetail->quantity }}
                					</td>
                					<td class="text-center">
                						{{ single_price($orderDetail->price/$orderDetail->quantity) }}
                					</td>
                                    <td class="text-center">
                						{{ single_price($orderDetail->price) }}
                					</td>
                				</tr>
                            @endforeach
        				</tbody>
    				</table>
    			</div>
    		</div>
    		<div class="clearfix">
    			<table class="table invoice-total">
    			<tbody>
    			<tr>
    				<td>
    					<strong>{{__('Sub Total')}} :</strong>
    				</td>
    				<td>
    					{{ single_price($order->orderDetails->sum('price')) }}
    				</td>
    			</tr>
    			<tr>
    				<td>
    					<strong>{{__('Tax')}} :</strong>
    				</td>
    				<td>
    					{{ single_price($order->orderDetails->sum('tax')) }}
    				</td>
    			</tr>
                <tr>
    				<td>
    					<strong>{{__('Shipping')}} :</strong>
    				</td>
    				<td>
    					{{ single_price($order->orderDetails->sum('shipping_cost')) }}
    				</td>
    			</tr>
    			<tr>
    				<td>
    					<strong>{{__('TOTAL')}} :</strong>
    				</td>
    				<td class="text-bold h4">
    					{{ single_price($order->grand_total) }}
    				</td>
    			</tr>
    			</tbody>
    			</table>
    		</div>
    		<div class="text-right no-print">
    			<a href="{{ route('customer.invoice.download', $order->id) }}" class="btn btn-success"><i class="demo-pli-printer icon-lg"></i></a>
    		</div>
    	</div>
    </div>
        </div>
    </div>
</section>
@endsection

<script type="text/javascript">

    $('#update_delivery_status').on('change', function(){

        var order_id = {{ $order->id }};

        var status = $('#update_delivery_status').val();

        $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}',order_id:order_id,status:status,update_by:'admin'}, function(data){

            $('#order_details').modal('hide');

            showFrontendAlert('success', 'Order status has been updated');

            location.reload().setTimeOut(500);

        });

    });



    $('#update_payment_status').on('change', function(){

        var order_id = {{ $order->id }};

        var status = $('#update_payment_status').val();

        $.post('{{ route('orders.update_payment_status') }}', {_token:'{{ @csrf_token() }}',order_id:order_id,status:status,update_by:'admin'}, function(data){

            $('#order_details').modal('hide');

            //console.log(data);

            showFrontendAlert('success', 'Payment status has been updated');

            location.reload().setTimeOut(500);

        });

    });

</script>