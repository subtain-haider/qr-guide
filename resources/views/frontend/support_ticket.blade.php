@extends('frontend.layouts.new_app_1')

<style type="text/css">
<!-- All css -->
 
body{
    background-color: #F7F7F7;
}
* {
    margin: 0;
    padding: 0;
    box-sizing: 0;
}
.support-ticket{
    padding: 50px 0px;
}    
.spt-flex-1{
    display: flex;
    justify-content: center;
}
.spt-flex-2{
    display: flex;
    justify-content: space-between;
}
.spt-page {
    width: 80%;
}
.spt-pg-inner  {
    width: 100%;
}
.spt-inners-pg-1 {
    width: 100%;
}
.spt-inners-pg-1 h3 {
    text-align: center;
    font-weight: bolder;
    font-size: 30px;
    margin-bottom: 20px;
}
.spt-inners-pg-2 table {
    width: 100%;
}
.spt-inners-pg-2 thead th {
    background-color: #000000;
    color:#fff;
    padding: 5px 0px 5px 10px;
    border-right: 2px solid #F7F7F7;
    font-size: 16px;
    font-weight: 400;
}
.spt-custom-th {
    background-color: #1B9FB6!important;
}
.spt-inners-pg-2  table {
    width: 100%;
    box-shadow: #e3e3e3 0px 0px 12px 0px;
    overflow-x: auto;
}
.spt-tr-cst {
    border-bottom: 1px solid #000000;
    margin-bottom: 5px;
    background-color: #fff;
}
.spt-tr-cst th {
    border-right: 2px solid #F7F7F7;
    padding: 20px 0px 20px 10px;
    font-size: 16px;
    font-weight: bold;
}
.spt-tr-cst  button{
    background-color: #FC6E62;
    color: #fff;
    border:unset;
    padding: 1px 30px 1px 30px;
    font-size: 16px;
    font-weight: 400;
    outline: unset;
}
.spt-tr-cst  button a{
    color: #fff;
    font-size: 16px;
    font-weight: 400;
}
.spt-inners-pg-3 {
    display: flex;
    justify-content: center;
    width: 100%;
    padding-top: 10px;
}
.spt-inners-pg-3 button{
    width: 100%;
    text-align: center;
    border: unset;
    background-color: #D1EAEF;
    padding: 25px 0px;
}
.spt-inners-pg-3 button:hover{
    width: 100%;
    text-align: center;
    border: unset;
    background-color: #D1EAEF;
    padding: 25px 0px;
}
.spt-inners-pg-3 button:focus{
    width: 100%;
    text-align: center;
    border: unset;
    background-color: #D1EAEF;
    padding: 25px 0px;
}
.spt-inners-pg-3 button a{
    color: #000;
    font-size: 20px;
    font-weight: bold;
}
/* modal */
.mdt-cst {
    background-color: #000;
    width: 100%;
    color: #fff;
    padding:0px 0px 5px 10px;
    font-weight: 400;
    font-size: 19px!important;
}
.mdt-cst-btn {
    background-color: #FC6E61!important;
    color: #fff!important;
    padding: 1px 10px 4px 10px!important;
    margin: unset!important;
    line-height: unset!important;
    font-size: 19px!important;
}
.mdt-cst-btn span{
    background-color: #FC6E61!important;
    color: #fff!important;
    line-height: 1.5!important;
}
.spt-modal-inner {
    display: flex;
    justify-content: center;
    width: 100%;
}
.spt-modal-inner input {
    width: 100%;
    padding: 6px 0px 6px 8px;
    border:1px solid #1B9FB6;
    outline-color: #1B9FB6;
}
.cst-btn-modal-1 {
    background-color: #000!important;
    color: #fff!important;
    padding: 2px 35px!important;
    border-radius: 2px!important;
}
.cst-btn-modal-2 {
    background-color: #1B9FB6!important;
    color: #fff!important;
    padding: 2px 35px!important;
    border-radius: 2px!important;
}
.border-none {
    border-bottom: none!important;
    border-top: none!important;
}

/* text box */
		.editor
        {
			border:solid 1px #ccc;
			padding: 20px;
			min-height:200px;
        }

        .sample-toolbar
        {
			border:solid 1px #ddd;
			background:#f4f4f4;
			padding: 5px 5px 5px 5px;
			border-radius:3px;
            margin-top: 15px;
        }

        .sample-toolbar > span
        {
			cursor:pointer;
		}

        .sample-toolbar > span:hover
        {
			text-decoration:underline;
		}
@media screen and (max-width: 768px) {
    .spt-inners-pg-2 table{
        width: 600px;
    }
    .spt-inners-pg-2 {
        overflow: auto;
    }
    
}
</style>

@php
            $PackageInfo = \App\Models\User::where('package', Auth::user()->package)->first();
            $PP = Auth::user()->package_for;
            $Permissions = json_decode($PackageInfo->package_for);
        @endphp
@section('content')
<section class="dashboard-recover-sub-links-sec" id="dashrecoversublinks">
        <div class="dashboard-recover-two-sides-left-all">
            <div class="dashboard-recover-two-sides-left-all-ul">
                           <ul class="dashboard-recover-two-sides-left-links">
                <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                <!-- <li><a href="{{ route('company.index') }}">{{__('Create Mini Site')}}</a></li> -->
                <li><a href="#">{{__('Mini Sites')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('company.index') }}">{{__('Mini Sites')}}</a></li>
                        <li><a href="{{ route('company.create') }}">{{__('Create Mini Site')}}</a></li>
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                
               
                        
                <li><a href="#">{{__('Products')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('seller.products') }}">{{__('All Products and Guides')}}</a></li>
                        <li><a href="{{ route('seller.products.upload')}}">{{__('Add Product to Sell')}}</a></li>
                        @if(in_array(3,$Permissions))
                        <li><a href="{{ route('seller.guide.upload')}}">{{__('Add Products Guides')}}</a></li>
                        @endif
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                <li><a href="#">{{__('Shopify Store')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{route('shopify.index')}}">{{__('All Shopify Store')}}</a></li>
                        <li><a href="{{route('shopify.create')}}">{{__('Add Shopify Store')}}</a></li>
                        
                        <!-- <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> -->
                    </ul>
                </li>
                <li><a href="{{route('woocommerce.index')}}">{{__('WooCommerce')}}</a></li>
                <!-- <li><a href="#">{{__('Guides')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('seller.guides') }}">{{__('Guides')}}</a></li>
                        <li><a href="{{ route('seller.guide.upload')}}">{{__('Add Guide')}}</a></li>
                      <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li> 
                    </ul>
                </li> -->
                <!-- <li><a href="#">{{__('Inhouse Orders')}}</a></li> -->
                <li><a href="{{ route('total.sales') }}">{{__('Total Sales')}}</a></li>
                <li><a href="#">{{__('Saller Sales via Affiliate Links')}}</a></li>
                <li><a href="{{ route('sellercoupon') }}">{{__('Saller Coupons')}}</a></li>
                <!-- <li><a href="#">{{__('Vendors')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li> -->
                <li><a href="#">{{__('Customer')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="{{ route('company_suggessions',Auth::user()->id) }}">Suggessions</a></li>
                        <li><a href="{{ route('seller.reviews',Auth::user()->id) }}">Reviews</a></li>
                         <li><a href="{{ route('clientsreport') }}">Clients Report</a></li>
                         @php
                            $domain = (explode(".",request()->getHost()));
                            $shop = App\Models\Shop::where('slug',$domain[0])->first();
                        @endphp
                        @if(in_array(15,$Permissions))
                        <li><a href="{{ route('seller_questions',Auth::user()->id) }}">Seller Questions</a></li>
                        @endif
                        @if(in_array(19,$Permissions))
                        <li><a href="{{ route('clientsreport') }}">Clients Report</a></li>
                        @endif
                    </ul>
                </li>
                <li><a href="#">{{__('Reports')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        @if(in_array(11,$Permissions))
                        <li><a href="{{ route('club_info') }}">Club Info</a></li>
                        @endif
                        @if(in_array(6,$Permissions))
                        <li><a href="{{ route('company_warranties', Auth::user()->id) }}">Warranty Request </a></li>
                        @endif
                        <li><a href="{{ route('sellingreport') }}">Selling Report</a></li>
                    </ul>
                </li>
                <!-- <li><a href="#">{{__('Pricing')}} </a></li>
                <li><a href="#">{{__('Bussines Setting')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li>
                <li><a href="#">{{__('E-commerce Setup')}} <i class="fa fa-angle-right"></i></a>
                    <ul class="dashboard-recover-two-sides-left-sub-links">
                        <li><a href="#">Sub links1</a></li>
                        <li><a href="#">Sub links2</a></li>
                        <li><a href="#">Sub links3</a></li>
                        <li><a href="#">Sub links4</a></li>
                    </ul>
                </li> -->
                @if(in_array(13,$Permissions))
                <li>
                    <a href="{{ route('support_ticket.index') }}" class="{{ areActiveRoutesHome(['support_ticket.index'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Support Ticket')}}
                        </span>
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{ route('marketing') }}" class="{{ areActiveRoutesHome(['marketing'])}}">
                        <i class="la la-support"></i>
                        <span class="category-name">
                            {{__('Email Marketing')}}
                        </span>
                    </a>
                </li>
                 <li class="">
                            <a class="nav-link" href="{{ route('inboxseller') }}">
                              
                                <span class="menu-title">Support Messages</span>
                            </a>
                </li>
                
                <li><a href="#">{{__('Log Activity')}}</a></li>
                
            </ul>


            </div>
        </div>
        <div class="dashboard-recover-res-sublinks-cross">
            <i class="fa fa-times" onclick="hidedashrecoversublinks()"></i>
        </div>
    </section>
    <!-- end res sub mnu -->
    <section class="dashboard-recover-burdger">
        <div class="dashboard-recover-burdger-page">
            <div class="dashboard-recover-burdger-bars">
                <i class="fa fa-bars" onclick="showdashrecoversublinks()"></i>
            </div>
        </div>
    </section>
<section class="gry-bg py-4 profile">
            <div class="row cols-xs-space cols-sm-space cols-md-space">]
                    @include('frontend.inc.seller_side_nav_new')

                <div class="col-lg-9">
                    <div class="main-content">
                        	<section class="support-ticket">    
            <div class="spt-flex-2">
                <div class="spt-pg-inner">
                    <div class="spt-inners-pg-1">
                        <h3>SUPPORT TICKET</h3>
                    </div>    
                    <div class="spt-inners-pg-2">
                        <table>
                            <thead>
                                <th class="spt-custom-th">#</th>
                                <th>Sending Date</th>
                                <th>Ticket Number</th>
                                <th>Subject</th>
                                <th>Options</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach ($tickets as $key => $ticket)
                                       <tr class="spt-tr-cst">
                                           <td>{{ $key+1 }}</td>
                                           <td>{{ $ticket->created_at }}</td>
                                           <td>{{ $ticket->id }}</td>
                                           <td>{{ $ticket->subject }}</td>
                                           <td>
                                               <button><a href="{{route('support_ticket.show', encrypt($ticket->id))}}">VIEW</a></button>
                                           </td>
                                           <td>
                                               <button class="btn btn-{{ ($ticket->is_closed == '0') ? 'success' : 'primary'; }}" {{ ($ticket->is_closed == '0') ? '' : 'disabled'; }}><a href="{{route('support_ticket.changestatus', $ticket->id)}}">Close Ticket</a></button>
                                           </td>
                                       </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>    
                    <div class="spt-inners-pg-3">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><a href="#">CREATE A NEW TICKET</a></button>
                    </div>
                </div>
            </div>   
    </section>
<!-- Modal -->

<section>   
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header border-none">
          <h5 class="modal-title mdt-cst" id="exampleModalLabel">Send Ticket</h5>
          <button type="button" class="close mdt-cst-btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form class="" action="{{ route('support_ticket.store') }}" method="post">
                    @csrf
            <div class="spt-modal-inner">
                <input type="text" class="form-control mb-3" name="subject" placeholder="Subject" required>
            </div>
            <div class="sample-toolbar">
                <a href="javascript:void(0)" onclick="format('bold')"><span class="fa fa-bold fa-fw"></span></a>
                <a href="javascript:void(0)" onclick="format('italic')"><span class="fa fa-italic fa-fw"></span></a>
                <a href="javascript:void(0)" onclick="format('insertunorderedlist')"><span class="fa fa-list fa-fw"></span></a>
                <a href="javascript:void(0)" onclick="setUrl()"><span class="fa fa-link fa-fw"></span></a>
                <!-- <span><input id="txtFormatUrl" placeholder="url" class="form-control"></span> -->
            </div>
            <textarea class="editor" name="details"></textarea>
        </div>
        <div class="modal-footer border-none">
          <button type="button" class="btn cst-btn-modal-1" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn cst-btn-modal-2">Confirm</button>
        </div>
        </form>
      </div>
    </div>
  </div>
</section> 
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        function show_ticket_modal(){
            $('#ticket_modal').modal('show');
        }
    </script>
@endsection