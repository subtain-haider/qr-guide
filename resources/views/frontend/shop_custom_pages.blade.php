@extends('frontend.layouts.new_app')

@section('content')
    <section class="pro-page-guides-sec">
		<div class="page">
			<div class="pro-page-guides-head">
				<h2>{{str_replace('-', ' ', $name)}}</h2>
			</div>
		</div>
	</section>

@endsection
