@extends(auth()->user()->is_admin ? 'chatlayouts/app2' : 'chatlayouts/app')
@section('content')
<div class="container">
                


                @livewire('message', ['users' => $users, 'messages' => $messages ?? null])
</div>
@endsection
