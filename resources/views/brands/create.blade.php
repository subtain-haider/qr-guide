@extends('layouts.app')

@section('content')

<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Brand Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('brands.store') }}" method="POST" enctype="multipart/form-data">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">{{__('Name')}}</label>
                    <div class="col-sm-10">
                        <input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="logo">{{__('Logo')}} <small>(120x80)</small></label>
                    <div class="col-sm-10">
                        <input type="file" id="logo" name="logo" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="brand_info">{{__('Brand Info')}}</label>
                    <div class="col-sm-10">
                        <textarea placeholder="{{__('Brand Info')}}" id="brand_info" name="brand_info" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">{{__('Category')}}</label>
                    <div class="col-sm-10">
                        <select name="category[]" id="category" class="form-control demo-select2" onchange="getCategories($(this).val(),$(this).attr('id'))" multiple data-placeholder="Choose Category">
                            @foreach($Category as $CategoryVal)
                                <option value="{{$CategoryVal->id}}">{{$CategoryVal->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">{{__('Sub Category')}}</label>
                    <div class="col-sm-10">
                        <select name="subcategory[]" id="subcategory" class="form-control demo-select2" onchange="getCategories($(this).val(),$(this).attr('id'))" multiple data-placeholder="Choose Sub Category">
                            @foreach($SubCategory as $brand)
                                <option value="{{$CategoryVal->id}}">{{$CategoryVal->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">{{__('Sub Sub Category')}}</label>
                    <div class="col-sm-10">
                        <select name="subsubcategory[]" id="subsubcategory" class="form-control demo-select2" multiple data-placeholder="Choose Sub Sub Category">
                            @foreach($SubSubCategory as $brand)
                                <option value="{{$CategoryVal->id}}">{{$CategoryVal->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>				
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>
@endsection
@section('script')    <script type="text/javascript">        function getCategories(val,name){            $.post('{{ route('brands.ajax') }}', {_token:'{{ csrf_token() }}', category:val,category_type:name}, function(data){				var returnedData = JSON.parse(data);				if(name == 'category'){					$('select#subsubcategory, select#subcategory').html();					$('select#subcategory').html(returnedData.subcategories);					$('.demo-select2').select2();				}if(name == 'subcategory'){					$('select#subsubcategory').html();					$('select#subsubcategory').html(returnedData.subsubcategories);					$('.demo-select2').select2();				}                /*location.reload();*/            });        }    </script>@endsection