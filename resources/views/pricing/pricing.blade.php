@extends('frontend.layouts.new_app_1')

@section('content')
    <!-- slider -->
	<section class="new2-product-page-slider-sec">
		<div class="new2-product-page-slider-sec-page">
			<div class="new2-product-page-slider-data">

				<div class="new2-product-page-slider-detail-1">
					<h2>{{__('WELCOME TO')}}<br><span> {{__('MY QR QUIDE')}}</span></h2>
					<p>{{__('WE WANT YOU TO JOIN THE DIGITAL ON LINE ERA')}}</p>
				</div>

				<div class="new2-product-page-slider-detail-2">
					<img src="{{asset('frontend/images/bg3.png')}}" class="new2-product-page-slider-right-1">
					<img src="{{asset('frontend/images/bg4.png')}}" class="new2-product-page-slider-right-2">
				</div>
			</div>
		</div>
		<div class="new2-product-page-slider-sec-img">
			<img src="{{asset('frontend/images/bg1.png')}}">
		</div>
	</section>
	<!--about us-->
	<section class="new-product-page-about-sec">
		<div class="new-product-page-about-sec-page">
			<div class="new-product-page-about-data">

				<div class="new-product-page-about-detail">
					<div class="new-product-page-about-detail-head">
						<p>{{__('POJECT INFO')}}</p>
						<h3>{{__('ABOUT US')}}</h3>
					</div>
					<div class="new-product-page-about-detail-para">
						<p>{{__('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed efficitur justo id diam accumsan placerat. Sed libero orci, cursus in viverra dignissim, mollis nec diam. Vestibulum consectetur faucibus tortor ac fermentum. Sed libero orci,<a href="#">MyQRGuide.com</a> cursus in viverra dignissim, mollis nec diam. Vestibulum consectetur faucibus tortor ac fermentum.')}}</p>
					</div>
					<div class="new-product-page-about-detail-para-btn">
						<a href="#">{{__('Read More')}}</a>
					</div>
				</div>
				<div class="new-product-page-about-detail">
					<div class="new-product-page-about-detail-img">
						<img src="{{asset('frontend/images/hand.png')}}">
					</div>
				</div>


			</div>
		</div>
	</section>
	<!-- VIDEO SECTION -->
	<section class="new-product-page-video-sec">
		<div class="new-product-page-video-sec-page">
			<div class="new-product-page-video-data">

				<div class="new-product-page-video-detail-1">
					<div class="new-product-page-video-detail-1-text">
						<p>{{__('MY QR GUIDE')}}</p>
						<h4>{{__('INTRO VIDEO')}}</h4>
						<hr>
					</div>
				</div>

				<div class="new-product-page-video-detail-2">
					<iframe width="100%" height="330" src="https://www.youtube.com/embed/KBo2El1bcwQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>

			</div>
		</div>
	</section>
	<!-- offer -->
	<section class="new-product-page-offer-sec">
		<div class="new-product-page-offer-sec-page">
			<div class="new-product-page-offer-sec-head">
				<p>{{__('We are unique')}}</p>
				<h3>{{__('WHAT WE OFFER')}}</h3>
			</div>
			<div class="new-product-page-offer-data">

				<div class="new-product-page-offer-detail">
					<div class="new-product-page-offer-detail-icon">
						<i class="fa"><img src="{{asset('frontend/images/work-with-a-dedicated-ecommerce-company.png')}}"></i>
					</div>
					<div class="new-product-page-offer-detail-head">
						<h4>{{__('Work with a Dedicated Ecommerce Company')}}</h4>
					</div>
					<div class="new-product-page-offer-detail-para">
						<p>{{__('myQRGuide makes your business green, eco-friendly and cost-effective. With our unique product branding services, you can cut down significantly on production cost by uploading online manuals to your mini site.')}}</p>
					</div>
				</div>

				<div class="new-product-page-offer-detail">
					<div class="new-product-page-offer-detail-icon">
						<i class="fa"><img src="{{asset('frontend/images/full-data-report.png')}}"></i>
					</div>
					<div class="new-product-page-offer-detail-head">
						<h4>{{__('Full Data Report')}}</h4>
					</div>
					<div class="new-product-page-offer-detail-para">
						<p>{{__('Get full reports about your client’s behavior and product remarks. Track all incoming traffic ')}}</p>
					</div>
				</div>

				<div class="new-product-page-offer-detail">
					<div class="new-product-page-offer-detail-icon">
						<i class="fa"><img src="{{asset('frontend/images/seo-marketing.png')}}"></i>
					</div>
					<div class="new-product-page-offer-detail-head">
						<h4>{{__('SEO Marketing')}}</h4>
					</div>
					<div class="new-product-page-offer-detail-para">
						<p>{{__('Get full Seo Info stracture to Raise Your Product and Brand In search Engines')}}</p>
					</div>
				</div>

				<div class="new-product-page-offer-detail">
					<div class="new-product-page-offer-detail-icon">
						<i class="fa"><img src="{{asset('frontend/images/direct-contact-to-your-clients.png')}}"></i>
					</div>
					<div class="new-product-page-offer-detail-head">
						<h4>{{__('Direct Contact to your Clients')}}</h4>
					</div>
					<div class="new-product-page-offer-detail-para">
						<p>{{__('Get Live Q/A from your Clients or a direct Chat and Emails')}}</p>
					</div>
				</div>

				<div class="new-product-page-offer-detail">
					<div class="new-product-page-offer-detail-icon">
						<i class="fa"><img src="{{asset('frontend/images/manual-upload.png')}}"></i>
					</div>
					<div class="new-product-page-offer-detail-head">
						<h4>{{__('Manual Upload')}}</h4>
					</div>
					<div class="new-product-page-offer-detail-para">
						<p>{{_('Upload Unlimited manuals with our unique tool. Keep clients well educated on how to unpack and use your product')}}</p>
					</div>
				</div>

				<div class="new-product-page-offer-detail">
					<div class="new-product-page-offer-detail-icon">
						<i class="fa"><img src="{{asset('frontend/images/product-video.png')}}"></i>
					</div>
					<div class="new-product-page-offer-detail-head">
						<h4>{{__('Product Video')}}</h4>
					</div>
					<div class="new-product-page-offer-detail-para">
						<p>{{__('Show customers a close-up reveal of your product with our video feature. With video uploads, you can share a lot more information about product specifications, use and benefits. We Can Even Help You Create A video Guide for your Product')}}</p>
					</div>
				</div>

				<div class="new-product-page-offer-detail">
					<div class="new-product-page-offer-detail-icon">
						<i class="fa"><img src="{{asset('frontend/images/direct-contact-to-your-clients.png')}}"></i>
					</div>
					<div class="new-product-page-offer-detail-head">
						<h4>{{__('API Connectivity')}}</h4>
					</div>
					<div class="new-product-page-offer-detail-para">
						<p>{{__('API-led connectivity pools data from all your brand’s social platforms. This unique feature enables you to link your manuals directly to your store and other site platforms.')}}</p>
					</div>
				</div>

				<div class="new-product-page-offer-detail">
					<div class="new-product-page-offer-detail-icon">
						<i class="fa"><img src="{{asset('frontend/images/manual-upload.png')}}"></i>
					</div>
					<div class="new-product-page-offer-detail-head">
						<h4>{{__('Warranty and Club Database')}}</h4>
					</div>
					<div class="new-product-page-offer-detail-para">
						<p>{{__('Keep close track of all your customers’ warranties, with our digital database feature. With this feature, you can stay connected with customers on a personal level as they begin to use your products Or Even Email Them')}}</p>
					</div>
				</div>

				<div class="new-product-page-offer-detail">
					<div class="new-product-page-offer-detail-icon">
						<i class="fa"><img src="{{asset('frontend/images/product-video.png')}}"></i>
					</div>
					<div class="new-product-page-offer-detail-head">
						<h4>{{__('Save Money')}}</h4>
					</div>
					<div class="new-product-page-offer-detail-para">
						<p>{{__('Lorem ipsum dolor sit, consectetur adipiscing elit. Ut elit ullamcorper mattis, pulvinar dapibus leo.')}}</p>
					</div>
				</div>

			</div>
		</div>
	</section>

    @php
        $Permissions = array(
                        1 => 'Manual',
                        2 => 'Create Unique QR Code For Mini Site',
                        3 => 'Upload A Manual',
                        4 => 'Product Info',
                        5 => 'Product Video',
                        6 => 'Digital Data Records Of Warranty',
                        7 => 'Link Your Store To The Manual',
                        8 => 'Full Mini Site And Brand Visibility',
                        9 => 'View & Buy Any Product On QR Guide System',
                        10 => 'Create And Register Warranty For Your Product',
                        11 => 'Register Club Members',
                        12 => 'Automatic Emails',
                        13 => 'Tickets Support',
                        14 => 'Selling Of Products',
                        15 => 'Interactive Q/A',
                        16 => 'Follow Up Warranty Dates Of Differnent Clients',
                        17 => 'Minisite Can Be Integrated With Your Own Domain',
                        18 => 'Competitor Data Research',
                        19 => 'Download Client Info',
                        20 => 'Can Design Unique Template For Mini Site',
                        21 => 'Follow Up Warranty Dates Of Differnent Clients',
                        22 => 'Minisite Can Be Integrated With Your Own Domain',
                        23 => 'Full SEO Capability',
                        24 => 'Full Social Media Conectivity',
                        25 => 'Direct Chat With Your Clients',
                        26 => 'Option To Send Customized Thank You Cards Digital',
                        27 => 'Multi Language Support',
                        28 => 'Social Media Support'
                    );
        
        $PackagesInfo = \App\Models\PricingPlan::all();
    @endphp

	<!-- pricing table -->
    <section class="new-product-page-pricing-sec">
		<div class="new-product-page-pricing-sec-page">
			<div class="new-product-page-pricing-sec-head">
				<h2>{{__('OUR PACKAGES')}}</h2>
			</div>
			<!-- desktop view -->
			<div class="new-product-page-pricing-data-1">
				<table class="new-product-page-pricing-table-1">
					
				    <tr>
                        <th></th>
                        @foreach($PackagesInfo as $key => $val)
                            <th class="new-product-page-pricing-thstl">
                                @if(empty($val->per_month) and empty($val->per_year))
                                    {{__('Free')}}<span class="tooltiptext1">{{_($val->name)}}</span>
                                @else
                                    @if(!empty($val->per_month))
                                        @php
                                            $per_month = explode('.',$val->per_month);
                                        @endphp
                                        <span class="new-product-page-pricing-dolar-1"> $ </span>{{$per_month[0]}}<span class="new-product-page-pricing-point-1"><sup>.{{$per_month[1]}}</sup></span>
                                    @endif
                                    @if(!empty($val->per_year))
                                        @php
                                            $per_year = explode('.',$val->per_year);
                                        @endphp
                                        <span class="new-product-page-pricing-dolar-1"> $ </span>{{$per_year[0]}}<span class="new-product-page-pricing-point-1"><sup>.{{$per_year[1]}}</sup></span>
                                    @endif
                                    <span class="tooltiptext{{$key+1}}">{{_($val->name)}}</span>
                                @endif
                            </th>
                        @endforeach
				    </tr>
                    <tr>
                        <td></td>
                        <td class="new-product-page-pricing-borderleft new-product-page-pricing-red">User Vendor & client</td>
                        <td class="new-product-page-pricing-borderleft new-product-page-pricing-red">1 QR Guide + Full Minisite Support</td>
                        <td class="new-product-page-pricing-borderleft new-product-page-pricing-red">Full Minisite Support Upto 10 QR Guide's</td>
                        <td class="new-product-page-pricing-borderleft new-product-page-pricing-red">Full Minisite Support Upto 100 QR Guide's</td>
                    </tr>
                    @foreach($Permissions as $key => $val)
                        <tr class="service-inner-2">
                            <td class="new-product-page-pricing-hed">{{$val}}</td>
                            @foreach($PackagesInfo as $pkgkey => $pkgval)
                                @php
                                    $PkgPermissions = json_decode($pkgval->permissions);
                                @endphp
                                <td class="new-product-page-pricing-borderleft @if($key == 2 and in_array($key, $PkgPermissions) and empty($pkgval->per_month) and empty($pkgval->per_year)) new-product-page-pricing-gry @endif">
                                    @if($key == 2 and in_array($key, $PkgPermissions) and empty($pkgval->per_month) and empty($pkgval->per_year))
                                        {{__('Create Unique QR Code For the Product')}}
                                    @elseif(in_array($key, $PkgPermissions))
                                        <i class="fa fa-check"></i>
                                    @else
                                        <span><i class="fa fa-times"></i></span>
                                    @endif
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
				</table>
				<div class="new-product-page-pricing-load-moredata">
					<button id="newpromore">{{__('Click Here For More info')}} <i class="fa fa-angle-double-down"></i></button>
				</div>
				<table class="new-product-page-pricing-table-button">
					<tr>
						<td></td>
                        @foreach($PackagesInfo as $key => $val)
                            <td class="new-product-page-pricing-table-btn-wdth">
                                @if(empty($val->per_month) and empty($val->per_year))
                                    <a href="{{route('user.login')}}" class="new-product-page-pricing-table-btn-1">{{__('LOG IN NOW')}}</a>
                                @else
                                    <a href="{{route('buy.plan', $val->id)}}" class="new-product-page-pricing-table-btn-{{$key+1}}">{{__('BUY NOW')}}</a>
                                @endif
                            </td>
                        @endforeach
					</tr>
				</table>
			</div>

			<!-- desktop-view end -->

			<!-- responsive view -->
			<div class="newpropriwrapper">
		    	<div class="newpropriwrapper-page">
				    <div class="newpropritabs">
                        @foreach($PackagesInfo as $key => $val)
                            <span class="newpropritab text-center"><button class="newpropritabs-btn">{{_($val->name)}}</button></span>
                        @endforeach
				    </div>
				    <div class="newtab_content">
                        @foreach($PackagesInfo as $key => $val)
                            <div class="newprotab_item">
                                <!-- table-2 -->
                                <table class="new-product-page-pricing-table-{{$key+2}}">
                                    <tr>
                                        <th class="new-product-page-pricing-table-tbl-text">Pricing Table</th>
                                        <th class="@if($key==0){{'new-product-page-pricing-thstl'}}@else{{'new-product-page-pricing-thst'}}@endif{{$key+2}}">
                                            @if(empty($val->per_month) and empty($val->per_year))
                                                <span class="new-product-page-pricing-dolar-{{$key+2}}"> $ </span>{{__('Free')}}<span class="totooltiptext{{$key+2}}">{{_($val->name)}}</span>
                                            @else
                                                @if(!empty($val->per_month))
                                                    @php
                                                        $per_month = explode('.',$val->per_month);
                                                    @endphp
                                                    <span class="new-product-page-pricing-dolar-{{$key+2}}"> $ </span>{{$per_month[0]}}<span class="new-product-page-pricing-point-{{$key+2}}"><sup>.{{$per_month[1]}}</sup></span>
                                                @endif
                                                @if(!empty($val->per_year))
                                                    @php
                                                        $per_year = explode('.',$val->per_year);
                                                    @endphp
                                                    <span class="new-product-page-pricing-dolar-{{$key+2}}"> $ </span>{{$per_month[0]}}<span class="new-product-page-pricing-point-{{$key+2}}"><sup>.{{$per_month[1]}}</sup></span>
                                                @endif
                                                <span class="totooltiptext{{$key+2}}">{{_($val->name)}}</span>
                                            @endif
                                        </th>
                                    </tr>
                                    <tr @if($key==0)class="new-product-page-pricing-red-wdt"@endif>
                                        <td class="new-product-page-pricing-borderleft{{$key+2}} new-product-page-pricing-red{{$key+2}}">
                                            @if($key == 0)
                                                {{__('Free User Vendor & client')}}
                                            @elseif($key == 1)
                                                {{__('1 QR Guide + Full Minisite Support')}}
                                            @elseif($key == 2)
                                                {{__('Full Minisite Support Upto 10 QR Guide\'s')}}
                                            @elseif($key == 3)
                                                {{__('Full Minisite Support Upto 100 QR Guide\'s')}}
                                            @endif
                                        </td>
                                    </tr>
                                    @php
                                        $PkgPermissions = json_decode($val->permissions);
                                    @endphp
                                    @foreach($Permissions as $Perkey => $PerVal)
                                        <tr>
                                            <td class="new-product-page-pricing-hed{{$key+2}}">{{__($PerVal)}}</td>
                                            <td class="new-product-page-pricing-borderleft{{$key+2}}">
                                                @if($Perkey == 2 and in_array($Perkey, $PkgPermissions) and empty($val->per_month) and empty($val->per_year))
                                                    {{__('Create Unique QR Code For the Product')}}
                                                @elseif(in_array($Perkey, $PkgPermissions))
                                                    <i class="fa fa-check"></i>
                                                @else
                                                    <i class="fa fa-times"></i>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>

                                <div class="new-product-page-pricing-load-moredatares">
                                    <button id="@if($key == 0){{'newpromoreresone'}}@elseif($key == 1){{'newpromorerestwo'}}@elseif($key == 2){{'newpromoreresthree'}}@elseif($key == 3){{'newpromoreresfour'}}@endif">{{__('Click Here For More info')}} <i class="fa fa-angle-double-down"></i></button>
                                </div>

                                <table class="@if($key == 0){{'new-product-page-pricing-table-button-res-one'}}@elseif($key == 1){{'new-product-page-pricing-table-button-res-two'}}@elseif($key == 2){{'new-product-page-pricing-table-button-res-three'}}@elseif($key == 3){{'new-product-page-pricing-table-button-res-four'}}@endif">
                                    <tr>
                                        <td class="new-product-page-pricing-table-btn-wdthres">
                                            @if($key == 0)
                                                <a href="{{route('user.login')}}" class="new-product-page-pricing-table-btn-{{$key+1}}-res">{{__('LOG IN NOW')}}</a>
                                            @else
                                                <a href="{{route('buy.plan', $val->id)}}" class="new-product-page-pricing-table-btn-{{$key+1}}-res">{{__('LOG IN NOW')}}</a>
                                            @endif
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        @endforeach
				    </div>
				</div>
			</div>
			

			</div>
			<!-- responsive-view-end -->
		</div>
	</section>
@endsection

@section('script')
    <script type="text/javascript">
        $(function () {
		$(".new-product-page-pricing-table-1 tr").slice(0, 9).show();
		$("#newpromore").on('click', function (e) {
			e.preventDefault();
			$(".new-product-page-pricing-table-1 tr").slice(0, 9).slideDown()
			if ($(".new-product-page-pricing-table-1 tr:hidden").length == 0) {
				$("#load").fadeOut('slow');
			}else {
			$(".new-product-page-pricing-table-1 tr").show();
			$("#newpromore").hide();
			}
				
		});

		$(".new-product-page-pricing-table-2 tr").slice(0, 9).show();
		$("#newpromoreresone").on('click', function (e) {
			e.preventDefault();
			$(".new-product-page-pricing-table-2 tr").slice(0, 9).slideDown()
			if ($(".new-product-page-pricing-table-2 tr:hidden").length == 0) {
				$("#load").fadeOut('slow');
			}else {
			$(".new-product-page-pricing-table-2 tr").show();
			$("#newpromoreresone").hide();
			}
				
		});

		$(".new-product-page-pricing-table-3 tr").slice(0, 9).show();
		$("#newpromorerestwo").on('click', function (e) {
			e.preventDefault();
			$(".new-product-page-pricing-table-3 tr").slice(0, 9).slideDown()
			if ($(".new-product-page-pricing-table-3 tr:hidden").length == 0) {
				$("#load").fadeOut('slow');
			}else {
			$(".new-product-page-pricing-table-3 tr").show();
			$("#newpromorerestwo").hide();
			}
				
		});

		$(".new-product-page-pricing-table-4 tr").slice(0, 9).show();
		$("#newpromoreresthree").on('click', function (e) {
			e.preventDefault();
			$(".new-product-page-pricing-table-4 tr").slice(0, 9).slideDown()
			if ($(".new-product-page-pricing-table-4 tr:hidden").length == 0) {
				$("#load").fadeOut('slow');
			}else {
			$(".new-product-page-pricing-table-4 tr").show();
			$("#newpromoreresthree").hide();
			}
				
		});

		$(".new-product-page-pricing-table-5 tr").slice(0, 9).show();
		$("#newpromoreresfour").on('click', function (e) {
			e.preventDefault();
			$(".new-product-page-pricing-table-5 tr").slice(0, 9).slideDown()
			if ($(".new-product-page-pricing-table-5 tr:hidden").length == 0) {
				$("#load").fadeOut('slow');
			}else {
			$(".new-product-page-pricing-table-5 tr").show();
			$("#newpromoreresfour").hide();
			}
				
		});

		// tabs

		// tabs

		$(".newpropriwrapper .newpropritab").click(function() {
		$(".newpropriwrapper .newtabpropri").removeClass("active").eq($(this).index()).addClass("newpropractive");
		$(".newprotab_item").hide().eq($(this).index()).fadeIn()
		}).eq(0).addClass("active");
	});

</script>
@endsection