@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('pricing.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Plan')}}</a>
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Pricing')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Monthly')}}</th>
                    <!--<th>{{__('Yearly')}}</th>-->
                    {{-- <th>{{__('Status')}}</th> --}}
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($pricing_plans as $key => $pricing_plan)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{ $pricing_plan->name }}</td>
                        <td>{{ number_format($pricing_plan->per_month,2) }}</td>
                        <!--<td>{{ number_format($pricing_plan->per_year,2) }}</td>-->
                        {{-- <td>
                            <label class="switch">
                                <input onchange="update_featured(this)" value="{{ $pricing_plan->id }}" type="checkbox" @if($pricing_plan->status == 1) echo "checked"; @endif >
                                <span class="slider round"></span>
                            </label>
                        </td> --}}
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{route('pricing.edit', encrypt($pricing_plan->id))}}">{{__('Edit')}}</a></li>
                                    <li><a onclick="confirm_modal('{{route('pricing.destroy', $pricing_plan->id)}}');">{{__('Delete')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

@endsection
