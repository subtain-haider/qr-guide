@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('payment_method.create_payment_type')}}" class="btn btn-rounded btn-info pull-right">{{__('Add Payment Type')}}</a>
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Payment Types')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10%">#</th>
                    <th>{{__('Name')}}</th>
                </tr>
            </thead>
            <tbody>
				@php
					foreach(DB::table('payment_types')->get() as $key => $payment_types){
				@endphp
						<tr>
							<td>{{$key+1}}</td>
							<td>{{$payment_types->name}}</td>
						</tr>
				@php
					}
				@endphp
            </tbody>
        </table>

    </div>
</div>

@endsection
