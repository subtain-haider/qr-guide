@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('business_settings.countrycreate')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Country')}}</a>
    </div>
</div>

<br>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('All Country')}}</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('Country name')}}</th>
						<th>{{__('Code')}}</th>
						<th>{{__('Status')}}</th>
						<th>{{__('Options')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @for($i = 0; $i < count($countries)-1; $i++)
                        <tr>
                            <td>{{$i+1}}</td>
                            <td>{{$countries[$i]->asciiname}}</td>
							<td>{{$countries[$i]->code}}</td>
							<td><label class="switch"><input id="status_{{ $countries[$i]->id }}" type="checkbox" <?php if($countries[$i]->status == 1) echo "checked";?> ><span class="slider round"></span></label></td>
							<td><button class="btn btn-purple" type="submit" onclick="updateCountry({{ $countries[$i]->id }})">{{__('Save')}}</button></td>
                        </tr>
                    @endfor
					<tr>
                        <td>{{count($countries)}}</td>
                        <td><input id="name_{{ $countries[count($countries)-1]->id }}" class="form-control" type="text" value="{{$countries[count($countries)-1]->asciiname}}"></td>
                        <td><input id="code_{{ $countries[count($countries)-1]->id }}" class="form-control" type="text" value="{{$countries[count($countries)-1]->code}}"></td>
                        <td><label class="switch"><input id="status_{{ $countries[count($countries)-1]->id }}" class="demo-sw" type="checkbox" <?php if($countries[count($countries)-1]->status == 1) echo "checked";?> ><span class="slider round"></span></label></td>
                        <td><button class="btn btn-purple" type="submit" onclick="updateYourCountry({{ $countries[count($countries)-1]->id }})" >{{__('Save')}}</button></td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>

@endsection

@section('script')
    <script type="text/javascript">

        //Updates default countries
        function updateCountry(i){
            if($('#status_'+i).is(':checked')){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('country.update') }}', {_token:'{{ csrf_token() }}', id:i, status:status}, function(data){
                location.reload();
            });
        }

        //Updates your country
        function updateYourCountry(i){
            var name = $('#name_'+i).val();
            var code = $('#code_'+i).val();
            if($('#status_'+i).is(':checked')){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('your_country.update') }}', {_token:'{{ csrf_token() }}', id:i, name:name, code:code, status:status}, function(data){
                location.reload();
            });
        }
    </script>
@endsection
