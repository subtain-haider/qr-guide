@extends('layouts.app')

@section('content')

<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Shipping Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" id="shipping_method" onsubmit="return shipping_form_submit()" action="{{ route('shipping_method.update') }}" method="POST" enctype="multipart/form-data">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>{{__('Free Shipping')}}</label>
                        </div>
                        @php
							$free_shipping = \App\Models\BusinessSetting::where('type', 'free_shipping')->first()->value;
							$free_shipping = json_decode($free_shipping);
							$free_shipping_status = '';
							if($free_shipping->status == 'on'){
								$free_shipping_status = ' checked';
							}
						@endphp
						<div class="col-md-6">
                            <input type="number" min="0" step="0.01" value="{{$free_shipping->cost}}" class="form-control mb-3" name="free_shipping_cost" placeholder="{{__('Free Shipping Cost')}}" readonly>
                        </div>
                        <div class="col-md-2">
                            <label class="switch" style="margin-top:5px;">
                                <input type="checkbox" name="free_shipping_status"{{$free_shipping_status}}>
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <div class="row">
                        @php
							$standard_shipping = \App\Models\BusinessSetting::where('type', 'standard_shipping')->first()->value;
							$standard_shipping = json_decode($standard_shipping);
							$standard_shipping_status = '';
							if($standard_shipping->status == 'on'){
								$standard_shipping_status = ' checked';
							}
						@endphp
						<div class="col-md-4">
                            <label>{{__('Standard Shipping')}}</label>
                        </div>
                        <div class="col-md-6">
                            <input type="number" min="0" step="0.01" value="{{$standard_shipping->cost}}" class="form-control mb-3" name="standard_shipping_cost" placeholder="{{__('Standard Shipping Cost')}}">
                        </div>
                        <div class="col-md-2">
                            <label class="switch" style="margin-top:5px;">
                                <input type="checkbox" name="standard_shipping_status"{{$standard_shipping_status}}>
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <div class="row">
                        @php
							$express_shipping = \App\Models\BusinessSetting::where('type', 'express_shipping')->first()->value;
							$express_shipping = json_decode($express_shipping);
							$express_shipping_status = '';
							if($express_shipping->status == 'on'){
								$express_shipping_status = ' checked';
							}
						@endphp
						<div class="col-md-4">
                            <label>{{__('Express Shipping')}}</label>
                        </div>
                        <div class="col-md-6">
                            <input type="number" min="0" step="0.01" value="{{$express_shipping->cost}}" class="form-control mb-3" name="express_shipping_cost" placeholder="{{__('Express Pickup Shipping Cost')}}">
                        </div>
                        <div class="col-md-2">
                            <label class="switch" style="margin-top:5px;">
                                <input type="checkbox" name="express_shipping_status"{{$express_shipping_status}}>
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
	function shipping_form_submit() {
        var min = 0;
        $('#shipping_method input[type="checkbox"]').each(function(){
			if($(this).is(':checked')){
                min = 1;
            }
		});
        if(min == 0){
            alert('Please selectminimum 1 shipping method!'); 
            return false;
        }else{
            return true;
        }
    }


</script>

@endsection
