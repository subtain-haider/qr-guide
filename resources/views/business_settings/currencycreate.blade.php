@extends('layouts.app')

@section('content')

<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Currency Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('currency.currencystore') }}" method="POST">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="name">{{__('Currency Name')}}</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="{{__('Currency Name')}}" id="name" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="name">{{__('Currency Symbol')}}</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="{{__('Currency Symbol')}}" id="symbol" name="symbol" class="form-control" required>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-4 control-label" for="name">{{__('Currency Code')}}</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="{{__('Currency Code')}}" id="code" name="code" class="form-control" required>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-4 control-label" for="name">{{__('Exchange Rate(1 USD = ?)')}}</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="{{__('Exchange Rate(1 USD = ?)')}}" id="exchange_rate" name="exchange_rate" class="form-control" required>
                    </div>
                </div>
				<div class="form-group">
					<label class="col-sm-4 control-label">{{__('Status')}}</label>
					<div class="col-sm-8">
						<label class="switch"><input id="status" type="checkbox" name="status"><span class="slider round"></span></label>
					</div>
				</div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
