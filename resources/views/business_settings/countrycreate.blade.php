@extends('layouts.app')

@section('content')

<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Country Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('country.countrystore') }}" method="POST">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="name">{{__('Country Name')}}</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="{{__('Country Name')}}" id="name" name="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="name">{{__('Country Code')}}</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="{{__('Country Code')}}" id="code" name="code" class="form-control" required>
                    </div>
                </div>
			</div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
