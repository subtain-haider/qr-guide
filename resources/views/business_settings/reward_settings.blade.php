@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <h2 class="panel-title">{{__('Point Configuration')}}</h2>
            </div>
            <div class="panel-body">
                <h3 class="panel-title">{{__('Point Overview')}}</h3>
				<form class="form-horizontal" action="{{ route('rewards.update_points_configuration') }}" method="POST">
                    @csrf
                    
					@php
						$points_configuration = \App\Models\BusinessSetting::where('type', 'points_configuration')->first();
						if(isset($points_configuration->value) and !empty($points_configuration->value)){
							$points_configuration = json_decode($points_configuration->value);
							
							$customer_points_points = $points_configuration->customer_points->points;
							$seller_points_points = $points_configuration->seller_points->points;
							$customer_points_status = $points_configuration->customer_points->status;
							$seller_points_status = $points_configuration->seller_points->status;
						}else{
							$seller_points_status = $customer_points_status = $seller_points_points = $customer_points_points = '';
						}
					@endphp
					
					<div class="form-group">
                        <div class="col-lg-2">
                            <label class="control-label">{{__('Points Per Customer')}}</label>
                        </div>
                        <div class="col-sm-5">
							<input type="number" placeholder="{{__('Points Per Customer')}}" id="points_per_customer" name="points_per_customer" class="form-control" min="0" step="0.01" value="{{$customer_points_points}}" required>
						</div>
						<div class="col-sm-5">
							<label class="switch"><input id="points_per_customer_status" name="points_per_customer_status" type="checkbox" <?php if($customer_points_status == 'on') echo 'checked'; ?>><span class="slider round"></span></label>
						</div>
                    </div>
					
					<div class="form-group">
                        <div class="col-lg-2">
                            <label class="control-label">{{__('Points Per Seller')}}</label>
                        </div>
                        <div class="col-sm-5">
							<input type="number" placeholder="{{__('Points Per Seller')}}" id="points_per_seller" name="points_per_seller" class="form-control" min="0" step="0.01" value="{{$seller_points_points}}" required>
						</div>
						<div class="col-sm-5">
							<label class="switch"><input id="points_per_seller_status" name="points_per_seller_status" type="checkbox" <?php if($seller_points_status == 'on') echo 'checked'; ?>><span class="slider round"></span></label>
						</div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12 text-right">
                            <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
	
	<div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <h2 class="panel-title">{{__('Profit Configuration')}}</h2>
            </div>
            <div class="panel-body">
                @foreach ($active_currencies as $key => $currency)
                    @php
						if(\App\Models\BusinessSetting::where('type', 'system_default_currency')->first()->value == $currency->id){
							$profit_configuration = \App\Models\BusinessSetting::where('type', 'profit_configuration')->first();
							if(isset($profit_configuration->value) and !empty($profit_configuration->value)){
								$profit_configuration = json_decode($profit_configuration->value);
								
								$profit = $profit_configuration->profit;
								$profit_status = $profit_configuration->status;
							}else{
								$profit_status = $profit = '';
							}
					@endphp
						<form class="form-horizontal" action="{{ route('rewards.update_profit_configuration') }}" method="POST">
							@csrf
							<div class="form-group">
								<div class="col-sm-5">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">1 {{$currency->name}} = </span>
										</div>
										<input type="number" name="profit_configuration" class="form-control" placeholder="Points" min="0" step="0.01" value="{{$profit}}">
									</div>
								</div>
								<div class="col-sm-5">
									<label class="switch"><input id="profit_configuration_status" name="profit_configuration_status" type="checkbox" <?php if($profit_status == 'on') echo 'checked'; ?>><span class="slider round"></span></label>
								</div>
							</div>
								
							<div class="form-group">
								<div class="col-lg-12 text-right">
									<button class="btn btn-purple" type="submit">{{__('Save')}}</button>
								</div>
							</div>
						</form>
					@php
						}
					@endphp
				@endforeach
            </div>
        </div>
    </div>
</div>

@endsection
