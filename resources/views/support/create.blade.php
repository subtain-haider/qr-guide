@extends('layouts.app')

@section('content')

<div class="col-lg-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Live Support User')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('support.store') }}" method="POST" enctype="multipart/form-data">
        	@csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="staff">{{__('Staff')}}</label>
                    <div class="col-sm-10">
                        <select name="staff" required class="form-control demo-select2-placeholder">
                            <option value="">{{__('Please Select')}}</option>
							@foreach($staffs as $staff)
                                @if(\App\Models\User::where('id', $staff->user_id)->first() != '')
									<option value="{{$staff->id}}">{{\App\Models\User::where('id', $staff->user_id)->first()->name}}</option>
								@endif
							@endforeach
                        </select>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-2 control-label" for="role">{{__('Role')}}</label>
                    <div class="col-sm-10">
                        <select name="role" required class="form-control demo-select2-placeholder">
                            <option value="">{{__('Please Select')}}</option>
							<option value="1">{{__('Customer Support')}}</option>
							<option value="2">{{__('Sales Support')}}</option>
							<option value="3">{{__('Technical Support')}}</option>
							<option value="4">{{__('Products Support')}}</option>
                        </select>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-2 control-label" for="phone">{{__('Phone Number')}}</label>
                    <div class="col-sm-10">
                        <input type="text" placeholder="{{__('Phone Number')}}" id="phone" name="phone" class="form-control" required>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-2 control-label" for="message">{{__('Message')}}</label>
                    <div class="col-sm-10">
                        <input type="text" placeholder="{{__('Message')}}" id="message" name="message" class="form-control" required>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-2 control-label" for="message">{{__('Profile Image')}}</label>
                    <div class="col-sm-10">
                        <input type="file" id="image" name="image" class="form-control" required>
                    </div>
                </div>
			</div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
