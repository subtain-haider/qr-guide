@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('support.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Support User')}}</a>
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Live Support')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
					<th>{{ __('Image') }}</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Phone')}}</th>
                    <th>{{__('Role')}}</th>
                    <th>{{ __('Message') }}</th>
                    <th>{{__('Actions')}}</th>
				</tr>
            </thead>
            <tbody>
                @foreach($support as $key => $support_user)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td><img class="img-md" src="{{ asset($support_user->profile_image) }}" alt="" /></td>
						<td>
							@if(\App\Models\Staff::where('id', $support_user->staff_id)->first())
								@if(\App\Models\User::where('id', \App\Models\Staff::where('id', $support_user->staff_id)->first()->user_id)->first())
								{{ \App\Models\User::where('id', \App\Models\Staff::where('id', $support_user->staff_id)->first()->user_id)->first()->name }}
								@endif
							@endif
						</td>
						<td>{{ $support_user->phone_number }}</td>
						<td>
							@if($support_user->role == 1)
								{{__('Customer Support')}}
							@elseif($support_user->role == 2)
								{{__('Sales Support')}}
							@elseif($support_user->role == 3)
								{{__('Technical Support')}}
							@elseif($support_user->role == 3)
								{{__('Products Support')}}
							@endif
						</td>
						<td>{{ $support_user->message }}</td>
						<td>
							<div class="btn-group dropdown">
								<button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
									{{__('Actions')}} <i class="dropdown-caret"></i>
								</button>
								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="{{route('support.edit', encrypt($support_user->id))}}">{{__('Edit')}}</a></li>
									<li><a onclick="confirm_modal('{{route('support.destroy', $support_user->id)}}');">{{__('Delete')}}</a></li>
								</ul>
							</div>
						</td>
					</tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@endsection