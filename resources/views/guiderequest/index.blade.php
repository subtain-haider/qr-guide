@extends('layouts.app')

@section('content')



<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Request for videos from product creation')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Vendor ID')}}</th>
                    <th>{{__('Vendor Name')}}</th>
                    <th>{{__('Vendor Email')}}</th>
                    <th>{{__('Message')}}</th>
                    {{-- <th>{{__('Status')}}</th> --}}
                    <th width="10%">{{__('Options')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($requestuss as $key => $requestus)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{ $requestus->userid }}</td>
                        <td>{{ $requestus->username }}</td>
                        <td>{{ $requestus->useremail }}</td>
                        <td>{{ $requestus->notify }}</td>
                       
                        {{-- <td>
                            <label class="switch">
                                <input onchange="update_featured(this)" value="{{ $pricing_plan->id }}" type="checkbox" @if($pricing_plan->status == 1) echo "checked"; @endif >
                                <span class="slider round"></span>
                            </label>
                        </td> --}}
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                   
                                    <li><a onclick="confirm_modal('{{route('guiderequest.destroy', $requestus->id)}}');">{{__('Delete')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

@endsection
