@extends('layouts.app')



@section('content')



<div class="col-sm-12">

    <div class="panel">

        <div class="panel-heading">

            <h3 class="panel-title">Add Advertisement</h3>

        </div>

        <!--Horizontal Form-->

        <!--===================================================-->

        <form class="form-horizontal" action="{{ route('advertising.update', $advertising->id) }}" method="POST">

        	@csrf
			
			<div class="panel-body">

                <div class="form-group">
					<label class="col-sm-3" for="name">{{__('Name')}}</label>
					<div class="col-sm-9">
						<input type="text" placeholder="{{__('Name')}}" id="name" name="name" value="{{ $advertising->provider_name }}" class="form-control" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3" for="slug">{{__('Advertisement Position')}}</label>
					<div class="col-sm-9">
						<select class="form-control demo-select2" name="slug" required>
							<option value="">{{__('Please Select')}}</option>
							<option value="Slider Sidebar"@php if($advertising->slug == 'Slider Sidebar') echo ' selected'; @endphp>{{__('Slider Sidebar')}}</option>
							<option value="Before Featured Products"@php if($advertising->slug == 'Before Featured Products') echo ' selected'; @endphp>{{__('Before Featured Products')}}</option>
							<option value="After Featured Products"@php if($advertising->slug == 'After Featured Products') echo ' selected'; @endphp>{{__('After Featured Products')}}</option>
							<option value="Bottom Banner"@php if($advertising->slug == 'Bottom Banner') echo ' selected'; @endphp>{{__('Bottom Banner')}}</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3" for="name">{{__('Script')}}</label>
					<div class="col-sm-9">

                        <textarea name="content" class="editor" required>{{ $advertising->tracking_code_large }}</textarea>

                    </div>

                </div>

            </div>

            <div class="panel-footer text-right">

                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>

            </div>

        </form>



        <!--===================================================-->

        <!--End Horizontal Form-->



    </div>

</div>



@endsection

