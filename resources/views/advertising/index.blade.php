@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <a href="{{ route('advertising.create')}}" class="btn btn-rounded btn-info pull-right">{{__('Add New Advertising')}}</a>
    </div>
</div>

<br>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('All Advertisings')}}</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('Advertising name')}}</th>
						<th>{{__('Slug')}}</th>
						<th>{{__('Status')}}</th>
						<th>{{__('Options')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @for($i = 0; $i < count($advertising); $i++)
                        <tr>
                            <td>{{$i+1}}</td>
                            <td>{{$advertising[$i]->slug}}</td>
							<td>{{$advertising[$i]->provider_name}}</td>
							<td><label class="switch"><input id="status_{{ $advertising[$i]->id }}" type="checkbox" <?php if($advertising[$i]->status == 1) echo "checked";?> ><span class="slider round"></span></label></td>
							<td>
								<button class="btn btn-purple" type="submit" onclick="updateadvertising({{ $advertising[$i]->id }})">{{__('Save')}}</button>
								<div class="btn-group dropdown">
									<button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
										{{__('Actions')}} <i class="dropdown-caret"></i>
									</button>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="{{route('advertising.edit', encrypt($advertising[$i]->id))}}">{{__('Edit')}}</a></li>
										<li><a onclick="confirm_modal('{{route('advertising.destroy', $advertising[$i]->id)}}');">{{__('Delete')}}</a></li>
									</ul>
								</div>
							</td>
                        </tr>
                    @endfor
                </tbody>
            </table>

        </div>
    </div>
</div>

@endsection

@section('script')
    <script type="text/javascript">

        //Updates default advertising
        function updateadvertising(i){
            if($('#status_'+i).is(':checked')){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('advertising.update_status') }}', {_token:'{{ csrf_token() }}', id:i, status:status}, function(data){
                location.reload();
            });
        }
	</script>
@endsection