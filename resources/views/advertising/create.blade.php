@extends('layouts.app')



@section('content')



<div class="col-sm-12">

    <div class="panel">

        <div class="panel-heading">

            <h3 class="panel-title">Add Advertisement</h3>

        </div>

        <!--Horizontal Form-->

        <!--===================================================-->

        <form class="form-horizontal" action="{{ route('advertising.store') }}" method="POST">

        	@csrf
			
			<div class="panel-body">

                <div class="form-group">
					<label class="col-sm-3" for="name">{{__('Name')}}</label>
					<div class="col-sm-9">
						<input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3" for="slug">{{__('Advertisement Position')}}</label>
					<div class="col-sm-9">
						<select class="form-control demo-select2" name="slug" required>
							<option value="">{{__('Please Select')}}</option>
							<option value="Slider Sidebar">{{__('Slider Sidebar')}}</option>
							<option value="Before Featured Products">{{__('Before Featured Products')}}</option>
							<option value="After Featured Products">{{__('After Featured Products')}}</option>
							<option value="Bottom Banner">{{__('Bottom Banner')}}</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3" for="name">{{__('Script')}}</label>
					<div class="col-sm-9">

                        <textarea name="content" class="editor" required></textarea>

                    </div>

                </div>

            </div>

            <div class="panel-footer text-right">

                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>

            </div>

        </form>



        <!--===================================================-->

        <!--End Horizontal Form-->



    </div>

</div>



@endsection

