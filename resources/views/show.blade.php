@extends(auth()->user()->is_admin ? 'chatlayouts/app2' : 'chatlayouts/app')
@section('content')
  
        <div class="container">
            @livewire('show', ['users' => $users, 'messages' => $messages, 'sender' => $sender])
        </div>
 
@endsection
