<!DOCTYPE html>
<html>
<head>
	<title>FAQ page</title>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, intial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/qr.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js'></script><script  src="./script.js"></script>
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css'>
</head>
<body>
<table style="width: 100%;">
	<tr>
		<td style="width: 90%;"><a href="#"><img src="images/logoof.png" style="padding-left: 20px;width: 140px;"></a></td>
		<td style="font-weight: 600;"><a style="color: #000;" href="#">Home</a></td>
		<td style="font-weight: 600;"><a style="color: #000;" href="#">About</a></td>
		<td style="font-weight: 600;"><a style="color: #000;padding-right: 20px;" href="#">Contact</a></td>

	</tr>
</table>
<table style="width: 100%; margin-top: 50px;">
	<tr style="display: flex; justify-content: center;">
		<th style="width: 90%; text-align: center; font-size: 38px; font-family: 'play'; font-weight: 500; ">Thanks for your order</th>
	</tr>
	<tr style="display: flex; justify-content: center;">
		<td style="width:90%;text-align: center;font-size: 20px; font-family: 'play';">You'r receive an email when your items are shipped. If you have any<br> questions. Call Us 12345667</td>
	</tr>
</table>
<table style="width: 100%; margin-top: 50px; background-color: #1a9fb62b;padding: 30px 0px;">
	<tr>
		<th style="font-size: 18px; font-family: 'play'; font-weight: 600; text-align: center; padding: 10px 0px;">SUMMARY:</th>
		<th style="padding: 10px 0px;"></th>
		<th style="font-size: 18px; font-family: 'play'; font-weight: 600; text-align: center;padding: 10px 0px;">SHIPPING ADDRESS:</th>
	</tr>
	<tr>
		<td style="font-size: 18px;font-weight: 600; padding: 10px 0px; font-family: 'play';text-align: center;">Order No:</td>
		<td style="font-size: 18px; padding: 10px 0px;font-family: 'play';">945678</td>
		<td style="text-align:  center;padding: 10px 0px;font-size: 18px;">Example Street example road example house </td>
	</tr>
	<tr>
		<td style="font-size: 18px;font-weight: 600;padding: 10px 0px; font-family: 'play';text-align: center;">Order Date:</td>
		<td style="font-size: 18px;padding: 10px 0px; font-family: 'play';">Oct21, 2021</td>
		<td style="text-align: center;padding: 10px 0px;"></td>
	</tr>
	<tr>
		<td style="font-size: 18px;font-weight: 600;padding: 10px 0px; font-family: 'play';text-align: center;">Order Total:</td>
		<td style="font-size: 18px;padding: 10px 0px; font-family: 'play';">$80.67</td>
		<td style="text-align: center;padding: 10px 0px;"></td>
	</tr>
</table>
<table style="border-collapse:separate;border-spacing:0 15px;width: 100%;overflow: auto; padding: 30px 50px ;">
					<tr style=" background-color: #fff;box-shadow: 1px 4px 12px -5px rgba(0,0,0,0.45);border-radius: 6px;">
						<th style="background-color: #fff;padding: 20px 20px;text-align: left;border-right: 2px solid #ccc;">Name</th>
						<th style="background-color: #fff;padding: 20px 10px;text-align: center;border-right: 2px solid #ccc;">QTY</th>
						<th style="background-color: #fff;padding: 20px 10px;text-align: center;border-right: 2px solid #ccc;">Price</th>
						
					</tr>
					<tr style=" background-color: #fff;box-shadow: 1px 4px 12px -5px rgba(0,0,0,0.45);border-radius: 6px;">
						<td style="background-color: #fff;padding: 20px 20px;text-align: left;border-right: 2px solid #ccc;">product Name</td>
						<td style="background-color: #fff;padding: 20px 10px;text-align: center;border-right: 2px solid #ccc; color: #08A3BE;font-weight: 600;">2</td>
						<td style="background-color: #fff;padding: 20px 10px;text-align: center;border-right: 2px solid #ccc;">300$</td>
						
					</tr>
					
				</table>
<table style="width: 100%; margin-top: 30px;">
	<tr>
		<td style="text-align: right; font-size:18px;padding:  5px 10px;">Subtotal(3items):</td>
		<td style=" padding: 5px 10px; text-align: center;">$40.57</td>
	</tr>
	<tr>
		<td style="text-align: right; padding: 5px 10px;font-size:18px;">Flat-rate Shipping:</td>
		<td style="text-align: center; padding: 5px 10px; color: #1A9FB6;font-weight: 600">Free</td>
	</tr>
	<tr>
		<td style="text-align: right;font-size:18px; padding: 5px 10px;">Discount:</td>
		<td style="text-align: center; padding: 5px 10px;">$0.00</td>
	</tr>
	<tr>
		<td style="text-align: right;font-size:18px;padding: 5px 10px; font-weight: 600;">Order Total:</td>
		<td style="text-align: center; padding: 5px 10px;color: #1A9FB6;font-weight: 600">$40.57</td>
	</tr>
</table>
<table style="width: 100%;margin-top: 30px;">
	<tr>
		<td style="text-align: center;"><a href="#"><img src="images/logoof.png" style="width: 140px;text-align: center;"></a></td>
	</tr>
	<tr>
		<td style="text-align: center;">BUILD A MINI-SITE WITH COMPANY PROFILE</td>
	</tr>
</table>
</body>
</html>