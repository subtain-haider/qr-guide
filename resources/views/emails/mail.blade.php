@php

 $order = \App\Models\Order::findOrFail(Session::get('order_id'));
                $shop = \App\Models\Shop::where('user_id',$order->user_id)->first();
                $orderUpdate = \App\Models\Order::findOrFail($order->id);
                $orderUpdate->shop_id = $shop->id;
                $product_array = array();
                foreach($cart_detail = Session::get('cart') as $product)
                {
                    // echo $product['id'];
                    $product_array[] = ($product['id']);
                }
                $orderUpdate->product_id = $product_array;
                $orderUpdate->save();
                $user = \App\Models\User::where('id',$order->user_id)->first();
                $template = $user->template;
              
@endphp
@if($template == "temp-1")
<style>
    .mail-cnfrm-page{
	display: flex;
	justify-content: center;
	padding: 30px 0px;
}
.mail-cnfrm-page-wdth{
	width: 85%;
}
.mail-cnfrm-page-detail{

	display: flex;
	justify-content: center;

}
.mail-cnfrm-page-detail img{
	width: 200px;
	height: auto;
}
.mail-cnfrm-page-detail-2{
	display: flex;
	justify-content: center;
}
.mail-cnfrm-page-detail-2 img{
	width: 250px;
	height: auto;
}
.mail-cnfrm-page-tbl-area{
	margin-top: 40px;
	overflow: auto
}
.mail-cnfrm-page-table{
	border-collapse:separate;
    border-spacing:0 15px;
    width: 100%;
    overflow: auto;
} 
.mail-cnfrm-page-table  td {
 	background-color: #fff;
  padding: 20px 10px;
  text-align: center;
  border-right: 2px solid #ccc;


}
.mail-cnfrm-page-table  th{
	background-color: #fff;
 	 padding: 20px 10px;
 	 text-align: center;
 	 border-right: 2px solid #ccc;
}
.mail-cnfrm-page-table tr{
	 background-color: #fff;
  box-shadow: 1px 4px 12px -5px rgba(0,0,0,0.45);
  border-radius: 6px;
  
}
.mail-cnfrm-page-clr-text{
	color: #08A3BE;
	font-weight: 600;
}
.mail-cnfrm-page-total{
	display: flex;
	justify-content: flex-end;
	margin-top: 20px;
}
@media only screen and (max-width: 697px) {
	.mail-cnfrm-page-table{
		width: 700px;
	}
}
</style>
<section class="mail-cnfrm-page">
	<div class="mail-cnfrm-page-wdth">
		<div class="mail-cnfrm-page-data">
			<div class="mail-cnfrm-page-detail">

				 <img src="images/thnk5.png">
			</div>
			<div class="mail-cnfrm-page-detail-2">
				 <img src="http://testdomain.myqrguide.com/uploads/logo/logo.png">
			</div>

			<div class="mail-cnfrm-page-tbl-area">
				<table class="mail-cnfrm-page-table">
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Price</th>
						<th>Number</th>
					</tr>
					<tr>
						<td>John</td>
						<td class="mail-cnfrm-page-clr-text">info@gmail.com</td>
						<td>300$</td>
						<td class="mail-cnfrm-page-clr-text">6789123</td>
					</tr>
					<tr>
						<td>John</td>
						<td class="mail-cnfrm-page-clr-text">info@gmail.com</td>
						<td>300$</td>
						<td class="mail-cnfrm-page-clr-text">6789123</td>
					</tr>
					<tr>
						<td>John</td>
						<td class="mail-cnfrm-page-clr-text">info@gmail.com</td>
						<td>300$</td>
						<td class="mail-cnfrm-page-clr-text">6789123</td>
					</tr>
				</table>
			</div>
			<div class="mail-cnfrm-page-total">
				<p>Total</p>
			</div>
		</div>
	</div>
</section>
@else
Hello <strong>{{ $name }}</strong>,
<p>Second Template</p>
@endif
