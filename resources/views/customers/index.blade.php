@extends('layouts.app')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

<div class="row">
    <div class="col-sm-12">
        <!-- <a href="{{ route('sellers.create')}}" class="btn btn-info pull-right">{{__('add_new')}}</a> -->
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Customers')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" id="table_id" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Email Address')}}</th>
                    <th>{{__('Shop Name')}}</th>
                    <th width="10%">{{__('Actions')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($customers as $key => $customer)
                    @php
                        $Order = \App\Models\Order::where('user_id', $customer->id)->count();
                        $shopname = \App\Models\Shop::where('user_id', $customer->slug)->first();
                    @endphp
                    @if(($type == 'have-orders' and $Order > 0) || ($type == 'no-orders' and $Order <= 0) || $type == null)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$customer->name}}</td>
                            <td>{{$customer->email}}</td>
                            <td>{{ isset($shopname->title) ?  $shopname->title : '' }}</td>
                            <td>
                                <div class="btn-group dropdown">
                                    <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                        {{__('Actions')}} <i class="dropdown-caret"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a data-toggle="modal" data-target="#exampleModal" data-whatever="{{ $customer->id }}" >{{__('Assign Package')}}</a></li>
                                        <li><a onclick="confirm_modal('{{route('customers.destroy', $customer->id)}}');">{{__('Delete')}}</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>

    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Assign Plan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ route('customers.assignplan') }}">
          <div class="form-group">
            <!--<label for="recipient-name" class="col-form-label">Recipient:</label>-->
            <input type="hidden" class="form-control recipient-name" name="customerid"  id="recipient-name">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Select Package:</label>
            <select class="form-control" name="packegeid">
                @foreach($packages as $pkg)
                <option value="{{ $pkg->id }}" >{{ $pkg->name }}</option>
                @endforeach
            </select>
            {{ csrf_field() }}
            <!--<textarea class="form-control" id="message-text"></textarea>-->
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Assign</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script>
$(document).ready( function () {
    $('#table_id').DataTable();
} );
    $('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
//   modal.find('.modal-title').text('Assign to ' + recipient)
  modal.find('.modal-body input.recipient-name').val(recipient)
})
</script>
@endsection
