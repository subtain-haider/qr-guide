<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Seller;

use App\Models\Staff;

use App\Models\Role;

use App\Models\User;

use App\Models\Customer;

use App\Models\Shop;

use App\Models\Product;

use App\Models\Order;

use App\Models\OrderDetail;

use App\Models\Points;

use App\Models\RewardPayments;

use App\Models\PaymentTypes;

use Auth;

use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\SearchController;

class MarketingController extends Controller
{
    public function sellers()
    {
        $sellers = Seller::where('referral', '!=', '')->orderBy('created_at', 'desc')->get();
		
		return view('marketing.sellers', compact('sellers'));
    }
	
	public function customer_points()
    {
        $customers = Customer::where('referral', Auth::user()->referral)->where('referral', '!=', '')->get();
		
		return view('marketing.customer_points', compact('customers'));
    }
	
	public function reward_points()
    {
        $Points = Points::all();
		
		return view('marketing.reward_points', compact('Points'));
    }
	
	public function seller_points()
    {
        $sellers = Seller::where('referral', '!=', '')->where('referral', Auth::user()->referral)->get();
		
		return view('marketing.seller_points', compact('sellers'));
    }
	
	public function payout_request()
    {
        $reward_payments = RewardPayments::where('status', '0')->orderBy('created_at', 'desc')->get();
		
		return view('marketing.payout_request', compact('reward_payments'));
    }
	
	public function reject_request($id)
    {
        $RewardPayments = RewardPayments::findOrFail(decrypt($id));
		
		$RewardPayments->status = 2;

        if($RewardPayments->save()){

            flash(__('Request has been updated successfully'))->success();
			
			return redirect()->route('marketing.payout_request');

        }
		
		flash(__('Something went wrong'))->error();

        return back();
    }
	
	public function complete_request()
    {
        $user = auth()->user()->staff->role;
		if($user->name == 'Advertiser'){
		    $reward_payments = RewardPayments::where('status', '1')->where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        }else{
            $reward_payments = RewardPayments::where('status', '1')->orderBy('created_at', 'desc')->get();
        }
        return view('marketing.complete_request', compact('reward_payments'));
    }
	
	public function payment_form(Request $request)

    {

        $RewardPayments = RewardPayments::findOrFail($request->request_id);
		
		$PaymentType = PaymentTypes::all();

        return view('marketing.payment_form', compact('RewardPayments', 'PaymentType'));

    }
	
	public function update_advertiser_status(Request $request)

    {

        $user = User::findOrFail($request->id);

        $user->status = $request->status;
		
		if($user->save()){

            flash(__('Status has been updated successfully'))->success();
			
			return 1;

        }
		
		flash(__('Something went wrong'))->error();

        return 1;

    }
	
	public function payment_by_advertiser(Request $request)

    {

        $RewardPayments = RewardPayments::findOrFail($request->request_id);

        $RewardPayments->status = 1;
		
		$RewardPayments->pay_by = $request->payment_type;
		
		$RewardPayments->receipt_name = $request->transaction_name;
		
		$RewardPayments->transaction_no = $request->transaction_no;
		
		$RewardPayments->branch = $request->location;
		
		$RewardPayments->paid_amount = $request->amount;
		
		$RewardPayments->pay_date = date('Y-m-d H:i:s');
		
		$RewardPayments->paid_by = Auth::user()->id;

        if($RewardPayments->save()){

            flash(__('Payment has been updated successfully'))->success();
			
			return redirect()->route('marketing.complete_request');

        }
		
		flash(__('Something went wrong'))->error();

        return back();

    }
	
	public function advertiser_payment_voucher($id)
    {
		$RewardPayments = RewardPayments::findOrFail(decrypt($id));
		
        return view('marketing.advertiser_payment_voucher', compact('RewardPayments'));
    }
	
	public function advertiser_details_view()
    {
        return view('marketing.advertiser_details_view');
    }
	
	public function advertisers()
    {
		$staffs = Staff::where('role_id', '3')->get();
        
		return view('marketing.advertisers', compact('staffs'));
    }
}