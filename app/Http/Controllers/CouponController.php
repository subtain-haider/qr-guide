<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use App\Models\Coupon;
use Schema;
use Image;
use Auth;


class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::orderBy('id','desc')->get();
        return view('coupons.index', compact('coupons'));
    }
    
    public function sellercoupon()
    {
        $coupons = Coupon::orderBy('id','desc')->where('user_id',Auth::user()->id)->get();
        return view('frontend.sellers_coupons', compact('coupons'));
    }
    public function get_discountbycoupon(Request $request){
        $coupon = Coupon::where('code', $request->coupon_code)->first();
        if($coupon != null && strtotime(date('d-m-Y')) >= $coupon->start_date && strtotime(date('d-m-Y')) <= $coupon->end_date){
            $coupon_details = json_decode($coupon->details);
            if ($coupon->type == 'pkg_base')
            {
            if ($coupon->discount_type == 'percent') {
                    $coupon_discount =  ($request->couponamount * $coupon->discount)/100;
                    if ($coupon_discount > $coupon_details->max_discount) {
                       return $coupon_discount = '$'.$coupon_details->max_discount;
                    }
                }
                elseif ($coupon->discount_type == 'amount') {
                    if($request->couponamount > $coupon->discount) {
                    return $coupon_discount = '$'.($request->couponamount  - $coupon->discount);
                    } else{
                        return '$'.$request->couponamount;
                    }
                }
            }
        }

        else {
            flash('No Coupon Exixts')->warning();
        }
        return back();
        
       return $coupons = Coupon::where('code',$request->coupon_code)->first();
    }
    public function sellercouponcreate()
    {
        return view('frontend.create_coupons');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('coupons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $coupon = new Coupon;
        if ($request->coupon_type == "product_base") {
            $coupon->type = $request->coupon_type;
            $coupon->code = $request->coupon_code;
            $coupon->discount = $request->discount;
            $coupon->discount_type = $request->discount_type;
            $coupon->start_date = strtotime($request->start_date);
            $coupon->end_date = strtotime($request->end_date);
            $cupon_details = array();
            for($key = 0; $key < count($request->category_ids)-1; $key++) {
                $data['category_id'] = $request->category_ids[$key];
                $data['subcategory_id'] = $request->subcategory_ids[$key];
                $data['subsubcategory_id'] = $request->subsubcategory_ids[$key];
                $data['product_id'] = $request->product_ids[$key];
                array_push($cupon_details, $data);
            }
            $coupon->details = json_encode($cupon_details);
            if ($coupon->save()) {
                flash('Coupon has been saved successfully')->success();
                return redirect()->route('coupon.index');
            }
            else{
                flash('Something went wrong')->danger();
                return back();
            }
        }
        elseif ($request->coupon_type == "cart_base") {
            $coupon->type = $request->coupon_type;
            $coupon->code = $request->coupon_code;
            $coupon->discount = $request->discount;
            $coupon->discount_type = $request->discount_type;
            $coupon->start_date = strtotime($request->start_date);
            $coupon->end_date = strtotime($request->end_date);
            $data = array();
            $data['min_buy'] = $request->min_buy;
            $data['max_discount'] = $request->max_discount;
            $coupon->details = json_encode($data);
            if ($coupon->save()) {
                flash('Coupon has been saved successfully')->success();
                return redirect()->route('coupon.index');
            }
            else{
                flash('Something went wrong')->danger();
                return back();
            }
        }
        elseif ($request->coupon_type == "pkg_base") {
            $coupon->type = $request->coupon_type;
            $coupon->code = $request->coupon_code;
            $coupon->discount = $request->discount;
            $coupon->discount_type = $request->discount_type;
            $coupon->start_date = strtotime($request->start_date);
            $coupon->end_date = strtotime($request->end_date);
            $data = array();
            $data['max_discount'] = $request->max_discount;
            $coupon->details = json_encode($data);
            if ($coupon->save()) {
                flash('Coupon has been saved successfully')->success();
                return redirect()->route('coupon.index');
            }
            else{
                flash('Something went wrong')->danger();
                return back();
            }
        }

    }
    
    public function seller_coupon_store(Request $request)
    {
      $coupon = new Coupon;
        if ($request->coupon_type == "product_base") {
            $coupon->type = $request->coupon_type;
            $coupon->code = $request->coupon_code;
            $coupon->user_id = Auth::user()->id;
            $coupon->discount = $request->discount;
            $coupon->discount_type = $request->discount_type;
            $coupon->start_date = strtotime($request->start_date);
            $coupon->end_date = strtotime($request->end_date);
            $cupon_details = array();
            for($key = 0; $key < count($request->category_ids)-1; $key++) {
                $data['category_id'] = $request->category_ids[$key];
                $data['subcategory_id'] = $request->subcategory_ids[$key];
                $data['subsubcategory_id'] = $request->subsubcategory_ids[$key];
                $data['product_id'] = $request->product_ids[$key];
                array_push($cupon_details, $data);
            }
            $coupon->details = json_encode($cupon_details);
            if ($coupon->save()) {
                flash('Coupon has been saved successfully')->success();
                return redirect()->route('sellercoupon');
            }
            else{
                flash('Something went wrong')->danger();
                return back();
            }
        }
        elseif ($request->coupon_type == "cart_base") {
            $coupon->type = $request->coupon_type;
            $coupon->code = $request->coupon_code;
            $coupon->discount = $request->discount;
            $coupon->user_id = Auth::user()->id;
            $coupon->discount_type = $request->discount_type;
            $coupon->start_date = strtotime($request->start_date);
            $coupon->end_date = strtotime($request->end_date);
            $data = array();
            $data['min_buy'] = $request->min_buy;
            $data['max_discount'] = $request->max_discount;
            $coupon->details = json_encode($data);
            if ($coupon->save()) {
                flash('Coupon has been saved successfully')->success();
                return redirect()->route('sellercoupon');
            }
            else{
                flash('Something went wrong')->danger();
                return back();
            }
        }

    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $coupon = Coupon::findOrFail(decrypt($id));
      return view('coupons.edit', compact('coupon'));
    }
    
    public function sellercouponedit($id)
    {
      $coupon = Coupon::findOrFail(decrypt($id));
      return view('frontend.edit_coupons', compact('coupon'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $coupon = Coupon::findOrFail($id);
        if ($request->coupon_type == "product_base") {
            $coupon->type = $request->coupon_type;
            $coupon->code = $request->coupon_code;
            $coupon->discount = $request->discount;
            $coupon->discount_type = $request->discount_type;
            $coupon->start_date = strtotime($request->start_date);
            $coupon->end_date = strtotime($request->end_date);
            $cupon_details = array();
            for($key = 0; $key < count($request->category_ids)-1; $key++) {
                $data['category_id'] = $request->category_ids[$key];
                $data['subcategory_id'] = $request->subcategory_ids[$key];
                $data['subsubcategory_id'] = $request->subsubcategory_ids[$key];
                $data['product_id'] = $request->product_ids[$key];
                array_push($cupon_details, $data);
            }
            $coupon->details = json_encode($cupon_details);
            if ($coupon->save()) {
                flash('Coupon has been saved successfully')->success();
                return redirect()->route('coupon.index');
            }
            else{
                flash('Something went wrong')->danger();
                return back();
            }
        }
        elseif ($request->coupon_type == "cart_base") {
            $coupon->type = $request->coupon_type;
            $coupon->code = $request->coupon_code;
            $coupon->discount = $request->discount;
            $coupon->discount_type = $request->discount_type;
            $coupon->start_date = strtotime($request->start_date);
            $coupon->end_date = strtotime($request->end_date);
            $data = array();
            $data['min_buy'] = $request->min_buy;
            $data['max_discount'] = $request->max_discount;
            $coupon->details = json_encode($data);
            $img = Image::make(public_path('img/sale_ads.png'));
            $img->text($request->discount.' %', 1020, 1000, function($font) {
                $font->file(public_path('fonts/Arial.ttf'));
                $font->size(100);
                $font->color('#fff');
                $font->align('center');
                $font->valign('bottom');
                $font->angle(25);
            });
            $img->save(public_path('img/hardik3.png'));
            if ($coupon->save()) {
                flash('Coupon has been saved successfully')->success();
                return redirect()->route('coupon.index');
            }
            else{
                flash('Something went wrong')->danger();
                return back();
            }
        }
    }
    
    public function seller_coupon_edit(Request $request, $id)
    {
      $coupon = Coupon::findOrFail($id);
        if ($request->coupon_type == "product_base") {
            $coupon->type = $request->coupon_type;
            $coupon->code = $request->coupon_code;
            $coupon->discount = $request->discount;
            $coupon->discount_type = $request->discount_type;
            $coupon->start_date = strtotime($request->start_date);
            $coupon->end_date = strtotime($request->end_date);
            $cupon_details = array();
            for($key = 0; $key < count($request->category_ids)-1; $key++) {
                $data['category_id'] = $request->category_ids[$key];
                $data['subcategory_id'] = $request->subcategory_ids[$key];
                $data['subsubcategory_id'] = $request->subsubcategory_ids[$key];
                $data['product_id'] = $request->product_ids[$key];
                array_push($cupon_details, $data);
            }
            $coupon->details = json_encode($cupon_details);
            if ($coupon->save()) {
                flash('Coupon has been saved successfully')->success();
                return redirect()->route('sellercoupon');
            }
            else{
                flash('Something went wrong')->danger();
                return back();
            }
        }
        elseif ($request->coupon_type == "cart_base") {
            $coupon->type = $request->coupon_type;
            $coupon->code = $request->coupon_code;
            $coupon->discount = $request->discount;
            $coupon->discount_type = $request->discount_type;
            $coupon->start_date = strtotime($request->start_date);
            $coupon->end_date = strtotime($request->end_date);
            $data = array();
            $data['min_buy'] = $request->min_buy;
            $data['max_discount'] = $request->max_discount;
            $coupon->details = json_encode($data);
            // $img = Image::make(public_path('img/sale_ads.png'));
            // $img->text($request->discount.' %', 1020, 1000, function($font) {
            //     $font->file(public_path('fonts/Arial.ttf'));
            //     $font->size(100);
            //     $font->color('#fff');
            //     $font->align('center');
            //     $font->valign('bottom');
            //     $font->angle(25);
            // });
            // $img->save(public_path('img/hardik3.png'));
            if ($coupon->save()) {
                flash('Coupon has been saved successfully')->success();
                return redirect()->route('sellercoupon');
            }
            else{
                flash('Something went wrong')->danger();
                return back();
            }
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coupon = Coupon::findOrFail($id);
        if(Coupon::destroy($id)){
            flash('Coupon has been deleted successfully')->success();
            return redirect()->route('coupon.index');
        }

        flash('Something went wrong')->error();
        return back();
    }
    public function seller_destroy($id)
    {
        $coupon = Coupon::findOrFail($id);
        if(Coupon::destroy($id)){
            flash('Coupon has been deleted successfully')->success();
            return redirect()->route('sellercoupon');
        }

        flash('Something went wrong')->error();
        return back();
    }
    public function get_coupon_form(Request $request)
    {
        if($request->coupon_type == "product_base") {
            return view('partials.product_base_coupon');
        }
        elseif($request->coupon_type == "cart_base"){
            return view('partials.cart_base_coupon');
        }
        elseif($request->coupon_type == "pkg_base"){
            return view('partials.pkg_base_coupon');
        }
    }
    
    public function seller_get_coupon_form(Request $request)
    {
        if($request->coupon_type == "product_base") {
            return view('partials.seller_product_base_coupon');
        }
        elseif($request->coupon_type == "cart_base"){
            return view('partials.seller_cart_base_coupon');
        }
    }
    
    public function get_coupon_form_edit(Request $request)
    {
        if($request->coupon_type == "product_base") {
            $coupon = Coupon::findOrFail($request->id);
            return view('partials.product_base_coupon_edit',compact('coupon'));
        }
        elseif($request->coupon_type == "cart_base"){
            $coupon = Coupon::findOrFail($request->id);
            return view('partials.cart_base_coupon_edit',compact('coupon'));
        }
        elseif($request->coupon_type == "pkg_base"){
            $coupon = Coupon::findOrFail($request->id);
            return view('partials.pkg_base_coupon_edit',compact('coupon'));
        }
    }
     public function seller_get_coupon_form_edit(Request $request)
    {
        if($request->coupon_type == "product_base") {
            $coupon = Coupon::findOrFail($request->id);
            return view('partials.seller_product_base_coupon_edit',compact('coupon'));
        }
        elseif($request->coupon_type == "cart_base"){
            $coupon = Coupon::findOrFail($request->id);
            return view('partials.seller_cart_base_coupon_edit',compact('coupon'));
        }
    }

}
