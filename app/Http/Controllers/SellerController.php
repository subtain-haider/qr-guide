<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Seller;
use App\Models\Staff;
use App\Models\Role;
use App\Models\User;
use App\Models\Shop;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Order;
use App\Models\Points;
use App\Models\OrderDetail;
use App\Models\RewardPayments;
use App\Models\BusinessSetting;
use Auth;
use Illuminate\Support\Facades\Hash;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($plan=NULL)
    {
        $user = auth()->user()->staff->role;
		$sellers = User::where('user_type','seller')->orderBy('created_at', 'desc')->get();
		return view('sellers.index', compact('sellers', 'plan'));
    }
	
	public function advertiser_report()
    {
		$user = auth()->user()->staff->role;
		if($user->name == 'Advertiser'){
			$user = auth()->user();			
			$sellers = Seller::where('referral', $user->ref_code)->orderBy('created_at', 'desc')->get();
		}else{
			$sellers = Seller::orderBy('created_at', 'desc')->get();
		}
		//$sellers = $sellers->where('referral', $user->email);
        return view('staffs.sales_report', compact('sellers'));
    }
	
	public function customers_report()
    {
		
		$user = auth()->user()->staff->role;
		
		if($user->name == 'Advertiser'){
			
			$user = auth()->user();			
			
			$customers = Customer::where('referral', $user->ref_code)->where('referral', '!=', '')->orderBy('created_at', 'desc')->get();
		
		}else{
			
			$customers = Customer::orderBy('created_at', 'desc')->get();
		
		}
		
		//$sellers = $sellers->where('referral', $user->email);
        return view('sellers.customers_report', compact('customers'));
    }
	
	public function referrals()
    {
		$user = auth()->user()->staff->role;
		
		if($user->name == 'Advertiser'){
			
			$user = auth()->user();			
			
			$users = User::where('referral', $user->ref_code)->where('referral', '!=', '')->orderBy('created_at', 'desc')->get();
		
		}else{
			
			$users = User::orderBy('created_at', 'desc')->get();
		
		}
		
		return view('sellers.referrals', compact('users'));
    }
	
	public function invite_seller()	{
		$user = auth()->user();
		$sellers = Seller::where('referral', $user->ref_code)->orderBy('created_at', 'desc')->get();
		$points_configuration = \App\BusinessSetting::where('type', 'points_configuration')->first();
		if(isset($points_configuration->value) and !empty($points_configuration->value)){
			$points_configuration = json_decode($points_configuration->value);
			$seller_points_points = $points_configuration->seller_points->points;
			$seller_points_status = $points_configuration->seller_points->status;
		}else{
			$seller_points_status = $seller_points_points = '';
		}
		return view('sellers.invite_seller', compact('sellers', 'seller_points_points'));
	}
	
	public function invite_customer()	{
		$user = auth()->user();
		$sellers = Seller::where('referral', $user->ref_code)->orderBy('created_at', 'desc')->get();
		$points_configuration = \App\BusinessSetting::where('type', 'points_configuration')->first();
		if(isset($points_configuration->value) and !empty($points_configuration->value)){
			$points_configuration = json_decode($points_configuration->value);
			$customer_points_points = $points_configuration->customer_points->points;
			$customer_points_status = $points_configuration->customer_points->status;
		}else{
			$customer_points_status = $customer_points_points = '';
		}
		return view('sellers.invite_customer', compact('sellers', 'customer_points_points'));
	}
	
	public function payment_requests()	{
		$user = auth()->user();
		$sellers = Seller::where('referral', $user->ref_code)->orderBy('created_at', 'desc')->get();
		$points_configuration = \App\BusinessSetting::where('type', 'points_configuration')->first();
		if(isset($points_configuration->value) and !empty($points_configuration->value)){
			$points_configuration = json_decode($points_configuration->value);
			$customer_points = $points_configuration->customer_points->points;
			$customer_points_status = $points_configuration->customer_points->status;
			$seller_points = $points_configuration->seller_points->points;
			$seller_points_status = $points_configuration->seller_points->status;
		}else{
			$seller_points_status = $seller_points = $customer_points_status = $customer_points = '';
		}
		$reward_payments = RewardPayments::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
		return view('sellers.payment_request', compact('sellers', 'customer_points', 'seller_points', 'reward_payments'));		
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sellers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(User::where('email', $request->email)->first() != null){
            flash(__('Email already exists!'))->error();
            return back();
        }
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->user_type = "seller";
        $user->password = Hash::make($request->password);
		if(isset($request->ref)){
			$user->referral = $request->referral;
		}
        if($user->save()){
            $seller = new Seller;
            $seller->user_id = $user->id;
            if(isset($request->ref)){
				$seller->referral = $request->referral;
				$points_configuration = \App\BusinessSetting::where('type', 'points_configuration')->first();
				if(isset($points_configuration->value) and !empty($points_configuration->value)){
					$points_configuration = json_decode($points_configuration->value);
					if($points_configuration->seller_points->points == 'on'){
						$seller->points = $points_configuration->seller_points->points;
					}
				}
			}
			if($seller->save()){
                $shop = new Shop;
                $shop->user_id = $user->id;
                $shop->slug = 'demo-shop-'.$user->id;
                $shop->save();
                flash(__('Seller has been inserted successfully'))->success();
                return redirect()->route('sellers.index');
            }
        }

        flash(__('Something went wrong'))->error();
        return back();
    }
	
	public function customer_points_withdraw()
	{
		if(!empty(\App\Points::where('user_id', Auth::user()->id)->first())){
		
			$CheckPoints = \App\Points::where('user_id', Auth::user()->id)->first();
			
			if($CheckPoints->current_points_customer <= 0){
				
				flash(__("You don't have any coins!"))->error();
				
				return back();
				
			}
			
			$businesssetting = \App\BusinessSetting::where('type', 'profit_configuration')->first();
			
			$profit_configuration = 0;
			
			if(isset($businesssetting->value) and !empty($businesssetting->value)){
			
				$businesssetting = json_decode($businesssetting->value);
				
				if($businesssetting->status != NULL){
				
					$profit_configuration = $businesssetting->profit;
					
				}
				
			}
			
			$RewardPayments = new RewardPayments;
			
			$RewardPayments->user_id = $CheckPoints->user_id;
			
			$RewardPayments->points = $CheckPoints->total_points_customer-$CheckPoints->redeemed_points_customer;
			
			$RewardPayments->amount = ($CheckPoints->total_points_customer-$CheckPoints->redeemed_points_customer)/$profit_configuration;
			
			$RewardPayments->type = "Customers";
			
			if($RewardPayments->save()){
				
				// $CheckPoints->redeemed_points_customer = $CheckPoints->total_points_customer+$CheckPoints->redeemed_points_customer;

				$CheckPoints->redeemed_points_customer = $CheckPoints->total_points_customer;
				
				$CheckPoints->current_points_customer = 0;
				
				$CheckPoints->balance_customer = 0;
				
				$CheckPoints->save();
				
				flash(__("Redeem request sent successfully"))->success();
				
				return redirect()->route('advertiser.payment_requests');
			
			}
			
		}
		
		flash(__('Something went wrong'))->error();
		
		return back();
	
	}
	
	public function seller_points_withdraw()
	{
	
		if(!empty(Points::where('user_id', Auth::user()->id)->first())){
			
			$CheckPoints = Points::where('user_id', Auth::user()->id)->first();
			
			if($CheckPoints->current_points_seller <= 0){
			
				flash(__("You don't have any coins!"))->error();
				
				return back();
				
			}
			
			$businesssetting = BusinessSetting::where('type', 'profit_configuration')->first();
			
			$profit_configuration = 0;
			
			if(isset($businesssetting->value) and !empty($businesssetting->value)){
			
				$businesssetting = json_decode($businesssetting->value);
				
				if($businesssetting->status != NULL){
				
					$profit_configuration = $businesssetting->profit;
					
				}
			
			}
			
			$RewardPayments = new RewardPayments;
			
			$RewardPayments->user_id = $CheckPoints->user_id;
			
			$RewardPayments->points = $CheckPoints->total_points_seller-$CheckPoints->redeemed_points_seller;
			
			$RewardPayments->amount = ($CheckPoints->total_points_seller-$CheckPoints->redeemed_points_seller)/$profit_configuration;
			
			$RewardPayments->type = "Seller";
			
			if($RewardPayments->save()){
			
				// $CheckPoints->redeemed_points_seller = $CheckPoints->total_points_seller+$CheckPoints->redeemed_points_seller;

				$CheckPoints->redeemed_points_seller = $CheckPoints->total_points_seller;
				
				$CheckPoints->current_points_seller = 0;
				
				$CheckPoints->balance_seller = 0;
				
				$CheckPoints->save();
				
				flash(__("Redeem request sent successfully"))->success();
				
				return redirect()->route('advertiser.payment_requests');
				
			}
		
		}
		
		flash(__('Something went wrong'))->error();
		
		return back();
	
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
	
	public function seller_details($id)
    {
        $seller = Seller::findOrFail($id);
        return view('sellers.seller_details', compact('seller'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seller = Seller::findOrFail(decrypt($id));
        return view('sellers.edit', compact('seller'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $seller = Seller::findOrFail($id);
        $user = $seller->user;
        $user->name = $request->name;
        $user->email = $request->email;
        if(strlen($request->password) > 0){
            $user->password = Hash::make($request->password);
        }
        if($user->save()){
            if($seller->save()){
                flash(__('Seller has been updated successfully'))->success();
                return redirect()->route('sellers.index');
            }
        }

        flash(__('Something went wrong'))->error();
        return back();
    }
	
	/**
     * Update the seller order payment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function payment_by_order(Request $request)
    {
        $OrderDetails = \App\OrderDetail::where('order_id', $request->order_id)->where('seller_id', $request->seller_id)->get();
        foreach($OrderDetails as $OrderDetail){
			$SellerOrderDetail = OrderDetail::findOrFail($OrderDetail->id);
			$SellerOrderDetail->admin_to_seller_payment = $OrderDetail->price;
			$SellerOrderDetail->payment_status = 'paid';
			$SellerOrderDetail->payment_type = $OrderDetail->payment_type;
			$SellerOrderDetail->transaction_name = $OrderDetail->transaction_name;
			$SellerOrderDetail->transaction_no = $OrderDetail->transaction_no;
			$SellerOrderDetail->location = $OrderDetail->location;
			$SellerOrderDetail->pay_payment = $OrderDetail->amount;
			$SellerOrderDetail->payment_time = date('Y-m-d H:i:s');
			$SellerOrderDetail->save();
		}

        flash(__('Payment has been updated successfully'))->success();
		return redirect()->route('sales.seller_sales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seller = Seller::findOrFail($id);
        Shop::destroy($seller->user->id);
        Product::where('user_id', $seller->user->id)->delete();
        Order::where('user_id', $seller->user->id)->delete();
        OrderDetail::where('seller_id', $seller->user->id)->delete();
        User::destroy($seller->user->id);
        if(Seller::destroy($id)){
            flash(__('Seller has been deleted successfully'))->success();
            return redirect()->route('sellers.index');
        }

        flash(__('Something went wrong'))->error();
        return back();
    }

    public function show_verification_request($id)
    {
        $seller = Seller::findOrFail($id);
        return view('sellers.verification', compact('seller'));
    }

    public function approve_seller($id)
    {
        $seller = User::findOrFail($id);
        if($seller->status == 0){
			$seller->status = 1;
		}
        if($seller->save()){
            flash(__('Seller has been approved successfully'))->success();
			//return redirect()->route('sellers.index');
			return back();
		}
        flash(__('Something went wrong'))->error();
        return back();
        
		$seller_points = 0;
		$recent_points = $seller->points;
		
		if(isset($seller->referral) and $seller->verification_status == 0){
			
			$points_configuration = \App\BusinessSetting::where('type', 'points_configuration')->first();
			
			if(isset($points_configuration->value) and !empty($points_configuration->value)){
				
				$points_configuration = json_decode($points_configuration->value);
				
				if($points_configuration->seller_points->status == 'on'){
					$seller_points = $points_configuration->seller_points->points;
				}else{
					$seller_points = 0;
				}
				if($seller_points > $recent_points){
					$seller_points = $seller_points-$recent_points;
				}		
			}
					
			$referral_id = User::where('ref_code', $seller->referral)->first();
					
			if(!empty($referral_id)){
						
				$user_points = Points::where('user_id', $referral_id->id)->first();
						
				if(!empty($user_points)){
							
					$user_points->total_points_seller = $user_points->total_points_seller+$seller_points;
							
					$user_points->current_points_seller = $user_points->current_points_seller+$seller_points;
							
					$user_points->balance_seller = $user_points->balance_seller+$seller_points;
							
					$user_points->save();
							
				}else {
						
					$Points = new Points;
				
					$Points->user_id = $referral_id->id;
							
					$Points->total_points_seller = $seller_points;
								
					$Points->current_points_seller = $seller_points;
								
					$Points->balance_seller = $seller_points;
								
					$Points->save();
						
				}
					
			}
			
		}
		
		if($seller->verification_status == 0){
			$seller->points = $seller_points;
		}
		$seller->verification_status = 1;
        if($seller->save()){
            flash(__('Seller has been approved successfully'))->success();
			//return redirect()->route('sellers.index');
			return back();
		}
        flash(__('Something went wrong'))->error();
        return back();
    }

    public function reject_seller($id)
    {
        $seller = User::findOrFail($id);
        $seller->status = 0;
        // $seller->verification_info = null;
        if($seller->save()){
            flash(__('Seller verification request has been rejected successfully'))->success();
            //return redirect()->route('sellers.index');
			return back();
        }
        flash(__('Something went wrong'))->error();
        return back();
    }


    public function payment_modal(Request $request)
    {
        $seller = Seller::findOrFail($request->id);
        return view('sellers.payment_modal', compact('seller'));
    }
}
