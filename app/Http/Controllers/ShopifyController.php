<?php

namespace App\Http\Controllers;

use App\Models\Shopify;
use App\Models\Product;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
// use Auth;

class ShopifyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $products = array();

            $shopifies = $user->shopify()->get();
            foreach ($shopifies as $shopify){
                // Set variables for our request
                $shop = $shopify->shop;
                $token = $shopify->token;
                $query = array(
                    "Content-type" => "application/json" // Tell Shopify that we're expecting a response in JSON format
                );

                // Run API call to get all products
                $response = $this->shopify_call($token, $shop, "/admin/products.json", array(), 'GET');
              
                if(isset(json_decode($response['response'])->errors)){
                    // Get response
                    $shopify->update(['status' => 0]);

                }
            }
            return view('frontend.seller.listshopify',compact('products', 'shopifies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('admin.shopify.create');
        return view('frontend.seller.addshopify');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        return $this->install($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shopify  $shopify
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shopify  $shopify
     * @return \Illuminate\Http\Response
     */
    public function edit(Shopify $shopify)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shopify  $shopify
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shopify $shopify)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shopify  $shopify
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shopify $shopify)
    {
        if(Shopify::destroy($shopify->id)){

            flash(translate('Shopify store has been deleted successfully'))->success();

            Artisan::call('view:clear');
            Artisan::call('cache:clear');

            if(Auth::user()->user_type == 'admin'){
                return redirect()->route('shopify.index');
            }
            else{
                return redirect()->route('shopify.index');
            }
        }
        else{
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    public function install(Request $request)
    {
        // Set variables for our request
        $shop = $request->shop;
        $api_key = "61d8776e2fa53f183de20f0e1578c53a";
        $scopes = "read_orders,write_products";
        $redirect_uri = url('/')."/generate_token";

        // Build install/approval URL to redirect to
        $install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

        // Redirect
        header("Location: " . $install_url);
        die();

        return $request;
    }

    public function generate_token(Request $request)
    {

        // Set variables for our request
        $api_key = "61d8776e2fa53f183de20f0e1578c53a";
        $shared_secret = "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";
        $params = $request->all(); // Retrieve all request parameters
        $hmac = $request->hmac; // Retrieve HMAC request parameter

        $params = array_diff_key($params, array('hmac' => '')); // Remove hmac from params
        ksort($params); // Sort params lexographically

        $computed_hmac = hash_hmac('sha256', http_build_query($params), $shared_secret);

        // Use hmac data to check that the response is from Shopify or not
        if (hash_equals($hmac, $computed_hmac)) {

            // Set variables for our request
            $query = array(
                "client_id" => $api_key, // Your API key
                "client_secret" => $shared_secret, // Your app credentials (secret key)
                "code" => $params['code'] // Grab the access key from the URL
            );

            // Generate access token URL
            $access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";

            // Configure curl client and execute request
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $access_token_url);
            curl_setopt($ch, CURLOPT_POST, count($query));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
            $result = curl_exec($ch);
            curl_close($ch);

            // Store the access token
            $result = json_decode($result, true);
            $access_token = $result['access_token'];

            // Show the access token (don't do this in production!)
            $user = Auth::user();
            $shopify = $user->shopify()->create(['token' => $access_token, 'shop' => substr($params['shop'],0,-14)]);

            return redirect('/shopify');

        } else {
            // Someone is trying to be shady!
            die('This request is NOT from Shopify!');
        }
    }

       public function shopify_call($token, $shop, $api_endpoint, $query = array(), $method = 'GET', $request_headers = array()) {

        // Build URL
        $url = "https://" . $shop . ".myshopify.com" . $api_endpoint;
        if (!is_null($query) && in_array($method, array('GET', 	'DELETE'))) $url = $url . "?" . http_build_query($query);

        // Configure cURL
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 3);
        // curl_setopt($curl, CURLOPT_SSLVERSION, 3);
        curl_setopt($curl, CURLOPT_USERAGENT, 'My New Shopify App v.1');
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

        // Setup headers
        $request_headers[] = "";
        if (!is_null($token)) $request_headers[] = "X-Shopify-Access-Token: " . $token;
        curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);

        if ($method != 'GET' && in_array($method, array('POST', 'PUT'))) {
            if (is_array($query)) $query = http_build_query($query);
            curl_setopt ($curl, CURLOPT_POSTFIELDS, $query);
        }

        // Send request to Shopify and capture any errors
        $response = curl_exec($curl);
        $error_number = curl_errno($curl);
        $error_message = curl_error($curl);

        // Close cURL to be nice
        curl_close($curl);

        // Return an error is cURL has a problem
        if ($error_number) {
            return $error_message;
        } else {

            // No error, return Shopify's response by parsing out the body and the headers
            $response = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);

            // Convert headers into an array
            $headers = array();
            $header_data = explode("\n",$response[0]);
            $headers['status'] = $header_data[0]; // Does not contain a key, have to explicitly set
            array_shift($header_data); // Remove status, we've already set it above
            foreach($header_data as $part) {
                $h = explode(":", $part);
                $headers[trim($h[0])] = trim($h[1]);
            }

            // Return headers and Shopify's response
            return array('headers' => $headers, 'response' => $response[1]);

        }

    }
        public  function products($selected = null){

    }
    
    public  function reviewCall($token,$shop,$productId)
    {
        // Build URL
        $url = "https://" . $shop . ".myshopify.com" . "/admin/products/". $productId . "/metafields.json";
        // $url = "https://" . $shop . ".myshopify.com" . "/admin/products/7190238626044/metafields.json";
        $response = Http::withHeaders(['X-Shopify-Access-Token'=> $token])->get($url);
        return json_decode($response);
       
    }

    public function shopifyProductsSync($id){
        $product_delete = \App\Models\Product::where([['user_id','=',$id],['product_type','=','shopify_product']])->delete();
        $review_delete = \App\Models\Review::where('shopify_id','!=',null)->delete();
        $shopify = Shopify::where('user_id',$id)->first();
        // dd($shopify);
        $shop = $shopify->shop;
        $token = $shopify->token;
        $query = array(
            "Content-type" => "application/json" // Tell Shopify that we're expecting a response in JSON format
        );

        // Run API call to get all products
        $response = $this->shopify_call($token, $shop, "/admin/products.json", array(), 'GET');
        if(!isset(json_decode($response['response'])->errors)){
            // Get response
            $products = json_decode($response['response'])->products;
            // dd($products[24]);
            $product_array = array();
            foreach($products as $product)
            {
                $product_array[] = json_encode($product->id);
                $productId = $product->id;
                $data = new Product;
                $data->name = $product->title;
                $pname = str_replace('', '|', $product->title);
                $data->slug = str_replace(' ', '-', $pname);
                $data->description = $product->body_html;
                $data->product_type = "shopify_product";
                $data->category_id  = "34";
                $data->subcategory_id = "104";
                $data->subsubcategory_id = "747";
                $data->category_name = "undefined";
                $data->subcategory_name = "undefined";
                $data->subsubcategory_name = "undefined";
                $data->added_by = "seller";
                $data->choice_options = "["."]";
                $data->user_id = Auth::user()->id;
                $data->thumbnail_img = Auth::user()->id;
                // dd($shop.$product->id);
                $data->shopify_id = $shop.$product->id;
               foreach($product->variants as $variants)
               {
                   $data->unit_price = $variants->price;
                   $data->sku = $variants->sku;
               }
                if(!empty($product->image))
                {
                    $data->photos = "[".json_encode($product->image->src)."]";
                    $data->thumbnail_img = $product->image->src;
                }
                else
                {
                    $data->photos = "[".json_encode(url('/').'/'."public/img/no-image-available.png")."]";
                    $data->thumbnail_img = url('/').'/'."public/img/no-image-available.png";
                }
                $data->save();
            }
            foreach($product_array as $p_id)
            {
            $review = $this->reviewCall($token,$shop,$p_id);
                foreach($review as $res)
                {
                    if($res)
                    {
                    $review_table = new Review;
                    $review_table->comment = $res[0]->description;
                    $review_table->shopify_id = $shop.$p_id;
                    foreach(json_decode($res[0]->value) as $key => $val)
                    {
                        if($key == 'value')
                        {
                        $rating = $val;
                        }
                    }
                    $review_table->rating = $rating;
                    $review_table->save();
                    }
                }
           
            }
            foreach($product_array as $p_id)
            {
            $product_datas = \App\Models\Product::where('shopify_id',$shop.$p_id)->get();
            foreach($product_datas as $product_data)
            {
                $id = $shop.$p_id;
                $review_product_id = Review::where('shopify_id',$id)->first();
                if($review_product_id)
                {
                $review_product_id->product_id = $product_data->id;
                $review_product_id->save();
                }
            } 
            }
                
                
        }
        flash('All Products Synced from this shopify store successfully')->success();

        Artisan::call('view:clear');
        Artisan::call('cache:clear');

        if(Auth::user()->user_type == 'admin'){
            return redirect('seller/products');
        }
        else{
            return redirect('seller/products');
        }
    }
}