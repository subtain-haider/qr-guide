<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Models\User;
use App\Models\Shop;
use \App\Models\Message;
use Auth;
use Hash;
use Session;

class InboxsellerController extends Controller
{

    public function index() {
        // Show just the users and not the sellers as well
   
       
  
       
        
        // $bcs = User$bcs = User::select('slug')
        //                 ->where('id', '=', auth()->id())
        //                 ->get();
        //  = Shop::select('slug')
        //                 ->where('user_id'd '=', auth()->id())
        //                 ->get(););
                           
                         
     
//  >select('league_name')
//     ->join('countries', 'countries.country_id', '=', 'leagues.country_id')
//     ->where('countries.country_name', $country)
        // $users = User::where('is_seller', false)->orWhere($bcs, $bc)->orderBy('id', 'DESC')->get();
        
        
        $users = User::where('is_seller', false)->where('slug',auth()->id())->orderBy('id', 'DESC')->get();


        if (auth()->user()->is_seller == false) {
            $messages = Message::where('user_id', auth()->id())->orWhere('receiver', auth()->id())->orderBy('id', 'DESC')->get();
        }

        return view('home2', [
            'users' => $users,
            'messages' => $messages ?? null,
        ]);
    }

    public function show($id) {
        if (auth()->user()->is_seller == false) {
            abort(404);
        }

        $sender = User::findOrFail($id);

        /*$users = User::with(['message' => function($query) {
            return $query->orderBy('created_at', 'DESC');
        }])->where('is_seller', false)
            ->where('slug',auth()->id())
            ->orderBy('id', 'DESC')
            ->get();*/
            $users = User::where('is_seller', false)->where('slug',auth()->id())->orderBy('id', 'DESC')->get();
        
        if (auth()->user()->is_seller == false) {
            $messages = Message::where('user_id', auth()->id())->orWhere('receiver', auth()->id())->orderBy('id', 'DESC')->get();
        } else {
            $messages = Message::where('user_id', $sender)->orWhere('receiver', $sender)->orderBy('id', 'DESC')->get();
        }

        return view('show2', [
            'users' => $users,
            'messages' => $messages,
            'sender' => $sender,
        ]);
    }

}
