<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Homediv;

class HomedivController extends Controller
{

   public function index()
    {
        $homedivs = Homediv::all();
        return view('homediv.index', compact('homedivs'));
    }




   public function create()
    {
		return view('homediv.create');
    }





   public function store(Request $request)
    {
        $plan = new Homediv;
        $plan->icon = $request->icon;
        $plan->heading = $request->heading;
        $plan->content = $request->content;
       
        if($plan->save()){
            flash(__('Content has been inserted successfully'))->success();
            return redirect()->route('homediv.index');
        }
		else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }




   public function edit($id)
    {
        $homediv = Homediv::findOrFail(decrypt($id));
        return view('homediv.edit', compact('homediv'));
    }



   public function update(Request $request, $id)
    {
        $plan = Homediv::findOrFail($id);

        $plan->icon = $request->icon;
        $plan->heading = $request->heading;
        $plan->content = $request->content;
        
        if($plan->save()){
            flash(__('Content has been updated successfully'))->success();
            return redirect()->route('homediv.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }


   public function destroy($id)
    {
        if(Homediv::destroy($id)){
            flash(__('Content has been deleted successfully'))->success();
            return redirect()->route('homediv.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }




}

?>