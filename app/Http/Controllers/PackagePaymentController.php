<?php

namespace App\Http\Controllers;
use DB;
use App\Http\Requests;
use App\Models\Product;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Carbon\Carbon;


class PackagePaymentController extends Controller
{
    private $_api_context;
    protected $data;
    public function __construct()
    {
        // $paypal_configuration = \Config::get('paypal');
        // $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_configuration['client_id'], $paypal_configuration['secret']));
        // $this->_api_context->setConfig($paypal_configuration['settings']);
    }

    public function payment()
    {


        $pak_plan = Session()->get('pak_plan');
        
            $pricing_plan = \DB::table('pricing_plans')->where('id', $pak_plan)->first();

            $name = $pricing_plan->name;
// dd($name);
            $price = $pricing_plan->per_month;
            
            $admin = DB::table('users')->where('user_type', 'admin')->first();
            
            $admin_paypal_c_id = $admin->paypal_c_id;
            $admin_paypal_s_id = $admin->paypal_s_id;
            
         $paypal_configuration = [
            'client_id' => $admin_paypal_c_id,
            'secret' =>  $admin_paypal_s_id,
            'settings' => array(
                'mode' => 'sandbox',
                'http.ConnectionTimeOut' => 1000,
                'log.LogEnabled' => true,
                'log.FileName' => storage_path() . '/logs/paypal.log',
                'log.LogLevel' => 'FINE'
            ),
        ];
        // dd($paypal_configuration);
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_configuration['client_id'], $paypal_configuration['secret']));
        // dd($this->_api_context);
        $this->_api_context->setConfig($paypal_configuration['settings']);
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

    	$item_1 = new Item();

        $item_1->setName($name)
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($price);

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        $total = $price;
        $amount->setCurrency('USD')
            ->setTotal($total);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Package purchasing is done');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('package.status'))
            ->setCancelUrl(URL::route('package.status'));

        $payment = new Payment();
        // dd($payment);
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error','Connection timeout');
                return Redirect::route('pricing');
            } else {
                \Session::put('error','Some error occur, sorry for inconvenient');
                return Redirect::route('pricing');
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {
            return Redirect::away($redirect_url);
        }

        \Session::put('error','Unknown error occurred');
            return Redirect::route('pricing');
    }

    public function getPaymentStatus(Request $request)
    {
        $admin = DB::table('users')->where('user_type', 'admin')->first();
        $admin_paypal_c_id = $admin->paypal_c_id;
        $admin_paypal_s_id = $admin->paypal_s_id;        
         $paypal_configuration = [
            'client_id' => $admin_paypal_c_id,
            'secret' =>  $admin_paypal_s_id,
            'settings' => array(
                'mode' => 'sandbox',
                'http.ConnectionTimeOut' => 1000,
                'log.LogEnabled' => true,
                'log.FileName' => storage_path() . '/logs/paypal.log',
                'log.LogLevel' => 'FINE'
            ),
        ];       
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_configuration['client_id'], $paypal_configuration['secret']));
        $this->_api_context->setConfig($paypal_configuration['settings']);
        
        $payment_id = Session::get('paypal_payment_id');

        Session::forget('paypal_payment_id');
        if (empty($request->input('PayerID')) || empty($request->input('token'))) {
            flash(__('Payment failed.'))->error();
            return Redirect::route('pricing');
        }
        $payment = Payment::get($payment_id, $this->_api_context);        
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input('PayerID'));        
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->state == 'approved') {
            $user_id = Session()->get('new_user_id');
            $pak_plan = Session()->get('pak_plan');
            $pricing_plan = \DB::table('pricing_plans')->where('id', $pak_plan)->first();
            $user = \App\Models\User::where('id',$user_id)->first();
            $user->package_plane = $pricing_plan->name;
            $user->package_for  = $pricing_plan->permissions;
            $user->package  = $pricing_plan->id;
            $user->package_start   = Carbon::now()->toDateTimeString();
            // dd($pricing_plan->name);
            // if($pricing_plan->name == 'month'){
            $user->package_end = $today = Carbon::now()->addMonth(1)->toDateTimeString();;
            // }
            // if($pricing_plan->name == 'year'){
            // $shop->package_end = $today = Carbon::now()->addYear(1)->toDateTimeString();;
            // }
            
            // $user->package_end  = $pricing_plan->id;
            $user->save();
            $to_name = $user->name;
            $to_email = $user->email;
            $data = array('name'=>$user->name, "body" => "Your package is confirmed");
            \Mail::send("emails.packagemail", $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
            ->subject("Package confirm email.");
            $message->from("zain.ulabidin7501@gmail.com","Package email.");
            });
        }
        // dd($result);
        if ($result->getState() == 'approved') {         
            // \Session::put('success','Payment success !!');
        flash(__('Payment is successfully done'))->success();
            return Redirect::route('pricing');
        }

        flash(__('Payment failed.'))->error();
		return Redirect::route('pricing');
    }

}
