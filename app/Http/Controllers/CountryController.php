<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\City;

class CountryController extends Controller
{

    public function country(Request $request)
    {
        $countries = Country::all();
        //$active_countries = Country::where('code', 'AF')->get();
        //return view('business_settings.country', compact('countries', 'active_currencies'));
		return view('business_settings.country', compact('countries', 'countries'));
    }

	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function countrystore(Request $request)
    {
        $Count = Country::where('asciiname', '=', $request->name)->orWhere('code', $request->code)->count();
		if($Count == 0){
			$country = new Country;
			$country->name = $request->name;
			$country->asciiname = $request->name;
			$country->code = $request->code;
			$country->status = 1;
			$country->updated_at = date('Y-m-d H:i:s');
			$country->created_at = date('Y-m-d H:i:s');
			//print_r($country);exit;
			if($country->save()){
				flash(__('Country has been inserted successfully'))->success();
				return redirect()->route('country.index');
			}
			else{
				flash(__('Something went wrong'))->error();
				return back();
			}
		}else{
			flash(__('Country Name OR Code already exist'))->error();
			return back();
		}
    }

    public function updateCountry(Request $request)
    {
        $country = Country::findOrFail($request->id);
        $country->status = $request->status;
        if($country->save()){
            flash('Country updated successfully')->success();
            return '1';
        }
        flash('Something went wrong')->error();
        return '0';
    }

    public function updateYourCountry(Request $request)
    {
        $country = Country::findOrFail($request->id);
        $country->name = $request->name;
        $country->code = $request->code;
        $country->status = $request->status;
        if($country->save()){
            flash('Country updated successfully')->success();
            return '1';
        }
        flash('Something went wrong')->error();
        return '0';
    }

	public function cities(Request $request)
    {
		$CitiesOptions = '<option value="">Please Select</option>';
		$cities = City::all();
		$cities = $cities->where('country_code', '=', $request->country_code);
		foreach($cities as $city){
			$CitiesOptions .= '<option value="'.$city->id.'">'.$city->name.'</option>';
		}
		return json_encode(array('options'=>$CitiesOptions));
    }
}
