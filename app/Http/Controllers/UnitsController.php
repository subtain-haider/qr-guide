<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Units;
use Hash;

class UnitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$units = Units::all();
        return view('units.index', compact('units'));
    }
	

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('units.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $unit = new Units;
		$Units = Units::where('name', $request->full_name)->orWhere('full_name', $request->full_name)->orWhere('full_name', $request->name)->orWhere('full_name', $request->name)->count();
		if($Units == 0){
			$unit->full_name = $request->full_name;
			$unit->name = $request->name;
			$unit->updated_at = date('Y-m-d H:i:s');
			$unit->created_at = date('Y-m-d H:i:s');
			if($unit->save()){
				flash(__('Unit has been inserted successfully'))->success();
				return redirect()->route('units.index');
			}else{
				flash(__('Something went wrong'))->error();
				return back();
			}
		}else{
			flash(__('Unit already exist!'))->error();
			return redirect()->route('units.create');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit = Units::findOrFail(decrypt($id));
        return view('units.edit', compact('unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unit = Units::findOrFail($id);
		$unit->name = $request->name;
		$unit->full_name = $request->full_name;
		if($unit->save()){
			flash(__('Unit has been updated successfully!'))->success();
			return redirect()->route('units.index');
		}
		
		flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Units::destroy($id)){
            flash(__('Unit has been deleted successfully'))->success();
            return redirect()->route('units.index');
        }

        flash(__('Something went wrong'))->error();
        return back();
    }
}
