<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Advertising;

class AdvertisingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$advertising = Advertising::all();
		return view('advertising.index', compact('advertising'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('advertising.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAdvertising(Request $request)
    {
		$advertising = new Advertising;
		$advertising->slug = $request->slug;
		$advertising->provider_name = $request->name;
		$advertising->tracking_code_large = $request->content;
		$advertising->status = 0;
		$advertising->updated_at = date('Y-m-d H:i:s');
		$advertising->created_at = date('Y-m-d H:i:s');
		//print_r($advertising);
		if($advertising->save()){
			flash(__('Advertise has been inserted successfully'))->success();
			return redirect()->route('advertising.index');
		}else{
			flash(__('Something went wrong'))->error();
			return back();
		}
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advertising = Advertising::findOrFail(decrypt($id));
        return view('advertising.edit', compact('advertising'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $advertising = Advertising::findOrFail($id);
        $advertising->slug = $request->slug;
		$advertising->provider_name = $request->name;
		$advertising->tracking_code_large = $request->content;
		$advertising->updated_at = date('Y-m-d H:i:s');
        if($advertising->save()){
            flash('Advertising updated successfully')->success();
			return redirect()->route('advertising.index');
        }
    }

	public function update_status(Request $request)
    {
        $advertising = Advertising::findOrFail($request->id);
        $advertising->status = $request->status;
        if($advertising->save()){
            flash('Advertising updated successfully')->success();
			return '0';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Advertising::destroy($id)){
            flash(__('Advertising has been deleted successfully'))->success();
            return redirect()->route('advertising.index');
        }

        flash(__('Something went wrong'))->error();
        return back();
    }
}
