<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Guiderequest;
use Auth;
use Session;

class GuiderequestController extends Controller
{

   public function index()
    {
        $requestuss = Guiderequest::all();
        return view('guiderequest.index', compact('requestuss'));
    }



   public function destroy($id)
    {
        if(Guiderequest::destroy($id)){
            flash(__('request has been deleted successfully'))->success();
            return redirect()->route('guiderequest.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }




}

?>