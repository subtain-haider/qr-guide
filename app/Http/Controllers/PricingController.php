<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PricingPlan;

class PricingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pricing_plans = PricingPlan::all();
        return view('pricing.index', compact('pricing_plans'));
    }

    public function packageinfo(){
        return view('frontend.packageinfo');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('pricing.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $plan = new PricingPlan;
        $plan->name = $request->name;
        $plan->per_month = $request->per_month;
        $plan->per_year = $request->per_year;
        $plan->permissions = json_encode($request->permissions);
        if($plan->save()){
            flash(__('Plan has been inserted successfully'))->success();
            return redirect()->route('pricing.index');
        }
		else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
	
	public function ajax(Request $request)
    {
		// $SubSubCategoryOption = $SubCategoryOption = '';
		// foreach($request->category as $category_id){
		// 	if($request->category_type == 'category'){
		// 		$SubCategories = SubCategory::where('category_id', $category_id)->get();
		// 		foreach($SubCategories as $SubCategory){
		// 			$SubCategoryOption .= '<option value="'.$SubCategory->id.'">'.$SubCategory->name.'</option>';
		// 		}
		// 	}
		// 	if($request->category_type == 'subcategory'){
		// 		$SubSubCategories = SubSubCategory::where('sub_category_id', $category_id)->get();
		// 		foreach($SubSubCategories as $SubSubCategory){
		// 			$SubSubCategoryOption .= '<option value="'.$SubSubCategory->id.'">'.$SubSubCategory->name.'</option>';
		// 		}
		// 	}
		// }
		// $options = array('subcategories'=>$SubCategoryOption, 'subsubcategories'=>$SubSubCategoryOption);
		// return json_encode($options);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $PricingPlan = PricingPlan::findOrFail(decrypt($id));
        return view('pricing.edit', compact('PricingPlan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $plan = PricingPlan::findOrFail($id);

        $plan->name = $request->name;
        $plan->per_month = $request->per_month;
        $plan->per_year = $request->per_year;
        $plan->permissions = json_encode($request->permissions);
        if($plan->save()){
            flash(__('Plan has been updated successfully'))->success();
            return redirect()->route('pricing.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(PricingPlan::destroy($id)){
            flash(__('Plan has been deleted successfully'))->success();
            return redirect()->route('pricing.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
}
