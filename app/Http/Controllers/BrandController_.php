<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use App\Models\Product;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::all();
        return view('brands.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$Category = Category::all();
		$SubCategory = SubCategory::all();
		$SubSubCategory = SubSubCategory::all();
        return view('brands.create', compact('Category', 'SubCategory', 'SubSubCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand = new Brand;
        $brand->name = $request->name;

        if($request->hasFile('logo')){
            $brand->logo = $request->file('logo')->store('uploads/brands');
        }

		if(!empty($request->category)){
			$category->category = json_encode($request->category);
			$data = openJSONFile('en');
			$data[$brand->name] = $brand->name;
			saveJSONFile('en', $data);
		}

		if(!empty($request->subcategory)){
			$category->subcategory = json_encode($request->subcategory);
			$data = openJSONFile('en');
			$data[$brand->name] = $brand->name;
			saveJSONFile('en', $data);
		}

		if(!empty($request->subsubcategory)){
			$category->subsubcategory = json_encode($request->subsubcategory);
			$data = openJSONFile('en');
			$data[$brand->name] = $brand->name;
			saveJSONFile('en', $data);
		}

		if($brand->save()){
            flash(__('Brand has been inserted successfully'))->success();
            return redirect()->route('brands.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

	public function ajax(Request $request)
    {
		$SubSubCategoryOption = $SubCategoryOption = '';
		foreach($request->category as $category_id){
			if($request->category_type == 'category'){
				$SubCategories = SubCategory::where('category_id', $category_id)->get();
				foreach($SubCategories as $SubCategory){
					$SubCategoryOption .= '<option value="'.$SubCategory->id.'">'.$SubCategory->name.'</option>';
				}
			}
			if($request->category_type == 'subcategory'){
				$SubSubCategories = SubSubCategory::where('sub_category_id', $category_id)->get();
				foreach($SubSubCategories as $SubSubCategory){
					$SubSubCategoryOption .= '<option value="'.$SubSubCategory->id.'">'.$SubSubCategory->name.'</option>';
				}
			}
		}
		$options = array('subcategories'=>$SubCategoryOption, 'subsubcategories'=>$SubSubCategoryOption);
		return json_encode($options);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::findOrFail(decrypt($id));
		$Category = Category::all();
		$SubCategory = SubCategory::all();
		$SubSubCategory = SubSubCategory::all();
        return view('brands.edit', compact('Category', 'SubCategory', 'SubSubCategory', 'brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = Brand::findOrFail($id);
        $brand->name = $request->name;
        if($request->hasFile('logo')){
            $brand->logo = $request->file('logo')->store('uploads/brands');
        }

        if($brand->save()){
            flash(__('Brand has been updated successfully'))->success();
            return redirect()->route('brands.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::findOrFail($id);
        Product::where('brand_id', $brand->id)->delete();
        if(Brand::destroy($id)){
            if($brand->logo != null){
                //unlink($brand->logo);
            }
            flash(__('Brand has been deleted successfully'))->success();
            return redirect()->route('brands.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
}
