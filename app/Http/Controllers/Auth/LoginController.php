<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use App\Models\Customer;
// use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {
        // dd("login");
        Auth::logout();
        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
            if(Auth::user()->user_type == 'admin')
            {
                return redirect('/admin');
            }
            else
            {
                return redirect('/dashboard');
            }
        }
    }
        public function redirectToProvider(Request $request , $provider)
    {
        session(['slug' => $request->slug]);
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request , $provider)
    {
        try {
            $user = Socialite::driver($provider)->stateless()->user();
            // dd($user);
        } catch (\Exception $e) {
            // dd($e);
            return redirect()->route('user.login');
        }

        // check if they're an existing user
        $existingUser = User::where('email', $user->email)->first();
        // dd($existingUser);
        if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            // create a new user
            $newUser                  = new User;
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            $newUser->email_verified_at = date('Y-m-d H:m:s');
            $newUser->provider_id     = $user->id;
            $newUser->avatar          = $user->avatar;
            $newUser->avatar_original = $user->avatar_original;
            $newUser->slug = $request->session()->get('slug');
            $newUser->save();

            $customer = new Customer;
            $customer->user_id = $newUser->id;
            $customer->save();
            
            Auth::login($newUser);
        }
        return redirect()->route('login');
    }


    /**
     * Check user's role and redirect user based on their role
     * @return
     */
    public function authenticated()
    {
        if(auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'staff')
        {
			if(auth()->user()->status == 1){
			
				$this->guard()->logout();
				
				$redirect_route = 'login';
				
				return redirect()->route($redirect_route);
			
			}else{
				return redirect()->route('admin.dashboard');
			
			}
        }

        return redirect()->route('login');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function logout(Request $request)
    // {
    //     // dd("logout");
    //     if(auth()->user() != null && (auth()->user()->user_type == 'admin' || auth()->user()->user_type == 'staff')){
    //         $redirect_route = 'user.login';
    //     }
    //     else{
    //         $redirect_route = 'user.login';
    //     }

    //   Auth::logout();

    //     // $request->session()->invalidate();

    // }
}
