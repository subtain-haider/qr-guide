<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Redirect;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Review;
use App\Models\User;
use App\Models\Seller;
use App\Models\Order;
use App\Models\Points;
use App\Models\BusinessSetting;
use App\Models\Warranty;
use App\Models\Club;
use App\Models\Comment;
use App\Models\Coupon;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use QrCode;
use PDF;
use Auth;
use Hash;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
		// $shop = Auth::user()->shop;
// 		$shops = filter_products(Shop::where('user_id', Auth::user()->id)->orderBy('id', 'desc'))->paginate(12);
        $shops = Shop::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->paginate(12);

        // $shops = Shop::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        return view('frontend.seller.shops', compact('shops'));
    }

    //downloads QR Codes
    public function qr_codes_download($id)
    {
        $shop = Shop::findOrFail($id);
        $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                        'logOutputFile' => storage_path('logs/log.htm'),
                        'tempDir' => storage_path('logs/')
                    ])->loadView('frontend.partials.qr_codes', compact('shop'));
        return $pdf->download('qr_code-'.$shop->slug.'.pdf');
    }

    public function allqr_codes_download($id)
    {
        $shop = Shop::where('user_id', $id)->get();
        $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                        'logOutputFile' => storage_path('logs/log.htm'),
                        'tempDir' => storage_path('logs/')
                    ])->loadView('frontend.partials.allqr_codes', compact('shop'));
        return $pdf->download('qr_code-allslugqr.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('frontend.seller.shop');
    }

    public function confirmation(Request $request)
    {

        $data = $request->all();
        // return Product::where('id', $data['product'])->first();

        return view('frontend.seller.shop_confirmation', compact('data'));

    }
    public function questionsubmit(Request $request)
    {
        $shop  = Shop::where('slug', $request->slug)->first();
        $questions = $shop->questions;
        $arr = json_decode($questions, TRUE);
        $arr[] = ["question" => $request->question,"answer" => "null","status" => "0"];
        $newquestion = json_encode($arr);
        $qu = Shop::where('slug', $request->slug)->update(['questions' => $newquestion]);
        if($qu != null){
            return Redirect::back();
            return view('frontend.seller_shop', compact('data'));
        }
        abort(404);

    }
    public function reviewssubmit(Request $request)
    {
        if(!Auth::check()){
            flash('Please Login for Reviews')->error();
            return redirect()->route('home');
        }
        else{
            if(Review::where('product_id', $request->product_id)->where('user_id', Auth::user()->id)->exists())
            {
                flash('Review already submitted')->success();
                return back();
            } else {
                $review = new Review;
                $review->product_id = $request->product_id;
                $review->user_id = Auth::user()->id;
                $review->rating = $request->rating;
                $review->comment = $request->comment;
                
                if($review->save()){
                    $product = Product::findOrFail($request->product_id);
                    if(count(Review::where('product_id', $product->id)->where('status', 1)->get()) > 0){
                        $product->rating = Review::where('product_id', $product->id)->where('status', 1)->sum('rating')/count(Review::where('product_id', $product->id)->where('status', 1)->get());
                    }
                    else {
                        $product->rating = 0;
                    }
                    $product->save();
                    flash('Review has been submitted successfully')->success();
                    return back();
                }
            }
        
                flash('Something went wrong')->error();
                return back();
            }
}


    public function clubsubmit(Request $request)
    {
        $club = new Club;
        $club->slug = $request->slug;
        $club->email = $request->email;
        $club->contact_no = $request->contact_no;
        $club->dob = $request->dob;
        $club->getinfo = $request->check;
        if($club->save()){
            flash('Information has been submitted successfully')->success();
            return back();
        }
        flash('Something went wrong')->error();
        return back();
    }
    public function clubinfo()
    {
        $club = Club::get();
        return view('frontend.club_page', compact('club'));

    }

    public function club_form($slug)
    {
        $shop = Shop::where('slug',$slug)->first();
        return view('frontend.clubinfo', compact('shop'));

    }

    public function commentsubmit(Request $request)
    {
        $comment = new Comment;
        $comment->shop_id = $request->shop_id;
        $comment->user_id = $request->user_id;
        $comment->email = $request->email;
        $comment->comment = $request->comment;
        if($comment->save()){
            flash('Information has been submitted successfully')->success();
            return back();
        }
        flash('Something went wrong')->error();
        return back();
    }

    public function company_suggessions($uid)
    {
        $comments = Comment::where('user_id',$uid)->get();
        return view('frontend.suggession_page', compact('comments'));
    }

    public function seller_questions($uid)
    {
        // $questions = Shop::where('id',$uid)->select('questions')->get();
      $shop  = Shop::where('user_id', $uid)->where('questions','!=',null)->get();

        return view('frontend.qa_page', compact('shop'));
    }
    public function totalsales()
    {
        $orders = Order::orderBy('code', 'desc')->where('user_id',Auth::user()->id)->get();
        return view('frontend.sale_page', compact('orders'));
    }
    public function salesshow($id)
    {
        $order = Order::findOrFail(decrypt($id));
        return view('frontend.sale_show', compact('order'));
    }
    public function destroys($id)
    {
        $order = Order::findOrFail($id);
        if($order != null){
            foreach($order->orderDetails as $key => $orderDetail){
                $orderDetail->delete();
            }
            $order->delete();
            flash('Order has been deleted successfully')->success();
        }
        else{
            flash('Something went wrong')->error();
        }
        return back();
    }
    public function warranties($uid)
    { 
        $warranties = Warranty::where('seller_id',$uid)->orderby('id','desc')->get();
        return view('frontend.warranties_page', compact('warranties'));
    }

    public function sellingreport()
    {
         $orders = Order::where('user_id', Auth::user()->id)->orderBy('code', 'desc')->paginate(9);
        // dd($orders);
        if(Auth::user()->user_type == 'seller'){
            return view('frontend.sellingreport', compact('orders'));
        }
        elseif(Auth::user()->user_type == 'customer'){
            return view('frontend.sellingreport', compact('orders'));
        }
        else {
            abort(404);
        }

    }
    public function clientsreport()
    {
        $orders = User::where('user_type','=','customer')->paginate(50);
        // dd($orders);
        if(Auth::user()->user_type == 'seller'){
            return view('frontend.clientsreport', compact('orders'));
        }
        elseif(Auth::user()->user_type == 'customer'){
            return view('frontend.clientsreport', compact('orders'));
        }
        else {
            abort(404);
        }

    }
    
//     public function exportCsv(Request $request)
// {
//   $fileName = 'tasks.csv';
//   $tasks = User::where('user_type','=','customer');
   
//           $headers = array(
//             "Content-type"        => "text/csv",
//             "Content-Disposition" => "attachment; filename=$fileName",
//             "Pragma"              => "no-cache",
//             "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
//             "Expires"             => "0"
//         );

//         $columns = array('ID', 'Name', 'Email', 'ip', 'Country', 'regionName', 'city', 'postal_code', 'phone');
          
//         $callback = function() use($tasks, $columns) {
            
//             $file = fopen('php://output', 'w');
//             fputcsv($file, $columns);
                
//             foreach ($tasks as $task) {
                
//                 $row['id']  = '2';
//                 $row['name']    = $task->assign->name;
//                 $row['email']    = $task->email;
//                 $row['ip']  = $task->ip;
//                 $row['country']  = $task->country;
//                 $row['regionName']  = $task->regionName;
//                 $row['city']  = $task->city;
//                 $row['postal_code']  = $task->postal_code;
//                 $row['phone']  = $task->phone;
                

//                 fputcsv($file, array($row['id'], $row['name'], $row['ip'], $row['country'], $row['regionName'],  $row['city'],  $row['postal_code'],  $row['phone']));
//             }

//             fclose($file);
//         };
        

//         return response()->stream($callback, 200, $headers);
//     }
    
    
        public function exportCsv() 

    {

        return Excel::download(new UsersExport, 'users.xlsx');

    }
    public function sendemailtocustomer(Request $request)
    {
        $msg = $request->reply;

        // use wordwrap() if lines are longer than 70 characters
        $msg = wordwrap($msg,70);
        if(mail($request->email,"Reply",$msg)){
            flash('Information has been submitted successfully')->success();
            return back();
        }
        flash('Something went wrong')->error();
        return back();
    }

    public function destroyclub($id)
    {
        $club = Club::find($id);

        if($club->delete()){
            flash(__('Your club has been deleted successfully!'))->success();
            return redirect()->back();
        }
    }

    public function destroycomment($id)
    {
        $shop = Comment::find($id);

        if($shop->delete()){
            flash(__('Your comment has been deleted successfully!'))->success();
            return redirect()->back();
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
       public function store(Request $request)
    {
        
        $tabs = explode(',', $request->tab_sorting);
        if(in_array('main-product', $tabs)){
        $user = null;
        $recent_points = 0;
		if(!Auth::check()){
            return redirect()->route('home');
        }
        else{
            $shop = new Shop;
            $shop->user_id = Auth::user()->id;
            // dd($request->warranty_check);
            if(isset($request->company_check)){
                $shop->company_check = $request->company_check;
            }
            if(isset($request->qa_check)){
                $shop->qa_check = $request->qa_check;
            }
            if(isset($request->couponcode_check)){
                $shop->couponcode_check = $request->couponcode_check;
            }
            if(isset($request->coupon_check)){
                $shop->coupon_check = $request->coupon_check;
            }
            if(isset($request->sm_support_check)){
                $shop->sm_support_check = $request->sm_support_check;
            }
            if(isset($request->pdf_check)){
                $shop->pdf_check = $request->pdf_check;
            }
            if(isset($request->main_product_club_check)){
                $shop->main_product_club_check = $request->main_product_club_check;
            }
            if(isset($request->info_check)){
                $shop->info_check = $request->info_check;
            }
            if(isset($request->warranty_check)){
                $shop->warranty_check = $request->warranty_check;
            }
            if(isset($request->club_check)){
                $shop->club_check = $request->club_check;
            }
            if(isset($request->related_check)){
                $shop->related_check = $request->related_check;
            }
            if(isset($request->company_check)){
                $shop->company_check = $request->company_check;
            }
            if(isset($request->footer_check)){
                $shop->footer_check = $request->footer_check;
            }
            if(isset($request->buynonw_check)){
                $shop->buynonw_check = $request->buynonw_check;
            }
            
             if(isset($request->font)){
                $shop->font = $request->font;
            }

            if(isset($request->video_check)){
                $shop->video_check = $request->video_check;
            }

            if(isset($request->status)){
                $shop->tbasstatus = $request->status;
            }
            if(isset($request->name)){
                $shop->name = $request->name;
            }
            if(isset($request->address)){
                $shop->address = $request->address;
            }
            if(isset($request->whatsapp_number)){
                $shop->contact_no = $request->whatsapp_number;
            }
            if(isset($request->company_email)){
                $shop->company_email = $request->company_email;
            }
            if(isset($request->company_website)){
                $shop->company_website = $request->company_website;
            }
            if(isset($request->description)){
                $shop->description = $request->description;
            }
            $title = '';
            if(isset($request->title)){
                $shop->title = $request->title;
                $title = $request->title;
            }

            if(isset($request->minibgcolor)){
            $shop->minibgcolor = $request->minibgcolor;
            }

            if(isset($request->video_provider)){
                $shop->video_provider = $request->video_provider;
            }

            if(isset($request->video_link)){
                $shop->video_link = $request->video_link;
            }

            if(isset($request->social_bgcolor)){
                $shop->social_bgcolor = $request->social_bgcolor;
            }

            if(isset($request->facebook_link)){
                $shop->facebook = $request->facebook_link;
            }

            if(isset($request->twitter_link)){
                $shop->twitter = $request->twitter_link;
            }

            if(isset($request->instagram_link)){
                $shop->instagram = $request->instagram_link;
            }

            if(isset($request->linkedin_link)){
                $shop->linkedin = $request->linkedin_link;
            }

            if(isset($request->youtube_link)){
                $shop->youtube = $request->youtube_link;
            }

            if(isset($request->tiktok)){
                $shop->tiktok = $request->tiktox;
            }

            if(isset($request->whatsapp)){
                $shop->whatsapp = $request->whatsapp;
            }

            if($request->hasFile('video')){
                $shop->video_link = $request->video->store('uploads/minisite/videos');
            }
            $path=array();
            if($request->hasFile('banner')){
                foreach ($request->banner as $key => $photo) {
                $imageName = date('mdYHis') . uniqid() .'.'.$photo->extension();
                $store = $photo->storeAs('public/products/photos/',$imageName);
                $path[]  = "storage/products/photos/".$imageName;

                }
             $shop->sliders = json_encode($path);
            }
            if($request->hasFile('logo')){
                $imageName = time().'.'.$request->logo->extension();
                $store = $request->logo->storeAs('public/products/photos/',$imageName);
                $path  = "storage/products/photos/".$imageName;
                $shop->logo = $path;
            }

            $pdfs = array();
            if($request->hasFile('pdf')){
                foreach ($request->pdf as $key => $pdf) {
                    $imageName = time().'.'.$pdf->extension();
                    $store = $pdf->storeAs('public/minisite/pdf/',$imageName);
                    $path  = "storage/minisite/pdf/".$imageName;
                    $pdf_details = array('path'=>$path, 'lng'=>$request->lng[$key]);
                    array_push($pdfs, $pdf_details);
                    //ImageOptimizer::optimize(base_path('public/').$path);
                }
                $shop->pdf = json_encode($pdfs);
            }

            if(isset($request->pdf_title)){
                $shop->pdf_title = $request->pdf_title;
            }

            if(isset($request->pdf_text)){
                $shop->pdf_text = $request->pdf_text;
            }

            $questions = array();
            if(isset($request->question)){
                foreach ($request->question as $key => $question) {
                    if(!empty($question)){
                        $question_answer = array('question'=>$question, 'answer'=>$request->answer[$key],'status'=>'1');
                        array_push($questions, $question_answer);
                    }
                }
                $shop->questions = json_encode($questions);
            }
            if(isset($request->link)){
                $shop->external_link = $request->link;
            }
            if(isset($request->tab_sorting)){
                $shop->tabs = $request->tab_sorting;
            }
            if(isset($request->club_text)){
                $shop->club_text = $request->club_text;
            }
            
            if(isset($request->category_id)){
                $shop->category_id = $request->category_id;
            }
            if(isset($request->subcategory_id)){
                $shop->subcategory_id = $request->subcategory_id;
            }
            if(isset($request->subsubcategory_id)){
                $shop->subsubcategory_id = $request->subsubcategory_id;
            }
            if(isset($request->related_products)){
                $shop->related_products = json_encode($request->related_products);
            }
            
            if(isset($request->animateproducts)){
                $shop->animateproducts = $request->animateproducts;
            }
             if(isset($request->animateguide)){
                $shop->animateguide = $request->animateguide;
            }
             if(isset($request->animateprofile)){
                $shop->animateprofile = $request->animateprofile;
            }
             if(isset($request->animatevideo)){
                $shop->animatevideo = $request->animatevideo;
            }
             if(isset($request->animateslides)){
                $shop->animateslides = $request->animateslides;
            }
             if(isset($request->animatepdf)){
                $shop->animatepdf = $request->animatepdf;
            }
             if(isset($request->animateclub)){
                $shop->animateclub = $request->animateclub;
            }
             if(isset($request->animatewarranty)){
                $shop->animatewarranty = $request->animatewarranty;
            }
             if(isset($request->animatesocial)){
                $shop->animatesocial = $request->animatesocial;
            }
             if(isset($request->animatefaq)){
                $shop->animatefaq = $request->animatefaq;
            }
             if(isset($request->animatereviews)){
                $shop->animatereviews = $request->animatereviews;
            }
             if(isset($request->animatecoupan)){
                $shop->animatecoupan = $request->animatecoupan;
            }
            if(isset($request->selectedcoupon)){
                $shop->coupon_act = $request->coupon_act;
                $shop->coupon_id = $request->selectedcoupon;
            }
            if(isset($request->product)){
                $shop->product = $request->product;
            }
            
            if(isset($request->metatag)){
                $shop->metatag = $request->metatag;
            }
            
            if(isset($request->metadescription)){
                $shop->metadescription = $request->metadescription;
            }
            
            if(isset($request->faq)){
                $shop->faq = $request->faq;
            }
            if(isset($request->reviews)){
                $shop->reviews = $request->reviews;
            }
            if(isset($request->warranty)){
                $form = array();
                $select_types = ['select', 'multi_select', 'radio'];
                $j = 0;
                if(isset($request->type)) {
                    for ($i=0; $i < count($request->type); $i++) {
                        $item['type'] = $request->type[$i];
                        $item['label'] = $request->label[$i];
                        if(in_array($request->type[$i], $select_types)){
                            $item['options'] = json_encode($request['options_'.$request->option[$j]]);
                            $j++;
                        }
                        array_push($form, $item);
                    }
                }
                $shop->warranty_text = $request->warranty_text;
                $shop->warranty = json_encode($form);
                $shop->warranty_content = $request->content;
                $shop->w_edate = $request->edate;
            }
            if(!empty($title)){
                $shop->slug = preg_replace('/\s+/', '-', $title);//.'-'.$shop->id;//$title.$shop->id;//preg_replace('/\s+/', '-', $title).'-'.$shop->id;
            }else{
                $shop->slug = time().'-'.$shop->id;
            }
	    if(isset($request->coupon_active)){
                $shop->c_active = $request->coupon_active;
                $shop->coupon_text = json_encode($request->coupon_text);
            }
            if(isset($request->submitbutton)){
                $shop->published = 1;
            } else {
                $shop->published = 0;
            }
            if($shop->save()){
                flash(__('Your Minisite has been created successfully!'))->success();
                return redirect()->route('company.index');
            }
        }
        } else {
            flash(__('Sorry! Please Add Atleast one Guide .'))->error();
            return back();
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        $shop = Shop::find($id);

        if($shop != ''){
            return view('frontend.seller.edit_shop', compact('shop'));
        }
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function slugchange(Request $request, $id)
    {
        if (isset($request->slug) && Shop::where('slug', '=', $request->slug)->where('id','!=',$id)->exists() ) {
               
                flash(__('Sorry! Minisite URL already exist.'))->error();
                return back();
            }
        else{
            $qu = Shop::where('id', $id)->update(['slug' => $request->slug]);
            flash(__('Your URL has been Updated successfully!'))->success();
                return back();
        }
        
        
    }
    public function update(Request $request, $id)
    {
       
        $shop = Shop::find($id);
        // if(!$request->warranty_check)
        //  {
            
        //         $shop->warranty_check = null;
        //  }
        //  else
        //  {
        //      $shop->video_check = $request->video_check;
        //  }
        // if(!$request->club_check)
        //  {
        //         $shop->club_check = null;
        //  }
        //  else
        //  {
        //      $shop->club_check = $request->club_check;
        //  }
        // if(!$request->related_check)
        //  {
        //         $shop->related_check = null;
        //  }
        //  else
        //  {
        //      $shop->related_check = $request->related_check;
        //  }
        // if(!$request->video_check)
        //  {
        //         $shop->video_check = null;
        //  }
        //  else
        //  {
        //      $shop->video_check = $request->video_check;
        //  }

        if(isset($request->name)){
            $shop->name = $request->name;
        } else {
            $shop->name = NULL;
        }
        if(isset($request->address)){
            $shop->address = $request->address;
        } else {
            $shop->address = NULL;
        }
        if(isset($request->whatsapp_number)){
            $shop->contact_no = $request->whatsapp_number;
        } else {
            $shop->contact_no = NULL;
        }
        if(isset($request->company_email)){
            $shop->company_email = $request->company_email;
        } else {
            $shop->company_email = NULL;
        }
        if(isset($request->company_website)){
            $shop->company_website = $request->company_website;
        } else {
            $shop->company_website = NULL;
        }
        if(isset($request->description)){
            $shop->description = $request->description;
        } else {
            $shop->description = NULL;
        }
        $title = '';
        if(isset($request->title)){
            $shop->title = $request->title;
            $title = $request->title;
        } 

        if(isset($request->minibgcolor)){
            $shop->minibgcolor = $request->minibgcolor;
        } else {
            $shop->minibgcolor = NULL;
        }

        $slug = '';
        
        if(isset($request->slug)){
            if (isset($request->urlupdate) && Shop::where('slug', '=', $request->slug)->exists() ) {
               
                flash(__('Sorry! Minisite URL already exist.'))->error();
                return back();
            }
            else {
                $shop->slug = $request->validate([
                'slug' => 'required|max:20',
        
                ]);
                if (isset($request->downbutton) && Shop::where('slug', '=', $request->slug)->where('id','!=',$id)->exists() ) {
                    flash('Sorry! Minisite URL already exist.')->error();
                    return back();
                } else {
                    $slug = $request->slug;
                }
            }
            
           
            
            
              
        //     $data = Shop::where('user_id',Auth::user()->id)->first();
        //     if($data->slug == $request->slug)
        //     {
               
        //     }
        //     else
        //     {
        //         flash(__('Sorry! Minisite URL already exist.'))->error();
        // return back();
        //     }
            
   
            
        
            
        }

        if(isset($request->video_provider)){
            $shop->video_provider = $request->video_provider;
        } else {
            $shop->video_provider = NULL;
        }

        if(isset($request->video_link)){
            $shop->video_link = $request->video_link;
        } else {
            $shop->video_link = NULL;
        }

        if(isset($request->social_bgcolor)){
            $shop->social_bgcolor = $request->social_bgcolor;
        } else {
            $shop->social_bgcolor = NULL;
        }

        if(isset($request->facebook_link)){
            $shop->facebook = $request->facebook_link;
        } else {
            $shop->facebook = NULL;
        }

        if(isset($request->twitter_link)){
            $shop->twitter = $request->twitter_link;
        } else {
            $shop->twitter = NULL;
        }

        if(isset($request->instagram_link)){
            $shop->instagram = $request->instagram_link;
        } else {
            $shop->instagram = NULL;
        }

        if(isset($request->linkedin_link)){
            $shop->linkedin = $request->linkedin_link;
        } else {
            $shop->linkedin = NULL;
        }

        if(isset($request->youtube_link)){
            $shop->youtube = $request->youtube_link;
        } else {
            $shop->youtube = NULL;
        }

        if(isset($request->tiktox_link)){
            $shop->tiktok = $request->tiktox_link;
        } else {
            $shop->tiktok = NULL;
        }
        
        if(isset($request->footer_check)){
            $shop->footer_check = $request->footer_check;
        } else {
            $shop->footer_check = NULL;
        }
        if(isset($request->buynonw_check)){
            $shop->buynonw_check = $request->buynonw_check;
        } else {
            $shop->buynonw_check = NULL;
        }
        if(isset($request->font)){
            $shop->font = $request->font;
        } else {
            $shop->font = NULL;
        }
           
        if(isset($request->metatag)){
            $shop->metatag = $request->metatag;
        } else {
            $shop->metatag = NULL;
        }
            
        if(isset($request->metadescription)){
            $shop->metadescription = $request->metadescription;
        } else {
            $shop->metadescription = NULL;
        }
        
          if(isset($request->animateproducts)){
                $shop->animateproducts = $request->animateproducts;
            } else {
            $shop->animateproducts = NULL;
        }
             if(isset($request->animateguide)){
                $shop->animateguide = $request->animateguide;
            } else {
            $shop->animateguide = NULL;
        }
             if(isset($request->animateprofile)){
                $shop->animateprofile = $request->animateprofile;
            } else {
            $shop->animateprofile = NULL;
        }
             if(isset($request->animatevideo)){
                $shop->animatevideo = $request->animatevideo;
            } else {
            $shop->animatevideo = NULL;
        }
             if(isset($request->animateslides)){
                $shop->animateslides = $request->animateslides;
            } else {
            $shop->animateslides = NULL;
        }
             if(isset($request->animatepdf)){
                $shop->animatepdf = $request->animatepdf;
            } else {
            $shop->animatepdf = NULL;
        }
             if(isset($request->animateclub)){
                $shop->animateclub = $request->animateclub;
            } else {
            $shop->animateclub = NULL;
        }
             if(isset($request->animatewarranty)){
                $shop->animatewarranty = $request->animatewarranty;
            } else {
            $shop->animatewarranty = NULL;
        }
             if(isset($request->animatesocial)){
                $shop->animatesocial = $request->animatesocial;
            } else {
            $shop->animatesocial = NULL;
        }
             if(isset($request->animatefaq)){
                $shop->animatefaq = $request->animatefaq;
            } else {
            $shop->animatefaq = NULL;
        }
             if(isset($request->animatereviews)){
                $shop->animatereviews = $request->animatereviews;
            } else {
            $shop->animatereviews = NULL;
        }
             if(isset($request->animatecoupan)){
                $shop->animatecoupan = $request->animatecoupan;
            } else {
            $shop->animatecoupan = NULL;
        }
        if(isset($request->whatsapp)){
            $shop->whatsapp = $request->whatsapp;
        } else {
            $shop->whatsapp = NULL;
        }

        if($request->hasFile('video')){
            $shop->video_link = $request->video->store('uploads/minisite/videos');
        }

        if($request->hasFile('banner')){
            $path = array();
            foreach ($request->banner as $key => $photo) { 
                $imageName = date('mdYHis') . uniqid() .'.'.$photo->extension();
                $store = $photo->storeAs('public/products/photos/',$imageName);
                $path[]  = "storage/products/photos/".$imageName;
                // $shop->sliders = $path;
            }
           $shop->sliders = json_encode($path);
        }
        $pdfs = array();
        if($request->hasFile('pdf')){
            foreach ($request->pdf as $key => $pdf) {
                $imageName = time().'.'.$pdf->extension();
                    $store = $pdf->storeAs('public/minisite/pdf/',$imageName);
                    $path  = "storage/minisite/pdf/".$imageName;
                    $pdf_details = array('path'=>$path, 'lng'=>$request->lng[$key]);
                    array_push($pdfs, $pdf_details);
                //ImageOptimizer::optimize(base_path('public/').$path);
            }
            $shop->pdf = json_encode($pdfs);
        } else {
            $shop->pdf = NULL;
        }

        if($request->hasFile('logo')){
            $imageName = time().'.'.$request->logo->extension();
                $store = $request->logo->storeAs('public/products/photos/',$imageName);
                $path  = "storage/products/photos/".$imageName;
                $shop->logo = $path;
            }

        if(isset($request->pdf_title)){
            $shop->pdf_title = $request->pdf_title;
        } else {
            $shop->pdf_title = NULL;
        }

        if(isset($request->pdf_text)){
            $shop->pdf_text = $request->pdf_text;
        } else {
            $shop->pdf_text = NULL;
        }

        $questions = array();
        if(isset($request->question)){
            foreach ($request->question as $key => $question) {
                if(!empty($question)){
                    $question_answer = array('question'=>$question, 'answer'=>$request->answer[$key],'status'=>'1');
                    array_push($questions, $question_answer);
                }
            }
            // array_push($questions, ['status'=>'1']);
            $shop->questions = json_encode($questions);
        } else {
            $shop->questions = $questions;
        }
        if(isset($request->link)){
            $shop->external_link = $request->link;
        } else {
            $shop->external_link = NULL;
        }
        if(isset($request->tab_sorting)){
            $shop->tabs = $request->tab_sorting;
        } else {
            $shop->tabs = $shop->tabs;
        }
        if(isset($request->club_text)){
            $shop->club_text = $request->club_text;
        } else {
            $shop->club_text = NULL;
        }
        if(isset($request->coupon_code)){
                $shop->coupon_code = $request->coupon_code;
            } 
            if(isset($request->coupon_end)){
                $shop->coupon_end = $request->coupon_end;
            } 
            if(isset($request->coupon_amount)){
                $shop->coupon_amount = $request->coupon_amount;
            } 
        if(isset($request->category_id)){
            $shop->category_id = $request->category_id;
        } else {
            $shop->category_id = NULL;
        }
        if(isset($request->subcategory_id)){
            $shop->subcategory_id = $request->subcategory_id;
        } else {
            $shop->subcategory_id = NULL;
        }
        if(isset($request->subsubcategory_id)){
            $shop->subsubcategory_id = $request->subsubcategory_id;
        } else {
            $shop->subsubcategory_id = NULL;
        }
        if(isset($request->related_products)){
            $shop->related_products = json_encode($request->related_products);
        } else {
            $shop->related_products = NULL;
        }
        if(isset($request->product)){
            $shop->product = $request->product;
        } else {
            $shop->product = NULL;
        }
        if(isset($request->faq)){
            $shop->faq = $request->faq;
        } else {
            $shop->faq = NULL;
        }
        if(isset($request->reviews)){
            $shop->reviews = $request->reviews;
        } else {
            $shop->reviews = NULL;
        }
        if(isset($request->warranty)){
            $form = array();
            $select_types = ['select', 'multi_select', 'radio'];
            $j = 0;
            if(isset($request->type)) {
            for ($i=0; $i < count($request->type); $i++) {
                $item['type'] = $request->type[$i];
                $item['label'] = $request->label[$i];
                if(in_array($request->type[$i], $select_types)){
                    $item['options'] = json_encode($request['options_'.$request->option[$j]]);
                    $j++;
                }
                array_push($form, $item);
            }
            }
            $shop->warranty_text = $request->warranty_text;
            $shop->warranty = json_encode($form);
            $shop->warranty_content = $request->content;
            $shop->w_edate = $request->edate;
        } else {
            $shop->w_edate = NULL;
            $shop->warranty_text = NULL;
            $shop->warranty_content = NULL;
            $shop->warranty = NULL;
        }
        if(!empty($slug)){
            $shop->slug = preg_replace('/\s+/', '-', $slug);//.'-'.$shop->id;
        }
        // $shop->slug = preg_replace('/\s+/', '-', $title).'-'.$shop->id;
        
        if(isset($request->coupon_active)){
            $shop->c_active = $request->coupon_active;
            $shop->coupon_text = json_encode($request->coupon_text);
        } else {
            $shop->c_active = 0;
            $shop->coupon_text = NULL;
        }
        if(isset($request->selectedcoupon)){
                $shop->coupon_act = $request->coupon_act;
                $shop->coupon_id = $request->selectedcoupon;
            } else {
                $shop->coupon_act = 0;
            $shop->coupon_id = 0;
        }
         if(isset($request->submitbutton)){
                $shop->published = 1;
            } else {
                $shop->published = 0;
            }   
        if($shop->save()){
            flash(__('Your Minisite has been updated successfully!'))->success();
            return redirect()->route('company.index');
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shop = Shop::find($id);
        if($shop != NULL){
            if($shop->pdf != NULL){
                $pdfs = json_decode($shop->pdf, true);
                foreach ($pdfs as $key => $pdf) {
                    if(file_exists(public_path('uploads/minisite/pdf' . $pdf['path']))) {
                        unlink(public_path('uploads/minisite/pdf' . $pdf['path']));
                    }
                }
            }

            if($shop->video_link != NULL){
                if(file_exists(public_path('uploads/minisite/videos' . $shop->video_link))) {
                    unlink(public_path('uploads/minisite/videos' . $shop->video_link));
                }
            }

            if($shop->sliders != NULL){
                $sliders = json_decode($shop->sliders, true);
                foreach ($sliders as $key => $photo) {
                    if(file_exists(public_path('uploads/photos' . $photo))) {
                        unlink(public_path('uploads/photos' . $photo));
                    }
                }
            }
            if($shop->delete()){
                flash(__('Your Minisite has been deleted successfully!'))->success();
                return redirect()->back();
            }
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    public function verify_form(Request $request)
    {
        if(Auth::user()->seller->verification_info == null){
            $shop = Auth::user()->shop;
            return view('frontend.seller.verify_form', compact('shop'));
        }
        else {
            flash(__('Sorry! You have sent verification request already.'))->error();
            return back();
        }
    }

    public function warranty_form(Request $request)
    {
    	return view('frontend.seller.warranty');
    }

    /**
     * Update warranty form.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function warranty_form_update(Request $request)
    {
        $form = array();
        $select_types = ['select', 'multi_select', 'radio'];
        $j = 0;
        if(isset($request->type)) {
            for ($i=0; $i < count($request->type); $i++) {
                $item['type'] = $request->type[$i];
                $item['label'] = $request->label[$i];
                if(in_array($request->type[$i], $select_types)){
                    $item['options'] = json_encode($request['options_'.$request->option[$j]]);
                    $j++;
                }
                array_push($form, $item);
            }
        }
        $warranty_settings = shop::where('id', Auth::user()->shop->id)->first();
        $warranty_settings->warranty = json_encode($form);
        $warranty_settings->warranty_content = $request->content;
        if($warranty_settings->save()){
            flash("Warranty Setting updated successfully")->success();
            return back();
        }
    }

    public function verify_form_store($slug, Request $request)
    {
        // $mytime = Carbon::now();
        // $mytime->toDateString();
        $shop = Shop::where('slug', $slug)->first();
        $datetime1 = strtotime($request->date_of_purchase); // convert to timestamps
        $datetime2 = strtotime('+'.$shop->w_edate .'days');
        $days = (int)(($datetime2 - $datetime1)/86400);
        if($days <= $shop->w_edate){
            $data = array();
            $i = 0;
            foreach (json_decode(Shop::where('slug', $slug)->first()->warranty) as $key => $element) {
                $item = array();
                if ($element->type == 'text') {
                    $item['type'] = 'text';
                    $item['label'] = $element->label;
                    $item['value'] = $request['element_'.$i];
                }
                elseif ($element->type == 'select' || $element->type == 'radio') {
                    $item['type'] = 'select';
                    $item['label'] = $element->label;
                    $item['value'] = $request['element_'.$i];
                }
                elseif ($element->type == 'multi_select') {
                    $item['type'] = 'multi_select';
                    $item['label'] = $element->label;
                    $item['value'] = json_encode($request['element_'.$i]);
                }
                elseif ($element->type == 'file') {
                    $item['type'] = 'file';
                    $item['label'] = $element->label;
                    $imageName = time().'.'.$request['element_'.$i]->extension();
                    $store = $request['element_'.$i]->storeAs('public/verification_form/',$imageName);
                    $path  = "storage/verification_form/".$imageName;
                    $item['value'] = $path;
                }
                array_push($data, $item);
                $i++;
            }
            $warranty = new Warranty;
            $warranty->info = json_encode($data);
            $warranty->shop_id = Shop::where('slug', $slug)->first()->id;
            if(Auth::check()){
                $warranty->user_id = Auth::user()->id;
                $warranty->seller_id = $shop->user_id;
                if($warranty->save()){
                flash(__('Your query has been submitted successfully!'))->success();
                return back()->with('success', 'Claim Warranty Has been Updated');
                }
            } else {
                flash(__('Sorry! Please login for Claim Warranty'))->error();
            return back()->with('fail', 'Please login for Claim Warranty');
            }
            
        } else {
        
            flash(__('Sorry! Something went wrong.'))->error();
            return back()->with('fail', 'Claim Warranty Has been Expired');
        }
    }
    public function warranty_invoice_download(Request $request)
    { 
        $shops = $request->all();
    //     $keys = array();
    //   foreach($shops as $key => $sho){
    //     //   if($key == 'shopid'){
    //         //   return $sho;
    //         //   return $ary = json_decode($sho);
    //         //   foreach($ary as $ar) {
    //             $keys[] = $sho;
    //         //   }
    //     //   }
    //   }
    //   return $keys;
       $shopid = Shop::where('id',$request->shopid)->get();
       $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                        'logOutputFile' => storage_path('logs/log.htm'),
                        'tempDir' => storage_path('logs/')
                    ])->loadView('frontend.allwarantyview', compact('shops','shopid'));
        return  $pdf->download('warranty_dataas.pdf');
    }

    public function show_warranty_request($id)
    {
        $warranty = Warranty::findOrFail($id);
        $shop = Shop::where('id', $warranty->shop_id)->first();
        $seller = Seller::where('id', $shop->user_id)->first();
        $user = User::where('id', $shop->user_id)->first();
        return view('frontend.all_warranty_view', compact('warranty', 'shop', 'seller', 'user'));
    }
    public function seller_questions_reply(Request $request)
    {
        $shop = Shop::find($request->shopid);
        $mykey = $request->valekey-1;
        $questions = array();
        if(!empty($shop->questions)){
            $questions = json_decode($shop->questions, true);
            foreach ($questions as $key => $question) {
                if($key == $mykey){
                     $questions[$key]['answer']  = $request->answer;
                }
            }
            $shop->questions = json_encode($questions);
        }
        if($shop->save()){
            flash(__('Your Answer has been updated successfully!'))->success();
            return back();
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }
    public function seller_questions_status(Request $request)
    {
        $shop = Shop::find($request->shopid);
        $mykey = $request->valekey;
        $questions = array();
        if(!empty($shop->questions)){
            $questions = json_decode($shop->questions, true);
            foreach ($questions as $key => $question) {
                if($key == $mykey){
                     $questions[$key]['status']  = $request->status;
                }
            }
            $shop->questions = json_encode($questions);
        }
        if($shop->save()){

            flash(__('Your Answer has been updated successfully!'))->success();
            return back();
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }
}
