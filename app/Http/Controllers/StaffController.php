<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Staff;
use App\Models\Seller;
use App\Models\User;
use App\Models\Country;
use App\Models\Shop;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffs = Staff::all();
        return view('staffs.index', compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
		$countries = Country::all();
        return view('staffs.create', compact('roles','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check_user = User::where('email', $request->email)->count();
        if($check_user > 0){
            flash(__('Sorry! email already registerd.'))->error();
            return back();
        }else{
            $user = new User;
            $user->name = $request->name;
            $user->gender = $request->gender;
            $user->cnic = $request->cnic;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->country = $request->country;
            $user->city = $request->city;
            $user->user_type = "staff";
            if($request->role_id == 3){
                $user->ref_code = Hash::make($user->email);
            }
            $user->password = Hash::make($request->password);
            if($user->save()){
                $staff = new Staff;
                $staff->user_id = $user->id;
                $staff->role_id = $request->role_id;
                if($staff->save()){
                    flash(__('Staff has been inserted successfully'))->success();
                    return redirect()->route('staffs.index');
                }
            }

            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staff = Staff::findOrFail(decrypt($id));
        $roles = Role::all();
		$countries = Country::all();
        return view('staffs.edit', compact('staff', 'roles' ,'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $staff = Staff::findOrFail($id);
        $user = $staff->user;
        $user->name = $request->name;
        $user->gender = $request->gender;
		$user->cnic = $request->cnic;
		$user->phone = $request->phone;
		$user->email = $request->email;
		$user->country = $request->country;
		$user->city = $request->city;
        if(strlen($request->password) > 0){
            $user->password = Hash::make($request->password);
        }
        if($user->save()){
            if($staff->save()){
                flash(__('Staff has been updated successfully'))->success();
                return redirect()->route('staffs.index');
            }
        }

        flash(__('Something went wrong'))->error();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy(Staff::findOrFail($id)->user->id);
        if(Staff::destroy($id)){
            flash(__('Staff has been deleted successfully'))->success();
            return redirect()->route('staffs.index');
        }

        flash(__('Something went wrong'))->error();
        return back();
    }
}
