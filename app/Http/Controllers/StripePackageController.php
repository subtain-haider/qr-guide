<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe;
// use App\Models\Order;
use App\Models\BusinessSetting;
use App\Models\Seller;
use Session;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\CommissionController;
use App\Http\Controllers\WalletController;
use Redirect;
use Auth;
use Carbon\Carbon;

class StripePackageController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {
        if(Session::has('payment_type')){
            if(Session::get('payment_type') == 'cart_payment' || Session::get('payment_type') == 'wallet_payment'){
                return view('frontend.payment.packagestripe');
            }
            elseif (Session::get('payment_type') == 'seller_payment') {
                $seller = Seller::findOrFail(Session::get('payment_data')['seller_id']);
                return view('stripes.stripe', compact('seller'));
            }
        }
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
     function stripePost(Request $request)
    {
        if($request->session()->has('payment_type')){
        if($request->session()->get('payment_type') == 'cart_payment'){
        $pak_plan = Session()->get('pak_plan');
        $pricing_plan = \DB::table('pricing_plans')->where('id', $pak_plan)->first();
        $name = $pricing_plan->name;
        $price = $pricing_plan->per_month;
        $admin = \DB::table('users')->where('user_type', 'admin')->first();
        $admin_stripe_c_id = $admin->stripe_c_id;
        $admin_stripe_s_id = $admin->stripe_s_id;
        Stripe\Stripe::setApiKey($admin_stripe_s_id);
        $payment = Stripe\Charge::create ([
                        "amount" => $price * 100,
                        "currency" => "usd",
                        "source" => $request->stripeToken
                ]);
                if($payment->status)
                {
                    
                    $user_id = Session()->get('new_user_id');
                    if($user_id == ''){
                        $user_id = Auth::user()->id;
                    }
                    // dd($user_id);
                    $pak_plan = Session()->get('pak_plan');
                    $pricing_plan = \DB::table('pricing_plans')->where('id', $pak_plan)->first();
                    $user = \App\Models\User::where('id',$user_id)->first();
                    $user->package_plane = $pricing_plan->name;
                    $user->package_for  = $pricing_plan->permissions;
                    $user->package  = $pricing_plan->id;
                    $user->user_type  = (Auth::user()->user_type == 'customer') ? 'seller' : 'customer';
                    $user->package_start   = Carbon::now()->toDateTimeString();
                    $user->package_end = $today = Carbon::now()->addMonth(1)->toDateTimeString();;
                    $user->save();
                    $to_name = $user->name;
                    $to_email = $user->email;
                    $data = array('name'=>$user->name, "body" => "Your package is confirmed");
                    \Mail::send("emails.packagemail", $data, function($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                    ->subject("Package confirm email.");
                    $message->from("zain.ulabidin7501@gmail.com","Package email.");
                    });
                    flash(__('Payment is successfully done'))->success();
                    return Redirect::route('pricing');
                }
            
            
        
            }
           
        }
        
    
}
}
