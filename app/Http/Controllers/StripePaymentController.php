<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe;
use App\Models\Order;
use App\Models\BusinessSetting;
use App\Models\Seller;
use Session;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\CommissionController;
use App\Http\Controllers\WalletController;
use Auth;

class StripePaymentController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {
       
        if(Session::has('payment_type')){
            if(Session::get('payment_type') == 'cart_payment' || Session::get('payment_type') == 'wallet_payment'){
                return view('frontend.payment.stripe');
            }
            elseif (Session::get('payment_type') == 'seller_payment') {
                $seller = Seller::findOrFail(Session::get('payment_data')['seller_id']);
                return view('stripes.stripe', compact('seller'));
            }
        }
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
     function stripePost(Request $request)
    {
        //  dd($request->url());
        //   <?php
$server_name = explode('.', $_SERVER['SERVER_NAME']);
$mini_site_slug = $server_name[0];
// dd($mini_site_slug);
        // dd($cart_detail = Session::get('cart'));
        if($request->session()->has('payment_type')){
            
            if($request->session()->get('payment_type') == 'cart_payment'){
                $order = Order::findOrFail(Session::get('order_id'));
               
                $product = \App\Models\Product::where('id',Session::get('cart')[0]['id'])->first();
                $user = \App\Models\User::where('id',$product->user_id)->first();
                $stripe_s_id = $user->stripe_s_id;
                if(Auth::check())
                {
                $shop = \App\Models\Shop::where('user_id',$order->user_id)->first();
                $orderUpdate = Order::findOrFail($order->id);
                $orderUpdate->shop_id = $shop->id;
                $product_array = array();
                foreach($cart_detail = Session::get('cart') as $product)
                {
                    // echo $product['id'];
                    $product_array[] = json_encode($product['id']);
                }
                $orderUpdate->product_id = $product_array;
                $orderUpdate->save();
                }
              else
              {
                $shop = \App\Models\Shop::where('slug',$mini_site_slug)->first();
                // dd($shop);
                $orderUpdate = Order::findOrFail($order->id);
                $orderUpdate->shop_id = $shop->id;
                $product_array = array();
                foreach($cart_detail = Session::get('cart') as $product)
                {
                    // echo $product['id'];
                    $product_array[] = json_encode($product['id']);
                }
                $orderUpdate->product_id = $product_array;
                $orderUpdate->save();
              }
               
                Stripe\Stripe::setApiKey($stripe_s_id);

                $payment = json_encode(Stripe\Charge::create ([
                        "amount" => $order->grand_total * 100,
                        "currency" => "usd",
                        "source" => $request->stripeToken
                ]));
            $shipping = json_decode($order->shipping_address);
            $to_name = $shipping->name;
            $to_email = $shipping->email;
            $data = array('name'=>"Ogbonna Vitalis('sender_name')", "body" => "A test mail");
            \Mail::send("emails.ordermail", $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
            ->subject("Order Confirmation mail");
            $message->from("zain.ulabidin7501@gmail.com","Order Confirmation mail");
            });
            
                $checkoutController = new CheckoutController;
                return $checkoutController->checkout_done($request->session()->get('order_id'), $payment);
            }
            elseif ($request->session()->get('payment_type') == 'seller_payment') {
                $seller = Seller::findOrFail($request->session()->get('payment_data')['seller_id']);
                dd($seller);
                Stripe\Stripe::setApiKey($seller->stripe_secret);

                $payment = json_encode(Stripe\Charge::create ([
                        "amount" => round(convert_to_usd($request->session()->get('payment_data')['amount']) * 100),
                        "currency" => "usd",
                        "source" => $request->stripeToken
                ]));

                $commissionController = new CommissionController;
                return $commissionController->seller_payment_done($request->session()->get('payment_data'), $payment);
            }
            elseif ($request->session()->get('payment_type') == 'wallet_payment') {

                Stripe\Stripe::setApiKey($stripe_s_id);

                $payment = json_encode(Stripe\Charge::create ([
                        "amount" => round(convert_to_usd($request->session()->get('payment_data')['amount']) * 100),
                        "currency" => "usd",
                        "source" => $request->stripeToken
                ]));

                $walletController = new WalletController;
                return $walletController->wallet_payment_done($request->session()->get('payment_data'), $payment);
            }
        }
    }
}
