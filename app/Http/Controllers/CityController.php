<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;

class CityController extends Controller
{

    public function city(Request $request)
    {
        $cities = City::all();
        //$active_countries = Country::where('code', 'AF')->get();
        //return view('business_settings.country', compact('countries', 'active_currencies'));
		return view('business_settings.city', compact('cities'));
    }

    public function cities(Request $request)
    {
		$CitiesArray = array();
		$Cities = Cities::findOrFail($request->id);
		return json_encode(array('options'=>$CitiesArray));
    }
}
