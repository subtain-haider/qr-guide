<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Product;
use App\Models\User;
use App\Models\Shop;
use Auth;
use DB;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::where('parent_id', null)->get();
        return view('frontend.seller.questions', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $question = Question::where('id', $id)->where('parent_id', null)->first();
        if($question != null){
            return view('frontend.seller.answer', compact('question'));
        }
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $question_details = Question::where('id', $request->question)->where('parent_id', null)->first();
        $question = new Question;
        $question->product_id = $question_details->product_id;
        $question->user_id = Auth::user()->id;
        $question->shop_id = $question_details->shop_id;
        $question->parent_id = $request->question;
        $question->comment = $request->details;
        if($question->save()){
            flash('Answer has been submitted successfully')->success();
            return back();
        }
        flash('Something went wrong')->error();
        return back();
    }

    public function store_question(Request $request)
    {
        $product_details = Product::where('id', $request->product_id)->first();
        $question = new Question;
        $question->product_id = $product_details->id;
        $question->user_id = Auth::user()->id;
        $question->shop_id = Shop::where('user_id', $product_details->user_id)->first()->id;
        $question->comment = $request->question;
        if($question->save()){
            flash('Question has been submitted successfully')->success();
            return back();
        }
        flash('Something went wrong')->error();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
