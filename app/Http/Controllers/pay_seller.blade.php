<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Seller Payment')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{route('sellers.payment_by_order')}}" method="POST" enctype="multipart/form-data">
        	@csrf
			<input type="hidden" id="order_id" name="order_id" value="{{$order->id}}" required readonly>
			<input type="hidden" id="seller_id" name="seller_id" value="{{$seller_id}}" required readonly>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="name">{{__('Order Code')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Order Code')}}" id="order" name="order" class="form-control" value="{{$order->code}}" required readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="email">{{__('Total Amount')}}</label>
                    <div class="col-sm-9">
                        <input type="number" placeholder="{{__('Total Amount')}}" id="total" name="total" class="form-control" value="{{ $order->orderDetails->where('seller_id', $seller_id)->sum('price') }}" required readonly>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-3 control-label" for="email">{{__('Pay Amount')}}</label>
                    <div class="col-sm-9">
                        <input type="number" placeholder="{{__('Pay Amount')}}" id="pay_amount" name="pay_amount" class="form-control" value="{{ $order->orderDetails->where('seller_id', $seller_id)->sum('price') }}" required readonly>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-3 control-label payment_type" for="payment_type">{{__('Payment Type')}}</label>
					<select class="form-control selectpicker form-control-sm" name="payment_type" data-minimum-results-for-search="Infinity" id="update_delivery_status" required>
						<option>{{__('Please Select')}}</option>
						
					</select>
				</div>
				<div class="form-group payment_details_block hide">
                    <label class="col-sm-3 control-label" for="name">{{__('Name')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Name')}}" id="transaction_name" name="transaction_name" class="form-control" required>
                    </div>
                </div>
				<div class="form-group payment_details_block hide">
                    <label class="col-sm-3 control-label" for="transaction_no">{{__('Transaction No.')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Transaction No.')}}" id="transaction_no" name="transaction_no" class="form-control" required>
                    </div>
                </div>
				<div class="form-group payment_details_block hide">
                    <label class="col-sm-3 control-label" for="location">{{__('Branch / Location')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Branch / Location')}}" id="location" name="location" class="form-control" required>
                    </div>
                </div>
				<div class="form-group payment_details_block hide">
                    <label class="col-sm-3 control-label" for="amount">{{__('Amount')}}</label>
                    <div class="col-sm-9">
                        <input type="number" placeholder="{{__('Amount')}}" id="amount" name="amount" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>