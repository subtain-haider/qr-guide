<?php

namespace App\Http\Controllers;

use App\Models\Woocommerce;
use Automattic\WooCommerce\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Review as RV;
use Codexshaper\WooCommerce\Facades\Review;

class WoocommerceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $user = Auth::user();
        if($user->user_type == 'seller'){

            $woocomerces = Woocommerce::where('user_id',Auth::user()->id)->get();
            return view('frontend.seller.woocommerce',compact('woocomerces'));
        }
        else {
            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user_id = Auth::user()->id;
        $data = Woocommerce::create(['user_id'=>$user_id,'url' => $request->url, 'consumer_key' => $request->consumer_key, 'consumer_secret' => $request->consumer_secret]);
        // $data = $user->woocomerce()->create(['url' => $request->url, 'consumer_key' => $request->consumer_key, 'consumer_secret' => $request->consumer_secret]);
        if($data){

            flash('WooCommerce store added successfully')->success();

            Artisan::call('view:clear');
            Artisan::call('cache:clear');

            return back();
        }
        else{
            flash('Something went wrong')->error();
            return redirect("woocommerce.index");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Woocommerce  $woocommerce
     * @return \Illuminate\Http\Response
     */
    public function show(Woocommerce $woocommerce)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Woocommerce  $woocommerce
     * @return \Illuminate\Http\Response
     */
    public function edit(Woocommerce $woocommerce)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Woocommerce  $woocommerce
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Woocommerce $woocommerce)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Woocommerce  $woocommerce
     * @return \Illuminate\Http\Response
     */
    public function destroy(Woocommerce $woocommerce)
    {
        //
    }
       public function woocommerceProductsSync($id){
        
        $user_id = Auth::user()->id;
        \App\Models\Product::where([['user_id','=',$user_id],['product_type','=','woocommerce_product']])->delete();
        \App\Models\Review::where('woo_id','!=',null)->delete();
        $woocommerce_datas = Woocommerce::where('user_id',$user_id)->get();
        foreach($woocommerce_datas as $woocommerce_data)
        {
        $woocommerce = new Client(
            trim($woocommerce_data->url), // Your store URL
            trim($woocommerce_data->consumer_key), // Your consumer key
            trim($woocommerce_data->consumer_secret), // Your consumer secret
            [
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3' // WooCommerce WP REST API version
            ]
        );
        $products =$woocommerce->get('products',array('per_page' => 100));
        if(count($products) > 0){
            // $i=0;
            foreach ($products as $product){
                $images = $product->images;
                $tags = '';
                foreach ($product->tags as $tag){
                    $tags = $tag->name .', ';
                }
                $image = "[".json_encode($images[0]->src)."]";
                $emptyarray = "["."]";
                \App\Models\Product::create(['category_name'=>'undefined','subcategory_name'=>'undefined','subsubcategory_name'=>'undefined','unit_price' => $product->price,'category_id'=>'34','subcategory_id'=>"104",'subsubcategory_id'=>'147','photos'=> $image,'thumbnail_img'=>$images[0]->src, 'name' => $product->name, 'description' => $product->description, 'product_type' => "woocommerce_product",'slug' => $product->slug,'user_id' =>$user_id,'choice_options' => $emptyarray,'woo_id' => $woocommerce_data->url.$product->id]);
            }
        }
        $reviews =$woocommerce->get('products/reviews',array('per_page' => 100));
        foreach($reviews as $review)
        {
        $updateProduct = \DB::table('reviews')->Insert(['woo_id' => $woocommerce_data->url.$product_id,'rating' => $review->rating,'comment' => $review->review]);
        $product_datas = \App\Models\Product::where('woo_id',$woocommerce_data->url.$product_id)->get();
        foreach($product_datas as $product_data)
        {
            $review_product_id = \App\Models\Review::where('woo_id',$woocommerce_data->url.$product_id)->first();
            $review_product_id->product_id = $product_data->id;
            $review_product_id->save();
        }
        }
        
        }
        flash('All Products Synced from this WooCommerce store successfully')->success();

        Artisan::call('view:clear');
        Artisan::call('cache:clear');

        if(Auth::user()->user_type == 'admin'){
            return redirect('seller/products');
        }
        else{
            return redirect('seller/products');
        }
    }

}
