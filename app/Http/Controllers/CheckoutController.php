<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Category;
use App\Http\Controllers\PaypalController;
use App\Http\Controllers\PayumoneyController;
use App\Http\Controllers\StripePaymentController;
use App\Http\Controllers\PublicSslCommerzPaymentController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PricingController;
use App\Models\Order;
use App\Models\BusinessSetting;
use App\Models\Coupon;
use App\Models\CouponUsage;
use App\Models\User;
use App\Models\Seller;
use App\Models\Shop;
use Session;
use Hash;

class CheckoutController extends Controller
{

    public function __construct()
    {
        //
    }

    //check the selected payment gateway and redirect to that controller accordingly
    public function checkout(Request $request)
    {
    //   dd($request->all());
        if(isset($request->plan)){    
            $user = null;
            if(!Auth::check()){
                if(User::where('email', $request->email)->first() != null){
                    flash(__('Email already exists!'))->error();
                    return back();
                }
                if($request->password == $request->password_confirmation){
                    $user = new User;
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->whatsapp_number = $request->whatsapp_number;
                    $user->calling_code = $request->calling_code;
                    $user->user_type = "seller";
                    $user->password = Hash::make($request->password);
                    // dd($user);
                    if(isset($request->ref)){
                        $user->referral = $request->ref;
                    }
                    $user->save();
                    if(BusinessSetting::where('type', 'email_verification')->first()->value != 1){
                        $user->email_verified_at = date('Y-m-d H:m:s');
                        $user->save();
                        // dd("ok");
                    }
                    // dd(Seller::where('user_id', $user->id)->first());
                    if(Seller::where('user_id', $user->id)->first() === null){
                        $seller = new Seller;
                        $seller->user_id = $user->id;

                        if($seller->save()){
                            $shop = new Shop;
                            $shop->user_id = $user->id;
                            $shop->name = $request->name;
                            $shop->address = $request->address;
                            $shop->slug = preg_replace('/\s+/', '-', $request->name).'-'.$shop->id;
                            $shop->package = $request->plan;
                            $PackageInfo = \App\Models\PricingPlan::where('id', $request->plan)->first();
                            $shop->package_plane = $PackageInfo->permissions;
                            $shop->package_for = $request->plan_for;
                            $shop->package_start = date('Y-m-d');
                            if($request->plan_for == 'month'){
                                $shop->package_end = date("Y-m-d",strtotime("1 month"));
                            }
                            if($request->plan_for == 'year'){
                                $shop->package_year = date("Y-m-d",strtotime("1 month"));
                            }
                            if($shop->save()){
                                $request->session()->put('new_shop_id', $seller->id);
                                $request->session()->put('new_user_id', $user->id);
                                $request->session()->get('new_shop_id');
                                if($request->plan_for == 'month'){
                                    $request->session()->put('plan_price', $seller->per_month);
                                }
                                if($request->plan_for == 'year'){
                                    $request->session()->put('plan_price', $seller->per_year);
                                }
                            }
                        }
                       
                    }
                   
                }
                else{
                    flash(__('Sorry! Password did not match.'))->error();
                    return back();
                }
            }
            else {
                if(Seller::where('user_id', Auth::user()->id)->first() === null){
                        $seller = new Seller;
                        $seller->user_id = Auth::user()->id;

                        if($seller->save()){
                            $shop = new Shop;
                            $shop->user_id = Auth::user()->id;
                            $shop->name = Auth::user()->name;
                            $shop->address = Auth::user()->address;
                            $shop->slug = preg_replace('/\s+/', '-', Auth::user()->name).'-'.$shop->id;
                            $shop->package = $request->plan;
                            $PackageInfo = \App\Models\PricingPlan::where('id', $request->plan)->first();
                            $shop->package_plane = $PackageInfo->permissions;
                            $shop->package_for = $request->plan_for;
                            $shop->package_start = date('Y-m-d');
                            if($request->plan_for == 'month'){
                                $shop->package_end = date("Y-m-d",strtotime("1 month"));
                            }
                            if($request->plan_for == 'year'){
                                $shop->package_year = date("Y-m-d",strtotime("1 month"));
                            }
                            if($shop->save()){
                                $request->session()->put('new_shop_id', $seller->id);
                                $request->session()->put('new_user_id', Auth::user()->id);
                                $request->session()->get('new_shop_id');
                                if($request->plan_for == 'month'){
                                    $request->session()->put('plan_price', $seller->per_month);
                                }
                                if($request->plan_for == 'year'){
                                    $request->session()->put('plan_price', $seller->per_year);
                                }
                            }
                        }
                       
                    }
            }
        }else{
            $orderController = new OrderController;
            $orderController->store($request);
        }

        $request->session()->put('payment_type', 'cart_payment');

        if($request->session()->get('order_id') != null || $request->session()->get('new_shop_id') != null){
            if($request->payment_option == 'paypal'){
                $paypal = new PaypalController;
                
               
                if(isset($request->plan)){ 
                     Session::put("pak_plan",$request->plan);
                    return redirect()->route('package.payment');
                }
                return redirect('payment');
            }
            elseif ($request->payment_option == 'stripe') {
                
                 if(isset($request->plan)){ 
                     Session::put("pak_plan",$request->plan);
                    return redirect()->route('packagestripe.post');
                }
                $stripe = new StripePaymentController;
                return $stripe->stripe();
            }
            elseif ($request->payment_option == 'sslcommerz') {
                $sslcommerz = new PublicSslCommerzPaymentController;
                return $sslcommerz->index($request);
            }
            elseif ($request->payment_option == 'payumoney') {
                $payumoney = new PayumoneyController;
                return $payumoney->index($request);
            }
            elseif ($request->payment_option == 'cash_on_delivery' || $request->payment_option == 'bank_transfer') {
                $order = Order::findOrFail($request->session()->get('order_id'));
                $commission_percentage = BusinessSetting::where('type', 'vendor_commission')->first()->value;
                foreach ($order->orderDetails as $key => $orderDetail) {
                    if($orderDetail->product->user->user_type == 'seller'){
                        $seller = $orderDetail->product->user->seller;
                        $seller->admin_to_pay = $seller->admin_to_pay - ($orderDetail->price*$commission_percentage)/100;
                        $seller->save();
                    }
                }

                $request->session()->put('cart', collect([]));
                $request->session()->forget('order_id');

                flash("Your order has been placed successfully")->success();
            	return redirect()->route('home');
            }
            elseif ($request->payment_option == 'wallet') {
                $user = Auth::user();
                $user->balance -= Order::findOrFail($request->session()->get('order_id'))->grand_total;
                $user->save();
                return $this->checkout_done($request->session()->get('order_id'), null);
            }
        }
    }

    //redirects to this method after a successfull checkout
    public function checkout_done($order_id, $payment)
    {
        if(Session::has('order_id')){
            $order = Order::findOrFail($order_id);
            $order->payment_status = 'paid';
            $order->payment_details = $payment;
            $order->save();

            $commission_percentage = BusinessSetting::where('type', 'vendor_commission')->first()->value;
            foreach ($order->orderDetails as $key => $orderDetail) {
                if($orderDetail->product->user->user_type == 'seller'){
                    $seller = $orderDetail->product->user->seller;
                    $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price*(100-$commission_percentage))/100;
                    $seller->save();
                }
            }

            Session::put('cart', collect([]));
            Session::forget('order_id');
            Session::forget('payment_type');
            Session::forget('shipping_method');

            // flash(__('Payment completed'))->success();
            // return redirect()->route('home');
            return view('frontend.thankyou');
        }else{
            return redirect()->route('home');
        }

        // return redirect()->route('thankyou');
    }

    public function get_shipping_info(Request $request)
    {
        $categories = Category::all();
        return view('frontend.shipping_info', compact('categories'));
    }

    public function store_shipping_info(Request $request)
    {
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['address'] = $request->address;
        $data['country'] = $request->country;
        $data['city'] = $request->city;
        $data['postal_code'] = $request->postal_code;
        $data['phone'] = $request->phone;
        $data['checkout_type'] = $request->checkout_type;

        foreach (\App\Models\BusinessSetting::where('type', $request->checkout_type)->get() as $ShippingMethods){
            $ShippingMethod = json_decode($ShippingMethods->value);
            if($ShippingMethod->status == 'on'){
                $shipping = array('type'=>$request->checkout_type, 'cost'=>$ShippingMethod->cost);
                $request->session()->put('shipping_method', $shipping);
            }
        }

        $shipping_info = $data;
        $request->session()->put('shipping_info', $shipping_info);

        $subtotal = 0;
        $tax = 0;
        $shipping = 0;
        foreach (Session::get('cart') as $key => $cartItem){
            $subtotal += $cartItem['price']*$cartItem['quantity'];
            // dd($cartItem);
            $tax += $cartItem['tax']*$cartItem['quantity'];
            $shipping += $cartItem['shipping']*$cartItem['quantity'];
        }

        $total = $subtotal + $tax + $shipping;

        if(Session::has('coupon_discount')){
                $total -= Session::get('coupon_discount');
        }

        return view('frontend.payment_select', compact('total'));
    }

    public function get_payment_info(Request $request)
    {
        $subtotal = 0;
        $tax = 0;
        $shipping = 0;
        foreach (Session::get('cart') as $key => $cartItem){
            $subtotal += $cartItem['price']*$cartItem['quantity'];
            $tax += $cartItem['tax']*$cartItem['quantity'];
            $shipping += $cartItem['shipping']*$cartItem['quantity'];
        }

        $total = $subtotal + $tax + $shipping;

        if(Session::has('coupon_discount')){
                $total -= Session::get('coupon_discount');
        }

        return view('frontend.payment_select', compact('total'));
    }
    public function apply_coupon(Request $request){
        // dd($request->all());
        // return $request->coupon_code;
        $coupon = Coupon::where('code', $request->coupon_code)->first();
        
        if($coupon != null && strtotime(date('d-m-Y')) >= $coupon->start_date && strtotime(date('d-m-Y')) <= $coupon->end_date && CouponUsage::where('user_id', Auth::user()->id)->where('coupon_id', $coupon->id)->first() == null){
            $coupon_details = json_decode($coupon->details);
            
            if ($coupon->type == 'cart_base')
            {
                $subtotal = 0;
                $tax = 0;
                $shipping = 0;
                foreach (Session::get('cart') as $key => $cartItem)
                {
                    $subtotal += $cartItem['price']*$cartItem['quantity'];
                    $tax += $cartItem['tax']*$cartItem['quantity'];
                    $shipping += $cartItem['shipping']*$cartItem['quantity'];
                }
               $sum = $subtotal+$tax+$shipping;
                
                if ($sum > $coupon_details->min_buy) {
                    if ($coupon->discount_type == 'percent') {
                        $coupon_discount =  ($sum * $coupon->discount)/100;
                        if ($coupon_discount > $coupon_details->max_discount) {
                            $coupon_discount = $coupon_details->max_discount;
                        }
                    }
                    elseif ($coupon->discount_type == 'amount') {
                        $coupon_discount = $coupon->discount;
                    }

                    if(!Session::has('coupon_discount')){
                        
                        $request->session()->put('coupon_id', $coupon->id);
                        $request->session()->put('coupon_discount', $coupon_discount);
                        
                        flash('Coupon has been applied')->success();
                    }
                    else{
                        flash('Coupon is already applied')->warning();
                    }
                }
            }
            elseif ($coupon->type == 'product_base')
            {
                $coupon_discount = 0;
                foreach (Session::get('cart') as $key => $cartItem){
                    foreach ($coupon_details as $key => $coupon_detail) {
                        if($coupon_detail->product_id == $cartItem['id']){
                            if ($coupon->discount_type == 'percent') {
                                $coupon_discount += $cartItem['price']*$coupon->discount/100;
                            }
                            elseif ($coupon->discount_type == 'amount') {
                                $coupon_discount += $coupon->discount;
                            }
                        }
                    }
                }
                if(!Session::has('coupon_discount')){
                    $request->session()->put('coupon_id', $coupon->id);
                    $request->session()->put('coupon_discount', $coupon_discount);
                    flash('Coupon has been applied')->success();
                }
                else{
                    flash('Coupon is already applied')->warning();
                }
            }
        }
        else {
            flash('No Coupon Exixts')->warning();
        }
        return back();
    }
    public function apply_coupon_code(Request $request){
        // dd($request->all());
        $coupon = Coupon::where('code', $request->code)->first();

        if($coupon != null && strtotime(date('d-m-Y')) >= $coupon->start_date && strtotime(date('d-m-Y')) <= $coupon->end_date && CouponUsage::where('user_id', Auth::user()->id)->where('coupon_id', $coupon->id)->first() == null){
            $coupon_details = json_decode($coupon->details);

            if ($coupon->type == 'cart_base')
            {
                $subtotal = 0;
                $tax = 0;
                $shipping = 0;
                foreach (Session::get('cart') as $key => $cartItem)
                {
                    $subtotal += $cartItem['price']*$cartItem['quantity'];
                    $tax += $cartItem['tax']*$cartItem['quantity'];
                    $shipping += $cartItem['shipping']*$cartItem['quantity'];
                }
                $sum = $subtotal+$tax+$shipping;

                if ($sum > $coupon_details->min_buy) {
                    if ($coupon->discount_type == 'percent') {
                        $coupon_discount =  ($sum * $coupon->discount)/100;
                        if ($coupon_discount > $coupon_details->max_discount) {
                            $coupon_discount = $coupon_details->max_discount;
                        }
                    }
                    elseif ($coupon->discount_type == 'amount') {
                        $coupon_discount = $coupon->discount;
                    }

                    if(!Session::has('coupon_discount')){
                        $request->session()->put('coupon_id', $coupon->id);
                        $request->session()->put('coupon_discount', $coupon_discount);
                        flash('Coupon has been applied')->success();
                    }
                    else{
                        flash('Coupon is already applied')->warning();
                    }
                }
            }
            elseif ($coupon->type == 'product_base')
            {
                $coupon_discount = 0;
                foreach (Session::get('cart') as $key => $cartItem){
                    foreach ($coupon_details as $key => $coupon_detail) {
                        if($coupon_detail->product_id == $cartItem['id']){
                            if ($coupon->discount_type == 'percent') {
                                $coupon_discount += $cartItem['price']*$coupon->discount/100;
                            }
                            elseif ($coupon->discount_type == 'amount') {
                                $coupon_discount += $coupon->discount;
                            }
                        }
                    }
                }
                if(!Session::has('coupon_discount')){
                    $request->session()->put('coupon_id', $coupon->id);
                    $request->session()->put('coupon_discount', $coupon_discount);
                    flash('Coupon has been applied')->success();
                }
                else{
                    flash('Coupon is already applied')->warning();
                }
            }
        }
        else {
            flash('No Coupon Exixts')->warning();
        }
        return back();
    }
}
