<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserRegister2 extends Controller
{

    public function index()
    {
        return view('support.register2');
    }
    public function register2(Request $request)
    {
  
        $data = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'slug' => $request['vid'],
            'user_type' => 'support',
            'password' => Hash::make($request['password']),
            'email_verified_at' => date('Y-m-d H:m:s'),
        ]);
        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
            return redirect('inboxseller');
        }
    }
}
