<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;use App\Models\Seller;
use App\Models\Product;
use App\Models\Category;
use App\Models\Units;

use App\Models\Language;

use Auth;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use App\Models\Guiderequest;
use App\Models\Requestus;
use Session;
use ImageOptimizer;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function admin_products()
    {
        $type = 'In House';
        $products = Product::where('added_by', 'admin')->orderBy('created_at', 'desc')->get();
        return view('products.index', compact('products','type'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function seller_products()
    {
        $type = 'Seller';
        $user = auth()->user()->staff->role;
        if($user->name == 'Advertiser'){
            $user = auth()->user();
            $sellers = Seller::where('referral',
                $user->ref_code)->orderBy('created_at', 'desc')->get();
            $products = Product::where('added_by', 'seller')->orderBy('created_at', 'desc')->get();
        return view('products.seller_products', compact('sellers','products','type'));
        }else{
            $products = Product::where('added_by', 'seller')->orderBy('created_at', 'desc')->get();
        return view('products.index', compact('products','type'));		
            
        }			
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        $categories = Category::all();
        return view('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

   
 

public function requestus(Request $request)
    {
  
        $plan = new Requestus;
        $plan->notify = $request->notify;
        $plan->username = $request->username;
        $plan->userid = $request->userid;
        $plan->useremail = $request->useremail;
        $plan->save();
        return response()->json('getting reponse from ajax');
       

    }
public function guiderequest(Request $request)
    {
  
      
        $plan = new Guiderequest;
        $plan->notify = $request->notify;
        $plan->username = $request->username;
        $plan->userid = $request->userid;
        $plan->useremail = $request->useremail;
        $plan->save();
         return response()->json('getting reponse from ajax');
       

    }


    public function store(Request $request)
    {
        //dd($request->all());
        $product = new Product;
        $product->name = str_replace(array('\'', '"'), '', $request->name);
        $product->sku = $request->sku;
        $product->asin = $request->asin;
        $product->added_by = $request->added_by;
        $product->user_id = Auth::user()->id;
        $product->category_id = $request->category_id;
        $product->subcategory_id = $request->subcategory_id;
        $product->subsubcategory_id = $request->subsubcategory_id;
        $product->category_name = Category::where('id', $request->category_id)->first()->name;
        $product->subcategory_name = SubCategory::where('id', $request->subcategory_id)->first()->name;
        $product->subsubcategory_name = SubSubCategory::where('id', $request->subsubcategory_id)->first()->name;
        $product->brand_id = $request->brand_id;
        $product->product_type = $request->product_type ? $request->product_type : 'product';
        $product->external_website = $request->external_website;
        $product->external_link = $request->external_link;
       

         $product->quantity = $request->quantity;
        $size = array();
         $product->size = json_encode($request->size);
        $photos = array();



        // if($request->hasFile('photos')){
        //     if($user->image != null)
        //     {
        //         $image_path = public_path('storage/users/').$user->image;
        //         unlink($image_path);
        //     }
        //         $name = time() .'.'. $request->image->extension();
        //         $store = $request->image->storeAs('public/users/',$name);
        //         $user->image=$name;
        //         // $user->save();
        // }
            // dd($request->hasFile('photos'));
        
        
        if($request->hasFile('photos')){
            foreach ($request->photos as $key => $photo) {
                $imageName = uniqid().time().'.'.$photo->extension();
                $store = $photo->storeAs('public/products/photos',$imageName);
                $path  = "storage/products/photos/".$imageName;
                array_push($photos, $path);
            }
            $product->photos = json_encode($photos);
        }

        if($request->hasFile('thumbnail_img')){
            $imageName = time().'.'.$request->thumbnail_img->extension();
            $store = $request->thumbnail_img->storeAs('public/products/thumbnail',$imageName);
            $path  = "storage/products/thumbnail/".$imageName;
            $product->thumbnail_img = $path;
            //ImageOptimizer::optimize(base_path('public/').$product->thumbnail_img);
        }

        if($request->hasFile('featured_img')){
            $product->featured_img = $request->featured_img->store('uploads/products/featured');
            //ImageOptimizer::optimize(base_path('public/').$product->featured_img);
        }

        if($request->hasFile('flash_deal_img')){
            $product->flash_deal_img = $request->flash_deal_img->store('uploads/products/flash_deal');
            //ImageOptimizer::optimize(base_path('public/').$product->flash_deal_img);
        }

        $product->unit = $request->unit;
        $product->tags = implode('|',$request->tags);

        $product->description = $request->description;
        $product->spacification = $request->spacification;
        if(isset($request->video_link)){
            $product->video_provider = $request->video_provider;
            $product->video_link = $request->video_link;
        }
        $product->unit_price = $request->unit_price;
        $product->purchase_price = $request->purchase_price;
        // $product->currency = $request->currency;
        $product->tax = $request->tax;
        $product->tax_type = $request->tax_type;
        $product->discount = $request->discount;
        $product->discount_type = $request->discount_type;
        $product->shipping_type = $request->shipping_type;
        if($request->shipping_type == 'free'){
            $product->shipping_cost = 0;
        }
        elseif ($request->shipping_type == 'local_pickup') {
            $product->shipping_cost = $request->local_pickup_shipping_cost;
        }
        elseif ($request->shipping_type == 'flat_rate') {
            $product->shipping_cost = $request->flat_shipping_cost;
        }
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;

        if($request->hasFile('meta_img')){
            $product->meta_img = $request->meta_img->store('uploads/products/meta');
            //ImageOptimizer::optimize(base_path('public/').$product->meta_img);
        }

        // if($request->hasFile('pdf')){
        //     $product->pdf = $request->pdf->store('uploads/products/pdf');
        // }
        $pdfs = array();
        if($request->hasFile('pdf')){
            foreach ($request->pdf as $key => $pdf) {
                $path = $pdf->store('uploads/products/pdf');
                $pdf_details = array('path'=>$path, 'lng'=>$request->lng[$key]);
                array_push($pdfs, $pdf_details);
                //ImageOptimizer::optimize(base_path('public/').$path);
            }
            $product->pdf = json_encode($pdfs);
        }

        $product->slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)).'-'.str::random(5);

        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $product->colors = json_encode($request->colors);
        }
        else {
            //$colors = array();
            //$product->colors = json_encode($colors);
             $colors = array();
        $product->colors = json_encode($request->colors);
        }

        $choice_options = array();

        if($request->has('choice')){
            foreach ($request->choice_no as $key => $no) {
                $str = 'choice_options_'.$no;
                $item['name'] = 'choice_'.$no;
                $item['title'] = $request->choice[$key];
                $item['options'] = explode(',', implode('|', $request[$str]));
                array_push($choice_options, $item);
            }
        }

        $product->choice_options = json_encode($choice_options);

        $variations = array();

        //combinations start
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $my_str = implode('|',$request[$name]);
                array_push($options, explode(',', $my_str));
            }
        }

        //Generates the combinations of customer choice options
        $combinations = combinations($options);
        if(count($combinations[0]) > 0){
            foreach ($combinations as $key => $combination){
                $str = '';
                foreach ($combination as $key => $item){
                    if($key > 0 ){
                        $str .= '-'.str_replace(' ', '', $item);
                    }
                    else{
                        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
                            $color_name = \App\Models\Color::where('code', $item)->first()->name;
                            $str .= $color_name;
                        }
                        else{
                            $str .= str_replace(' ', '', $item);
                        }
                    }
                }
                $item = array();
                $item['price'] = $request['price_'.str_replace('.', '_', $str)];
                $item['sku'] = $request['sku_'.str_replace('.', '_', $str)];
                $item['qty'] = $request['qty_'.str_replace('.', '_', $str)];
                $variations[$str] = $item;
            }
        }
        //combinations end

        $product->variations = json_encode($variations);


        $data = openJSONFile('en');
        $data[str_replace('"', '', $product->name)] = str_replace('"', '',$product->name);
        saveJSONFile('en', $data);

        if($product->save()){

            flash(__('Product has been inserted successfully'))->success();
            if(Auth::user()->user_type == 'admin'){
                return redirect()->route('products.admin');
            }
            else{
                return redirect()->route('seller.products');
            }
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_product_edit($id)
    {
        $product = Product::findOrFail(decrypt($id));
        //dd(json_decode($product->price_variations)->choices_0_S_price);
        $tags = json_decode($product->tags);
        $categories = Category::all();
        return view('products.edit', compact('product', 'categories', 'tags'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function seller_product_edit($id)
    {
        $product = Product::findOrFail(decrypt($id));
        //dd(json_decode($product->price_variations)->choices_0_S_price);
        $tags = json_decode($product->tags);
        $categories = Category::all();

        return view('products.edit', compact('product', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->name = str_replace(array('\'', '"'), '', $request->name);
        $product->sku = $request->sku;
        $product->asin = $request->asin;
        $product->category_id = $request->category_id;
        $product->subcategory_id = $request->subcategory_id;
        $product->subsubcategory_id = $request->subsubcategory_id;
        $product->category_name = Category::where('id', $request->category_id)->first()->name;
        $product->subcategory_name = SubCategory::where('id', $request->subcategory_id)->first()->name;
        $product->subsubcategory_name = SubSubCategory::where('id', $request->subsubcategory_id)->first()->name;
        $product->brand_id = $request->brand_id;
        $product->external_website = $request->external_website;
        $product->external_link = $request->external_link;


        $product->quantity = $request->quantity;
        $size = array();
        $product->size = json_encode($request->size);


        if($request->has('previous_photos')){
            $photos = $request->previous_photos;
        }
        else{
            $photos = array();
        }

        if($request->hasFile('photos')){
            foreach ($request->photos as $key => $photo) {
                 $imageName = date('mdYHis') . uniqid() .'.'.$photo->extension();
                $store = $photo->storeAs('public/products/photos',$imageName);
                $path  = "storage/products/photos/".$imageName;
                array_push($photos, $path);
                //ImageOptimizer::optimize(base_path('public/').$path);
            }
        }
        $product->photos = json_encode($photos);
        if(isset(Auth::user()->shop->package)){
            $PackageInfo = \App\Models\PricingPlan::where('id', Auth::user()->shop->package)->first();
            if(empty($PackageInfo->per_month) || $PackageInfo->per_year){
                $product->published = 0;
            }
        }
        $product->thumbnail_img = $request->previous_thumbnail_img;
        if($request->hasFile('thumbnail_img')){
             $imageName = time().'.'.$request->thumbnail_img->extension();
            $store = $request->thumbnail_img->storeAs('public/products/thumbnail',$imageName);
            $path  = "storage/products/thumbnail/".$imageName;
            $product->thumbnail_img = $path;
            //ImageOptimizer::optimize(base_path('public/').$product->thumbnail_img);
        }

        $product->featured_img = $request->previous_featured_img;
        if($request->hasFile('featured_img')){
            $product->featured_img = $request->featured_img->store('uploads/products/featured');
            //ImageOptimizer::optimize(base_path('public/').$product->featured_img);
        }

        $product->flash_deal_img = $request->previous_flash_deal_img;
        if($request->hasFile('flash_deal_img')){
            $product->flash_deal_img = $request->flash_deal_img->store('uploads/products/flash_deal');
            //ImageOptimizer::optimize(base_path('public/').$product->flash_deal_img);
        }

        $product->unit = $request->unit;
        $product->tags = implode('|',$request->tags);
        $product->description = $request->description;
        $product->spacification = $request->spacification;
        if(isset($request->video_link)){
            $product->video_provider = $request->video_provider;
            $product->video_link = $request->video_link;
        }
        $product->unit_price = $request->unit_price;
        $product->purchase_price = $request->purchase_price;
        $product->tax = $request->tax;
        $product->tax_type = $request->tax_type;
        $product->discount = $request->discount;
        $product->shipping_type = $request->shipping_type;
        if($request->shipping_type == 'free'){
            $product->shipping_cost = 0;
        }
        elseif ($request->shipping_type == 'local_pickup') {
            $product->shipping_cost = $request->local_pickup_shipping_cost;
        }
        elseif ($request->shipping_type == 'flat_rate') {
            $product->shipping_cost = $request->flat_shipping_cost;
        }
        $product->discount_type = $request->discount_type;
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;

        $product->meta_img = $request->previous_meta_img;
        if($request->hasFile('meta_img')){
            $imageName = time().'.'.$request->meta_img->extension();
            $store = $request->meta_img->storeAs('public/products/meta',$imageName);
            $path  = "storage/products/meta/".$imageName;
            $product->meta_img = $path;
            // $product->meta_img = $request->meta_img->store('uploads/products/meta');
            //ImageOptimizer::optimize(base_path('public/').$product->meta_img);
        }

        $pdfs = array();
        if($request->hasFile('pdf')){
            foreach ($request->pdf as $key => $pdf) {
                $path = $pdf->store('uploads/products/pdf');
                $pdf_details = array('path'=>$path, 'lng'=>$request->lng[$key]);
                array_push($pdfs, $pdf_details);
                //ImageOptimizer::optimize(base_path('public/').$path);
            }
            if(!empty($pdfs)){
                $product->pdf = json_encode($pdfs);
            }
        }

        $product->slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)).'-'.substr($product->slug, -5);

        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $product->colors = json_encode($request->colors);
        }
        else {
              $colors = array();
        $product->colors = json_encode($request->colors);

        }

        $choice_options = array();

        if($request->has('choice')){
            foreach ($request->choice_no as $key => $no) {
                $str = 'choice_options_'.$no;
                $item['name'] = 'choice_'.$no;
                $item['title'] = $request->choice[$key];
                $item['options'] = explode(',', implode('|', $request[$str]));
                array_push($choice_options, $item);
            }
        }

        $product->choice_options = json_encode($choice_options);

        foreach (Language::all() as $key => $language) {
            $data = openJSONFile($language->code);
            unset($data[str_replace('"', '', $product->name)]);
            $data[str_replace('"', '', $request->name)] = "";
            saveJSONFile($language->code, $data);
        }

        $variations = array();

        //combinations start
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $my_str = implode('|',$request[$name]);
                array_push($options, explode(',', $my_str));
            }
        }

        $combinations = combinations($options);
        if(count($combinations[0]) > 0){
            foreach ($combinations as $key => $combination){
                $str = '';
                foreach ($combination as $key => $item){
                    if($key > 0 ){
                        $str .= '-'.str_replace(' ', '', $item);
                    }
                    else{
                        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
                            $color_name = \App\Models\Color::where('code', $item)->first()->name;
                            $str .= $color_name;
                        }
                        else{
                            $str .= str_replace(' ', '', $item);
                        }
                    }
                }
                $item = array();
                $item['price'] = $request['price_'.str_replace('.', '_', $str)];
                $item['sku'] = $request['sku_'.str_replace('.', '_', $str)];
                $item['qty'] = $request['qty_'.str_replace('.', '_', $str)];
                $variations[$str] = $item;
            }
        }
        //combinations end

        $product->variations = json_encode($variations);

        if($product->save()){
            flash(__('Product has been updated successfully'))->success();
            if(Auth::user()->user_type == 'admin'){
                return redirect()->route('products.admin');
            }
            else{
                return redirect()->route('seller.products');
            }
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        // dd($product);
        if(Product::destroy($id)){
            foreach (Language::all() as $key => $language) {
                $data = openJSONFile($language->code);
                unset($data[$product->name]);
                saveJSONFile($language->code, $data);
            }
            if($product->thumbnail_img != null){
                //unlink($product->thumbnail_img);
            }
            if($product->featured_img != null){
                //unlink($product->featured_img);
            }
            if($product->flash_deal_img != null){
                //unlink($product->flash_deal_img);
            }
            // $image_path = public_path('/').$product->photo;
            // dd('public'.'/'.$product->photos);
            // unlink('public'.'/'.$product->photos);
            // foreach (json_decode($product->photos) as $key => $photo) {
            //     //unlink($photo);
            // }
            flash(__('Product has been deleted successfully'))->success();
            if(Auth::user()->user_type == 'admin'){
                return redirect()->route('products.admin');
            }
            else{
                return redirect()->route('seller.products');
            }
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Duplicates the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function duplicate($id)
    {
        $product = Product::find($id);
        $product_new = $product->replicate();
        $product_new->slug = substr($product_new->slug, 0, -5).str::random(5);

        if($product_new->save()){
            flash(__('Product has been duplicated successfully'))->success();
            if(Auth::user()->user_type == 'admin'){
                return redirect()->route('products.admin');
            }
            else{
                return redirect()->route('seller.products');
            }
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function get_products_by_subsubcategory(Request $request)
    {
        $products = Product::where('subsubcategory_id', $request->subsubcategory_id)->get();
        return $products;
    }

    public function get_products_by_brand(Request $request)
    {
        $products = Product::where('brand_id', $request->brand_id)->get();
        return view('partials.product_select', compact('products'));
    }

    public function updateTodaysDeal(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->todays_deal = $request->status;
        if($product->save()){
            return 1;
        }
        return 0;
    }

    public function updatePublished(Request $request)
    {
        $product = Product::findOrFail($request->id);
      
        $product->published = $request->status;
        $product->status = $request->status;

        if($product->save()){
            return 1;
        }
        return 0;
    }

    public function updateFeatured(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->featured = $request->status;
        if($product->save()){
            return 1;
        }
        return 0;
    }

    public function sku_combination(Request $request)
    {
        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }
        else {
            $colors_active = 0;
        }

        $unit_price = $request->unit_price;
        $product_name = $request->name;

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $my_str = implode('|', $request[$name]);
                array_push($options, explode(',', $my_str));
            }
        }

        $combinations = combinations($options);
        return view('partials.sku_combinations', compact('combinations', 'unit_price', 'colors_active', 'product_name'));
    }

    public function sku_combination_edit(Request $request)
    {
        $product = Product::findOrFail($request->id);

        $options = array();
        if($request->has('colors_active') && $request->has('colors') && count($request->colors) > 0){
            $colors_active = 1;
            array_push($options, $request->colors);
        }
        else {
            $colors_active = 0;
        }

        $product_name = $request->name;
        $unit_price = $request->unit_price;

        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;
                $my_str = implode('|', $request[$name]);
                array_push($options, explode(',', $my_str));
            }
        }

        $combinations = combinations($options);
        return view('partials.sku_combinations_edit', compact('combinations', 'unit_price', 'colors_active', 'product_name', 'product'));
    }

}
