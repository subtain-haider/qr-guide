<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Currency;
use App\Models\Country;
use App\Models\BusinessSetting;
use App\Models\PaymentType;

class BusinessSettingsController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function currencycreate()
    {
        return view('business_settings.currencycreate');
    }

	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function countrycreate()
    {
        return view('business_settings.countrycreate');
    }

    public function country(Request $request)
    {
        return view('business_settings.country');
    }

	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function currencystore(Request $request)
    {
        $currency = new Currency;
        $currency->name = $request->name;
		$currency->symbol = $request->symbol;
		$currency->code = $request->code;
		$currency->exchange_rate = $request->exchange_rate;
		$currency->status = $request->status;
		$currency->created_at = date('Y-m-d H:i:s');
		$currency->updated_at = date('Y-m-d H:i:s');

        if($currency->save()){
            flash(__('Currency has been inserted successfully'))->success();
            return redirect()->route('currency.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function countrystore(Request $request)
    {
        $country = new Country;
        $country->name = $request->name;
		$country->code = $request->code;
		//print_r($country);exit;
		if($country->save()){
            flash(__('Country has been inserted successfully'))->success();
            return redirect()->route('country.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

	public function activation(Request $request)
    {
    	return view('business_settings.activation');
    }

    public function social_login(Request $request)
    {
        return view('business_settings.social_login');
    }

    public function smtp_settings(Request $request)
    {
        return view('business_settings.smtp_settings');
    }

    public function google_analytics(Request $request)
    {
        return view('business_settings.google_analytics');
    }

    public function facebook_chat(Request $request)
    {
        return view('business_settings.facebook_chat');
    }

    public function payment_method(Request $request)
    {
        return view('business_settings.payment_method');
    }

	public function payment_types(Request $request)
    {
        return view('business_settings.payment_types');
    }

	public function create_payment_type(Request $request)
    {
        return view('business_settings.create_payment_type');
    }

	/**
     * Store a newly created payment type storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_payment_type(Request $request)
    {
        $payment_type = new PaymentType;
        $payment_type->name = $request->name;
		$payment_type->created_at = date('Y-m-d H:i:s');
		$payment_type->updated_at = date('Y-m-d H:i:s');

        if($payment_type->save()){
            flash(__('Payment type has been inserted successfully'))->success();
            return redirect()->route('payment_method.payment_types');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }			public function reward_settings(Request $request)    {        $active_currencies = Currency::where('status', 1)->get();		return view('business_settings.reward_settings', compact('active_currencies'));    }		/**     * Update the point configuration.     * @param  \Illuminate\Http\Request  $request     * @return \Illuminate\Http\Response     */    public function update_points_configuration(Request $request)    {        $business_settings = BusinessSetting::where('type', 'points_configuration')->first();        if($business_settings != null){            $customer_points = array('points'=>$request->points_per_customer, 'status'=>$request->points_per_customer_status);						$seller_points = array('points'=>$request->points_per_seller, 'status'=>$request->points_per_seller_status);			$json = array('customer_points'=>$customer_points, 'seller_points'=>$seller_points);						$business_settings->value = json_encode($json);			$business_settings->save();		}        flash("Settings updated successfully")->success();        return back();    }		/**     * Update the profit configuration.     * @param  \Illuminate\Http\Request  $request     * @return \Illuminate\Http\Response     */    public function update_profit_configuration(Request $request)    {        $business_settings = BusinessSetting::where('type', 'profit_configuration')->first();        if($business_settings != null){            $json = array('profit'=>$request->profit_configuration, 'status'=>$request->profit_configuration_status);			$business_settings->value = json_encode($json);			$business_settings->save();		}        flash("Settings updated successfully")->success();        return back();    }

	public function shipping_method(Request $request)
    {
        return view('business_settings.shipping_method');
    }

    /**
     * Update the API key's for payment methods.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function payment_method_update(Request $request)
    {
        foreach ($request->types as $key => $type) {
                $this->overWriteEnvFile($type, $request[$type]);
        }

        $business_settings = BusinessSetting::where('type', $request->payment_method.'_sandbox')->first();
        if($business_settings != null){
            if ($request->has($request->payment_method.'_sandbox')) {
                $business_settings->value = 1;
                $business_settings->save();
            }
            else{
                $business_settings->value = 0;
                $business_settings->save();
            }
        }

        flash("Settings updated successfully")->success();
        return back();
    }

	/**
     * Update the API key's for payment methods.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function shipping_method_update(Request $request)
    {

		$business_settings = BusinessSetting::where('type', 'free_shipping')->first();
        if($business_settings != null){
			$json = array('status'=>$request->free_shipping_status, 'cost'=>0);
            $business_settings->value = json_encode($json);
			$business_settings->save();
		}

		$business_settings = BusinessSetting::where('type', 'standard_shipping')->first();
        if($business_settings != null){
            $json = array('status'=>$request->standard_shipping_status, 'cost'=>$request->standard_shipping_cost);
			$business_settings->value = json_encode($json);
			$business_settings->save();
		}

		$business_settings = BusinessSetting::where('type', 'express_shipping')->first();
        if($business_settings != null){
			$json = array('status'=>$request->express_shipping_status, 'cost'=>$request->express_shipping_cost);
            $business_settings->value = json_encode($json);
			$business_settings->save();
		}

		flash("Settings updated successfully")->success();
        return back();
    }

    /**
     * Update the API key's for GOOGLE analytics.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function google_analytics_update(Request $request)
    {
        foreach ($request->types as $key => $type) {
                $this->overWriteEnvFile($type, $request[$type]);
        }

        $business_settings = BusinessSetting::where('type', 'google_analytics')->first();

        if ($request->has('google_analytics')) {
            $business_settings->value = 1;
            $business_settings->save();
        }
        else{
            $business_settings->value = 0;
            $business_settings->save();
        }

        flash("Settings updated successfully")->success();
        return back();
    }

    /**
     * Update the API key's for GOOGLE analytics.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function facebook_chat_update(Request $request)
    {
        foreach ($request->types as $key => $type) {
                $this->overWriteEnvFile($type, $request[$type]);
        }

        $business_settings = BusinessSetting::where('type', 'facebook_chat')->first();

        if ($request->has('facebook_chat')) {
            $business_settings->value = 1;
            $business_settings->save();
        }
        else{
            $business_settings->value = 0;
            $business_settings->save();
        }

        flash("Settings updated successfully")->success();
        return back();
    }

    /**
     * Update the API key's for other methods.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function env_key_update(Request $request)
    {
        foreach ($request->types as $key => $type) {
                $this->overWriteEnvFile($type, $request[$type]);
        }

        flash("Settings updated successfully")->success();
        return back();
    }

    /**
     * overWrite the Env File values.
     * @param  String type
     * @param  String value
     * @return \Illuminate\Http\Response
     */
    public function overWriteEnvFile($type, $val)
    {
        $path = base_path('.env');
        if (file_exists($path)) {
            $val = '"'.trim($val).'"';
            if(strpos(file_get_contents($path), $type) >= 0){
                file_put_contents($path, str_replace(
                    $type.'="'.env($type).'"', $type.'='.$val, file_get_contents($path)
                ));
            }
            else{
                file_put_contents($path, file_get_contents($path).$type.'='.$val);
            }
        }
    }

    public function seller_verification_form(Request $request)
    {
    	return view('business_settings.seller_verification_form');
    }

    /**
     * Update sell verification form.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function seller_verification_form_update(Request $request)
    {
        $form = array();
        $select_types = ['select', 'multi_select', 'radio'];
        $j = 0;
        for ($i=0; $i < count($request->type); $i++) {
            $item['type'] = $request->type[$i];
            $item['label'] = $request->label[$i];
            if(in_array($request->type[$i], $select_types)){
                $item['options'] = json_encode($request['options_'.$request->option[$j]]);
                $j++;
            }
            array_push($form, $item);
        }
        $business_settings = BusinessSetting::where('type', 'verification_form')->first();
        $business_settings->value = json_encode($form);
        if($business_settings->save()){
            flash("Verification form updated successfully")->success();
            return back();
        }
    }

    public function update(Request $request)
    {
        foreach ($request->types as $key => $type) {
            $business_settings = BusinessSetting::where('type', $type)->first();
            if($business_settings!=null){
                $business_settings->value = $request[$type];
                $business_settings->save();
            }
            else{
                $business_settings = new BusinessSetting;
                $business_settings->type = $type;
                $business_settings->value = $request[$type];
                $business_settings->save();
            }
        }
        flash("Settings updated successfully")->success();
        return back();
    }

    public function updateActivationSettings(Request $request)
    {
        $business_settings = BusinessSetting::where('type', $request->type)->first();
        if($business_settings!=null){
            $business_settings->value = $request->value;
            $business_settings->save();
        }
        else{
            $business_settings = new BusinessSetting;
            $business_settings->type = $request->type;
            $business_settings->value = $request->value;
            $business_settings->save();
        }
        return '1';
    }

    public function vendor_commission(Request $request)
    {
        $business_settings = BusinessSetting::where('type', 'vendor_commission')->first();
        return view('business_settings.vendor_commission', compact('business_settings'));
    }

    public function vendor_commission_update(Request $request){
        $business_settings = BusinessSetting::where('type', $request->type)->first();
        $business_settings->type = $request->type;
        $business_settings->value = $request->value;
        $business_settings->save();

        flash('Seller Commission updated successfully')->success();
        return back();
    }
}
