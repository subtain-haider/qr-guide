<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faq;

class FaqController extends Controller
{

   public function index()
    {
        $faqs = Faq::all();
        return view('faq.index', compact('faqs'));
    }




   public function create()
    {
		return view('faq.create');
    }





   public function store(Request $request)
    {
        $plan = new Faq;
        $plan->question = $request->question;
        $plan->answer = $request->answer;
       
        if($plan->save()){
            flash(__('Faq has been inserted successfully'))->success();
            return redirect()->route('faq.index');
        }
		else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }




   public function edit($id)
    {
        $faq = Faq::findOrFail(decrypt($id));
        return view('faq.edit', compact('faq'));
    }



   public function update(Request $request, $id)
    {
        $plan = Faq::findOrFail($id);

        $plan->question = $request->question;
        $plan->answer = $request->answer;
        
        if($plan->save()){
            flash(__('Faq has been updated successfully'))->success();
            return redirect()->route('faq.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }


   public function destroy($id)
    {
        if(Faq::destroy($id)){
            flash(__('Faq has been deleted successfully'))->success();
            return redirect()->route('faq.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }




}

?>