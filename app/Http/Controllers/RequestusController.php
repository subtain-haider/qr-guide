<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Requestus;
use Auth;
use Session;

class RequestusController extends Controller
{

   public function index()
    {
        $requestuss = Requestus::all();
        return view('requestus.index', compact('requestuss'));
    }



   public function destroy($id)
    {
        if(Requestus::destroy($id)){
            flash(__('request has been deleted successfully'))->success();
            return redirect()->route('requestus.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }




}

?>