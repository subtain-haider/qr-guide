<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserRegister extends Controller
{

    public function index()
    {
        return view('support.register');
    }
    public function register(Request $request)
    {
  
        $data = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'slug' => $request['vid'],
            'user_type' => 'support',
            'password' => Hash::make($request['password']),
        ]);
        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
            return redirect('inbox');
        }
    }
}
