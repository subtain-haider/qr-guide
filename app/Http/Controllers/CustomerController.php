<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\User;
use App\Models\Order;
use App\Models\PricingPlan;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type=NULL)
    {
        $customers = User::where('user_type','customer')->orderBy('created_at', 'desc')->get();
        $packages = PricingPlan::get();
        return view('customers.index', compact('customers', 'type','packages'));
    }
    public function assignplan(Request $request)
    {
        $seller = User::findOrFail($request->customerid);
        $packages = PricingPlan::where('id',$request->packegeid)->first();
        $seller->package = $request->packegeid;
        $seller->user_type = 'seller';
        $seller->package_for = $packages->permissions;
        $seller->package_plane = $packages->name;
        $seller->package_start = date('Y-m-d H:i:s');
        $seller->package_end = date('Y-m-d H:i:s', strtotime("+30 days"));
        if($seller->save()){
            flash(__('Customer update to seller successfully'))->success();
            //return redirect()->route('sellers.index');
			return back();
        }
        flash(__('Something went wrong'))->error();
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::where('user_id', User::findOrFail($id)->id)->delete();
        if(User::destroy(User::findOrFail($id)->id)){
            flash(__('Customer has been deleted successfully'))->success();
            return redirect()->route('customers.index');
        }

        flash(__('Something went wrong'))->error();
        return back();
    }
}
