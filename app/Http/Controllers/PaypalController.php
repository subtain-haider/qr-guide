<?php

namespace App\Http\Controllers;
use DB;
use App\Http\Requests;
use App\Models\Product;
use App\Models\Order;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;


class PaypalController extends Controller
{
    private $_api_context;
    protected $data;
    public function __construct()
    {
        // $paypal_configuration = \Config::get('paypal');
        // $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_configuration['client_id'], $paypal_configuration['secret']));
        // $this->_api_context->setConfig($paypal_configuration['settings']);
    }

    public function payWithPaypal()
    {
        return view('paywithpaypal');
    }

    public function payment(Request $request)
    {
        $cart_detail = Session::get('cart');
        // dd($cart_detail);
        $products = \DB::table('products')->where('id', $cart_detail[0]['id'])->first();
        $user = \DB::table('users')->where('id',$products->user_id)->first();
        $paypal_c_id = $user->paypal_c_id;
        $paypal_s_id = $user->paypal_s_id;
         $paypal_configuration = [
            'client_id' => $paypal_c_id,
            'secret' =>  $paypal_s_id,
            'settings' => array(
                'mode' => 'sandbox',
                'http.ConnectionTimeOut' => 1000,
                'log.LogEnabled' => true,
                'log.FileName' => storage_path() . '/logs/paypal.log',
                'log.LogLevel' => 'FINE'
            ),
        ];
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_configuration['client_id'], $paypal_configuration['secret']));
        $this->_api_context->setConfig($paypal_configuration['settings']);
        $shippin_amount = Session::get('shipping_method')['cost'];
        $subtotal = 0;
        $tax = 0;
        $shipping = 0;
        foreach (Session::get('cart') as $key => $cartItem)
        {
        $product = \App\Models\Product::find($cartItem['id']);
        $subtotal += $cartItem['price']*$cartItem['quantity'];
        $tax += $cartItem['tax']*$cartItem['quantity'];
        $shipping += $cartItem['shipping']*$cartItem['quantity'];
        $product_name_with_choice = $product->name;
        if(isset($cartItem['color'])){
            $product_name_with_choice .= ' - '.\App\Models\Color::where('code', $cartItem['color'])->first()->name;
        }
        foreach (json_decode($product->choice_options) as $choice){
            $str = $choice->name; // example $str =  choice_0
            $product_name_with_choice .= ' - '.$cartItem[$str];
        }
        }

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

    	$item_1 = new Item();

        $item_1->setName('Product 1')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($subtotal + $cartItem['tax'] + $shippin_amount);

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        $total = $subtotal + $cartItem['tax'] + $shippin_amount;
        $amount->setCurrency('USD')
            ->setTotal($total);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Enter Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('status'))
            ->setCancelUrl(URL::route('status'));

        $payment = new Payment();
        // dd($payment);
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error','Connection timeout');
                return Redirect('paywithpaypal');
            } else {
                \Session::put('error','Some error occur, sorry for inconvenient');
                return Redirect('paywithpaypal');
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {
            return Redirect::away($redirect_url);
        }

        \Session::put('error','Unknown error occurred');
    	return Redirect('paywithpaypal');
    }

    public function getPaymentStatus(Request $request)
    {
        $cart_detail = Session::get('cart');
        $products = \DB::table('products')->where('id', $cart_detail[0]['id'])->first();
        $user = \DB::table('users')->where('id',$products->user_id)->first();
        $paypal_c_id = $user->paypal_c_id;
        $paypal_s_id = $user->paypal_s_id;
         $paypal_configuration = [
            'client_id' => $paypal_c_id,
            'secret' =>  $paypal_s_id,
            'settings' => array(
                'mode' => 'sandbox',
                'http.ConnectionTimeOut' => 1000,
                'log.LogEnabled' => true,
                'log.FileName' => storage_path() . '/logs/paypal.log',
                'log.LogLevel' => 'FINE'
            ),
        ];
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_configuration['client_id'], $paypal_configuration['secret']));
        $this->_api_context->setConfig($paypal_configuration['settings']);
        $payment_id = Session::get('paypal_payment_id');
        // dd($payment_id);
        Session::forget('paypal_payment_id');
        if (empty($request->input('PayerID')) || empty($request->input('token'))) {
            \Session::put('error','Payment failed');
            return Redirect('checkout/payment_select');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input('PayerID'));
        $result = json_decode($payment->execute($execution, $this->_api_context));

        $get = $result->transactions;
        foreach ($get as $gets) {
            $ab = $gets->amount;
            $pay_description = $gets->description;
            

            $final_amount = $ab->total;
            
        }


            $payg = $result->payer;
            $pay_method = $payg->payment_method;

        if ($result->state == 'approved') {

            DB::table('payments')->insert([
                'seller_id' => $products->user_id,
                'amount' => $final_amount,
                'payment_details' => $pay_description,
                'payment_method' => $pay_method,
                
            ]);
            $order = Order::findOrFail(Session::get('order_id'));
            $shipping = json_decode($order->shipping_address);
            $to_name = $shipping->name;
            $to_email = $shipping->email;
            $data = array('name'=>"Ogbonna Vitalis('sender_name')", "body" => "A test mail");
            \Mail::send("emails.ordermail", $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
            ->subject("Order Confirmation mail");
            $message->from("zain.ulabidin7501@gmail.com","Order Confirmation mail");
            });
            
                $checkoutController = new CheckoutController;
                return $checkoutController->checkout_done($request->session()->get('order_id'), $payment);
        }
        else{

            \Session::put('error','Payment failed !!');
            return Redirect('checkout/payment_select');

        }

       
    }
}
