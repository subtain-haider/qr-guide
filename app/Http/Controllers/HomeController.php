<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use Hash;
use Carbon\Carbon;
use App\Models\Category;
use App\Models\Review;
use App\Models\Country;
use App\Models\Careers;
use App\Models\City;
use App\Models\Brand;
use App\Models\SubSubCategory;
use App\Models\Product;
use App\Models\User;
use App\Models\Staff;
use App\Models\Seller;
use App\Models\Shop;
use App\Models\Color;
use App\Models\Order;
use App\Models\Warranty;
use App\Models\ClaimWarranty;
use App\Models\Units;
use App\Models\BusinessSetting;
use App\Models\Homediv;
use App\Models\Faq;
use App\Models\Coupon;
use App\Models\Maillist;
use App\Models\Aboutus;
use App\Http\Controllers\SearchController;
use Illuminate\Support\Facades\Storage;
use Shetabit\Visitor\Traits\Visitor;
use ImageOptimizer;
Use Mail;
Use DB;
class HomeController extends Controller
{
    public function login()
    {
        if(Auth::check()){
            return redirect()->route('dashboard');
        }
        return view('frontend.user_login');
    }
    
    public function viewednotification($pagelink,$id)
    {
        // return $pagelink;
        
        $viewed = DB::table($pagelink)->where('id',$id)->update(array('viewed' => '1'));
        if($viewed){
            if($pagelink == 'users'){
                return redirect('/clients-report');
            } elseif($pagelink == 'orders'){
                return redirect('/total-sales');
            } elseif($pagelink == 'questions'){
                return redirect('/seller-questions');
            } elseif($pagelink == 'requestuses'){
                return redirect()->back();
            } elseif($pagelink == 'reviews'){
                return redirect('/reviews');
            } elseif($pagelink == 'tickets'){
                return redirect('/customers/tickets');
            } elseif($pagelink == 'guiderequests'){
                return redirect('/');
            }
        } else {
            return 'not update';
        }
        
    }

    public function registration()
    {
        if(Auth::check()){
            return redirect()->route('dashboard');
        }
        return view('frontend.user_registration');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function logActivity()
    {
        $logs = logActivityLists();
        return view('logActivity',compact('logs'));
    }

    public function flash_deal()
    {
        $flash_deal = \App\Models\FlashDeal::where('status', 1)->first();
        $flash_deal_products = array();
        foreach(\App\Models\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->get() as $deal_products){
            $flash_deal_products[] = $deal_products->product_id;
        }
        //$flash_deal = filter_flash_products($flash_deal)->paginate(12)->appends(request()->query());
        $products = filter_products(Product::whereIn('id', $flash_deal_products)->orderBy('created_at', 'desc'))->paginate(12);
        return view('frontend.flash_deal' , compact('flash_deal', 'products'));
    }

    public function user_login(Request $request)
    {
        // dd($request->all());
        $user = User::whereIn('user_type', ['customer', 'seller'])->where('email', $request->email)->first();
        if($user != null){
            if(Hash::check($request->password, $user->password)){
                if($request->has('remember')){
                    auth()->login($user, true);
                }
                else{
                    auth()->login($user, false);
                }
                if($user->user_type == 'seller'){
                    LogActivity('Vendor login successfully.');
                }else if($user->user_type == 'customer'){
                    LogActivity('User login successfully.');
                }
                return redirect()->route('dashboard');
            }
        }
        flash(__('Login Unsuccessful. Please check your email and password'))->error();
        return back();
    }

    public function cart_login(Request $request)
    {
        $user = User::whereIn('user_type', ['customer', 'seller'])->where('email', $request->email)->first();
        if($user != null){
            updateCartSetup();
            if(Hash::check($request->password, $user->password)){
                if($request->has('remember')){
                    auth()->login($user, true);
                }
                else{
                    auth()->login($user, false);
                }
            }
        }
        return back();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_dashboard()
    {
       
        
        return view('dashboard');
    }

    /**
     * Show the customer/seller dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
        $orders = Order::where('user_id', Auth::user()->id)->orderBy('code', 'desc')->paginate(9);
        if(Auth::user()->user_type == 'seller'){
            
            
            return view('frontend.seller.dashboard', compact('orders'));
        }
        elseif(Auth::user()->user_type == 'customer'){
            return view('frontend.customer.dashboard', compact('orders'));
        }
        else {
            abort(404);
        }
    }
    public function marketing(Request $request)
    {
        if(Auth::user()->user_type == 'seller'){
            return view('frontend.seller.email_marketing');
        }
    }
    public function profile(Request $request)
    {
        if(Auth::user()->user_type == 'customer'){
            return view('frontend.customer.profile');
        }
        elseif(Auth::user()->user_type == 'seller'){
            return view('frontend.seller.profile');
        }
    }

    public function customer_update_profile(Request $request)
    {
        $user = Auth::user();
        $user->name = $request->name;
        $user->address = $request->address;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->postal_code = $request->postal_code;
        $user->phone = $request->phone;

        if($request->new_password != null && ($request->new_password == $request->confirm_password)){
            $user->password = Hash::make($request->new_password);
        }

        if($request->hasFile('photo')){
            $imageName = time().'.'.$request->photo->extension();
            $store = $request->photo->storeAs('public/profile/',$imageName);
            $path  = "storage/profile/".$imageName;
            $user->avatar_original = $path;
        }
        if($user->save()){
            flash(__('Your Profile has been updated successfully!'))->success();
            return back();
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    public function seller_email_send(Request $request) {
        // return  $request->selectelist;
        //  return $mails = User::whereIn('slug', $request->selectelist)->get();
         $mails = $request->selectelist;
        if(isset($request->cemail)){
            array_push($mails, $request->cemail);
        }
        $to_subject = $request->subject;
        $from_mail = $request->fromemail;
        foreach($mails as $email){
            $to_email = $email;
            \Mail::send(['html' => 'frontend.seller.mail'],['coupon' => $request->selectedcoupon,'name' => $request->bodymessage],function($message)  use ($to_subject, $to_email,$from_mail) {
			$message->to($to_email,$to_email)->subject($to_subject);
			$message->from($from_mail,$from_mail); });
        }
        flash(__('Emails has been sent successfully!'))->success();
            return back();
        
    }
    
    public function seller_update_profile(Request $request)
    {
        // dd($request->paypal_c_id);
        $user = Auth::user();

        $user->name = $request->name;
        $user->address = $request->address;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->postal_code = $request->postal_code;
        $user->phone = $request->phone;
        $user->template = $request->template;
        $user->paypal_c_id  = $request->paypal_c_id;
        $user->paypal_s_id  = $request->paypal_s_id;
        $user->stripe_c_id = $request->stripe_c_id;
        $user->stripe_s_id = $request->stripe_s_id;

        if($request->new_password != null && ($request->new_password == $request->confirm_password)){
            $user->password = Hash::make($request->new_password);
        }

        if($request->hasFile('photo')){
            $imageName = time().'.'.$request->photo->extension();
            $store = $request->photo->storeAs('public/profile/',$imageName);
            $path  = "storage/profile/".$imageName;
            $user->avatar_original = $path;
        }

        $seller = $user->seller;
        // $seller->cash_on_delivery_status = $request->cash_on_delivery_status;
        // $seller->sslcommerz_status = $request->sslcommerz_status;
        // $seller->ssl_store_id = $request->ssl_store_id;
        // $seller->ssl_password = $request->ssl_password;
        // $seller->paypal_status = $request->paypal_status;
        // $seller->paypal_client_id = $request->paypal_client_id;
        // $seller->paypal_client_secret = $request->paypal_client_secret;
        // $seller->stripe_status = $request->stripe_status;
        // $seller->stripe_key = $request->stripe_key;
        // $seller->stripe_secret = $request->stripe_secret;
        // $seller->jazz_cash_status= $request->jazz_cash_status;
        // $seller->jazz_cash_number= $request->jazz_cash_number;
        // $seller->easypaisa_status= $request->easypaisa_status;
        // $seller->easypaisa_number= $request->easypaisa_number;

        // $seller->ubl_omni_status= $request->ubl_omni_status;
        // $seller->ubl_omni_number= $request->ubl_omni_number;

		// $seller->bank_name = $request->bank_name;
        // $seller->account_no = $request->account_no;
        // $seller->bank_code = $request->bank_code;
        // $seller->bank_address = $request->bank_address;

        if($user->save() && $seller->save()){
            flash(__('Your Profile has been updated successfully!'))->success();
            return back();
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

	public function careers(Request $request)
    {
		$careers = Careers::all();
        return view('frontend.careers.index', compact('careers'));
    }
    public function update_payment_methodes(Request $request)
    {
		$user = Auth::user();
        $seller = $user->seller;
        $seller->cash_on_delivery_status = $request->cash_on_delivery_status;
        $seller->sslcommerz_status = $request->sslcommerz_status;
        $seller->ssl_store_id = $request->ssl_store_id;
        $seller->ssl_password = $request->ssl_password;
        $seller->paypal_status = $request->paypal_status;
        $seller->paypal_client_id = $request->paypal_client_id;
        $seller->paypal_client_secret = $request->paypal_client_secret;
        $seller->stripe_status = $request->stripe_status;
        $seller->stripe_key = $request->stripe_key;
        $seller->stripe_secret = $request->stripe_secret;

        $seller->jazz_cash_status = $request->jazz_cash_status;
        $seller->jazz_cash_number = $request->jazz_cash_number;
        $seller->easypaisa_status = $request->easypaisa_status;
        $seller->easypaisa_number = $request->easypaisa_number;

        $seller->ubl_omni_status = $request->ubl_omni_status;
        $seller->ubl_omni_number = $request->ubl_omni_number;

		$seller->bank_name = $request->bank_name;
        $seller->account_no = $request->account_no;
        $seller->bank_code = $request->bank_code;
        $seller->bank_address = $request->bank_address;
        if($seller->save()){
            flash(__('Payment Details has been updated successfully!'))->success();
            return back();
        }

        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

	/**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        $careers = Careers::findOrFail(decrypt($id));

		$countries = Country::all();

        return view('frontend.careers.show', compact('careers', 'countries'));

    }

	public function register_application(Request $request)
    {
        $check_user = User::where('email', $request->email)->count();
        if($check_user > 0){
            flash(__('Sorry! email already registerd.'))->error();
            return back();
        }else{
            $user = new User;
            $user->name = $request->name;
            $user->gender = $request->gender;
            $user->cnic = $request->cnic;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->country = $request->country;
            $user->city = $request->city;
            $user->user_type = "staff";
            $user->status = 1;
            $user->ref_code = Hash::make($user->email);
            $user->password = Hash::make($request->password);
            if($request->hasFile('photo')){
                $user->avatar_original = $request->photo->store('uploads');
            }
            if($user->save()){
                LogActivity('Vendor register successfully.');
                $staff = new Staff;
                $staff->user_id = $user->id;
                $staff->role_id = 3;
                if($staff->save()){
                    flash(__('Application has been inserted successfully'))->success();
                    return redirect()->route('career_show', encrypt($request->job_id));
                }
            }
        }
    }

	public function cities_by_country(Request $request)
    {
		$CitiesOptions = '<option value="">Please Select</option>';
		$cities = City::all();
		$cities = $cities->where('country_code', '=', $request->country_code);
		foreach($cities as $city){
			$CitiesOptions .= '<option value="'.$city->id.'">'.$city->name.'</option>';
		}
		return json_encode(array('options'=>$CitiesOptions));
    }

    /**
     * Show the application frontend home.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $files = scandir(base_path('public/uploads/categories'));
        // foreach($files as $file) {
        //     ImageOptimizer::optimize(base_path('public/uploads/categories/').$file);
        // }

        // if(Auth::user() and Auth::user()->user_type == 'seller'){
        //     return view('frontend.seller.dashboard');
        // }
        // elseif(Auth::user() and Auth::user()->user_type == 'customer'){
        //     return view('frontend.customer.dashboard');
        // }
        // else {
        //     return view('frontend.user_login');
        // }

        //return view('frontend.index');
        $homedivs = Homediv::all();
        return view('frontend.index', compact('homedivs'));
    }

    public function pricing()
    {
        // $files = scandir(base_path('public/uploads/categories'));
        // foreach($files as $file) {
        //     ImageOptimizer::optimize(base_path('public/uploads/categories/').$file);
        // }

        // if(Auth::user() and Auth::user()->user_type == 'seller'){
        //     return view('frontend.seller.dashboard');
        // }
        // elseif(Auth::user() and Auth::user()->user_type == 'customer'){
        //     return view('frontend.customer.dashboard');
        // }
        // else {
        //     return view('frontend.user_login');
        // }

        return view('frontend.pricing');
    }

    public function trackOrder(Request $request)
    {
        if($request->has('order_code')){
            $order = Order::where('code', $request->order_code)->first();
            if($order != null){
                return view('frontend.track_order', compact('order'));
            }
        }
        return view('frontend.track_order');
    }

    public function product($slug)
    {
        $product  = Product::where('slug', $slug)->first();
        if($product!=null){
            LogActivity('Visit Item:'.$product->name);
            updateCartSetup();
            return view('frontend.product_details', compact('product'));
        }
        abort(404);
    }

    public function singleproduct($slug)
    {
        $product  = Product::where('slug', $slug)->first();
        if($product!=null){
            return view('frontend.single-product', compact('product'));
        }
        abort(404);
    }

    public function seller_product($slug)
    {
        $product  = Product::where('slug', $slug)->first();
        if($product!=null){
            return view('frontend.seller.product_view', compact('product'));
        }
        abort(404);
    }

    public function shop($slug, Request $request)
    {
       
        //   DB::table('users')->update(['votes' => 1]);
        $shop  = Shop::where('slug', $slug)->first();
        
        $request->visitor()->visit();
       
       
        
        // $slug2 = ($slug);
        // Session::put('slugs', url()->current());
          
          
        
        if(!empty($shop->product)){
        $reviews  = Review::orderBy('id', 'DESC')->where('product_id', $shop->product)->where('status', 1)->paginate(10);
        }
        else{
            $reviews  = '';
        }
        $datas=array();
        $i=0;
        if(!empty($shop->coupon_text) && $shop->coupon_text != 'null'){
        foreach(json_decode($shop->coupon_text) as $coupentxt){
            $datas[$i] = $coupentxt;
            $i++;
            }
        }
        $coupon =array();
        if(!empty($shop->coupon_id) && $shop->coupon_id != 'null'){
          $coupon = Coupon::where('id',$shop->coupon_id)->first();
        }
        if($shop!=null){
            
            return view('frontend.seller_shop', compact('shop','datas','reviews','coupon'));
        }
        abort(404);
    }

    public function getreviewsbyrating($slug,$rating)
    {
        $shop  = Shop::where('slug', $slug)->first();
        if(!empty($shop->product)){
        $reviews  = Review::orderBy('id', 'DESC')->where('product_id', $shop->product)->where('rating',$rating)->where('status', 1)->paginate(3);
        }
        else{
            $reviews  = '';
        }
        $datas=array();
        $i=0;
        if(!empty($shop->coupon_text) && $shop->coupon_text != 'null'){
        foreach(json_decode($shop->coupon_text) as $coupentxt){
            $datas[$i] = $coupentxt;
            $i++;
            }
        }
        if($shop!=null){
            return view('frontend.seller_shop', compact('shop','datas','reviews'));
        }
        abort(404);
    }

    public function custompages($name,$slug)
    {
        $pages = array('page-1', 'page-2', 'page-3','page-4', 'page-5', 'page-6');
        $shop  = Shop::where('slug', $slug)->first();
        if($shop!=null and in_array($name, $pages)){
            return view('frontend.shop_custom_pages', compact('shop', 'name'));
        }
        abort(404);
    }

    public function pdf($slug)
    {
        $product  = Product::where('slug', $slug)->first();
        if($product!=null){
            return view('frontend.view_pdf', compact('product'));
        }
        abort(404);
    }

    public function filter_shop($slug, $type)
    {
        $shop  = Shop::where('slug', $slug)->first();
        if($shop!=null && $type != null){
            return view('frontend.seller_shop', compact('shop', 'type'));
        }
        abort(404);
    }

    public function listing(Request $request)
    {
        $contents = Storage::disk('public/uploads/products/photos')->get('file.jpg');
        dd($contents);
        $brands = Brand::all();
        $products = filter_products(Product::orderBy('created_at', 'desc'))->paginate(12);
        return view('frontend.product_listing', compact('products','brands'));
    }

    public function all_categories(Request $request)
    {
        $categories = Category::all();
        return view('frontend.all_category', compact('categories'));
    }
    public function all_brands(Request $request)
    {
        $categories = Category::all();
        return view('frontend.all_brand', compact('categories'));
    }

    public function show_product_upload_form(Request $request)
    {
        $categories = Category::all();
        return view('frontend.seller.product_upload', compact('categories'));
    }

    public function show_guide_upload_form(Request $request)
    {
        $categories = Category::all();
        return view('frontend.seller.guide_upload', compact('categories'));
    }

    public function company_warranty($slug)
    {
        $shop = Shop::where('slug', $slug)->first();
        if($shop != null){
            return view('frontend.warranty', compact('shop'));
        }
        abort(404);
    }

    public function company_warranty_claim($slug)
    {
        $shop = Shop::where('slug', $slug)->first();
        if($shop != null){
            return view('frontend.claim-warranty', compact('shop'));
        }
        abort(404);
    }

    public function company_warranty_claim_submit(Request $request, $slug)
    {
            // $shop = Shop::where('slug', $slug)->first();
            $updt = Warranty::where('id', $slug)->update(['status' => 1]);
            // $warranties = new ClaimWarranty;
            // $warranties->date_of_purchase = $request->date_of_purchase;
            // $warranties->date_of_product_end = $request->date_of_product_end;
            // $warranties->client_name = $request->client_name;
            // $warranties->sq_of_product = $request->sq_of_product;
            // $warranties->product_serial_number = $request->product_serial_number;
            // $warranties->shop_id = $shop->id;
            // $datetime1 = strtotime($request->date_of_purchase); // convert to timestamps
            // $datetime2 = strtotime($request->date_of_product_end); // convert to timestamps
            // $days = (int)(($datetime2 - $datetime1)/86400);
            // if($days <= $shop->w_edate){
            //     if($warranties->save())
            if($updt)
                {
                    $warranties = Warranty::where('seller_id', Auth::user()->id)->orderby('id','desc')->get();
                    flash('Claim Warranty Has been Updated')->success();
                    return view('frontend.warranties_page', compact('warranties'))->with('msg', 'Claim Warranty Has been Updated');
                    return redirect()->back()->with('msg', 'Claim Warranty Has been Updated');
                // }
            } else {
               flash('Something went wrong')->error();
               return back();
            }


        abort(404);
    }

    public function warranties()
    {
        $warranties = Warranty::all();
        if($warranties != null){
            return view('warranties', compact('warranties'));
        }
        abort(404);
    }

    public function show_product_edit_form(Request $request, $id)
    {
        $categories = Category::all();
        $product = Product::find(decrypt($id));
        return view('frontend.seller.product_edit', compact('categories', 'product'));
    }

    public function seller_product_list(Request $request)
    {
        $products = Product::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(10);
        // dd($products);
        
        
        
        return view('frontend.seller.products', compact('products'));
    }



    public function seller_guide_list(Request $request)
    {
        $products = Product::where('user_id', Auth::user()->id)->where('product_type','guide')->orderBy('created_at', 'desc')->paginate(10);
        return view('frontend.seller.products', compact('products'));
    }

    public function ajax_search(Request $request)
    {
        $keywords = array();
        $products = Product::where('published', 1)->where('tags', 'like', '%'.$request->search.'%')->where('name', 'like', '%'.$request->search.'%')->get();
        foreach ($products as $key => $product) {
            foreach (explode(',',$product->tags) as $key => $tag) {
                if(stripos($tag, $request->search) !== false){
                    if(sizeof($keywords) > 5){
                        break;
                    }
                    else{
                        if(!in_array(strtolower($tag), $keywords)){
                            array_push($keywords, strtolower($tag));
                        }
                    }
                }
            }
        }

        $products = filter_products(Product::where('published', 1)->where('name', 'like', '%'.$request->search.'%'))->get()->take(3);

        $subsubcategories = SubSubCategory::where('name', 'like', '%'.$request->search.'%')->get()->take(3);

        $shops = Shop::where('name', 'like', '%'.$request->search.'%')->get()->take(3);

        if(sizeof($keywords)>0 || sizeof($subsubcategories)>0 || sizeof($products)>0 || sizeof($shops) >0){
            return view('frontend.partials.search_content', compact('products', 'subsubcategories', 'keywords', 'shops'));
        }
        return '0';
    }

public function searchminisite(Request $request)
    {
        $query = $request->q;
        if(isset($request->gsearch)){
            $query = $request->gsearch;
        }
            $shops = Shop::where('title', 'like', '%'.$query.'%')->orWhere('slug', 'like', '%'.$query.'%')->paginate(12);
            return view('frontend.seller.shops', compact('shops'));
    }
public function searchproducts(Request $request)
    {
        $query = $request->q;
        if(isset($request->gsearch)){
            $query = $request->gsearch;
        }
            $products = Product::where('name', 'like', '%'.$query.'%')->orWhere('slug', 'like', '%'.$query.'%')->paginate(12);
            return view('frontend.seller.products', compact('products'));
    }
    public function search(Request $request)
    {
        $query = $request->q;
        if(isset($request->gsearch)){
            $query = $request->gsearch;
        }

        // $conditions = ['published' => 1];

        // $products = Product::where($conditions);

        // echo join(',',$Category_array);exit;
        // $conditions = array_merge($conditions, ['brand_id' => $request->brand_id]);

        // if($query != null){
            $products = Product::where('product_type', 'guide')->where('name', 'like', '%'.$query.'%')->orWhere('category_name', 'like', '%'.$query.'%')->orWhere('subcategory_name', 'like', '"'.$query.'%')->orWhere('subsubcategory_name', 'like', '%'.$query.'%')->get();
        // }
        // $products = $products->get();

        // $products = filter_products($products)->paginate(12)->appends(request()->query());

        return view('frontend.product_listing', compact('products'));
    }

    public function search_best_sellers(Request $request)
    {
        $query = $request->q;
        $array = array();
        $users = \App\Models\User::where('user_type', 'seller')->where('name', 'like', '%'.$request->q.'%')->get();
        $sellers = array();
        foreach($users as $user){
            $sellers[] = $user->id;
        }
        foreach (\App\Models\Seller::where('user_id', $sellers)->get() as $key => $seller) {
            if($seller->user != null && $seller->user->shop != null){
                $total_sale = 0;
                foreach ($seller->user->products as $key => $product) {
                    $total_sale += $product->num_of_sale;
                }
                $array[$seller->id] = $total_sale;
            }
        }
        $sellers = $array;
        asort($sellers);
        $products = Product::where('published', 1)->get();
        exit;
        return view('frontend.best_sellers', compact('products','sellers'));
    }

    public function best_sellers(Request $request)
    {
        $sellers_array = $array = $sellers = array();
        if(isset($request->q)){
            $query = $request->q;
            $users = \App\Models\Shop::where('name', 'like', '%'.$request->q.'%')->get();
            foreach($users as $user){
                $sellers[] = $user->user_id;
            }
            $sellers = \App\Models\Seller::whereIn('user_id', $sellers)->get();
        }else{
            $sellers = \App\Models\Seller::all();
        }

        foreach ($sellers as $key => $seller) {
            if($seller->user != null && $seller->user->shop != null){
                $total_sale = 0;
                foreach ($seller->user->products as $key => $product) {
                    $total_sale += $product->num_of_sale;
                }
                $array[$seller->id] = $total_sale;
                $sellers_array[] = $seller->id;
            }
        }
        $sellers = filter_sellers(\App\Models\Seller::whereIn('id', $sellers_array))->paginate(12);
        $products = Product::where('published', 1)->get();
        return view('frontend.best_sellers', compact('products','sellers'));
    }
    public function today_deals(Request $request)
    {
        $products = Product::where('published', 1)->orderBy('num_of_sale', 'desc');
        $products = filter_products($products)->where('todays_deal', '1')->paginate(12)->appends(request()->query());
       // $products = filter_products(\App\Models\Product::where('published', 1)->orderBy('num_of_sale', 'desc')->get());
        return view('frontend.today_deals', compact('products'));
    }

    public function product_content(Request $request){
        $connector  = $request->connector;
        $selector   = $request->selector;
        $select     = $request->select;
        $type       = $request->type;
        productDescCache($connector,$selector,$select,$type);
    }

    public function home_settings(Request $request)
    {
        return view('home_settings.index');
    }

    public function top_10_settings(Request $request)
    {
        foreach (Category::all() as $key => $category) {
            if(in_array($category->id, $request->top_categories)){
                $category->top = 1;
                $category->save();
            }
            else{
                $category->top = 0;
                $category->save();
            }
        }

        foreach (Brand::all() as $key => $brand) {
            if(in_array($brand->id, $request->top_brands)){
                $brand->top = 1;
                $brand->save();
            }
            else{
                $brand->top = 0;
                $brand->save();
            }
        }

        flash(__('Top 10 categories and brands have been updated successfully'))->success();
        return redirect()->route('home_settings.index');
    }

    public function variant_price(Request $request)
    {
        $product = Product::find($request->id);
        $str = '';

        if($request->has('color')){
            $data['color'] = $request['color'];
            $str = Color::where('code', $request['color'])->first()->name;
        }

        foreach (json_decode(Product::find($request->id)->choice_options) as $key => $choice) {
            if($str != null){
                $str .= '-'.str_replace(' ', '', $request[$choice->name]);
            }
            else{
                $str .= str_replace(' ', '', $request[$choice->name]);
            }
        }

        if($str != null){
            $price = json_decode($product->variations)->$str->price;
        }
        else{
            $price = $product->unit_price;
        }

        //discount calculation
        $flash_deal = \App\Models\FlashDeal::where('status', 1)->first();
        if ($flash_deal != null && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <= $flash_deal->end_date && \App\Models\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first() != null) {
            $flash_deal_product = \App\Models\FlashDealProduct::where('flash_deal_id', $flash_deal->id)->where('product_id', $product->id)->first();
            if($flash_deal_product->discount_type == 'percent'){
                $price -= ($price*$flash_deal_product->discount)/100;
            }
            elseif($flash_deal_product->discount_type == 'amount'){
                $price -= $flash_deal_product->discount;
            }
        }
        else{
            if($product->discount_type == 'percent'){
                $price -= ($price*$product->discount)/100;
            }
            elseif($product->discount_type == 'amount'){
                $price -= $product->discount;
            }
        }

        if($product->tax_type == 'percent'){
            $price += ($price*$product->tax)/100;
        }
        elseif($product->tax_type == 'amount'){
            $price += $product->tax;
        }
        return single_price($price*$request->quantity);
    }
    
     public function contactus(){
        return view("frontend.contact-us");
    }

    public function faq(){
         $faqs = Faq::all();
        return view('frontend.faq-page', compact('faqs'));
        
    }
    public function apipage(){
        return view('frontend.api-page');
    }
    

    public function aboutus(){
         $abouts = Aboutus::all();
        return view('frontend.about-us', compact('abouts'));
        
    }
    
    public function terms_and_conditions(){
        return view('frontend.terms');
        
    }


    public function sellerpolicy(){
        return view("frontend.policies.sellerpolicy");
    }

    public function returnpolicy(){
        return view("frontend.policies.returnpolicy");
    }

    public function supportpolicy(){
        return view("frontend.policies.supportpolicy");
    }

    public function terms(){
        return view("frontend.policies.terms");
    }

    public function privacypolicy(){
        return view("frontend.policies.privacypolicy");
    }
}
