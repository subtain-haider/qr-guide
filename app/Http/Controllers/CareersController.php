<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Models\Careers;

use App\Models\Role;

use App\Models\Language;

use Session;

class CareersController extends Controller
{
    public function index()
    {
        $careers  = Careers::all();
        return view('careers.index', compact('careers'));
    }

	/**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        $languages = Language::all();

		$roles = Role::all();

        return view('careers.create', compact('languages', 'roles'));

    }

	/**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $career = new Careers;

        $career->title = $request->job_title;

        $career->role_id = $request->role;

        $career->job_type = $request->job_type;

        $career->location = $request->location;

		$career->education_level = $request->education_level;

		$career->language = $request->language;

		$career->experience = $request->experience;

		$career->experience_field = $request->experience_field;

		$career->details = $request->description;

		$career->salary = $request->salary;

		$career->salary_based_on = $request->salary_based_on;

		$career->joining = $request->joining;

		$career->publish_by = Auth::user()->id;

        if($career->save()){

            flash(__('Career has been inserted successfully'))->success();

            return redirect()->route('careers.index');

        }



        flash(__('Something went wrong'))->error();

        return back();

    }

	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $languages = Language::all();

		$roles = Role::all();

        $careers = Careers::findOrFail(decrypt($id));

        return view('careers.edit', compact('languages', 'roles', 'careers'));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $career = Careers::findOrFail($id);

        $career->title = $request->job_title;

        $career->role_id = $request->role;

        $career->job_type = $request->job_type;

        $career->location = $request->location;

		$career->education_level = $request->education_level;

		$career->language = $request->language;

		$career->experience = $request->experience;

		$career->experience_field = $request->experience_field;

		$career->details = $request->description;

		$career->salary = $request->salary;

		$career->salary_based_on = $request->salary_based_on;

		$career->joining = $request->joining;

		$career->publish_by = Auth::user()->id;
        if($career->save()){

            flash(__('Career has been updated successfully'))->success();

            return redirect()->route('careers.index');

        }

        flash(__('Something went wrong'))->error();
        return back();
    }

	/**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        $product = Careers::findOrFail($id);

        if(Careers::destroy($id)){

            flash(__('Career has been deleted successfully'))->success();

            return redirect()->route('careers.index');

        }

        else{

            flash(__('Something went wrong'))->error();

            return back();

        }

    }
}
