<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aboutus;

class AboutController extends Controller
{

   public function index()
    {
        $abouts = Aboutus::all();
        return view('aboutus.index', compact('abouts'));
    }




   public function create()
    {
		return view('aboutus.create');
    }





   public function store(Request $request)
    {
        $plan = new Aboutus;
        $plan->heading = $request->heading;
        $plan->content = $request->content;
       
        if($plan->save()){
            flash(__('Content has been inserted successfully'))->success();
            return redirect()->route('aboutus.index');
        }
		else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }




   public function edit($id)
    {
        $about = Aboutus::findOrFail(decrypt($id));
        return view('aboutus.edit', compact('about'));
    }



   public function update(Request $request, $id)
    {
        $plan = Aboutus::findOrFail($id);

        $plan->heading = $request->heading;
        $plan->content = $request->content;
        
        if($plan->save()){
            flash(__('Content has been updated successfully'))->success();
            return redirect()->route('aboutus.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }


   public function destroy($id)
    {
        if(Aboutus::destroy($id)){
            flash(__('Content has been deleted successfully'))->success();
            return redirect()->route('aboutus.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }




}

?>