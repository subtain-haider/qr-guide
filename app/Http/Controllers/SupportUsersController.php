<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Models\User;

use App\Models\SupportUsers;

use App\Models\Staff;

use Session;

class SupportUsersController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {
        
		$support = SupportUsers::all();
		
		return view('support.index', compact('support'));
	
	}
	


    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
		
		$staffs = Staff::all();

        return view('support.create', compact('staffs'));

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {
	
        $support = new SupportUsers;

        $support->staff_id = $request->staff;

        $support->role = $request->role;

        $support->message = $request->message;
		
		$support->phone_number = $request->phone;
		
		if($request->hasFile('image')){

			$support->profile_image = $request->image->store('frontend/images/whatsapp-profile');

        }
		
		if($support->save()){

            flash(__('Live Support User has been inserted successfully'))->success();

            return redirect()->route('support.index');

        }



        flash(__('Something went wrong'))->error();

        return back();

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)
    {
        $staffs = Staff::all();

        $support = SupportUsers::findOrFail(decrypt($id));
		
        return view('support.edit', compact('staffs', 'support'));
    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $support = SupportUsers::findOrFail($id);

        $support->staff_id = $request->staff;

        $support->role = $request->role;

        $support->message = $request->message;
		
		$support->phone_number = $request->phone;
		
		if($request->hasFile('image')){

			$support->profile_image = $request->image->store('frontend/images/whatsapp-profile');

        }
		
		if($support->save()){

            flash(__('Live Support User has been inserted successfully'))->success();

            return redirect()->route('support.index');

        }
		
		flash(__('Something went wrong'))->error();

        return back();

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        $support = SupportUsers::findOrFail($id);

        if(SupportUsers::destroy($id)){

            flash(__('Live Support User has been deleted successfully'))->success();

            return redirect()->route('support.index');

        }else{

            flash(__('Something went wrong'))->error();

            return back();

        }

    }

}

