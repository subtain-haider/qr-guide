<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InboxController;
use App\Http\Controllers\InboxsellerController;
use App\Http\Controllers\PaypalController;
use App\Http\Controllers\PackagePaymentController;
use App\Http\Controllers\StripePackageController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['domain' => '{slug}.myqrguide.com'], function()
{

    Route::get('/', function($slug)
    {
        return \App::call('App\Http\Controllers\HomeController@shop' , ['slug' => $slug]);
    });

});

// Route::group(['domain' => '{slug}.myqrguide.com'], function()
// {
//     Route::get('/', function ($slug) {
//       return \App::call('App\Http\Controllers\HomeController::class,shop' , ['slug' => $slug]);
//     });

// });
// Route::domain('{slug}.myqrguide.com')->group(function () {
// 	return 'Hello World';
// 	Route::get('/company/visit/{slug}', 'HomeController::class,shop')->name('company.visit');
// });
Route::get('/install', [App\Http\Controllers\ShopifyController::class, 'install']);
Route::get('/generate_token', [App\Http\Controllers\ShopifyController::class, 'generate_token']);
Route::get('/productsync/{id}', [App\Http\Controllers\ShopifyController::class, 'shopifyProductsSync']);

Route::resource('woocommerce', App\Http\Controllers\WoocommerceController::class);
Route::get('/woocommerce-productsync/{id}', [App\Http\Controllers\WoocommerceController::class, 'woocommerceProductsSync']);

Route::post('/get_discountbycoupon', [App\Http\Controllers\CouponController::class,'get_discountbycoupon'])->name('coupon.get_discountbycoupon');

Route::get('/viewcart', function () {
    return view('frontend.cart.cart');
});



// Auth::routes();
Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/inbox', [InboxController::class, 'index'])->name('inbox');
    Route::get('/inbox/{id}', [InboxController::class, 'show'])->name('inbox.show');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/inboxseller', [InboxsellerController::class, 'index'])->name('inboxseller');
    Route::get('/inboxseller/{id}', [InboxsellerController::class, 'show'])->name('inboxseller.show');
});

Route::get('user/register', [App\Http\Controllers\UserRegister::class, 'index']);
Route::get('user/register2', [App\Http\Controllers\UserRegister2::class, 'index']);




Route::post('user/register', [App\Http\Controllers\UserRegister::class,'register']);
Route::post('user/register2', [App\Http\Controllers\UserRegister2::class,'register2']);

Route::post('/language', [App\Http\Controllers\LanguageController::class,'changeLanguage'])->name('language.change');
Route::post('/currency', [App\Http\Controllers\CurrencyController::class,'changeCurrency'])->name('currency.change');

Route::get('/social-login/redirect/{provider}', [App\Http\Controllers\Auth\LoginController::class,'redirectToProvider'])->name('social.login');
Route::get('/social-login/{provider}/callback', [App\Http\Controllers\Auth\LoginController::class,'handleProviderCallback'])->name('social.callback');

Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class,'logout']);

Route::get('/', [App\Http\Controllers\HomeController::class,'index'])->name('home');
Route::get('pricing/', [App\Http\Controllers\HomeController::class,'pricing'])->name('pricing');
Route::get('/viewed/{pagelink}/{id}', [App\Http\Controllers\HomeController::class,'viewednotification'])->name('viewednotification');

Route::get('/users/login', [App\Http\Controllers\HomeController::class,'login'])->name('user.login');
Route::get('/users/registration', [App\Http\Controllers\HomeController::class,'registration'])->name('user.registration');
Route::post('/users/login', [App\Http\Controllers\HomeController::class,'user_login'])->name('user.login.submit');

Route::post('/register_application', [App\Http\Controllers\HomeController::class,'register_application'])->name('register_application');

Route::get('/product/{slug}', [App\Http\Controllers\HomeController::class,'product'])->name('product');
Route::get('/single-product/{slug}', [App\Http\Controllers\HomeController::class,'singleproduct'])->name('singleproduct');
Route::get('/products', [App\Http\Controllers\HomeController::class,'listing'])->name('products');

Route::post('/storecategories', [App\Http\Controllers\CategoryController::class,'storecategories'])->name('storecategories');
Route::post('/storesubcategories', [App\Http\Controllers\CategoryController::class,'storesubcategories'])->name('storesubcategories');
Route::post('/storesubsubcategories', [App\Http\Controllers\CategoryController::class,'storesubsubcategories'])->name('storesubsubcategories');

Route::post('/product/variant_price', [App\Http\Controllers\HomeController::class,'variant_price'])->name('products.variant_price');
Route::get('/company/visit/{slug}', [App\Http\Controllers\HomeController::class,'shop'])->name('company.visit');
Route::get('/company/{name}/{slug}', [App\Http\Controllers\HomeController::class,'custompages'])->name('company.custompages');
Route::get('/company/visit/{slug}', [App\Http\Controllers\HomeController::class,'shop'])->name('company.visit');
Route::get('/company/visit/{slug}/{type}', [App\Http\Controllers\HomeController::class,'filter_shop'])->name('company.visit.type');
Route::get('/suggessions/{slug}', [App\Http\Controllers\CompanyController::class,'company_suggessions'])->name('company_suggessions');
Route::post('/customerreplay', [App\Http\Controllers\CompanyController::class,'sendemailtocustomer'])->name('customerreplay');
Route::get('/suggessions/destroy/{id}', [App\Http\Controllers\CompanyController::class,'destroycomment'])->name('destroycomment');
Route::get('/warranty/{slug}', [App\Http\Controllers\HomeController::class,'company_warranty'])->name('company_warranty.visit');
Route::post('/warranty/{slug}/sumbit', [App\Http\Controllers\CompanyController::class,'verify_form_store'])->name('shop.verify.store');
Route::get('/warranty/claim/{slug}', [App\Http\Controllers\HomeController::class,'company_warranty_claim'])->name('company_warranty.claim');
Route::post('/warranty/claim/{slug}', [App\Http\Controllers\HomeController::class,'company_warranty_claim_submit'])->name('company_warranty_submits');
Route::get('/warranty/view/{id}', [App\Http\Controllers\CompanyController::class,'show_warranty_request'])->name('show_warranty_request.view');
Route::get('/warranties/{id}', [App\Http\Controllers\CompanyController::class,'warranties'])->name('company_warranties');
Route::get('/seller-questions/{slug}', [App\Http\Controllers\CompanyController::class,'seller_questions'])->name('seller_questions');
Route::post('/seller-questions-reply/', [App\Http\Controllers\CompanyController::class,'seller_questions_reply'])->name('seller_questions_reply');
Route::post('/seller-questions-status/', [App\Http\Controllers\CompanyController::class,'seller_questions_status'])->name('seller_questions_status');
Route::post('/warranty/message', [App\Http\Controllers\CompanyController::class,'warranty_message_update'])->name('warranty_message_update');
//submit question
Route::post('/company/question/sumbit', [App\Http\Controllers\CompanyController::class,'questionsubmit'])->name('company.question.submit');
Route::post('/company/reviews/sumbit', [App\Http\Controllers\CompanyController::class,'reviewssubmit'])->name('company.reviews.submit');
Route::post('/company/club/sumbit', [App\Http\Controllers\CompanyController::class,'clubsubmit'])->name('company.clubsubmit');
Route::post('/company/comments/sumbit', [App\Http\Controllers\CompanyController::class,'commentsubmit'])->name('company.commentsubmit');
Route::get('/club-info/', [App\Http\Controllers\CompanyController::class,'clubinfo'])->name('club_info');
Route::get('/club-info/{slug}', [App\Http\Controllers\CompanyController::class,'club_form'])->name('club_form');
Route::get('/selling-report/', [App\Http\Controllers\CompanyController::class,'sellingreport'])->name('sellingreport');
Route::get('/clients-report/', [App\Http\Controllers\CompanyController::class,'clientsreport'])->name('clientsreport');
Route::get('/export-tasks',  [App\Http\Controllers\CompanyController::class,'exportCsv'])->name('export-tasks');
Route::get('/club-info/destroy/{id}', [App\Http\Controllers\CompanyController::class,'destroyclub'])->name('destroyclub');
Route::get('/getreviewsbyrating/{slug}/{rating}', [App\Http\Controllers\HomeController::class,'getreviewsbyrating'])->name('getreviewsbyrating');
Route::get('/total-sales/', [App\Http\Controllers\CompanyController::class,'totalsales'])->name('total.sales');
Route::get('/sale/{id}/show', [App\Http\Controllers\CompanyController::class,'salesshow'])->name('saler.show');
Route::get('/sale/destroy/{id}', [App\Http\Controllers\CompanyController::class,'destroys'])->name('saledestroy');
// Route::get('/company/{slug}/warranty/', 'HomeController::class,company_warranty')->name('company.warranty.view');

Route::get('/faq-page', [App\Http\Controllers\HomeController::class,'faq'])->name('faq-page');
Route::get('/api-page', [App\Http\Controllers\HomeController::class,'apipage'])->name('api-page');
Route::get('/about-us', [App\Http\Controllers\HomeController::class,'aboutus'])->name('about-us');
Route::get('/contact-us', [App\Http\Controllers\HomeController::class,'contactus'])->name('contact-us');
Route::get('/terms-and-conditions', [App\Http\Controllers\HomeController::class,'terms_and_conditions'])->name('terms_and_conditions');


Route::get('/sellerpolicy', [App\Http\Controllers\HomeController::class,'sellerpolicy'])->name('sellerpolicy');
Route::get('/returnpolicy', [App\Http\Controllers\HomeController::class,'returnpolicy'])->name('returnpolicy');
Route::get('/supportpolicy', [App\Http\Controllers\HomeController::class,'supportpolicy'])->name('supportpolicy');
Route::get('/terms', [App\Http\Controllers\HomeController::class,'terms'])->name('terms');
Route::get('/privacypolicy', [App\Http\Controllers\HomeController::class,'privacypolicy'])->name('privacypolicy');

Route::get('/search', [App\Http\Controllers\HomeController::class,'search'])->name('search'); 
Route::get('/search?q={search}', [App\Http\Controllers\HomeController::class,'search'])->name('suggestion.search');
Route::get('/view-pdf/{slug}', [App\Http\Controllers\HomeController::class,'pdf'])->name('pdf.view');
Route::get('/search/mini-site', [App\Http\Controllers\HomeController::class,'searchminisite'])->name('search.minisite'); 
Route::get('/search/products', [App\Http\Controllers\HomeController::class,'searchproducts'])->name('search.products'); 

Route::group(['middleware' => ['user', 'verified']], function(){

	Route::get('/dashboard', [App\Http\Controllers\HomeController::class,'dashboard'])->name('dashboard'); 
	Route::get('/home', [App\Http\Controllers\HomeController::class,'dashboard'])->name('dashboard1');
	Route::get('/profile', [App\Http\Controllers\HomeController::class,'profile'])->name('profile');
	Route::post('/customer/update-profile', [App\Http\Controllers\HomeController::class,'customer_update_profile'])->name('customer.profile.update');
	Route::post('/seller/update-profile', [App\Http\Controllers\HomeController::class,'seller_update_profile'])->name('seller.profile.update');
	Route::get('/marketing', [App\Http\Controllers\HomeController::class,'marketing'])->name('marketing');
	Route::post('/seller/email-send', [App\Http\Controllers\HomeController::class,'seller_email_send'])->name('seller.send.email');

	Route::resource('purchase_history',App\Http\Controllers\PurchaseHistoryController::class);
	Route::post('/purchase_history/details', [App\Http\Controllers\PurchaseHistoryController::class,'purchase_history_details'])->name('purchase_history.details');
	Route::get('/purchase_history/destroy/{id}', [App\Http\Controllers\PurchaseHistoryController::class,'destroy'])->name('purchase_history.destroy');
    Route::get('supportticket',[App\Http\Controllers\SupportTicketController::class,'index']);
	Route::resource('support_ticket',App\Http\Controllers\SupportTicketController::class);
	Route::post('support_ticket/reply',[App\Http\Controllers\SupportTicketController::class,'seller_store'])->name('support_ticket.seller_store');
    
    Route::get('/customers/tickets/', [App\Http\Controllers\SupportTicketController::class,'customers_tickets'])->name('support_ticket.customerstickets');
});
Route::get('/support_ticket/changestatus/{id}', [App\Http\Controllers\SupportTicketController::class,'changestatus'])->name('support_ticket.changestatus');
Route::get('contact_support',[App\Http\Controllers\SupportTicketController::class,'contact_support'])->name('customer.contact_support');
Route::get('/cart', [App\Http\Controllers\CartController::class,'index'])->name('cart');
Route::post('/cart/nav-cart-items', [App\Http\Controllers\CartController::class,'updateNavCart'])->name('cart.nav_cart');
Route::post('/cart/show-cart-modal', [App\Http\Controllers\CartController::class,'showCartModal'])->name('cart.showCartModal');
Route::post('/cart/addtocart', [App\Http\Controllers\CartController::class,'addToCart'])->name('cart.addToCart');
Route::post('/cart/removeFromCart', [App\Http\Controllers\CartController::class,'removeFromCart'])->name('cart.removeFromCart');
Route::post('/cart/updateQuantity', [App\Http\Controllers\CartController::class,'updateQuantity'])->name('cart.updateQuantity');
Route::post('/cart/updateShippingMethod', [App\Http\Controllers\CartController::class,'updateShippingMethod'])->name('cart.updateShippingMethod');
// Route::post('/cart/updateShippingMethod', 'CartController::class,updateShippingMethod')->name('cart.updateShippingMethod');

Route::get('/plan/{id}', [App\Http\Controllers\CartController::class,'buy_plan'])->name('buy.plan');

Route::post('/checkout/payment', [App\Http\Controllers\CheckoutController::class,'checkout'])->name('payment.checkout');
Route::get('/checkout', [App\Http\Controllers\CheckoutController::class,'get_shipping_info'])->name('checkout.shipping_info');
Route::post('/checkout/payment_select', [App\Http\Controllers\CheckoutController::class,'store_shipping_info'])->name('checkout.store_shipping_infostore');
Route::get('/checkout/payment_select', [App\Http\Controllers\CheckoutController::class,'get_payment_info'])->name('checkout.payment_info');
Route::post('/checkout/apply_coupon_code', [App\Http\Controllers\CheckoutController::class,'apply_coupon_code'])->name('checkout.apply_coupon_code');
Route::post('/checkout/apply_coupon', [App\Http\Controllers\CheckoutController::class,'apply_coupon'])->name('checkout.apply_coupon');


Route::get('paywithpaypal', [PaypalController::class,'payWithPaypal'])->name('paywithpaypal');
Route::get('payment', [PaypalController::class,'payment'])->name('payment');
Route::get('status', [PaypalController::class,'getPaymentStatus'])->name('status');


//Payment for Packages

Route::get('package/payment', [PackagePaymentController::class,'payment'])->name('package.payment');
Route::get('package/status', [PackagePaymentController::class,'getPaymentStatus'])->name('package.status');


// Route::get('package/payment', [StripePackageController::class,'payment'])->name('stripepackage.payment');
// Route::get('package/status', [StripePackageController::class,'getPaymentStatus'])->name('stripepackage.status');

Route::get('package/stripe', [StripePackageController::class,'stripe']);
Route::post('package/stripe', [StripePackageController::class,'stripePost'])->name('packagestripe.post');

//Paypal START
// Route::post('payment', [App\Http\Controllers\PaypalController::class,'payment'])->name('payment');
// Route::get('cancel', [App\Http\Controllers\PaypalController::class,'cancel'])->name('payment.cancel');
// Route::get('payment/success', [App\Http\Controllers\PaypalController::class,'success'])->name('payment.success');


// Route::get('/paypal/payment/done', [App\Http\Controllers\PaypalController::class,'getDone'])->name('payment.done');
// Route::get('/paypal/payment/cancel', [App\Http\Controllers\PaypalController::class,'getCancel'])->name('payment.cancel');

// Route::get('payment', [PaypalPaymentController::class,'payment'])->name('payment');
// Route::get('cancel', [PaypalPaymentController::class,'cancel'])->name('payment.cancel');
// Route::get('payment/success', [PaypalPaymentController::class,'success'])->name('payment.success');

//Paypal END

// SSLCOMMERZ Start
Route::get('/sslcommerz/pay', [App\Http\Controllers\PublicSslCommerzPaymentController::class,'index']);
Route::POST('/sslcommerz/success', [App\Http\Controllers\PublicSslCommerzPaymentController::class,'success']);
Route::POST('/sslcommerz/fail', [App\Http\Controllers\PublicSslCommerzPaymentController::class,'fail']);
Route::POST('/sslcommerz/cancel', [App\Http\Controllers\PublicSslCommerzPaymentController::class,'cancel']);
Route::POST('/sslcommerz/ipn', [App\Http\Controllers\PublicSslCommerzPaymentController::class,'ipn']);
//SSLCOMMERZ END

//Stipe Start
Route::get('stripe', [App\Http\Controllers\StripePaymentController::class,'stripe']);
Route::post('stripe', [App\Http\Controllers\StripePaymentController::class,'stripePost'])->name('stripe.post');
//Stripe END
Route::group(['prefix' =>'seller', 'middleware' => ['seller', 'verified']], function(){
	Route::get('/products', [App\Http\Controllers\HomeController::class,'seller_product_list'])->name('seller.products');
	Route::get('/guides', [App\Http\Controllers\HomeController::class,'seller_guide_list'])->name('seller.guides');
	Route::get('/product/upload', [App\Http\Controllers\HomeController::class,'show_product_upload_form'])->name('seller.products.upload');
	Route::get('/product/{id}/edit', [App\Http\Controllers\HomeController::class,'show_product_edit_form'])->name('seller.products.edit');
	Route::get('/package-info', [App\Http\Controllers\PricingController::class,'packageinfo'])->name('packageinfo');

	//Guide upload
	

    
	Route::get('/guide/upload', [App\Http\Controllers\HomeController::class,'show_guide_upload_form'])->name('seller.guide.upload');

	Route::resource('company', App\Http\Controllers\CompanyController::class);
	Route::get('/mini-sites', [App\Http\Controllers\CompanyController::class,'index'])->name('company.index');
	Route::get('/mini-site/create', [App\Http\Controllers\CompanyController::class,'create'])->name('company.create');
	Route::post('/mini-site/confirmation', [App\Http\Controllers\CompanyController::class,'confirmation'])->name('company.confirmation');
	Route::post('/mini-site/upload', [App\Http\Controllers\CompanyController::class,'store'])->name('company.upload');
	Route::get('/mini-sites/{id}/edit', [App\Http\Controllers\CompanyController::class,'edit'])->name('company.edit');
	Route::post('/mini-sites/{id}/update', [App\Http\Controllers\CompanyController::class,'update'])->name('company.update');
	Route::get('/mini-sites/{id}/delete', [App\Http\Controllers\CompanyController::class,'destroy'])->name('company.delete');

	Route::get('minisite/{id}', [App\Http\Controllers\CompanyController::class,'qr_codes_download'])->name('company.qrcode.download');
	Route::get('allqr_codes_download/{id}', [App\Http\Controllers\CompanyController::class,'allqr_codes_download'])->name('company.allqrcode.download');
	
    Route::post('/slug/{id}', [App\Http\Controllers\CompanyController::class, 'slugchange'])->name('slug.change');
	Route::resource('/question', App\Http\Controllers\QuestionController::class);
	Route::get('/question/create/{id}', [App\Http\Controllers\QuestionController::class,'create'])->name('answer');
	Route::get('coupon', [App\Http\Controllers\CouponController::class,'sellercoupon'])->name('sellercoupon');
	Route::get('/coupon/create', [App\Http\Controllers\CouponController::class,'sellercouponcreate'])->name('sellercouponcreate');
	Route::get('/coupon/{id}', [App\Http\Controllers\CouponController::class,'sellercouponedit'])->name('sellercouponedit');
	Route::post('/coupon/get_form', [App\Http\Controllers\CouponController::class,'seller_get_coupon_form'])->name('coupon.seller_get_coupon_form');
	Route::post('/coupon/store', [App\Http\Controllers\CouponController::class,'seller_coupon_store'])->name('seller_coupon.store');
	Route::post('/coupon/edit/{id}', [App\Http\Controllers\CouponController::class,'seller_coupon_edit'])->name('seller_coupon.edit');
	Route::post('/coupon/get_form_edit', [App\Http\Controllers\CouponController::class,'seller_get_coupon_form_edit'])->name('coupon.seller_get_coupon_form_edit');
	Route::get('/coupon/destroy/{id}', [App\Http\Controllers\CouponController::class,'seller_destroy'])->name('coupon.sellerdestroy');
	
});
Route::group(['middleware' => ['auth']], function(){
	///// Chat module
	// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
	Route::post('warranty_invoice_download', [App\Http\Controllers\CompanyController::class,'warranty_invoice_download'])->name('company.warranty.download');
    Route::get('/inbox', [App\Http\Controllers\InboxController::class,'index'])->name('inbox');
    Route::get('/inbox/{id}', [App\Http\Controllers\InboxController::class,'show'])->name('inbox.show');
	///// Chat module
	
	    Route::get('/inboxseller', [App\Http\Controllers\InboxsellerController::class,'index'])->name('inboxseller');
    Route::get('/inboxseller/{id}', [App\Http\Controllers\InboxsellerController::class,'show'])->name('inboxseller.show');


    Route::resource('shopify',App\Http\Controllers\ShopifyController::class);

	Route::post('/question/create-question', [App\Http\Controllers\QuestionController::class,'store_question'])->name('question.store_question');
	Route::post('/products/requestus/',[App\Http\Controllers\ProductController::class,'requestus'])->name('products.requestus');
	Route::post('/products/guiderequest/',[App\Http\Controllers\ProductController::class,'guiderequest'])->name('products.guiderequest');
	

	Route::post('/products/store/',[App\Http\Controllers\ProductController::class,'store'])->name('products.store');
	Route::post('/products/update/{id}',[App\Http\Controllers\ProductController::class,'update'])->name('products.update');
	Route::get('/products/destroy/{id}', [App\Http\Controllers\ProductController::class,'destroy'])->name('products.destroy');
	Route::get('/products/duplicate/{id}', [App\Http\Controllers\ProductController::class,'duplicate'])->name('products.duplicate');
	Route::post('/products/sku_combination', [App\Http\Controllers\ProductController::class,'sku_combination'])->name('products.sku_combination');
	Route::post('/products/sku_combination_edit', [App\Http\Controllers\ProductController::class,'sku_combination_edit'])->name('products.sku_combination_edit');
	Route::post('/products/featured', [App\Http\Controllers\ProductController::class,'updateFeatured'])->name('products.featured');
	Route::post('/products/published', [App\Http\Controllers\ProductController::class,'updatePublished'])->name('products.published');

	Route::get('invoice/customer/{order_id}', [App\Http\Controllers\InvoiceController::class,'customer_invoice_download'])->name('customer.invoice.download');
	Route::get('invoice/seller/{order_id}', [App\Http\Controllers\InvoiceController::class,'seller_invoice_download'])->name('seller.invoice.download');

	Route::post('/subcategories/get_subcategories_by_category', [App\Http\Controllers\SubCategoryController::class,'get_subcategories_by_category'])->name('subcategories.get_subcategories_by_category');
	Route::post('/subsubcategories/get_subsubcategories_by_subcategory', [App\Http\Controllers\SubSubCategoryController::class,'get_subsubcategories_by_subcategory'])->name('subsubcategories.get_subsubcategories_by_subcategory');
	Route::post('/subsubcategories/get_brands_by_subsubcategory', [App\Http\Controllers\SubSubCategoryController::class,'get_brands_by_subsubcategory'])->name('subsubcategories.get_brands_by_subsubcategory');
	Route::post('/subsubcategories/get_products_by_subsubcategory', [App\Http\Controllers\SubSubCategoryController::class,'get_products_by_subsubcategory'])->name('subsubcategories.get_products_by_subsubcategory');
	Route::get('/search?subsubcategory_id={subsubcategory_id}', [App\Http\Controllers\HomeController::class,'search'])->name('products.subsubcategory');
	Route::get('/brands', [App\Http\Controllers\HomeController::class,'all_brands'])->name('brands.all');
	Route::get('/categories', [App\Http\Controllers\HomeController::class,'all_categories'])->name('categories.all');
	Route::get('/search?category_id={category_id}', [App\Http\Controllers\HomeController::class,'search'])->name('products.category');
	Route::get('/search?subcategory_id={subcategory_id}', [App\Http\Controllers\HomeController::class,'search'])->name('products.subcategory');
	Route::get('/search?subsubcategory_id={subsubcategory_id}', [App\Http\Controllers\HomeController::class,'search'])->name('products.subsubcategory');

	Route::resource('orders',App\Http\Controllers\OrderController::class);
	Route::get('/orders/destroy/{id}', [App\Http\Controllers\OrderController::class,'destroy'])->name('orders.destroy');
	Route::post('/orders/details', [App\Http\Controllers\OrderController::class,'order_details'])->name('orders.details');
	Route::post('/orders/update_delivery_status', [App\Http\Controllers\OrderController::class,'update_delivery_status'])->name('orders.update_delivery_status');
	Route::post('/orders/seller_update_delivery_status', [App\Http\Controllers\OrderController::class,'seller_update_delivery_status'])->name('orders.seller_update_delivery_status');
		Route::post('/orders/seller_update_payment_status', [App\Http\Controllers\OrderController::class,'seller_update_payment_status'])->name('orders.seller_update_payment_status');
	Route::post('/orders/update_payment_status', [App\Http\Controllers\OrderController::class,'update_payment_status'])->name('orders.update_payment_status');

	Route::resource('/reviews', App\Http\Controllers\ReviewController::class);
	Route::get('/sellerreviews', [App\Http\Controllers\ReviewController::class,'seller_reviewss'])->name('seller.reviews');
	Route::post('/sellerupdatePublished', [App\Http\Controllers\ReviewController::class,'sellerupdatePublished'])->name('reviews.sellerupdatePublished');


	// Route::resource('company', 'CompanyController');

	Route::get('/seller/company-warranty', [App\Http\Controllers\CompanyController::class,'warranty_form'])->name('company.warranty');
	Route::post('/seller/company-warranty', [App\Http\Controllers\CompanyController::class,'warranty_form_update'])->name('company.warranty.update');
});


Route::get('/track_your_order', [App\Http\Controllers\HomeController::class,'trackOrder'])->name('orders.track');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Admin Routes
Route::get('/admin', [App\Http\Controllers\HomeController::class,'admin_dashboard'])->name('admin.dashboard')->middleware('admin');
Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'admin']], function(){
	Route::get('/activity-log', [App\Http\Controllers\HomeController::class,'logActivity'])->name('logActivity');

	Route::resource('categories',App\Http\Controllers\CategoryController::class);
	Route::get('/categories/destroy/{id}', [App\Http\Controllers\CategoryController::class,'destroy'])->name('categories.destroy');
	Route::post('/categories/featured', [App\Http\Controllers\CategoryController::class,'updateFeatured'])->name('categories.featured');

	Route::resource('subcategories',App\Http\Controllers\SubCategoryController::class);
	Route::get('/subcategories/destroy/{id}', [App\Http\Controllers\SubCategoryController::class,'destroy'])->name('subcategories.destroy');

	Route::resource('subsubcategories',App\Http\Controllers\SubSubCategoryController::class);
	Route::get('/subsubcategories/destroy/{id}', [App\Http\Controllers\SubSubCategoryController::class,'destroy'])->name('subsubcategories.destroy');

	Route::resource('brands',App\Http\Controllers\BrandController::class);
	Route::post('/brands/ajax/', [App\Http\Controllers\BrandController::class,'ajax'])->name('brands.ajax');
	Route::get('/brands/destroy/{id}', [App\Http\Controllers\BrandController::class,'destroy'])->name('brands.destroy');

	Route::get('/products/admin',[App\Http\Controllers\ProductController::class,'admin_products'])->name('products.admin');
	Route::get('/products/seller',[App\Http\Controllers\ProductController::class,'seller_products'])->name('products.seller');
	Route::get('/products/create',[App\Http\Controllers\ProductController::class,'create'])->name('products.create');
	Route::get('/products/admin/{id}/edit',[App\Http\Controllers\ProductController::class,'admin_product_edit'])->name('products.admin.edit');
	Route::get('/products/seller/{id}/edit',[App\Http\Controllers\ProductController::class,'seller_product_edit'])->name('products.seller.edit');
	Route::post('/products/todays_deal', [App\Http\Controllers\ProductController::class,'updateTodaysDeal'])->name('products.todays_deal');
	Route::post('/products/get_products_by_subsubcategory', [App\Http\Controllers\ProductController::class,'get_products_by_subsubcategory'])->name('products.get_products_by_subsubcategory');

	Route::resource('sellers',App\Http\Controllers\SellerController::class);
	Route::get('/sellers/filter/{type}', [App\Http\Controllers\SellerController::class,'index'])->name('sellers.filter');
	Route::get('/sellers/destroy/{id}', [App\Http\Controllers\SellerController::class,'destroy'])->name('sellers.destroy');
	Route::get('/sellers/seller-details/{id}', [App\Http\Controllers\SellerController::class,'seller_details'])->name('sellers.seller_details');
	Route::get('/sellers/view/{id}/verification', [App\Http\Controllers\SellerController::class,'show_verification_request'])->name('sellers.show_verification_request');
	Route::get('/sellers/approve/{id}', [App\Http\Controllers\SellerController::class,'approve_seller'])->name('sellers.approve');
	Route::get('/sellers/reject/{id}', [App\Http\Controllers\SellerController::class,'reject_seller'])->name('sellers.reject');
	Route::post('/sellers/payment_modal', [App\Http\Controllers\SellerController::class,'payment_modal'])->name('sellers.payment_modal');
	Route::post('/sellers/payment', [App\Http\Controllers\SellerController::class,'payment_by_order'])->name('sellers.payment_by_order');

	Route::resource('customers',App\Http\Controllers\CustomerController::class);
	Route::get('/customers/filter/{type}', [App\Http\Controllers\CustomerController::class,'index'])->name('customers.custom');
	Route::get('/customers/destroy/{id}', [App\Http\Controllers\CustomerController::class,'destroy'])->name('customers.destroy');
	Route::post('/customers/assignplan', [App\Http\Controllers\CustomerController::class,'assignplan'])->name('customers.assignplan');

	Route::get('/newsletter', [App\Http\Controllers\NewsletterController::class,'index'])->name('newsletters.index');
	Route::post('/newsletter/send', [App\Http\Controllers\NewsletterController::class,'send'])->name('newsletters.send');

	Route::resource('profile',App\Http\Controllers\ProfileController::class);

	Route::get('/Customers', [App\Http\Controllers\SellerController::class,'customers_report'])->name('advertiser_report.customers');
	Route::get('/seller', [App\Http\Controllers\SellerController::class,'advertiser_report'])->name('advertiser_report.index');
	Route::get('/InviteSellers', [App\Http\Controllers\SellerController::class,'invite_seller'])->name('advertiser_share_link.invite_seller');
	Route::get('/Referrals', [App\Http\Controllers\SellerController::class,'referrals'])->name('advertiser.referrals');
	Route::get('/InviteCustomers', [App\Http\Controllers\SellerController::class,'invite_customer'])->name('advertiser_share_link.invite_customers');
	Route::get('/PaymentRequest', [App\Http\Controllers\SellerController::class,'payment_requests'])->name('advertiser.payment_requests');
	Route::get('/SellerPointsWithdraw', [App\Http\Controllers\SellerController::class,'seller_points_withdraw'])->name('advertiser.seller_payment_request');
	Route::get('/CustomerPointsWithdraw', [App\Http\Controllers\SellerController::class,'customer_points_withdraw'])->name('advertiser.customer_payment_request');
	Route::get('/profile/view', [App\Http\Controllers\ProfileController::class,'profile_view'])->name('profile.view');
	Route::get('/currencycreate',[App\Http\Controllers\BusinessSettingsController::class,'currencycreate'])->name('business_settings.currencycreate');
	Route::post('/currency/currencystore', [App\Http\Controllers\BusinessSettingsController::class,'currencystore'])->name('currency.currencystore');
	Route::get('/countrycreate',[App\Http\Controllers\BusinessSettingsController::class,'countrycreate'])->name('business_settings.countrycreate');
	Route::post('/countrystore', [App\Http\Controllers\CountryController::class,'countrystore'])->name('country.countrystore');
	Route::post('/business-settings/update', [App\Http\Controllers\BusinessSettingsController::class,'update'])->name('business_settings.update');
	Route::post('/business-settings/update/activation', [App\Http\Controllers\BusinessSettingsController::class,'updateActivationSettings'])->name('business_settings.update.activation');
	Route::get('/activation', [App\Http\Controllers\BusinessSettingsController::class,'activation'])->name('activation.index');
	Route::get('/payment-method', [App\Http\Controllers\BusinessSettingsController::class,'payment_method'])->name('payment_method.index');
	Route::get('/payment-types', [App\Http\Controllers\BusinessSettingsController::class,'payment_types'])->name('payment_method.payment_types');
	Route::get('/reward_settings', [App\Http\Controllers\BusinessSettingsController::class,'reward_settings'])->name('rewards.rewards_settings');
	Route::post('/reward_settings/update_points_configuration', [App\Http\Controllers\BusinessSettingsController::class,'update_points_configuration'])->name('rewards.update_points_configuration');
	Route::post('/reward_settings/update_profit_configuration', [App\Http\Controllers\BusinessSettingsController::class,'update_profit_configuration'])->name('rewards.update_profit_configuration');
	Route::get('/create-payment-types', [App\Http\Controllers\BusinessSettingsController::class,'create_payment_type'])->name('payment_method.create_payment_type');
	Route::post('/payment-types/store_payment_type', [App\Http\Controllers\BusinessSettingsController::class,'store_payment_type'])->name('payment_method.store_payment_type');
	Route::get('/shipping-method', [App\Http\Controllers\BusinessSettingsController::class,'shipping_method'])->name('shipping_method.index');
	Route::get('/social-login', [App\Http\Controllers\BusinessSettingsController::class,'social_login'])->name('social_login.index');
	Route::get('/smtp-settings', [App\Http\Controllers\BusinessSettingsController::class,'smtp_settings'])->name('smtp_settings.index');
	Route::get('/google-analytics', [App\Http\Controllers\BusinessSettingsController::class,'google_analytics'])->name('google_analytics.index');
	Route::get('/facebook-chat', [App\Http\Controllers\BusinessSettingsController::class,'facebook_chat'])->name('facebook_chat.index');
	Route::post('/env_key_update', [App\Http\Controllers\BusinessSettingsController::class,'env_key_update'])->name('env_key_update.update');
	Route::post('/payment_method_update', [App\Http\Controllers\BusinessSettingsController::class,'payment_method_update'])->name('payment_method.update');
	Route::post('/shipping_method_update', [App\Http\Controllers\BusinessSettingsController::class,'shipping_method_update'])->name('shipping_method.update');
	Route::post('/google_analytics', [App\Http\Controllers\BusinessSettingsController::class,'google_analytics_update'])->name('google_analytics.update');
	Route::post('/facebook_chat', [App\Http\Controllers\BusinessSettingsController::class,'facebook_chat_update'])->name('facebook_chat.update');
	Route::get('/currency', [App\Http\Controllers\CurrencyController::class,'currency'])->name('currency.index');
	Route::get('/country', [App\Http\Controllers\CountryController::class,'country'])->name('country.index');
    Route::post('/currency/update', [App\Http\Controllers\CurrencyController::class,'updateCurrency'])->name('currency.update');
    Route::post('/your-currency/update', [App\Http\Controllers\CurrencyController::class,'updateYourCurrency'])->name('your_currency.update');
	Route::post('/country/update', [App\Http\Controllers\CountryController::class,'updateCountry'])->name('country.update');
    Route::post('/your-country/update', [App\Http\Controllers\CountryController::class,'updateYourCountry'])->name('your_country.update');
	Route::post('/country/cities', [App\Http\Controllers\CountryController::class,'cities'])->name('country.cities');
	Route::get('/verification/form', [App\Http\Controllers\BusinessSettingsController::class,'seller_verification_form'])->name('seller_verification_form.index');
	Route::post('/verification/form', [App\Http\Controllers\BusinessSettingsController::class,'seller_verification_form_update'])->name('seller_verification_form.update');
	Route::get('/vendor_commission', [App\Http\Controllers\BusinessSettingsController::class,'vendor_commission'])->name('business_settings.vendor_commission');
	Route::post('/vendor_commission_update', [App\Http\Controllers\BusinessSettingsController::class,'vendor_commission_update'])->name('business_settings.vendor_commission.update');

	Route::resource('/languages', App\Http\Controllers\LanguageController::class);
	Route::post('/languages/update_rtl_status', [App\Http\Controllers\LanguageController::class,'update_rtl_status'])->name('languages.update_rtl_status');
	Route::get('/languages/destroy/{id}', [App\Http\Controllers\LanguageController::class,'destroy'])->name('languages.destroy');
	Route::get('/languages/{id}/edit', [App\Http\Controllers\LanguageController::class,'edit'])->name('languages.edit');
	Route::post('/languages/{id}/update', [App\Http\Controllers\LanguageController::class,'update'])->name('languages.update');
	Route::post('/languages/key_value_store', [App\Http\Controllers\LanguageController::class,'key_value_store'])->name('languages.key_value_store');

	Route::get('/frontend_settings/home', [App\Http\Controllers\HomeController::class,'home_settings'])->name('home_settings.index');
	Route::get('/warranties', [App\Http\Controllers\HomeController::class,'warranties'])->name('warranties');
	Route::get('/warranties/view/{id}', [App\Http\Controllers\CompanyController::class,'show_warranty_request'])->name('show_warranty_request');
	Route::post('/frontend_settings/home/top_10', [App\Http\Controllers\HomeController::class,'top_10_settings'])->name('top_10_settings.store');
	Route::get('/sellerpolicy/{type}', [App\Http\Controllers\PolicyController::class,'index'])->name('sellerpolicy.index');
	Route::get('/returnpolicy/{type}', [App\Http\Controllers\PolicyController::class,'index'])->name('returnpolicy.index');
	Route::get('/supportpolicy/{type}', [App\Http\Controllers\PolicyController::class,'index'])->name('supportpolicy.index');
	Route::get('/terms/{type}', [App\Http\Controllers\PolicyController::class,'index'])->name('terms.index');
	Route::get('/privacypolicy/{type}', [App\Http\Controllers\PolicyController::class,'index'])->name('privacypolicy.index');

	//Policy Controller
	Route::post('/policies/store', [App\Http\Controllers\PolicyController::class,'store'])->name('policies.store');

	Route::group(['prefix' => 'frontend_settings'], function(){
		Route::resource('sliders', App\Http\Controllers\SliderController::class);
	    Route::get('/sliders/destroy/{id}', [App\Http\Controllers\SliderController::class,'destroy'])->name('sliders.destroy');

		Route::resource('home_banners', App\Http\Controllers\BannerController::class);
		Route::get('/home_banners/create/{position}', [App\Http\Controllers\BannerController::class,'create'])->name('home_banners.create');
		Route::post('/home_banners/update_status', [App\Http\Controllers\BannerController::class,'update_status'])->name('home_banners.update_status');
	    Route::get('/home_banners/destroy/{id}', [App\Http\Controllers\BannerController::class,'destroy'])->name('home_banners.destroy');

		Route::resource('home_categories', App\Http\Controllers\HomeCategoryController::class);
	    Route::get('/home_categories/destroy/{id}', [App\Http\Controllers\HomeCategoryController::class,'destroy'])->name('home_categories.destroy');
		Route::post('/home_categories/update_status', [App\Http\Controllers\HomeCategoryController::class,'update_status'])->name('home_categories.update_status');
		Route::post('/home_categories/get_subsubcategories_by_category', [App\Http\Controllers\HomeCategoryController::class,'getSubSubCategories'])->name('home_categories.get_subsubcategories_by_category');

		Route::resource('advertising', App\Http\Controllers\AdvertisingController::class);
		Route::get('/advertising', [App\Http\Controllers\AdvertisingController::class,'index'])->name('advertising.index');
		Route::get('/advertising/create', [App\Http\Controllers\AdvertisingController::class,'create'])->name('advertising.create');
		Route::post('/advertising', [App\Http\Controllers\AdvertisingController::class,'storeAdvertising'])->name('advertising.store');
		Route::post('/advertising/update_status',[App\Http\Controllers\AdvertisingController::class,'update_status'])->name('advertising.update_status');
		Route::post('/advertising/update/{id}', [App\Http\Controllers\AdvertisingController::class,'update'])->name('advertising.update');
		Route::get('/advertising/edit/{id}', [App\Http\Controllers\AdvertisingController::class,'edit'])->name('advertising.edit');
		Route::get('/advertising/destroy/{id}', [App\Http\Controllers\AdvertisingController::class,'destroy'])->name('advertising.destroy');
	});

	Route::resource('pricing', App\Http\Controllers\PricingController::class);
	Route::get('/pricing/destroy/{id}', [App\Http\Controllers\PricingController::class,'destroy'])->name('pricing.destroy');



Route::resource('requestus', App\Http\Controllers\RequestusController::class);
	Route::get('/requestus/destroy/{id}', [App\Http\Controllers\RequestusController::class,'destroy'])->name('requestus.destroy');
	

	Route::resource('guiderequest', App\Http\Controllers\GuiderequestController::class);
	Route::get('/guiderequest/destroy/{id}', [App\Http\Controllers\GuiderequestController::class,'destroy'])->name('guiderequest.destroy');

	Route::resource('faq', App\Http\Controllers\FaqController::class);
	Route::get('/faq/destroy/{id}', [App\Http\Controllers\FaqController::class,'destroy'])->name('faq.destroy');

		Route::resource('aboutus', App\Http\Controllers\AboutController::class);
	Route::get('/aboutus/destroy/{id}', [App\Http\Controllers\AboutController::class,'destroy'])->name('aboutus.destroy');

	Route::resource('homediv', App\Http\Controllers\HomedivController::class);
	Route::get('/homediv/destroy/{id}', [App\Http\Controllers\HomedivController::class,'destroy'])->name('homediv.destroy');

	Route::resource('roles', App\Http\Controllers\RoleController::class);
    Route::get('/roles/destroy/{id}', [App\Http\Controllers\RoleController::class,'destroy'])->name('roles.destroy');

    Route::resource('staffs', App\Http\Controllers\StaffController::class);
    Route::get('/staffs/destroy/{id}', [App\Http\Controllers\StaffController::class,'destroy'])->name('staffs.destroy');

	Route::resource('flash_deals', App\Http\Controllers\FlashDealController::class);
    Route::get('/flash_deals/destroy/{id}', [App\Http\Controllers\FlashDealController::class,'destroy'])->name('flash_deals.destroy');
	Route::post('/flash_deals/update_status', [App\Http\Controllers\FlashDealController::class,'update_status'])->name('flash_deals.update_status');
	Route::post('/flash_deals/product_discount', [App\Http\Controllers\FlashDealController::class,'product_discount'])->name('flash_deals.product_discount');
	Route::post('/flash_deals/product_discount_edit', [App\Http\Controllers\FlashDealController::class,'product_discount_edit'])->name('flash_deals.product_discount_edit');

	Route::get('/orders', [App\Http\Controllers\OrderController::class,'admin_orders'])->name('orders.index.admin');
	Route::get('/orders/{id}/show', [App\Http\Controllers\OrderController::class,'show'])->name('orders.show');
	Route::get('/sales/{id}/show', [App\Http\Controllers\OrderController::class,'sales_show'])->name('sales.show');
	Route::get('/orders/destroy/{id}', [App\Http\Controllers\OrderController::class,'destroy'])->name('orders.destroy');
	Route::get('/sales', [App\Http\Controllers\OrderController::class,'sales'])->name('sales.index');
	Route::get('/sales/seller-sales', [App\Http\Controllers\OrderController::class,'seller_sales'])->name('sales.seller_sales');
	Route::post('/sales/pay_seller', [App\Http\Controllers\OrderController::class,'pay_seller'])->name('sales.pay_seller');

	Route::resource('links', App\Http\Controllers\LinkController::class);
	Route::get('/links/destroy/{id}', [App\Http\Controllers\LinkController::class,'destroy'])->name('links.destroy');

	Route::resource('generalsettings', App\Http\Controllers\GeneralSettingController::class);
	Route::get('/logo', [App\Http\Controllers\GeneralSettingController::class,'logo'])->name('generalsettings.logo');
	Route::post('/logo', [App\Http\Controllers\GeneralSettingController::class,'storeLogo'])->name('generalsettings.logo.store');
	Route::get('/color', [App\Http\Controllers\GeneralSettingController::class,'color'])->name('generalsettings.color');
	Route::post('/color', [App\Http\Controllers\GeneralSettingController::class,'storeColor'])->name('generalsettings.color.store');

	Route::resource('seosetting', App\Http\Controllers\SEOController::class);

	Route::post('/pay_to_seller', [App\Http\Controllers\CommissionController::class,'pay_to_seller'])->name('commissions.pay_to_seller');

	//Reports
	Route::get('/stock_report', [App\Http\Controllers\ReportController::class,'stock_report'])->name('stock_report.index');
	Route::get('/in_house_sale_report', [App\Http\Controllers\ReportController::class,'in_house_sale_report'])->name('in_house_sale_report.index');
	Route::get('/seller_report', [App\Http\Controllers\ReportController::class,'seller_report'])->name('seller_report.index');
	Route::get('/seller_sale_report', [App\Http\Controllers\ReportController::class,'seller_sale_report'])->name('seller_sale_report.index');
	Route::get('/wish_report', [App\Http\Controllers\ReportController::class,'wish_report'])->name('wish_report.index');

	//Coupons
	Route::resource('coupon', App\Http\Controllers\CouponController::class);
	Route::post('/coupon/get_form', [App\Http\Controllers\CouponController::class,'get_coupon_form'])->name('coupon.get_coupon_form');
	Route::post('/coupon/get_form_edit', [App\Http\Controllers\CouponController::class,'get_coupon_form_edit'])->name('coupon.get_coupon_form_edit');
	Route::get('/coupon/destroy/{id}', [App\Http\Controllers\CouponController::class,'destroy'])->name('coupon.destroy');

	//Reviews
	Route::get('/reviews', [App\Http\Controllers\ReviewController::class,'index'])->name('reviews.index');
	Route::post('/reviews/published', [App\Http\Controllers\ReviewController::class,'updatePublished'])->name('reviews.published');

	//Support_Ticket
	Route::get('support_ticket/', [App\Http\Controllers\SupportTicketController::class,'admin_index'])->name('support_ticket.admin_index');
	Route::get('support_ticket/{id}/show', [App\Http\Controllers\SupportTicketController::class,'admin_show'])->name('support_ticket.admin_show');
	Route::post('support_ticket/reply', [App\Http\Controllers\SupportTicketController::class,'admin_store'])->name('support_ticket.admin_store');

	//Marketing
	Route::get('/marketing/RewardPoints', [App\Http\Controllers\MarketingController::class,'reward_points'])->name('marketing.reward_points');
	Route::get('/marketing/CustomerPoints', [App\Http\Controllers\MarketingController::class,'customer_points'])->name('marketing.customer_points');
	Route::get('/marketing/SellerPoints', [App\Http\Controllers\MarketingController::class,'seller_points'])->name('marketing.seller_points');
	Route::get('/marketing/PayoutRequests', [App\Http\Controllers\MarketingController::class,'payout_request'])->name('marketing.payout_request');
	Route::get('/marketing/CompleteRequests', [App\Http\Controllers\MarketingController::class,'complete_request'])->name('marketing.complete_request');
	Route::post('/marketing/PaymentForm', [App\Http\Controllers\MarketingController::class,'payment_form'])->name('marketing.pay_form');
	Route::post('/marketing/PaymentByAdvertiser', [App\Http\Controllers\MarketingController::class,'payment_by_advertiser'])->name('marketing.payment_by_advertiser');
	Route::get('/marketing/{id}/RejectRequest', [App\Http\Controllers\MarketingController::class,'reject_request'])->name('marketing.reject_request');
	Route::get('/marketing/{id}/PaymentVoucher', [App\Http\Controllers\MarketingController::class,'advertiser_payment_voucher'])->name('marketing.advertiser_payment_voucher');
	Route::get('/marketing/AdvertiserDetailsView', [App\Http\Controllers\MarketingController::class,'advertiser_details_view'])->name('marketing.advertiser_details_view');
	Route::get('/marketing/advertiser', [App\Http\Controllers\MarketingController::class,'advertisers'])->name('marketing.advertiser');
	Route::post('/marketing/update_advertiser_status', [App\Http\Controllers\MarketingController::class,'update_advertiser_status'])->name('marketing.update_advertiser_status');
	Route::get('/marketing/sellers', [App\Http\Controllers\MarketingController::class,'sellers'])->name('marketing.sellers');

	//Careers
	Route::resource('careers', App\Http\Controllers\CareersController::class);
	Route::get('/careers', [App\Http\Controllers\CareersController::class,'index'])->name('careers.index');
	Route::get('/careers/create', [App\Http\Controllers\CareersController::class,'create'])->name('careers.create');
	Route::post('/careers/store', [App\Http\Controllers\CareersController::class,'store'])->name('careers.store');
	Route::get('/careers/destroy/{id}', [App\Http\Controllers\CareersController::class,'destroy'])->name('careers.destroy');
	Route::get('/careers/{id}/edit', [App\Http\Controllers\CareersController::class,'edit'])->name('careers.edit');
	Route::post('/careers/{id}/update', [App\Http\Controllers\CareersController::class,'update'])->name('careers.update');

	//Careers
	Route::resource('supportusers', SupportUsersController::class);
	Route::get('/support', [App\Http\Controllers\SupportUsersController::class,'index'])->name('support.index');
	Route::get('/support/create', [App\Http\Controllers\SupportUsersController::class,'create'])->name('support.create');
	Route::post('/support/store', [App\Http\Controllers\SupportUsersController::class,'store'])->name('support.store');
	Route::get('/support/destroy/{id}', [App\Http\Controllers\SupportUsersController::class,'destroy'])->name('support.destroy');
	Route::get('/support/{id}/edit', [App\Http\Controllers\SupportUsersController::class,'edit'])->name('support.edit');
	Route::post('/support/{id}/update', [App\Http\Controllers\SupportUsersController::class,'update'])->name('support.update');
});
