<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
     'google' => [
        'client_id'     => "402325178271-3dus9h89gbqggs247l7uhjak8r42ddno.apps.googleusercontent.com",
        'client_secret' => "OzV7Zi7Bee6fg044hxSQOoDi",
        'redirect'      => env('APP_URL').'social-login/google/callback',
    ],

    'facebook' => [
        'client_id'     => "283355470337231",
        'client_secret' => "c978b4f2cd5e1171ebe62a78103b3ebb",
        'redirect'      => "https://myqrguide.com/social-login/facebook/callback",
    ],
    'linkedin' => [
    'client_id' => "78l00hv0ap6t8i",
    'client_secret' => "uXKiYYI0WOyQJMEk",
    'redirect' => "https://myqrguide.com/social-login/linkedin/callback",
],

];
